<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLDS"); //Categoria del modulo
require_once "ctrl_diafestivo.control.php";// Class CONTROL ControlCtrlDiafestivo()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlDiafestivo extends ControlCtrlDiafestivo{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlDiafestivo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;
        
        if (count(CtrlDiafestivos::getDiasRepetidos($_REQUEST["dia"], $_REQUEST["mes"], $_REQUEST["id"]))>0){
            $this->mensaje = "El D&iacute;a Festivo ".sprintf('%02d',$_REQUEST["dia"])."-".sprintf('%02d',$_REQUEST["mes"])."-".date("Y")." ya est&aacute; registrado.";
            return false;
        }

        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlDiafestivo()) return false;

        //------------------ Sincronizar Horarios  -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar()){
            $this->mensaje = $sinc->mensaje;
            return false;
        }
        
        return true;        
    }
    
    /**
     * Proceso de resetear horario (borrar)
     * @return boolean Devuelve verdadero si se resetea correctamente
     */
    function resetCtrlDiafestivo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();

        $_REQUEST["nombre"] = "-";
        $_REQUEST["dia"] = "0";
        $_REQUEST["mes"] = "0";
        
        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlDiafestivo()) return false;

        //------------------ Sincronizar Horarios  -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar()){
            $this->mensaje = $sinc->mensaje;
            return false;
        }

        return true;
    }
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlDiafestivo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlDiafestivo()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("D&iacute;a Festivo");
        
        $this->setCampos("nombre","Nombre");
        $this->setCampos("mes","Mes");
        $this->setCampos("dia","D&iacute;a");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"mes","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"dia","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

}
?>