<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * 
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
require_once "../controller/ctrl_diafestivo.control.op.php";// Class CONTROLLER
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link type="text/css" href="../css/fullcalendar.css" rel="stylesheet" />
        <link type="text/css" href="../css/fullcalendar.print.css" rel="stylesheet" media='print'/>
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/fullcalendar.js"></script>

        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 700px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;                
            }
            #botones{
                margin: 10px;
            }
            /*#loading {
		position: absolute;
		top: 5px;
		right: 5px;
            }*/

            #calendar {
                width: 700px;
		
		margin: 0 auto;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            .dias{
                height: 65px;
            }
            #divmensaje{
                width: 99%;
                position: absolute;
                top: 25px;
                opacity:0.9;
            }
            
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
                
                $('#calendar').fullCalendar({
                    buttonText: {
                            today: 'Hoy'
                    },
                    monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                    monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
                    dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
                    dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
                    editable: true,
                    disableResizing:true,
                    events: {
                        url: 'fullcalendar-events.php',
                        className:'dias'
                    },
                    eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {                                               
                        var dia = event.start.getDate();
                        var mes = event.start.getMonth()+1;                        
                        if (event.start.getFullYear()!='<?php echo date("Y")?>'){                            
                            setMensaje({'exito':false,'mensaje':'Los días festivos deben ser asignados al año en curso: <?php echo date("Y")?>'});
                            revertFunc();                            
                        }else{
                            $.ajax({
                                type: 'POST',
                                dataType:'json',
                                url: 'ctrl_diafestivo.Op.php',
                                data: 'id='+event.id+'&nombre='+event.title+'&dia='+dia+'&mes='+mes+'&accion=modificar&ajax=true',
                                success: function(res){                                    
                                    setMensaje(res);
                                    if (res.exito){
                                        setTimeout('top.location.href="../../inicio/view/index.php?op=CtrlDiafestivo";', 3*1000 );
                                    }
                                    if (!res.exito){                                        
                                        revertFunc();
                                    }                                    
                                }
                             });
                        }                        
                    },
                    loading: function(bool) {
                            //if (bool) $('#loading').show('slow');
                            //else $('#loading').hide('slow');
                            if (bool) $("#loading").fadeIn("slow");
                            else $("#loading").fadeOut("slow");

                    }
		});
                $('#divmensaje').live({
                    click: function() {
                        $(this).fadeOut("slow");
                    }
                });
            });

            function setMensaje(obj){
                var str;
                str = '<div class="ui-widget">';
                if (obj.exito){
                    str += '    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
                    str += '        <p>';
                    str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
                    str += '                '+obj.mensaje;
                    str += '        </p>';
                    str += '    </div>';
                }
                if (!obj.exito){
                    str += '    <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
                    str += '        <p>';
                    str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
                    str += '                '+obj.mensaje;
                    str += '        </p>';
                    str += '    </div>';
                }
                str += '</div>';
                $('#divmensaje').hide();
                $('#divmensaje').html(str).fadeIn("slow");
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <?php
        if (!Controller::chkPermiso($_SESSION["usuario"], "CTRLDS","calendario")){
            echo "<div id=\"contenido\" align=\"center\">".Controller::$mensajePermisos."</div>";
        }else{
            ?>
            <div id="loading" align="center" style="position: absolute;background-color:#000000;opacity:.7;width: 100%;height: 100%;vertical-align: middle;color: #FFFFFF;font-size: 20px;">
                <br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<img src="../images/loading51.gif" border="0"><br>&nbsp;<b>Cargando...</b>
            </div>
            <div id="contenido" align="center">
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> D&iacute;a Festivo</div>
                <br/>
                <div id="divmensaje" ></div>
                <div id='calendar'></div>
            </div>
            <?php
        }
        ?>
    </body>
</html>