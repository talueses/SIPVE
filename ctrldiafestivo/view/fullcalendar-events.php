<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../controller/ctrl_diafestivo.control.op.php";// Class CONTROLLER

$obj  = new CtrlDiafestivos();
$dataAll = $obj->getCtrlDiafestivos();

$year = date('Y');
$month = date('m');

$data = array();
if (count($dataAll)>0){

    foreach ($dataAll as $row){
        if ($row->mes!="0" && $row->dia!="0"){
            $data [] = array(
                'id' => $row->id,
                'title' => $row->nombre,
                'start' => date("Y")."-".sprintf('%02d',$row->mes)."-".sprintf('%02d',$row->dia),
                'url' => "ctrl_diafestivo.Acc.php?id=".$row->id."&accion=modificar"
            );
        }
        
    }
    echo json_encode($data);
}
?>