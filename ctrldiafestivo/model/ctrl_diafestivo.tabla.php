<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlDiafestivo {
    
    private $id = null;
    private $nombre = null;
    private $mes = null;
    private $dia = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setMes($mes){
        $this->mes = $mes;
    }
    public function getMes(){
        return $this->mes;
    }
    public function setDia($dia){
        $this->dia = $dia;
    }
    public function getDia(){
        return $this->dia;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->id !== null) && (trim($this->id)!=='') ){
            $campos .= "id,";
            $valores .= "'".$this->id."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->mes !== null) && (trim($this->mes)!=='') ){
            $campos .= "mes,";
            $valores .= "'".$this->mes."',";
        }
        if(($this->dia !== null) && (trim($this->dia)!=='') ){
            $campos .= "dia,";
            $valores .= "'".$this->dia."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_diafestivo $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_diafestivo SET ";
        
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        if(($this->mes !== null) && (trim($this->mes)!=='') ){
            $sql .= "mes = '".$this->mes."',";
        }
        if(($this->dia !== null) && (trim($this->dia)!=='') ){
            $sql .= "dia = '".$this->dia."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_diafestivo  WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlDiafestivos
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlDiafestivos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idBuscador"]){
            $in = null;
            foreach ($_REQUEST["idBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where id in (".$in.")";
        }
        
        $sql = "select /*start*/ id,nombre,mes,dia /*end*/ from ctrl_diafestivo ".$arg." order by nombre,mes";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlDiafestivo
     * @param int $id Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlDiafestivo($id){
        $sql = "SELECT id,nombre,mes,dia FROM ctrl_diafestivo WHERE id = '".$id."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>