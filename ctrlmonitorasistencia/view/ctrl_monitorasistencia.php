<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_monitorasistencia.control.op.php";// Class CONTROLLER
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>                
        <script type="text/javascript" src="../js/ctrl_monitorasistencia.local.js"></script>
        
        <style type="text/css"> 
            
            #tabla {
                width:1100px;
                height: 98%;
                            
            }
            #listado{
                width:99%;
                margin: 0px 1px 0px 1px;
                
            }
            #titulo{
                font-size: 14px;
                padding: 5px 0px 5px 0px;
                
            }   
            #header-fixed {
                width:1089px;
                position: fixed;
                top: 0px;
                margin-left: 6px;
                display:none;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #listado tr td {
                border: 1px #ddd solid;                
            }
            
            #listado tr.markCedula{
                background:#ddd;
            }
            #cargando{
                width: 250px;
                background-color:#000000;
                opacity:.7;
                color: #FFFFFF;
                font-size: 20px;
                margin: 10px;
                padding: 10px;
            }    
            #logContent {
                margin-top: 1px;
                overflow-x:auto;
                overflow-y: auto;
            }
            
            #loadinImg, #popUpImg{
                width: 1100px;
                position: absolute;
                top: 10px;
                z-index: 9999;
            }
            #loadinImg img{                
                float: right;
                position: relative;                
                top: 1px;
                margin-right: 16px; 
            }            
            #popUpImg button{               
                font-size: 0px;                
                float: left;              
                position: relative;
                top: -3px;
                height: 32px;
                width: 32px;
                padding: 0px 0px 0px 0px ;
                margin-left: 16px;
                
            }
            small.comment{
                font-size: 9px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <center>
            <?php
            if ((!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,'monitor')) || (!Controller::$chkCookie["CtrlAsistencia"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="logContent">                
                    <center>
                        <div id="tabla" class="ui-corner-all ui-widget-content">
                            <div id="cargando"  class="ui-corner-all" align="center" >                        
                                <img src="../images/loading51.gif" alt="Cargando..." /><br>&nbsp;<b>Cargando...</b>
                            </div>
                        </div>                        
                    </center>
                </div>
                <?php
            }
            ?>            
        </center>
        <input type="hidden" name="logID" id="logID" />
    </body>
</html>