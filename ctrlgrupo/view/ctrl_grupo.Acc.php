<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_grupo.control.op.php";// Class CONTROLLER
$obj = new CtrlGrupos();
$numExist = $obj->getNumerosGrupos();
if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){    
    $data     = $obj->getCtrlGrupo($_REQUEST["idgrupo"]);    
    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" media="screen" href="../css/jquery.toChecklist.css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/jquery.toChecklist.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $('input:text,input:password').css({
                    background: '#fff' ,
                    border: '1px solid #d5d5d5',
                    '-moz-border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    'border-radius': '4px'
                });
                $( "input:button, input:submit" ).button();

                //$('#idpuerta').toChecklist();
                <?php
                // ------- FUNCION JS DESABILITA EL LISTADO DE PUERTAS CUANDO SE VISUALISAN LOS DATOS DE LOS GRUPOS -------- //
                if ($_REQUEST["accion"] == "visualizar"){
                    echo "  $(\"#idpuerta input[type='checkbox']\").attr(\"disabled\",true);";
                }
                ?>
            });
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Grupo</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="ctrl_grupo.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr title="Grupo">
                                <td align="right">Grupo:</td>
                                <td>
                                    <input type="text" name="grupo" id="grupo" maxlength="100" value="<?php echo $data->grupo;?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="N&uacute;mero">
                                <td align="right">N&uacute;mero:</td>
                                <td>
                                    <select name="numero" id="numero" onchange="" <?php echo $disabled;?> >                                        
                                        <?php
                                        for ($i=1;$i<=255;$i++){
                                            $chkNum = false;
                                            foreach ($numExist as $row){
                                                if ($row->numero == $i){
                                                    $chkNum = true;
                                                }
                                            }
                                            if (!$chkNum || $data->numero == $i){
                                                echo '<option value="'.$i.'" '.ControlCtrlGrupo::busca_valor($data->numero, $i ).'>'.$i.'</option>';
                                            }

                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Nivel de Prioridad">
                                <td align="right">Nivel:</td>
                                <td>
                                    <select name="nivel" id="nivel" <?php echo $disabled;?>>                                        
                                        <?php
                                        for ($i=0;$i<=63;$i++){

                                            echo "<option value=\"".$i."\" ".ControlCtrlGrupo::busca_valor($data->nivel, $i).">".$i."</option>\n";

                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Grupo de Enlace">
                                <td align="right">Grupo Enlace:</td>
                                <td>
                                    <select name="idgrupo_padre" id="idgrupo_padre" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php
                                        if ($data->idgrupo){
                                            $arg = "where idgrupo <> '".$data->idgrupo."'";//and idgrupo_padre <> '".$data->idgrupo_padre."'
                                        }
                                        echo ControlCtrlGrupo::make_combo("ctrl_grupo",$arg." order by grupo", "idgrupo", "grupo", $data->idgrupo_padre,false);?>
                                    </select>
                                </td>
                            </tr>
                            <?php
                            //if ($_REQUEST["accion"]!="agregar"){
                                ?>
                                <tr title="Puertas">
                                    <td align="right" valign="top">Puertas:</td>
                                    <td>
                                        <select name="idpuerta[]" id="idpuerta" <?php echo $disabled;?> multiple style="width: 400px;" size="10">
                                            <?php                                            
                                            echo ControlCtrlGrupo::make_combo("ctrl_puerta p,ctrl_lectora cl, ctrl_controladora cc", " where p.idpuerta = cl.idpuerta and cl.idcontroladora = cc.idcontroladora and cc.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') order by cast(p.numero as :cast_int),p.puerta", "p.idpuerta", "p.numero,p.puerta", CtrlGrupos::getPuertasGrupo($data->idgrupo?$data->idgrupo:"0"));
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <?php
                            //}
                            ?>
                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idgrupo" id="idgrupo" value="<?php echo $data->idgrupo ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Grupo\" />";
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>