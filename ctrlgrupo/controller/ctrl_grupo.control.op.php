<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLGPR"); //Categoria del modulo
require_once "ctrl_grupo.control.php";// Class CONTROL ControlCtrlGrupo()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlGrupo extends ControlCtrlGrupo{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlGrupo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        //------------------ Metodos Set  -----------------//
        if(!$this->setCtrlGrupo()) return false;
        
        if ($this->accion == "agregar"){
            $_REQUEST["idgrupo"] = DB_Class::$PDO->lastInsertId("ctrl_grupo_idgrupo_seq");
        }        
        
        if(!$this->setGrupoPuertas()) return false;        

        //------------------ Sincronizar Grupos de Puertas  -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        $data = CtrlGrupos::getNumerosPuertas($_REQUEST["idgrupo"]);
        foreach ($data as $row){
            $sinc->agregarPuerta($row->numero);
        }            
        $exito = $sinc->setOpCtrlSincronizar();
        if(!$exito){
            $this->mensaje = $sinc->mensaje;
            return false;
        }
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlGrupo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodos Set  -----------------//
        if(!$this->eliminarGrupoPuertas()) return false;
        if(!$this->eliminarCtrlGrupo()) return false;        

        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Grupo");
        
        $this->setCampos("numero","N&uacute;mero");
        $this->setCampos("grupo","Grupo");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"grupo","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"numero","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;        
        
        return true;
    }

    // ------------------- PUERTAS DE UN GRUPO -----------------------//

    /**
     * Agregar Puertas a un Grupo
     * @return boolean Devuelve verdadero si se registra correctamente
     */
    function setGrupoPuertas(){
        $obj = new CtrlGrupoPuerta();
        $exito = true;
        //--------------------- DATOS --------------------------//
        $obj->setIdgrupo($_REQUEST["idgrupo"]);

        $obj->eliminarRegistro($obj->getIdgrupo());
        if (count($_REQUEST["idpuerta"])>0){
            foreach ($_REQUEST["idpuerta"] as $idpuerta){            
                $obj->setIdpuerta($idpuerta);
                $exito = $obj->insertarRegistro();
            }
        }        
        if(!$exito){
            $this->mensaje = "Las Puertas no pudieron ser agregadas: ".$obj->getMensaje();
            return false;
        }        
        return true;
    }

    /**
     * === Eliminar Puertas de un Grupo ===
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarGrupoPuertas(){
        // -------------------- ELIMINAR LECTORA ----------------------//
        $obj = new CtrlGrupoPuerta();
        //--------------------- DATOS --------------------------//
        $obj->setIdgrupo($_REQUEST["idgrupo"]);

        $exito = $obj->eliminarRegistro($obj->getIdgrupo());
        if(!$exito){
            $this->mensaje = "Las Puertas asociadas al grupo no pudieron ser eliminadas: ".$obj->getMensaje();
            return false;
        }
        return true;
    }


    
    
}
?>