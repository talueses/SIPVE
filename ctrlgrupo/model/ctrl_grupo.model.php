<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'ctrl_grupo.tabla.php';
require_once 'ctrl_grupo_puerta.tabla.php';


/**
 * Clase CtrlGrupos{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlGrupos extends CtrlGrupo{

    /**
     * Consulta de puertas de un grupo
     * @param int $idgrupo Codigo del Grupo
     * @return object Devuelve registros como objeto
     */
    static function getPuertasGrupo($idgrupo){
        
        $sql = "SELECT idpuerta FROM ctrl_grupo_puerta WHERE idgrupo = '".$idgrupo."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_COLUMN);
        
    }

    /**
     * Consulta de los numeros existentes de grupos
     * @return object Devuelve registros como objeto
     */
    function getNumerosGrupos() {
        $sql = "SELECT numero FROM ctrl_grupo";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de los numeros existentes de puertas asociadas a un grupo
     * @return object Devuelve registros como objeto
     */
     static function getNumerosPuertas($idgrupo) {
        $sql = "select p.numero from ctrl_grupo_puerta gp, ctrl_puerta p where gp.idgrupo = '".$idgrupo."' and gp.idpuerta = p.idpuerta";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
}
?>
