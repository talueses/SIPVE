<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlGrupo {
    
    private $idgrupo = null;
    private $numero = null;
    private $grupo = null;
    private $idgrupo_padre = null;
    private $nivel = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdgrupo($idgrupo){
        $this->idgrupo = $idgrupo;
    }
    public function getIdgrupo(){
        return $this->idgrupo;
    }
    public function setNumero($numero){
        $this->numero = $numero;
    }
    public function getNumero(){
        return $this->numero;
    }
    public function setGrupo($grupo){
        $this->grupo = $grupo;
    }
    public function getGrupo(){
        return $this->grupo;
    }
    public function setIdgrupo_padre($idgrupo_padre){
        $this->idgrupo_padre = $idgrupo_padre;
    }
    public function getIdgrupo_padre(){
        return $this->idgrupo_padre;
    }
    public function setNivel($nivel){
        $this->nivel = $nivel;
    }
    public function getNivel(){
        return $this->nivel;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idgrupo !== null) && (trim($this->idgrupo)!=='') ){
            $campos .= "idgrupo,";
            $valores .= "'".$this->idgrupo."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $campos .= "numero,";
            $valores .= "'".$this->numero."',";
        }
        if(($this->grupo !== null) && (trim($this->grupo)!=='') ){
            $campos .= "grupo,";
            $valores .= "'".$this->grupo."',";
        }
        if(($this->idgrupo_padre !== null) && (trim($this->idgrupo_padre)!=='') ){
            $campos .= "idgrupo_padre,";
            $valores .= "'".$this->idgrupo_padre."',";
        }
        if(($this->nivel !== null) && (trim($this->nivel)!=='') ){
            $campos .= "nivel,";
            $valores .= "'".$this->nivel."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_grupo $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_grupo SET ";
        
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $sql .= "numero = '".$this->numero."',";
        }
        if(($this->grupo !== null) && (trim($this->grupo)!=='') ){
            $sql .= "grupo = '".$this->grupo."',";
        }
        if(($this->idgrupo_padre !== null) && (trim($this->idgrupo_padre)!=='') ){
            $sql .= "idgrupo_padre = '".$this->idgrupo_padre."',";
        }
        else{
            $sql .= "idgrupo_padre = NULL,";
        }
        if(($this->nivel !== null) && (trim($this->nivel)!=='') ){
            $sql .= "nivel = '".$this->nivel."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idgrupo = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_grupo  WHERE idgrupo = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlGrupos
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlGrupos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idgrupoBuscador"]){
            $in = null;
            foreach ($_REQUEST["idgrupoBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idgrupo in (".$in.")";
        }
        
        $sql = "select /*start*/ idgrupo,numero,grupo,idgrupo_padre,nivel /*end*/ from ctrl_grupo ".$arg." order by numero,grupo";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlGrupo
     * @param int $idgrupo Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlGrupo($idgrupo){
        $sql = "SELECT idgrupo,numero,grupo,idgrupo_padre,nivel FROM ctrl_grupo WHERE idgrupo = '".$idgrupo."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>