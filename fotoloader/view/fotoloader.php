<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Interfaz de captura, carga y edicion (cortar) de imagenes
 * @author David Concepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
require_once "../controller/fotoloader.control.php";// Class CONTROLLER
//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

if ($_REQUEST["tipo"]=="empleado"){

}
if ($_REQUEST["tipo"]=="visitante"){

}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <!-- First, include the JPEGCam JavaScript Library -->
        <script type="text/javascript" src="../js/webcam.js"></script>
        <script type="text/javascript" src="../js/jquery.Jcrop.js"></script>
        <link rel="stylesheet" href="../css/jquery.Jcrop.css" type="text/css" />
        
        <!-- Local JS Library -->
        <script type="text/javascript" src="../js/fotoloader.local.js"></script>
        
        <style type="text/css">
            .contenido{                
                width:99%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            img{
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #divUploadPhoto, #divTakePhoto, #crop, #options{
                margin-top: 5px;
            }

            #divmensaje{
                width:99%;
                position: absolute;
                top: -10px;
                opacity:0.9;
            }
        </style>
        <script type="text/javascript">
            

        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <iframe name="opFrame" id="opFrame" width="0" height="0" frameborder="0"></iframe>
            <form id="mainForm" name="mainForm" action="fotoloader.Op.php" method="post" target="opFrame"  enctype="multipart/form-data">
            <!-- Historial de imagenes guardadas-->
            <div id="selectImage" align="center"></div>
            <!-- Controles -->
            <div id="options" align="center">                
                <input type="button" value="Capturar Foto" onclick="setView('capturar')" >
                <input type="button" value="Cargar Foto" onclick="setView('cargar')" >
                <input type="button" value="Eliminar Foto" onclick="isValid('deletePhoto');" title="Eliminar foto seleccionada">
                <input type="button" value="Guardar Foto" onclick="isValid('savePhoto');" title="Guardar foto seleccionada">
            </div>
            <!-- Mensajes de operaciones -->
            <div id="divmensaje"></div>
            <!-- Video de la camara Camera -->
            <div id="divTakePhoto" align="center">
                <button name="takePhoto" id="takePhoto" type="button" onClick="take_snapshot()" title="Capturar Foto" >
                    <div id="webcam" ></div>
                    <div style="width: 100%;text-align: center;margin-top: 5px;"><b>Tomar Foto</b></div>
                </button>
            </div>
            <!-- Cortar imagen -->
            <div id="crop" class="contenido">
                <table border="0" width="100%">
                    <tr>
                        <td width="50%" align="center">
                            <!-- Imagen guardada -->
                            <div id="uploaded_image" class="contenido"></div>
                            <!-- Subir archivo de Imagen -->
                            <div id="divUploadPhoto">
                                <form id="uploadForm" name="uploadForm" action="fotoloader.Op.php" method="post" target="opFrame">
                                    <input name="file_foto" type="file" id="file_foto" size="5" onchange="previewImg(this)" />
                                    <input type="button" name="ButtonUploadPhoto" id="ButtonUploadPhoto" value="Cargar Foto" onclick="isValid('uploadPhoto');" />
                                </form>
                            </div>
                        </td>
                        <td align="center" valign="middle"  width="50%">
                            <!-- Previsualizacion de corte de imagen -->
                            <div class="contenido" style="padding: 20px;width: 200px;" id="divPreview"></div>
                            <br>
                            <input type="button" value="Cortar Foto" onclick="isValid('crop');" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="thumbnail_fields" >
                <input type="hidden" id="x1" name="x1" />
                <input type="hidden" id="y1" name="y1" />
                <input type="hidden" id="x2" name="x2" />
                <input type="hidden" id="y2" name="y2" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <input type="hidden" id="cedula" name="cedula" value="<?php echo $_REQUEST["cedula"]?>" />
                <input type="hidden" id="tipo" name="tipo" value="<?php echo $_REQUEST["tipo"]?>" />
                <input type="hidden" id="accion" name="accion" value="" />
                <input type="hidden" id="widthCrop" name="widthCrop" value="<?php echo $_REQUEST["foto_w"]?>"/>
                <input type="hidden" id="heightCrop" name="heightCrop"  value="<?php echo $_REQUEST["foto_h"]?>"/>
                <input type="hidden" id="maxWidth" name="maxWidth" value="320"/>
                <input type="hidden" id="maxHeight" name="maxHeight"  value="240"/>
                <input type="hidden" id="archivo_foto" name="archivo_foto" value="<?php echo $_REQUEST["archivo_foto"]?>" />
                <input type="hidden" id="archivo_foto_old" name="archivo_foto_old" value="<?php echo $_REQUEST["archivo_foto"]?>" />
            </div>
        </form>        
    </body>
</html>