<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../model/fotoloader.model.php"; // Class MODEL 
require_once "../../inicio/controller/controller.php";// Class CONTROL Controller()

/**
 * Description
 * @author David Concepcion
 */
class ControlFotoLoader{
    /**
     * @var string Carpeta fotos
     */
    private $folder = "../images/fotos/";
    /**
     * @var string mensaje de exito o error
     */
    var $mensaje = null;
    /**
     * @var string accion agregar, modificar o eliminar dato
     */
    var $accion  = null;
    /**
     * @var string Ruta donde se guardan las imagenes. Valor por defecto "../images/fotos"
     */
    private $uploaddir = "../images/fotos/";
    /**
     * @var int Ancho maximo de la imagen.
     */
    private $maxWidth   = null ;
    /**
     * @var int Alto maximo de la imagen.
     */
    private $maxHeight  = null;
    /**
     * @var int Peso maximo en bits. Valor por defecto 2.097.152 Bits, equivalen a 2 MB.
     */
    private $maxSize    = 2097152;
    /**
     * Establece la acción
     * @param string $accion Acción
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }
    /**
     * @return string Devuelve la accion establecida
     */
    public function getAccion(){
         return $this->accion;
    }
    /**
     * Establece el Ancho maximo de la imagen
     * @param string $accion Acción
     */
    public function setMaxWidth($maxWidth){
         $this->maxWidth = $maxWidth;
    }
    /**
     * @return string Devuelve el Ancho maximo de la imagen
     */
    public function getMaxWidth(){
         return $this->maxWidth;
    }
    /**
     * Establece el Alto maximo de la imagen
     * @param string $accion Acción
     */
    public function setMaxHeight($maxHeight){
         $this->maxHeight = $maxHeight;
    }
    /**
     * @return string Devuelve el Alto maximo de la imagen
     */
    public function getMaxHeight(){
         return $this->maxHeight;
    }
        
    /**
     * Resive imagen JPEG de WEBCAM y guarda la imagen en el servidor
     * Libreria JPEGCam JS
     * @return string Devuelve el nombre del archivo guardado
     */
    function setCamPhoto(){
        /* JPEGCam Test Script */
        /* Receives JPEG webcam submission and saves to local file. */
        /* Make sure your directory has permission to write files as your web server user! */

        $file = $this->folder.strtoupper(substr($_REQUEST["tipo"], 0,1)).$_REQUEST["cedula"].".jpg";
        $result = file_put_contents( $file, file_get_contents('php://input') );

        if (!$result) {
            return "ERROR: Fallo al escribir el archivo $file. Verificar Permisos\n";
            exit();
        }

        //$url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . '/' . $filename;
        $url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']).'/|'.$file;
        return $url."\n";
    }
    
    /**
     * Carga un archivo de imagen en el servidor
     * @param file $param Archivo cargado en HTML
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function uploadFile($param){
        //echo "<div align='left'><pre>".print_r($param,true)."</pre></div>";
        /**
         * @var string $param["fileObj"]  Nombre del objeto FILE que se pasa por el formulario
         * @var string $param["objName"]  Nombre del objeto  que contiene el nombre del archivo guardado que se pasa por el formulario
         * @var string $param["fileName"] Nombre que se le da al archivo a guardar;
         */
        $exito = true;
        

        $file = $_FILES[$param["fileObj"]]['name'];

        //echo $param["fileObj"]."---".$name."---".$file."<br>";

        if (!empty($file)){

            
            $imageInfo = getimagesize($_FILES[$param["fileObj"]]["tmp_name"]);
            if (!$imageInfo){
                $this->mensaje = "El archivo debe ser una imagen.";
                return false;
            }else{
                if ($imageInfo[0] > $this->getMaxWidth() || $imageInfo[1] > $this->getMaxHeight()){
                    $this->mensaje = "La imagen es muy grande. Debe tener un m&aacute;ximo de ".$this->getMaxWidth()." x ".$this->getMaxHeight()." pixeles.";
                    return false;
                }
            }


            $size = $_FILES[$param["fileObj"]]['size'];
            if ($size > $this->maxSize){
                $this->mensaje = "La imagen de ".ucfirst($name)." es muy grande. Debe tener un m&aacute;ximo de ".$this->file_size($this->maxSize);
                return false;
            }

            // Eliminar archivo ya creado //
            if (!empty($_REQUEST[$param["objName"]])){
                $exito = unlink($this->uploaddir.$_REQUEST[$param["objName"]]);
            }

            if ($exito){

                $ext = substr($file,strlen($file)-4,4);
                $file = $param["fileName"].$ext;

                $url_file = $this->uploaddir.$file;
                $exito = move_uploaded_file($_FILES[$param["fileObj"]]['tmp_name'], $url_file);
                
                if ($exito){
                    $src_image = imagecreatefromstring(file_get_contents($url_file));
                    $exito = imagejpeg($src_image, $this->uploaddir.$param["fileName"].".jpg",100);
                    if ($ext!=".jpg"){
                        $exito = @unlink($url_file);
                    }                    
                }
            }

        }else{
            if (!empty($_REQUEST[$param["objName"]])){
                $file = $_REQUEST[$param["objName"]];
            }
        }

        $_REQUEST[$param["objName"]]= $file;

        if (!$exito){
            $this->mensaje = "Ocurri&oacute; un error al guardar la imagen ";
            return false;
        }
        $this->mensaje = "La imagen se carg&oacute; exitosamente...";
        return true;
    }

    /**
     * Elimina un archivo de imagen en el servidor
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function deleteFile(){
        if ($_REQUEST["radioImage"]==""){
            $this->mensaje = "Debe seleccionar una imagen";
            return false;
        }
        $exito = @unlink($this->folder.strtoupper(substr($_REQUEST["tipo"], 0,1)).$_REQUEST["cedula"]."-crop-".$_REQUEST["radioImage"].".png");
        if (!$exito){
            $this->mensaje = "Ocurri&oacute; un error al eliminar la imagen";
            return false;
        }
        $this->mensaje = "La imagen fu&eacute; eliminada exitosamente...";
        return true;
    }

    /**
     * Guarda la imagen seleccionada en el modulo de carnet respectivo (Empleado - Visitante)
     * @return boolean Devuelve verdadero si el proseco de guardar se ejecuta exitosamente
     */
    function saveFile(){
        if ($_REQUEST["radioImage"]==""){
            $this->mensaje = "Debe seleccionar una imagen";
            return false;
        }

        $file = strtoupper(substr($_REQUEST["tipo"], 0,1)).$_REQUEST["cedula"]."-crop-".$_REQUEST["radioImage"].".png";
        $source = $this->folder.$file;
        if ($_REQUEST["tipo"]=="empleado"){
            $dest = "../../carempleado/images/fotos/".$file;
        }
        if ($_REQUEST["tipo"]=="visitante"){

        }
        $exito = copy($source, $dest);
        if (!$exito){
            $this->mensaje = "Ocurri&oacute; un error al guardar la imagen";
            return false;
        }

        $this->mensaje = "La imagen fu&eacute; guardada exitosamente...";
        return true;        
    }

    /**
     * Proceso de cortar una imagen segun dimenciones establecidas desde una imagen origen
     * Se genera una nueva imagen
     * @return boolean Devuelve verdadero si el proseco de cortar se ejecuta exitosamente
     */
    function setCrop(){
        $exito = true;

        if ($_POST['w']==""){
            $this->mensaje = "Debe seleccionar una porci&oacute;n de la imagen a cortar";
            return false;
        }
        
        $targ_w = $_POST['widthCrop'];
        $targ_h = $_POST['heightCrop'];

        // Nombre archivo de imagen origen
        $src_image = $this->folder.strtoupper(substr($_REQUEST["tipo"], 0,1)).$_REQUEST["cedula"].".jpg";

        // Lee archivo de imagen origen
        $src_image = imagecreatefromstring(file_get_contents($src_image));
        // Se crea imagen base destino
        $dst_image = ImageCreateTrueColor( $targ_w, $targ_h );
        // Se corta la imagen origen al tamanño de la imagen destino establecido
        imagecopyresampled($dst_image,$src_image,0,0,$_POST['x1'],$_POST['y1'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

        // Se guardan maximo 5 imagenes por persona
        for ($i=1;$i<=5;$i++){
            $file = $this->folder.strtoupper(substr($_REQUEST["tipo"], 0,1)).$_REQUEST["cedula"]."-crop-".$i.".png";
            if (!file_exists($file)){ // si el archivo no existe se guarda
                $exito = imagepng($dst_image, $file);
                break;
            }elseif ($i==5){ // Reescribir el ultimo archivo
                $exito = imagepng($dst_image, $file);
                break;
            }
        }
        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            $this->mensaje = "La imagen no pudo ser cortada";
            return false;
        }
        // ------------------------------- MENSAJE EXITO --------------------------------//
        $this->mensaje = "La imagen fu&eacute; cortada exitosamente...";
                
        return true;
    }
    
    /**
     * Generacion de objetos HTML de imagenes en radio buttons guardades en el sevidor
     * @return string Devuelve Objetos HTML de la imagenes guardadas en el servidor
     */
    function getImagesHistory(){
        
        $str = "";
        for ($i=5;$i>=1;$i--){
            $file = $this->folder.strtoupper(substr($_REQUEST["tipo"], 0,1)).$_REQUEST["cedula"]."-crop-".$i.".png";
            $checked = $file==($this->folder.$_REQUEST["archivo_foto"])?"checked=\"checked\"":"";
            if (file_exists($file)){ // Verificacion si el archivo existe
                $str .= "<input type=\"radio\" id=\"radioImage".$i."\" name=\"radioImage\" value=\"".$i."\" ".$checked." />\n";
                $str .= "<label for=\"radioImage".$i."\">\n";
                $str .= "<img src=\"".$file."?".microtime(true)."\"  style=\"width:".$_REQUEST["widthCrop"]."px;height:".$_REQUEST["heightCrop"]."px;\" title=\"Imagen-".$i."\" alt=\"Imagen-".$i."\" />\n";
                $str .= "</label>\n";
            }
        }
        return $str;
    }
    
}
?>