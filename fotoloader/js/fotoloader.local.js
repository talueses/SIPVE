var loadingMsg = '<img src="../images/loading51.gif" alt="" width="16" /> Cargando...';
var maxWidth;
var maxHeight;
var imgToLoad, imgCrop;
var widthCrop;
var heightCrop;
var d;
//------------------------------------------LOAD-------------------------------------------------------------//
$(function() {
    maxWidth  = parseInt($('#maxWidth').val());
    maxHeight = parseInt($('#maxHeight').val());
    
    widthCrop  = parseInt($('#widthCrop').val());
    heightCrop = parseInt($('#heightCrop').val());

    $( "input:button, input:submit, button" ).button();
    

    webcam.set_quality( 100 ); // JPEG quality (1 - 100)
    webcam.set_shutter_sound( true ); // play shutter click sound
    webcam.set_api_url( './fotoloader.Op.php?accion=camPhoto&cedula='+$('#cedula').val()+'&tipo='+$('#tipo').val());

    // --- Hide objs default --- //
    $('#divTakePhoto, #crop, #divUploadPhoto').hide();


    // -- Load Video from the Camera --//
    //<!-- Next, write the movie to the page at 320x240 -->
    $('#webcam').html(webcam.get_html(maxWidth, maxHeight));


    //-- Code to handle the server response --//
    webcam.set_hook( 'onComplete', 'my_completion_handler' );
    webcam.set_stealth( true );

    setImg(); //
    
    
    $('#thumbnail').attr('src','../images/Picture-file-256.png');
    $('#preview').attr({
        src: '../images/Picture-file-256.png',
        width: widthCrop,
        height: heightCrop

    });

    setView('capturar');
    //-----------------------------------MENSAJE-----------------------------------------------//
    $('#divmensaje').live({
        click: function() {
            $(this).fadeOut(500);
        }
    });

    loadImagesHistory();
});
//---------------------------------------END LOAD---------------------------------------------------------------//
function take_snapshot() {
    // take snapshot and upload to server
    $('#uploaded_image').html('Cargando Imagen ...');
    webcam.snap();
}

function my_completion_handler(msg) {

    // extract URL out of PHP output
    if (msg.match(/(http\:\/\/\S+)/)) {
        var image_url = RegExp.$1;
        var aux = image_url.split('|');


        // reset camera for another shot
        webcam.reset();

        setImg(); //
        d = new Date();
        $('#thumbnail,#preview').attr('src',aux[1]+'?'+d.getMilliseconds());

        setView('crop');
        loadJcrop();
    }
    else{
        alert("PHP Error: " + msg);
    }

}
var jcrop_api, boundx, boundy;
function loadJcrop(){
    var w = widthCrop, h=heightCrop // Get W & H from the DB

    // Create variables (in this scope) to hold the API and image size
    $('#thumbnail').Jcrop({
        onChange:   updatePreview,
        onSelect:   updatePreview,
        onRelease:  clearCoords,
        bgFade:     true,
        bgOpacity: .3,
        aspectRatio: w/h,
        minSize:     [50,50]/*,
        maxSize:     [w,h]*/
    },function(){
        // Use the API to get the real image size
        var bounds = this.getBounds();
        boundx = bounds[0];
        boundy = bounds[1];
        // Store the API in the jcrop_api variable
        jcrop_api = this;
    });


}


function clearCoords()
{
    $('#x1').val('');
    $('#y1').val('');
    $('#x2').val('');
    $('#y2').val('');
    $('#w').val('');
    $('#h').val('');
};
function updatePreview(c)
{
    //$('#divPreview div').show();
    if (parseInt(c.w) > 0)
    {
        //var rx = 111 / c.w;
        //var ry = 140 / c.h;
        var rx = widthCrop  / c.w;
        var ry = heightCrop / c.h;
        $('#preview').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
        });


    }
    $('#x1').val(c.x);
    $('#y1').val(c.y);
    $('#x2').val(c.x2);
    $('#y2').val(c.y2);
    $('#w').val(c.w);
    $('#h').val(c.h);
};
function setView(op){

    if (op=='capturar'){
        $('#divTakePhoto').fadeIn(500);
        $('#crop, #divUploadPhoto').hide();
    }
    if (op=='cargar'){
        //loadJcrop();
        $('#divTakePhoto').hide();
        $('#crop, #divUploadPhoto').fadeIn(500);
    }
    if (op=='crop'){
        $('#divTakePhoto, #divUploadPhoto').hide();
        $('#crop').fadeIn(500);
    }
}

function previewImg(obj) {
    var rFilter = /^(image\/bmp|image\/cis-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x-cmu-raster|image\/x-cmx|image\/x-icon|image\/x-portable-anymap|image\/x-portable-bitmap|image\/x-portable-graymap|image\/x-portable-pixmap|image\/x-rgb|image\/x-xbitmap|image\/x-xpixmap|image\/x-xwindowdump)$/i;
    var str;
    $('#divmensaje').html('');
    $('#ButtonUploadPhoto').show();
    if (!rFilter.test(obj.files[0].type)){
        str = '<div class="ui-widget">';
        str += '    <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
        str += '        <p>';
        str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
        str += '                El archivo debe ser una imagen';
        str += '        </p>';
        str += '    </div>';
        str += '</div>';
        $('#divmensaje').html(str).fadeIn(500);
        return false;
    }

    if (obj.files && obj.files[0]) {

        var reader = new FileReader();
        reader.onload = function (e) {
            jcrop_api = boundx = boundy = false;
            var newSrc = e.target.result;

            setImg(); //

            $('#thumbnail,#preview').attr('src',newSrc);

            setTimeout('chkSize();', 500);

            //setView('crop');
            //loadJcrop();

        };
        reader.readAsDataURL(obj.files[0]);
    }
    return true;
}
/**
* Delay for a number of milliseconds
*/
function chkSize()
{
    //alert($('#thumbnail').width()+'---'+$('#thumbnail').height());
    if (parseInt($('#thumbnail').width()) > maxWidth || parseInt($('#thumbnail').height())>maxHeight){
        str = '<div class="ui-widget">';
        str += '    <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
        str += '        <p>';
        str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
        str += '                La imagen es muy grande. Debe tener un m&aacute;ximo de 320 x 240 pixeles';
        str += '        </p>';
        str += '    </div>';
        str += '</div>';
        $('#divmensaje').html(str).fadeIn(500);
        $('#thumbnail').attr('src','../images/Picture-file-256.png');
        $('#preview').attr({
            src: '../images/Picture-file-256.png',
            width: widthCrop,
            height: heightCrop

        });
        $('#ButtonUploadPhoto').hide();
    }
}
function setImg(){
    imgToLoad = '<img src="" id="thumbnail" alt="Create Thumbnail" />'
    imgCrop   = '<div style="width:'+widthCrop+'px;height:'+heightCrop+'px;overflow:hidden;"><img src="" id="preview" alt="Preview" /></div>';
    $('#uploaded_image').html(imgToLoad);
    $('#divPreview').html(imgCrop);
}

function loadImagesHistory(){
    $.ajax({
        beforeSend: function(){
            
            $('#divmensaje').html('').hide();
            $('#selectImage').html(loadingMsg);
        },
        type: 'POST',
        dataType:'json',
        url: 'fotoloader.Op.php',
        data: 'cedula='+$('#cedula').val()+'&accion=imagesHistory&tipo='+$('#tipo').val()+'&archivo_foto='+$('#archivo_foto').val()+'&widthCrop='+$('#widthCrop').val()+'&heightCrop='+$('#heightCrop').val(),
        success: function(data){            
            $('#selectImage').html(data.str).buttonset();            
            return true;
        }
    });
}
function isValid(op){
    if (op == 'deletePhoto'){
        if (!confirm('\xbf Esta seguro que desea eliminar la imagen ?')){
            return false;
        }
    }
    if (op == 'savePhoto'){
        if (!confirm('\xbf Esta seguro que desea guardar la imagen ?')){
            return false;
        }
    }
    $('#accion').val(op);
    $('#mainForm').submit();
}
function savePhoto($file){
    $('#archivo_foto', parent.document).val($file);
    parent.setPhoto();
    
    
}

