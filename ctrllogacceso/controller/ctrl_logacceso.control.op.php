<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLLOG"); //Categoria del modulo
require_once "ctrl_logacceso.control.php";// Class CONTROL ControlCtrlLogacceso()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlLogacceso extends ControlCtrlLogacceso{
    /**
     * @var string mensaje de exito o error
     */
    public $error = false;
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlLogacceso(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlLogacceso()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlLogacceso(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlLogacceso()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("CtrlLogacceso");
        
        $this->setCampos("dirnodofuente","Dirnodofuente");
        $this->setCampos("nodolectora","Nodolectora");
        $this->setCampos("numeropuerta","Numeropuerta");
        $this->setCampos("revision","Revision");
        $this->setCampos("indiceusuario","Indiceusuario");
        $this->setCampos("valortransferido","Valortransferido");
        $this->setCampos("numerotarjeta","Numerotarjeta");
        $this->setCampos("numeroaltotarjeta","Numeroaltotarjeta");
        $this->setCampos("numerobajotarjeta","Numerobajotarjeta");
        $this->setCampos("codigofuncion","Codigofuncion");
        $this->setCampos("subcodigo","Subcodigo");
        $this->setCampos("subfuncion","Subfuncion");
        $this->setCampos("codigoext","Codigoext");
        $this->setCampos("nivelusuario","Nivelusuario");
        $this->setCampos("anio","Anio");
        $this->setCampos("dia","Dia");
        $this->setCampos("mes","Mes");
        $this->setCampos("diasemana","Diasemana");
        $this->setCampos("hora","Hora");
        $this->setCampos("minuto","Minuto");
        $this->setCampos("segundo","Segundo");
        $this->setCampos("log","Log");
        $this->setCampos("fcreado","Fcreado");
        $this->setCampos("idsincronizar","Idsincronizar");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        if ($_REQUEST["fecha_desde"] == "" && $_REQUEST["fecha_hasta"] != "" || $_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] == ""){
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_desde","tipoDato"=>"");
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_hasta","tipoDato"=>"");
        }
        

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

    
    public function loadLog(){
        if (!$this->sonValidosDatos()){
            $this->mensaje  = "<div class=\"ui-widget\">\n";
            $this->mensaje .= "    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">\n";
            $this->mensaje .= "        <p>\n";
            $this->mensaje .= "            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>\n";
            $this->mensaje .= "                ".$this->mensaje;
            $this->mensaje .= "        </p>\n";
            $this->mensaje .= "    </div>\n";
            $this->mensaje .= "</div>\n";
            $this->error = true;
            return false;
        }

        // ---- Argumento de la consulta segun parametros seleccionados en la vista ---- //
        $arg = array();

        //-------------------------------------------------------------------------------//
        if ($_REQUEST["numeroUsuario"] != "" ){
            $arg["campos"] .= " and cu.numero = '".$_REQUEST["numeroUsuario"]."'\n";
        }
        //-------------------------------------------------------------------------------//
        if ($_REQUEST["numeroPuerta"] != "" ){
            $arg["campos"] .= " and cp.numero = '".$_REQUEST["numeroPuerta"]."'\n";
        }
        //-------------------------------------------------------------------------------//
        if ($_REQUEST["codigo_id"] != "" ){
            $arg["campos"] .= " and cu.codigo_id = '".sprintf('%010d',$_REQUEST["codigo_id"])."'\n";
        }
        //-------------------------------------------------------------------------------//
        if ($_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] != ""){
            $arg["campos"] .= " and cast(concat(lg.anio,'-',lg.mes,'-',lg.dia,' ',lg.hora,':',lg.minuto,':',lg.segundo) as datetime) between '".Controller::formatoFecha($_REQUEST["fecha_desde"])."' and '".Controller::formatoFecha($_REQUEST["fecha_hasta"])."'\n";
        }
        //-------------------------------------------------------------------------------//
        
        $obj = new CtrlLogaccesos();
        $data = $obj->getLogs($arg);

        return $data;
    }

}
?>