<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../model/ctrl_logacceso.model.php"; // Class MODEL CtrlLogacceso()
require_once "ctrl_logacceso.controller.php";// Class CONTROL Controller()

/**
 * Description
 * @author David Concepcion
 */
class ControlCtrlLogacceso extends Controller{
    /**
     * @var string mensaje de exito o error
     */
    var $mensaje = null;
    /**
     * @var string accion agregar, modificar o eliminar dato
     */
    var $accion  = null;
    /**
     * @var array nombre de campos campos
     */
    var $campos  = null;
    /**
     * @var string nombre de la entidad 
     */
    var $entidad  = null;

    /**
     * Establece la acción
     * @param string $accion Acción
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }

    /**
     * @return string Devuelve la accion establecida
     */
    public function getAccion(){
         return $this->accion;
    }

    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }

    /**
     * Establece la Entidad
     * @param string $entidad Entidad
     */
    public function setEntidad($entidad){
         $this->entidad = $entidad;
    }

    /**
     * @return string Devuelve la Entidad establecida
     */
    public function getEntidad(){
         return $this->entidad;
    }
    
    /**
     * Agregar o modificar un CtrlLogacceso
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setCtrlLogacceso(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;

        $obj = new CtrlLogacceso();

        //--------------------- DATOS --------------------------//
        $obj->setId($_REQUEST["id"]);
        
        $obj->setDirnodofuente($_REQUEST["dirnodofuente"]);
        $obj->setNodolectora($_REQUEST["nodolectora"]);
        $obj->setNumeropuerta($_REQUEST["numeropuerta"]);
        $obj->setRevision($_REQUEST["revision"]);
        $obj->setIndiceusuario($_REQUEST["indiceusuario"]);
        $obj->setValortransferido($_REQUEST["valortransferido"]);
        $obj->setNumerotarjeta($_REQUEST["numerotarjeta"]);
        $obj->setNumeroaltotarjeta($_REQUEST["numeroaltotarjeta"]);
        $obj->setNumerobajotarjeta($_REQUEST["numerobajotarjeta"]);
        $obj->setCodigofuncion($_REQUEST["codigofuncion"]);
        $obj->setSubcodigo($_REQUEST["subcodigo"]);
        $obj->setSubfuncion($_REQUEST["subfuncion"]);
        $obj->setCodigoext($_REQUEST["codigoext"]);
        $obj->setNivelusuario($_REQUEST["nivelusuario"]);
        $obj->setAnio($_REQUEST["anio"]);
        $obj->setDia($_REQUEST["dia"]);
        $obj->setMes($_REQUEST["mes"]);
        $obj->setDiasemana($_REQUEST["diasemana"]);
        $obj->setHora($_REQUEST["hora"]);
        $obj->setMinuto($_REQUEST["minuto"]);
        $obj->setSegundo($_REQUEST["segundo"]);
        $obj->setLog($_REQUEST["log"]);
        $obj->setFcreado($_REQUEST["fcreado"]);
        $obj->setIdsincronizar($_REQUEST["idsincronizar"]);
        

        if ($this->getAccion()=="agregar"){
            $exito = $obj->insertarRegistro();
        }

        if ($this->getAccion()=="modificar"){
            $exito = $obj->modificarRegistro($obj->getId($_REQUEST["id"]));
        }

        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            if ($this->getAccion()=="agregar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser creado: ".$obj->getMensaje();
            }
            if ($this->getAccion()=="modificar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser modificado: ".$obj->getMensaje();
            }
            return false;
        }
        // ------------------------------- MENSAJE EXITO --------------------------------//
        if ($this->getAccion()=="agregar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; creado exitosamente...";
        }
        if ($this->getAccion()=="modificar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; modificado exitosamente...";
        }


        return true;
    }

    /**
     * === Eliminar CtrlLogacceso ===
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarCtrlLogacceso(){
        // -------------------- ELIMINAR LECTORA ----------------------//
        $obj = new CtrlLogacceso();
        //--------------------- DATOS --------------------------//
        $obj->setId($_REQUEST["id"]);

        $exito = $obj->eliminarRegistro($obj->getId($_REQUEST["id"]));
        if(!$exito){
            $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser eliminado: ".$obj->getMensaje();
            return false;
        }

        $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; eliminado exitosamente...";
        return true;
    }

    
    
}
?>
