<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlLogacceso {
    
    private $id = null;
    private $dirnodofuente = null;
    private $nodolectora = null;
    private $numeropuerta = null;
    private $revision = null;
    private $indiceusuario = null;
    private $valortransferido = null;
    private $numerotarjeta = null;
    private $numeroaltotarjeta = null;
    private $numerobajotarjeta = null;
    private $codigofuncion = null;
    private $subcodigo = null;
    private $subfuncion = null;
    private $codigoext = null;
    private $nivelusuario = null;
    private $anio = null;
    private $dia = null;
    private $mes = null;
    private $diasemana = null;
    private $hora = null;
    private $minuto = null;
    private $segundo = null;
    private $log = null;
    private $fcreado = null;
    private $idsincronizar = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    public function setDirnodofuente($dirnodofuente){
        $this->dirnodofuente = $dirnodofuente;
    }
    public function getDirnodofuente(){
        return $this->dirnodofuente;
    }
    public function setNodolectora($nodolectora){
        $this->nodolectora = $nodolectora;
    }
    public function getNodolectora(){
        return $this->nodolectora;
    }
    public function setNumeropuerta($numeropuerta){
        $this->numeropuerta = $numeropuerta;
    }
    public function getNumeropuerta(){
        return $this->numeropuerta;
    }
    public function setRevision($revision){
        $this->revision = $revision;
    }
    public function getRevision(){
        return $this->revision;
    }
    public function setIndiceusuario($indiceusuario){
        $this->indiceusuario = $indiceusuario;
    }
    public function getIndiceusuario(){
        return $this->indiceusuario;
    }
    public function setValortransferido($valortransferido){
        $this->valortransferido = $valortransferido;
    }
    public function getValortransferido(){
        return $this->valortransferido;
    }
    public function setNumerotarjeta($numerotarjeta){
        $this->numerotarjeta = $numerotarjeta;
    }
    public function getNumerotarjeta(){
        return $this->numerotarjeta;
    }
    public function setNumeroaltotarjeta($numeroaltotarjeta){
        $this->numeroaltotarjeta = $numeroaltotarjeta;
    }
    public function getNumeroaltotarjeta(){
        return $this->numeroaltotarjeta;
    }
    public function setNumerobajotarjeta($numerobajotarjeta){
        $this->numerobajotarjeta = $numerobajotarjeta;
    }
    public function getNumerobajotarjeta(){
        return $this->numerobajotarjeta;
    }
    public function setCodigofuncion($codigofuncion){
        $this->codigofuncion = $codigofuncion;
    }
    public function getCodigofuncion(){
        return $this->codigofuncion;
    }
    public function setSubcodigo($subcodigo){
        $this->subcodigo = $subcodigo;
    }
    public function getSubcodigo(){
        return $this->subcodigo;
    }
    public function setSubfuncion($subfuncion){
        $this->subfuncion = $subfuncion;
    }
    public function getSubfuncion(){
        return $this->subfuncion;
    }
    public function setCodigoext($codigoext){
        $this->codigoext = $codigoext;
    }
    public function getCodigoext(){
        return $this->codigoext;
    }
    public function setNivelusuario($nivelusuario){
        $this->nivelusuario = $nivelusuario;
    }
    public function getNivelusuario(){
        return $this->nivelusuario;
    }
    public function setAnio($anio){
        $this->anio = $anio;
    }
    public function getAnio(){
        return $this->anio;
    }
    public function setDia($dia){
        $this->dia = $dia;
    }
    public function getDia(){
        return $this->dia;
    }
    public function setMes($mes){
        $this->mes = $mes;
    }
    public function getMes(){
        return $this->mes;
    }
    public function setDiasemana($diasemana){
        $this->diasemana = $diasemana;
    }
    public function getDiasemana(){
        return $this->diasemana;
    }
    public function setHora($hora){
        $this->hora = $hora;
    }
    public function getHora(){
        return $this->hora;
    }
    public function setMinuto($minuto){
        $this->minuto = $minuto;
    }
    public function getMinuto(){
        return $this->minuto;
    }
    public function setSegundo($segundo){
        $this->segundo = $segundo;
    }
    public function getSegundo(){
        return $this->segundo;
    }
    public function setLog($log){
        $this->log = $log;
    }
    public function getLog(){
        return $this->log;
    }
    public function setFcreado($fcreado){
        $this->fcreado = $fcreado;
    }
    public function getFcreado(){
        return $this->fcreado;
    }
    public function setIdsincronizar($idsincronizar){
        $this->idsincronizar = $idsincronizar;
    }
    public function getIdsincronizar(){
        return $this->idsincronizar;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->id !== null) && (trim($this->id)!=='') ){
            $campos .= "id,";
            $valores .= "'".$this->id."',";
        }
        if(($this->dirnodofuente !== null) && (trim($this->dirnodofuente)!=='') ){
            $campos .= "dirnodofuente,";
            $valores .= "'".$this->dirnodofuente."',";
        }
        if(($this->nodolectora !== null) && (trim($this->nodolectora)!=='') ){
            $campos .= "nodolectora,";
            $valores .= "'".$this->nodolectora."',";
        }
        if(($this->numeropuerta !== null) && (trim($this->numeropuerta)!=='') ){
            $campos .= "numeropuerta,";
            $valores .= "'".$this->numeropuerta."',";
        }
        if(($this->revision !== null) && (trim($this->revision)!=='') ){
            $campos .= "revision,";
            $valores .= "'".$this->revision."',";
        }
        if(($this->indiceusuario !== null) && (trim($this->indiceusuario)!=='') ){
            $campos .= "indiceusuario,";
            $valores .= "'".$this->indiceusuario."',";
        }
        if(($this->valortransferido !== null) && (trim($this->valortransferido)!=='') ){
            $campos .= "valortransferido,";
            $valores .= "'".$this->valortransferido."',";
        }
        if(($this->numerotarjeta !== null) && (trim($this->numerotarjeta)!=='') ){
            $campos .= "numerotarjeta,";
            $valores .= "'".$this->numerotarjeta."',";
        }
        if(($this->numeroaltotarjeta !== null) && (trim($this->numeroaltotarjeta)!=='') ){
            $campos .= "numeroaltotarjeta,";
            $valores .= "'".$this->numeroaltotarjeta."',";
        }
        if(($this->numerobajotarjeta !== null) && (trim($this->numerobajotarjeta)!=='') ){
            $campos .= "numerobajotarjeta,";
            $valores .= "'".$this->numerobajotarjeta."',";
        }
        if(($this->codigofuncion !== null) && (trim($this->codigofuncion)!=='') ){
            $campos .= "codigofuncion,";
            $valores .= "'".$this->codigofuncion."',";
        }
        if(($this->subcodigo !== null) && (trim($this->subcodigo)!=='') ){
            $campos .= "subcodigo,";
            $valores .= "'".$this->subcodigo."',";
        }
        if(($this->subfuncion !== null) && (trim($this->subfuncion)!=='') ){
            $campos .= "subfuncion,";
            $valores .= "'".$this->subfuncion."',";
        }
        if(($this->codigoext !== null) && (trim($this->codigoext)!=='') ){
            $campos .= "codigoext,";
            $valores .= "'".$this->codigoext."',";
        }
        if(($this->nivelusuario !== null) && (trim($this->nivelusuario)!=='') ){
            $campos .= "nivelusuario,";
            $valores .= "'".$this->nivelusuario."',";
        }
        if(($this->anio !== null) && (trim($this->anio)!=='') ){
            $campos .= "anio,";
            $valores .= "'".$this->anio."',";
        }
        if(($this->dia !== null) && (trim($this->dia)!=='') ){
            $campos .= "dia,";
            $valores .= "'".$this->dia."',";
        }
        if(($this->mes !== null) && (trim($this->mes)!=='') ){
            $campos .= "mes,";
            $valores .= "'".$this->mes."',";
        }
        if(($this->diasemana !== null) && (trim($this->diasemana)!=='') ){
            $campos .= "diasemana,";
            $valores .= "'".$this->diasemana."',";
        }
        if(($this->hora !== null) && (trim($this->hora)!=='') ){
            $campos .= "hora,";
            $valores .= "'".$this->hora."',";
        }
        if(($this->minuto !== null) && (trim($this->minuto)!=='') ){
            $campos .= "minuto,";
            $valores .= "'".$this->minuto."',";
        }
        if(($this->segundo !== null) && (trim($this->segundo)!=='') ){
            $campos .= "segundo,";
            $valores .= "'".$this->segundo."',";
        }
        if(($this->log !== null) && (trim($this->log)!=='') ){
            $campos .= "log,";
            $valores .= "'".$this->log."',";
        }
        if(($this->fcreado !== null) && (trim($this->fcreado)!=='') ){
            $campos .= "fcreado,";
            $valores .= "'".$this->fcreado."',";
        }
        if(($this->idsincronizar !== null) && (trim($this->idsincronizar)!=='') ){
            $campos .= "idsincronizar,";
            $valores .= "'".$this->idsincronizar."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_logacceso $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_logacceso SET ";
        
        if(($this->dirnodofuente !== null) && (trim($this->dirnodofuente)!=='') ){
            $sql .= "dirnodofuente = '".$this->dirnodofuente."',";
        }
        if(($this->nodolectora !== null) && (trim($this->nodolectora)!=='') ){
            $sql .= "nodolectora = '".$this->nodolectora."',";
        }
        if(($this->numeropuerta !== null) && (trim($this->numeropuerta)!=='') ){
            $sql .= "numeropuerta = '".$this->numeropuerta."',";
        }
        if(($this->revision !== null) && (trim($this->revision)!=='') ){
            $sql .= "revision = '".$this->revision."',";
        }
        if(($this->indiceusuario !== null) && (trim($this->indiceusuario)!=='') ){
            $sql .= "indiceusuario = '".$this->indiceusuario."',";
        }
        if(($this->valortransferido !== null) && (trim($this->valortransferido)!=='') ){
            $sql .= "valortransferido = '".$this->valortransferido."',";
        }
        if(($this->numerotarjeta !== null) && (trim($this->numerotarjeta)!=='') ){
            $sql .= "numerotarjeta = '".$this->numerotarjeta."',";
        }
        if(($this->numeroaltotarjeta !== null) && (trim($this->numeroaltotarjeta)!=='') ){
            $sql .= "numeroaltotarjeta = '".$this->numeroaltotarjeta."',";
        }
        if(($this->numerobajotarjeta !== null) && (trim($this->numerobajotarjeta)!=='') ){
            $sql .= "numerobajotarjeta = '".$this->numerobajotarjeta."',";
        }
        if(($this->codigofuncion !== null) && (trim($this->codigofuncion)!=='') ){
            $sql .= "codigofuncion = '".$this->codigofuncion."',";
        }
        if(($this->subcodigo !== null) && (trim($this->subcodigo)!=='') ){
            $sql .= "subcodigo = '".$this->subcodigo."',";
        }
        if(($this->subfuncion !== null) && (trim($this->subfuncion)!=='') ){
            $sql .= "subfuncion = '".$this->subfuncion."',";
        }
        if(($this->codigoext !== null) && (trim($this->codigoext)!=='') ){
            $sql .= "codigoext = '".$this->codigoext."',";
        }
        if(($this->nivelusuario !== null) && (trim($this->nivelusuario)!=='') ){
            $sql .= "nivelusuario = '".$this->nivelusuario."',";
        }
        if(($this->anio !== null) && (trim($this->anio)!=='') ){
            $sql .= "anio = '".$this->anio."',";
        }
        if(($this->dia !== null) && (trim($this->dia)!=='') ){
            $sql .= "dia = '".$this->dia."',";
        }
        if(($this->mes !== null) && (trim($this->mes)!=='') ){
            $sql .= "mes = '".$this->mes."',";
        }
        if(($this->diasemana !== null) && (trim($this->diasemana)!=='') ){
            $sql .= "diasemana = '".$this->diasemana."',";
        }
        if(($this->hora !== null) && (trim($this->hora)!=='') ){
            $sql .= "hora = '".$this->hora."',";
        }
        else{
            $sql .= "hora = NULL,";
        }
        if(($this->minuto !== null) && (trim($this->minuto)!=='') ){
            $sql .= "minuto = '".$this->minuto."',";
        }
        else{
            $sql .= "minuto = NULL,";
        }
        if(($this->segundo !== null) && (trim($this->segundo)!=='') ){
            $sql .= "segundo = '".$this->segundo."',";
        }
        else{
            $sql .= "segundo = NULL,";
        }
        if(($this->log !== null) && (trim($this->log)!=='') ){
            $sql .= "log = '".$this->log."',";
        }
        if(($this->fcreado !== null) && (trim($this->fcreado)!=='') ){
            $sql .= "fcreado = '".$this->fcreado."',";
        }
        if(($this->idsincronizar !== null) && (trim($this->idsincronizar)!=='') ){
            $sql .= "idsincronizar = '".$this->idsincronizar."',";
        }
        else{
            $sql .= "idsincronizar = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_logacceso  WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlLogaccesos
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlLogaccesos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idBuscador"]){
            $in = null;
            foreach ($_REQUEST["idBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where id in (".$in.")";
        }
        
        $sql = "select id,dirnodofuente,nodolectora,numeropuerta,revision,indiceusuario,valortransferido,numerotarjeta,numeroaltotarjeta,numerobajotarjeta,codigofuncion,subcodigo,subfuncion,codigoext,nivelusuario,anio,dia,mes,diasemana,hora,minuto,segundo,log,fcreado,idsincronizar from ctrl_logacceso ".$arg." order by dirnodofuente,nodolectora";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlLogacceso
     * @param int $id Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlLogacceso($id){
        $sql = "SELECT id,dirnodofuente,nodolectora,numeropuerta,revision,indiceusuario,valortransferido,numerotarjeta,numeroaltotarjeta,numerobajotarjeta,codigofuncion,subcodigo,subfuncion,codigoext,nivelusuario,anio,dia,mes,diasemana,hora,minuto,segundo,log,fcreado,idsincronizar FROM ctrl_logacceso WHERE id = '".$id."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>