<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'ctrl_logacceso.tabla.php';

/**
 * Clase CtrlLogaccesos{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlLogaccesos extends CtrlLogacceso{

    /**
     * Consulta de CtrlLogaccesos
     * @return object Devuelve un registro como objeto
     */
    function  getLogs($arg){
        $sql = "select /*start*/ 
                       lg.id as logID,
                       lg.dirnodofuente,
                       lg.nodolectora,
                       lg.numeropuerta,
                       lg.revision,
                       lg.indiceusuario,
                       lg.valortransferido,
                       lg.numerotarjeta,
                       lg.numeroaltotarjeta,
                       lg.numerobajotarjeta,
                       lg.codigofuncion,
                       lg.subcodigo,
                       lg.subfuncion,
                       lg.codigoext,
                       lg.nivelusuario,
                       lg.anio,
                       lg.dia,
                       lg.mes,
                       lg.diasemana,
                       lg.hora,
                       lg.minuto,
                       lg.segundo,
                       lg.log,
                       lg.fcreado,
                       lg.idsincronizar,
                       cast(concat(lg.anio,'-',lg.mes,'-',lg.dia,' ',lg.hora,':',lg.minuto,':',lg.segundo) as :cast_datetime)as fechaEvento,
                       cu.idusuario,
                       cu.tipo_tarjeta,                       
                       concat(cu.nombres,' ',COALESCE(cu.apellidos))as usuarioTrj,
                       cp.puerta,
                       (select 1 from car_visitante cv where cv.numero = cu.numero_visitante) as hasCarnetVisitante,
                       ce.idcarnet_empleado as hasCarnetEmpleado,
                       ce.archivo_foto
                       /*end*/
                from ctrl_logacceso lg
                        left join ctrl_usuario cu on (lg.indiceusuario = cu.numero)
                        left join car_empleado ce on (ce.cedula = cu.cedula)
                        left join ctrl_puerta cp on (lg.numeropuerta = cast(cp.numero as :cast_int))
		where lg.idsincronizar is null
                    ".$arg["campos"]."
                    order by lg.id desc";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
}
?>
