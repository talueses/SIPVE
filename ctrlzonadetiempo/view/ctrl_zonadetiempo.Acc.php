<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo date("h:iA", mktime(0,715)); // DB to view ej:11:55
//echo (11*60)+55; // View to BD ej:11:55

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_zonadetiempo.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new CtrlZonadetiempos();
    $data = $obj->getCtrlZonadetiempo($_REQUEST["indice"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/jquery.timeentry.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            #listado{
                width: 250px;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x;  font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr td:first-child {
                border: 1px solid #d3d3d3;
                background: #F0F0F0;
                font-weight: normal;
                color: #555555;
                -moz-border-radius-bottomleft: 12px;
                -moz-border-radius-topleft: 12px;
                -webkit-border-bottom-left-radius:12px;
                -webkit-border-top-left-radius:12px;
                border-bottom-left-radius: 12px;
                border-top-left-radius: 12px;
                
				
            }
            #listado input[type="text"]{                
                width: 65px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
            });
            // --- INSTACIA PLUGIN JQUERY TIME ENTRY --- //
            $(function () {
                $('.timeRange').timeEntry({
                    beforeShow: customRange,
                    spinnerImage: ''
                });
            });
            function customRange(input) {
                return {
                    minTime: ((input.id == ($('#'+input.id).attr('rel')+'hasta')) ? $('#'+$('#'+input.id).attr('rel')+'desde').timeEntry('getTime') : null),
                    maxTime: ((input.id == ($('#'+input.id).attr('rel')+'desde'))? $('#'+$('#'+input.id).attr('rel')+'hasta').timeEntry('getTime') : null)
                };
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Horarios de Trabajo</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="ctrl_zonadetiempo.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr title="Descripcion">
                                <td align="right">Descripci&oacute;n:</td>
                                <td>
                                    <input type="text" name="descripcion" id="descripcion" maxlength="200" value="<?php echo $data->descripcion;?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Disponible en Feriado">
                                <td align="right">Disponible en Feriado:</td>
                                <td>
                                    <input type="checkbox" name="disponibleenferiado" id="disponibleenferiado" value="1" <?php echo $disabled;?> <?php echo $data->disponibleenferiado=="1"?"checked":"";?>>
                                </td>
                            </tr>
                            <tr title="Nivel de Prioridad">
                                <td align="right">Nivel:</td>
                                <td>
                                    <select name="nivel" id="nivel" <?php echo $disabled;?>>
                                        
                                        <?php
                                        for ($i=0;$i<=63;$i++){
                                            
                                            echo "<option value=\"".$i."\" ".ControlCtrlZonadetiempo::busca_valor($data->nivel, $i).">".$i."</option>\n";
                                            
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Horario Enlazado">
                                <td align="right">Horario Enlazado:</td>
                                <td>

                                    <select name="indicelink" id="indicelink" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php
                                        if ($data->indice){
                                            $arg = "where indice <> '".$data->indicelink?$data->indicelink:"0"."'";//and idgrupo_padre <> '".$data->idgrupo_padre."'
                                        }

                                        echo ControlCtrlZonadetiempo::make_combo("ctrl_zonadetiempo", $arg." order by indice", "indice", "descripcion", $data->indicelink);
                                        ?>
                                    </select>
                                </td>
                            </tr>

                        </table>
                        <table id="listado" align="center" border="0" >
                            <tr>
                                <th colspan="2" align="center">Horarios</th>
                            </tr>
                            <tr title="Lunes">
                                <td align="right" class="tagHorario">Lunes:</td>
                                <td>
                                    <input type="text" name="lunesdesde" id="lunesdesde" rel="lunes" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->lunesdesde);?>" <?php echo $disabled;?>>
                                    &harr;
                                    <input type="text" name="luneshasta" id="luneshasta" rel="lunes" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->luneshasta);?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Martes">
                                <td align="right">Martes:</td>
                                <td>
                                    <input type="text" name="martesdesde" id="martesdesde" rel="martes" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->martesdesde);?>" <?php echo $disabled;?>>
                                    &harr;
                                    <input type="text" name="marteshasta" id="marteshasta" rel="martes" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->marteshasta);?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Miercoles">
                                <td align="right">Miercoles:</td>
                                <td>
                                    <input type="text" name="miercolesdesde" id="miercolesdesde" rel="miercoles" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->miercolesdesde);?>" <?php echo $disabled;?>>
                                    &harr;
                                    <input type="text" name="miercoleshasta" id="miercoleshasta" rel="miercoles" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->miercoleshasta);?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Jueves">
                                <td align="right">Jueves:</td>
                                <td>
                                    <input type="text" name="juevesdesde" id="juevesdesde" rel="jueves" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->juevesdesde);?>" <?php echo $disabled;?>>
                                    &harr;
                                    <input type="text" name="jueveshasta" id="jueveshasta" rel="jueves" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->jueveshasta);?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Viernes">
                                <td align="right">Viernes:</td>
                                <td>
                                    <input type="text" name="viernesdesde" id="viernesdesde" rel="viernes" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->viernesdesde);?>" <?php echo $disabled;?>>
                                    &harr;
                                    <input type="text" name="vierneshasta" id="vierneshasta" rel="viernes" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->vierneshasta);?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Sabado">
                                <td align="right">Sabado:</td>
                                <td>
                                    <input type="text" name="sabadodesde" id="sabadodesde" rel="sabado" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->sabadodesde);?>" <?php echo $disabled;?>>
                                    &harr;
                                    <input type="text" name="sabadohasta" id="sabadohasta" rel="sabado" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->sabadohasta);?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Domingo">
                                <td align="right">Domingo:</td>
                                <td>
                                    <input type="text" name="domingodesde" id="domingodesde" rel="domingo" class="timeRange" size="7" maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->domingodesde);?>" <?php echo $disabled;?>>
                                    &harr;
                                    <input type="text" name="domingohasta" id="domingohasta" rel="domingo" class="timeRange" size="7"maxlength="4" value="<?php echo ControlOpCtrlZonadetiempo::formatHorarioToView($data->domingohasta);?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="indice" id="indice" value="<?php echo $data->indice ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Horarios de Trabajo\" />";
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>