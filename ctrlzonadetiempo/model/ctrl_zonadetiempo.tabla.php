<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlZonadetiempo {
    
    private $indice = null;
    private $descripcion = null;
    private $disponibleenferiado = null;
    private $nivel = null;
    private $indicelink = null;
    private $domingodesde = null;
    private $domingohasta = null;
    private $lunesdesde = null;
    private $luneshasta = null;
    private $martesdesde = null;
    private $marteshasta = null;
    private $miercolesdesde = null;
    private $miercoleshasta = null;
    private $juevesdesde = null;
    private $jueveshasta = null;
    private $viernesdesde = null;
    private $vierneshasta = null;
    private $sabadodesde = null;
    private $sabadohasta = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIndice($indice){
        $this->indice = $indice;
    }
    public function getIndice(){
        return $this->indice;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setDisponibleenferiado($disponibleenferiado){
        $this->disponibleenferiado = $disponibleenferiado;
    }
    public function getDisponibleenferiado(){
        return $this->disponibleenferiado;
    }
    public function setNivel($nivel){
        $this->nivel = $nivel;
    }
    public function getNivel(){
        return $this->nivel;
    }
    public function setIndicelink($indicelink){
        $this->indicelink = $indicelink;
    }
    public function getIndicelink(){
        return $this->indicelink;
    }
    public function setDomingodesde($domingodesde){
        $this->domingodesde = $domingodesde;
    }
    public function getDomingodesde(){
        return $this->domingodesde;
    }
    public function setDomingohasta($domingohasta){
        $this->domingohasta = $domingohasta;
    }
    public function getDomingohasta(){
        return $this->domingohasta;
    }
    public function setLunesdesde($lunesdesde){
        $this->lunesdesde = $lunesdesde;
    }
    public function getLunesdesde(){
        return $this->lunesdesde;
    }
    public function setLuneshasta($luneshasta){
        $this->luneshasta = $luneshasta;
    }
    public function getLuneshasta(){
        return $this->luneshasta;
    }
    public function setMartesdesde($martesdesde){
        $this->martesdesde = $martesdesde;
    }
    public function getMartesdesde(){
        return $this->martesdesde;
    }
    public function setMarteshasta($marteshasta){
        $this->marteshasta = $marteshasta;
    }
    public function getMarteshasta(){
        return $this->marteshasta;
    }
    public function setMiercolesdesde($miercolesdesde){
        $this->miercolesdesde = $miercolesdesde;
    }
    public function getMiercolesdesde(){
        return $this->miercolesdesde;
    }
    public function setMiercoleshasta($miercoleshasta){
        $this->miercoleshasta = $miercoleshasta;
    }
    public function getMiercoleshasta(){
        return $this->miercoleshasta;
    }
    public function setJuevesdesde($juevesdesde){
        $this->juevesdesde = $juevesdesde;
    }
    public function getJuevesdesde(){
        return $this->juevesdesde;
    }
    public function setJueveshasta($jueveshasta){
        $this->jueveshasta = $jueveshasta;
    }
    public function getJueveshasta(){
        return $this->jueveshasta;
    }
    public function setViernesdesde($viernesdesde){
        $this->viernesdesde = $viernesdesde;
    }
    public function getViernesdesde(){
        return $this->viernesdesde;
    }
    public function setVierneshasta($vierneshasta){
        $this->vierneshasta = $vierneshasta;
    }
    public function getVierneshasta(){
        return $this->vierneshasta;
    }
    public function setSabadodesde($sabadodesde){
        $this->sabadodesde = $sabadodesde;
    }
    public function getSabadodesde(){
        return $this->sabadodesde;
    }
    public function setSabadohasta($sabadohasta){
        $this->sabadohasta = $sabadohasta;
    }
    public function getSabadohasta(){
        return $this->sabadohasta;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->indice !== null) && (trim($this->indice)!=='') ){
            $campos .= "indice,";
            $valores .= "'".$this->indice."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $campos .= "descripcion,";
            $valores .= "'".$this->descripcion."',";
        }
        if(($this->disponibleenferiado !== null) && (trim($this->disponibleenferiado)!=='') ){
            $campos .= "disponibleenferiado,";
            $valores .= "'".$this->disponibleenferiado."',";
        }
        if(($this->nivel !== null) && (trim($this->nivel)!=='') ){
            $campos .= "nivel,";
            $valores .= "'".$this->nivel."',";
        }
        if(($this->indicelink !== null) && (trim($this->indicelink)!=='') ){
            $campos .= "indicelink,";
            $valores .= "'".$this->indicelink."',";
        }
        if(($this->domingodesde !== null) && (trim($this->domingodesde)!=='') ){
            $campos .= "domingodesde,";
            $valores .= "'".$this->domingodesde."',";
        }
        if(($this->domingohasta !== null) && (trim($this->domingohasta)!=='') ){
            $campos .= "domingohasta,";
            $valores .= "'".$this->domingohasta."',";
        }
        if(($this->lunesdesde !== null) && (trim($this->lunesdesde)!=='') ){
            $campos .= "lunesdesde,";
            $valores .= "'".$this->lunesdesde."',";
        }
        if(($this->luneshasta !== null) && (trim($this->luneshasta)!=='') ){
            $campos .= "luneshasta,";
            $valores .= "'".$this->luneshasta."',";
        }
        if(($this->martesdesde !== null) && (trim($this->martesdesde)!=='') ){
            $campos .= "martesdesde,";
            $valores .= "'".$this->martesdesde."',";
        }
        if(($this->marteshasta !== null) && (trim($this->marteshasta)!=='') ){
            $campos .= "marteshasta,";
            $valores .= "'".$this->marteshasta."',";
        }
        if(($this->miercolesdesde !== null) && (trim($this->miercolesdesde)!=='') ){
            $campos .= "miercolesdesde,";
            $valores .= "'".$this->miercolesdesde."',";
        }
        if(($this->miercoleshasta !== null) && (trim($this->miercoleshasta)!=='') ){
            $campos .= "miercoleshasta,";
            $valores .= "'".$this->miercoleshasta."',";
        }
        if(($this->juevesdesde !== null) && (trim($this->juevesdesde)!=='') ){
            $campos .= "juevesdesde,";
            $valores .= "'".$this->juevesdesde."',";
        }
        if(($this->jueveshasta !== null) && (trim($this->jueveshasta)!=='') ){
            $campos .= "jueveshasta,";
            $valores .= "'".$this->jueveshasta."',";
        }
        if(($this->viernesdesde !== null) && (trim($this->viernesdesde)!=='') ){
            $campos .= "viernesdesde,";
            $valores .= "'".$this->viernesdesde."',";
        }
        if(($this->vierneshasta !== null) && (trim($this->vierneshasta)!=='') ){
            $campos .= "vierneshasta,";
            $valores .= "'".$this->vierneshasta."',";
        }
        if(($this->sabadodesde !== null) && (trim($this->sabadodesde)!=='') ){
            $campos .= "sabadodesde,";
            $valores .= "'".$this->sabadodesde."',";
        }
        if(($this->sabadohasta !== null) && (trim($this->sabadohasta)!=='') ){
            $campos .= "sabadohasta,";
            $valores .= "'".$this->sabadohasta."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_zonadetiempo $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_zonadetiempo SET ";
        
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $sql .= "descripcion = '".$this->descripcion."',";
        }
        if(($this->disponibleenferiado !== null) && (trim($this->disponibleenferiado)!=='') ){
            $sql .= "disponibleenferiado = '".$this->disponibleenferiado."',";
        }
        if(($this->nivel !== null) && (trim($this->nivel)!=='') ){
            $sql .= "nivel = '".$this->nivel."',";
        }
        if(($this->indicelink !== null) && (trim($this->indicelink)!=='') ){
            $sql .= "indicelink = '".$this->indicelink."',";
        }
        else{
            $sql .= "indicelink = NULL,";
        }
        if(($this->domingodesde !== null) && (trim($this->domingodesde)!=='') ){
            $sql .= "domingodesde = '".$this->domingodesde."',";
        }
        if(($this->domingohasta !== null) && (trim($this->domingohasta)!=='') ){
            $sql .= "domingohasta = '".$this->domingohasta."',";
        }
        if(($this->lunesdesde !== null) && (trim($this->lunesdesde)!=='') ){
            $sql .= "lunesdesde = '".$this->lunesdesde."',";
        }
        if(($this->luneshasta !== null) && (trim($this->luneshasta)!=='') ){
            $sql .= "luneshasta = '".$this->luneshasta."',";
        }
        if(($this->martesdesde !== null) && (trim($this->martesdesde)!=='') ){
            $sql .= "martesdesde = '".$this->martesdesde."',";
        }
        if(($this->marteshasta !== null) && (trim($this->marteshasta)!=='') ){
            $sql .= "marteshasta = '".$this->marteshasta."',";
        }
        if(($this->miercolesdesde !== null) && (trim($this->miercolesdesde)!=='') ){
            $sql .= "miercolesdesde = '".$this->miercolesdesde."',";
        }
        if(($this->miercoleshasta !== null) && (trim($this->miercoleshasta)!=='') ){
            $sql .= "miercoleshasta = '".$this->miercoleshasta."',";
        }
        if(($this->juevesdesde !== null) && (trim($this->juevesdesde)!=='') ){
            $sql .= "juevesdesde = '".$this->juevesdesde."',";
        }
        if(($this->jueveshasta !== null) && (trim($this->jueveshasta)!=='') ){
            $sql .= "jueveshasta = '".$this->jueveshasta."',";
        }
        if(($this->viernesdesde !== null) && (trim($this->viernesdesde)!=='') ){
            $sql .= "viernesdesde = '".$this->viernesdesde."',";
        }
        if(($this->vierneshasta !== null) && (trim($this->vierneshasta)!=='') ){
            $sql .= "vierneshasta = '".$this->vierneshasta."',";
        }
        if(($this->sabadodesde !== null) && (trim($this->sabadodesde)!=='') ){
            $sql .= "sabadodesde = '".$this->sabadodesde."',";
        }
        if(($this->sabadohasta !== null) && (trim($this->sabadohasta)!=='') ){
            $sql .= "sabadohasta = '".$this->sabadohasta."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE indice = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_zonadetiempo  WHERE indice = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlZonadetiempos
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlZonadetiempos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["indiceBuscador"]){
            $in = null;
            foreach ($_REQUEST["indiceBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where indice in (".$in.")";
        }
        
        $sql = "select /*start*/ indice,descripcion,disponibleenferiado,nivel,indicelink,domingodesde,domingohasta,lunesdesde,luneshasta,martesdesde,marteshasta,miercolesdesde,miercoleshasta,juevesdesde,jueveshasta,viernesdesde,vierneshasta,sabadodesde,sabadohasta /*end*/ from ctrl_zonadetiempo ".$arg." order by descripcion,disponibleenferiado";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlZonadetiempo
     * @param int $indice Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlZonadetiempo($indice){
        $sql = "SELECT indice,descripcion,disponibleenferiado,nivel,indicelink,domingodesde,domingohasta,lunesdesde,luneshasta,martesdesde,marteshasta,miercolesdesde,miercoleshasta,juevesdesde,jueveshasta,viernesdesde,vierneshasta,sabadodesde,sabadohasta FROM ctrl_zonadetiempo WHERE indice = '".$indice."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>