<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
//require_once "../../ctrlsincronizar/controller/ctrl_sincronizar.control.op.php"; // Class CONTROL ControlOpCtrlSincronizar()
require_once "../controller/ctrl_zonadetiempo.control.php";// Class CONTROL ControlCtrlZonadetiempo()


/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlZonadetiempo extends ControlCtrlZonadetiempo{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlZonadetiempo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();

        if (!$_REQUEST["disponibleenferiado"]){
            $_REQUEST["disponibleenferiado"] = "0";            
        }

        //if(!$this->sonValidosDatos()) return false;
        $_REQUEST["domingodesde"] = self::formatHorarioToModel($_REQUEST["domingodesde"]);
        $_REQUEST["domingohasta"] = self::formatHorarioToModel($_REQUEST["domingohasta"]);
        $_REQUEST["lunesdesde"] = self::formatHorarioToModel($_REQUEST["lunesdesde"]);
        $_REQUEST["luneshasta"] = self::formatHorarioToModel($_REQUEST["luneshasta"]);
        $_REQUEST["martesdesde"] = self::formatHorarioToModel($_REQUEST["martesdesde"]);
        $_REQUEST["marteshasta"] = self::formatHorarioToModel($_REQUEST["marteshasta"]);
        $_REQUEST["miercolesdesde"] = self::formatHorarioToModel($_REQUEST["miercolesdesde"]);
        $_REQUEST["miercoleshasta"] = self::formatHorarioToModel($_REQUEST["miercoleshasta"]);
        $_REQUEST["juevesdesde"] = self::formatHorarioToModel($_REQUEST["juevesdesde"]);
        $_REQUEST["jueveshasta"] = self::formatHorarioToModel($_REQUEST["jueveshasta"]);
        $_REQUEST["viernesdesde"] = self::formatHorarioToModel($_REQUEST["viernesdesde"]);
        $_REQUEST["vierneshasta"] = self::formatHorarioToModel($_REQUEST["vierneshasta"]);
        $_REQUEST["sabadodesde"] = self::formatHorarioToModel($_REQUEST["sabadodesde"]);
        $_REQUEST["sabadohasta"] = self::formatHorarioToModel($_REQUEST["sabadohasta"]);

        

        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlZonadetiempo()) return false;

        //------------------ Sincronizar Horarios  -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar()){
            $this->mensaje = $sinc->mensaje;
            return false;
        }
        
        return true;
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlZonadetiempo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlZonadetiempo()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Horarios de Trabajo");
        
        $this->setCampos("descripcion","Descripci&oacute;n");
        $this->setCampos("disponibleenferiado","Disponible en Feriado");
        $this->setCampos("indicelink","Horario Enlazado");
        $this->setCampos("domingodesde","Domingo Desde");
        $this->setCampos("domingohasta","Domingo Hasta");
        $this->setCampos("lunesdesde","Lunes Desde");
        $this->setCampos("luneshasta","Lunes Hasta");
        $this->setCampos("martesdesde","Martes Desde");
        $this->setCampos("marteshasta","Martes Hasta");
        $this->setCampos("miercolesdesde","Mi&eacute;rcoles Desde");
        $this->setCampos("miercoleshasta","Mi&eacute;rcoles Hasta");
        $this->setCampos("juevesdesde","Jueves Desde");
        $this->setCampos("jueveshasta","Jueves Hasta");
        $this->setCampos("viernesdesde","Viernes Desde");
        $this->setCampos("vierneshasta","Viernes Hasta");
        $this->setCampos("sabadodesde","S&aacute;bado Desde");
        $this->setCampos("sabadohasta","S&aacute;bado Hasta");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"descripcion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"lunesdesde","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"luneshasta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"martesdesde","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"marteshasta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"miercolesdesde","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"miercoleshasta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"juevesdesde","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"jueveshasta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"viernesdesde","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"vierneshasta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"sabadodesde","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"sabadohasta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"domingodesde","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"domingohasta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;        
        
        return true;
    }

    /**
     * Formatea la hora en formato de minutos desde la hora 00:00 a normal en formato srting 480 => 08:00AM
     * @param int $hora hora a traducir
     * @return string Devuelve la hora en formato hh:00A
     */
    static  function formatHorarioToView($hora){
        if ($hora){
            $hora = date("h:iA", mktime(0,$hora));
        }
        return $hora;
    }
    /**
     * Da formato a una hora en 12hrs con meridiano a formato de minutos desde la hora 00:00. Ej. 2:35PM -> 875 minutos desde las 00:00
     * @param string $hora Hora en formato 12hrs con meridiano
     * @return string Devuelve la hora en formato de minutos desde la hora 00:00
     */
    static  function formatHorarioToModel($hora){
        if ($hora){
            $horas   = substr($hora,0,2);
            $minutos = substr($hora,3,2);

            $meridian = substr($hora,5,2);
            if ($meridian=="PM"){
                $horas += 12;
            }

            $hora = ($horas * 60) + $minutos;
        }
        return $hora;
    }
}
?>