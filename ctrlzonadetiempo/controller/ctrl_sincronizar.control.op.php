<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLHRS"); //Categoria del modulo
require_once "../../ctrlsincronizar/controller/ctrl_sincronizar.control.php";// Class CONTROL ControlCtrlSincronizar()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlSincronizar extends ControlCtrlSincronizar{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlSincronizar(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        //echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
        $this->setAccion("agregar");
        $_REQUEST["item"]         = "Timezone"; // Etiqueta de registro en el proceso de sincronizacion para zona de tiempo
        $_REQUEST["accion"]       = "Modificar Zonas de Tiempo";    // Etiqueta de crear zona de tiempo
        $_REQUEST["codigoaccion"] = "7";       // Codigo de crear zona de tiempo en la controladora
        $_REQUEST["prioridad"]    = "85";       // Prioridad del proceso de sincronizacion para zona de tiempo
        
        // Consulta de todas las controladoras a sincronizar los horarios
        $data = CtrlSincronizars::getControladoras();
        if (count($data) > 0){
            //echo "<div align='left'><pre>".print_r($data,true)."</pre></div>";
            foreach ($data as $key => $row){

                $_REQUEST["nodo"]   = $row->nodo;
                $_REQUEST["ipv4"]   = $row->ipv4;
                $_REQUEST["puerto"] = $row->puerto;
                $_REQUEST["param1"] = $_REQUEST["indice"];              // ID primario 
                $_REQUEST["param2"] = $_REQUEST["disponibleenferiado"]; // Disponible en Feriado
                $_REQUEST["param3"] = $_REQUEST["nivel"];               // Nivel de prioridad.

                $_REQUEST["param4"] = $_REQUEST["domingodesde"];
                $_REQUEST["param5"] = $_REQUEST["domingohasta"];
                $_REQUEST["param6"] = $_REQUEST["lunesdesde"];
                $_REQUEST["param7"] = $_REQUEST["luneshasta"];
                $_REQUEST["param8"] = $_REQUEST["martesdesde"];
                $_REQUEST["param9"] = $_REQUEST["marteshasta"];
                $_REQUEST["param10"] = $_REQUEST["miercolesdesde"];
                $_REQUEST["param11"] = $_REQUEST["miercoleshasta"];
                $_REQUEST["param12"] = $_REQUEST["juevesdesde"];
                $_REQUEST["param13"] = $_REQUEST["jueveshasta"];
                $_REQUEST["param14"] = $_REQUEST["viernesdesde"];
                $_REQUEST["param15"] = $_REQUEST["vierneshasta"];
                $_REQUEST["param16"] = $_REQUEST["sabadodesde"];
                $_REQUEST["param17"] = $_REQUEST["sabadohasta"];

                $_REQUEST["param18"] = $_REQUEST["indicelink"]; // Horario Enlazado

                //------------------ Metodo Set  -----------------//
                if(!$this->setCtrlSincronizar()) return false;

            }

            
        }
        return true;
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlSincronizar(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlSincronizar()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("CtrlSincronizar");
        
        $this->setCampos("item","Item");
        $this->setCampos("accion","Accion");
        $this->setCampos("codigoaccion","Codigoaccion");
        $this->setCampos("nodo","Nodo");
        $this->setCampos("ipv4","Ipv4");
        $this->setCampos("puerto","Puerto");
        $this->setCampos("prioridad","Prioridad");
        $this->setCampos("param1","Param1");
        $this->setCampos("param2","Param2");
        $this->setCampos("param3","Param3");
        $this->setCampos("param4","Param4");
        $this->setCampos("param5","Param5");
        $this->setCampos("param6","Param6");
        $this->setCampos("param7","Param7");
        $this->setCampos("param8","Param8");
        $this->setCampos("param9","Param9");
        $this->setCampos("param10","Param10");
        $this->setCampos("param11","Param11");
        $this->setCampos("param12","Param12");
        $this->setCampos("param13","Param13");
        $this->setCampos("param14","Param14");
        $this->setCampos("param15","Param15");
        $this->setCampos("param16","Param16");
        $this->setCampos("param17","Param17");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){

        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"item","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"accion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"codigoaccion","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nodo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"ipv4","tipoDato"=>"esValidaIP");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"puerto","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"prioridad","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param1","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param2","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param3","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param4","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param5","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param6","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param7","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param8","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param9","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param10","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param11","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param12","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param13","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param14","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param15","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param16","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param17","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

}
?>