<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Modificado por David Concepcion 08-09-2010 CENIT
 * Opcion de menu Camara
 *
 * Modificado por David Concepcion 21-09-2010 CENIT
 * Opcion de menu Monitoreo
 */
session_start(); // start up your PHP session! 
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$opt = isset($_REQUEST['op']) ? "op".trim($_REQUEST['op']) : 'op2';
?>			
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
    <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
    <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
    <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
    <head><title>Video Vigilancia</title>
        <style type="text/css">
            body{background:#dfdfdf;font-size:14px; font-family:arial,verdana,sans-serif;margin-left:50px;margin-right:50px;}
            
            .mattblacktabs{width: 100%;overflow: hidden;border-bottom: 1px solid #d02433; /*bottom horizontal line that runs beneath tabs*/}
            .mattblacktabs ul{margin: 0;padding: 0;padding-left: 10px; /*offset of tabs relative to browser left edge*//*font: bold 12px Verdana;*/list-style-type: none;}
            .mattblacktabs li{display: inline;margin: 0;}
            .mattblacktabs li a{ float: left; display: block; text-decoration: none; margin: 0;  padding: 7px 8px; /*padding inside each tab*/ border-right: 1px solid white; /*right divider between tabs*/color: white;background: #414141; /*background of tabs (default state)*/}
            .mattblacktabs li a:visited{color: white;}
            .mattblacktabs li a:hover, .mattblacktabs li.selected a{color:red;background: #dfdfdf; /*background of tabs for hover state, plus tab with "selected" class assigned to its LI */}
            .mattblacktabs li a.current{color:red;background: #dfdfdf;}
            .mattblacktabs li a.current:hover{color:red;background: #dfdfdf;}

            #cabecera {
                width:100%;
                background:#ffffff url('./../../inicio/images/separador.jpg') repeat-x right top;
            }
        </style>
        <script type="text/javascript" language="javascript">                                
                $(function(){
                    $(".mattblacktabs a").unbind('click').click(function(){
                        $(".mattblacktabs a").each(function(){
                            $(this).attr({"class":""});
                        });
                        $(this).attr({"class":"current"});
                    });
                    $("#op2").attr({"class":"current"});
                }); 
                var op = '<?php echo $opt?>';
                $('body').ready(function(){
                    $('#'+op).trigger('click');
                    $('#mainFrame').attr('src',$('#'+op).attr('href'));
                });
        </script>
    </head>
    <body style="margin-left:50px;margin-right:50px;">
        <div id="principal" style="background:#ffffff;  border: 1px solid #cccccc;">            
            <div id="cabecera">
                    <div style="float:left;"><img src="./../../inicio/images/gobierno_bolivariano.jpg" width="169px"/></div>
                    <div style="float:left;"><img src="./../../inicio/images/ministerio_energia_petroleo.jpg" width="145px"/></div>
                    <div style="float:right;"><img src="./../../inicio/images/bicentenario.jpg" width="74px"/></div>
                    <div style="clear: both;"></div>
            </div>            
            <div class="mattblacktabs">
                <ul>
                    <li><a id="op1" href="./../../inicio/index.php">Inicio</a></li>
                    <li><a id="op2" href="./grabadorVideo.php" target="mainFrame">Grabador de Video</a></li>
                    <li><a id="op3" href="../../vidcamara/view/camaras.php" target="mainFrame">Cámaras</a></li>
                    <li><a id="op11" href="../../vidcamaraaxisguard/view/vid_camara_axisguard.php" target="mainFrame">Guardias</a></li>
                    <li><a id="op4" href="../../monitorremoto/view/setPlantilla.php" target="mainFrame">Monitor Remoto</a></li>
                    <li><a id="op5" href="../../monitorasoc/view/monitorasoc.php" target="mainFrame">Asociar Monitoreo</a></li>
                    <li><a id="op6" href="../../zonas/view/zonas.php" target="mainFrame">Zonas Monitoreo</a></li>
                    <li><a id="op7" href="../../plantafisica/view/plantafisica.php" target="mainFrame">Planta F&iacute;sica</a></li>                    
                    <li><a id="op8" href="../../vidmarcacamara/view/vid_marca_camara.php" target="mainFrame">Marca C&aacute;mara</a></li>               
                    <li><a id="op9" href="../../vidmodelocamara/view/vid_modelo_camara.php" target="mainFrame">Modelo C&aacute;mara</a></li>            
                    <li><a id="op10" href="../../vidtipocamara/view/vid_tipo_camara.php" target="mainFrame">Tipo C&aacute;mara</a></li>
                    <li><a id="op99" href="./salir.php" target="mainFrame">Salir</a></li>
                </ul>
            </div>
            <div id="iframe">
                <iframe name="mainFrame" id="mainFrame" frameborder="0" scrolling="no" style="width:100%; height:650px" src="./grabadorVideo.php"></iframe>
            </div>
            <div id="piepagina" style="border-top:1px solid #d02433; width:100%; padding-bottom:50px; text-align:center">
                    <small>Centro Nacional de Innovación Tecnológica (CENIT) </small>
            </div>
        </div>
    </body>
</html>

