<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
* Modificado por David Concepcion 08-09-2010 CENIT
* Datos de ubicacion
*
* Modificado por David Concepcion 21-09-2010 CENIT
* Lista dinamica de Zona Horaria
*
* Modificado por David Concepcion 07-10-2010 CENIT
* Asociacion segmento servidores de video
* Mensaje Cargando...
*/
session_start(); // start up your PHP session! 
if(!isset($_SESSION['usuario'])){
    header("location: login.php",true);
    return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/videovigilancia_servidor_control.php";

$idservidor = (isset($_REQUEST['idservidor'])) ? trim($_REQUEST['idservidor']) : null;

$servidor = new Servidor();
$srv = $servidor->getServidor($idservidor);

$configuracion = new MotionConf();
$conf = $configuracion->getConfiguracion($idservidor);



?>			
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>Ver Servidores Grabadores de Video</title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        
        <style type="text/css">
            ul#savedItems { color: #770; font-size: .9em; list-style-position: outside; margin-left: 0; padding-left: 1.4em; }
            ul#savedItems li:hover { cursor: pointer;  }
            ul#savedItems li.error { color: #ff0000; }
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 80%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            #loadMap{
                position: absolute;
                background-color:#000000;
                opacity:.9;
                width: 100%;
                height: 100%;
                vertical-align: middle;
                color: #FFFFFF;
                font-size: 20px;
                display: none;                
            }
            
            /* Para que se vean los Tabs cuando se cargue el mapa*/
            .ui-tabs, .ui-tabs .ui-tabs-nav li, .ui-button{
                position: inherit;
            }
        </style>
        <script type="text/javascript" lang="javascript">
            $(function() {
                $('input:text,input:password').css({
                    background: '#fff' ,
                    border: '1px solid #d5d5d5',
                    '-moz-border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    'border-radius': '4px'
                });
                $( "input:button, input:submit" ).button();
                $('#tabs').tabs();
            });
            function mostrarMensajeError(mensaje){
                document.getElementById('divmensaje').style.border = "2px solid red";
                document.getElementById('divmensaje').innerHTML = mensaje;
            }
            function mostrarMensajeExito(mensaje){
                document.getElementById('divmensaje').style.border = "2px solid green";
                document.getElementById('divmensaje').innerHTML = mensaje;	
            }
            function hideLoading(){
                $("#cargando",parent.document).fadeOut("slow");
            }
            
            function setImage(idplantafisica){

                var link = 'mapaGetZonasCamaras.php?idservidor='+$('#idservidor').val()+'&idplantafisica='+idplantafisica;

                $("#frameMap").attr("src",link);
                $("#loadMap").fadeIn("slow");
            }
            // --- OBTIENE EL ESTADO DE EJECUCION DE PROCESO DE CAPTURA DE ALARMAS DE UNA CAMARA --- //
            function ajaxGetMotionServiceStatus(){
                $.ajax({
                    beforeSend: function( xhr ) {
                        $("#serverStatus").html('<img src="../images/loading51.gif" alt="" width="16" title="Buscando..." />');
                    },
                    type: "POST",
                    url: "getMotionServiceStatus.php",
                    data: 'ipv4='+$('#ipv4').val()+'&so_usuario='+$('#so_usuario').val()+'&so_clave='+$('#so_clave').val(),
                    success: function(html){
                        $("#serverStatus").html(html);
                    }
                });
                setTimeout("ajaxGetMotionServiceStatus();", 10*1000);
            }

            $(function(){
               ajaxGetMotionServiceStatus();
            });
        </script>	
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="loadMap" align="center" >
            <iframe id="frameMap" src="" ></iframe>
        </div>
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px">Visualizar Grabador de Video</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>

                <iframe name="ifrm1" id="ifrm1" frameborder="0" height="0px" width="0px" scrolling="no"></iframe>

                <form method="POST" name="f1" action="grabadorVideoAgregar2.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr>
                            <td align="right">Identificador</td>
                            <td><input type="text" name="idservidor" id="idservidor" value="<?php echo $srv->idservidor?>" disabled ></td>
                            <td align="right">Tipo</td>
                            <td><select name="tiposervidor" disabled>
                            <option value="motion"  <?php echo ($srv->idservidor==='motion')?'selected':''?>    >Motion</option>
                            </select>
                            <input type="checkbox" name="enlinea" value="1" <?php echo ($srv->enlinea)?'checked':''?> disabled>  En L&iacute;nea
                            </td>
                            </tr>
                        </table>
                        
                        <div id="tabs">

                            <ul >
                                <li><a href="#conexion">Conexi&oacute;n</a></li>
                                <li><a href="#deteccion">Detecci&oacute;n Mov.</a></li>
                                <li><a href="#archivoimagen">Archivo Imagen</a></li>
                                <li><a href="#opcionesffmpeg">Opciones FFMPEG</a></li>
                                <li><a href="#textomostrado">Textos</a></li>
                                <li><a href="#ubicacion">Ubicaci&oacute;n</a></li>
                            </ul>

                            <div id="conexion">
                                <table>                                    
                                    <tr title="Estado de Ejecuci&oacute;n del Grabador de Video">
                                        <td align="right">Estado:</td>
                                        <td>
                                            <div id="serverStatus"></div>
                                        </td>
                                    </tr>
                                <tr>
                                <td align="right">Estaci&oacute;n de Trabajo</td>
                                <td><input type="text" name="computador" value="<?php echo $srv->computador?>" disabled></td>
                                </tr>
                                <tr>
                                <td align="right">Direcci&oacute;n IP</td>
                                <td><input type="text" name="ipv4" id="ipv4" value="<?php echo $srv->ipv4?>" disabled></td>
                                </tr>
                                <tr>
                                <td align="right">Zona Horaria</td>
                                <td>
                                <select name="idtimezone" style="width: 400px;" disabled>
                                <?php
                                echo $servidor->makeCombo("timezone", "order by location", "idtimezone", "location,'   ',standard_time", $srv->idtimezone,false);
                                ?>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Usuario de Sistema</td>
                                <td><input type="text" name="so_usuario" id="so_usuario" value="<?php echo $srv->so_usuario?>" disabled></td>
                                </tr>
                                <tr>
                                <td align="right">Clave</td>
                                <td><input type="password" name="so_clave" id="so_clave" value="<?php echo $srv->so_clave?>" disabled></td>
                                </tr>
                                <tr>
                                <td>&nbsp;</td>
                                <td><input type="checkbox" name="conexioncamaras" value="1" <?php echo ($srv->conexioncamaras)?'checked':''?> disabled> Permite conexi&oacute;n directa con las c&aacute;maras</td>
                                </tr>
                                </table>
                            </div>

                            <div id="ubicacion">
                                <table>
                                <tr>
                                <td align="right">Pa&iacute;s:</td>
                                <td>
                                <input type="text" name="pais" size="50" maxlength="100" title="Pa&iacute;s" value="<?php echo $srv->pais?>" disabled>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Estado:</td>
                                <td>
                                <input type="text" name="estado" size="50" maxlength="100" title="Estado" value="<?php echo $srv->estado?>" disabled>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Ciudad:</td>
                                <td>
                                <input type="text" name="ciudad" size="50" maxlength="100" title="Ciudad" value="<?php echo $srv->ciudad?>" disabled>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Avenida:</td>
                                <td>
                                <input type="text" name="avenida" size="50" maxlength="100" title="Avenida" value="<?php echo $srv->avenida?>" disabled>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Edificio:</td>
                                <td>
                                <input type="text" name="edificio" size="50" maxlength="100" title="Edificio" value="<?php echo $srv->edificio?>" disabled>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Piso:</td>
                                <td>
                                <input type="text" name="piso" size="50" maxlength="100" title="Piso" value="<?php echo $srv->piso?>" disabled>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Oficina:</td>
                                <td>
                                <input type="text" name="oficina" size="50" maxlength="100" title="Oficina" value="<?php echo $srv->oficina?>" disabled>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Segmento</td>
                                <td>
                                <select name="idsegmento" style="width: 400px;" disabled>
                                <option value="">Seleccione</option>
                                <?php
                                echo $servidor->makeCombo("segmento", "where idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') order by segmento", "idsegmento", "segmento", $srv->idsegmento,false);
                                ?>
                                </select>
                                </td>
                                </tr>
                                <tr title="Planta F&iacute;sica">
                                <td align="right" valign="top">Planta F&iacute;sica:</td>
                                <td class="list">
                                <ul id="savedItems">
                                <?php
                                $getPlantasFisicas = $servidor->getPlantasFisicas($_REQUEST["idservidor"]);
                                if (count($getPlantasFisicas)>0){
                                foreach ($getPlantasFisicas as $row){
                                echo "<li onclick=\"setImage('".$row->idplantafisica."')\">".$row->plantafisica."</li>";
                                }
                                }
                                if (count($getPlantasFisicas)<=0){
                                echo "<li class=\"error\">No hay registros</li>";
                                }
                                ?>
                                </ul>
                                </td>
                                </tr>
                                </table>
                            </div>

                            <div id="deteccion">
                                <table>
                                <tr>
                                <td align="right">L&iacute;mite m&iacute;nimo de pixeles modificados para detecci&oacute;n</td>
                                <td>
                                <input type="text" name="threshold" value="<?php echo $conf->threshold?>" disabled size="4" maxlength="4" title="Limite m&iacute;nimo de pixeles modificados para activar la detecci&oacute;n movimiento">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Entonaci&oacute;n autom&aacute;tica del l&iacute;mite</td>
                                <td>
                                <select name="threshold_tune" title="Reduce autom&aacute;ticamente el limite si es posible" disabled>
                                <option value="off" <?php echo ($conf->zonahoraria==='off')?'selected':''?> >Desactivado</option>
                                <option value="on"  <?php echo ($conf->zonahoraria==='on')?'selected':''?> >Activado</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">L&iacute;mite m&iacute;nimo de ruido para detecci&oacute;n</td>
                                <td>
                                <input type="text" name="noise_level" value="<?php echo $conf->noise_level?>" disabled size="3" maxlength="3" title="Limite m&iacute;nimo de ruido en el ambiente para activar la detecci&oacute;n movimiento">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Entonaci&oacute;n autom&aacute;tica del l&iacute;mite de ruido</td>
                                <td>
                                <select name="noise_tune" title="Reduce autom&aacute;ticamente el limite m&iacute;nimo de ruido" disabled>
                                <option value="on"  <?php echo ($conf->noise_tune==='on')?'selected':''?> >Activado</option>
                                <option value="off" <?php echo ($conf->noise_tune==='off')?'selected':''?> >Desactivado</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Compensaci&oacute;n por Oscuridad</td>
                                <td>
                                <select name="night_compensate" disabled title="Entonaci&oacute;n autom&aacute;tica del l&iacute;mite de ruido y limite m&iacute;nimo de pixeles modificados para activar la detecci&oacute;n por oscuridad">
                                <option value="off" <?php echo ($conf->night_compensate==='off')?'selected':''?> >Desactivado</option>
                                <option value="on"  <?php echo ($conf->night_compensate==='on')?'selected':''?> >Activado</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">N&uacute;mero m&iacute;nimo de frames para detecci&oacute;n</td>
                                <td>
                                <input type="text" name="minimum_motion_frames" value="<?php echo $conf->minimum_motion_frames?>" disabled size="2" maxlength="2" title="N&uacute;mero m&iacute;nimo de frames con movimiento para activar la deteccion de movimiento">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Cantidad de segundos sin movimiento para desactivar la detecci&oacute;n</td>
                                <td>
                                <input type="text" name="gap" value="<?php echo $conf->gap?>" disabled size="2" maxlength="2" title="Cantidad m&aacute;xima de segundos transcurridos sin movimiento para desactivar el evento de detecci&oacute;n de movimiento.">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Cantidad m&aacute;xima en segundos de videos mpeg</td>
                                <td>
                                <input type="text" name="max_mpeg_time" value="<?php echo $conf->max_mpeg_time?>" disabled size="4" maxlength="4" title="Cantidad m&aacute;xima en segundos para la generacion de videos mpeg. Cuando esta cantidad es alcanzada se genera otro archivo de video.">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Frames por seg. a capturar si no hay movimiento.</td>
                                <td>
                                <input type="text" name="low_cpu" value="<?php echo $conf->low_cpu?>" disabled size="2" maxlength="2" title="Cantidad de frames por segundo a capturar cuando no detecci&oacute;n de Movimiento">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Salvar im&aacute;genes </td>
                                <td>
                                <select name="output_all" disabled title="Entonaci&oacute;n autom&aacute;tica del l&iacute;mite de ruido y limite m&iacute;nimo de pixeles modificados para activar la detecci&oacute;n por oscuridad">
                                <option value="off" <?php echo ($conf->output_all==='off')?'selected':''?> >Cuando detecci&oacute;n</option>
                                <option value="on"  <?php echo ($conf->output_all==='on')?'selected':''?> >Siempre</option>
                                </select>
                                </td>
                                </tr>
                                </table>
                            </div>

                            <div id="archivoimagen">
                                <table>
                                <tr>
                                <td align="right">Producci&oacute;n de Archivos de im&aacute;genes</td>
                                <td>
                                <select name="output_normal" disabled title="Producci&oacute;n de im&aacute;genes cuando se ha detectado Movimiento. Se puede tomar solo la primera, o la mejor, o todas, o ninguna ">
                                <option value="on"		<?php echo ($conf->output_normal==='on')?'selected':''?> >Siempre</option>
                                <option value="off" 	<?php echo ($conf->output_normal==='off')?'selected':''?> >Desactivado</option>
                                <option value="first"	<?php echo ($conf->output_normal==='first')?'selected':''?> >La primera</option>
                                <option value="Best" 	<?php echo ($conf->output_normal==='Best')?'selected':''?> >La mejor</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Salvado de Im&aacute;genes con solo los pixeles del movimiento</td>
                                <td>
                                <select name="output_motion" disabled title="Guardado o salvado de im&aacute;genes conteniendo solo los pixeles del movimiento (ghost Images)">
                                <option value="off" <?php echo ($conf->output_motion==='off')?'selected':''?> >Desactivado</option>
                                <option value="on"  <?php echo ($conf->output_motion==='on')?'selected':''?> >Activado</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Calidad para la comprensi&oacute;n jpeg</td>
                                <td>
                                <input type="text" name="quality" value="<?php echo $conf->quality?>" disabled size="2" maxlength="2" title="Calidad que se usara para la compresi&oacute;n de las im&aacute;genes jpeg">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Producci&oacute;n de im&aacute;genes ppm en vez de jpeg</td>
                                <td>
                                <select name="ppm" disabled title="No se producen im&aacute;genes jpeg, solo se producen im&aacute;genes ppm.">
                                <option value="off" <?php echo ($conf->ppm==='off')?'selected':''?> >Desactivado</option>
                                <option value="on"  <?php echo ($conf->ppm==='on')?'selected':''?> >Activado</option>
                                </select>
                                </td>
                                </tr>
                                </table>
                            </div>

                            <div id="opcionesffmpeg">
                                <table>
                                <tr>
                                <td align="right">Codificaci&oacute;n FFMPEG en tiempo real</td>
                                <td>
                                <select name="ffmpeg_cap_new" disabled title="Usar FFMPEG para codificar las Pel&iacute;culas mpeg en tiempo real">
                                <option value="on"  <?php echo ($conf->ffmpeg_cap_new==='on')?'selected':''?> >Activado</option>
                                <option value="off" <?php echo ($conf->ffmpeg_cap_new==='off')?'selected':''?> >Desactivado</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Pel&iacute;culas de los pixeles de los objetos en movimiento</td>
                                <td>
                                <select name="ffmpeg_cap_motion" disabled title="Usar FFMPEG para hacer Pel&iacute;culas solo con los pixeles de los objetos en movimiento (ghost images)">
                                <option value="off" <?php echo ($conf->ffmpeg_cap_motion==='off')?'selected':''?> >Desactivado</option>
                                <option value="on"  <?php echo ($conf->ffmpeg_cap_motion==='on')?'selected':''?> >Activado</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Bitrate para el codificador FFMPEG</td>
                                <td>
                                <input type="text" name="ffmpeg_bps" value="<?php echo $conf->ffmpeg_bps?>" disabled size="7" maxlength="7" title="Bitrate a ser utilizado por el codificador FFMPEG. Por defecto este valor es 500000.">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Bitrate variable para el codificador FFMPEG</td>
                                <td>
                                <input type="text" name="ffmpeg_variable_bitrate" value="<?php echo $conf->ffmpeg_variable_bitrate?>" disabled size="2" maxlength="2" title="Establece un Bitrate variable para el codificador FFMPEG. Si este valor es cero entonces se considera el Bitrate fijo, el fijado por el campo anterior. Tambi&eacute;n se puede colocar un n&uacute;mero entre 2 y 31 donde 2 significa la mejor calidad y 31 significa la peor calidad.">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Codec para la compresi&oacute;n de video.</td>
                                <td>
                                <select name="ffmpeg_video_codec" disabled title="Codec a utilizar por FFMPEG para la compresion de video. Los videos por lapsos de tiempo (timelapse) son hechos en mpeg1 independiente de la opcion que se seleccione aqu&iacute;.">
                                <option value="flv"		<?php echo ($conf->ffmpeg_video_codec==='flv')?'selected':''?> >flv</option>
                                <option value="mpeg1"	<?php echo ($conf->ffmpeg_video_codec==='mpeg1')?'selected':''?> >mpeg1</option>
                                <option value="mpeg4"	<?php echo ($conf->ffmpeg_video_codec==='mpeg4')?'selected':''?> >mpeg4</option>
                                <option value="msmpeg4"	<?php echo ($conf->ffmpeg_video_codec==='msmpeg4')?'selected':''?> >msmpeg4</option>
                                <option value="swf"		<?php echo ($conf->ffmpeg_video_codec==='swf')?'selected':''?> >swf</option>
                                <option value="ffv1"	<?php echo ($conf->ffmpeg_video_codec==='ffv1')?'selected':''?> >ffv1</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Usar FFMPEG para deinterlace videos</td>
                                <td>
                                <select name="ffmpeg_deinterlace" disabled title="Usar FFMPEG para deinterlace videos. Esto es necesario si se usan camaras anal&oacute;gicas y se muestran peinados horizontales en objetos en movimiento">
                                <option value="off"	<?php echo ($conf->ffmpeg_deinterlace==='off')?'selected':''?> >Desactivado</option>
                                <option value="on"	<?php echo ($conf->ffmpeg_deinterlace==='on')?'selected':''?> >Activado</option>
                                </select>
                                </td>
                                </tr>
                                </table>
                            </div>

                            <div id="textomostrado">
                                <table>
                                <tr>
                                <td align="right">Mostrar la fecha y la hora</td>
                                <td>
                                <input type="checkbox" name="text_right" value="1" <?php echo ($srv->text_right)?'checked':''?> disabled title="Dibuja la fecha y la hora en lado derecho.">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Texto en im&aacute;genes</td>
                                <td>
                                <input type="text" name="text_left" value="<?php echo $conf->text_left?>" disabled title="Coloca el texto en el lado inferior izquierdo de las im&aacute;genes">
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Mostrar el n&uacute;mero de pixeles cambiados</td>
                                <td>
                                <select name="text_changes" disabled title="Muestra el n&uacute;mero de pixeles cambiados en las im&aacute;genes.">
                                <option value="off" <?php echo ($conf->text_changes==='off')?'selected':''?> >Desactivado</option>
                                <option value="on"  <?php echo ($conf->text_changes==='on')?'selected':''?> >Activado</option>
                                </select>
                                </td>
                                </tr>
                                <tr>
                                <td align="right">Tama&ntilde;o de letras 2 veces m&aacute;s grande.</td>
                                <td>
                                <select name="text_double" disabled title="Dibuja los caracteres dos veces el tama&ntilde;o normal en las im&aacute;genes">
                                <option value="off" <?php echo ($conf->text_double==='off')?'selected':''?>>Desactivado</option>
                                <option value="on"  <?php echo ($conf->text_double==='on')?'selected':''?>>Activado</option>
                                </select>
                                </td>
                                </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
            }
            ?>   
        </div>
    </body>
</html>
