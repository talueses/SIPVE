<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Modificado por David Concepcion 08-09-2010 CENIT
 * Archivo de acciones agragar y modificar servidor y su configuracion
 *
 * Modificado por David Concepcion 21-09-2010 CENIT
 * Aumento de tiempo de redirección
 *
 * Modificado por David Concepcion 07-10-2010 CENIT
 * Mensaje Cargando...
 */
session_start(); // start up your PHP session!

//echo "<div align='left'><pre>".print_r($_POST)."</pre></div>";

if(!isset($_SESSION['usuario'])){
	echo "Sesión vencida. Recargue la página para iniciar la sesión.";
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past


require_once "../controller/videovigilancia_servidor_control.php";
$servidor = new ControlServidor();
$servidor ->setAccion($_POST["accion"]);
$exito = $servidor->agregarServidor();


?>			
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Agregar Servidores Grabadores de Video</title>
    <link href="../css/comunes.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script>
        <?php
        if(!$exito){
            echo "parent.mostrarMensajeError(\"".$servidor->mensaje."\");";
        }
        if($exito){
            echo "parent.mostrarMensajeExito(\"".$servidor->mensaje."\");";
            
            //if ($_POST["accion"]=="insertar"){
                echo "setTimeout('top.location.href=\"../../inicio/view/index.php?op=GrabadorVideo\";', 3*1000 );";
            //}
        }?>
            
        
    </script>

    </head>
    <body onload="parent.hideLoading();">
    </body>
</html>
