//Funcion que crea los alertas de las validaciones o mensajes 
function alerta(titulo, texto) {
var fondo = document.createElement('div');      
      fondo.style.backgroundImage = "url(img/fondo.gif)"; 
      fondo.style.position = "absolute"; 
      fondo.style.width = "100%"; 
      fondo.style.height = "100%"; 
      fondo.style.top = "200px"; 
      fondo.style.left = "0px"; 
      fondo.style.verticalAlign = "middle"; 
      fondo.align = "center";

var ventana = document.createElement('div'); 
      ventana.style.width = "250px"; 
      ventana.style.height = "100px"; 
      ventana.style.margin = "5px"; 
      ventana.style.border = "solid 1px #999999"; 
      ventana.style.backgroundColor = "#FFFFFF"; 
      
      ventana.onclick = function() { 
               fondo.style.visibility = "hidden"; 
            } 
           
      var barraTitulo = document.createElement('div'); 
         barraTitulo.style.backgroundColor = "#e2e3e5"; 
         barraTitulo.innerHTML = titulo; 
       
      var contenido = document.createElement('div'); 
         contenido.style.padding = "5px"; 
         contenido.innerHTML = texto; 
       
      var cerrar = document.createElement('div'); 
         cerrar.style.padding = "5px"; 

	ventana.appendChild (barraTitulo); 
        ventana.appendChild (contenido); 
        ventana.appendChild (cerrar);     
   	fondo.appendChild (ventana); 
 	document.body.appendChild(fondo); 
 
return true; 
}   
