//Funciones que asignar estilos especificos a la lista generada de usuarios en el div mostrarUsuarios de usuarios.php//
var filaSeleccionada = null;
var filaSeleccionadaEsPar = null;
    //
	function activar(fila, par){
		if(filaSeleccionada!=null && filaSeleccionadaEsPar){
			document.getElementById(filaSeleccionada).className='par';
		}

		if(filaSeleccionada!=null && !filaSeleccionadaEsPar){
			document.getElementById(filaSeleccionada).className='impar';
		}
		
		filaSeleccionada = fila.id;
		filaSeleccionadaEsPar = par;
		fila.className='seleccionado'
		//document.getElementById('frm1').src="consultarUsuario.php?id="+filaSeleccionada+"";          
	}

	function mOver(fila){
		if(filaSeleccionada == fila.id) return;
		fila.className='sel'
	}

	function mOut(fila, estilo){
		if(filaSeleccionada == fila.id) return;
		fila.className=estilo;
	}


//Funcion que muestra los mensajes que devuelve el controlador de usuario
//	function mostrarMensaje(){
//		<?php
//		if(isset($_POST['accion'])){
//			echo "alert('".$usuario_ctrl->mensaje."');\n";
//		}?>
//			return false;
//		}
//funcion que muestra y oculta las pestañas de opciones de usuario
	function subir(elemento,op) {
    /*  general.style.display = "none";
      general2.style.display = "none";
      general3.style.display = "none";
      general4.style.display = "none";
      general5.style.display = "none";
      general6.style.display = "none";
      elemento.style.display = "block";*/
	 
	 
   	    document.getElementById('general').style.display = "none";
		document.getElementById('general2').style.display = "none";
		document.getElementById('general3').style.display = "none";
		document.getElementById('general4').style.display = "none";
		document.getElementById('general5').style.display = "none";
		document.getElementById('general6').style.display = "none";
		document.getElementById(elemento).style.display = "block";
	 
	 
	 
	 
	 
	 
	  /* Cambia el current del menu de la derecha */
	/*  op1.className="";
	  op2.className="";
	  op3.className="";
	  op4.className="";
	 op5.className="";
	 op6.className="";
	 op.className="current";*/
	
	document.getElementById("op1").className="";
	document.getElementById("op2").className="";
	document.getElementById("op3").className="";
	document.getElementById("op4").className="";
	document.getElementById("op5").className="";
	document.getElementById("op6").className="";
	document.getElementById(op).className="current";
							 
	  }
	  
	  //Funcion que monta una pagina sobre la otra en el div addUsuario	  
	 function mostrarUsuario(usuario){
	 	usuarioGlobal = usuario;
	 	//creo objeto vacio
		var param = {};
		//Le asigno atributos al objeto
		param.usuario = usuario;
		param.accion = "Buscar Usuario";
		//	new Ajax.Updater('id del div', 'pagina.php', {  parameters: param,  method:'post'});
		$('cargando-ajax').innerHTML="<img src='../images/ajax-loader.gif'>";
		$('cargando-ajax').style.display='block';
	 	new Ajax.Updater('addUsuario', 'consultarUsuario.php', {  parameters: param,  method:'post'});
	 }
	 
	 //Funcion para cargar el formulario necesario para crear un nuevo usuario
	  function crearUsuario(){  
		$('cargando-ajax').innerHTML="<img src='../images/ajax-loader.gif'>";
		$('cargando-ajax').style.display='block';
	 	new Ajax.Updater('addUsuario', 'agregarUsuario.php');
	 }
	 
	 //Funcion para cargar el formulario necesario para poder modificar usuario
	  function modificarUsuario(){
        if(filaSeleccionada!=null){	   
	    	//creo objeto vacio
			var param = {};
			//Le asigno atributos al objeto
			param.usuario =usuarioGlobal;
			param.accion = "modificar Usuario";
	 		new Ajax.Updater('addUsuario', 'modificarUsuario.php', {  parameters: param,  method:'post'});
			$('cargando-ajax').innerHTML="<img src='../images/ajax-loader.gif'>";
			$('cargando-ajax').style.display='block';	   
		  }
		else {alerta("alerta", "Debe Seleccionar un usuario para poder modificarlo");}
	 }	 
	 
	 function orden(elem,orden){
			document.forden.orderBy.value=orden;
			document.forden.orderType.value=elem.className;
			document.forden.submit();
			return false;	
		}	
	  function eliminarUsuario(){
	  	
        if(filaSeleccionada!=null){	   
	    	//creo objeto vacio
			var param = {};
			//Le asigno atributos al objeto
			param.usuario =usuarioGlobal;
			param.accion = "modificar Usuario";
	 		$('cargando-ajax').innerHTML="<img src='../images/ajax-loader.gif'>";
			$('cargando-ajax').style.display='block';	
			new Ajax.Updater('addUsuario', 'eliminarUsuario.php', {  parameters: param,  method:'post'});
			
			if(confirm(" ¿Estas seguro que deseas eliminar al usuario seleccionado? "))
			{document.f1.submit()
		}  
			
			   
		  }
		else {alerta("alerta", "Debe Seleccionar un usuario para poder eliminarlo");}
		
		
		
	    
		//if(confirm('¿Estas seguro que desea eliminar a este usuario?'))
	//	{document.tuformulario.submit()}   
	//	  }
	//	else {alerta("alerta", "Debe Seleccionar un usuario para poder eliminarlo");}
	 }	 
	 
	 
	 //Funcion que reenvia el formulario forden con los parametros para ordenar la lista de usuarios
	 	
	  


//Funcion que retorna pagina para el pagineo	  
	function returnPagina(i){	
		document.forden.pagina.value=i;
		document.forden.submit();
		return;
	}
	
//funcion que verifica la condicion del check para ocultar o aparecer el div activo	
	function condicionCheck(){
	
	if(check.checkbox1==on){activo.style.display === "none";return;}
	if(check.checkbox1!=on){activo.style.display === "block";return;}
	
	
	}
	
//Funcion que comprueba el check box para mostarra los div.	
	function CompruebaCheckBox(checkerbox){
	
	
	if(checkerbox.checked){
	document.getElementById('activado').style.display="block"
	document.getElementById('desactivado').style.display="none"
	document.getElementById('original').style.display="none"
	}
	else{
	document.getElementById('activado').style.display="none"
	document.getElementById('desactivado').style.display="block"
	document.getElementById('original').style.display="none"
	}
	}  
		
	  	
	
	
	
//GRUPO DE PERMISOS DE SISTEMAS

	 //Funcion para cargar el formulario necesario para crear un nuevo grupo de permiso
	  function agregarGrupoPermisoSistema(){  
		$('cargando-ajax').innerHTML="<img src='../images/ajax-loader.gif'>";
		$('cargando-ajax').style.display='block';
	 	new Ajax.Updater('addUsuario', 'agregarGrupoPermisoSistema.php',{ evalScripts: true });
	 }  
	  
	  
	  
	  
	  
	  
	  