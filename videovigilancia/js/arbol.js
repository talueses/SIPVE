
//función que alterna los nodos      
	function toggleNode(node) 
      {
        var nodeArray = node.childNodes;
        for(i=0; i < nodeArray.length; i++)
        {
          node = nodeArray[i];
          if (node.tagName && node.tagName.toLowerCase() == 'div')
            node.style.display = (node.style.display == 'block') ? 'none' : 'block';
        }
      }
      
      
//función que abre los nodos
	function openNode(node)
      {
        var nodeArray = node.childNodes;
        for(i=0; i < nodeArray.length; i++)
        {
          node = nodeArray[i];
          if (node.tagName && node.tagName.toLowerCase() == 'div')
            node.style.display = 'block';
        }
      }

    
//función que cierra los nodos
	function closeNode(node)
      {
        var nodeArray = node.childNodes;
        for(i=0; i < nodeArray.length; i++)
        {
          node = nodeArray[i];
          if (node.tagName && node.tagName.toLowerCase() == 'div')
            node.style.display = 'none';
        }
      }

      
//función que muestra los nodos
	function showNode(node)
      {
        if (!node) return;
        if (!node.parentNode) return;
        node = node.parentNode;
        while (node.tagName.toLowerCase() == 'div')
        {
          openNode(node);
          node = node.parentNode;
        }
      }


