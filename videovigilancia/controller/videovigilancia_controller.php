<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
include_once '../../inicio/controller/controller.php';
/**
 * Clase Control Local
 * Metodos de control base
 * @todo desarrollar metodo getMotionServiceStatus(), usar de ejemplo el metodo getMotionStatus() de monitor remoto
 *       http://150.186.195.144:18080/ <= url para saber si el servidor de video esta en linea
 */
class ControllerLocal extends Controller{

        var $idservidor = null;
        
        public function setIdservidor($idservidor){
		$this->idservidor = $idservidor;
	}
	public function getIdservidor(){
		 return $this->idservidor;
	}
        public function setDataServer($dataServer){
                $this->dataServer = $dataServer;
        }

        public function getDataServer(){
                 return $this->dataServer;
        }

        public function setServerRemoto($serverRemoto){
                $this->serverRemoto = $serverRemoto;
        }

        public function getServerRemoto(){
                 return $this->serverRemoto;
        }

        public function setUsuarioRemoto($usuarioRemoto){
                $this->usuarioRemoto = $usuarioRemoto;
        }

        public function getUsuarioRemoto(){
                 return $this->usuarioRemoto;
        }

        public function setPasswordRemoto($passwordRemoto){
                $this->passwordRemoto = $passwordRemoto;
        }

        public function getPasswordRemoto(){
                 return $this->passwordRemoto;
        }

        public function setArchivoSubir($archivoSubir){
                $this->archivoSubir = $archivoSubir;
        }

        public function getArchivoSubir(){
                 return $this->archivoSubir;
        }

        public function setComandoRemoto($comandoRemoto){
                $this->comandoRemoto = $comandoRemoto;
        }

        public function getComandoRemoto(){
                 return $this->comandoRemoto;
        }

        public function setConf($conf){
                $this->conf = $conf;
        }

        public function getConf(){
                 return $this->conf;
        }

        public function setContenido($contenido){
                $this->contenido = $contenido;
        }

        public function getContenido(){
                 return $this->contenido;
        }

        public function setFileName($fileName){
                $this->fileName = $fileName;
        }

        public function getFileName(){
                 return $this->fileName;
        }
	/*
	 * Diagnostica si el servidor esta prestando servicio.
	 * Modo de uso:
	 * sipPing("150.186.193.104", 5)
	 * Envia 5 paquetes de diagnostico a la computadora con direccion ip 150.186.193.104
	 * Devuelve true si el servidor o computador recive alguno de los paquetes enviados.
	 * Devuelve false cuando el servidor o computador no recive ninguno de los paquetes enviados.
	 */
 	function sipPing($host, $c){
		$comm = "ping -c$c $host"; // ping -c4 150.186.193.104
		$output=shell_exec($comm);
		$pos = strpos($output, "$c packets transmitted, 0 received");
		if($pos){
			echo "no hay conexion con la maquina, enviar una alarma a la base de datos....<br>";
			return false;
		}
		$output = str_replace("\n", "<br>", $output);
		echo $output;
		return true;
	}

        /**
         * Verifica el estado de captura de eventos de una camara
         * @return boolean Devuelve verdadero si la camara esta activa y vis.
         */
        static function getMotionServiceStatus($args){
            
            $url = "http://".$args->ipv4.":18080";
            //echo "<br>".$url;
            $resp = self::httpRequest($url, $args->so_usuario, $args->so_clave);
            //echo $resp;
            
            if (preg_match("/Running/", $resp)){
                //echo "<p>true</p>";
                return true;
            }
            if (preg_match("/Invalid server response/", $resp)){
                //echo "<p>false</p>";
                return false;
            }
        }

	
	/*
	 * Envia archivos al servidor mediante conexion ssh.
         * Debe estar instalado el paquete ssh
	 */
	function sipFtp(){

            $server_remoto   = $this->getServerRemoto();
            $usuario_remoto  = $this->getUsuarioRemoto();
            $password_remoto = $this->getPasswordRemoto();
            $archivo_a_subir = $this->getArchivoSubir();
            if (!$server_remoto || !$usuario_remoto || !$password_remoto || !$archivo_a_subir){
                return false;
            }

            $script_folder = "../shell_script/";
            $script = "sip_ftp.py";

            if (!file_exists($script_folder.$script))
                return false;
                //return "El script $script_folder$script, para copia remota de archivos, no existe en el servidor...";


            if (!file_exists($archivo_a_subir))
                return false;
                //return "El archivo a subir: $archivos_a_subir no existe en el servidor...";

            $directorio_remoto = "/home/$usuario_remoto/motion";

            $command = $script_folder.$script;
            $parametros =" -s $server_remoto -d $directorio_remoto -u $usuario_remoto -p $password_remoto -f $archivo_a_subir";
            //echo $command.$parametros."<br>";
            $output = shell_exec($command.$parametros);

            return $output;
	}

	/*
         * Ejecuta comandos en el servidor mediante conexion ssh
         * Debe estar instalado el paquete ssh
         */
	function sipSsh(){

            $server_remoto   = $this->getServerRemoto();
            $usuario_remoto  = $this->getUsuarioRemoto();
            $password_remoto = $this->getPasswordRemoto();
            $comando_remoto  = $this->getComandoRemoto();
            if (!$server_remoto || !$usuario_remoto || !$password_remoto || !$comando_remoto){
                return false;
            }

            $script_folder = "../shell_script/";
            $script = "sip_ssh.py";

            if (!file_exists($script_folder.$script))
                return false;
                //"El script $script_folder$script, para la ejecucion remota de comandos, no existe en el servidor...";

            //EJEMPLO:  ./sip_ssh.py -s 150.186.193.179 -u simulab-5 -p cenit -c '/etc/init.d/motion stop'
            //$comando_remoto = "/etc/init.d/motion stop";
            //$comando_remoto = "\"motion -c ./motion/motion.conf\"";


            $command = $script_folder.$script;
            $parametros =" -s $server_remoto -u $usuario_remoto -p $password_remoto -c \"$comando_remoto\"";
            //echo $command.$parametros."<br>";
            $output = shell_exec($command.$parametros);

            return $output;
	}
	
	/*
         * Escribe el archivo de configuracion con datos guardados en bases de datos
         */
	function escribirConfiguracion(){

            $conf = $this->getConf();
            $contenido = $this->getContenido();
            $fileName = $this->getFileName();
            if (!$conf || !$contenido || !$fileName){
                return false;
            }

            foreach ($conf as $key => $value) {
                //echo $key . "--> ".$value."<br>";
                $contenido = str_replace("{".$key."}", $value, $contenido );
            }
            $contenido = str_replace("{SERVER_ADDR}", $_SERVER["SERVER_ADDR"], $contenido ); // IP ADDRESS APACHE SERVER

            $exito = file_put_contents('../motion/'.$fileName.'.conf', $contenido);
            return $exito;
	}

        /**
         * Eliminar archivo de configuracion con datos guardados en bases de datos
         */
        function eliminarConfiguracion(){
            $fileName = $this->getFileName();
            @unlink('../motion/'.$fileName.'.conf');
        }

        
}
?>