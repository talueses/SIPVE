<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDSRV"); //Categoria del modulo
require_once "../model/videovigilancia_servidor.model.php";
require_once "../model/videovigilancia_motionconf.model.php";
require_once "videovigilancia_controller.php";

/**
 * Clase Control Sevidor
 * Metodos de Control Logica de Negocios
 */
class ControlServidor extends ControllerLocal{
    /**
     * @var string Mensaje de Exito o Error
     */
    var $mensaje = null;
    /**
     * @var string Accion a realizar
     */
    var $accion  = null;

    /**
     * Establece Accion
     * @param string Accion a realizar
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }

    /**
     * Devuelve la Accion
     * @return string Accion a realizar
     */
    public function getAccion(){
         return $this->accion;
    }

    /**
     * Devuelve los servidores registrados
     * @return object Devuelve registros como arreglo de objetos
     */
    function getServidores(){
            
            $servidor = new Servidor();
            return $servidor->getServidores();
    }

    /*
     *  === Ejecucion de comandos remotos ===
     *  Configuraciones del servidor de video remoto
     */
    function setServer(){

        
        $model = new Servidor();

        if ($this->getIdservidor()){
            $arg["idservidor"] = "'".$this->getIdservidor()."'";
        }
        $this->setDataServer($model->getConfiguracionServidor($arg));

        if (!$this->setConfiguracionServidor()) return false;

        return true;

    }

    /**
     * Elimina un servidor y sus datos relacionados
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarServidor(){

        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->esValidoIdServidor()) return false;

        $this->setIdservidor(strtoupper(trim($_POST['idservidor'])));

        // ----------- ESTABLECER LAS CONFIGURACIONES EN EL SERVIDOR -----------//
        if (!$this->setServer()) return false;

        // -------- ELIMINAR CONFIGURACION SERVIDOR --------------------//
        
        $config = new MotionConf();
        $config->setIdservidor(strtoupper(trim($_POST['idservidor'])));
        $exito = $config->eliminarRegistro($config->getIdservidor());
        if(!$exito){
            $this->mensaje = "La configuraci&oacute;n del grabador de video no pudo ser eliminada: ".$config->getMensaje();
            return false;
        }


        
        $servidor = new Servidor();
        $servidor->setIdservidor(strtoupper(trim($_POST['idservidor'])));

        // ----------------- ELIMINAR CAMARAS --------------------------//
        $exito = $servidor->eliminarCamaras($this->getIdservidor());
        if(!$exito){
            $this->mensaje = "Las camaras no se eliminaron".$servidor->getMensaje();
            return false;
        }
        // ----------------- ELIMINAR  SERVIDOR ------------------------//
        $exito = $servidor->eliminarRegistro($servidor->getIdservidor());
        if(!$exito){
            $this->mensaje = "El grabador de video no pudo ser eliminado: ".$servidor->getMensaje();
            return false;
        }

        $this->mensaje = "El grabador de video fué eliminado exitosamente...<br><b><i>Debe reiniciar el grabador de video</i></b>";
        return true;

    }

    /**
     * Agregar y modificar servidor de video
     * @return boolean Devuelve verdadero si el proseco se ejecuta exitosamente
     */
    function agregarServidor(){

            //------------------ VALIDACION DE CAMPOS  -----------------//
            if(!$this->esValidoIdServidor()) return false;
            if(!$this->sonValidosParametrosConexion()) return false;
            if(!$this->sonValidosParametrosDeteccionMovimiento()) return false;
            if(!$this->sonValidosParametrosArchivoImagenes()) return false;
            if(!$this->sonValidosParametrosOpcionesFFMPEG()) return false;
            if(!$this->sonValidosParametrosTextos()) return false;
            if(!$this->sonValidosParametrosUbicacion()) return false;

            $this->setIdservidor(strtoupper(trim($_POST['idservidor'])));

            // ----------------- AGREGAR SERVIDOR ----------------------//
            
            $servidor = new Servidor();
            $servidor->setIdservidor(strtoupper(trim($_POST['idservidor'])));
            $servidor->setComputador(trim($_POST['computador']));
            $servidor->setTiposervidor(trim($_POST['tiposervidor']));
            $servidor->setIpv4(trim($_POST['ipv4']));
            $servidor->setEnlinea(trim($_POST['enlinea']));
            $servidor->setIdtimezone(trim($_POST['idtimezone']));
            $servidor->setSo_usuario(trim($_POST['so_usuario']));
            $servidor->setSo_clave(trim($_POST['so_clave']));
            $servidor->setConexioncamaras(trim($_POST['conexioncamaras']));

            // --------------------- DATOS DE UBICACION -------------------//
            $servidor->setPais(trim($_POST['pais']));
            $servidor->setEstado(trim($_POST['estado']));
            $servidor->setCiudad(trim($_POST['ciudad']));
            $servidor->setAvenida(trim($_POST['avenida']));
            $servidor->setEdificio(trim($_POST['edificio']));
            $servidor->setPiso(trim($_POST['piso']));
            $servidor->setOficina(trim($_POST['oficina']));
            $servidor->setIdsegmento(trim($_POST['idsegmento']));

            if ($this->getAccion()=="insertar"){
                $exito = $servidor->insertarRegistro();
            }
            if ($this->getAccion()=="modificar"){
                $exito = $servidor->modificarRegistro($servidor->getIdservidor());
            }

            if(!$exito){
                    if ($this->getAccion()=="insertar"){
                        $this->mensaje = "El grabador de video no pudo ser creado: ".$servidor->getMensaje();
                    }
                    if ($this->getAccion()=="modificar"){
                        $this->mensaje = "El grabador de video no pudo ser modificado: ".$servidor->getMensaje();
                    }
                    return false;
            }
            // --------------FIN AGREGAR SERVIDOR ----------------------//


            // ----------------- AGREGAR CONF SERVIDOR ----------------------//
            $tiposervidor = (isset($_POST['tiposervidor']) && trim($_POST['tiposervidor'])!=='')? trim($_POST['tiposervidor']) : 0;
            if(strtoupper($tiposervidor)==='MOTION'){

                    
                    $config = new MotionConf();
                    $config->setIdservidor(strtoupper(trim($_POST['idservidor'])));
                    $config->setIpv4(trim($_POST['ipv4']));
                    $config->setDaemon(trim($_POST['daemon']));
                    $config->setProcess_id_file(trim($_POST['process_id_file']));
                    $config->setSetup_mode(trim($_POST['setup_mode']));
                    $config->setVideodevice(trim($_POST['videodevice']));
                    $config->setTunerdevice(trim($_POST['tunerdevice']));
                    $config->setInput(trim($_POST['input']));
                    $config->setNorm(trim($_POST['norm']));
                    $config->setFrequency(trim($_POST['frequency']));
                    $config->setRotate(trim($_POST['rotate']));
                    $config->setWidth(trim($_POST['width']));
                    $config->setHeight(trim($_POST['height']));
                    $config->setFramerate(trim($_POST['framerate']));
                    $config->setMinimum_frame_time(trim($_POST['minimum_frame_time']));
                    $config->setNetcam_url(trim($_POST['netcam_url']));
                    $config->setNetcam_userpass(trim($_POST['netcam_userpass']));
                    $config->setNetcam_proxy(trim($_POST['netcam_proxy']));
                    $config->setAuto_brightness(trim($_POST['auto_brightness']));
                    $config->setBrightness(trim($_POST['brightness']));
                    $config->setContrast(trim($_POST['contrast']));
                    $config->setSaturation(trim($_POST['saturation']));
                    $config->setHue(trim($_POST['hue']));
                    $config->setRoundrobin_frames(trim($_POST['roundrobin_frames']));
                    $config->setRoundrobin_skip(trim($_POST['roundrobin_skip']));
                    $config->setSwitchfilter(trim($_POST['switchfilter']));
                    $config->setThreshold(trim($_POST['threshold']));
                    $config->setThreshold_tune(trim($_POST['threshold_tune']));
                    $config->setNoise_level(trim($_POST['noise_level']));
                    $config->setNoise_tune(trim($_POST['noise_tune']));
                    $config->setNight_compensate(trim($_POST['night_compensate']));
                    $config->setDespeckle(trim($_POST['despeckle']));
                    $config->setMask_file(trim($_POST['mask_file']));
                    $config->setSmart_mask_speed(trim($_POST['smart_mask_speed']));
                    $config->setLightswitch(trim($_POST['lightswitch']));
                    $config->setMinimum_motion_frames(trim($_POST['minimum_motion_frames']));
                    $config->setPre_capture(trim($_POST['pre_capture']));
                    $config->setPost_capture(trim($_POST['post_capture']));
                    $config->setGap(trim($_POST['gap']));
                    $config->setMax_mpeg_time(trim($_POST['max_mpeg_time']));
                    $config->setLow_cpu(trim($_POST['low_cpu']));
                    $config->setOutput_all(trim($_POST['output_all']));
                    $config->setOutput_normal(trim($_POST['output_normal']));
                    $config->setOutput_motion(trim($_POST['output_motion']));
                    $config->setQuality(trim($_POST['quality']));
                    $config->setPpm(trim($_POST['ppm']));
                    $config->setFfmpeg_cap_new(trim($_POST['ffmpeg_cap_new']));
                    $config->setFfmpeg_cap_motion(trim($_POST['ffmpeg_cap_motion']));
                    $config->setFfmpeg_timelapse(trim($_POST['ffmpeg_timelapse']));
                    $config->setFfmpeg_timelapse_mode(trim($_POST['ffmpeg_timelapse_mode']));
                    $config->setFfmpeg_bps(trim($_POST['ffmpeg_bps']));
                    $config->setFfmpeg_variable_bitrate(trim($_POST['ffmpeg_variable_bitrate']));
                    $config->setFfmpeg_video_codec(trim($_POST['ffmpeg_video_codec']));
                    $config->setFfmpeg_deinterlace(trim($_POST['ffmpeg_deinterlace']));
                    $config->setSnapshot_interval(trim($_POST['snapshot_interval']));
                    $config->setLocate(trim($_POST['locate']));
                    $config->setText_right(trim($_POST['text_right']));
                    $config->setText_left(trim($_POST['text_left']));
                    $config->setText_changes(trim($_POST['text_changes']));
                    $config->setText_event(trim($_POST['text_event']));
                    $config->setText_double(trim($_POST['text_double']));
                    $config->setTarget_dir(trim($_POST['target_dir']));
                    $config->setSnapshot_filename(trim($_POST['snapshot_filename']));
                    $config->setJpeg_filename(trim($_POST['jpeg_filename']));
                    $config->setMovie_filename(trim($_POST['movie_filename']));
                    $config->setTimelapse_filename(trim($_POST['timelapse_filename']));
                    $config->setWebcam_port(trim($_POST['webcam_port']));
                    $config->setWebcam_quality(trim($_POST['webcam_quality']));
                    $config->setWebcam_motion(trim($_POST['webcam_motion']));
                    $config->setWebcam_maxrate(trim($_POST['webcam_maxrate']));
                    $config->setWebcam_localhost(trim($_POST['webcam_localhost']));
                    $config->setWebcam_limit(trim($_POST['webcam_limit']));
                    $config->setControl_port(trim($_POST['control_port']));
                    $config->setControl_localhost(trim($_POST['control_localhost']));
                    $config->setControl_html_output(trim($_POST['control_html_output']));
                    $config->setControl_authentication(trim($_POST['control_authentication']));
                    $config->setTrack_type(trim($_POST['track_type']));
                    $config->setTrack_auto(trim($_POST['track_auto']));
                    $config->setTrack_port(trim($_POST['track_port']));
                    $config->setTrack_motorx(trim($_POST['track_motorx']));
                    $config->setTrack_motory(trim($_POST['track_motory']));
                    $config->setTrack_maxx(trim($_POST['track_maxx']));
                    $config->setTrack_maxy(trim($_POST['track_maxy']));
                    $config->setTrack_iomojo_id(trim($_POST['track_iomojo_id']));
                    $config->setTrack_step_angle_x(trim($_POST['track_step_angle_x']));
                    $config->setTrack_step_angle_y(trim($_POST['track_step_angle_y']));
                    $config->setTrack_move_wait(trim($_POST['track_move_wait']));
                    $config->setTrack_speed(trim($_POST['track_speed']));
                    $config->setTrack_stepsize(trim($_POST['track_stepsize']));
                    $config->setQuiet(trim($_POST['quiet']));
                    $config->setOn_event_start(trim($_POST['on_event_start']));
                    $config->setOn_event_end(trim($_POST['on_event_end']));
                    $config->setOn_picture_save(trim($_POST['on_picture_save']));
                    $config->setOn_motion_detected(trim($_POST['on_motion_detected']));
                    $config->setOn_movie_start(trim($_POST['on_movie_start']));
                    $config->setOn_movie_end(trim($_POST['on_movie_end']));
                    $config->setVideo_pipe(trim($_POST['video_pipe']));
                    $config->setMotion_video_pipe(trim($_POST['motion_video_pipe']));

                    if ($this->getAccion()=="insertar"){
                        $registro_exitoso = $config->insertarRegistro();
                    }
                    if ($this->getAccion()=="modificar"){
                        $registro_exitoso = $config->modificarRegistro($config->getIdservidor());
                    }
                    if(!$registro_exitoso){
                            $this->mensaje = $config->getMensaje();
                            return false;
                    }

                    // ----------- ESTABLECER LAS CONFIGURACIONES EN EL SERVIDOR -----------//
                    if (!$this->setServer()) return false;

                    // ------------------------------- MENSAJE EXITO --------------------------------//
                    if ($this->getAccion()=="insertar"){
                        $this->mensaje = "El grabador de video fué creado exitosamente...<br><b><i>Debe reiniciar el servidor de video</i></b>";
                    }
                    if ($this->getAccion()=="modificar"){
                        $this->mensaje = "El grabador de video fué modificado exitosamente...<br><b><i>Debe reiniciar el servidor de video</i></b>";
                    }

            }
            // --------------FIN AGREGAR CONF SERVIDOR ----------------------//
            return true;
    }

    function esValidoIdServidor(){
            $idservidor = (isset($_POST['idservidor']) && trim($_POST['idservidor'])!=='')? trim($_POST['idservidor']) : null;
            if($idservidor===null){
                    $this->mensaje = "Falta introducir el identificador del grabador de video";
                    return false;
            }
            if(!$this->esAlfaNumerico($idservidor)){
                    $this->mensaje = "El identificador del grabador de video tiene que contener solo caracteres alfanuméricos";
                    return false;
            }
            return true;
    }


    function sonValidosParametrosTextos(){
            $text_left = (isset($_POST['text_left']) && trim($_POST['text_left'])!=='')? trim($_POST['text_left']) : '';
            if($text_left==='') return true;
            /*if(!$this->esAlfaNumericoConEspacios($text_left)){
                    $this->mensaje = "Texto en imagenes: NO VÁLIDO";
                    return false;
            }*/
            return true;
    }

    function sonValidosParametrosOpcionesFFMPEG(){
            $ffmpeg_timelapse = (isset($_POST['ffmpeg_timelapse']) && trim($_POST['ffmpeg_timelapse'])!=='')? trim($_POST['ffmpeg_timelapse']) : 0;
            if(!$this->esNumerico($ffmpeg_timelapse)){
                    $this->mensaje = "Crear video usando FFMPEG para videos por lapsos contiene caracteres NO NUMÉRICOS";
                    return false;
            }
            $ffmpeg_bps = (isset($_POST['ffmpeg_bps']) && trim($_POST['ffmpeg_bps'])!=='')? trim($_POST['ffmpeg_bps']) : 500000;
            if(!$this->esNumerico($ffmpeg_bps)){
                    $this->mensaje = "Bitrate para el codificador FFMPEG contiene caracteres NO NUMÉRICOS";
                    return false;
            }
            $ffmpeg_variable_bitrate = (isset($_POST['ffmpeg_variable_bitrate']) && trim($_POST['ffmpeg_variable_bitrate'])!=='')? trim($_POST['ffmpeg_variable_bitrate']) : 0;
            if(!$this->esNumerico($ffmpeg_variable_bitrate)){
                    $this->mensaje = "Bitrate variable para el codificador FFMPEG contiene caracteres NO NUMÉRICOS";
                    return false;
            }
            return true;
    }

    function sonValidosParametrosArchivoImagenes(){
            $quality = (isset($_POST['quality']) && trim($_POST['quality'])!=='')? trim($_POST['quality']) : 75;
            if(!$this->esNumerico($quality)){
                    $this->mensaje = "Calidad para la comprensión jpeg contiene caracteres NO NUMÉRICOS";
                    return false;
            }
            return true;
    }

    function sonValidosParametrosDeteccionMovimiento(){
            $threshold = (isset($_POST['threshold']) && trim($_POST['threshold'])!=='')? trim($_POST['threshold']) : 1500;
            if(!$this->esNumerico($threshold)){
                    $this->mensaje = "Límite mínimo de pixeles modificados para detección contiene caracteres NO NUMÉRICOS";
                    return false;
            }

            $noise_level = (isset($_POST['noise_level']) && trim($_POST['noise_level'])!=='')? trim($_POST['noise_level']) : 32;
            if(!$this->esNumerico($noise_level)){
                    $this->mensaje = "Límite mínimo de ruido para detección contiene caracteres NO NUMÉRICOS";
                    return false;
            }

            $minimum_motion_frames = (isset($_POST['minimum_motion_frames']) && trim($_POST['minimum_motion_frames'])!=='')? trim($_POST['minimum_motion_frames']) : 1;
            if(!$this->esNumerico($minimum_motion_frames)){
                    $this->mensaje = "Número mínimo de frames para detección contiene caracteres NO NUMÉRICOS";
                    return false;
            }

            $gap = (isset($_POST['gap']) && trim($_POST['gap'])!=='')? trim($_POST['gap']) : 60;
            if(!$this->esNumerico($gap)){
                    $this->mensaje = "Cantidad de segundos sin movimiento para desactivar la detección contiene caracteres NO NUMÉRICOS";
                    return false;
            }

            $max_mpeg_time = (isset($_POST['max_mpeg_time']) && trim($_POST['max_mpeg_time'])!=='')? trim($_POST['max_mpeg_time']) : 3600;
            if(!$this->esNumerico($max_mpeg_time)){
                    $this->mensaje = "Cantidad máxima en segundos de videos mpeg contiene caracteres NO NUMÉRICOS";
                    return false;
            }

            $low_cpu = (isset($_POST['low_cpu']) && trim($_POST['low_cpu'])!=='')? trim($_POST['low_cpu']) : 3600;
            if(!$this->esNumerico($low_cpu)){
                    $this->mensaje = "Frames por seg. a capturar si no hay movimiento contiene caracteres NO NUMÉRICOS";
                    return false;
            }

            return true;
    }

    function sonValidosParametrosConexion(){
            $computador=	(isset($_POST['computador']) && trim($_POST['computador'])!=='')? trim($_POST['computador']) : null;
            if($computador===null){
                    $this->mensaje = "Falta introducir de la Estación de Trabajo";
                    return false;
            }
            if(!$this->esAlfaNumericoConEspacios($computador)){
                    $this->mensaje = "El nombre del Computador introducido no es válido";
                    return false;
            }



            $ipv4=	(isset($_POST['ipv4']) && trim($_POST['ipv4'])!=='')? trim($_POST['ipv4']) : null;
            if($ipv4===null){
                    $this->mensaje = "Falta introducir la Dirección IP-";
                    return false;
            }
            if(!$this->esValidaIP($ipv4)){
                    $this->mensaje = "La Dirección IP introducida no es valido";
                    return false;
            }

            $so_usuario=	(isset($_POST['so_usuario']) && trim($_POST['so_usuario'])!=='')? trim($_POST['so_usuario']) : null;
            if($so_usuario===null){
                    $this->mensaje = "Falta introducir El Usuario de Sistema.";
                    return false;
            }
            if(!$this->esAlfaNumerico($so_usuario)){
                    $this->mensaje = "El Usuario del Sistema tiene caracteres no válidos";
                    return false;
            }


            $so_clave=	(isset($_POST['so_clave']) && trim($_POST['so_clave'])!=='')? trim($_POST['so_clave']) : null;
            if($so_clave===null){
                    $this->mensaje = "Falta introducir la Clave del Usuario de Sistema.";
                    return false;
            }
            if(!$this->esAlfaNumerico($so_clave)){
                    $this->mensaje = "La Clave del Usuario del Sistema tiene caracteres no v&aacute;lidos";
                    return false;
            }
            return true;
    }
    /*
     * Autor David Concepcion 07-06-2010 CENIT
     * Validacion de campos de ubicacion del servidor DVR
     */
    function sonValidosParametrosUbicacion(){
        $preMsj = "Debe introducir toda la informaci&oacute;n en la pestaña Ubicaci&oacute;n<br>";
        $pais = (isset($_POST['pais']) && trim($_POST['pais'])!=='')? trim($_POST['pais']) : null;
        if($pais===null){
            $this->mensaje = $preMsj."Falta introducir el Pa&iacute;s ";
            return false;
        }
        if(!$this->esAlfabetico($pais)){
            $this->mensaje = "El nombre del Pa&iacute;s introducido no es válido";
            return false;
        }

        $estado = (isset($_POST['estado']) && trim($_POST['estado'])!=='')? trim($_POST['estado']) : null;
        if($estado===null){
            $this->mensaje = $preMsj."Falta introducir el Estado";
            return false;
        }
        if(!$this->esAlfabetico($estado)){
            $this->mensaje = "El nombre del Estado introducido no es válido";
            return false;
        }

        $ciudad = (isset($_POST['ciudad']) && trim($_POST['ciudad'])!=='')? trim($_POST['ciudad']) : null;
        if($ciudad===null){
            $this->mensaje = $preMsj."Falta introducir la Ciudad";
            return false;
        }
        if(!$this->esAlfabetico($ciudad)){
            $this->mensaje = "El nombre de la Ciudad introducida no es valido";
            return false;
        }

        $avenida = (isset($_POST['avenida']) && trim($_POST['avenida'])!=='')? trim($_POST['avenida']) : null;
        if($avenida===null){
            $this->mensaje = $preMsj."Falta introducir la Avenida";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($avenida)){
            $this->mensaje = "El nombre de la Avenida introducida no es valido";
            return false;
        }

        $edificio = (isset($_POST['edificio']) && trim($_POST['edificio'])!=='')? trim($_POST['edificio']) : null;
        if($edificio===null){
            $this->mensaje = $preMsj."Falta introducir el Edificio";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($edificio)){
            $this->mensaje = "El nombre del Edificio introducido no es válido";
            return false;
        }

        $piso = (isset($_POST['piso']) && trim($_POST['piso'])!=='')? trim($_POST['piso']) : null;
        if($piso===null){
            $this->mensaje = $preMsj."Falta introducir el Piso";
            return false;
        }
        if(!$this->esAlfaNumerico($piso)){
            $this->mensaje = "El nombre del Piso introducido no es válido";
            return false;
        }

        $oficina = (isset($_POST['oficina']) && trim($_POST['oficina'])!=='')? trim($_POST['oficina']) : null;
        if($oficina===null){
            $this->mensaje = $preMsj."Falta introducir la Oficina";
            return false;
        }
        if(!$this->esAlfaNumerico($oficina)){
            $this->mensaje = "El nombre de la Oficina introducida no es valido";
            return false;
        }

        $idsegmento = (isset($_POST['idsegmento']) && trim($_POST['idsegmento'])!=='')? trim($_POST['idsegmento']) : null;
        if($idsegmento===null){
            $this->mensaje = $preMsj."Falta seleccionar el Segmento";
            return false;
        }
        
        return true;

    }

    /*
     * === Configuracion de servidor de video remoto ===
     * Ejecucion de comandos en servidor remoto
     */
    function setConfiguracionServidor(){
        $this->setContenido(file_get_contents('../motion/motion_template.conf'));

        // --------------------- DATOS DEL SERVIDOR --------- ---------------------------//
        foreach ($this->getDataServer() as $key => $value) {

            $conf = (object) $value;

            //-------- SI LA CAMARA EXISTE SE ESTABLECEN LAS RUTAS DE LAS CAMARAS -------//
            if ($conf->idcamara){
                $numero = $conf->numero;
                if ($numero < 10){
                    $numero = "0".$numero;
                }
                $threads .= "thread /home/".$conf->so_usuario."/motion/thread".$numero.".conf\n";
            }
        }
        $conf->threads = $threads;
        $this->setServerRemoto($conf->ipv4Servidor);
        $this->setUsuarioRemoto($conf->so_usuario);
        $this->setPasswordRemoto($conf->so_clave);
        $this->setConf($conf);
        //echo "<div align='left'><pre>".print_r($conf, true)."</pre></div>";
        // ------------------FIN DATOS DEL SERVIDOR Y CAMARAS ---------------------------//

        
        // --------------------- ELIMIARN ARCHIVOS DE CONFIGURACION ---------------------//
        if ($this->getAccion()=="eliminar"){
            $this->setComandoRemoto("rm ./motion/*.conf");
            $exito = $this->sipSsh();
            if ($exito){
                $this->mensaje = "El archivo de configuraci&oacute;n del grabador de video no se a eliminado";
                return false;
            }
        }

        if ($this->getAccion()!="eliminar"){
            
            $this->setComandoRemoto("mkdir ./motion");
            $exito = $this->sipSsh();
            if ($exito){
                $this->mensaje = "El directorio de configuraci&oacute;n del grabador de video no se a creado";
                return false;
            }
            
            // --------------------- GENERAR ARCHIVOS DE CONFIGURACION ----------------------//
            $this->setFileName("motion_".$conf->idservidor);
            $exito = $this->escribirConfiguracion();
            if (!$exito){
                $this->mensaje = "El archivo de configuraci&oacute;n del grabador de video no se a escrito";
                return false;
            }

            // --------------------- ENVIAR ARCHIVOS DE CONFIGURACION -----------------------//
            $this->setArchivoSubir("../motion/motion_".$conf->idservidor.".conf");
            $exito = $this->sipFtp();
            if ($exito){
                $this->mensaje = "El archivo de configuraci&oacute;n del grabador de video no se a enviado";
                return false;
            }

            // --------------------- COMANDOS REMOTOS ------------------------------------//
            $this->setComandoRemoto("mv ./motion/motion_".$conf->idservidor.".conf ./motion/motion.conf");
            $exito = $this->sipSsh();
            if ($exito){
                $this->mensaje = "El archivo de configuraci&oacute;n del grabador de video no se a renombrado";
                return false;
            }
        }
        // --------------------- REINICIAR SERVICIOS ------------------------------------//
        $this->setComandoRemoto("/etc/init.d/motion stop");
        $this->sipSsh();
        $this->eliminarConfiguracion(); // eliminar el archivo de configuracion generado local
        /*if ($this->getAccion()!="eliminar"){
            $this->setComandoRemoto("motion -c ./motion/motion.conf");
            $this->sipSsh();
        }*/
        return true;
    }

    /*
     * === Detener servicio del servidor de video remoto ===
     * Ejecucion de comandos en servidor remoto
     */
    function detenerServidor(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->esValidoIdServidor()) return false;

        $this->setIdservidor(strtoupper(trim($_POST['idservidor'])));

        
        $servidor = new Servidor();
        $srv = $servidor->getServidor($this->getIdservidor());
        
        $this->setServerRemoto($srv->ipv4);
        $this->setUsuarioRemoto($srv->so_usuario);
        $this->setPasswordRemoto($srv->so_clave);
        $this->setComandoRemoto("/etc/init.d/motion stop");
        $exito = $this->sipSsh();
        if ($exito){
            $this->mensaje = "El servicio del grabador de video no se ha podido detener";
            return false;
        }

        $this->mensaje = "El grabador de video se ha detenido exitosamente...";
        return true;
    }

    /*
     * === Reiniciar servicio del servidor de video remoto ===
     * Ejecucion de comandos en servidor remoto
     */
    function reiniciarServidor(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->esValidoIdServidor()) return false;

        $this->setIdservidor(strtoupper(trim($_POST['idservidor'])));

        
        $servidor = new Servidor();
        $srv = $servidor->getServidor($this->getIdservidor());

        $this->setServerRemoto($srv->ipv4);
        $this->setUsuarioRemoto($srv->so_usuario);
        $this->setPasswordRemoto($srv->so_clave);
        $this->setComandoRemoto("motion -c ./motion/motion.conf");
        $exito = $this->sipSsh();
        if ($exito){
            $this->mensaje = "El servicio del grabador de video no se ha podido reiniciar";
            return false;
        }

        $this->mensaje = "El grabador de video se ha reiniciado exitosamente...";
        return true;
    }
    
    /**
     * Verifica el estado de captura de eventos de una camara
     * @return boolean Devuelve verdadero si la camara esta activa y vis.
     */
    function getMotionStatus(){
        
        $camaras = Servidor::getCamarasServidor($_REQUEST["idservidor"]);
        $nThread = 0;
        foreach ($camaras as $row){
            $nThread++;
            if ($row->idcamara == trim($_REQUEST["idcamara"])){
                // ---------- PAUSA DETECCION DE MOVIMIENTO ------------//
                $url = "http://".$row->ipv4Serv.":18080/".$nThread."/detection/status";
                //echo "<br>".$url;
                $resp = self::httpRequest($url, $row->so_usuario, $row->so_clave);
                break;
            }
        }
        //echo $resp;
        if (preg_match("/ACTIVE/", $resp)){
            return true;
        }
        if (!preg_match("/ACTIVE/", $resp)){
            return false;
        }
    }
    
}?>