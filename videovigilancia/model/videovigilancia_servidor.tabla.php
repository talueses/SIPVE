<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Vid_servidor {
    
    private $idservidor = null;
    private $idsegmento = null;
    private $computador = null;
    private $tiposervidor = null;
    private $ipv4 = null;
    private $enlinea = null;
    private $idtimezone = null;
    private $so_usuario = null;
    private $so_clave = null;
    private $conexioncamaras = null;
    private $pais = null;
    private $estado = null;
    private $ciudad = null;
    private $avenida = null;
    private $edificio = null;
    private $piso = null;
    private $oficina = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdservidor($idservidor){
        $this->idservidor = $idservidor;
    }
    public function getIdservidor(){
        return $this->idservidor;
    }
    public function setIdsegmento($idsegmento){
        $this->idsegmento = $idsegmento;
    }
    public function getIdsegmento(){
        return $this->idsegmento;
    }
    public function setComputador($computador){
        $this->computador = $computador;
    }
    public function getComputador(){
        return $this->computador;
    }
    public function setTiposervidor($tiposervidor){
        $this->tiposervidor = $tiposervidor;
    }
    public function getTiposervidor(){
        return $this->tiposervidor;
    }
    public function setIpv4($ipv4){
        $this->ipv4 = $ipv4;
    }
    public function getIpv4(){
        return $this->ipv4;
    }
    public function setEnlinea($enlinea){
        $this->enlinea = $enlinea;
    }
    public function getEnlinea(){
        return $this->enlinea;
    }
    public function setIdtimezone($idtimezone){
        $this->idtimezone = $idtimezone;
    }
    public function getIdtimezone(){
        return $this->idtimezone;
    }
    public function setSo_usuario($so_usuario){
        $this->so_usuario = $so_usuario;
    }
    public function getSo_usuario(){
        return $this->so_usuario;
    }
    public function setSo_clave($so_clave){
        $this->so_clave = $so_clave;
    }
    public function getSo_clave(){
        return $this->so_clave;
    }
    public function setConexioncamaras($conexioncamaras){
        $this->conexioncamaras = $conexioncamaras;
    }
    public function getConexioncamaras(){
        return $this->conexioncamaras;
    }
    public function setPais($pais){
        $this->pais = $pais;
    }
    public function getPais(){
        return $this->pais;
    }
    public function setEstado($estado){
        $this->estado = $estado;
    }
    public function getEstado(){
        return $this->estado;
    }
    public function setCiudad($ciudad){
        $this->ciudad = $ciudad;
    }
    public function getCiudad(){
        return $this->ciudad;
    }
    public function setAvenida($avenida){
        $this->avenida = $avenida;
    }
    public function getAvenida(){
        return $this->avenida;
    }
    public function setEdificio($edificio){
        $this->edificio = $edificio;
    }
    public function getEdificio(){
        return $this->edificio;
    }
    public function setPiso($piso){
        $this->piso = $piso;
    }
    public function getPiso(){
        return $this->piso;
    }
    public function setOficina($oficina){
        $this->oficina = $oficina;
    }
    public function getOficina(){
        return $this->oficina;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idservidor !== null) && (trim($this->idservidor)!=='') ){
            $campos .= "idservidor,";
            $valores .= "'".$this->idservidor."',";
        }
        if(($this->idsegmento !== null) && (trim($this->idsegmento)!=='') ){
            $campos .= "idsegmento,";
            $valores .= "'".$this->idsegmento."',";
        }
        if(($this->computador !== null) && (trim($this->computador)!=='') ){
            $campos .= "computador,";
            $valores .= "'".$this->computador."',";
        }
        if(($this->tiposervidor !== null) && (trim($this->tiposervidor)!=='') ){
            $campos .= "tiposervidor,";
            $valores .= "'".$this->tiposervidor."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $campos .= "ipv4,";
            $valores .= "'".$this->ipv4."',";
        }
        if(($this->enlinea !== null) && (trim($this->enlinea)!=='') ){
            $campos .= "enlinea,";
            $valores .= "'".$this->enlinea."',";
        }
        if(($this->idtimezone !== null) && (trim($this->idtimezone)!=='') ){
            $campos .= "idtimezone,";
            $valores .= "'".$this->idtimezone."',";
        }
        if(($this->so_usuario !== null) && (trim($this->so_usuario)!=='') ){
            $campos .= "so_usuario,";
            $valores .= "'".$this->so_usuario."',";
        }
        if(($this->so_clave !== null) && (trim($this->so_clave)!=='') ){
            $campos .= "so_clave,";
            $valores .= "'".$this->so_clave."',";
        }
        if(($this->conexioncamaras !== null) && (trim($this->conexioncamaras)!=='') ){
            $campos .= "conexioncamaras,";
            $valores .= "'".$this->conexioncamaras."',";
        }
        if(($this->pais !== null) && (trim($this->pais)!=='') ){
            $campos .= "pais,";
            $valores .= "'".$this->pais."',";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $campos .= "estado,";
            $valores .= "'".$this->estado."',";
        }
        if(($this->ciudad !== null) && (trim($this->ciudad)!=='') ){
            $campos .= "ciudad,";
            $valores .= "'".$this->ciudad."',";
        }
        if(($this->avenida !== null) && (trim($this->avenida)!=='') ){
            $campos .= "avenida,";
            $valores .= "'".$this->avenida."',";
        }
        if(($this->edificio !== null) && (trim($this->edificio)!=='') ){
            $campos .= "edificio,";
            $valores .= "'".$this->edificio."',";
        }
        if(($this->piso !== null) && (trim($this->piso)!=='') ){
            $campos .= "piso,";
            $valores .= "'".$this->piso."',";
        }
        if(($this->oficina !== null) && (trim($this->oficina)!=='') ){
            $campos .= "oficina,";
            $valores .= "'".$this->oficina."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_servidor $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_servidor SET ";
        
        if(($this->idsegmento !== null) && (trim($this->idsegmento)!=='') ){
            $sql .= "idsegmento = '".$this->idsegmento."',";
        }
        if(($this->computador !== null) && (trim($this->computador)!=='') ){
            $sql .= "computador = '".$this->computador."',";
        }
        if(($this->tiposervidor !== null) && (trim($this->tiposervidor)!=='') ){
            $sql .= "tiposervidor = '".$this->tiposervidor."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $sql .= "ipv4 = '".$this->ipv4."',";
        }
        if(($this->enlinea !== null) && (trim($this->enlinea)!=='') ){
            $sql .= "enlinea = '".$this->enlinea."',";
        }
        if(($this->idtimezone !== null) && (trim($this->idtimezone)!=='') ){
            $sql .= "idtimezone = '".$this->idtimezone."',";
        }
        if(($this->so_usuario !== null) && (trim($this->so_usuario)!=='') ){
            $sql .= "so_usuario = '".$this->so_usuario."',";
        }
        if(($this->so_clave !== null) && (trim($this->so_clave)!=='') ){
            $sql .= "so_clave = '".$this->so_clave."',";
        }
        if(($this->conexioncamaras !== null) && (trim($this->conexioncamaras)!=='') ){
            $sql .= "conexioncamaras = '".$this->conexioncamaras."',";
        }
        if(($this->pais !== null) && (trim($this->pais)!=='') ){
            $sql .= "pais = '".$this->pais."',";
        }
        else{
            $sql .= "pais = NULL,";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $sql .= "estado = '".$this->estado."',";
        }
        else{
            $sql .= "estado = NULL,";
        }
        if(($this->ciudad !== null) && (trim($this->ciudad)!=='') ){
            $sql .= "ciudad = '".$this->ciudad."',";
        }
        else{
            $sql .= "ciudad = NULL,";
        }
        if(($this->avenida !== null) && (trim($this->avenida)!=='') ){
            $sql .= "avenida = '".$this->avenida."',";
        }
        else{
            $sql .= "avenida = NULL,";
        }
        if(($this->edificio !== null) && (trim($this->edificio)!=='') ){
            $sql .= "edificio = '".$this->edificio."',";
        }
        else{
            $sql .= "edificio = NULL,";
        }
        if(($this->piso !== null) && (trim($this->piso)!=='') ){
            $sql .= "piso = '".$this->piso."',";
        }
        else{
            $sql .= "piso = NULL,";
        }
        if(($this->oficina !== null) && (trim($this->oficina)!=='') ){
            $sql .= "oficina = '".$this->oficina."',";
        }
        else{
            $sql .= "oficina = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idservidor = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_servidor  WHERE idservidor = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidServidors
     * @return object Devuelve un registro como objeto
     */
    function  getVidServidors(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idservidorBuscador"]){
            $in = null;
            foreach ($_REQUEST["idservidorBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idservidor in (".$in.")";
        }
        
        $sql = "select idservidor,idsegmento,computador,tiposervidor,ipv4,enlinea,idtimezone,so_usuario,so_clave,conexioncamaras,pais,estado,ciudad,avenida,edificio,piso,oficina from vid_servidor ".$arg." order by computador,ipv4";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidServidor
     * @param int $idservidor Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidServidor($idservidor){
        $sql = "SELECT idservidor,idsegmento,computador,tiposervidor,ipv4,enlinea,idtimezone,so_usuario,so_clave,conexioncamaras,pais,estado,ciudad,avenida,edificio,piso,oficina FROM vid_servidor WHERE idservidor = '".$idservidor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>