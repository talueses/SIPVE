<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Modificado por David Concepcion 16-09-2010 CENIT
 * Metodos nuevos
 *  => getConfiguracionServidor(), eliminarCamaras():
 *
 * Modificado por David Concepcion 21-09-2010 CENIT
 * getConfiguracionServidor()
 *  => Order by en consulta
 */
require_once "videovigilancia_servidor.tabla.php";  
class Servidor extends Vid_servidor{	

    /**
     * Consulta de servidores
     * Solo se listan los servidores pertenecientes al segmento del usuario logueado
     * @return object Devuelve registros como arreglo de objetos
     */
    function getServidores(){
        $ret = array();

        // --- Valores del Buscador --- //
        if ($_REQUEST["idservidorBuscador"]){
            $in = null;
            foreach ($_REQUEST["idservidorBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and idservidor in (".$in.")";
        }

        $sql = "SELECT /*start*/ * /*end*/ FROM vid_servidor where idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') ".$arg;
        //echo "<div align='left'><pre>".paginationSQL::setSql($sql)."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de un servidor
     * @param int $idservidor codigo del servidor
     * @return object Devuelve un registro como arreglo de objetos
     */
    function getServidor($idservidor){
        $sql = "SELECT * FROM vid_servidor WHERE idservidor='$idservidor'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }

    /**
     * Consulta de datos basicos y configuracion servidor de video con sus camaras asociadas
     * @param array $arg arreglo de datos: codigo del servidor
     * @return object Devuelve registros como arreglo de objetos
     */
    function getConfiguracionServidor($arg){
        $sql = "SELECT vsc.*,
                       vsc.ipv4 as \"ipv4Servidor\",
                       vs.idtimezone,
                       vs.so_usuario,
                       vs.so_clave,
                       vc.idcamara,
                       vc.ipv4 as \"ipv4Camara\",
                       vc.numero,
                       vc.camara
                FROM vid_servidor vs
                        left join vid_camara vc on (vs.idservidor = vc.idservidor ),
                    vid_motionconf vsc
                WHERE vs.idservidor = ".$arg["idservidor"]."
                and vs.idservidor = vsc.idservidor
                order by vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Eliminar camaras asociadas al servidor de video
     * @param int $idservidor codigo del servidor
     * @return Boolean Devuelve falso si la ejecucion SQL falla
     */
    function eliminarCamaras($idservidor){
        $sql = "DELETE FROM  vid_camara WHERE idservidor='".$idservidor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }
    
    /**
     * Instanciacion de metodo busca_valor() en clase Controller{} (../controller/videovigilancia_controller.php)
     * @param string $equal valor a comparar
     * @param string $valor valor a comparar
     * @return string Devuelve SELECTED si los valores $equal y $valor son iguales
     */
    function busca_valor($equal, $valor){
        require_once "../controller/videovigilancia_controller.php";
        $obj = new Controller();
        return $obj->busca_valor($equal, $valor);
    }

    /**
     * Instanciacion de metodo makeCombo() en clase Controller{} (../controller/videovigilancia_controller.php)
     * @param string $tabla tabla a consultar en base de datos
     * @param string $arg argumento consulta de base de datos
     * @param string $value campo de base de datos para valor de la opcion
     * @param string $descripcion campo de base de datos para descripcion que se muestra
     * @param string $equal valor a comparar por metodo busca_valor()
     * @param boolean $group valor de etiqueta de agrupacion <optgroup>
     * @return string Devuelve opciones de listas dinamicas HTML
     */
    function makeCombo($tabla, $arg, $value, $descripcion, $equal, $group){
        require_once "../controller/videovigilancia_controller.php";
        $obj = new Controller();
        return $obj->make_combo($tabla, $arg, $value, $descripcion, $equal, $group);
    }



    /**
     * Consulta de Plantas Fisicas de un servidor de video NVR
     * @param string $idservidor Codigo del Servidor
     * @return object Devuelve registros como objeto
     */
    function getPlantasFisicas($idservidor){
        $sql = "select pf.*
                from vid_camara vc,
                     vid_zona_camara vzc,
                     vid_plantafisica_zona vpz,
                     plantafisica pf
                where vc.idservidor = '".$idservidor."'
                  and vc.idcamara        =vzc.idcamara
                  and vzc.idzona         = vpz.idzona
                  and vpz.idplantafisica = pf.idplantafisica
                   group by pf.idplantafisica";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de zonas y camaras asociadas a una planta fisica del un servidor
     * @param string $idservidor Codigo del Servidor
     * @param int $idplantafisica codigo de la planta fisica
     * @return object Devuelve registros como objeto
     */
    static function getZonasCamarasPlantafisica($idservidor,$idplantafisica){
        $sql = "select pf.plantafisica,
                       pf.file,
                       vpz.leftX as \"leftXZona\",
                       vpz.topY as \"topYZona\",
                       vpz.width,
                       vpz.height,
                       vpz.bgcolor,
                       vz.idzona,
                       vz.zona,
                       vz.descripcion,
                       vc.idcamara,
                       vc.camara,
                       vc.numero,
                       vc.idplantafisica as \"idplantafisicaCamara\",
                       vc.leftX as \"leftXCamara\",
                       vc.topY as \"topYCamara\"
                from plantafisica pf,
                     vid_plantafisica_zona vpz,
                     vid_zona vz,
                     vid_zona_camara vzc,
                     vid_camara vc
                where pf.idplantafisica = '".$idplantafisica."'
                  and pf.idplantafisica = vpz.idplantafisica
                  and vpz.idzona        = vz.idzona
                  and vz.idzona         = vzc.idzona
                  and vzc.idcamara      = vc.idcamara
                  and vc.idservidor     = '".$idservidor."'
                      ";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de camaras asociadas a un servidor ordenada por numero
     * @param string $idservidor Codigo del servidor
     * @return object Devuelve registros como objeto
     */
    static function getCamarasServidor($idservidor){
        $sql = "select vs.idservidor,
                       vc.idcamara,
                       vs.ipv4 as \"ipv4Serv\",
                       vs.so_usuario,
                       vs.so_clave,
                       vc.numero
                from vid_camara vc,
                     vid_servidor vs
                where vc.idservidor = '".$idservidor."'
                  and vc.idservidor = vs.idservidor
                  order by vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
}
?>
