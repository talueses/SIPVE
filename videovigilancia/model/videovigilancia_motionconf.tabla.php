<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Vid_motionconf {
    
    private $idservidor = null;
    private $ipv4 = null;
    private $daemon = null;
    private $process_id_file = null;
    private $setup_mode = null;
    private $videodevice = null;
    private $tunerdevice = null;
    private $input = null;
    private $norm = null;
    private $frequency = null;
    private $rotate = null;
    private $width = null;
    private $height = null;
    private $framerate = null;
    private $minimum_frame_time = null;
    private $netcam_url = null;
    private $netcam_userpass = null;
    private $netcam_proxy = null;
    private $auto_brightness = null;
    private $brightness = null;
    private $contrast = null;
    private $saturation = null;
    private $hue = null;
    private $roundrobin_frames = null;
    private $roundrobin_skip = null;
    private $switchfilter = null;
    private $threshold = null;
    private $threshold_tune = null;
    private $noise_level = null;
    private $noise_tune = null;
    private $night_compensate = null;
    private $despeckle = null;
    private $mask_file = null;
    private $smart_mask_speed = null;
    private $lightswitch = null;
    private $minimum_motion_frames = null;
    private $pre_capture = null;
    private $post_capture = null;
    private $gap = null;
    private $max_mpeg_time = null;
    private $low_cpu = null;
    private $output_all = null;
    private $output_normal = null;
    private $output_motion = null;
    private $quality = null;
    private $ppm = null;
    private $ffmpeg_cap_new = null;
    private $ffmpeg_cap_motion = null;
    private $ffmpeg_timelapse = null;
    private $ffmpeg_timelapse_mode = null;
    private $ffmpeg_bps = null;
    private $ffmpeg_variable_bitrate = null;
    private $ffmpeg_video_codec = null;
    private $ffmpeg_deinterlace = null;
    private $snapshot_interval = null;
    private $locate = null;
    private $text_right = null;
    private $text_left = null;
    private $text_changes = null;
    private $text_event = null;
    private $text_double = null;
    private $target_dir = null;
    private $snapshot_filename = null;
    private $jpeg_filename = null;
    private $movie_filename = null;
    private $timelapse_filename = null;
    private $webcam_port = null;
    private $webcam_quality = null;
    private $webcam_motion = null;
    private $webcam_maxrate = null;
    private $webcam_localhost = null;
    private $webcam_limit = null;
    private $control_port = null;
    private $control_localhost = null;
    private $control_html_output = null;
    private $control_authentication = null;
    private $track_type = null;
    private $track_auto = null;
    private $track_port = null;
    private $track_motorx = null;
    private $track_motory = null;
    private $track_maxx = null;
    private $track_maxy = null;
    private $track_iomojo_id = null;
    private $track_step_angle_x = null;
    private $track_step_angle_y = null;
    private $track_move_wait = null;
    private $track_speed = null;
    private $track_stepsize = null;
    private $quiet = null;
    private $on_event_start = null;
    private $on_event_end = null;
    private $on_picture_save = null;
    private $on_motion_detected = null;
    private $on_movie_start = null;
    private $on_movie_end = null;
    private $video_pipe = null;
    private $motion_video_pipe = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdservidor($idservidor){
        $this->idservidor = $idservidor;
    }
    public function getIdservidor(){
        return $this->idservidor;
    }
    public function setIpv4($ipv4){
        $this->ipv4 = $ipv4;
    }
    public function getIpv4(){
        return $this->ipv4;
    }
    public function setDaemon($daemon){
        $this->daemon = $daemon;
    }
    public function getDaemon(){
        return $this->daemon;
    }
    public function setProcess_id_file($process_id_file){
        $this->process_id_file = $process_id_file;
    }
    public function getProcess_id_file(){
        return $this->process_id_file;
    }
    public function setSetup_mode($setup_mode){
        $this->setup_mode = $setup_mode;
    }
    public function getSetup_mode(){
        return $this->setup_mode;
    }
    public function setVideodevice($videodevice){
        $this->videodevice = $videodevice;
    }
    public function getVideodevice(){
        return $this->videodevice;
    }
    public function setTunerdevice($tunerdevice){
        $this->tunerdevice = $tunerdevice;
    }
    public function getTunerdevice(){
        return $this->tunerdevice;
    }
    public function setInput($input){
        $this->input = $input;
    }
    public function getInput(){
        return $this->input;
    }
    public function setNorm($norm){
        $this->norm = $norm;
    }
    public function getNorm(){
        return $this->norm;
    }
    public function setFrequency($frequency){
        $this->frequency = $frequency;
    }
    public function getFrequency(){
        return $this->frequency;
    }
    public function setRotate($rotate){
        $this->rotate = $rotate;
    }
    public function getRotate(){
        return $this->rotate;
    }
    public function setWidth($width){
        $this->width = $width;
    }
    public function getWidth(){
        return $this->width;
    }
    public function setHeight($height){
        $this->height = $height;
    }
    public function getHeight(){
        return $this->height;
    }
    public function setFramerate($framerate){
        $this->framerate = $framerate;
    }
    public function getFramerate(){
        return $this->framerate;
    }
    public function setMinimum_frame_time($minimum_frame_time){
        $this->minimum_frame_time = $minimum_frame_time;
    }
    public function getMinimum_frame_time(){
        return $this->minimum_frame_time;
    }
    public function setNetcam_url($netcam_url){
        $this->netcam_url = $netcam_url;
    }
    public function getNetcam_url(){
        return $this->netcam_url;
    }
    public function setNetcam_userpass($netcam_userpass){
        $this->netcam_userpass = $netcam_userpass;
    }
    public function getNetcam_userpass(){
        return $this->netcam_userpass;
    }
    public function setNetcam_proxy($netcam_proxy){
        $this->netcam_proxy = $netcam_proxy;
    }
    public function getNetcam_proxy(){
        return $this->netcam_proxy;
    }
    public function setAuto_brightness($auto_brightness){
        $this->auto_brightness = $auto_brightness;
    }
    public function getAuto_brightness(){
        return $this->auto_brightness;
    }
    public function setBrightness($brightness){
        $this->brightness = $brightness;
    }
    public function getBrightness(){
        return $this->brightness;
    }
    public function setContrast($contrast){
        $this->contrast = $contrast;
    }
    public function getContrast(){
        return $this->contrast;
    }
    public function setSaturation($saturation){
        $this->saturation = $saturation;
    }
    public function getSaturation(){
        return $this->saturation;
    }
    public function setHue($hue){
        $this->hue = $hue;
    }
    public function getHue(){
        return $this->hue;
    }
    public function setRoundrobin_frames($roundrobin_frames){
        $this->roundrobin_frames = $roundrobin_frames;
    }
    public function getRoundrobin_frames(){
        return $this->roundrobin_frames;
    }
    public function setRoundrobin_skip($roundrobin_skip){
        $this->roundrobin_skip = $roundrobin_skip;
    }
    public function getRoundrobin_skip(){
        return $this->roundrobin_skip;
    }
    public function setSwitchfilter($switchfilter){
        $this->switchfilter = $switchfilter;
    }
    public function getSwitchfilter(){
        return $this->switchfilter;
    }
    public function setThreshold($threshold){
        $this->threshold = $threshold;
    }
    public function getThreshold(){
        return $this->threshold;
    }
    public function setThreshold_tune($threshold_tune){
        $this->threshold_tune = $threshold_tune;
    }
    public function getThreshold_tune(){
        return $this->threshold_tune;
    }
    public function setNoise_level($noise_level){
        $this->noise_level = $noise_level;
    }
    public function getNoise_level(){
        return $this->noise_level;
    }
    public function setNoise_tune($noise_tune){
        $this->noise_tune = $noise_tune;
    }
    public function getNoise_tune(){
        return $this->noise_tune;
    }
    public function setNight_compensate($night_compensate){
        $this->night_compensate = $night_compensate;
    }
    public function getNight_compensate(){
        return $this->night_compensate;
    }
    public function setDespeckle($despeckle){
        $this->despeckle = $despeckle;
    }
    public function getDespeckle(){
        return $this->despeckle;
    }
    public function setMask_file($mask_file){
        $this->mask_file = $mask_file;
    }
    public function getMask_file(){
        return $this->mask_file;
    }
    public function setSmart_mask_speed($smart_mask_speed){
        $this->smart_mask_speed = $smart_mask_speed;
    }
    public function getSmart_mask_speed(){
        return $this->smart_mask_speed;
    }
    public function setLightswitch($lightswitch){
        $this->lightswitch = $lightswitch;
    }
    public function getLightswitch(){
        return $this->lightswitch;
    }
    public function setMinimum_motion_frames($minimum_motion_frames){
        $this->minimum_motion_frames = $minimum_motion_frames;
    }
    public function getMinimum_motion_frames(){
        return $this->minimum_motion_frames;
    }
    public function setPre_capture($pre_capture){
        $this->pre_capture = $pre_capture;
    }
    public function getPre_capture(){
        return $this->pre_capture;
    }
    public function setPost_capture($post_capture){
        $this->post_capture = $post_capture;
    }
    public function getPost_capture(){
        return $this->post_capture;
    }
    public function setGap($gap){
        $this->gap = $gap;
    }
    public function getGap(){
        return $this->gap;
    }
    public function setMax_mpeg_time($max_mpeg_time){
        $this->max_mpeg_time = $max_mpeg_time;
    }
    public function getMax_mpeg_time(){
        return $this->max_mpeg_time;
    }
    public function setLow_cpu($low_cpu){
        $this->low_cpu = $low_cpu;
    }
    public function getLow_cpu(){
        return $this->low_cpu;
    }
    public function setOutput_all($output_all){
        $this->output_all = $output_all;
    }
    public function getOutput_all(){
        return $this->output_all;
    }
    public function setOutput_normal($output_normal){
        $this->output_normal = $output_normal;
    }
    public function getOutput_normal(){
        return $this->output_normal;
    }
    public function setOutput_motion($output_motion){
        $this->output_motion = $output_motion;
    }
    public function getOutput_motion(){
        return $this->output_motion;
    }
    public function setQuality($quality){
        $this->quality = $quality;
    }
    public function getQuality(){
        return $this->quality;
    }
    public function setPpm($ppm){
        $this->ppm = $ppm;
    }
    public function getPpm(){
        return $this->ppm;
    }
    public function setFfmpeg_cap_new($ffmpeg_cap_new){
        $this->ffmpeg_cap_new = $ffmpeg_cap_new;
    }
    public function getFfmpeg_cap_new(){
        return $this->ffmpeg_cap_new;
    }
    public function setFfmpeg_cap_motion($ffmpeg_cap_motion){
        $this->ffmpeg_cap_motion = $ffmpeg_cap_motion;
    }
    public function getFfmpeg_cap_motion(){
        return $this->ffmpeg_cap_motion;
    }
    public function setFfmpeg_timelapse($ffmpeg_timelapse){
        $this->ffmpeg_timelapse = $ffmpeg_timelapse;
    }
    public function getFfmpeg_timelapse(){
        return $this->ffmpeg_timelapse;
    }
    public function setFfmpeg_timelapse_mode($ffmpeg_timelapse_mode){
        $this->ffmpeg_timelapse_mode = $ffmpeg_timelapse_mode;
    }
    public function getFfmpeg_timelapse_mode(){
        return $this->ffmpeg_timelapse_mode;
    }
    public function setFfmpeg_bps($ffmpeg_bps){
        $this->ffmpeg_bps = $ffmpeg_bps;
    }
    public function getFfmpeg_bps(){
        return $this->ffmpeg_bps;
    }
    public function setFfmpeg_variable_bitrate($ffmpeg_variable_bitrate){
        $this->ffmpeg_variable_bitrate = $ffmpeg_variable_bitrate;
    }
    public function getFfmpeg_variable_bitrate(){
        return $this->ffmpeg_variable_bitrate;
    }
    public function setFfmpeg_video_codec($ffmpeg_video_codec){
        $this->ffmpeg_video_codec = $ffmpeg_video_codec;
    }
    public function getFfmpeg_video_codec(){
        return $this->ffmpeg_video_codec;
    }
    public function setFfmpeg_deinterlace($ffmpeg_deinterlace){
        $this->ffmpeg_deinterlace = $ffmpeg_deinterlace;
    }
    public function getFfmpeg_deinterlace(){
        return $this->ffmpeg_deinterlace;
    }
    public function setSnapshot_interval($snapshot_interval){
        $this->snapshot_interval = $snapshot_interval;
    }
    public function getSnapshot_interval(){
        return $this->snapshot_interval;
    }
    public function setLocate($locate){
        $this->locate = $locate;
    }
    public function getLocate(){
        return $this->locate;
    }
    public function setText_right($text_right){
        $this->text_right = $text_right;
    }
    public function getText_right(){
        return $this->text_right;
    }
    public function setText_left($text_left){
        $this->text_left = $text_left;
    }
    public function getText_left(){
        return $this->text_left;
    }
    public function setText_changes($text_changes){
        $this->text_changes = $text_changes;
    }
    public function getText_changes(){
        return $this->text_changes;
    }
    public function setText_event($text_event){
        $this->text_event = $text_event;
    }
    public function getText_event(){
        return $this->text_event;
    }
    public function setText_double($text_double){
        $this->text_double = $text_double;
    }
    public function getText_double(){
        return $this->text_double;
    }
    public function setTarget_dir($target_dir){
        $this->target_dir = $target_dir;
    }
    public function getTarget_dir(){
        return $this->target_dir;
    }
    public function setSnapshot_filename($snapshot_filename){
        $this->snapshot_filename = $snapshot_filename;
    }
    public function getSnapshot_filename(){
        return $this->snapshot_filename;
    }
    public function setJpeg_filename($jpeg_filename){
        $this->jpeg_filename = $jpeg_filename;
    }
    public function getJpeg_filename(){
        return $this->jpeg_filename;
    }
    public function setMovie_filename($movie_filename){
        $this->movie_filename = $movie_filename;
    }
    public function getMovie_filename(){
        return $this->movie_filename;
    }
    public function setTimelapse_filename($timelapse_filename){
        $this->timelapse_filename = $timelapse_filename;
    }
    public function getTimelapse_filename(){
        return $this->timelapse_filename;
    }
    public function setWebcam_port($webcam_port){
        $this->webcam_port = $webcam_port;
    }
    public function getWebcam_port(){
        return $this->webcam_port;
    }
    public function setWebcam_quality($webcam_quality){
        $this->webcam_quality = $webcam_quality;
    }
    public function getWebcam_quality(){
        return $this->webcam_quality;
    }
    public function setWebcam_motion($webcam_motion){
        $this->webcam_motion = $webcam_motion;
    }
    public function getWebcam_motion(){
        return $this->webcam_motion;
    }
    public function setWebcam_maxrate($webcam_maxrate){
        $this->webcam_maxrate = $webcam_maxrate;
    }
    public function getWebcam_maxrate(){
        return $this->webcam_maxrate;
    }
    public function setWebcam_localhost($webcam_localhost){
        $this->webcam_localhost = $webcam_localhost;
    }
    public function getWebcam_localhost(){
        return $this->webcam_localhost;
    }
    public function setWebcam_limit($webcam_limit){
        $this->webcam_limit = $webcam_limit;
    }
    public function getWebcam_limit(){
        return $this->webcam_limit;
    }
    public function setControl_port($control_port){
        $this->control_port = $control_port;
    }
    public function getControl_port(){
        return $this->control_port;
    }
    public function setControl_localhost($control_localhost){
        $this->control_localhost = $control_localhost;
    }
    public function getControl_localhost(){
        return $this->control_localhost;
    }
    public function setControl_html_output($control_html_output){
        $this->control_html_output = $control_html_output;
    }
    public function getControl_html_output(){
        return $this->control_html_output;
    }
    public function setControl_authentication($control_authentication){
        $this->control_authentication = $control_authentication;
    }
    public function getControl_authentication(){
        return $this->control_authentication;
    }
    public function setTrack_type($track_type){
        $this->track_type = $track_type;
    }
    public function getTrack_type(){
        return $this->track_type;
    }
    public function setTrack_auto($track_auto){
        $this->track_auto = $track_auto;
    }
    public function getTrack_auto(){
        return $this->track_auto;
    }
    public function setTrack_port($track_port){
        $this->track_port = $track_port;
    }
    public function getTrack_port(){
        return $this->track_port;
    }
    public function setTrack_motorx($track_motorx){
        $this->track_motorx = $track_motorx;
    }
    public function getTrack_motorx(){
        return $this->track_motorx;
    }
    public function setTrack_motory($track_motory){
        $this->track_motory = $track_motory;
    }
    public function getTrack_motory(){
        return $this->track_motory;
    }
    public function setTrack_maxx($track_maxx){
        $this->track_maxx = $track_maxx;
    }
    public function getTrack_maxx(){
        return $this->track_maxx;
    }
    public function setTrack_maxy($track_maxy){
        $this->track_maxy = $track_maxy;
    }
    public function getTrack_maxy(){
        return $this->track_maxy;
    }
    public function setTrack_iomojo_id($track_iomojo_id){
        $this->track_iomojo_id = $track_iomojo_id;
    }
    public function getTrack_iomojo_id(){
        return $this->track_iomojo_id;
    }
    public function setTrack_step_angle_x($track_step_angle_x){
        $this->track_step_angle_x = $track_step_angle_x;
    }
    public function getTrack_step_angle_x(){
        return $this->track_step_angle_x;
    }
    public function setTrack_step_angle_y($track_step_angle_y){
        $this->track_step_angle_y = $track_step_angle_y;
    }
    public function getTrack_step_angle_y(){
        return $this->track_step_angle_y;
    }
    public function setTrack_move_wait($track_move_wait){
        $this->track_move_wait = $track_move_wait;
    }
    public function getTrack_move_wait(){
        return $this->track_move_wait;
    }
    public function setTrack_speed($track_speed){
        $this->track_speed = $track_speed;
    }
    public function getTrack_speed(){
        return $this->track_speed;
    }
    public function setTrack_stepsize($track_stepsize){
        $this->track_stepsize = $track_stepsize;
    }
    public function getTrack_stepsize(){
        return $this->track_stepsize;
    }
    public function setQuiet($quiet){
        $this->quiet = $quiet;
    }
    public function getQuiet(){
        return $this->quiet;
    }
    public function setOn_event_start($on_event_start){
        $this->on_event_start = $on_event_start;
    }
    public function getOn_event_start(){
        return $this->on_event_start;
    }
    public function setOn_event_end($on_event_end){
        $this->on_event_end = $on_event_end;
    }
    public function getOn_event_end(){
        return $this->on_event_end;
    }
    public function setOn_picture_save($on_picture_save){
        $this->on_picture_save = $on_picture_save;
    }
    public function getOn_picture_save(){
        return $this->on_picture_save;
    }
    public function setOn_motion_detected($on_motion_detected){
        $this->on_motion_detected = $on_motion_detected;
    }
    public function getOn_motion_detected(){
        return $this->on_motion_detected;
    }
    public function setOn_movie_start($on_movie_start){
        $this->on_movie_start = $on_movie_start;
    }
    public function getOn_movie_start(){
        return $this->on_movie_start;
    }
    public function setOn_movie_end($on_movie_end){
        $this->on_movie_end = $on_movie_end;
    }
    public function getOn_movie_end(){
        return $this->on_movie_end;
    }
    public function setVideo_pipe($video_pipe){
        $this->video_pipe = $video_pipe;
    }
    public function getVideo_pipe(){
        return $this->video_pipe;
    }
    public function setMotion_video_pipe($motion_video_pipe){
        $this->motion_video_pipe = $motion_video_pipe;
    }
    public function getMotion_video_pipe(){
        return $this->motion_video_pipe;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idservidor !== null) && (trim($this->idservidor)!=='') ){
            $campos .= "idservidor,";
            $valores .= "'".$this->idservidor."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $campos .= "ipv4,";
            $valores .= "'".$this->ipv4."',";
        }
        if(($this->daemon !== null) && (trim($this->daemon)!=='') ){
            $campos .= "daemon,";
            $valores .= "'".$this->daemon."',";
        }
        if(($this->process_id_file !== null) && (trim($this->process_id_file)!=='') ){
            $campos .= "process_id_file,";
            $valores .= "'".$this->process_id_file."',";
        }
        if(($this->setup_mode !== null) && (trim($this->setup_mode)!=='') ){
            $campos .= "setup_mode,";
            $valores .= "'".$this->setup_mode."',";
        }
        if(($this->videodevice !== null) && (trim($this->videodevice)!=='') ){
            $campos .= "videodevice,";
            $valores .= "'".$this->videodevice."',";
        }
        if(($this->tunerdevice !== null) && (trim($this->tunerdevice)!=='') ){
            $campos .= "tunerdevice,";
            $valores .= "'".$this->tunerdevice."',";
        }
        if(($this->input !== null) && (trim($this->input)!=='') ){
            $campos .= "input,";
            $valores .= "'".$this->input."',";
        }
        if(($this->norm !== null) && (trim($this->norm)!=='') ){
            $campos .= "norm,";
            $valores .= "'".$this->norm."',";
        }
        if(($this->frequency !== null) && (trim($this->frequency)!=='') ){
            $campos .= "frequency,";
            $valores .= "'".$this->frequency."',";
        }
        if(($this->rotate !== null) && (trim($this->rotate)!=='') ){
            $campos .= "rotate,";
            $valores .= "'".$this->rotate."',";
        }
        if(($this->width !== null) && (trim($this->width)!=='') ){
            $campos .= "width,";
            $valores .= "'".$this->width."',";
        }
        if(($this->height !== null) && (trim($this->height)!=='') ){
            $campos .= "height,";
            $valores .= "'".$this->height."',";
        }
        if(($this->framerate !== null) && (trim($this->framerate)!=='') ){
            $campos .= "framerate,";
            $valores .= "'".$this->framerate."',";
        }
        if(($this->minimum_frame_time !== null) && (trim($this->minimum_frame_time)!=='') ){
            $campos .= "minimum_frame_time,";
            $valores .= "'".$this->minimum_frame_time."',";
        }
        if(($this->netcam_url !== null) && (trim($this->netcam_url)!=='') ){
            $campos .= "netcam_url,";
            $valores .= "'".$this->netcam_url."',";
        }
        if(($this->netcam_userpass !== null) && (trim($this->netcam_userpass)!=='') ){
            $campos .= "netcam_userpass,";
            $valores .= "'".$this->netcam_userpass."',";
        }
        if(($this->netcam_proxy !== null) && (trim($this->netcam_proxy)!=='') ){
            $campos .= "netcam_proxy,";
            $valores .= "'".$this->netcam_proxy."',";
        }
        if(($this->auto_brightness !== null) && (trim($this->auto_brightness)!=='') ){
            $campos .= "auto_brightness,";
            $valores .= "'".$this->auto_brightness."',";
        }
        if(($this->brightness !== null) && (trim($this->brightness)!=='') ){
            $campos .= "brightness,";
            $valores .= "'".$this->brightness."',";
        }
        if(($this->contrast !== null) && (trim($this->contrast)!=='') ){
            $campos .= "contrast,";
            $valores .= "'".$this->contrast."',";
        }
        if(($this->saturation !== null) && (trim($this->saturation)!=='') ){
            $campos .= "saturation,";
            $valores .= "'".$this->saturation."',";
        }
        if(($this->hue !== null) && (trim($this->hue)!=='') ){
            $campos .= "hue,";
            $valores .= "'".$this->hue."',";
        }
        if(($this->roundrobin_frames !== null) && (trim($this->roundrobin_frames)!=='') ){
            $campos .= "roundrobin_frames,";
            $valores .= "'".$this->roundrobin_frames."',";
        }
        if(($this->roundrobin_skip !== null) && (trim($this->roundrobin_skip)!=='') ){
            $campos .= "roundrobin_skip,";
            $valores .= "'".$this->roundrobin_skip."',";
        }
        if(($this->switchfilter !== null) && (trim($this->switchfilter)!=='') ){
            $campos .= "switchfilter,";
            $valores .= "'".$this->switchfilter."',";
        }
        if(($this->threshold !== null) && (trim($this->threshold)!=='') ){
            $campos .= "threshold,";
            $valores .= "'".$this->threshold."',";
        }
        if(($this->threshold_tune !== null) && (trim($this->threshold_tune)!=='') ){
            $campos .= "threshold_tune,";
            $valores .= "'".$this->threshold_tune."',";
        }
        if(($this->noise_level !== null) && (trim($this->noise_level)!=='') ){
            $campos .= "noise_level,";
            $valores .= "'".$this->noise_level."',";
        }
        if(($this->noise_tune !== null) && (trim($this->noise_tune)!=='') ){
            $campos .= "noise_tune,";
            $valores .= "'".$this->noise_tune."',";
        }
        if(($this->night_compensate !== null) && (trim($this->night_compensate)!=='') ){
            $campos .= "night_compensate,";
            $valores .= "'".$this->night_compensate."',";
        }
        if(($this->despeckle !== null) && (trim($this->despeckle)!=='') ){
            $campos .= "despeckle,";
            $valores .= "'".$this->despeckle."',";
        }
        if(($this->mask_file !== null) && (trim($this->mask_file)!=='') ){
            $campos .= "mask_file,";
            $valores .= "'".$this->mask_file."',";
        }
        if(($this->smart_mask_speed !== null) && (trim($this->smart_mask_speed)!=='') ){
            $campos .= "smart_mask_speed,";
            $valores .= "'".$this->smart_mask_speed."',";
        }
        if(($this->lightswitch !== null) && (trim($this->lightswitch)!=='') ){
            $campos .= "lightswitch,";
            $valores .= "'".$this->lightswitch."',";
        }
        if(($this->minimum_motion_frames !== null) && (trim($this->minimum_motion_frames)!=='') ){
            $campos .= "minimum_motion_frames,";
            $valores .= "'".$this->minimum_motion_frames."',";
        }
        if(($this->pre_capture !== null) && (trim($this->pre_capture)!=='') ){
            $campos .= "pre_capture,";
            $valores .= "'".$this->pre_capture."',";
        }
        if(($this->post_capture !== null) && (trim($this->post_capture)!=='') ){
            $campos .= "post_capture,";
            $valores .= "'".$this->post_capture."',";
        }
        if(($this->gap !== null) && (trim($this->gap)!=='') ){
            $campos .= "gap,";
            $valores .= "'".$this->gap."',";
        }
        if(($this->max_mpeg_time !== null) && (trim($this->max_mpeg_time)!=='') ){
            $campos .= "max_mpeg_time,";
            $valores .= "'".$this->max_mpeg_time."',";
        }
        if(($this->low_cpu !== null) && (trim($this->low_cpu)!=='') ){
            $campos .= "low_cpu,";
            $valores .= "'".$this->low_cpu."',";
        }
        if(($this->output_all !== null) && (trim($this->output_all)!=='') ){
            $campos .= "output_all,";
            $valores .= "'".$this->output_all."',";
        }
        if(($this->output_normal !== null) && (trim($this->output_normal)!=='') ){
            $campos .= "output_normal,";
            $valores .= "'".$this->output_normal."',";
        }
        if(($this->output_motion !== null) && (trim($this->output_motion)!=='') ){
            $campos .= "output_motion,";
            $valores .= "'".$this->output_motion."',";
        }
        if(($this->quality !== null) && (trim($this->quality)!=='') ){
            $campos .= "quality,";
            $valores .= "'".$this->quality."',";
        }
        if(($this->ppm !== null) && (trim($this->ppm)!=='') ){
            $campos .= "ppm,";
            $valores .= "'".$this->ppm."',";
        }
        if(($this->ffmpeg_cap_new !== null) && (trim($this->ffmpeg_cap_new)!=='') ){
            $campos .= "ffmpeg_cap_new,";
            $valores .= "'".$this->ffmpeg_cap_new."',";
        }
        if(($this->ffmpeg_cap_motion !== null) && (trim($this->ffmpeg_cap_motion)!=='') ){
            $campos .= "ffmpeg_cap_motion,";
            $valores .= "'".$this->ffmpeg_cap_motion."',";
        }
        if(($this->ffmpeg_timelapse !== null) && (trim($this->ffmpeg_timelapse)!=='') ){
            $campos .= "ffmpeg_timelapse,";
            $valores .= "'".$this->ffmpeg_timelapse."',";
        }
        if(($this->ffmpeg_timelapse_mode !== null) && (trim($this->ffmpeg_timelapse_mode)!=='') ){
            $campos .= "ffmpeg_timelapse_mode,";
            $valores .= "'".$this->ffmpeg_timelapse_mode."',";
        }
        if(($this->ffmpeg_bps !== null) && (trim($this->ffmpeg_bps)!=='') ){
            $campos .= "ffmpeg_bps,";
            $valores .= "'".$this->ffmpeg_bps."',";
        }
        if(($this->ffmpeg_variable_bitrate !== null) && (trim($this->ffmpeg_variable_bitrate)!=='') ){
            $campos .= "ffmpeg_variable_bitrate,";
            $valores .= "'".$this->ffmpeg_variable_bitrate."',";
        }
        if(($this->ffmpeg_video_codec !== null) && (trim($this->ffmpeg_video_codec)!=='') ){
            $campos .= "ffmpeg_video_codec,";
            $valores .= "'".$this->ffmpeg_video_codec."',";
        }
        if(($this->ffmpeg_deinterlace !== null) && (trim($this->ffmpeg_deinterlace)!=='') ){
            $campos .= "ffmpeg_deinterlace,";
            $valores .= "'".$this->ffmpeg_deinterlace."',";
        }
        if(($this->snapshot_interval !== null) && (trim($this->snapshot_interval)!=='') ){
            $campos .= "snapshot_interval,";
            $valores .= "'".$this->snapshot_interval."',";
        }
        if(($this->locate !== null) && (trim($this->locate)!=='') ){
            $campos .= "locate,";
            $valores .= "'".$this->locate."',";
        }
        if(($this->text_right !== null) && (trim($this->text_right)!=='') ){
            $campos .= "text_right,";
            $valores .= "'".$this->text_right."',";
        }
        if(($this->text_left !== null) && (trim($this->text_left)!=='') ){
            $campos .= "text_left,";
            $valores .= "'".$this->text_left."',";
        }
        if(($this->text_changes !== null) && (trim($this->text_changes)!=='') ){
            $campos .= "text_changes,";
            $valores .= "'".$this->text_changes."',";
        }
        if(($this->text_event !== null) && (trim($this->text_event)!=='') ){
            $campos .= "text_event,";
            $valores .= "'".$this->text_event."',";
        }
        if(($this->text_double !== null) && (trim($this->text_double)!=='') ){
            $campos .= "text_double,";
            $valores .= "'".$this->text_double."',";
        }
        if(($this->target_dir !== null) && (trim($this->target_dir)!=='') ){
            $campos .= "target_dir,";
            $valores .= "'".$this->target_dir."',";
        }
        if(($this->snapshot_filename !== null) && (trim($this->snapshot_filename)!=='') ){
            $campos .= "snapshot_filename,";
            $valores .= "'".$this->snapshot_filename."',";
        }
        if(($this->jpeg_filename !== null) && (trim($this->jpeg_filename)!=='') ){
            $campos .= "jpeg_filename,";
            $valores .= "'".$this->jpeg_filename."',";
        }
        if(($this->movie_filename !== null) && (trim($this->movie_filename)!=='') ){
            $campos .= "movie_filename,";
            $valores .= "'".$this->movie_filename."',";
        }
        if(($this->timelapse_filename !== null) && (trim($this->timelapse_filename)!=='') ){
            $campos .= "timelapse_filename,";
            $valores .= "'".$this->timelapse_filename."',";
        }
        if(($this->webcam_port !== null) && (trim($this->webcam_port)!=='') ){
            $campos .= "webcam_port,";
            $valores .= "'".$this->webcam_port."',";
        }
        if(($this->webcam_quality !== null) && (trim($this->webcam_quality)!=='') ){
            $campos .= "webcam_quality,";
            $valores .= "'".$this->webcam_quality."',";
        }
        if(($this->webcam_motion !== null) && (trim($this->webcam_motion)!=='') ){
            $campos .= "webcam_motion,";
            $valores .= "'".$this->webcam_motion."',";
        }
        if(($this->webcam_maxrate !== null) && (trim($this->webcam_maxrate)!=='') ){
            $campos .= "webcam_maxrate,";
            $valores .= "'".$this->webcam_maxrate."',";
        }
        if(($this->webcam_localhost !== null) && (trim($this->webcam_localhost)!=='') ){
            $campos .= "webcam_localhost,";
            $valores .= "'".$this->webcam_localhost."',";
        }
        if(($this->webcam_limit !== null) && (trim($this->webcam_limit)!=='') ){
            $campos .= "webcam_limit,";
            $valores .= "'".$this->webcam_limit."',";
        }
        if(($this->control_port !== null) && (trim($this->control_port)!=='') ){
            $campos .= "control_port,";
            $valores .= "'".$this->control_port."',";
        }
        if(($this->control_localhost !== null) && (trim($this->control_localhost)!=='') ){
            $campos .= "control_localhost,";
            $valores .= "'".$this->control_localhost."',";
        }
        if(($this->control_html_output !== null) && (trim($this->control_html_output)!=='') ){
            $campos .= "control_html_output,";
            $valores .= "'".$this->control_html_output."',";
        }
        if(($this->control_authentication !== null) && (trim($this->control_authentication)!=='') ){
            $campos .= "control_authentication,";
            $valores .= "'".$this->control_authentication."',";
        }
        if(($this->track_type !== null) && (trim($this->track_type)!=='') ){
            $campos .= "track_type,";
            $valores .= "'".$this->track_type."',";
        }
        if(($this->track_auto !== null) && (trim($this->track_auto)!=='') ){
            $campos .= "track_auto,";
            $valores .= "'".$this->track_auto."',";
        }
        if(($this->track_port !== null) && (trim($this->track_port)!=='') ){
            $campos .= "track_port,";
            $valores .= "'".$this->track_port."',";
        }
        if(($this->track_motorx !== null) && (trim($this->track_motorx)!=='') ){
            $campos .= "track_motorx,";
            $valores .= "'".$this->track_motorx."',";
        }
        if(($this->track_motory !== null) && (trim($this->track_motory)!=='') ){
            $campos .= "track_motory,";
            $valores .= "'".$this->track_motory."',";
        }
        if(($this->track_maxx !== null) && (trim($this->track_maxx)!=='') ){
            $campos .= "track_maxx,";
            $valores .= "'".$this->track_maxx."',";
        }
        if(($this->track_maxy !== null) && (trim($this->track_maxy)!=='') ){
            $campos .= "track_maxy,";
            $valores .= "'".$this->track_maxy."',";
        }
        if(($this->track_iomojo_id !== null) && (trim($this->track_iomojo_id)!=='') ){
            $campos .= "track_iomojo_id,";
            $valores .= "'".$this->track_iomojo_id."',";
        }
        if(($this->track_step_angle_x !== null) && (trim($this->track_step_angle_x)!=='') ){
            $campos .= "track_step_angle_x,";
            $valores .= "'".$this->track_step_angle_x."',";
        }
        if(($this->track_step_angle_y !== null) && (trim($this->track_step_angle_y)!=='') ){
            $campos .= "track_step_angle_y,";
            $valores .= "'".$this->track_step_angle_y."',";
        }
        if(($this->track_move_wait !== null) && (trim($this->track_move_wait)!=='') ){
            $campos .= "track_move_wait,";
            $valores .= "'".$this->track_move_wait."',";
        }
        if(($this->track_speed !== null) && (trim($this->track_speed)!=='') ){
            $campos .= "track_speed,";
            $valores .= "'".$this->track_speed."',";
        }
        if(($this->track_stepsize !== null) && (trim($this->track_stepsize)!=='') ){
            $campos .= "track_stepsize,";
            $valores .= "'".$this->track_stepsize."',";
        }
        if(($this->quiet !== null) && (trim($this->quiet)!=='') ){
            $campos .= "quiet,";
            $valores .= "'".$this->quiet."',";
        }
        if(($this->on_event_start !== null) && (trim($this->on_event_start)!=='') ){
            $campos .= "on_event_start,";
            $valores .= "'".$this->on_event_start."',";
        }
        if(($this->on_event_end !== null) && (trim($this->on_event_end)!=='') ){
            $campos .= "on_event_end,";
            $valores .= "'".$this->on_event_end."',";
        }
        if(($this->on_picture_save !== null) && (trim($this->on_picture_save)!=='') ){
            $campos .= "on_picture_save,";
            $valores .= "'".$this->on_picture_save."',";
        }
        if(($this->on_motion_detected !== null) && (trim($this->on_motion_detected)!=='') ){
            $campos .= "on_motion_detected,";
            $valores .= "'".$this->on_motion_detected."',";
        }
        if(($this->on_movie_start !== null) && (trim($this->on_movie_start)!=='') ){
            $campos .= "on_movie_start,";
            $valores .= "'".$this->on_movie_start."',";
        }
        if(($this->on_movie_end !== null) && (trim($this->on_movie_end)!=='') ){
            $campos .= "on_movie_end,";
            $valores .= "'".$this->on_movie_end."',";
        }
        if(($this->video_pipe !== null) && (trim($this->video_pipe)!=='') ){
            $campos .= "video_pipe,";
            $valores .= "'".$this->video_pipe."',";
        }
        if(($this->motion_video_pipe !== null) && (trim($this->motion_video_pipe)!=='') ){
            $campos .= "motion_video_pipe,";
            $valores .= "'".$this->motion_video_pipe."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_motionconf $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_motionconf SET ";
        
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $sql .= "ipv4 = '".$this->ipv4."',";
        }
        if(($this->daemon !== null) && (trim($this->daemon)!=='') ){
            $sql .= "daemon = '".$this->daemon."',";
        }
        if(($this->process_id_file !== null) && (trim($this->process_id_file)!=='') ){
            $sql .= "process_id_file = '".$this->process_id_file."',";
        }
        if(($this->setup_mode !== null) && (trim($this->setup_mode)!=='') ){
            $sql .= "setup_mode = '".$this->setup_mode."',";
        }
        if(($this->videodevice !== null) && (trim($this->videodevice)!=='') ){
            $sql .= "videodevice = '".$this->videodevice."',";
        }
        if(($this->tunerdevice !== null) && (trim($this->tunerdevice)!=='') ){
            $sql .= "tunerdevice = '".$this->tunerdevice."',";
        }
        if(($this->input !== null) && (trim($this->input)!=='') ){
            $sql .= "input = '".$this->input."',";
        }
        if(($this->norm !== null) && (trim($this->norm)!=='') ){
            $sql .= "norm = '".$this->norm."',";
        }
        if(($this->frequency !== null) && (trim($this->frequency)!=='') ){
            $sql .= "frequency = '".$this->frequency."',";
        }
        if(($this->rotate !== null) && (trim($this->rotate)!=='') ){
            $sql .= "rotate = '".$this->rotate."',";
        }
        if(($this->width !== null) && (trim($this->width)!=='') ){
            $sql .= "width = '".$this->width."',";
        }
        if(($this->height !== null) && (trim($this->height)!=='') ){
            $sql .= "height = '".$this->height."',";
        }
        if(($this->framerate !== null) && (trim($this->framerate)!=='') ){
            $sql .= "framerate = '".$this->framerate."',";
        }
        if(($this->minimum_frame_time !== null) && (trim($this->minimum_frame_time)!=='') ){
            $sql .= "minimum_frame_time = '".$this->minimum_frame_time."',";
        }
        if(($this->netcam_url !== null) && (trim($this->netcam_url)!=='') ){
            $sql .= "netcam_url = '".$this->netcam_url."',";
        }
        if(($this->netcam_userpass !== null) && (trim($this->netcam_userpass)!=='') ){
            $sql .= "netcam_userpass = '".$this->netcam_userpass."',";
        }
        if(($this->netcam_proxy !== null) && (trim($this->netcam_proxy)!=='') ){
            $sql .= "netcam_proxy = '".$this->netcam_proxy."',";
        }
        if(($this->auto_brightness !== null) && (trim($this->auto_brightness)!=='') ){
            $sql .= "auto_brightness = '".$this->auto_brightness."',";
        }
        if(($this->brightness !== null) && (trim($this->brightness)!=='') ){
            $sql .= "brightness = '".$this->brightness."',";
        }
        if(($this->contrast !== null) && (trim($this->contrast)!=='') ){
            $sql .= "contrast = '".$this->contrast."',";
        }
        if(($this->saturation !== null) && (trim($this->saturation)!=='') ){
            $sql .= "saturation = '".$this->saturation."',";
        }
        if(($this->hue !== null) && (trim($this->hue)!=='') ){
            $sql .= "hue = '".$this->hue."',";
        }
        if(($this->roundrobin_frames !== null) && (trim($this->roundrobin_frames)!=='') ){
            $sql .= "roundrobin_frames = '".$this->roundrobin_frames."',";
        }
        if(($this->roundrobin_skip !== null) && (trim($this->roundrobin_skip)!=='') ){
            $sql .= "roundrobin_skip = '".$this->roundrobin_skip."',";
        }
        if(($this->switchfilter !== null) && (trim($this->switchfilter)!=='') ){
            $sql .= "switchfilter = '".$this->switchfilter."',";
        }
        if(($this->threshold !== null) && (trim($this->threshold)!=='') ){
            $sql .= "threshold = '".$this->threshold."',";
        }
        if(($this->threshold_tune !== null) && (trim($this->threshold_tune)!=='') ){
            $sql .= "threshold_tune = '".$this->threshold_tune."',";
        }
        if(($this->noise_level !== null) && (trim($this->noise_level)!=='') ){
            $sql .= "noise_level = '".$this->noise_level."',";
        }
        if(($this->noise_tune !== null) && (trim($this->noise_tune)!=='') ){
            $sql .= "noise_tune = '".$this->noise_tune."',";
        }
        if(($this->night_compensate !== null) && (trim($this->night_compensate)!=='') ){
            $sql .= "night_compensate = '".$this->night_compensate."',";
        }
        if(($this->despeckle !== null) && (trim($this->despeckle)!=='') ){
            $sql .= "despeckle = '".$this->despeckle."',";
        }
        if(($this->mask_file !== null) && (trim($this->mask_file)!=='') ){
            $sql .= "mask_file = '".$this->mask_file."',";
        }
        else{
            $sql .= "mask_file = NULL,";
        }
        if(($this->smart_mask_speed !== null) && (trim($this->smart_mask_speed)!=='') ){
            $sql .= "smart_mask_speed = '".$this->smart_mask_speed."',";
        }
        if(($this->lightswitch !== null) && (trim($this->lightswitch)!=='') ){
            $sql .= "lightswitch = '".$this->lightswitch."',";
        }
        if(($this->minimum_motion_frames !== null) && (trim($this->minimum_motion_frames)!=='') ){
            $sql .= "minimum_motion_frames = '".$this->minimum_motion_frames."',";
        }
        if(($this->pre_capture !== null) && (trim($this->pre_capture)!=='') ){
            $sql .= "pre_capture = '".$this->pre_capture."',";
        }
        if(($this->post_capture !== null) && (trim($this->post_capture)!=='') ){
            $sql .= "post_capture = '".$this->post_capture."',";
        }
        if(($this->gap !== null) && (trim($this->gap)!=='') ){
            $sql .= "gap = '".$this->gap."',";
        }
        if(($this->max_mpeg_time !== null) && (trim($this->max_mpeg_time)!=='') ){
            $sql .= "max_mpeg_time = '".$this->max_mpeg_time."',";
        }
        if(($this->low_cpu !== null) && (trim($this->low_cpu)!=='') ){
            $sql .= "low_cpu = '".$this->low_cpu."',";
        }
        if(($this->output_all !== null) && (trim($this->output_all)!=='') ){
            $sql .= "output_all = '".$this->output_all."',";
        }
        if(($this->output_normal !== null) && (trim($this->output_normal)!=='') ){
            $sql .= "output_normal = '".$this->output_normal."',";
        }
        if(($this->output_motion !== null) && (trim($this->output_motion)!=='') ){
            $sql .= "output_motion = '".$this->output_motion."',";
        }
        if(($this->quality !== null) && (trim($this->quality)!=='') ){
            $sql .= "quality = '".$this->quality."',";
        }
        if(($this->ppm !== null) && (trim($this->ppm)!=='') ){
            $sql .= "ppm = '".$this->ppm."',";
        }
        if(($this->ffmpeg_cap_new !== null) && (trim($this->ffmpeg_cap_new)!=='') ){
            $sql .= "ffmpeg_cap_new = '".$this->ffmpeg_cap_new."',";
        }
        if(($this->ffmpeg_cap_motion !== null) && (trim($this->ffmpeg_cap_motion)!=='') ){
            $sql .= "ffmpeg_cap_motion = '".$this->ffmpeg_cap_motion."',";
        }
        if(($this->ffmpeg_timelapse !== null) && (trim($this->ffmpeg_timelapse)!=='') ){
            $sql .= "ffmpeg_timelapse = '".$this->ffmpeg_timelapse."',";
        }
        if(($this->ffmpeg_timelapse_mode !== null) && (trim($this->ffmpeg_timelapse_mode)!=='') ){
            $sql .= "ffmpeg_timelapse_mode = '".$this->ffmpeg_timelapse_mode."',";
        }
        if(($this->ffmpeg_bps !== null) && (trim($this->ffmpeg_bps)!=='') ){
            $sql .= "ffmpeg_bps = '".$this->ffmpeg_bps."',";
        }
        if(($this->ffmpeg_variable_bitrate !== null) && (trim($this->ffmpeg_variable_bitrate)!=='') ){
            $sql .= "ffmpeg_variable_bitrate = '".$this->ffmpeg_variable_bitrate."',";
        }
        if(($this->ffmpeg_video_codec !== null) && (trim($this->ffmpeg_video_codec)!=='') ){
            $sql .= "ffmpeg_video_codec = '".$this->ffmpeg_video_codec."',";
        }
        if(($this->ffmpeg_deinterlace !== null) && (trim($this->ffmpeg_deinterlace)!=='') ){
            $sql .= "ffmpeg_deinterlace = '".$this->ffmpeg_deinterlace."',";
        }
        if(($this->snapshot_interval !== null) && (trim($this->snapshot_interval)!=='') ){
            $sql .= "snapshot_interval = '".$this->snapshot_interval."',";
        }
        if(($this->locate !== null) && (trim($this->locate)!=='') ){
            $sql .= "locate = '".$this->locate."',";
        }
        if(($this->text_right !== null) && (trim($this->text_right)!=='') ){
            $sql .= "text_right = '".$this->text_right."',";
        }
        if(($this->text_left !== null) && (trim($this->text_left)!=='') ){
            $sql .= "text_left = '".$this->text_left."',";
        }
        if(($this->text_changes !== null) && (trim($this->text_changes)!=='') ){
            $sql .= "text_changes = '".$this->text_changes."',";
        }
        if(($this->text_event !== null) && (trim($this->text_event)!=='') ){
            $sql .= "text_event = '".$this->text_event."',";
        }
        if(($this->text_double !== null) && (trim($this->text_double)!=='') ){
            $sql .= "text_double = '".$this->text_double."',";
        }
        if(($this->target_dir !== null) && (trim($this->target_dir)!=='') ){
            $sql .= "target_dir = '".$this->target_dir."',";
        }
        if(($this->snapshot_filename !== null) && (trim($this->snapshot_filename)!=='') ){
            $sql .= "snapshot_filename = '".$this->snapshot_filename."',";
        }
        if(($this->jpeg_filename !== null) && (trim($this->jpeg_filename)!=='') ){
            $sql .= "jpeg_filename = '".$this->jpeg_filename."',";
        }
        if(($this->movie_filename !== null) && (trim($this->movie_filename)!=='') ){
            $sql .= "movie_filename = '".$this->movie_filename."',";
        }
        if(($this->timelapse_filename !== null) && (trim($this->timelapse_filename)!=='') ){
            $sql .= "timelapse_filename = '".$this->timelapse_filename."',";
        }
        if(($this->webcam_port !== null) && (trim($this->webcam_port)!=='') ){
            $sql .= "webcam_port = '".$this->webcam_port."',";
        }
        if(($this->webcam_quality !== null) && (trim($this->webcam_quality)!=='') ){
            $sql .= "webcam_quality = '".$this->webcam_quality."',";
        }
        if(($this->webcam_motion !== null) && (trim($this->webcam_motion)!=='') ){
            $sql .= "webcam_motion = '".$this->webcam_motion."',";
        }
        if(($this->webcam_maxrate !== null) && (trim($this->webcam_maxrate)!=='') ){
            $sql .= "webcam_maxrate = '".$this->webcam_maxrate."',";
        }
        if(($this->webcam_localhost !== null) && (trim($this->webcam_localhost)!=='') ){
            $sql .= "webcam_localhost = '".$this->webcam_localhost."',";
        }
        if(($this->webcam_limit !== null) && (trim($this->webcam_limit)!=='') ){
            $sql .= "webcam_limit = '".$this->webcam_limit."',";
        }
        if(($this->control_port !== null) && (trim($this->control_port)!=='') ){
            $sql .= "control_port = '".$this->control_port."',";
        }
        if(($this->control_localhost !== null) && (trim($this->control_localhost)!=='') ){
            $sql .= "control_localhost = '".$this->control_localhost."',";
        }
        if(($this->control_html_output !== null) && (trim($this->control_html_output)!=='') ){
            $sql .= "control_html_output = '".$this->control_html_output."',";
        }
        if(($this->control_authentication !== null) && (trim($this->control_authentication)!=='') ){
            $sql .= "control_authentication = '".$this->control_authentication."',";
        }
        if(($this->track_type !== null) && (trim($this->track_type)!=='') ){
            $sql .= "track_type = '".$this->track_type."',";
        }
        if(($this->track_auto !== null) && (trim($this->track_auto)!=='') ){
            $sql .= "track_auto = '".$this->track_auto."',";
        }
        if(($this->track_port !== null) && (trim($this->track_port)!=='') ){
            $sql .= "track_port = '".$this->track_port."',";
        }
        if(($this->track_motorx !== null) && (trim($this->track_motorx)!=='') ){
            $sql .= "track_motorx = '".$this->track_motorx."',";
        }
        if(($this->track_motory !== null) && (trim($this->track_motory)!=='') ){
            $sql .= "track_motory = '".$this->track_motory."',";
        }
        if(($this->track_maxx !== null) && (trim($this->track_maxx)!=='') ){
            $sql .= "track_maxx = '".$this->track_maxx."',";
        }
        if(($this->track_maxy !== null) && (trim($this->track_maxy)!=='') ){
            $sql .= "track_maxy = '".$this->track_maxy."',";
        }
        if(($this->track_iomojo_id !== null) && (trim($this->track_iomojo_id)!=='') ){
            $sql .= "track_iomojo_id = '".$this->track_iomojo_id."',";
        }
        if(($this->track_step_angle_x !== null) && (trim($this->track_step_angle_x)!=='') ){
            $sql .= "track_step_angle_x = '".$this->track_step_angle_x."',";
        }
        if(($this->track_step_angle_y !== null) && (trim($this->track_step_angle_y)!=='') ){
            $sql .= "track_step_angle_y = '".$this->track_step_angle_y."',";
        }
        if(($this->track_move_wait !== null) && (trim($this->track_move_wait)!=='') ){
            $sql .= "track_move_wait = '".$this->track_move_wait."',";
        }
        if(($this->track_speed !== null) && (trim($this->track_speed)!=='') ){
            $sql .= "track_speed = '".$this->track_speed."',";
        }
        if(($this->track_stepsize !== null) && (trim($this->track_stepsize)!=='') ){
            $sql .= "track_stepsize = '".$this->track_stepsize."',";
        }
        if(($this->quiet !== null) && (trim($this->quiet)!=='') ){
            $sql .= "quiet = '".$this->quiet."',";
        }
        if(($this->on_event_start !== null) && (trim($this->on_event_start)!=='') ){
            $sql .= "on_event_start = '".$this->on_event_start."',";
        }
        if(($this->on_event_end !== null) && (trim($this->on_event_end)!=='') ){
            $sql .= "on_event_end = '".$this->on_event_end."',";
        }
        if(($this->on_picture_save !== null) && (trim($this->on_picture_save)!=='') ){
            $sql .= "on_picture_save = '".$this->on_picture_save."',";
        }
        if(($this->on_motion_detected !== null) && (trim($this->on_motion_detected)!=='') ){
            $sql .= "on_motion_detected = '".$this->on_motion_detected."',";
        }
        if(($this->on_movie_start !== null) && (trim($this->on_movie_start)!=='') ){
            $sql .= "on_movie_start = '".$this->on_movie_start."',";
        }
        if(($this->on_movie_end !== null) && (trim($this->on_movie_end)!=='') ){
            $sql .= "on_movie_end = '".$this->on_movie_end."',";
        }
        if(($this->video_pipe !== null) && (trim($this->video_pipe)!=='') ){
            $sql .= "video_pipe = '".$this->video_pipe."',";
        }
        if(($this->motion_video_pipe !== null) && (trim($this->motion_video_pipe)!=='') ){
            $sql .= "motion_video_pipe = '".$this->motion_video_pipe."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idservidor = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_motionconf  WHERE idservidor = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidMotionconfs
     * @return object Devuelve un registro como objeto
     */
    function  getVidMotionconfs(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idservidorBuscador"]){
            $in = null;
            foreach ($_REQUEST["idservidorBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idservidor in (".$in.")";
        }
        
        $sql = "select idservidor,ipv4,daemon,process_id_file,setup_mode,videodevice,tunerdevice,input,norm,frequency,rotate,width,height,framerate,minimum_frame_time,netcam_url,netcam_userpass,netcam_proxy,auto_brightness,brightness,contrast,saturation,hue,roundrobin_frames,roundrobin_skip,switchfilter,threshold,threshold_tune,noise_level,noise_tune,night_compensate,despeckle,mask_file,smart_mask_speed,lightswitch,minimum_motion_frames,pre_capture,post_capture,gap,max_mpeg_time,low_cpu,output_all,output_normal,output_motion,quality,ppm,ffmpeg_cap_new,ffmpeg_cap_motion,ffmpeg_timelapse,ffmpeg_timelapse_mode,ffmpeg_bps,ffmpeg_variable_bitrate,ffmpeg_video_codec,ffmpeg_deinterlace,snapshot_interval,locate,text_right,text_left,text_changes,text_event,text_double,target_dir,snapshot_filename,jpeg_filename,movie_filename,timelapse_filename,webcam_port,webcam_quality,webcam_motion,webcam_maxrate,webcam_localhost,webcam_limit,control_port,control_localhost,control_html_output,control_authentication,track_type,track_auto,track_port,track_motorx,track_motory,track_maxx,track_maxy,track_iomojo_id,track_step_angle_x,track_step_angle_y,track_move_wait,track_speed,track_stepsize,quiet,on_event_start,on_event_end,on_picture_save,on_motion_detected,on_movie_start,on_movie_end,video_pipe,motion_video_pipe from vid_motionconf ".$arg." order by ipv4,daemon";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidMotionconf
     * @param int $idservidor Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidMotionconf($idservidor){
        $sql = "SELECT idservidor,ipv4,daemon,process_id_file,setup_mode,videodevice,tunerdevice,input,norm,frequency,rotate,width,height,framerate,minimum_frame_time,netcam_url,netcam_userpass,netcam_proxy,auto_brightness,brightness,contrast,saturation,hue,roundrobin_frames,roundrobin_skip,switchfilter,threshold,threshold_tune,noise_level,noise_tune,night_compensate,despeckle,mask_file,smart_mask_speed,lightswitch,minimum_motion_frames,pre_capture,post_capture,gap,max_mpeg_time,low_cpu,output_all,output_normal,output_motion,quality,ppm,ffmpeg_cap_new,ffmpeg_cap_motion,ffmpeg_timelapse,ffmpeg_timelapse_mode,ffmpeg_bps,ffmpeg_variable_bitrate,ffmpeg_video_codec,ffmpeg_deinterlace,snapshot_interval,locate,text_right,text_left,text_changes,text_event,text_double,target_dir,snapshot_filename,jpeg_filename,movie_filename,timelapse_filename,webcam_port,webcam_quality,webcam_motion,webcam_maxrate,webcam_localhost,webcam_limit,control_port,control_localhost,control_html_output,control_authentication,track_type,track_auto,track_port,track_motorx,track_motory,track_maxx,track_maxy,track_iomojo_id,track_step_angle_x,track_step_angle_y,track_move_wait,track_speed,track_stepsize,quiet,on_event_start,on_event_end,on_picture_save,on_motion_detected,on_movie_start,on_movie_end,video_pipe,motion_video_pipe FROM vid_motionconf WHERE idservidor = '".$idservidor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>