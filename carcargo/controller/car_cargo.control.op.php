<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CARCRG"); //Categoria del modulo
require_once "car_cargo.control.php";// Class CONTROL ControlCarCargo()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCarCargo extends ControlCarCargo{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCarCargo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        //------------------ Metodo Set  -----------------//
        if(!$this->setCarCargo()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCarCargo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCarCargo()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Cargo");
        
        $this->setCampos("idplantilla","Idplantilla");
        $this->setCampos("cargo_nombre_chk","Cargo_nombre_chk");
        $this->setCampos("cargo_nombre","Nombre del Cargo");
        $this->setCampos("cargo_top","Cargo_top");
        $this->setCampos("cargo_left","Cargo_left");
        $this->setCampos("cargo_w","Cargo_w");
        $this->setCampos("cargo_h","Cargo_h");
        $this->setCampos("cargo_color","Cargo_color");
        $this->setCampos("cargo_bgcolor","Color de Fondo");
        $this->setCampos("cargo_fuentetamano","cargo_fuentetamano");
        $this->setCampos("cargo_fuenteletra","cargo_fuenteletra");
        $this->setCampos("cargo_fuentealign","cargo_fuentealign");
        $this->setCampos("cargo_fuentevalign","Departamento_fuentevalign");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"idplantilla","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_nombre_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cargo_nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_h","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_color","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cargo_bgcolor","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_fuentetamano","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_fuenteletra","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_fuentealign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cargo_fuentevalign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }
}
?>