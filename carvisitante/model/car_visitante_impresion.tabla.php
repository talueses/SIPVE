<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CarVisitanteImpresion {
    
    private $idcarnet_visitante_impresion = null;
    private $idcarnet_visitante = null;
    private $usuario = null;
    private $modo = null;
    private $fecha = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcarnet_visitante_impresion($idcarnet_visitante_impresion){
        $this->idcarnet_visitante_impresion = $idcarnet_visitante_impresion;
    }
    public function getIdcarnet_visitante_impresion(){
        return $this->idcarnet_visitante_impresion;
    }
    public function setIdcarnet_visitante($idcarnet_visitante){
        $this->idcarnet_visitante = $idcarnet_visitante;
    }
    public function getIdcarnet_visitante(){
        return $this->idcarnet_visitante;
    }
    public function setUsuario($usuario){
        $this->usuario = $usuario;
    }
    public function getUsuario(){
        return $this->usuario;
    }
    public function setModo($modo){
        $this->modo = $modo;
    }
    public function getModo(){
        return $this->modo;
    }
    public function setFecha($fecha){
        $this->fecha = $fecha;
    }
    public function getFecha(){
        return $this->fecha;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcarnet_visitante_impresion !== null) && (trim($this->idcarnet_visitante_impresion)!=='') ){
            $campos .= "idcarnet_visitante_impresion,";
            $valores .= "'".$this->idcarnet_visitante_impresion."',";
        }
        if(($this->idcarnet_visitante !== null) && (trim($this->idcarnet_visitante)!=='') ){
            $campos .= "idcarnet_visitante,";
            $valores .= "'".$this->idcarnet_visitante."',";
        }
        if(($this->usuario !== null) && (trim($this->usuario)!=='') ){
            $campos .= "usuario,";
            $valores .= "'".$this->usuario."',";
        }
        if(($this->modo !== null) && (trim($this->modo)!=='') ){
            $campos .= "modo,";
            $valores .= "'".$this->modo."',";
        }
        if(($this->fecha !== null) && (trim($this->fecha)!=='') ){
            $campos .= "fecha,";
            $valores .= "'".$this->fecha."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO car_visitante_impresion $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE car_visitante_impresion SET ";
        
        if(($this->idcarnet_visitante !== null) && (trim($this->idcarnet_visitante)!=='') ){
            $sql .= "idcarnet_visitante = '".$this->idcarnet_visitante."',";
        }
        if(($this->usuario !== null) && (trim($this->usuario)!=='') ){
            $sql .= "usuario = '".$this->usuario."',";
        }
        if(($this->modo !== null) && (trim($this->modo)!=='') ){
            $sql .= "modo = '".$this->modo."',";
        }
        if(($this->fecha !== null) && (trim($this->fecha)!=='') ){
            $sql .= "fecha = '".$this->fecha."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcarnet_visitante_impresion = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM car_visitante_impresion  WHERE idcarnet_visitante_impresion = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CarVisitanteImpresions
     * @return object Devuelve un registro como objeto
     */
    function  getCarVisitanteImpresions(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcarnet_visitante_impresionBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcarnet_visitante_impresionBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcarnet_visitante_impresion in (".$in.")";
        }
        
        $sql = "select idcarnet_visitante_impresion,idcarnet_visitante,usuario,modo,fecha from car_visitante_impresion ".$arg." order by usuario,modo";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CarVisitanteImpresion
     * @param int $idcarnet_visitante_impresion Codigo
     * @return object Devuelve registros como objeto
     */
    function getCarVisitanteImpresion($idcarnet_visitante_impresion){
        $sql = "SELECT idcarnet_visitante_impresion,idcarnet_visitante,usuario,modo,fecha FROM car_visitante_impresion WHERE idcarnet_visitante_impresion = '".$idcarnet_visitante_impresion."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>