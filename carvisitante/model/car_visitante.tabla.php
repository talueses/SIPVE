<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CarVisitante {
    
    private $idcarnet_visitante = null;
    private $idplantilla = null;
    private $nombre = null;
    private $numero = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcarnet_visitante($idcarnet_visitante){
        $this->idcarnet_visitante = $idcarnet_visitante;
    }
    public function getIdcarnet_visitante(){
        return $this->idcarnet_visitante;
    }
    public function setIdplantilla($idplantilla){
        $this->idplantilla = $idplantilla;
    }
    public function getIdplantilla(){
        return $this->idplantilla;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setNumero($numero){
        $this->numero = $numero;
    }
    public function getNumero(){
        return $this->numero;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcarnet_visitante !== null) && (trim($this->idcarnet_visitante)!=='') ){
            $campos .= "idcarnet_visitante,";
            $valores .= "'".$this->idcarnet_visitante."',";
        }
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $campos .= "idplantilla,";
            $valores .= "'".$this->idplantilla."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $campos .= "numero,";
            $valores .= "'".$this->numero."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO car_visitante $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE car_visitante SET ";
        
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $sql .= "idplantilla = '".$this->idplantilla."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $sql .= "numero = '".$this->numero."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcarnet_visitante = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM car_visitante  WHERE idcarnet_visitante = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CarVisitantes
     * @return object Devuelve un registro como objeto
     */
    function  getCarVisitantes(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcarnet_visitanteBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcarnet_visitanteBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcarnet_visitante in (".$in.")";
        }
        
        $sql = "select /*start*/ idcarnet_visitante,idplantilla,nombre,numero /*end*/ from car_visitante ".$arg." order by nombre,numero";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CarVisitante
     * @param int $idcarnet_visitante Codigo
     * @return object Devuelve registros como objeto
     */
    function getCarVisitante($idcarnet_visitante){
        $sql = "SELECT idcarnet_visitante,idplantilla,nombre,numero FROM car_visitante WHERE idcarnet_visitante = '".$idcarnet_visitante."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>