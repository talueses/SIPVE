var loadingMsg = '<img src="../images/loading51.gif" alt="" width="16" /> Cargando...';
var widthV;
var heightV;
var widthH;
var heightH;
var val;
var objId     = [];
var styleObjs = []; // [0]: Obj to apply style, [1]: css property, [2]: css sub-property, [3]: val to apply

//------------------------------------------LOAD-------------------------------------------------------------//
$(function() {
    
    widthV  = parseInt($('#defaultWidth').val());
    heightV = parseInt($('#defaultHeight').val());
    widthH  = heightV;
    heightH = widthV;
    
    $( "input:button, input:submit,button" ).button();
    // ------------------------------ COMBO PLANTILLA ---------------------------------------- //
    $('#idplantilla')
        .live({
            change:function(){
                $('#botones').hide();
                var url ='car_visitante.Acc.php?idcarnet_visitante='+$('#idcarnet_visitante').val()+'&idplantilla='+$(this).val()+'&accion='+$('#accion').val();
                //alert(url);
                $('#datos').show().html(loadingMsg);
                document.location.href = url;
            }
        });
    if (!$('#idplantilla').val()){
        $('#datos, #botones').hide();
    }
    //-----------------------------------MENSAJE-----------------------------------------------//
    $('#divmensaje').live({
        click: function() {
            $(this).fadeOut(500);
        }
    });
    //---------------------------------------DIV CARNET------------------------------------------//
    $('#divCarnet')
        .width(setWidth())
        .height(setHeight())
        .css({
            'background':'#D3D3D3',
            'background-size':'100%'
        });
    if ($('#orientacion').val()=='V'){
        $('#divCarnet, #carnetImg').width(widthV).height(heightV);
    }
    if ($('#orientacion').val()=='H'){
        $('#divCarnet, #carnetImg').width(widthH).height(heightH);
    }
    $('div#logo, div#institucion,div#barcode,div#visitante,div#visitantenro')
        .css({
            top: function(){
                return $('#'+$(this).attr('id')+'_top').val();
            },
            left: function(){
                return $('#'+$(this).attr('id')+'_left').val();
            },
            width: function(){
                return $('#'+$(this).attr('id')+'_w').val();
            },
            height: function(){
                return $('#'+$(this).attr('id')+'_h').val();
            },
            'background': function(){
                if ($(this).attr('id')=='logo'){                    
                    return 'url(../../carplantilla/images/imagesPlantillas/'+$('#archivo_logo').val()+') 50% 50% no-repeat';
                }
                
            },
            'background-size': function(){
                if ($(this).attr('id')=='logo'){
                    return '100%';
                }
            }
        })
    //----------------------------------------BARCODE IMG----------------------------------------------//
    $('div#barcode img').attr({
        src: setBarcodeImg()
    })
    //
    //----------------------------------COMBOS DE ESTILOS-OBJS-----------------------------------------//
    $(".styleObjs")
        .each(function(){
            objId = $(this).attr('id').split('_');
            
            styleObjs[0] = '_view';
            styleObjs[2] = '';
            styleObjs[3] = $('#'+objId[0]+'_'+objId[1]).val();

            if (objId[1] == 'fuentetamano'){                
                styleObjs[1] = 'font-size';
                styleObjs[2] = 'pt'

            }
            if (objId[1] == 'fuenteletra'){
                styleObjs[1] = 'font-family';                
            }
            if (objId[1] == 'fuentealign'){
                styleObjs[1] = 'text-align';
            }
            if (objId[1] == 'fuentevalign'){                
                styleObjs[1] = 'vertical-align';
            }
            if (objId[1] == 'color'){                
                styleObjs[1] = 'color';
                styleObjs[3] = "#"+styleObjs[3];
            }
            if (objId[1] == 'bgcolor'){                
                styleObjs[0] = '';
                styleObjs[1] = 'backgroundColor';
                styleObjs[3] = "#"+styleObjs[3];
            }
            $('#'+objId[0]+styleObjs[0]).css(styleObjs[1],styleObjs[3]+styleObjs[2]);
        });
    //-----------------------------------NOMBRES-----------------------------------------------//
    $(".nombres").live({
        keyup: function(){
            setNombres(this);            
        },
        change: function(){
            setNombres(this);
            //----------------------------------------BARCODE IMG----------------------------------------------//
            $('div#barcode img').attr({
                src: setBarcodeImg()
            });
        }
    }).each(function(){
        setNombres(this);
    });
    //-----------------------------------OBJETOS CHEQUEADOS PARA VISIBILIDAD--------------------------------------//
    $(".chkObj")
        .each(function(){
            objId = $(this).attr('id').split('_');
            
            if (objId[1] !="bgcolor"){

                if ($(this).val()=="1"){
                    $('#'+objId[0]).show();
                }else{
                    $('#'+objId[0]).hide();
                }
            }else{
                if ($(this).val()==""){
                    $('#'+objId[0]).css('backgroundColor','');
                }
            }
        });
    //-------------------------------------------AJAX Nro Visitante-------------------------------------------//
    accesoChk();
    $('#numero').live({
        change:function(){            
            accesoChk();
        }
    });
});
//---------------------------------------END LOAD-----------------------------------------------------------------//
function setWidth(){
    if ($('#orientacion').val()=="V"){
        return widthV;
    }
    if ($('#orientacion').val()=="H"){
        return widthH;
    }
    return true;
}
function setHeight(){
    if ($('#orientacion').val()=="V"){
        return heightV;
    }
    if ($('#orientacion').val()=="H"){
        return heightH;
    }
    return true;
}
function setNombres(obj){
    objId = $(obj).attr('id').split('_');
    val = '';

    
    if (objId[0]=='nombre' ){        
        val= $('#'+objId[0]).val();
        objId[0] = 'visitante';
    }
    if (objId[0]=='numero' ){        
        val= $('#'+objId[0]+' option:selected').text();
        objId[0] = 'visitantenro';
    }

    $('#'+objId[0]+'_view').html(val);
    
        
    if ($('#nombre').val()=="" && objId[0] == 'visitante'){
        $('#'+objId[0]+'_view').html('VISITANTE');
    }
    if ($('#numero').val()==""  && objId[0] == 'visitantenro'){
        $('#'+objId[0]+'_view').html('0000');
    }
    
}
function setBarcodeImg(){
    if ($('#numero').val()==""){
        return '../images/barcodeb39.png';
    }else{
        return 'car_visitante.barcodeImage.php?barcode='+$('#numero').val();
    }
}
function accesoChk(){
    $.ajax({
        beforeSend: function(){                    
            $('#hasAcceso').html(loadingMsg);
        },
        type: 'POST',
        dataType:'json',
        url: 'car_visitante.Op.php',
        data: 'numero='+$('#numero').val()+'&accion=accesoChk',
        success: function(data){            
            $('#hasAcceso').html('');
            if (data.hasAcceso=='1'){
                $('#hasAcceso').html('<img src="../images/Credit-Card-UI-32.png" alt="Tarjeta de Acceso Asignada" title="Tarjeta de Acceso Asignada" width="32"/>');
            }
            return true;
        }
    });      
}