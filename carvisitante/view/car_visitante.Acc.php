<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
if (!$_REQUEST["idcarnet_visitante"]){
    $_REQUEST["idcarnet_visitante"] = "0";
}
if (!$_REQUEST["idplantilla"]){
    $_REQUEST["idplantilla"] = "0";
}
require_once "../controller/car_visitante.control.op.php";// Class CONTROLLER
$obj = new CarVisitantes;
$numExist = $obj->getNumerosCarVisitantes();
$data = $obj->getCarPlantillaVisitante(array("idplantilla"=>$_REQUEST["idplantilla"],"idcarnet_visitante"=>$_REQUEST["idcarnet_visitante"]));
if (!$data ){
    echo $obj->getMensaje();
}
/*if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new CarVisitantes();
    $data = $obj->getCarVisitante($_REQUEST["idcarnet_visitante"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}*/
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/car_visitante.local.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                text-align: left;
                width: 98%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }            
            #divmensaje{
                width:99%;
                position: absolute;
                top: -10px;
                opacity:0.9;
            }
            /*-------------------------------------------------------------------------------*/
            #divCarnet{             
                position: relative;
            }
            .carnetObjects{
                
                position: absolute;
            }
            .carnetObjectsView{
                position: relative;
                width: 100%;
                height: 100%;
                display: table;
            }
            .carnetObjectsView p{
                display: table-cell;
            }
            span#hasAcceso{
                margin-left: 10px;
                position: absolute;
            }
        </style>
        <script type="text/javascript" language="javascript">
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Carnet para Visitante</div>
                <br/>
                <div id="divmensaje"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="car_visitante.Op.php" target="ifrm1">
                    Plantilla
                    <select name="idplantilla" id="idplantilla" <?php echo $disabled;?>>                        
                        <?php
                        echo "<option value=\"\">Seleccione</option>\n";
                        echo ControlCarVisitante::make_combo("car_plantilla",$arg." order by plantilla", "idplantilla", "plantilla", $data->idplantilla,false);

                        ?>
                    </select>
                    <div id="datos" >
                        <table border="0">
                            <tr>
                                <td rowspan="2" valign="top">
                                    <div id="divCarnet">
                                        <img src="../../carplantilla/images/imagesPlantillas/<?php echo $data->archivo_plantilla;?>" id="carnetImg" alt="" /> 
                                        <div id="logo" class="carnetObjects" title="Logo de la instituci&oacute;n"></div>

                                        <div id="institucion" class="carnetObjects" title="Nombre de la instituci&oacute;n" >
                                            <div class="carnetObjectsView" >
                                                <p id="institucion_view"><?php echo $data->institucion_nombre;?></p>
                                            </div>
                                        </div>
                                        <div id="barcode" class="carnetObjects" title="C&oacute;digo de Barras">
                                            <img src="" width="100%" height="100%" alt="barcode" />
                                        </div>                                        
                                        <div id="visitante" class="carnetObjects visitante" title="Etiqueta de VISITANTE">
                                            <div class="carnetObjectsView">
                                                <p id="visitante_view">VISITANTE</p>
                                            </div>
                                        </div>
                                        <div id="visitantenro" class="carnetObjects visitante" title="Etiqueta de N&uacute;mero de visitante">
                                            <div class="carnetObjectsView">
                                                <p id="visitantenro_view">0000</p>
                                            </div>
                                        </div>
                                        <!--

                                        -->
                                    </div>
                                </td>
                            </tr>
                            <tr >
                                <td valign="top" align="center">
                                    <table>
                                        <tr title="Nombre">
                                            <td align="right">Nombre:</td>
                                            <td>
                                                <input type="text" name="nombre" id="nombre" class="nombres" maxlength="100" value="<?php echo $data->nombre==""?"VISITANTE":$data->nombre;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Numero">
                                            <td align="right">Numero:</td>
                                            <td>
                                                <!--input type="text" name="numero" id="numero" class="nombres" maxlength="4" value="<?php echo $data->numero;?>"-->
                                                <select name="numero" id="numero" class="nombres" <?php echo $disabled;?>>
                                                    <?php                                                    
                                                    for ($i=1;$i<=1000;$i++){
                                                        $chkNum = false;
                                                        foreach ($numExist as $row){
                                                            if ($row->numero == $i){
                                                                $chkNum = true;
                                                            }
                                                        }
                                                        if (!$chkNum || $data->numero == $i){
                                                            echo '<option value="'.$i.'" '.Controller::busca_valor($data->numero, $i ).'>'.sprintf('%04d',$i).'</option>';
                                                        }

                                                    }                                                    
                                                    ?>
                                                </select>
                                                <span id="hasAcceso"></span>
                                            </td>
                                        </tr>
                                        <?php
                                        /*
                                        if ($data->barcode_front=="1" || $data->barcode_back=="1"){
                                            ?>
                                            <tr title="C&oacute;digo de Barras">
                                                <td align="right" valign="top">C&oacute;digo de Barras:</td>
                                                <td>
                                                    <input type="checkbox" name="barcode_asig" id="barcode_asig" value="1" <?php echo $disabled;?> <?php echo $_REQUEST["accion"]=="agregar"?"checked":""?> >
                                                    <small class="comment" style="display: inline">Seleccione para asignar un nuevo C&oacute;digo</small>
                                                    <br />                                                
                                                    <?php
                                                    if ($data->barcode){
                                                        echo "<img src=\"car_visitante.barcodeImage.php?barcode=".$data->barcode."\" alt=\"barcode\">\n";
                                                        echo "<br/>\n";
                                                        echo $data->barcode;                                                    
                                                    }
                                                    ?>
                                                    <input type="hidden" name="barcode" id="barcode_bd" value="<?php echo $data->barcode;?>">
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                         * 
                                         */
                                        ?>
                                    </table>
                                    <div id="botones" style="clear:left">
                                        <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">                        
                                        <input type="hidden" name="idcarnet_visitante" id="idcarnet_visitante" value="<?php echo $data->idcarnet_visitante ?>">
                                        
                                        <input type="hidden" id="defaultWidth" value="<?php echo Controller::$defaultWidth ;?>">
                                        <input type="hidden" id="defaultHeight" value="<?php echo Controller::$defaultHeight ;?>">
                                        
                                        <input type="hidden" name="orientacion" id="orientacion" value="<?php echo $data->orientacion ?>">    
                                        <input type="hidden" name="archivo_logo" id="archivo_logo" value="<?php echo $data->archivo_logo ?>">
                                        <input type="hidden" name="logo_chk" id="logo_chk" class="chkObj" value="<?php echo $data->logo_chk;?>">
                                        <input type="hidden" name="institucion_chk" id="institucion_chk" class="chkObj" value="<?php echo $data->institucion_chk;?>">
                                        <input type="hidden" name="barcode_front" id="barcode_front" class="chkObj" value="<?php echo $data->barcode_front ?>">
                                        <input type="hidden" name="barcode_back" id="barcode_back" value="<?php echo $data->barcode_back ?>">
                                        
                                        <!---------------------------------------------------VISITANTE------------------------------------------------------>
                                        <input type="hidden" name="visitante_top" id="visitante_top" value="<?php echo $data->visitante_top;?>" >
                                        <input type="hidden" name="visitante_left" id="visitante_left" value="<?php echo $data->visitante_left;?>" >
                                        <input type="hidden" name="visitante_w" id="visitante_w" value="<?php echo $data->visitante_w;?>" >
                                        <input type="hidden" name="visitante_h" id="visitante_h" value="<?php echo $data->visitante_h;?>" >
                                        
                                        <!---------------------------------------------------VISITANTE NRO------------------------------------------------------>
                                        <input type="hidden" name="visitantenro_top" id="visitantenro_top" value="<?php echo $data->visitantenro_top;?>" >
                                        <input type="hidden" name="visitantenro_left" id="visitantenro_left" value="<?php echo $data->visitantenro_left;?>" >
                                        <input type="hidden" name="visitantenro_w" id="visitantenro_w" value="<?php echo $data->visitantenro_w;?>" >
                                        <input type="hidden" name="visitantenro_h" id="visitantenro_h" value="<?php echo $data->visitantenro_h;?>" >
                                        
                                        <!---------------------------------------------------LOGO------------------------------------------------------>
                                        <input type="hidden" name="logo_top" id="logo_top" value="<?php echo $data->logo_top;?>" >
                                        <input type="hidden" name="logo_left" id="logo_left" value="<?php echo $data->logo_left;?>" >
                                        <input type="hidden" name="logo_w" id="logo_w" value="<?php echo $data->logo_w;?>" >
                                        <input type="hidden" name="logo_h" id="logo_h" value="<?php echo $data->logo_h;?>" >


                                        <!---------------------------------------------------INSTITUCION------------------------------------------------>
                                        <input type="hidden" name="institucion_top" id="institucion_top" value="<?php echo $data->institucion_top;?>" >
                                        <input type="hidden" name="institucion_left" id="institucion_left" value="<?php echo $data->institucion_left;?>" >
                                        <input type="hidden" name="institucion_w" id="institucion_w" value="<?php echo $data->institucion_w;?>" >
                                        <input type="hidden" name="institucion_h" id="institucion_h" value="<?php echo $data->institucion_h;?>" >
                                        
                                        <!--------------------------------------------------BARCODE----------------------------------------------------->
                                        <input type="hidden" name="barcode_top" id="barcode_top" value="<?php echo $data->barcode_top;?>" >
                                        <input type="hidden" name="barcode_left" id="barcode_left" value="<?php echo $data->barcode_left;?>" >
                                        <input type="hidden" name="barcode_w" id="barcode_w" value="<?php echo $data->barcode_w;?>" >
                                        <input type="hidden" name="barcode_h" id="barcode_h" value="<?php echo $data->barcode_h;?>" >
                                        
                                        <!--------------------------------------------------CONF ESTILOS------------------------------------------------>
                                        <?php
                                        
                                        $ops = array("institucion","visitante","visitantenro");
                                        echo Controller::setStyleInputs($ops,$data);
                                        ?>
                                        
                                        <?php
                                        if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                            echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Carnet\" />";
                                        }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>