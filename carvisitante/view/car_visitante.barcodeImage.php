<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Generedor de codigo de barras
 * Libreria Barcode Generator v4.1.0
 * @link: http://www.barcodephp.com
 * @author: David Concepcion DIDI
 * @var string $_REQUEST["barcode"] Texto para codificar en codigo de barras
 * @var string $_REQUEST["accion"] Operacion a ejecutar
 * @var resource $_REQUEST["barcodeImg"] Resource de la imagen generada
 */
require_once "../controller/car_visitante.control.op.php";// Class CONTROLLER

//--- Barcode Generator v4.1.0 ---//
require_once('../barcode/BCGFontFile.php');
require_once('../barcode/BCGColor.php');
require_once('../barcode/BCGDrawing.php');
require_once('../barcode/BCGcode39.barcode.php');


$font = new BCGFontFile('./../barcode/font/Arial.ttf', 12);
$color_black = new BCGColor(0, 0, 0);
$color_white = new BCGColor(255, 255, 255);
//$color_white->setTransparent(true);

// Barcode Part
$code = new BCGcode39();
$code->setScale(1);
$code->setThickness(20);
$code->setForegroundColor($color_black);
$code->setBackgroundColor($color_white);
$code->setFont($font);
$code->setChecksum(false);
$code->parse($_REQUEST["barcode"]);
$code->clearLabels(); // quitar el texto

// Drawing Part
$drawing = new BCGDrawing('', $color_white);
$drawing->setBarcode($code);
$drawing->draw();


if ($_REQUEST["accion"]=="print"){ // Resource of the image
    $_REQUEST["barcodeImg"] = $drawing->get_im();
}
if ($_REQUEST["accion"]!="print"){  // Print the image on the screen
    header('Content-Type: image/png');
    imagepng($drawing->get_im());
}
?>
