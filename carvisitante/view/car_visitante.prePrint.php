<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Previsualizacion de impresion de carnets
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/car_visitante.control.op.php";// Class CONTROLLER

$obj = new ControlOpCarVisitante();

$data = CarVisitantes::getCarPlantillaVisitante(array("idplantilla"=>$_REQUEST["idplantilla"],"idcarnet_visitante"=>$_REQUEST["idcarnet_visitante"]));

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>        
        <!--script type="text/javascript" src="../js/car_empleado.local.js"></script-->
        
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                text-align: center;
                display: table-cell;
                border:  #aaaaaa dashed 2px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            #divmensaje{
                width:99%;
                position: absolute;
                top: 35px;
                opacity:0.9;
            }
            /*****************************/
            .ui-widget div{
                width: 400px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            //------------------------------------------LOAD-------------------------------------------------------------//
            $(function() {
                $( "input:button, input:submit,button" ).button();
                //-----------------------------------MENSAJE-----------------------------------------------//
                $('#divmensaje').live({
                    click: function() {
                        $(this).fadeOut(500);
                    }
                });
                
                // --- FixSize Carnet --- //                
                $('.imgs').css({
                    width: function(){
                        if ($('#orientacion').val()=="V"){
                            return '300px';
                        }                        
                    },
                    height: function(){
                        if ($('#orientacion').val()=="H"){
                            return '230px';
                        }
                    }
                });
            });
            
            function Accion(acc){
            
                var str;
                var mensaje;
                
                if (acc[0]=="printFront"){
                    mensaje = 'frente';
                }
                if (acc[0]=="printBack"){
                    mensaje = 'reverso';
                }
                
                $('#accion').val(acc[0]);
                $('#op').val(acc[1]);
                if (!confirm('\xbf Esta seguro que desea imprimir el '+mensaje+' del carnet ?')){
                    return false;
                }
                $('#f1').submit();
                
                
                str  = '<div class=\"ui-widget\">';
                str += '    <div class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';
                str += '        <p>';
                str += '            <span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>';
                str += '                El '+mensaje+' del carnet ha sido enviado a la impresora';
                str += '        </p>';
                str += '    </div>';
                str += '</div>';
                $('#divmensaje').html(str).fadeIn(500);
                return true;
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">            
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Carnet</div>                
                <div class="ui-widget">
                    <div class="ui-state-highlight ui-corner-all info" style="margin-top: 20px; padding: 0 .7em;">
                        <span class="icon-info " style="float: left; margin-right: .3em;"></span>
                        <p>
                            Presione la imagen para imprimirla. 
                        </p>
                    </div>
                </div>
                <br>
                <div id="divmensaje"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" id="f1" action="car_visitante.Op.php" target="ifrm1">
                    <input type="hidden" name="idcarnet_visitante" id="idcarnet_visitante" value="<?php echo $_REQUEST["idcarnet_visitante"];?>">
                    <input type="hidden" name="idplantilla" id="idplantilla" value="<?php echo $_REQUEST["idplantilla"];?>">
                    <input type="hidden" name="fixSize" id="fixSize" value="<?php echo $obj->getFixSize() ?>" />
                    <input type="hidden" name="orientacion" id="orientacion" value="<?php echo $data->orientacion ?>" />
                    <input type="hidden" name="accion" id="accion" value="" />
                    <input type="hidden" name="op" id="op" value="" />
                    <div id="datos" >
                        <button type="button" name="prePrintFront" id="prePrintFront" title="Imprimir frente del carnet" onclick="Accion(['printFront','print']);" >
                            <img id="imgFront" class="imgs" src="car_visitante.Op.php?idplantilla=<?php echo $_REQUEST["idplantilla"];?>&idcarnet_visitante=<?php echo $_REQUEST["idcarnet_visitante"];?>&accion=printFront&op=prePrint" />
                        </button>
                        <?php
                        if ($data->orientacion=="H"){
                            echo "<div  style=\"margin-top: 5px;\"></div>";
                        }
                        ?>
                        <button type="button" name="prePrintBack" id="prePrintBack"  title="Imprimir reverso del carnet" onclick="Accion(['printBack','print']);" >
                            <img id="imgFront" class="imgs" src="car_visitante.Op.php?idplantilla=<?php echo $_REQUEST["idplantilla"];?>&idcarnet_visitante=<?php echo $_REQUEST["idcarnet_visitante"];?>&accion=printBack&op=prePrint" />
                        </button>
                    </div>
                </form>
                <?php
            }?>
        </div>
    </body>
</html>