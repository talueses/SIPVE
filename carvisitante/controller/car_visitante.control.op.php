<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CARVIS"); //Categoria del modulo
require_once "car_visitante.control.php";// Class CONTROL ControlCarVisitante()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCarVisitante extends ControlCarVisitante{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCarVisitante(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        
        if ($_REQUEST["idcarnet_visitante"]=="0"){
            $_REQUEST["idcarnet_visitante"] = "";
        }
        //------------------ Metodo Set  -----------------//
        if(!$this->setCarVisitante()) return false;
        
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCarVisitante(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCarVisitante()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Carnet para Visitante");
        
        $this->setCampos("id_plantilla","Plantilla");
        $this->setCampos("nombre","Nombre");
        $this->setCampos("numero","N&uacute;mero");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idplantilla","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"numero","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//        
        
        if (!$this->validarDatos($datos))return false;
        
        return true;
    }
    
    /**
     * Genera imagen unica del carnet con todos los elementos configurados (FRENTE)
     * @param type $op Valor de impresion o pre-impresion (vista previa)
     */
    function setCarnetFront($op){
        
        $data = CarVisitantes::getCarPlantillaVisitante(array("idplantilla"=>$_REQUEST["idplantilla"],"idcarnet_visitante"=>$_REQUEST["idcarnet_visitante"]));
        $data->cargo_bgcolor_chk = "1"; // obligar el color del fondo del cargo
        $param["data"] = $data;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Imagen Plantilla --- //
        $archivo = $this->getFolders("imagesPlantilla").$data->archivo_plantilla;
        $dst_image = imagecreatefromstring(file_get_contents($archivo));
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Redimencionar Imagen Plantilla --- //
        list($src_w,$src_h) = getimagesize($archivo);
        list($newWidth, $newHeight) = $this->getFixedFormatCarnet($data->orientacion);
        $tempImg = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled ($tempImg, $dst_image , 0 ,0 , 0 , 0 , $newWidth , $newHeight , $src_w , $src_h);
        $dst_image  = $tempImg;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN LOGO --- //
        if ($data->logo_chk=="1"){
            $param["archivo"]   = $this->getFolders("imagesPlantilla").$data->archivo_logo;
            $param["dst_image"] = $dst_image;
            $param["obj"] = "logo";
            $dst_image = $this->setImgage($param);
        }                
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN CODIGO DE BARRAS --- //
        if ($data->barcode_front=="1"){
            $_REQUEST["barcode"] = $data->numero;
            $_REQUEST["accion"]="print";
            include_once "../view/car_visitante.barcodeImage.php";
            $param["src_image"] = $_REQUEST["barcodeImg"];
            $param["dst_image"] = $dst_image;
            $param["obj"] = "barcode";
            $dst_image = $this->setImgage($param);
        }
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO INSTITUCION --- //
        if ($data->institucion_chk=="1"){
            $param["dst_image"] = $dst_image;
            $param["obj"] = "institucion";
            $param["text"] = $data->institucion_nombre;
            $dst_image = $this->setText($param);
        }
        //-------------------------------------------------------------------------------------------------------------------//
        // --- ETIQUETA VISITANTE --- //
        $param["dst_image"] = $dst_image;
        $param["obj"] = "visitante";
        $param["text"] = $data->nombre;
        $dst_image = $this->setText($param);
        //-------------------------------------------------------------------------------------------------------------------//
        // --- NUMERO VISITANTE --- //
        $param["dst_image"] = $dst_image;
        $param["obj"] = "visitantenro";
        $param["text"] = sprintf('%04d',$data->numero);
        $dst_image = $this->setText($param);

        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMPRIMIR --- //
        if ($op=="print"){
            self::printCarnet($data,'F');
        }
        
        header('Content-Type: image/jpeg');
        imagejpeg($dst_image,null,100);
        
    }
    
    /**
     * Genera imagen unica del carnet con todos los elementos configurados (REVERSO)
     * @param type $op Valor de impresion o pre-impresion (vista previa)
     */
    function setCarnetBack($op){
        $data = CarVisitantes::getCarPlantillaVisitante(array("idplantilla"=>$_REQUEST["idplantilla"],"idcarnet_visitante"=>$_REQUEST["idcarnet_visitante"]));
        $data->cargo_bgcolor_chk = "1"; // obligar el color del fondo del cargo
        $param["data"] = $data;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Imagen Plantilla --- //
        $archivo = $this->getFolders("imagesPlantilla").$data->archivoback_plantilla;
        $dst_image = imagecreatefromstring(file_get_contents($archivo));
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Redimencionar Imagen Plantilla --- //
        list($src_w,$src_h) = getimagesize($archivo);
        list($newWidth, $newHeight) = $this->getFixedFormatCarnet($data->orientacion);
        $tempImg = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled ($tempImg, $dst_image , 0 ,0 , 0 , 0 , $newWidth , $newHeight , $src_w , $src_h);
        $dst_image  = $tempImg;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN LOGO --- //
        if ($data->logoback_chk=="1"){
            $param["archivo"]   = $this->getFolders("imagesPlantilla").$data->archivoback_logo;
            $param["dst_image"] = $dst_image;
            $param["obj"] = "logoback";
            $dst_image = $this->setImgage($param);
        }     
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN CODIGO DE BARRAS --- //
        if ($data->barcode_back=="1"){
            $_REQUEST["barcode"] = $data->numero;
            $_REQUEST["accion"]="print";
            include_once "../view/car_visitante.barcodeImage.php";
            $param["src_image"] = $_REQUEST["barcodeImg"];
            $param["dst_image"] = $dst_image;
            $param["obj"] = "barcode";
            $dst_image = $this->setImgage($param);
        }       
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO INSTITUCION --- //
        if ($data->institucionback_chk=="1"){
            $param["dst_image"] = $dst_image;
            $param["obj"] = "institucionback";
            $param["text"] = $data->institucion_nombre;
            $dst_image = $this->setText($param);
        }
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMPRIMIR --- //
        if ($op=="print"){
            self::printCarnet($data,'B');
        }
        
        header('Content-Type: image/jpeg');
        imagejpeg($dst_image,null,100);
    }
    
    /**
     * Imprime (descarga) el carnet y guarda log de impresion
     * @param type $data Datos del carnet
     * @param type $modo Indica si es frente o reverso
     */
    private static function printCarnet($data,$modo){
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Descarga Imegen --- //
        if ($modo=="F"){
            $file_nameModo="FRENTE";
        }
        if ($modo=="B"){
            $file_nameModo="REVERSO";
        }        
        $file_name = "CARNET-VISITANTE-".$file_nameModo."-".sprintf('%04d',$data->numero).".jpeg";
        header('Content-Disposition: attachment; filename="'.$file_name.'"'); // download file
        ////-----------------------------------------------------------------------------------------------------------------//
        // --- Log Impresion --- //
        $obj = new ControlOpCarVisitanteImpresion();
        $_REQUEST["idcarnet_visitante"] = $data->idcarnet_visitante;
        $_REQUEST["usuario"] = $_SESSION["usuario"];
        $_REQUEST["modo"] = $modo;
        $obj->setAccion("agregar");
        $obj->setOpCarVisitanteImpresion;
    }
}
?>