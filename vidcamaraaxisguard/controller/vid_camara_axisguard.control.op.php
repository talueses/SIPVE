<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDGRD"); //Categoria del modulo
require_once "../controller/vid_camara_axisguard.control.php";// Class CONTROL ControlVidCamaraAxisguard()
require_once "../../inicio/controller/camaraCommands.control.php";

/**
 * Description
 * @author David Concepcion
 */
class ControlOpVidCamaraAxisguard extends ControlVidCamaraAxisguard{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpVidCamaraAxisguard(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        
        if(!$this->sonValidosDatos()) return false;        
        
        
        // --- Detener todas las guardias al iniciar una --- //
        if ($_REQUEST["running"]=="yes"){
            $obj = new VidCamaraAxisguard();
            $data = VidCamaraAxisguards::getGuardiasCamara($_REQUEST["idcamara"]);
            if (count($data)>0){
                $tmpAccion = $this->getAccion(); 
                $tmpGuardnbr = $_REQUEST["guardnbr"];
                $this->setAccion("stopGuards");
                foreach ($data as $row){
                    // --- Modificar parametros en dispositivo --- //
                    $_REQUEST["guardnbr"] = $row->guardnbr;
                    $this->setComandoCamara();
                    
                    // --- Modificar parametros en BD --- //
                    $obj->setIdcamaraaxisguard($row->idcamaraaxisguard);
                    $obj->setRunning("no");
                    $obj->modificarRegistro($obj->getIdcamaraaxisguard());
                    
                }
                $this->setAccion($tmpAccion); 
                $_REQUEST["guardnbr"] = $tmpGuardnbr;
            }            
        }
        
        
        $res = $this->setComandoCamara();
        $exito = preg_match("/OK/", $res);
        
        if ($this->getAccion()=="agregar"){
            $aux = explode(" ", $res);                         
            $_REQUEST["guardnbr"] = preg_replace("/[A-Za-z]/", "", $aux[0]);
        }
        
        if (!$exito){
            $this->mensaje = "Error al intentar guardar la Guardia en la c&aacute;mara ";
            return false;
        }
        //echo "<p>".$_REQUEST["guardnbr"]."</p>"; 
        
        //------------------ Metodo Set  -----------------//
        if(!$this->setVidCamaraAxisguard()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpVidCamaraAxisguard(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        
        
        $res = $this->setComandoCamara();
        $exito = preg_match("/OK/", $res);
        
        if (!$exito){
            $this->mensaje = "Error al intentar eliminar la Guardia en la c&aacute;mara ";
            return false;
        }        
        
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarVidCamaraAxisguard()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Guardia");
        
        $this->setCampos("idcamara","C&aacute;mara");
        $this->setCampos("guardnbr","Guardnbr");
        $this->setCampos("guardname","Descripci&oacute;n");
        $this->setCampos("running","Activo");
        $this->setCampos("camnbr","Camnbr");
        $this->setCampos("randomenabled","Secuencia aleatoria");
        $this->setCampos("timebetweensequences","Timebetweensequences");
        
    }
    
    function setComandoCamara(){
        $obj = new CamaraCommands();
        
        
        // --- Agregar guardia en el dispositivo --- //
        
        if ($this->getAccion()=="agregar"){
             
            // --- Parametros estaticos ---//
            // -- Action --//            
            $value["action"]  = "add";
            // -- Group --//
            $value["group"]  = "GuardTour";
            // -- Template --//
            $value["template"]  = "guardtour";
            
            // --- Parametros Dinamicos ---//
            // -- Running --//            
            $value["GuardTour.G.Running"]  = $_REQUEST["running"];
            // -- Name --//            
            $value["GuardTour.G.Name"]  = $_REQUEST["guardname"];
            // -- RandomEnabled --//            
            $value["GuardTour.G.RandomEnabled"]  = $_REQUEST["randomenabled"];
            //-- TimeBetweenSequences --//
            $value["GuardTour.G.TimeBetweenSequences"]  = $_REQUEST["timebetweensequences"];
            
        }
        
        // --- Modificar guardia en el dispositivo --- //
        if ($this->getAccion()=="modificar"){
            
            // --- Parametros estaticos ---//
            // -- Action --//            
            $value["action"]  = "update";
            
            // --- Parametros Dinamicos ---//
            // -- Running --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Running"]  = $_REQUEST["running"];
            // -- Name --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Name"]  = urlencode($_REQUEST["guardname"]);
            // -- RandomEnabled --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".RandomEnabled"]  = $_REQUEST["randomenabled"];
        }
        
        // --- Eliminar guardia en el dispositivo --- //        
        if ($this->getAccion()=="eliminar"){
            // -- Action --//        
            $value["action"]  = "remove";
            // -- Group --//
            $value["group"]  = "GuardTour.G".$_REQUEST["guardnbr"];
        }
        
        // --- Detener Guardia --- //
        if ($this->getAccion()=="stopGuards"){
            // -- Action --//            
            $value["action"]  = "update";
            // -- Running --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Running"]  = "no";
        }
        
        $obj->value = $value;
        
        if (!$obj->axis214ipptz_guardTour()){
            $this->mensaje = "Error al establecer parametros para la guardia de la c&aacute;mara ";
            return false;
        }
        
        //echo "--<p>".CamaraCommands::$command."</p>";
        $res = self::sendComandoCamara($_REQUEST["idcamara"]);
        
        return $res;
        
    }
    
    
    /**
     * Envia comandos hacia una camara por httpRequest usando PHP-CURL
     * @param int $idcamara Codigo de la camara
     * @return string Devuelve la respuesta de la ejecucion del comando remoto 
     */
    static function sendComandoCamara($idcamara){        
        
        $param = VidCamaraAxisguards::getCamaraConf($idcamara);
        //echo "<div align='left'><pre>".print_r($param, true).CamaraCommands::$command."</pre></div>";
        $response = self::httpRequest($param->ipv4.":".$param->puerto.CamaraCommands::$command,$param->cam_usuario,$param->cam_clave);

        return $response;
    }
    
}
?>