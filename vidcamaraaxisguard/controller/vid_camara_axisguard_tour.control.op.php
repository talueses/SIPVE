<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDGRD"); //Categoria del modulo
require_once "../controller/vid_camara_axisguard_tour.control.php";// Class CONTROL ControlVidCamaraAxisguardTour()
require_once "../../inicio/controller/camaraCommands.control.php";
/**
 * Description
 * @author David Concepcion
 */
class ControlOpVidCamaraAxisguardTour extends ControlVidCamaraAxisguardTour{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpVidCamaraAxisguardTour(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        $res = $this->setComandoCamara();
        $exito = preg_match("/OK/", $res);
        
        if ($this->getAccion()=="agregar"){
            $aux = explode(" ", $res);                         
            $_REQUEST["tournbr"] = preg_replace("/[A-Za-z]/", "", $aux[0]);
        }
        if (!$exito){
            $this->mensaje = "Error al intentar guardar el Tour de la Guardia en la c&aacute;mara ";
            return false;
        }
        //echo "<p>".$_REQUEST["guardnbr"]."</p>"; 
        
        //------------------ Metodo Set  -----------------//
        if(!$this->setVidCamaraAxisguardTour()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpVidCamaraAxisguardTour(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        
        $res = $this->setComandoCamara();
        $exito = preg_match("/OK/", $res);
        
        if (!$exito){
            $this->mensaje = "Error al intentar eliminar el Tour de la Guardia en la c&aacute;mara ";
            return false;
        }
        
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarVidCamaraAxisguardTour()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Posici&oacute;n");
        
        $this->setCampos("idcamaraaxisguard","Guardia");
        $this->setCampos("idcamaraaxispresetpos","Posici&oacute;n");
        $this->setCampos("position","Secuencia");
        $this->setCampos("movespeed","Velocidad");
        $this->setCampos("waittime","Tiempo de espera");
        $this->setCampos("waittimeviewtype","Tipo de tiempo de espera");     
        $this->setCampos("tournbr","N&uacute;mero Tour");
        
    }
    
    
   function setComandoCamara(){
       
        $row = VidCamaraAxisguardTours::getPresetnbr($_REQUEST["idcamaraaxispresetpos"]);        
        //echo "<div align='left'><pre>".print_r($row,true)."</pre></div>";
        $obj = new CamaraCommands();
        
        
        // --- Agregar guardia en el dispositivo --- //
        
        if ($this->getAccion()=="agregar"){
             
            // --- Parametros estaticos ---//
            // -- Action --//            
            $value["action"]  = "add";
            // -- Group --//
            $value["group"]  = "GuardTour.G".$_REQUEST["guardnbr"].".Tour";
            // -- Template --//
            $value["template"]  = "tour";
            
            // --- Parametros Dinamicos ---//
            // -- PresetNbr --//
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T.PresetNbr"]  =  $row->presetnbr;
            // -- Position --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T.Position"]  = $_REQUEST["position"];
            // -- MoveSpeed --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T.MoveSpeed"]  = $_REQUEST["movespeed"];
            // -- WaitTime --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T.WaitTime"]  = $_REQUEST["waittime"];
            // -- WaitTimeViewType --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T.WaitTimeViewType"]  = $_REQUEST["waittimeviewtype"];
        }
        
        // --- Modificar guardia en el dispositivo --- //
        if ($this->getAccion()=="modificar"){
            
            // --- Parametros estaticos ---//
            // -- Action --//            
            $value["action"]  = "update";
            
            // --- Parametros Dinamicos ---//
            // -- PresetNbr --//
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T".$_REQUEST["tournbr"].".PresetNbr"]  = $row->presetnbr;
            // -- Position --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T".$_REQUEST["tournbr"].".Position"]  = $_REQUEST["position"];
            // -- MoveSpeed --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T".$_REQUEST["tournbr"].".MoveSpeed"]  = $_REQUEST["movespeed"];            
            // -- WaitTime --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T".$_REQUEST["tournbr"].".WaitTime"]  = $_REQUEST["waittime"];
            // -- WaitTimeViewType --//            
            $value["GuardTour.G".$_REQUEST["guardnbr"].".Tour.T".$_REQUEST["tournbr"].".WaitTimeViewType"]  = $_REQUEST["waittimeviewtype"];
        }
        
        // --- Eliminar guardia en el dispositivo --- //        
        if ($this->getAccion()=="eliminar"){
            // -- Action --//        
            $value["action"]  = "remove";
            // -- Group --//
            $value["group"]  = "GuardTour.G".$_REQUEST["guardnbr"].".Tour.T".$_REQUEST["tournbr"];
        }        
        
        $obj->value = $value;
        
        if (!$obj->axis214ipptz_guardTour()){
            $this->mensaje = "Error al establecer parametros para el tour de la guardia de la c&aacute;mara ";
            return false;
        }
        
        //echo "--<p>".CamaraCommands::$command."</p>";
        $res = self::sendComandoCamara($_REQUEST["idcamara"]);
        
        return $res;
        
    }
    
    /**
     * Envia comandos hacia una camara por httpRequest usando PHP-CURL
     * @param int $idcamara Codigo de la camara
     * @return string Devuelve la respuesta de la ejecucion del comando remoto 
     */
    static function sendComandoCamara($idcamara){        
        
        $param = VidCamaraAxisguardTours::getCamaraConf($idcamara);
        //echo "<div align='left'><pre>".print_r($param, true).CamaraCommands::$command."</pre></div>";
        $response = self::httpRequest($param->ipv4.":".$param->puerto.CamaraCommands::$command,$param->cam_usuario,$param->cam_clave);

        return $response;
    }
}
?>