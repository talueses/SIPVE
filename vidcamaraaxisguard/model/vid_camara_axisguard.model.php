<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'vid_camara_axisguard.tabla.php';

/**
 * Clase VidCamaraAxisguards{}
 * @author David Concepcion CENIT-DIDI
 */
class VidCamaraAxisguards extends VidCamaraAxisguard{

    /**
     * Consulta de Guardias de camaras
     * @return object Devuelve un registro como objeto
     */
    function  getCamaraAxisguardias(){
        
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcamaraaxisguardBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcamaraaxisguardBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and vca.idcamaraaxisguard in (".$in.")";
        }
        
        $sql = "select /*start*/
                       vca.idcamaraaxisguard,
                       vca.idcamara,
                       vca.guardnbr,
                       vca.running,
                       vca.camnbr,
                       vca.randomenabled,
                       vca.timebetweensequences,
                       vs.idservidor,
                       vc.camara,
                       vca.guardname
                       /*end*/
                from vid_camara_axisguard vca,
                     vid_camara vc,
                     vid_servidor vs
                where vca.idcamara = vc.idcamara
                  and vc.idservidor = vs.idservidor
                  ".$arg."
                order by vs.idservidor,vc.camara,vca.guardname";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    /**
     * Consulta de una Guardia de una camara
     * @param int $idcamaraaxisguard Codigo
     * @return object Devuelve registros como objeto
     */
    function getCamaraAxisguardia($idcamaraaxisguard){
        $sql = "SELECT vg.idcamaraaxisguard,
                       vg.idcamara,
                       vg.guardnbr,
                       vg.guardname,
                       vg.running,
                       vg.camnbr,
                       vg.randomenabled,
                       vg.timebetweensequences,                       
                       (select count(vt.idvidcamaraaxisguardtour) from vid_camara_axisguard_tour vt where vt.idcamaraaxisguard = vg.idcamaraaxisguard)as countPositions
               FROM vid_camara_axisguard vg
               WHERE vg.idcamaraaxisguard = '".$idcamaraaxisguard."'";
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    
    /**
     * Consulta de posiciones asociadas a el tour de una guardia
     * @param int $idcamaraaxisguard Codigo de la guardia
     * @return object Devuelve registros como objeto
     */
    function getCamaraPosGuardia($idcamaraaxisguard){
        $sql = "select vt.position,
                       vcp.presetname 
                from vid_camara_axisguard_tour vt ,vid_camara_axispresetpos vcp
                where vt.idcamaraaxisguard     = '".$idcamaraaxisguard."'
                  and vt.idcamaraaxispresetpos = vcp.idcamaraaxispresetpos";
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Guardias de una camara
     * @param int $idcamara Codigo de la camara
     * @return object Devuelve un registro como objeto
     */
    static function  getGuardiasCamara($idcamara){
        $sql = "select *
                from vid_camara_axisguard 
                where idcamara = '".$idcamara."'
                order by idcamaraaxisguard";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de configuracion de una camara
     * @param int $idcamaraaxispresetpos Codigo
     * @return object Devuelve registros como objeto
     */
    static function getCamaraConf($idcamara){
        $sql = "select  vc.ipv4,
                        vc.puerto,
                        vtc.urlvideo,
                        concat('http://',vc.ipv4,':',vc.puerto,vtc.urlvideo,'?resolution=320x240') as videoUrl,
                        vc.cam_usuario,
                        vc.cam_clave,
                        vc.rotacion,
                        vtc.ptz
                from vid_camara vc,
                     vid_tipo_camara vtc
                where vc.idcamara = '".$idcamara."' 
                  and vc.idtipo_camara = vtc.idtipo_camara";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    
}
?>
