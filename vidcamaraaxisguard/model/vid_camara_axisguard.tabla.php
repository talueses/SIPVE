<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class VidCamaraAxisguard {
    
    private $idcamaraaxisguard = null;
    private $idcamara = null;
    private $guardnbr = null;
    private $guardname = null;
    private $running = null;
    private $camnbr = null;
    private $randomenabled = null;
    private $timebetweensequences = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcamaraaxisguard($idcamaraaxisguard){
        $this->idcamaraaxisguard = $idcamaraaxisguard;
    }
    public function getIdcamaraaxisguard(){
        return $this->idcamaraaxisguard;
    }
    public function setIdcamara($idcamara){
        $this->idcamara = $idcamara;
    }
    public function getIdcamara(){
        return $this->idcamara;
    }
    public function setGuardnbr($guardnbr){
        $this->guardnbr = $guardnbr;
    }
    public function getGuardnbr(){
        return $this->guardnbr;
    }
    public function setGuardname($guardname){
        $this->guardname = $guardname;
    }
    public function getGuardname(){
        return $this->guardname;
    }
    public function setRunning($running){
        $this->running = $running;
    }
    public function getRunning(){
        return $this->running;
    }
    public function setCamnbr($camnbr){
        $this->camnbr = $camnbr;
    }
    public function getCamnbr(){
        return $this->camnbr;
    }
    public function setRandomenabled($randomenabled){
        $this->randomenabled = $randomenabled;
    }
    public function getRandomenabled(){
        return $this->randomenabled;
    }
    public function setTimebetweensequences($timebetweensequences){
        $this->timebetweensequences = $timebetweensequences;
    }
    public function getTimebetweensequences(){
        return $this->timebetweensequences;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcamaraaxisguard !== null) && (trim($this->idcamaraaxisguard)!=='') ){
            $campos .= "idcamaraaxisguard,";
            $valores .= "'".$this->idcamaraaxisguard."',";
        }
        if(($this->idcamara !== null) && (trim($this->idcamara)!=='') ){
            $campos .= "idcamara,";
            $valores .= "'".$this->idcamara."',";
        }
        if(($this->guardnbr !== null) && (trim($this->guardnbr)!=='') ){
            $campos .= "guardnbr,";
            $valores .= "'".$this->guardnbr."',";
        }
        if(($this->guardname !== null) && (trim($this->guardname)!=='') ){
            $campos .= "guardname,";
            $valores .= "'".$this->guardname."',";
        }
        if(($this->running !== null) && (trim($this->running)!=='') ){
            $campos .= "running,";
            $valores .= "'".$this->running."',";
        }
        if(($this->camnbr !== null) && (trim($this->camnbr)!=='') ){
            $campos .= "camnbr,";
            $valores .= "'".$this->camnbr."',";
        }
        if(($this->randomenabled !== null) && (trim($this->randomenabled)!=='') ){
            $campos .= "randomenabled,";
            $valores .= "'".$this->randomenabled."',";
        }
        if(($this->timebetweensequences !== null) && (trim($this->timebetweensequences)!=='') ){
            $campos .= "timebetweensequences,";
            $valores .= "'".$this->timebetweensequences."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_camara_axisguard $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_camara_axisguard SET ";
        
        if(($this->idcamara !== null) && (trim($this->idcamara)!=='') ){
            $sql .= "idcamara = '".$this->idcamara."',";
        }
        if(($this->guardnbr !== null) && (trim($this->guardnbr)!=='') ){
            $sql .= "guardnbr = '".$this->guardnbr."',";
        }
        if(($this->guardname !== null) && (trim($this->guardname)!=='') ){
            $sql .= "guardname = '".$this->guardname."',";
        }
        if(($this->running !== null) && (trim($this->running)!=='') ){
            $sql .= "running = '".$this->running."',";
        }
        if(($this->camnbr !== null) && (trim($this->camnbr)!=='') ){
            $sql .= "camnbr = '".$this->camnbr."',";
        }
        if(($this->randomenabled !== null) && (trim($this->randomenabled)!=='') ){
            $sql .= "randomenabled = '".$this->randomenabled."',";
        }
        if(($this->timebetweensequences !== null) && (trim($this->timebetweensequences)!=='') ){
            $sql .= "timebetweensequences = '".$this->timebetweensequences."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcamaraaxisguard = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_camara_axisguard  WHERE idcamaraaxisguard = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidCamaraAxisguards
     * @return object Devuelve un registro como objeto
     */
    function  getVidCamaraAxisguards(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcamaraaxisguardBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcamaraaxisguardBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcamaraaxisguard in (".$in.")";
        }
        
        $sql = "select idcamaraaxisguard,idcamara,guardnbr,guardname,running,camnbr,randomenabled,timebetweensequences from vid_camara_axisguard ".$arg." order by guardnbr,guardname";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidCamaraAxisguard
     * @param int $idcamaraaxisguard Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidCamaraAxisguard($idcamaraaxisguard){
        $sql = "SELECT idcamaraaxisguard,idcamara,guardnbr,guardname,running,camnbr,randomenabled,timebetweensequences FROM vid_camara_axisguard WHERE idcamaraaxisguard = '".$idcamaraaxisguard."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>