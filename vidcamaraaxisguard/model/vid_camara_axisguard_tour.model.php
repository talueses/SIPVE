<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'vid_camara_axisguard_tour.tabla.php';

/**
 * Clase VidCamaraAxisguardTours{}
 * @author David Concepcion CENIT-DIDI
 */
class VidCamaraAxisguardTours extends VidCamaraAxisguardTour{

    /**
     * Consulta de posisciones estableciadas para una guardia
     * @param int $idcamaraaxisguard Codigo de la guardia
     * @return object Devuelve registros como objeto
     */
    function  getCamaraAxisGuardTours($idcamaraaxisguard){
        $sql = "select vt.idvidcamaraaxisguardtour,
                       vt.idcamaraaxisguard,
                       vt.idcamaraaxispresetpos,
                       vt.tournbr,
                       vt.position,
                       vt.movespeed,
                       vt.waittime,
                       vt.\"WaitTimeViewType\",
                       pos.presetname
               from vid_camara_axisguard_tour vt,
                    vid_camara_axispresetpos pos
               where vt.idcamaraaxisguard     = '".$idcamaraaxisguard."'
                 and vt.idcamaraaxispresetpos = pos.idcamaraaxispresetpos
               order by vt.idvidcamaraaxisguardtour";
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    /**
     * Conrulta de el numero de la posicion de una camara
     * @param int $idcamaraaxispresetpos Codigo de la posicion
     * @return object Devuelve un registro como objeto
     */
    static function getPresetnbr($idcamaraaxispresetpos){
        $sql = "select presetnbr-1 as presetnbr from vid_camara_axispresetpos where idcamaraaxispresetpos = '".$idcamaraaxispresetpos."'";
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    
    /**
     * Consulta de configuracion de una camara
     * @param int $idcamaraaxispresetpos Codigo
     * @return object Devuelve registros como objeto
     */
    static function getCamaraConf($idcamara){
        //$sql = "SELECT idcamaraaxispresetpos,idcamara,presetnbr,presetname FROM vid_camara_axispresetpos WHERE idcamaraaxispresetpos = '".$idcamaraaxispresetpos."'";
        $sql = "select  vc.ipv4,
                        vc.puerto,
                        vtc.urlvideo,
                        concat('http://',vc.ipv4,':',vc.puerto,vtc.urlvideo,'?resolution=320x240') as \"videoUrl\",
                        vc.cam_usuario,
                        vc.cam_clave,
                        vc.rotacion,
                        vtc.ptz
                from vid_camara vc,
                     vid_tipo_camara vtc
                where vc.idcamara = '".$idcamara."' 
                  and vc.idtipo_camara = vtc.idtipo_camara";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    
}
?>
