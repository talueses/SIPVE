<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class VidCamaraAxisguardTour {
    
    private $idvidcamaraaxisguardtour = null;
    private $idcamaraaxisguard = null;
    private $idcamaraaxispresetpos = null;
    private $tournbr = null;
    private $position = null;
    private $movespeed = null;
    private $waittime = null;
    private $waittimeviewtype = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdvidcamaraaxisguardtour($idvidcamaraaxisguardtour){
        $this->idvidcamaraaxisguardtour = $idvidcamaraaxisguardtour;
    }
    public function getIdvidcamaraaxisguardtour(){
        return $this->idvidcamaraaxisguardtour;
    }
    public function setIdcamaraaxisguard($idcamaraaxisguard){
        $this->idcamaraaxisguard = $idcamaraaxisguard;
    }
    public function getIdcamaraaxisguard(){
        return $this->idcamaraaxisguard;
    }
    public function setIdcamaraaxispresetpos($idcamaraaxispresetpos){
        $this->idcamaraaxispresetpos = $idcamaraaxispresetpos;
    }
    public function getIdcamaraaxispresetpos(){
        return $this->idcamaraaxispresetpos;
    }
    public function setTournbr($tournbr){
        $this->tournbr = $tournbr;
    }
    public function getTournbr(){
        return $this->tournbr;
    }
    public function setPosition($position){
        $this->position = $position;
    }
    public function getPosition(){
        return $this->position;
    }
    public function setMovespeed($movespeed){
        $this->movespeed = $movespeed;
    }
    public function getMovespeed(){
        return $this->movespeed;
    }
    public function setWaittime($waittime){
        $this->waittime = $waittime;
    }
    public function getWaittime(){
        return $this->waittime;
    }
    public function setWaittimeviewtype($WaitTimeViewType){
        $this->WaitTimeViewType = $WaitTimeViewType;
    }
    public function getWaittimeviewtype(){
        return $this->WaitTimeViewType;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idvidcamaraaxisguardtour !== null) && (trim($this->idvidcamaraaxisguardtour)!=='') ){
            $campos .= "idvidcamaraaxisguardtour,";
            $valores .= "'".$this->idvidcamaraaxisguardtour."',";
        }
        if(($this->idcamaraaxisguard !== null) && (trim($this->idcamaraaxisguard)!=='') ){
            $campos .= "idcamaraaxisguard,";
            $valores .= "'".$this->idcamaraaxisguard."',";
        }
        if(($this->idcamaraaxispresetpos !== null) && (trim($this->idcamaraaxispresetpos)!=='') ){
            $campos .= "idcamaraaxispresetpos,";
            $valores .= "'".$this->idcamaraaxispresetpos."',";
        }
        if(($this->tournbr !== null) && (trim($this->tournbr)!=='') ){
            $campos .= "tournbr,";
            $valores .= "'".$this->tournbr."',";
        }
        if(($this->position !== null) && (trim($this->position)!=='') ){
            $campos .= "position,";
            $valores .= "'".$this->position."',";
        }
        if(($this->movespeed !== null) && (trim($this->movespeed)!=='') ){
            $campos .= "movespeed,";
            $valores .= "'".$this->movespeed."',";
        }
        if(($this->waittime !== null) && (trim($this->waittime)!=='') ){
            $campos .= "waittime,";
            $valores .= "'".$this->waittime."',";
        }
        if(($this->WaitTimeViewType !== null) && (trim($this->WaitTimeViewType)!=='') ){
            $campos .= "\"WaitTimeViewType\",";
            $valores .= "'".$this->WaitTimeViewType."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_camara_axisguard_tour $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_camara_axisguard_tour SET ";
        
        if(($this->idcamaraaxisguard !== null) && (trim($this->idcamaraaxisguard)!=='') ){
            $sql .= "idcamaraaxisguard = '".$this->idcamaraaxisguard."',";
        }
        if(($this->idcamaraaxispresetpos !== null) && (trim($this->idcamaraaxispresetpos)!=='') ){
            $sql .= "idcamaraaxispresetpos = '".$this->idcamaraaxispresetpos."',";
        }
        if(($this->tournbr !== null) && (trim($this->tournbr)!=='') ){
            $sql .= "tournbr = '".$this->tournbr."',";
        }
        if(($this->position !== null) && (trim($this->position)!=='') ){
            $sql .= "position = '".$this->position."',";
        }
        if(($this->movespeed !== null) && (trim($this->movespeed)!=='') ){
            $sql .= "movespeed = '".$this->movespeed."',";
        }
        if(($this->waittime !== null) && (trim($this->waittime)!=='') ){
            $sql .= "waittime = '".$this->waittime."',";
        }
        if(($this->WaitTimeViewType !== null) && (trim($this->WaitTimeViewType)!=='') ){
            $sql .= "\"WaitTimeViewType\" = '".$this->WaitTimeViewType."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idvidcamaraaxisguardtour = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_camara_axisguard_tour  WHERE idvidcamaraaxisguardtour = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidCamaraAxisguardTours
     * @return object Devuelve un registro como objeto
     */
    function  getVidCamaraAxisguardTours(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idvidcamaraaxisguardtourBuscador"]){
            $in = null;
            foreach ($_REQUEST["idvidcamaraaxisguardtourBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idvidcamaraaxisguardtour in (".$in.")";
        }
        
        $sql = "select idvidcamaraaxisguardtour,idcamaraaxisguard,idcamaraaxispresetpos,tournbr,position,movespeed,waittime,\"WaitTimeViewType\" from vid_camara_axisguard_tour ".$arg." order by tournbr,position";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidCamaraAxisguardTour
     * @param int $idvidcamaraaxisguardtour Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidCamaraAxisguardTour($idvidcamaraaxisguardtour){
        $sql = "SELECT idvidcamaraaxisguardtour,idcamaraaxisguard,idcamaraaxispresetpos,tournbr,position,movespeed,waittime,WaitTimeViewType FROM vid_camara_axisguard_tour WHERE idvidcamaraaxisguardtour = '".$idvidcamaraaxisguardtour."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>