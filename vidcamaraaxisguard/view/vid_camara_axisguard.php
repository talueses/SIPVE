<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de horarios
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/vid_camara_axisguard.control.op.php";// Class CONTROLLER

$obj  = new VidCamaraAxisguards();
$data = $obj->getCamaraAxisguardias();

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>VidCamaraAxisguards</title>
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #tabla{
                float:left;
                width:30%;
                height:95%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
            });
            function visualizar(fila){
                var aux = fila.id.split('-');                
                $('#idcamaraaxisguard').val(aux[0]);
                $('#idcamara').val(aux[1]);
                $('#guardnbr').val(aux[2]);
                
                $('#accion').val('visualizar');
                $('#f1').attr('action','vid_camara_axisguard.Acc.php');
                $('#f1').submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    $('#'+fila.id).addClass('chequeada');
            }

            function omover(fila){                
                    if (fila.id===filaseleccionada.id ) return false;                                        
                    $('#'+fila.id).addClass('pasada');
            }
            
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;                    
                    $('#'+fila.id).removeClass('pasada');
            }
            function Accion(acc){
                $('#accion').val(acc);
                $('#f1').attr('action','vid_camara_axisguard.Acc.php');

                if (acc == "modificar" || acc == "eliminar" || acc == "enviarArchivo"){
                    if ($('#idcamaraaxisguard').val()==""){
                        alert("Debe seleccionar un registro de la lista");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("\xbf Esta seguro que desea eliminar el registro ?")){
                            return false;
                        }
                        $('#f1').attr('action','vid_camara_axisguard.Op.php');
                    }
                }

                $('#f1').submit();
                return true;
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <div id="tabla" align="center">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                    echo Controller::$mensajePermisos;
                }else{
                    echo Buscador::getObjBuscador("vca.idcamaraaxisguard", "vc.idservidor,vc.camara,vca.guardname", "vid_camara_axisguard vca,vid_camara vc", " where vca.idcamara = vc.idcamara order by vc.idservidor,vc.camara,vca.guardname");
                    ?>
                    <table id="listado">
                            <tr>
                                <td id="titulo" colspan="4" align="center"><b>Guardias</b></td>
                            </tr>
                            <tr>
                                <th align="center">N&deg;</th>
                                <th>C&aacute;mara</th>
                                <th>Descripci&oacute;n</th>
                                <th>Activo</th>

                            </tr>
                            <?php
                            if (count($data) > 0){
                                foreach ($data as $key => $row){?>
                                    <tr id="<?php echo $row->idcamaraaxisguard."-".$row->idcamara."-".$row->guardnbr;?>" onclick="visualizar(this);oclic(this)" onmouseover="omover(this)" onmouseout="omout(this)">
                                        <td align="center"><b><?php echo paginationSQL::$start+$key+1;?></b></td>
                                        <td><?php echo "<b>".$row->idservidor."</b> - ".$row->camara;?></td>
                                        <td><?php echo $row->guardname;?></td>
                                        <td align="center">
                                            <?php
                                            if ($row->running=="yes"){
                                                ?>
                                                <b><font style="color: #006400" >S&iacute;</font></b>
                                                <?php
                                            }
                                            if ($row->running=="no"){
                                                ?>
                                                <b><font style="color: #B22222" >No</font></b>
                                                <?php
                                            }?>
                                        </td>

                                    </tr>
                                <?php
                                }
                            }else{
                                ?>
                                <tr>
                                    <td colspan="4">
                                        <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                    </td>
                                </tr>

                                <?php
                            }
                            ?>

                    </table>
                    <?php echo paginationSQL::links();?>
                    <div id="botones" >
                        <center>
                            <form name="f1" id="f1" target="frm1" action="vid_camara_axisguard.Acc.php" method="POST">
                                <input type="hidden" name="idcamaraaxisguard" id="idcamaraaxisguard" value="">
                                <input type="hidden" name="accion" id="accion" >
                                <input type="hidden" name="guardnbr" id="guardnbr" >
                                <input type="hidden" name="idcamara" id="idcamara" >
                                <?php
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"agregar")){
                                    ?><input type="submit" value="Agregar" onclick="return Accion('agregar')"  class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"modificar")){
                                    ?><input type="submit" value="Modificar" onclick="return Accion('modificar')" class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"eliminar")){
                                    ?><input type="button" value="Eliminar" onclick="return Accion('eliminar')" class="boton"><?php
                                }
                                ?>
                                <input type="submit" value="Ayuda" 	onclick="this.form.action='vid_camara_axisguard.Ayuda.php'" class="boton">
                            </form>
                        </center>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                <iframe name="frm1" id="frm1" frameborder="0" scrolling="no" style="width:100%; height:650px" src="vid_camara_axisguard.Acc.php?accion=agregar"></iframe>
            </div>
        </div>        
    </body>
</html>