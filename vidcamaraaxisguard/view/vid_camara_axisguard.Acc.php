<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/vid_camara_axisguard.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new VidCamaraAxisguards();
    //$data = $obj->getVidCamaraAxisguard($_REQUEST["idcamaraaxisguard"]);
    $data = $obj->getCamaraAxisguardia($_REQUEST["idcamaraaxisguard"]);
    $dataPos = $obj->getCamaraPosGuardia($_REQUEST["idcamaraaxisguard"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = false;
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 200px;
                border: 1px solid #ccc0a9;                
                
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();
                $('#radioRunning, #radioRandomenabled').buttonset();
               <?php
               if ($data->running=="yes"){
                   ?>
                   $('#setPosButton').button({ disabled: true });                   
                   <?php
               }
               ?>
                
                $('#presetPosDialog').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    width: 410,//390
                    height:510,//460
                    position:'top',
                    modal:true,
                    open: function(event, ui) { 
                       $('#presetPosFrame').attr('src','vid_camara_axisguard_tour.php?idcamaraaxisguard='+$('#idcamaraaxisguard').val());
                    },
                    close: function(event, ui) { 
                       //$('#presetPosFrame').attr('src','');
                       document.location.href = 'vid_camara_axisguard.Acc.php?idcamaraaxisguard='+$('#idcamaraaxisguard').val()+'&accion='+$('#accion').val();
                    }
                }); 
                <?php
                if ($disabled){
                    ?>
                    $('#radioRunning, #radioRandomenabled').button({ disabled: true });
                    <?
                }
                ?>
            });
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Guardia</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="vid_camara_axisguard.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr title="C&aacute;mara">
                                <td align="right">C&aacute;mara:</td>
                                <td>
                                    <small class="comment">S&oacute;lo se listan las C&aacute;maras Axis 214 IPPTZ </small>
                                    <select name="idcamara" id="idcamara" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php

                                        if (count($dataPos) > 0){
                                            $arg = "and vc.idcamara = '".$data->idcamara."'";
                                        }

                                        echo ControlVidCamaraAxisguard::make_combo("vid_servidor vs,vid_camara vc, vid_tipo_camara vtc, vid_modelo_camara vmc, vid_marca_camara vmac","where vs.idservidor = vc.idservidor and vc.idtipo_camara = vtc.idtipo_camara and vtc.idmodelo_camara = vmc.idmodelo_camara and vmc.idmarca_camara = vmac.idmarca_camara and upper(vtc.nombre_tipo_camara) = 'IPPTZ' and upper(vmc.nombre_modelo_camara) = '214' and upper(vmac.nombre_marca_camara) = 'AXIS' and (select count(vp.idcamaraaxispresetpos) from vid_camara_axispresetpos vp where vp.idcamara = vc.idcamara) > 0 ".$arg." order by vc.idservidor, vc.camara", "vc.idcamara", "vc.idservidor ,vc.camara", $data->idcamara,true);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Nombre descriptivo de la guardia">
                                <td align="right">Descripci&oacute;n:</td>
                                <td>
                                    <input type="text" name="guardname" id="guardname" maxlength="100" value="<?php echo $data->guardname;?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Ejecuci&o&oacute;n de la guardia">
                                <td align="right">Activo:</td>
                                <td>
                                    <div id="radioRunning" >
                                        <?php
                                        if ($_REQUEST["accion"] != "agregar" && count($dataPos) > 0){
                                            ?>
                                            <input type="radio" name="running" id="radioRunning1" value="yes" <?php echo $data->running=="yes"?"checked":"";?> <?php echo $disabled;?> /><label for="radioRunning1">S&iacute;</label>
                                            <?php
                                        }
                                        ?>
                                        <input type="radio" name="running" id="radioRunning2" value="no" <?php echo ($data->running=="no" || !$data->running )?"checked":"";?>/><label for="radioRunning2">No</label>
                                    </div>
                                </td>
                            </tr>
                            <tr title="Ejecuci&oacute;n de secuencia aleatoria de posiciones asociadas a la guardia">
                                <td align="right">Secuencia aleatoria:</td>
                                <td>
                                    <div id="radioRandomenabled" >
                                        <input type="radio" name="randomenabled" id="radioRandomenabled1" value="yes" <?php echo $data->randomenabled=="yes"?"checked":"";?> <?php echo $disabled;?> /><label for="radioRandomenabled1">S&iacute;</label>
                                        <input type="radio" name="randomenabled" id="radioRandomenabled2" value="no" <?php echo ($data->randomenabled=="no" || !$data->randomenabled )?"checked":"";?>/><label for="radioRandomenabled2">No</label>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            // ---------- LISTADO DE POSICIONES ASOCIADAS -------------------------//
                            if (count($dataPos)){
                                ?>
                                <tr title="Posiciones Establecidas para &eacute;sta guardia">
                                    <td align="right" valign="top">Posiciones Establecidas:</td>
                                    <td class="list">
                                        <pre><?php
                                            foreach ($dataPos as $row){

                                                echo "&nbsp;&nbsp;".$row->position." - ".$row->presetname."\n";

                                            }
                                        ?></pre>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                    <div id="presetPosDialog" title="Establecer recorrido de la guardia: <i><?php echo $data->guardname;?></i>">
                        <iframe name="presetPosFrame" id="presetPosFrame" width="390" height="460" frameborder="0" ></iframe>
                    </div>

                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="guardnbr" id="guardnbr" value="<?php echo $data->guardnbr==""?"9999":$data->guardnbr?>" <?php echo $disabled;?> />
                        <input type="hidden" name="timebetweensequences" id="timebetweensequences" value="<?php echo $data->timebetweensequences==""?"0":$data->timebetweensequences?>" <?php echo $disabled;?> />

                            <input type="hidden" name="idcamaraaxisguard" id="idcamaraaxisguard" value="<?php echo $data->idcamaraaxisguard ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Guardia\" />";
                                if ($_REQUEST["accion"] == "modificar" && Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"grdPos")){
                                    echo "&nbsp;&nbsp;&nbsp;";
                                    echo "<input type=\"button\" name=\"setPosButton\" id=\"setPosButton\" value=\"Establecer Posiciones\" onclick=\"$('#presetPosDialog').dialog('open');\" />";
                                }
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>