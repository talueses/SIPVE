<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de horarios
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/vid_camara_axisguard_tour.control.op.php";// Class CONTROLLER

$obj  = new VidCamaraAxisguardTours();
$data = $obj->getCamaraAxisGuardTours($_REQUEST["idcamaraaxisguard"]);
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>VidCamaraAxisguardTours</title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #tabla{
                width:99%;
                height:95%;
                border:  #aaaaaa solid 1px;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
            
            img.accion{
                cursor: pointer;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();
                $('#guardnbr').val( $('#guardnbr',parent.document).val());
                $('#idcamara').val( $('#idcamara',parent.document).val());
            });
            function visualizar(fila){
                $('#idvidcamaraaxisguardtour').val(fila.id);
                //$('#accion').val('visualizar');
                //$('#f1').attr('action','vid_camara_axisguard_tour.Acc.php');
                //$('#f1').submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    $('#'+fila.id).attr("className",'chequeada');
            }

            function omover(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    //fila.className = 'pasada';
                    $('#'+fila.id).attr("className",'pasada');


            }
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    $('#'+fila.id).attr("className",'');
            }
            function Accion(acc){
                $('#accion').val(acc);
                $('#f1').attr('action','vid_camara_axisguard_tour.Acc.php');

                if (acc == "modificar" || acc == "eliminar" || acc == "enviarArchivo"){
                    if ($('#idvidcamaraaxisguardtour').val()==""){
                        alert("Debe seleccionar un registro de la lista");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("\xbf Esta seguro que desea eliminar el registro ?")){
                            return false;
                        }
                        $('#f1').attr('action','vid_camara_axisguard_tour.Op.php');
                    }
                }

                $('#f1').submit();
                return true;
            }
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <div id="principal" style="width:99%;" >
            <div id="tabla" align="center">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"grdPos")){
                    echo Controller::$mensajePermisos;
                }else{
                    ?>
                    <table id="listado">
                            <tr>
                                <th width="80%">Position</th>
                                <th width="20%" align="center" title="Agregar">
                                    <img class="accion" src="../images/sub_black_add-20.png" onclick="return Accion('agregar')" />
                                </th>

                            </tr>
                            <?php
                            $setPosition = null;
                            if (count($data) > 0){
                                foreach ($data as $row){?>
                                    <tr id="<?php echo $row->idvidcamaraaxisguardtour;?>" onmouseover="omover(this)" onmouseout="omout(this)">
                                        <td><?php echo $row->presetname;?></td>
                                        <td align="center">
                                            <img title="Modicicar" class="accion" src="../images/Notes-edit-20.png" onclick="$('#idvidcamaraaxisguardtour').val('<?php echo $row->idvidcamaraaxisguardtour;?>');return Accion('modificar')" />
                                            &nbsp;&nbsp;
                                            <img title="Eliminar" class="accion" src="../images/sub_black_delete-20.png" onclick="$('#idvidcamaraaxisguardtour').val('<?php echo $row->idvidcamaraaxisguardtour;?>');$('#tournbr').val('<?php echo $row->tournbr;?>');return Accion('eliminar')" />
                                        </td>
                                    </tr>
                                    <?php
                                    $countPos++;
                                    //echo "<p>$countPos!=$row->position && !$setPosition</p>";
                                    if ($countPos!=$row->position && !$setPosition){
                                        $setPosition = $countPos;
                                    }
                                }

                            }else{
                                ?>
                                <tr>
                                    <td colspan="3">
                                        <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                    </td>
                                </tr>

                                <?php
                            }
                            if (!$setPosition){
                                $countPos++;
                                $setPosition = $countPos;
                            }
                            ?>
                    </table>
                    <div id="botones" >
                        <center>
                            <form name="f1" id="f1" action="vid_camara_axisguard_tour.Acc.php" method="POST">
                                <input type="hidden" name="idvidcamaraaxisguardtour" id="idvidcamaraaxisguardtour">
                                <input type="hidden" name="idcamaraaxisguard" id="idcamaraaxisguard" value="<?php echo $_REQUEST["idcamaraaxisguard"]?>" />
                                <input type="hidden" name="setPosition" id="setPosition" value="<?php echo $setPosition?>" />
                                <input type="hidden" name="idcamara" id="idcamara"  />
                                <input type="hidden" name="guardnbr" id="guardnbr" />
                                <input type="hidden" name="tournbr" id="tournbr"  />

                                <input type="hidden" name="accion" id="accion" value="">
                                <!--<input type="submit" value="Agregar" 	onclick="return Accion('agregar')"  class="boton">
                                <input type="submit" value="Modificar"	onclick="return Accion('modificar')" class="boton">
                                <input type="button" value="Eliminar"	onclick="return Accion('eliminar')" class="boton">
                                <input type="submit" value="Ayuda" 	onclick="this.form.action='vid_camara_axisguard_tour.Ayuda.php'" class="boton">-->
                            </form>
                        </center>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>        
    </body>
</html>
