<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/vid_camara_axisguard_tour.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new VidCamaraAxisguardTours();
    $data = $obj->getVidCamaraAxisguardTour($_REQUEST["idvidcamaraaxisguardtour"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                width:95%;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 95%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();
                $('#guardnbr').val( $('#guardnbr',parent.document).val());
                $('#idcamara').val( $('#idcamara',parent.document).val());
            });
            
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <div id="divmensaje" style="width:99%;"></div>
        <br />
        <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
        <form method="POST" name="f1" action="vid_camara_axisguard_tour.Op.php" target="ifrm1">
            <div id="contenido" align="center">            
                    <table>
                        <tr title="Guardia">
                            <td align="right">Guardia:</td>
                            <td>
                                <select  disabled>                                    
                                    <?php echo ControlVidCamaraAxisguardTour::make_combo("vid_camara_axisguard","where idcamaraaxisguard = '".$_REQUEST["idcamaraaxisguard"]."'", "idcamaraaxisguard", "guardname", $_REQUEST["idcamaraaxisguard"],false);?>
                                </select>
                            </td>
                        </tr>
                        <tr title="Posiciones de la C&aacute;mara">
                            <td align="right">Posiciones de la C&aacute;mara:</td>
                            <td>
                                <select name="idcamaraaxispresetpos" id="idcamaraaxispresetpos" <?php echo $disabled;?>>
                                    <option value="">Seleccione</option>
                                    <?php echo ControlVidCamaraAxisguardTour::make_combo("vid_camara_axispresetpos pos","where pos.idcamara = (select vg.idcamara from vid_camara_axisguard vg where vg.idcamaraaxisguard ='".$_REQUEST["idcamaraaxisguard"]."') order by pos.presetname", "pos.idcamaraaxispresetpos", "pos.presetname", $data->idcamaraaxispresetpos,false);?>
                                </select>
                            </td>
                        </tr>

                    </table>
            </div>
            <br />
            <div id="botones" align="center">
                <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                <input type="hidden" name="idvidcamaraaxisguardtour" id="idvidcamaraaxisguardtour" value="<?php echo $data->idvidcamaraaxisguardtour ?>">
                <input type="hidden" name="idcamaraaxisguard" id="idcamaraaxisguard" value="<?php echo $_REQUEST["idcamaraaxisguard"]?>" />
                <input type="hidden" name="idcamara" id="idcamara"  />
                <input type="hidden" name="guardnbr" id="guardnbr" />
                
                <input type="hidden" name="tournbr" id="tournbr" value="<?php echo $data->tournbr==""?"9999":$data->tournbr;?>" >
                <input type="hidden" name="position" id="position" value="<?php echo $data->position==""?$_REQUEST["setPosition"]:$data->position;?>" >
                <input type="hidden" name="movespeed" id="movespeed" value="<?php echo $data->movespeed==""?"5":$data->movespeed;?>" >
                <input type="hidden" name="waittime" id="waittime" value="<?php echo $data->waittime==""?"1":$data->waittime;?>" >
                <input type="hidden" name="waittimeviewtype" id="waittimeviewtype" value="<?php echo $data->waittimeviewtype==""?"seconds":$data->waittimeviewtype;?>" >
                    <?php
                    if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                        echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Punto\" />&nbsp;&nbsp;&nbsp;";
                        echo "<input type=\"button\" onclick=\"document.location.href='vid_camara_axisguard_tour.php?idcamaraaxisguard=".$_REQUEST["idcamaraaxisguard"]."'\" value=\"Volver\" />";
                    }
                    ?>
            </div>
        </form>
    </body>
</html>