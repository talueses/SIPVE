<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php

require_once "../../ctrlsincronizar/controller/ctrl_sincronizar.control.php";// Class CONTROL ControlCtrlSincronizar()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlSincronizar extends ControlCtrlSincronizar{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlSincronizar($acc){
        
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        //echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
        $this->setAccion("agregar");
        
        if ($acc=="sincReloj"){
            $_REQUEST["item"]         = "Reloj"; 
            $_REQUEST["accion"]       = "Sincronizar Reloj";
            $_REQUEST["codigoaccion"] = "5";
            $_REQUEST["prioridad"]    = "100";
        }
        if ($acc=="sincHorarios"){
            $_REQUEST["item"]         = "Timezone";
            $_REQUEST["accion"]       = "Modificar Zonas de Tiempo";
            $_REQUEST["codigoaccion"] = "7";
            $_REQUEST["prioridad"]    = "85";
            
            $dataHorarios = CtrlSincronizars::getHorarios();
        }
        if ($acc=="sincGrpPuertas"){
            $_REQUEST["item"]         = "Grupo Puertas";
            $_REQUEST["accion"]       = "Modificar Grupo de Puertas";
            $_REQUEST["codigoaccion"] = "8";
            $_REQUEST["prioridad"]    = "75";
            
            $dataGrupos = CtrlSincronizars::getGrupo();
            
            $dataGrpPuertas = array();
            foreach ($dataGrupos as $rowGrupos){
                $this->unsetGrupoDePuertas();
                
                $dataPuertas = CtrlSincronizars::getGrpPuertas($rowGrupos->idgrupo);
                foreach ($dataPuertas as $rowPuertas){
                    $this->agregarPuerta($rowPuertas->numeroPuerta);
                }
                
                $str_grupo = $this->getGrupoStringEmpaquetado();                
                $dataGrpPuertas[]= (object) array("numeroGrupo"   => $rowGrupos->numeroGrupo,
                                                  "idgrupo_padre" => $rowGrupos->idgrupo_padre?$rowGrupos->idgrupo_padre:"0",
                                                  "nivel"         => $rowGrupos->nivel,
                                                  "numeroPuerta1" => substr($str_grupo,0,40),
                                                  "numeroPuerta2" => substr($str_grupo,40,40));
            }
        }
        if ($acc=="sincTrajetasAcc"){
            $_REQUEST["item"]         = "Usuario";          
            $_REQUEST["accion"]       = "Escribir Usuario"; 
            $_REQUEST["codigoaccion"] = "4";                
            $_REQUEST["prioridad"]    = "100";              
            $dataTarjetasAcceso = CtrlSincronizars::getTarjetasAcceso();
        }
        if ($acc=="sincFeriados"){
            $_REQUEST["item"]         = "Feriados";         
            $_REQUEST["accion"]       = "Modificar Dia Feriado";
            $_REQUEST["codigoaccion"] = "6";                    
            $_REQUEST["prioridad"]    = "75";                   
            $dataFeriados = CtrlSincronizars::getDiasFeriados();
        }
        
        
        $data = CtrlSincronizars::getControladoras($_REQUEST["idcontroladora"]);
        
        if (count($data) > 0){
            //echo "<div align='left'><pre>".print_r($data,true)."</pre></div>";
            foreach ($data as $key => $row){

                $_REQUEST["nodo"]   = $row->nodo;
                $_REQUEST["ipv4"]   = $row->ipv4;
                $_REQUEST["puerto"] = $row->puerto;
                
                
                if ($acc=="sincReloj"){
                    //------------------ Ingresar registro en CtrlSincronizar -----------------//
                    if(!$this->setCtrlSincronizar()) return false;
                }
                
                if ($acc=="sincHorarios"){
                    foreach ($dataHorarios as $key => $rowHorarios){

                        $_REQUEST["param1"] = $rowHorarios->indice;              
                        $_REQUEST["param2"] = $rowHorarios->disponibleenferiado; 
                        $_REQUEST["param3"] = $rowHorarios->nivel;               

                        $_REQUEST["param4"] = $rowHorarios->domingodesde;
                        $_REQUEST["param5"] = $rowHorarios->domingohasta;
                        $_REQUEST["param6"] = $rowHorarios->lunesdesde;
                        $_REQUEST["param7"] = $rowHorarios->luneshasta;
                        $_REQUEST["param8"] = $rowHorarios->martesdesde;
                        $_REQUEST["param9"] = $rowHorarios->marteshasta;
                        $_REQUEST["param10"] = $rowHorarios->miercolesdesde;
                        $_REQUEST["param11"] = $rowHorarios->miercoleshasta;
                        $_REQUEST["param12"] = $rowHorarios->juevesdesde;
                        $_REQUEST["param13"] = $rowHorarios->jueveshasta;
                        $_REQUEST["param14"] = $rowHorarios->viernesdesde;
                        $_REQUEST["param15"] = $rowHorarios->vierneshasta;
                        $_REQUEST["param16"] = $rowHorarios->sabadodesde;
                        $_REQUEST["param17"] = $rowHorarios->sabadohasta;
                        $_REQUEST["param18"] = $rowHorarios->indicelink; 


                        //------------------ Ingresar registro en CtrlSincronizar -----------------//
                        if(!$this->setCtrlSincronizar()) return false;
                    }
                }
                if ($acc=="sincGrpPuertas"){
                    foreach ($dataGrpPuertas as $key => $rowGrpPuertas){
                        $_REQUEST["param1"] = $rowGrpPuertas->numeroGrupo; 
                        $_REQUEST["param2"] = $rowGrpPuertas->idgrupo_padre;
                        $_REQUEST["param3"] = $rowGrpPuertas->nivel;       

                        $_REQUEST["param4"] = $rowGrpPuertas->numeroPuerta1;
                        $_REQUEST["param5"] = $rowGrpPuertas->numeroPuerta2;
                        
                        //------------------ Ingresar registro en CtrlSincronizar -----------------//
                        if(!$this->setCtrlSincronizar()) return false;
                    }
                    
                }
                if ($acc=="sincTrajetasAcc"){
                    foreach ($dataTarjetasAcceso as $key => $rowTarjetasAcceso){
                        $_REQUEST["param1"]  = $rowTarjetasAcceso->numeroUsuario;
                        $_REQUEST["param2"]  = $rowTarjetasAcceso->codigo_pin;
                        $_REQUEST["param3"]  = $rowTarjetasAcceso->codigo_tarjeta;
                        $_REQUEST["param4"]  = $rowTarjetasAcceso->codigo_sitio;
                        $_REQUEST["param5"]  = $rowTarjetasAcceso->tarjetayopin;
                        $_REQUEST["param6"]  = $rowTarjetasAcceso->tienefechalimite?$rowHorarios->tienefechalimite:"0";
                        $_REQUEST["param7"]  = $rowTarjetasAcceso->modificable;  
                        $_REQUEST["param8"]  = $rowTarjetasAcceso->rondasguardias;
                        $_REQUEST["param9"]  = $rowTarjetasAcceso->skipfpcheck;
                        $_REQUEST["param10"] = $rowTarjetasAcceso->numeroGrupo;
                        $_REQUEST["param11"] = $rowTarjetasAcceso->fechahasta;
                        $_REQUEST["param12"] = $rowTarjetasAcceso->idzonadetiempo;
                        $_REQUEST["param13"] = $rowTarjetasAcceso->nivel;
                        $_REQUEST["param14"] = $rowTarjetasAcceso->antipassback;
                        
                        //------------------ Ingresar registro en CtrlSincronizar -----------------//
                        if(!$this->setCtrlSincronizar()) return false;
                    }
                }
                if ($acc=="sincFeriados"){
                    foreach ($dataFeriados as $key => $rowFeriados){
                        $_REQUEST["param1"] = $rowFeriados->idferiado;
                        $_REQUEST["param2"] = $rowFeriados->dia;
                        $_REQUEST["param3"] = $rowFeriados->mes;
                        //------------------ Ingresar registro en CtrlSincronizar -----------------//
                        if(!$this->setCtrlSincronizar()) return false;
                    }
                    
                }
            }
        }
        return true;
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlSincronizar(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlSincronizar()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Sincronizacion");
        
        $this->setCampos("item","Item");
        $this->setCampos("accion","Accion");
        $this->setCampos("codigoaccion","Codigoaccion");
        $this->setCampos("nodo","Nodo");
        $this->setCampos("ipv4","Ipv4");
        $this->setCampos("puerto","Puerto");
        $this->setCampos("prioridad","Prioridad");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"item","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"accion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"codigoaccion","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nodo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"ipv4","tipoDato"=>"esValidaIP");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"puerto","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"prioridad","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }
}
?>