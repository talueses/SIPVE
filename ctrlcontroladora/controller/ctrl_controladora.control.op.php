<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLCTRL"); //Categoria del modulo
require_once "ctrl_controladora.control.php";// Class CONTROL ControlCtrlControladora()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlControladora extends ControlCtrlControladora{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlControladora(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlControladora()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlControladora(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlControladora()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Controladora");
        
        $this->setCampos("idtipo_controladora","tipo de Controladora");
        $this->setCampos("idsegmento","Segmento");
        $this->setCampos("nodo","Nodo");
        $this->setCampos("ipv4","Ipv4");
        $this->setCampos("puerto","Puerto");
        $this->setCampos("maximo_usuarios","M&aacute;ximo de usuarios");
        $this->setCampos("nombre","Nombre");
        $this->setCampos("descripcion","Descripci&oacute;n");
        $this->setCampos("ubicacion","Ubicaci&oacute;n");
        $this->setCampos("estado","Estado");
        $this->setCampos("fecha_peticion_estado","Fecha_peticion_estado");
        $this->setCampos("fecha_respuesta_estado","Fecha_respuesta_estado");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//        
        $datos[] = array("isRequired"=>true,"datoName"=>"idsegmento","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//        
        $datos[] = array("isRequired"=>true,"datoName"=>"nodo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"ipv4","tipoDato"=>"esValidaIP");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"puerto","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idtipo_controladora","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"descripcion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"ubicacion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"maximo_usuarios","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"estado","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"fecha_peticion_estado","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"fecha_respuesta_estado","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

    /**
     * Envia la peticion de estado de controladoras-lectoras
     */
    function enviarPeticion(){
        $data = CtrlControladoras::getEstadoControladorasLectoras();
        $obj = new CtrlControladora();
        
        if (count($data) > 0){
            foreach($data as $row){
                if ($idcontroladora != $row->idcontroladora){

                    //--------------------- DATOS CONTROLADORA --------------------------//
                    $obj->setIdcontroladora($row->idcontroladora);
                    $obj->setFecha_peticion_estado(date("Y-m-d H:i:s"));
                    $obj->setFecha_respuesta_estado($row->fecha_respuesta_estado);
                    $obj->modificarRegistro($obj->getIdcontroladora());
                    /**
                     * @todo registrar en la tabla de sincronizacion la peticion de estado
                     */                    
                }
                $idcontroladora = $row->idcontroladora;
            }
        }        
    }

    /**
     * Modifica el estado de las controladoras y sus lectoras a Fuera de linea
     * @param array $data Datos a modificar
     */
    function modificarEstados($data){
        $obj = new CtrlControladoras();
        if (count($data) > 0){
            foreach ($data->ctr as $ctr){
                if (strtotime($ctr->fecha_peticion_estado) > strtotime($ctr->fecha_respuesta_estado)){
                    
                    //--------------------- DATOS CONTROLADORA --------------------------//
                    $obj->setIdcontroladora($ctr->idcontroladora);
                    $obj->setEstado("0");
                    $obj->setFecha_peticion_estado($ctr->fecha_peticion_estado);
                    $obj->setFecha_respuesta_estado($ctr->fecha_respuesta_estado);
                    $obj->modificarRegistro($obj->getIdcontroladora());

                    //--------------------- DATOS LECTORAS --------------------------//
                    foreach ($data->reader as $reader){
                        if ($ctr->idcontroladora == $reader->idcontroladora){
                            $obj->setEstadoLectora($reader->idlectora, "0");
                        }
                    }
                }
            }
        }
    }

    /**
     * Obtienes el listado de estados de controladoras y sus lectoras
     * @return array Devuelve el listado 
     */
    function getEstado(){
        $data    = array();
        $ctrs    = array();
        $readers = array();
        
        $data = CtrlControladoras::getEstadoControladorasLectoras();

        if (count($data) > 0){
            $chkingEstado = "false";
            foreach($data as $row){
                if ($idcontroladora != $row->idcontroladora){
                    $chkEstado = false;
                    
                    if (strtotime($row->fecha_peticion_estado) > strtotime($row->fecha_respuesta_estado)){
                        $chkEstado = "<img  width=\"16\" src=\"../images/ajax-loader.gif\" title=\"Esperando respuesta de la controladora\" alt=\"\" border=\"0\"/>";
                        $chkingEstado = "true";
                        if ($_REQUEST["intento"]=="6"){
                            $chkEstado = "<img  width=\"16\" src=\"../images/Exclamation-32.png\" title=\"No se pudo verificar la conectividad.\" alt=\"\" border=\"0\"/>";
                        }
                    }

                    switch ($row->estadoControladora){
                        case "0":
                            $imgCtrl = "Bullet-Red-16.png";
                            $titleCtrl = "Fuera de Linea";
                            break;
                        case "1":
                            $imgCtrl = "Bullet-Green-16.png";
                            $titleCtrl = "En Linea";
                            break;
                        case "2":
                            default :
                            $imgCtrl = "Bullet-Yellow-26.png";
                            $titleCtrl = "No se pudo establecer la conexi&oacute;n";
                    }
                    $imgEstadoCtrl = "&nbsp;<img  width=\"16\" src=\"../images/".$imgCtrl."\" title=\"".$titleCtrl."\" alt=\"\" border=\"0\"/>&nbsp;";
                    $ctrs[] = (object) array("idcontroladora"=>$row->idcontroladora,
                                            "nodo"=>$row->nodo,
                                            "nombreControladora"=>$row->nombreControladora,
                                            "ipv4"=>$row->ipv4,
                                            "puerto"=>$row->puerto  ,
                                            "estadoControladora"=>$row->estadoControladora,
                                            "fecha_peticion_estado"=>$row->fecha_peticion_estado,
                                            "fecha_respuesta_estado"=>$row->fecha_respuesta_estado,
                                            "chkEstado"=>$chkEstado,
                                            "imgEstadoCtrl"=>$imgEstadoCtrl);
                }
                if ($idlectora != $row->idlectora && $row->idlectora){
                    switch ($row->estadoLectora){
                        case "0":
                            $imgReader = "Bullet-Red-16.png";
                            $titleReader = "Fuera de Linea";
                            break;
                        case "1":
                            $imgReader = "Bullet-Green-16.png";
                            $titleReader = "En Linea";
                            break;
                        case "2":
                            default :
                            $imgReader = "Bullet-Yellow-16.png";
                            $titleReader = "No se pudo establecer la conexi&oacute;n";
                            break;
                    }
                    $imgEstadoReader = "&nbsp;<img  width=\"16\" src=\"../images/".$imgReader."\" title=\"".$titleReader."\" alt=\"\" border=\"0\"/>&nbsp;";
                    $readers[] = (object) array("idcontroladora"=>$row->idcontroladora,
                                               "idlectora"=>$row->idlectora,
                                               "nombreLectora"=>$row->nombreLectora,
                                               "numeroLectora"=>$row->numeroLectora,
                                               "estadoLectora"=>$row->estadoLectora,
                                               "numeroPuerta"=>$row->numeroPuerta,
                                               "puerta"=>$row->puerta,
                                               "imgEstadoReader"=>$imgEstadoReader);
                }
                $idcontroladora = $row->idcontroladora;
                $idlectora      = $row->idlectora;
            }
            $data = (object) array("ctr"=>$ctrs,"reader"=>$readers,"chkingEstado"=>$chkingEstado);
        }
        return $data;
    }

    /**
     * Proceso de sincronizacion de controladoras
     * @return boolean Devuelve verdaero si el proceso se ejecuta
     */
    function syncControladora($acc){
        //------------------ Sincronizar -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar($acc)){
            $this->mensaje = $sinc->mensaje;
            return false;
        }

        $this->mensaje = "<center>Los datos fueron enviados a la(s) controladora(s)...</center>";

        return true;
    }
}
?>