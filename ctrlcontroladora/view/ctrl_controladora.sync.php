<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Descarga de datos a la controladora
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/ctrl_controladora.control.op.php";// Class CONTROLLER
$obj = new ControlOpCtrlControladora();
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="../css/jquery.treeview.css" >
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css">
            .contenido{
                width: 95%;
                height: 92%;
                padding: 5px;
            }
            .buttons{
                width:380px;
                padding: 2px;
                border:#c0c0c0 dashed 2px;
                text-align: center;
                margin-top: 5px;
            }
            .buttons button{
                width: 160px;
                margin: 5px;
            }                        
            #divmensaje{
                width:99%;
                position: absolute;
                top: -10px;
                opacity:0.9;
                z-index: 1000;
            }
            #idcontroladora{
                width: 80%;
            }
            .info{
                width: 50%;                
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();                
                //---------------------------------------------MENSAJE----------------------------------------------------------//
                $('#divmensaje').live({
                    click: function() {
                        $(this).fadeOut(500);
                    }
                });
                $('button').live({
                    click: function() {
                        $('#accion').val($(this).val());
                    }
                });
                $('#formSinc').live({
                    submit:function(){
                        
                        var msj;
                        if ($('#accion').val()){
                            msj = 'de todos los items'
                        }
                        if ($('#idcontroladora').val()==""){
                            if (!confirm('\xbf Esta seguro que desea enviar la configuraci\xf3n '+msj+' a todas las controladoras ?')){
                                return false;
                            }
                        }
                        $('#divmensaje').html('<center><div class="ui-state-highlight ui-widget ui-corner-all info" style="margin-top: 20px; "><p>Enviando datos...</p></div></center>').show();
                    }
                });
            });
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <div id="divmensaje"></div>
        <iframe name="frameOp" id="frameOp" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
        <form name="formSinc" id="formSinc" target="frameOp" action="ctrl_controladora.Op.php" method="POST" >            
            
                <div class="contenido ui-widget-content ui-content ui-corner-all ">
                    Controladora:
                    <select name="idcontroladora" id="idcontroladora" >
                        <option value="">Todas</option>
                        <?php echo Controller::make_combo("ctrl_controladora ", "where idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') order by nombre,ipv4,nodo", "idcontroladora", "nombre,concat('-'),ipv4,concat('-'),concat('Nodo:'),nodo", '',false);?>
                    </select>
                    <center>
                        <div class="buttons ui-widget-content ui-content ui-corner-all">
                            <button type="submit"  title="Sincronizaci&oacute;n de Reloj" value="sincReloj" >
                                <img src="../images/Clock-48.png" alt="" />
                                <br />
                                Reloj
                            </button>
                            <button type="submit" title="Sincronizaci&oacute;n de Horarios de trabajo" value="sincHorarios">
                                <img src="../images/ClockW-48.png" alt="" />
                                <br />
                                Horarios de trabajo
                            </button>
                            <button type="submit" title="Sincronizaci&oacute;n de Grupos de Puertas" value="sincGrpPuertas">
                                <img src="../images/DoorGpr-48.png" alt="" />
                                <br />
                                Grupos de Puertas
                            </button>
                            <button type="submit" title="Sincronizaci&oacute;n de Tarjetas de Acceso de Usuarios" value="sincTrajetasAcc">
                                <img src="../images/CardB-48.png" alt="" />
                                <br />
                                Tarjetas de Acceso
                            </button>
                            <button type="submit" title="Sincronizaci&oacute;n de D&iacute;as Feriados" value="sincFeriados">
                                <img src="../images/Calendar-Android-R2-48.png" alt="" />
                                <br />
                                D&iacute;as Feriados
                            </button>
                            <button type="submit" title="Sincronizaci&oacute;n de Todo" value="sincTodo">
                                <img src="../images/all.png" alt="" />
                                <br />
                                Todos los &Iacute;tems
                            </button>
                        </div>
                    </center>
                </div>
            
            <input type="hidden" name="accion" id="accion" value="">
        </form>
    </body>
</html>