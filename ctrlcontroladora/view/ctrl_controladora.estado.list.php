<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de estados 
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/ctrl_controladora.control.op.php";// Class CONTROLLER
$obj = new ControlOpCtrlControladora();
if ($_REQUEST["accion"]=="enviarPeticion"){
    //echo "<p>enviarPeticion</p>";
    $obj->enviarPeticion();
}

$data = $obj->getEstado();

if ($_REQUEST["accion"]=="modificarEstados"){
    //echo "<p>modificarEstados</p>";
    $obj->modificarEstados($data);
    header("Location: ctrl_controladora.estado.list.php?accion=verPeticion&intento=".($_REQUEST["intento"]+1));
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="../css/jquery.treeview.css" >
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/jquery.cookie.js"></script>
        <script type="text/javascript" src="../js/jquery.treeview.js"></script>

        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            .contenido{
                width:98%;
                padding: 2px;
                border:  #aaaaaa solid 1px;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #controles{
                width: 100px;
                padding: 0px;
                position: absolute;
                opacity: 0.8;
                right: 10px;
                cursor: move;
            }
            .titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            #main { padding: 1em; }

        </style>

        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
                $( "#controles" ).draggable({ containment: "body" });
            });
            $(document).ready(function(){
                $("#list").treeview({
                    control: "#treecontrol",
                    collapsed: true,
                    animated: "medium",
                    persist: "cookie",
                    cookieId: "treeview-black"
                });
                if ($('#chkingEstado').val()=="true"){
                    setTimeout("reloadPage();",15000);
                }                
            });
            function reloadPage(){
                var intento = parseInt($('#intento').val()) + 1;
                //alert('Intento: ' + intento);

                if (intento<5 ){
                    document.location.href = "ctrl_controladora.estado.list.php?accion=verPeticion&intento="+intento;
                }

                if (intento == 5 && $('#chkingEstado').val()=="true"){
                    document.location.href = "ctrl_controladora.estado.list.php?accion=modificarEstados&intento="+intento;
                }
                
            }
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <div class="contenido">
            <div id="controles" class="contenido" >
                <div id="treecontrol" align="center" style="margin: 1px;">
                    <a title="" href="#"><button style="font-size: 1px;margin: 1px;"><img src="../images/Bullet-Toggle-Minus-32.png" /></button></a>
                    <a title="" href="#"><button style="font-size: 1px;margin: 1px;"><img src="../images/Bullet-Toggle-Plus-32.png" /></button></a>
                </div>
            </div>
            <ul id="list" class="treeview-black">
                <?php                
                /*foreach ($data as $key => $row){
                    echo $key;
                    echo "<div align='left'><pre>".print_r($row,true)."</pre></div>";
                    ?>

                    <?php
                }*/
                if (count($data) > 0){
                    foreach ($data->ctr as $ctr){
                        ?>
                        <li>
                            <span>
                                <?php 
                                echo $ctr->imgEstadoCtrl.$ctr->nombreControladora. "&nbsp;(IPv4: ".$ctr->ipv4." - Nodo: ".$ctr->nodo.")";
                                echo "&nbsp;&nbsp;&nbsp;".$ctr->chkEstado;
                                ?>                        
                            </span>
                            <ul>
                                <li>
                                    <div class="contenido">
                                        <table style="margin: 5px 0px 0px 10px;" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left">
                                                    <small class="comment" ><b>&Uacute;ltima Petici&oacute;n Enviada</b>: <?php echo ControlCtrlControladora::formatoFecha($ctr->fecha_peticion_estado);?></small>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <small class="comment" ><b>&Uacute;ltima Respuesta Recibida</b>: <?php echo ControlCtrlControladora::formatoFecha($ctr->fecha_respuesta_estado);?></small>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </li>
                                <?php
                                foreach ($data->reader as $reader){
                                    if ($ctr->idcontroladora == $reader->idcontroladora){
                                        ?>
                                        <li class="closed">
                                            <?php echo $reader->imgEstadoReader.$reader->nombreLectora;?>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                }else{
                    echo "<div class=\"ui-widget\">\n";
                    echo "    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">\n";
                    echo "        <p>\n";
                    echo "            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>\n";
                    echo "                No se encontraron registros\n";
                    echo "        </p>\n";
                    echo "    </div>\n";
                    echo "</div>\n";
                    echo "<br>&nbsp;\n";
                }
                ?>
            </ul>                     
        </div>
        <form method="POST" name="f1" action="#" target="ifrm1">
            <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
            <input type="hidden" name="intento" id="intento" value="<?php echo $_REQUEST["intento"];?>">
            <input type="hidden" name="chkingEstado" id="chkingEstado" value="<?php echo $data->chkingEstado;?>">
            
        </form>
    </body>
</html>
