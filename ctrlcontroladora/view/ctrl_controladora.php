<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/** 
 * Listado de horarios
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/ctrl_controladora.control.op.php";// Class CONTROLLER

$obj  = new CtrlControladoras();
$data = $obj->getControladoras();

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>CtrlControladoras</title>
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #tabla{
                float:left;
                width:30%;
                height:95%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
            div#buttonsOptions{
                width: 200px;
                padding: 9px;
                border:#c0c0c0 dashed 2px;
            }
        </style>
        <script type="text/javascript" language="javascript">            
            $(function() {
                $( "input:button, input:submit,button" ).button();
                $('#divDialog').dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: false,
                    width: 510,//390
                    height:500,//460
                    position:'top',
                    modal:true,
                    open: function(event, ui) {
                        if ($('#accion').val()=='estadoList'){
                            $(this).dialog({title:'Listado de Estado de Conexi&oacute;n de las Controladoras'});
                            $('#dialogFrame').attr('src','ctrl_controladora.estado.list.php?accion=enviarPeticion&intento=1');                            
                        }
                        if ($('#accion').val()=='syncDatos'){
                            $(this).dialog({title:'Sincronizar a Controladora'});
                            $('#dialogFrame').attr('src','ctrl_controladora.sync.php?accion=sync');
                        }
                       
                    },
                    close: function(event, ui) {
                       $('#dialogFrame').attr('src','');
                    }
                });
            });
            function visualizar(fila){
                $('#idcontroladora').val(fila.id);
                $('#accion').val('visualizar');
                $('#f1').attr('action','ctrl_controladora.Acc.php');
                $('#f1').submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    $('#'+fila.id).addClass('chequeada');
            }

            function omover(fila){                
                    if (fila.id===filaseleccionada.id ) return false;                                        
                    $('#'+fila.id).addClass('pasada');
            }
            
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;                    
                    $('#'+fila.id).removeClass('pasada');
            }
            function Accion(acc){
                $('#accion').val(acc);
                $('#f1').attr('action','ctrl_controladora.Acc.php');

                if (acc == "modificar" || acc == "eliminar" ){
                    if ($('#idcontroladora').val()==""){
                        alert("Debe seleccionar un registro de la lista");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("\xbf Esta seguro que desea eliminar el registro ?")){
                            return false;
                        }
                        $('#f1').attr('action','ctrl_controladora.Op.php');
                    }
                }
                if (acc == "estadoList"){
                    if (confirm("Se enviar\xe1 una petici\xf3n a la controladora\n\xbf Esta seguro que desea continuar ?")){
                        
                        $('#divDialog').dialog('open');
                    }                    
                    return false;
                }                
                if (acc == "syncDatos"){
                    $('#divDialog').dialog('open');
                    return false;
                }
                if (acc == "sincReloj"){
                    if (!confirm("\xbf Esta seguro que desea sincronizar el reloj a todas las controladoras ?")){
                        return false;
                    }
                    $('#f1').attr('action','ctrl_controladora.Op.php');
                }

                $('#f1').submit();
                return true;
            }

        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="divDialog">
            <iframe name="dialogFrame" id="dialogFrame" width="480" height="450" frameborder="0" ></iframe>
        </div>
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <div id="tabla" align="center">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                    echo Controller::$mensajePermisos;
                }else{
                    echo Buscador::getObjBuscador("idcontroladora", "nombre,ipv4", "ctrl_controladora", "order by nombre,ipv4");
                    ?>
                    <table id="listado">
                            <tr>
                                <td id="titulo" colspan="5" align="center"><b>Controladoras</b></td>
                            </tr>
                            <tr>
                                <th align="center" width="10%">N&deg;</th>
                                <th>Nomnre</th>
                                <th>Ipv4</th>
                                <th>Nodo</th>
                                <!--th title="Estado de Comunicaci&oacute;n">&nbsp;</th-->
                            </tr>
                            <?php
                            if (count($data) > 0){
                                foreach ($data as $key => $row){?>
                                    <tr id="<?php echo $row->idcontroladora;?>" onclick="visualizar(this);oclic(this)" onmouseover="omover(this)" onmouseout="omout(this)">
                                        <td align="center"><b><?php echo paginationSQL::$start+$key+1;?></b></td>
                                        <td><?php echo $row->nombre;?></td>
                                        <td><b><?php echo $row->ipv4;?></b></td>
                                        <td align="center"><?php echo $row->nodo;?></td>
                                        <?php
                                        switch ($row->estado){
                                            case "0":
                                                $fileImg = "Bullet-Red-16.png";
                                                $title = "Fuera de Linea";
                                                break;
                                            case "1":
                                                $fileImg = "Bullet-Green-16.png";
                                                $title = "En Linea";
                                                break;
                                            case "2":
                                                $fileImg = "Bullet-Yellow-16.png";
                                                $title = "No se pudo establecer la conexi&oacute;n";
                                                break;
                                        }
                                        ?>
                                        <!--td align="center" title="<?php echo $title;?>">
                                            <img src="../images/<?php echo $fileImg;?>" title="<?php echo $title;?>" alt="" border="0"/>
                                        </td-->

                                    </tr>
                                <?php
                                }
                            }else{
                                ?>
                                <tr>
                                    <td colspan="5">
                                        <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                    </td>
                                </tr>

                                <?php
                            }
                            ?>

                    </table>
                    <?php echo paginationSQL::links();?>
                    <div id="botones" >
                        <center>
                            <form name="f1" id="f1" target="frm1" action="ctrl_controladora.Acc.php" method="POST">
                                <input type="hidden" name="idcontroladora" id="idcontroladora" value="">
                                <input type="hidden" name="accion" id="accion" value="">
                                <?php
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"agregar")){
                                    ?><input type="submit" value="Agregar" onclick="return Accion('agregar')"  class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"modificar")){
                                    ?><input type="submit" value="Modificar" onclick="return Accion('modificar')" class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"eliminar")){
                                    ?><input type="button" value="Eliminar" onclick="return Accion('eliminar')" class="boton"><?php
                                }
                                ?>
                                <input type="submit" value="Ayuda" onclick="this.form.action='ctrl_controladora.Ayuda.php'" class="boton">
                                <br />&nbsp;<br />
                                <?php
                                if (count($data) > 0){
                                    ?>
                                    <div id="buttonsOptions" class="ui-corner-all" >
                                        <?php 
                                        if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"estadoCnx")){
                                            ?>
                                            <button type="button" style="font-size: 10px;" onclick="return Accion('estadoList');" title="Petici&oacute;n de estado de conexi&oacute;n Controladoras - Lectoras">
                                                <img src="../images/Connectiions-32.png" alt="" />
                                                <br />
                                                Chk. Conexi&oacute;n
                                            </button>
                                            <?php
                                        }                                        
                                        if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"syncDatos")){
                                            ?>
                                            <button type="button" style="font-size: 10px;" onclick="return Accion('syncDatos');" title="Sincronizar datos a las Controladoras - Lectoras">
                                                <img src="../images/iSync-32.png" alt="" />
                                                <br />
                                                Sincronizar
                                            </button>
                                            <?php
                                        }
                                        ?>
                                        <!--
                                        <button type="button" style="font-size: 10px;" onclick="return Accion('sincReloj');"  title="Sincronizaci&oacute;n de Reloj a todas las controladoras">
                                            <img src="../images/Clock-32.png" alt="" />
                                            <br />
                                            Sinc. Reloj
                                        </button>
                                        -->
                                    </div>
                                    <?php
                                }
                                ?>
                            </form>
                        </center>
                    </div>
                    <?php
                }
                ?>
            </div>            
            <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                <iframe name="frm1" id="frm1" frameborder="0" scrolling="no" style="width:100%; height:650px" src="ctrl_controladora.Acc.php?accion=agregar"></iframe>
            </div>
        </div>        
    </body>
</html>