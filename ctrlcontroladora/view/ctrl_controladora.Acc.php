<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_controladora.control.op.php";// Class CONTROLLER
$obj = new CtrlControladoras();
$numExist = $obj->getNumerosControladoras();
if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    
    $data = $obj->getCtrlControladora($_REQUEST["idcontroladora"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.textarea.maxlength.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
                <?php
                if ($_REQUEST["accion"] == "visualizar"){
                    echo "$( 'textarea' ).resizable({disabled: true});";
                }
                ?>
                
            });
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Controladora</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="ctrl_controladora.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr title="Segmento">
                                <td align="right">Segmento:</td>
                                <td>
                                    <select name="idsegmento" id="idsegmento" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php echo ControlCtrlControladora::make_combo("segmento ", "where idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') order by segmento", "idsegmento", "segmento", $data->idsegmento,false);?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Nodo">
                                <td align="right">Nodo:</td>
                                <td>
                                    <select name="nodo" id="nodo" onchange="" <?php echo $disabled;?> >
                                        <option value="">Seleccione</option>
                                        <?php
                                        for ($i=1;$i<=255;$i++){
                                            $chkNum = false;
                                            foreach ($numExist as $row){
                                                if ($row->nodo == $i){
                                                    $chkNum = true;
                                                }
                                            }
                                            if (!$chkNum || $data->nodo == $i){
                                                echo '<option value="'.$i.'" '.ControlCtrlControladora::busca_valor($data->nodo, $i ).'>'.$i.'</option>';
                                            }

                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Ipv4">
                                <td align="right">Ipv4:</td>
                                <td>
                                    <input type="text" name="ipv4" id="ipv4" value="<?php echo $data->ipv4;?>" <?php echo $disabled;?>>:<input type="text" name="puerto" id="puerto" size="4" maxlength="4" value="<?php echo $data->puerto?$data->puerto:"1621";?>" <?php echo $disabled;?> title="Puerto">
                                </td>
                            </tr>
                            <tr title="Tipo de Controladora">
                                <td align="right">Tipo:</td>
                                <td>
                                    <select name="idtipo_controladora" id="idtipo_controladora" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php echo ControlCtrlControladora::make_combo("ctrl_tipo_controladora ", "order by tipo_controladora", "idtipo_controladora", "tipo_controladora", $data->idtipo_controladora,false);?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Nombre descriptivo de la controladora">
                                <td align="right">Nombre:</td>
                                <td>
                                    <input type="text" name="nombre" id="nombre" maxlength="100" value="<?php echo $data->nombre;?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <?php
                            if ($_REQUEST["accion"]!="agregar"){
                                /*?>
                                <tr title="Estado de conexi&oacute;n a la controladora">
                                    <td align="right" >Estado:</td>
                                    <td valign="top">
                                        <?php
                                        switch ($data->estado){
                                            case "0":
                                                $fileImg = "Bullet-Red-32.png";
                                                $title = "Fuera de Linea";
                                                break;
                                            case "1":
                                                $fileImg = "Bullet-Green-32.png";
                                                $title = "En Linea";
                                                break;
                                            case "2":
                                                $fileImg = "Bullet-Yellow-32.png";
                                                $title = "No se pudo establecer la conexi&oacute;n";
                                                break;
                                        }
                                        ?>
                                        <img style="float: left" src="../images/<?php echo $fileImg;?>" title="<?php echo $title;?>" alt="" border="0"/>
                                        <table style="float: left;margin: 5px 0px 0px 10px;" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left">
                                                    <small class="comment" ><b>&Uacute;ltima Petici&oacute;n Enviada</b>: <?php echo ControlCtrlControladora::formatoFecha($data->fecha_peticion_estado);?></small>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <small class="comment" ><b>&Uacute;ltima Respuesta Recibida</b>: <?php echo ControlCtrlControladora::formatoFecha($data->fecha_respuesta_estado);?></small>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?*/
                            }
                            ?>
                            <tr title="Breve descripci&oacute;n de la controladora">
                                <td align="right" valign="top" >Descripci&oacute;n:</td>
                                <td>
                                    <div class="divTextarea" >
                                        <textarea name="descripcion" id="descripcion" <?php echo $disabled;?> rows="6" cols="28" maxlength="250" <?php echo $disabled;?>><?php echo $data->descripcion;?></textarea>
                                        <div class="charLeftDiv"  align="right"><input type="text" class="charLeft" id="charLeft_descripcion" size="4" readonly > Caracteres Restantes</div>
                                    </div>
                                </td>
                            </tr>
                            <tr title="Ubicaci&oacute;n de la controladora">
                                <td align="right" valign="top" >Ubicaci&oacute;n:</td>
                                <td>
                                    <div class="divTextarea" >
                                        <textarea name="ubicacion" id="ubicacion" <?php echo $disabled;?> rows="6" cols="28" maxlength="250"><?php echo $data->ubicacion;?></textarea>
                                        <div class="charLeftDiv" align="right"><input type="text" class="charLeft" id="charLeft_ubicacion" size="4" readonly > Caracteres Restantes</div>
                                    </div>
                                </td>
                            </tr>
                            <tr title="M&aacute;ximo de Usuarios">
                                <td align="right">M&aacute;x. Usuarios:</td>
                                <td>
                                    <select name="maximo_usuarios" id="maximo_usuarios" <?php echo $disabled;?> >
                                        <?php
                                        for ($i=5000;$i<=15000;$i+=5000){
                                            echo "<option value=\"".$i."\" ".ControlCtrlControladora::busca_valor($data->maximo_usuarios , $i).">".$i."</option>\n";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idcontroladora" id="idcontroladora" value="<?php echo $data->idcontroladora ?>">
                        <input type="hidden" name="estado" id="estado" value="<?php echo $data->estado?$data->estado:"0" ?>">
                        <input type="hidden" name="fecha_peticion_estado" id="fecha_peticion_estado" value="<?php echo $data->fecha_peticion_estado ?>">
                        <input type="hidden" name="fecha_respuesta_estado" id="fecha_respuesta_estado" value="<?php echo $data->fecha_respuesta_estado ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Controladora\" />";
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>