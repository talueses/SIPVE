<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once '../../ctrlsincronizar/model/ctrl_sincronizar.tabla.php';

/**
 * Clase CtrlSincronizars{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlSincronizars extends CtrlSincronizar{

    /**
     * Consulta de Controladoras
     * @return object Devuelve un registro como objeto
     */
    static function getControladoras($idcontroladora){
        if ($idcontroladora){
            $arg = "where idcontroladora='".$idcontroladora."'";
        }
        $sql = "select idcontroladora,idtipo_controladora,nodo,ipv4,puerto,maximo_usuarios,nombre,descripcion,ubicacion from ctrl_controladora ".$arg." order by ipv4, nodo";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Horarios de Control de Acceso
     * @return object Devuelve un registro como objeto
     */
    static function  getHorarios(){
        
        $sql = "select indice,
                       descripcion,
                       disponibleenferiado,
                       nivel,
                       indicelink,
                       domingodesde,
                       domingohasta,
                       lunesdesde,
                       luneshasta,
                       martesdesde,
                       marteshasta,
                       miercolesdesde,
                       miercoleshasta,
                       juevesdesde,
                       jueveshasta,
                       viernesdesde,
                       vierneshasta,
                       sabadodesde,
                       sabadohasta 
                from ctrl_zonadetiempo 
                    order by indice";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Grupos de puertas
     * @return object Devuelve un registro como objeto
     */
    static function getGrupo(){
        $sql = "select idgrupo,
                       numero as numeroGrupo,
                       idgrupo_padre,
                       nivel
                from ctrl_grupo g 
                    order by numero";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Puertas de un Grupo
     * @return object Devuelve un registro como objeto
     */
    static function getGrpPuertas($idgrupo){
        $sql = "select p.numero as numeroPuerta
                from ctrl_grupo_puerta gp, 
                     ctrl_puerta p 
                where gp.idgrupo  = '".$idgrupo."'
                  and gp.idpuerta = p.idpuerta
                    order by p.numero";
        
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Usuarios de Acceso
     * @return object Devuelve un registro como objeto
     */
    static function  getTarjetasAcceso(){
        
               
        $sql = "select cu.numero as numeroUsuario,
                       cgp.numero as numeroGrupo,
                       cu.idzonadetiempo,
                       cu.codigo_pin,
                       cu.codigo_id,
                       cu.codigo_sitio,
                       cu.codigo_tarjeta,
                       cu.tarjetayopin,
                       cu.tienefechalimite,
                       cu.fechahasta,
                       cu.rondasguardias,
                       cu.skipfpcheck,
                       cu.modificable,
                       cu.nivel,
                       cu.antipassback                       
                from ctrl_usuario cu, 
                     ctrl_grupo cgp                     
                where cu.idgrupo = cgp.idgrupo
                order by cu.numero , cgp.numero, cu.idzonadetiempo";
        
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Diad Feriados
     * @return object Devuelve un registro como objeto
     */
    static function  getDiasFeriados(){
                        
        $sql = "select id as idferiado, mes, dia from ctrl_diafestivo order by id";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
}
?>
