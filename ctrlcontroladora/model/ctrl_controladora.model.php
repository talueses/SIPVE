<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'ctrl_controladora.tabla.php';

/**
 * Clase CtrlControladoras{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlControladoras extends CtrlControladora{
    
    /**
     * Consulta de Controladoras
     * @return object Devuelve un registro como objeto
     */
    function  getControladoras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcontroladoraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcontroladoraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and idcontroladora in (".$in.")";
        }
        
        $sql = "select /*start*/
                       idcontroladora,
                       idtipo_controladora,
                       idsegmento,
                       nodo,
                       ipv4,
                       puerto,
                       maximo_usuarios,
                       nombre,
                       descripcion,
                       ubicacion,
                       estado,
                       fecha_peticion_estado,
                       fecha_respuesta_estado 
                       /*end*/
                from ctrl_controladora 
                where idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."')
                ".$arg." order by nodo,ipv4";
        //echo "<div align='left'><pre>".print_r(paginationSQL::setSql($sql),true)."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de los numeros existentes de puertas
     * @return object Devuelve registros como objeto
     */
    function getNumerosControladoras() {
        $sql = "SELECT nodo FROM ctrl_controladora";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }


    /**
     * Consulta de estados de conectividad de controladoras y lectoras
     * @return object Devuelve registros como objeto
     */
    static function getEstadoControladorasLectoras() {        
        $sql = "select  c.idcontroladora,
                        c.nodo,
                        c.nombre as nombreControladora,
                        c.ipv4,
                        c.puerto,
                        c.estado as estadoControladora,
                        c.fecha_peticion_estado,
                        c.fecha_respuesta_estado,
                        l.idlectora,
                        l.nombre as nombreLectora,
                        l.numero as numeroLectora,
                        l.estado as estadoLectora,
                        p.numero as numeroPuerta,
                        p.puerta
                from
                    ctrl_controladora c
                        left join ctrl_lectora l on (c.idcontroladora = l.idcontroladora)
                        left join ctrl_puerta p on (l.idpuerta = p.idpuerta)                    
                    order by  c.ipv4,c.nodo,l.numero";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }


    function setEstadoLectora($idlectora,$estado){
        $sql = "update ctrl_lectora set estado= '".$estado."' where idlectora = '".$idlectora."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return true;
    }   
}
?>
