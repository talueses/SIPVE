<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlControladora {
    
    private $idcontroladora = null;
    private $idtipo_controladora = null;
    private $idsegmento = null;
    private $nodo = null;
    private $ipv4 = null;
    private $puerto = null;
    private $maximo_usuarios = null;
    private $nombre = null;
    private $descripcion = null;
    private $ubicacion = null;
    private $estado = null;
    private $fecha_peticion_estado = null;
    private $fecha_respuesta_estado = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcontroladora($idcontroladora){
        $this->idcontroladora = $idcontroladora;
    }
    public function getIdcontroladora(){
        return $this->idcontroladora;
    }
    public function setIdtipo_controladora($idtipo_controladora){
        $this->idtipo_controladora = $idtipo_controladora;
    }
    public function getIdtipo_controladora(){
        return $this->idtipo_controladora;
    }
    public function setIdsegmento($idsegmento){
        $this->idsegmento = $idsegmento;
    }
    public function getIdsegmento(){
        return $this->idsegmento;
    }
    public function setNodo($nodo){
        $this->nodo = $nodo;
    }
    public function getNodo(){
        return $this->nodo;
    }
    public function setIpv4($ipv4){
        $this->ipv4 = $ipv4;
    }
    public function getIpv4(){
        return $this->ipv4;
    }
    public function setPuerto($puerto){
        $this->puerto = $puerto;
    }
    public function getPuerto(){
        return $this->puerto;
    }
    public function setMaximo_usuarios($maximo_usuarios){
        $this->maximo_usuarios = $maximo_usuarios;
    }
    public function getMaximo_usuarios(){
        return $this->maximo_usuarios;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setUbicacion($ubicacion){
        $this->ubicacion = $ubicacion;
    }
    public function getUbicacion(){
        return $this->ubicacion;
    }
    public function setEstado($estado){
        $this->estado = $estado;
    }
    public function getEstado(){
        return $this->estado;
    }
    public function setFecha_peticion_estado($fecha_peticion_estado){
        $this->fecha_peticion_estado = $fecha_peticion_estado;
    }
    public function getFecha_peticion_estado(){
        return $this->fecha_peticion_estado;
    }
    public function setFecha_respuesta_estado($fecha_respuesta_estado){
        $this->fecha_respuesta_estado = $fecha_respuesta_estado;
    }
    public function getFecha_respuesta_estado(){
        return $this->fecha_respuesta_estado;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcontroladora !== null) && (trim($this->idcontroladora)!=='') ){
            $campos .= "idcontroladora,";
            $valores .= "'".$this->idcontroladora."',";
        }
        if(($this->idtipo_controladora !== null) && (trim($this->idtipo_controladora)!=='') ){
            $campos .= "idtipo_controladora,";
            $valores .= "'".$this->idtipo_controladora."',";
        }
        if(($this->idsegmento !== null) && (trim($this->idsegmento)!=='') ){
            $campos .= "idsegmento,";
            $valores .= "'".$this->idsegmento."',";
        }
        if(($this->nodo !== null) && (trim($this->nodo)!=='') ){
            $campos .= "nodo,";
            $valores .= "'".$this->nodo."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $campos .= "ipv4,";
            $valores .= "'".$this->ipv4."',";
        }
        if(($this->puerto !== null) && (trim($this->puerto)!=='') ){
            $campos .= "puerto,";
            $valores .= "'".$this->puerto."',";
        }
        if(($this->maximo_usuarios !== null) && (trim($this->maximo_usuarios)!=='') ){
            $campos .= "maximo_usuarios,";
            $valores .= "'".$this->maximo_usuarios."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $campos .= "descripcion,";
            $valores .= "'".$this->descripcion."',";
        }
        if(($this->ubicacion !== null) && (trim($this->ubicacion)!=='') ){
            $campos .= "ubicacion,";
            $valores .= "'".$this->ubicacion."',";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $campos .= "estado,";
            $valores .= "'".$this->estado."',";
        }
        if(($this->fecha_peticion_estado !== null) && (trim($this->fecha_peticion_estado)!=='') ){
            $campos .= "fecha_peticion_estado,";
            $valores .= "'".$this->fecha_peticion_estado."',";
        }
        if(($this->fecha_respuesta_estado !== null) && (trim($this->fecha_respuesta_estado)!=='') ){
            $campos .= "fecha_respuesta_estado,";
            $valores .= "'".$this->fecha_respuesta_estado."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_controladora $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_controladora SET ";
        
        if(($this->idtipo_controladora !== null) && (trim($this->idtipo_controladora)!=='') ){
            $sql .= "idtipo_controladora = '".$this->idtipo_controladora."',";
        }
        if(($this->idsegmento !== null) && (trim($this->idsegmento)!=='') ){
            $sql .= "idsegmento = '".$this->idsegmento."',";
        }
        if(($this->nodo !== null) && (trim($this->nodo)!=='') ){
            $sql .= "nodo = '".$this->nodo."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $sql .= "ipv4 = '".$this->ipv4."',";
        }
        if(($this->puerto !== null) && (trim($this->puerto)!=='') ){
            $sql .= "puerto = '".$this->puerto."',";
        }
        if(($this->maximo_usuarios !== null) && (trim($this->maximo_usuarios)!=='') ){
            $sql .= "maximo_usuarios = '".$this->maximo_usuarios."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $sql .= "descripcion = '".$this->descripcion."',";
        }
        else{
            $sql .= "descripcion = NULL,";
        }
        if(($this->ubicacion !== null) && (trim($this->ubicacion)!=='') ){
            $sql .= "ubicacion = '".$this->ubicacion."',";
        }
        else{
            $sql .= "ubicacion = NULL,";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $sql .= "estado = '".$this->estado."',";
        }
        if(($this->fecha_peticion_estado !== null) && (trim($this->fecha_peticion_estado)!=='') ){
            $sql .= "fecha_peticion_estado = '".$this->fecha_peticion_estado."',";
        }
        else{
            $sql .= "fecha_peticion_estado = NULL,";
        }
        if(($this->fecha_respuesta_estado !== null) && (trim($this->fecha_respuesta_estado)!=='') ){
            $sql .= "fecha_respuesta_estado = '".$this->fecha_respuesta_estado."',";
        }
        else{
            $sql .= "fecha_respuesta_estado = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcontroladora = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_controladora  WHERE idcontroladora = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlControladoras
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlControladoras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcontroladoraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcontroladoraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcontroladora in (".$in.")";
        }
        
        $sql = "select /*start*/ idcontroladora,idtipo_controladora,idsegmento,nodo,ipv4,puerto,maximo_usuarios,nombre,descripcion,ubicacion,estado,fecha_peticion_estado,fecha_respuesta_estado /*end*/ from ctrl_controladora ".$arg." order by nodo,ipv4";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlControladora
     * @param int $idcontroladora Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlControladora($idcontroladora){
        $sql = "SELECT idcontroladora,idtipo_controladora,idsegmento,nodo,ipv4,puerto,maximo_usuarios,nombre,descripcion,ubicacion,estado,fecha_peticion_estado,fecha_respuesta_estado FROM ctrl_controladora WHERE idcontroladora = '".$idcontroladora."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>