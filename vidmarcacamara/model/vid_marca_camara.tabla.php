<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class VidMarcaCamara {
    
    private $idmarca_camara = null;
    private $nombre_marca_camara = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdmarca_camara($idmarca_camara){
        $this->idmarca_camara = $idmarca_camara;
    }
    public function getIdmarca_camara(){
        return $this->idmarca_camara;
    }
    public function setNombre_marca_camara($nombre_marca_camara){
        $this->nombre_marca_camara = $nombre_marca_camara;
    }
    public function getNombre_marca_camara(){
        return $this->nombre_marca_camara;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idmarca_camara !== null) && (trim($this->idmarca_camara)!=='') ){
            $campos .= "idmarca_camara,";
            $valores .= "'".$this->idmarca_camara."',";
        }
        if(($this->nombre_marca_camara !== null) && (trim($this->nombre_marca_camara)!=='') ){
            $campos .= "nombre_marca_camara,";
            $valores .= "'".$this->nombre_marca_camara."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_marca_camara $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_marca_camara SET ";
        
        if(($this->nombre_marca_camara !== null) && (trim($this->nombre_marca_camara)!=='') ){
            $sql .= "nombre_marca_camara = '".$this->nombre_marca_camara."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idmarca_camara = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_marca_camara  WHERE idmarca_camara = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidMarcaCamaras
     * @return object Devuelve un registro como objeto
     */
    function  getVidMarcaCamaras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idmarca_camaraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idmarca_camaraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idmarca_camara in (".$in.")";
        }
        
        $sql = "select /*start*/ idmarca_camara,nombre_marca_camara /*end*/ from vid_marca_camara ".$arg." order by nombre_marca_camara";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidMarcaCamara
     * @param int $idmarca_camara Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidMarcaCamara($idmarca_camara){
        $sql = "SELECT idmarca_camara,nombre_marca_camara FROM vid_marca_camara WHERE idmarca_camara = '".$idmarca_camara."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>