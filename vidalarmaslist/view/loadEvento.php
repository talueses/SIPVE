<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 07-10-2010 CENIT
 * Listado de eventos
 */
session_start(); // start up your PHP session!
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/vid_alarmaslist.control.op.php";

//echo "<div align='left'><pre>".print_r($_SERVER,true)."</pre></div>";

$obj = new ControlAlarmasList();
$data = $obj->loadEvento();
$data = $data[0];
// ------------ SRC IMAGEN GUARDADA EN SERVIDOR MOTION -------------------------------//
$numero = $data->numero;
if ($data->numero < 10){
    $numero = "0".$data->numero;
}
$src = "http://".$data->ipv4Servidor."/motion/cam".$numero."/".basename($data->file_picture_save);

// ------------ SRCMOVIE VIDEO GUARDADO EN SERVIDOR MOTION -------------------------------//
$srcMovie = base64_encode("http://".$data->ipv4Servidor."/motion/cam".$numero."/".basename($data->file));
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>Listado de Eventos</title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link type="text/css" rel="stylesheet" media="screen" href="../css/jquery.toChecklist.css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #titulo{
                width: 440px;
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #datos{

                background:#fff;
                margin: 1px 1px 1px 1px;
                padding: 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            img{
                background:#fff;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }

            #divmensaje{
                width: 450px;
                position: absolute;
                top: -10px;
                opacity:0.9;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(document).ready(function(){
                $( "input:button, input:submit,button" ).button();
                
                //hideLoading();
                setColorStatus();
                $('#divmensaje').live({
                    click: function() {
                        $(this).fadeOut('slow');
                    }
                });
            });
            function hideLoading(){
                $("#cargando").fadeOut("slow");
            }
            function setColorStatus(){
                if ($("#status").val()=="0"){
                    $("#status").css("color","#FF0000");
                }
                if ($("#status").val()=="1"){
                    $("#status").css("color","#006400");
                }
            }
            function errorImg(obj){
                $("#"+obj.id).attr("src","../images/Video-Camera-640x480.png");
            }
        </script>
    </head>
    <body  style="margin:0px;background:#fff;" onload="hideLoading();">
        <div id="cargando" align="center" style="position: absolute;background-color:#000000;opacity:.7;width: 100%;height: 100%;vertical-align: middle;color: #FFFFFF;font-size: 20px;">
            <br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<img src="../images/loading51.gif" border="0"><br>&nbsp;<b>Cargando...</b>
        </div>
        <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
        <div id="datos" align="center">
            <table>
                <tr>
                    <th colspan="2" >
                        <img src="<?php echo $src;?>" alt="<?php echo $data->camara." - ".$data->numero?>" id="imagen" width="320" height="240" onerror="errorImg(this);">                        
                        <br><div id="divmensaje" ></div>
                    </th>
                </tr>
                <tr>
                    <th colspan="2" id="titulo" style="padding: 5px">
                        <div  align="right" style="float:left;" >
                            <a href="downloadFile.php?file=<?php echo $srcMovie;?>" target="ifrm1"><img src="../images/Video-Camera-32x15.png" alt="Descargar Video" title="Descargar Video" border="0" ></a>
                            <?php
                            if ($data->idplantafisica){
                                ?><a href="mapaGetZonasCamaras.php?idcamara=<?php echo $data->idcamara;?>&idplantafisica=<?php echo $data->idplantafisica;?>&idevento=<?php echo $data->idevento;?>" ><img src="../images/Layers-Map-32x16.png" alt="" title="Ubicaci&oacute;n de la c&aacute;mara"/></a><?php
                            }
                            ?>
                            

                            
                        </div>
                        <div style="width: 40%;float:left;" >
                            <?php echo trim($data->camara ." - ".$data->numero);?>
                        </div>
                        <div id="reloj" align="right" style="float: left;" >
                            <?php                            echo trim(Controller::formatoFecha($data->fecha));?>
                        </div>
                    </th>
                </tr>
                <tr title="Pa&iacute;s">
                    <td align="right">Pa&iacute;s:</td>
                    <td>
                        <input type="text" name="pais" size="30" maxlength="100" title="Pa&iacute;s" value="<?php echo $data->pais;?>" readonly>
                    </td>
                </tr>
                <tr title="Estado">
                    <td align="right">Estado:</td>
                    <td>
                        <input type="text" name="estado" size="30" maxlength="100" title="Estado"  value="<?php echo $data->estado;?>" readonly>
                    </td>
                </tr>
                <tr title="Ciudad">
                    <td align="right">Ciudad:</td>
                    <td>
                        <input type="text" name="ciudad" size="30" maxlength="100" title="Ciudad" value="<?php echo $data->ciudad;?>" readonly>
                    </td>
                </tr>
                <tr title="Avenida">
                    <td align="right">Avenida:</td>
                    <td>
                        <input type="text" name="avenida" size="30" maxlength="100" title="Avenida" value="<?php echo $data->avenida;?>" readonly>
                    </td>
                </tr>
                <tr title="Edificio">
                    <td align="right">Edificio:</td>
                    <td>
                        <input type="text" name="edificio" size="30" maxlength="100" title="Edificio" value="<?php echo $data->edificio;?>" readonly>
                    </td>
                </tr>
                <tr title="Piso">
                    <td align="right">Piso:</td>
                    <td>
                        <input type="text" name="piso" size="30" maxlength="100" title="Piso" value="<?php echo $data->piso;?>" readonly>
                    </td>
                </tr>
                <tr title="Oficina">
                    <td align="right">Oficina:</td>
                    <td>
                        <input type="text" name="oficina" size="30" maxlength="100" title="Oficina" value="<?php echo $data->oficina;?>" readonly>
                    </td>
                </tr>
                <!-------------------------------------------------------- DATOS EVENT_START ----------------------------------------------------->
                <tr title="Estado">
                    <td align="right">Estado:</td>
                    <td>
                        <select name="status" id="status" style="color: #FF0000;" disabled>
                            <option value="0" style="color: #FF0000;" onclick="setColorStatus(this)" <?php echo Controller::busca_valor($data->status, "0")?>>No Atendido</option>
                            <option value="1" style="color: #006400;" <?php echo Controller::busca_valor($data->status, "1")?>>Atendido</option>
                        </select>
                    </td>
                </tr>
                <tr title="Observaci&oacute;n">
                    <td align="right" valign="top">Observaci&oacute;n:</td>
                    <td>
                        <textarea name="observacion" id="observacion" cols="33" rows="5" readonly><?php echo $data->observacion;?></textarea>
                    </td>
                </tr>
            </table>
        </div>
        <center>
            <input type="hidden" name="idevento" id="idevento" value="<?php echo $data->idevento;?>">
        </center>
    </body>
</html>
