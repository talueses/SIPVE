<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 07-10-2010 CENIT
 * Listado de eventos 
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/vid_alarmaslist.control.op.php";

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

$obj = new ControlAlarmasList();
$data = $obj->loadEventos();

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>Listado de Eventos</title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #tabla {
                font-size: 13px;
                height: 99%;
                width:99%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            .boton{
                font-size: 10px;
            }
            /* Para que no se vean los Botones cuando se ejecute la accion*/
            .ui-button{
                position: inherit;
            }
            iframe {
                overflow-x: hidden;
                overflow-y: scroll;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {                   
                $( "input:button, input:submit, button" ).button();
                // --- Caja de Dialogo para listado y observaciones de eventos --- //
                $("#loadEvento").dialog({
                   autoOpen: false,
                   resizable: false,
                   draggable: false,
                   width: 500,
                   height:580,
                   position:'top',
                   modal:true,
                   close: function(event, ui) {
                       $('#frameEvento').attr('src','');
                   }
                });
            });
            function setLoadEvento(obj){
                $("#frameEvento").attr('src','');
                var d = new Date();
                $("#frameEvento").attr('src','loadEvento.php?idevento='+obj.id+'&'+d.getMilliseconds());

                $('#loadEvento').dialog('open');
            }
            filaseleccionada = {};
            function omover(fila){
                if (fila.id===filaseleccionada.id ) return false;
                fila.className = 'pasada';
            }
            function omout(fila){
                if (fila.id===filaseleccionada.id ) return false;
                fila.className = "";
            }
            function oclic(fila){
                alert(fila.id)
            }
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <div id="loadEvento" title="Alarma" >
            <iframe id="frameEvento" src="" width="480" height="530" frameborder="0" scrolling="auto" ></iframe>
        </div>
        <center>
            <div id="divmensaje" style="width:99%;"><?php echo $obj->mensaje?></div>
            <?php
            if (!$obj->error){
                ?>
                <div id="tabla" align="center">
                    <br>
                    <table id="listado" align="center">
                        <tr>
                            <td id="titulo" colspan="5" align="center"><b>Listado de Alarmas</b></td>
                        </tr>
                        <tr>
                            <th align="center" width="5%">N&deg;</th>
                            <th title="Tipo de Evento">&nbsp;</th>
                            <th>C&aacute;mara</th>
                            <th>Fecha</th>
                            <th>Estado</th>
                        </tr>
                        <?php
                        if (count($data) > 0){
                            foreach ($data as $key => $row){

                                echo "<tr id=\"".$row->idevento."\" class=\"fila\" onclick=\"setLoadEvento(this)\" onmouseover=\"omover(this)\" onmouseout=\"omout(this)\">\n";
                                echo "  <td align=\"center\"><b>".(paginationSQL::$start+$key+1)."</b></td>";
                                echo "  <td align=\"center\"  title=\"Tipo de Evento\">\n";
                                if (preg_match("/timelapse/", $row->file)){

                                    echo "<img src=\"../images/Clock-32.png\" width=\"20\" title=\"Video generado por lapso de tiempo\"/>";
                                }else{
                                    echo "<img src=\"../images/Video-security-32.png\"  width=\"20\" title=\"Video generado por detecci&oacute;n de movimiento\"/>";
                                }
                                echo "  </td>\n";
                                echo "  <td align=\"center\" >".$row->camara."</td>\n";
                                echo "  <td align=\"center\" >".Controller::formatoFecha($row->fecha)."</td>\n";
                                if ($row->status=="1"){
                                    echo "  <td align=\"center\" style=\"color:#006400;\"><b>Atendido</b></td>\n";
                                }else{
                                    echo "  <td align=\"center\" style=\"color:#FF0000;\"><b>No atendido</b></td>\n";
                                }

                                echo "</tr>\n";
                            }
                        }else{
                            ?>
                            <tr>
                                <td colspan="5">
                                    <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                </td>
                            </tr>

                            <?php
                        }
                        ?>
                    </table>
                    <?php echo paginationSQL::links();?>
                </div>
                <?php
            }
            ?>            
        </center>
    </body>
</html>
