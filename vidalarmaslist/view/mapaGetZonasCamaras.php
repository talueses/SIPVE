<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
session_start(); // start up your PHP session!

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/vid_alarmaslist.control.op.php";

$data = Alarmas::getZonasCamarasPlantafisica($_REQUEST["idcamara"],$_REQUEST["idplantafisica"]);

// ------------ IMAGEN DE PLANO --------------//
$image = "../../plantafisica/images/".$data[0]->file;
$imagesize = getimagesize($image);

foreach ($data as $row){
    if ($row->idplantafisicaCamara == $_REQUEST["idplantafisica"] && $idcamara != $row->idcamara ){ // si las coordenadas de la zona estan registradas
        $camarasCoord[$row->idcamara] = array("idcamara" => $row->idcamara,"camara" => $row->camara,"leftX" => $row->leftXCamara,"topY" => $row->topYCamara);
    }
    if ($idzona != $row->idzona ){
        $zonasCoord[$row->idzona] = array("idzona"=>$row->idzona,"zona"=>$row->zona,"leftX"=>$row->leftXZona,"topY"=>$row->topYZona,"width"=>$row->width,"height"=>$row->height,"bgcolor"=>$row->bgcolor);
    }
    $idzona   = $row->idzona;
    $idcamara = $row->idcamara;
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css">
            .title{
                width: <?php echo $imagesize[0]?>px;
                color: #770;
                font-style: oblique;
                background-color: #f0ebe2;
                border: 1px solid #ccc0a9;
            }            
            /*---------PLANO PLANTA FISICA--------*/
            #map {
                background: #fff url(<?php echo $image?>) no-repeat center;
                width: <?php echo $imagesize[0]?>px;
                height: <?php echo $imagesize[1]?>px;
                margin-top: 5px;
                position: relative;
                clear: both;                
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            /*--------------ZONAS------------------*/
            #map-part {
                /*left: <?php echo floor(($imagesize[0]-50)/2)?>px;
                top: <?php echo floor(($imagesize[1]-50)/2)?>px;
                width: 50px;
                height: 50px;
                background-color:#BCBCBC;*/
                opacity:0.5;
                border: dashed 1px #000000;
                position: absolute;
            }
            #map-part:hover { opacity:0.8; }

            #map-part span{ display: none; }

            #map-part:hover span{
                display: block;
                vertical-align: top;
                color: #000;
                background-color: #F4F4F4;
                position: absolute;
                border: solid 1px  #BCBCBC;
                bottom: 100%;
                width: 75%;
            }

            /*-------------CAMARAS--------------*/
            .camara {
                cursor: pointer;
                position: absolute;
            }
            #contenido{
                float:left;
                width:97%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 80%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
        </style>
        <script type="text/javascript">
            // -------------- DIMENCION DEL iFRAME frameMap------------------//
            var widthToSet = <?php echo $imagesize[0]+10?>;
            var heightToSet = <?php echo $imagesize[1]+100?>;
            $('#frameMap' ,parent.document).width(widthToSet);
            $('#frameMap' ,parent.document).height(heightToSet);
            // ------------- DIMENCION DEL CONTENEDOR loadMap DEL iFRAME frameMap----//
            if ( parseInt($('#loadMap',parent.document).width()) < parseInt(widthToSet) ){
                $('#loadMap',parent.document).width(widthToSet+50);
            }
            if ( parseInt($('#loadMap',parent.document).height()) < parseInt(heightToSet) ){
                $('#loadMap',parent.document).height(heightToSet+50);
            }
            $(function(){
                $( "input:button, input:submit, button" ).button();
                <?php
                if (count($zonasCoord) > 0){
                    // ----------------- SET CSS ZONAS ------------------//
                    foreach ($zonasCoord as $row){

                        echo "  $('.zona".$row["idzona"]."').css({\n";
                        echo "      'left':'".$row["leftX"]."px',\n";
                        echo "      'top' :'".$row["topY"]."px',\n";
                        echo "      'width':'".$row["width"]."px',\n";
                        echo "      'height':'".$row["height"]."px',\n";
                        echo "      'background-color':'".$row["bgcolor"]."'\n";
                        echo "  });\n";
                    }
                }
                if (count($camarasCoord) > 0){
                    foreach ($camarasCoord as $row){
                         // ----------------- SET CSS CAMARA ------------------//
                        echo "  $('#camara".$row["idcamara"]."').css({\n";
                        echo "      'left':'".$row["leftX"]."px',\n";
                        echo "      'top' :'".$row["topY"]."px'\n";
                        echo "  });\n";
                    }
                }
               ?>
            });
        </script>
    </head>
    <body style="margin:0px;background:#fff;" >
        <center>
            <div id="contenido" align="center">
                <div id="titulo">
                    <?php echo $data[0]->plantafisica?>
                </div>
                <div id="map" >
                    <?php
                    if (count($zonasCoord)>0){
                        // ------------------- ZONAS -------------------//
                        foreach ($zonasCoord as $row){
                            echo "<div id=\"map-part\" class=\"zona".$row["idzona"]."\"><span>".$row["zona"]."</span></div>\n";
                        }
                    }
                    if (count($camarasCoord)>0){
                        foreach ($camarasCoord as $row){
                            echo "<img src=\"../images/Video-Camera-32x15.png\"  border=\"0\" class=\"camara\" id=\"camara".$row["idcamara"]."\" alt=\"".$row["camara"]."\" title=\"".$row["camara"]."\">\n";
                        }
                    }
                    ?>
                </div>
                <br>
                <input type="button" value="Volver" onclick="document.location.href = 'loadEvento.php?idevento=<?php echo $_REQUEST["idevento"];?>'" >
            </div>
        </center>
    </body>

</html>