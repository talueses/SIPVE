<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Proceso Ajax para cargar las camaras segun la zona seleccionada
 * Solo se listan las Camaras pertenecientes al segmento del usuario logueado
 * @author David Concepcion
 */
session_start(); // start up your PHP session!
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/vid_alarmaslist.control.op.php";

$str  = "<select name=\"idcamara\" id=\"idcamara\">";
$str .= "    <option value=\"\">&laquo; Todos &raquo;</option>";
if ($_REQUEST["idzona"]){
    $str .= "    ".Controller::make_combo("vid_zona_camara vzc,vid_camara vc,vid_servidor vs", "where vzc.idzona = '".$_REQUEST["idzona"]."' and vzc.idcamara = vc.idcamara and vc.idservidor = vs.idservidor and vs.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') order by vc.camara", "vc.idcamara", "vc.camara", "");
}else{
    $str .= "    ".Controller::make_combo("vid_camara", "order by camara", "idcamara", "camara", "");
}

$str .= "</select>";

echo json_encode(array("camaras"=>$str));
?>