<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDALARM"); //Categoria del modulo
include_once "../model/vid_alarmaslist.model.php";
include_once "../../inicio/controller/controller.php";

class ControlAlarmasList extends Controller {
    /**
     * @var string mensaje de exito o error
     */
    public $mensaje = null;
    /**
     * @var string mensaje de exito o error
     */
    public $error = false;
    /**
     * @var array nombre de campos campos
     */
    var $campos  = null;
    /**
     * @var string nombre de la entidad
     */
    var $entidad  = null;
    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }

    /**
     * Establece la Entidad
     * @param string $entidad Entidad
     */
    public function setEntidad($entidad){
         $this->entidad = $entidad;
    }

    /**
     * @return string Devuelve la Entidad establecida
     */
    public function getEntidad(){
         return $this->entidad;
    }
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Acceso a Monitoreo");        
        $this->setCampos("fecha_desde","Fecha Desde");
        $this->setCampos("fecha_hasta","Fecha Hasta");
    }
    /**
     * Establece los criterios de busqueda del listado de eventos
     * @return object Devuelve registros como objeto
     */
    function loadEventos() {
        $this->setNombres();

        if ($_REQUEST["fecha_desde"] == "" && $_REQUEST["fecha_hasta"] != "" || $_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] == ""){
            
            $datos   = array();
            //------------------------------------------------------------------------------------------------------------//
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_desde","tipoDato"=>"");
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_hasta","tipoDato"=>"");
            if (!$this->validarDatos($datos)){
                $mensaje = $this->mensaje;
            }

            $this->mensaje  = "<div class=\"ui-widget\">\n";
            $this->mensaje .= "    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">\n";
            $this->mensaje .= "        <p>\n";
            $this->mensaje .= "            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>\n";
            $this->mensaje .= "                ".$mensaje;
            $this->mensaje .= "        </p>\n";
            $this->mensaje .= "    </div>\n";
            $this->mensaje .= "</div>\n";
            $this->error = true;
        }

        // ---- Argumento de la consulta segun parametros seleccionados en la vista ---- //
        $arg = array();
        if ($_REQUEST["tipoEvento"] != ""){
            if ($_REQUEST["tipoEvento"] == "timelapse"){
                $arg["campos"] .= " and ve.file like '%timelapse.mpg'\n";
            }
            if ($_REQUEST["tipoEvento"] != "timelapse"){
                $arg["campos"] .= " and ve.file not like '%timelapse.mpg'\n";
            }
        }
        
        if ($_REQUEST["status"] != ""){
            $arg["campos"] .= " and ve.status = '".$_REQUEST["status"]."'\n";
        }
        
        if ($_REQUEST["idzona"] != ""){
            $arg["tablas"] .= ", vid_zona_camara vzc";
            $arg["campos"] .= " and vzc.idzona = '".$_REQUEST["idzona"]."' and vzc.idcamara =  vc.idcamara\n";
            
        }
        
        if ($_REQUEST["idcamara"] != ""){
            $arg["campos"] .= " and vc.idcamara = '".$_REQUEST["idcamara"]."'\n";
        }
        
        if ($_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] != ""){
            $arg["campos"] .= " and ve.fecha between '".Controller::formatoFecha($_REQUEST["fecha_desde"])." 00:00:00' and '".Controller::formatoFecha($_REQUEST["fecha_hasta"])." 23:59:59'\n";
        }
        
        $obj = new Alarmas();
        $data = $obj->getEventos($arg);
        
        return $data;
    }
    
    /**
     * Consulta de un evento de una camara
     * @return object Devuelve un registro como objeto
     */
    function loadEvento() {
        $obj = new Alarmas();
        return $obj->getEventos(array("campos"=>" and ve.idevento = '".$_REQUEST["idevento"]."'\n"));
    }
}
?>