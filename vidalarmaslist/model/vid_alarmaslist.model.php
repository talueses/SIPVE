<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php'; // conexion Base de Datos

/**
 * Clase Alarmas{}
 */
class Alarmas{
    /**
     * Consulta de eventos de una camara
     * Solo se listan eventos de Camaras pertenecientes al segmento del usuario logueado
     * @param int $idcamara Codigo de la camara
     * @param string $arg Complemnto SQL del argumento de la consulta
     * @return object Devuelve registros como objeto
     */
    function getEventos($arg){
        $sql = "select /*start*/ 
                       ve.*,vc.*,tz.location,tz.standard_time,vs.ipv4 as \"ipv4Servidor\",vc.ipv4 as \"ipv4Camara\",
                       (select file from vid_evento \"veAUX\" where ve.idservidor = \"veAUX\".idservidor and ve.camara = \"veAUX\".camara and ve.evento = \"veAUX\".evento and \"veAUX\".descripcion = 'picture_save' and concat(extract(year from ve.fecha),'-',extract(month from ve.fecha),'-',extract(day from ve.fecha)) = concat(extract(year from \"veAUX\".fecha),'-',extract(month from \"veAUX\".fecha),'-',extract(day from \"veAUX\".fecha)) limit 1)as file_picture_save
                       /*end*/
                from vid_camara vc, vid_servidor vs, timezone tz, vid_evento ve ".$arg["tablas"]."
                where vc.idservidor = ve.idservidor
                  and vc.numero     = cast(ve.camara as :cast_int)
                  and vc.idservidor = vs.idservidor
                  and vs.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."')
                  and vs.idtimezone = tz.idtimezone
		  and ve.descripcion = 'movie_start'
                  ".$arg["campos"]."
			order by idevento desc";
        //echo "<div align=\"left\"><pre>". str_replace("\"", "", paginationSQL::setSql($sql))."</pre></div>";        
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de zonas asociadas a una planta fisica de una camara
     * @param int $idcamara Codigo de la Camara
     * @param int $idplantafisica codigo de la planta fisica
     * @return object Devuelve registros como objeto
     */
    static function getZonasCamarasPlantafisica($idcamara,$idplantafisica){
        $sql = "select pf.plantafisica,
                       pf.file,
                       vpz.\"leftX\" as \"leftXZona\",
                       vpz.\"topY\" as \"topYZona\",
                       vpz.width,
                       vpz.height,
                       vpz.bgcolor,
                       vz.idzona,
                       vz.zona,
                       vz.descripcion,
                       vc.idcamara,
                       vc.camara,
                       vc.numero,
                       vc.idplantafisica as \"idplantafisicaCamara\",
                       vc.\"leftX\" as \"leftXCamara\",
                       vc.\"topY\" as \"topYCamara\"
                from plantafisica pf,
                     vid_plantafisica_zona vpz,
                     vid_zona vz,
                     vid_zona_camara vzc,
                     vid_camara vc
                where pf.idplantafisica = '".$idplantafisica."'
                  and pf.idplantafisica = vpz.idplantafisica
                  and vpz.idzona        = vz.idzona
                  and vz.idzona         = vzc.idzona
                  and vzc.idcamara      = '".$idcamara."'
                      ";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

}

?>
