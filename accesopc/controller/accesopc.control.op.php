<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "ADMACCPC"); //Categoria del modulo
require_once "accesopc.control.php";// Class CONTROL ControlAccesopc()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpAccesopc extends ControlAccesopc{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpAccesopc(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        
        list($_REQUEST["idaccesopc_tipo"],$path) = explode("-", $_REQUEST["accesopc_tipo"]);
        
        $_REQUEST["fecha_expiracion"] = date("Y-m-d",strtotime("next year"));
        $_REQUEST["cookie"] = md5($_REQUEST["usuario"].$_REQUEST["idaccesopc_tipo"].$_REQUEST["ipv4_pc"].$_REQUEST["fecha_expiracion"]);
        $_REQUEST["fecha_expiracion"] = self::formatoFecha($_REQUEST["fecha_expiracion"]);
        
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;
        
        //------------------ Metodo Set  -----------------//
        $exito = $this->setAccesopc();
        
        if (!$exito){
            if (preg_match("/duplicate entry/", strtolower($this->mensaje))){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser creado <br><b>No puede generar mas de un mismo acceso a un usuario</b>";
            }            
            return false;
        }
        
        /**
         * Establecer la Cookie en la maquina local
         */
        //    
        $_REQUEST["fecha_expiracion"] = self::formatoFecha($_REQUEST["fecha_expiracion"]);
        setcookie(md5($_REQUEST["usuario"]."-".$_REQUEST["idaccesopc_tipo"].$_REQUEST["ipv4_pc"]), $_REQUEST["cookie"], strtotime(date($_REQUEST["fecha_expiracion"]." 00:00:00")),$path);
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpAccesopc(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarAccesopc()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Acceso");
        
        $this->setCampos("idaccesopc_tipo","Tipo de Acceso");
        $this->setCampos("usuario","Usuario");
        $this->setCampos("ipv4_pc","IPv4");
        $this->setCampos("fecha_expiracion","Fecha de Creaci&oacute;n");
        $this->setCampos("cookie","Cookie");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idaccesopc_tipo","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"usuario","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"ipv4_pc","tipoDato"=>"esValidaIP");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"fecha_expiracion","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cookie","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

}
?>