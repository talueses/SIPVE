<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  * 
 \*******************************************************************************/
?>
<?php
require_once 'accesopc.tabla.php';

/**
 * Clase Accesopcs{}
 * @author David Concepcion CENIT-DIDI
 */
class Accesopcs extends Accesopc{

    /**
     * Consulta de Accesos a PCs y sus tipos
     * @return object Devuelve un registro como objeto
     */
    public function  getAccesosPcsTipo(){
        // --- Valores del Buscador --- //
        if ($_REQUEST["idaccesopcBuscador"]){
            $in = null;
            foreach ($_REQUEST["idaccesopcBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and acc.idaccesopc in (".$in.")";
        }
        
        $sql = "select /*start*/
                       acc.idaccesopc,
                       acc.idaccesopc_tipo,
                       acct.path,
                       acct.descripcion,
                       acc.usuario,
                       acc.ipv4_pc,
                       acc.fecha_expiracion,
                       acc.cookie
                       /*end*/
                from accesopc acc, accesopc_tipo acct
                where acc.idaccesopc_tipo = acct.idaccesopc_tipo
                    ".$arg."
                    order by acct.descripcion,acc.usuario,acc.ipv4_pc";
        //echo "<div align='left'><pre>".paginationSQL::setSql($sql)."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de un Acceso a PC y su tipo
     * @param type $idaccesopc
     * @return object Devuelve un registro como objeto
     */
    public function getAccesoPcTipo($idaccesopc){
        $sql = "select acc.idaccesopc,
                       acc.idaccesopc_tipo,
                       acct.path,
                       acct.descripcion,
                       acc.usuario,
                       acc.ipv4_pc,
                       acc.fecha_expiracion,
                       acc.cookie
                from accesopc acc, accesopc_tipo acct
                where acc.idaccesopc = '".$idaccesopc."'
                  and acc.idaccesopc_tipo = acct.idaccesopc_tipo
                    order by acct.descripcion,acc.usuario,acc.ipv4_pc";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();       
    }
    
}
?>
