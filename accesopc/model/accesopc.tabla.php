<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Accesopc {
    
    private $idaccesopc = null;
    private $idaccesopc_tipo = null;
    private $usuario = null;
    private $ipv4_pc = null;
    private $fecha_expiracion = null;
    private $cookie = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdaccesopc($idaccesopc){
        $this->idaccesopc = $idaccesopc;
    }
    public function getIdaccesopc(){
        return $this->idaccesopc;
    }
    public function setIdaccesopc_tipo($idaccesopc_tipo){
        $this->idaccesopc_tipo = $idaccesopc_tipo;
    }
    public function getIdaccesopc_tipo(){
        return $this->idaccesopc_tipo;
    }
    public function setUsuario($usuario){
        $this->usuario = $usuario;
    }
    public function getUsuario(){
        return $this->usuario;
    }
    public function setIpv4_pc($ipv4_pc){
        $this->ipv4_pc = $ipv4_pc;
    }
    public function getIpv4_pc(){
        return $this->ipv4_pc;
    }
    public function setFecha_expiracion($fecha_expiracion){
        $this->fecha_expiracion = $fecha_expiracion;
    }
    public function getFecha_expiracion(){
        return $this->fecha_expiracion;
    }
    public function setCookie($cookie){
        $this->cookie = $cookie;
    }
    public function getCookie(){
        return $this->cookie;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idaccesopc !== null) && (trim($this->idaccesopc)!=='') ){
            $campos .= "idaccesopc,";
            $valores .= "'".$this->idaccesopc."',";
        }
        if(($this->idaccesopc_tipo !== null) && (trim($this->idaccesopc_tipo)!=='') ){
            $campos .= "idaccesopc_tipo,";
            $valores .= "'".$this->idaccesopc_tipo."',";
        }
        if(($this->usuario !== null) && (trim($this->usuario)!=='') ){
            $campos .= "usuario,";
            $valores .= "'".$this->usuario."',";
        }
        if(($this->ipv4_pc !== null) && (trim($this->ipv4_pc)!=='') ){
            $campos .= "ipv4_pc,";
            $valores .= "'".$this->ipv4_pc."',";
        }
        if(($this->fecha_expiracion !== null) && (trim($this->fecha_expiracion)!=='') ){
            $campos .= "fecha_expiracion,";
            $valores .= "'".$this->fecha_expiracion."',";
        }
        if(($this->cookie !== null) && (trim($this->cookie)!=='') ){
            $campos .= "cookie,";
            $valores .= "'".$this->cookie."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO accesopc $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE accesopc SET ";
        
        if(($this->idaccesopc_tipo !== null) && (trim($this->idaccesopc_tipo)!=='') ){
            $sql .= "idaccesopc_tipo = '".$this->idaccesopc_tipo."',";
        }
        if(($this->usuario !== null) && (trim($this->usuario)!=='') ){
            $sql .= "usuario = '".$this->usuario."',";
        }
        if(($this->ipv4_pc !== null) && (trim($this->ipv4_pc)!=='') ){
            $sql .= "ipv4_pc = '".$this->ipv4_pc."',";
        }
        if(($this->fecha_expiracion !== null) && (trim($this->fecha_expiracion)!=='') ){
            $sql .= "fecha_expiracion = '".$this->fecha_expiracion."',";
        }
        if(($this->cookie !== null) && (trim($this->cookie)!=='') ){
            $sql .= "cookie = '".$this->cookie."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idaccesopc = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM accesopc  WHERE idaccesopc = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de Accesopcs
     * @return object Devuelve un registro como objeto
     */
    function  getAccesopcs(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idaccesopcBuscador"]){
            $in = null;
            foreach ($_REQUEST["idaccesopcBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idaccesopc in (".$in.")";
        }
        
        $sql = "select /*start*/ idaccesopc,idaccesopc_tipo,usuario,ipv4_pc,fecha_expiracion,cookie /*end*/ from accesopc ".$arg." order by usuario,ipv4_pc";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de Accesopc
     * @param int $idaccesopc Codigo
     * @return object Devuelve registros como objeto
     */
    function getAccesopc($idaccesopc){
        $sql = "SELECT idaccesopc,idaccesopc_tipo,usuario,ipv4_pc,fecha_expiracion,cookie FROM accesopc WHERE idaccesopc = '".$idaccesopc."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>