<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/accesopc.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new Accesopcs();
    $data = $obj->getAccesoPcTipo($_REQUEST["idaccesopc"]);
    
    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <!-- IU DatePicker jQuery Calendar Plugin -->
        <script type="text/javascript" src="../../inicio/js/jquery.ui.datepicker-es.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
            });
            //--- IU DatePicker jQuery Calendar Plugin ---//
        $(function(){
            /*$.datepicker.setDefaults( $.datepicker.regional['es'] );
            $('#fecha_expiracion').datepicker({
                inline: true,
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
            }).attr('readonly',true).css({
                background: '#fff' ,
                border: '1px solid #d5d5d5',
                '-moz-border-radius': '4px',
                '-webkit-border-radius': '4px',
                'border-radius': '4px'
            });*/
        });
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"]=="agregar"?"generar":$_REQUEST["accion"]);?> Acceso</div>
                <br/>
                <div class="ui-widget">
                    <div class="ui-state-highlight ui-corner-all info" style="margin-top: 20px; padding: 0 .7em;">
                        <span class="icon-info " style="float: left; margin-right: .3em;"></span>
                        <p>
                            Debe dirigirse al computador en donde desee generar el acceso al usuario.
                        </p>
                    </div>
                </div>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="accesopc.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr title="Tipo de Acceso">
                                <td align="right">Tipo de Acceso:</td>
                                <td>
                                    <select name="accesopc_tipo" id="accesopc_tipo" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php echo (ControlAccesopc::make_combo("accesopc_tipo","order by descripcion", "idaccesopc_tipo||'-'||path as value", "descripcion", $data->idaccesopc_tipo."-".$data->path,false));?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Usuario">
                                <td align="right">Usuario:</td>
                                <td>
                                    <select name="usuario" id="usuario" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php echo ControlAccesopc::make_combo("usuario","where \"superUsuario\" = '0' order by nombre, apellido", "usuario", "usuario||' - '||nombre||' '||apellido as descripcion", $data->usuario,false);?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="IPv4">
                                <td align="right">IPv4:</td>
                                <td>
                                    <input type="text" name="ipv4_pc" id="ipv4_pc" maxlength="15"  value="<?php echo $data->ipv4_pc==""?$_SERVER["REMOTE_ADDR"]:$data->ipv4_pc;?>" readonly>
                                    <small class="comment">IP de la m&aacute;quina a generar el acceso</small>
                                </td>
                            </tr>
                            <tr title="Fecha de Creaci&oacute;n">
                                <td align="right">Fecha de Creaci&oacute;n: </td>
                                <td>
                                    <input type="text" value="<?php echo $data->fecha_expiracion ? date("d-m-Y",strtotime("last year",strtotime($data->fecha_expiracion))) : date("d-m-Y");?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <!--tr title="Cookie">
                                <td align="right">Cookie:</td>
                                <td>
                                    <input type="text" name="cookie" id="cookie" maxlength="40" value="<?php echo $data->cookie;?>" <?php echo $disabled;?>>
                                </td>
                            </tr-->
                            
                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idaccesopc" id="idaccesopc" value="<?php echo $data->idaccesopc ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"]=="agregar"?"generar":$_REQUEST["accion"])." Acceso\" />";
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>