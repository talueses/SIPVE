<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../model/ctrl_sincronizar.model.php"; // Class MODEL CtrlSincronizar()
require_once "ctrl_sincronizar.controller.php";// Class CONTROL Controller()

/**
 * Description
 * @author David Concepcion
 */
class ControlCtrlSincronizar extends Controller{
    /**
     * @var string mensaje de exito o error
     */
    var $mensaje = null;
    /**
     * @var string accion agregar, modificar o eliminar dato
     */
    var $accion  = null;
    /**
     * @var array nombre de campos campos
     */
    var $campos  = null;
    /**
     * @var string nombre de la entidad 
     */
    var $entidad  = null;

    /**
     * Establece la acción
     * @param string $accion Acción
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }

    /**
     * @return string Devuelve la accion establecida
     */
    public function getAccion(){
         return $this->accion;
    }

    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }

    /**
     * Establece la Entidad
     * @param string $entidad Entidad
     */
    public function setEntidad($entidad){
         $this->entidad = $entidad;
    }

    /**
     * @return string Devuelve la Entidad establecida
     */
    public function getEntidad(){
         return $this->entidad;
    }
    
    /**
     * Agregar o modificar un CtrlSincronizar
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setCtrlSincronizar(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;

        $obj = new CtrlSincronizar();

        //--------------------- DATOS --------------------------//
        $obj->setId($_REQUEST["id"]);
        
        $obj->setItem($_REQUEST["item"]);
        $obj->setAccion($_REQUEST["accion"]);
        $obj->setCodigoaccion($_REQUEST["codigoaccion"]);
        $obj->setNodo($_REQUEST["nodo"]);
        $obj->setIpv4($_REQUEST["ipv4"]);
        $obj->setPuerto($_REQUEST["puerto"]);
        $obj->setPrioridad($_REQUEST["prioridad"]);
        $obj->setParam1($_REQUEST["param1"]);
        $obj->setParam2($_REQUEST["param2"]);
        $obj->setParam3($_REQUEST["param3"]);
        $obj->setParam4($_REQUEST["param4"]);
        $obj->setParam5($_REQUEST["param5"]);
        $obj->setParam6($_REQUEST["param6"]);
        $obj->setParam7($_REQUEST["param7"]);
        $obj->setParam8($_REQUEST["param8"]);
        $obj->setParam9($_REQUEST["param9"]);
        $obj->setParam10($_REQUEST["param10"]);
        $obj->setParam11($_REQUEST["param11"]);
        $obj->setParam12($_REQUEST["param12"]);
        $obj->setParam13($_REQUEST["param13"]);
        $obj->setParam14($_REQUEST["param14"]);
        $obj->setParam15($_REQUEST["param15"]);
        $obj->setParam16($_REQUEST["param16"]);
        $obj->setParam17($_REQUEST["param17"]);
        $obj->setParam18($_REQUEST["param18"]);
        $obj->setParam19($_REQUEST["param19"]);
        $obj->setParam20($_REQUEST["param20"]);
        $obj->setParam21($_REQUEST["param21"]);
        $obj->setParam22($_REQUEST["param22"]);
        $obj->setParam23($_REQUEST["param23"]);
        $obj->setParam24($_REQUEST["param24"]);
        $obj->setParam25($_REQUEST["param25"]);
        $obj->setParam26($_REQUEST["param26"]);
        $obj->setParam27($_REQUEST["param27"]);
        $obj->setParam28($_REQUEST["param28"]);
        $obj->setParam29($_REQUEST["param29"]);
        $obj->setParam30($_REQUEST["param30"]);
        $obj->setParam31($_REQUEST["param31"]);
        $obj->setParam32($_REQUEST["param32"]);
        $obj->setParam33($_REQUEST["param33"]);
        $obj->setParam34($_REQUEST["param34"]);
        $obj->setParam35($_REQUEST["param35"]);
        $obj->setParam36($_REQUEST["param36"]);
        $obj->setParam37($_REQUEST["param37"]);
        $obj->setParam38($_REQUEST["param38"]);
        $obj->setParam39($_REQUEST["param39"]);
        $obj->setParam40($_REQUEST["param40"]);
        

        if ($this->getAccion()=="agregar"){
            $exito = $obj->insertarRegistro();
        }

        if ($this->getAccion()=="modificar"){
            $exito = $obj->modificarRegistro($obj->getId($_REQUEST["id"]));
        }

        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            if ($this->getAccion()=="agregar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser creado: ".$obj->getMensaje();
            }
            if ($this->getAccion()=="modificar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser modificado: ".$obj->getMensaje();
            }
            return false;
        }
        // ------------------------------- MENSAJE EXITO --------------------------------//
        if ($this->getAccion()=="agregar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; creado exitosamente...";
        }
        if ($this->getAccion()=="modificar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; modificado exitosamente...";
        }


        return true;
    }

    /**
     * === Eliminar CtrlSincronizar ===
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarCtrlSincronizar(){
        // -------------------- ELIMINAR LECTORA ----------------------//
        $obj = new CtrlSincronizar();
        //--------------------- DATOS --------------------------//
        $obj->setId($_REQUEST["id"]);

        $exito = $obj->eliminarRegistro($obj->getId($_REQUEST["id"]));
        if(!$exito){
            $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser eliminado: ".$obj->getMensaje();
            return false;
        }

        $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; eliminado exitosamente...";
        return true;
    }

    
    
}
?>
