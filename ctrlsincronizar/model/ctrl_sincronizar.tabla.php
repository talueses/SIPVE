<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlSincronizar {
    
    private $id = null;
    private $item = null;
    private $accion = null;
    private $codigoaccion = null;
    private $nodo = null;
    private $ipv4 = null;
    private $puerto = null;
    private $prioridad = null;
    private $param1 = null;
    private $param2 = null;
    private $param3 = null;
    private $param4 = null;
    private $param5 = null;
    private $param6 = null;
    private $param7 = null;
    private $param8 = null;
    private $param9 = null;
    private $param10 = null;
    private $param11 = null;
    private $param12 = null;
    private $param13 = null;
    private $param14 = null;
    private $param15 = null;
    private $param16 = null;
    private $param17 = null;
    private $param18 = null;
    private $param19 = null;
    private $param20 = null;
    private $param21 = null;
    private $param22 = null;
    private $param23 = null;
    private $param24 = null;
    private $param25 = null;
    private $param26 = null;
    private $param27 = null;
    private $param28 = null;
    private $param29 = null;
    private $param30 = null;
    private $param31 = null;
    private $param32 = null;
    private $param33 = null;
    private $param34 = null;
    private $param35 = null;
    private $param36 = null;
    private $param37 = null;
    private $param38 = null;
    private $param39 = null;
    private $param40 = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    public function setItem($item){
        $this->item = $item;
    }
    public function getItem(){
        return $this->item;
    }
    public function setAccion($accion){
        $this->accion = $accion;
    }
    public function getAccion(){
        return $this->accion;
    }
    public function setCodigoaccion($codigoaccion){
        $this->codigoaccion = $codigoaccion;
    }
    public function getCodigoaccion(){
        return $this->codigoaccion;
    }
    public function setNodo($nodo){
        $this->nodo = $nodo;
    }
    public function getNodo(){
        return $this->nodo;
    }
    public function setIpv4($ipv4){
        $this->ipv4 = $ipv4;
    }
    public function getIpv4(){
        return $this->ipv4;
    }
    public function setPuerto($puerto){
        $this->puerto = $puerto;
    }
    public function getPuerto(){
        return $this->puerto;
    }
    public function setPrioridad($prioridad){
        $this->prioridad = $prioridad;
    }
    public function getPrioridad(){
        return $this->prioridad;
    }
    public function setParam1($param1){
        $this->param1 = $param1;
    }
    public function getParam1(){
        return $this->param1;
    }
    public function setParam2($param2){
        $this->param2 = $param2;
    }
    public function getParam2(){
        return $this->param2;
    }
    public function setParam3($param3){
        $this->param3 = $param3;
    }
    public function getParam3(){
        return $this->param3;
    }
    public function setParam4($param4){
        $this->param4 = $param4;
    }
    public function getParam4(){
        return $this->param4;
    }
    public function setParam5($param5){
        $this->param5 = $param5;
    }
    public function getParam5(){
        return $this->param5;
    }
    public function setParam6($param6){
        $this->param6 = $param6;
    }
    public function getParam6(){
        return $this->param6;
    }
    public function setParam7($param7){
        $this->param7 = $param7;
    }
    public function getParam7(){
        return $this->param7;
    }
    public function setParam8($param8){
        $this->param8 = $param8;
    }
    public function getParam8(){
        return $this->param8;
    }
    public function setParam9($param9){
        $this->param9 = $param9;
    }
    public function getParam9(){
        return $this->param9;
    }
    public function setParam10($param10){
        $this->param10 = $param10;
    }
    public function getParam10(){
        return $this->param10;
    }
    public function setParam11($param11){
        $this->param11 = $param11;
    }
    public function getParam11(){
        return $this->param11;
    }
    public function setParam12($param12){
        $this->param12 = $param12;
    }
    public function getParam12(){
        return $this->param12;
    }
    public function setParam13($param13){
        $this->param13 = $param13;
    }
    public function getParam13(){
        return $this->param13;
    }
    public function setParam14($param14){
        $this->param14 = $param14;
    }
    public function getParam14(){
        return $this->param14;
    }
    public function setParam15($param15){
        $this->param15 = $param15;
    }
    public function getParam15(){
        return $this->param15;
    }
    public function setParam16($param16){
        $this->param16 = $param16;
    }
    public function getParam16(){
        return $this->param16;
    }
    public function setParam17($param17){
        $this->param17 = $param17;
    }
    public function getParam17(){
        return $this->param17;
    }
    public function setParam18($param18){
        $this->param18 = $param18;
    }
    public function getParam18(){
        return $this->param18;
    }
    public function setParam19($param19){
        $this->param19 = $param19;
    }
    public function getParam19(){
        return $this->param19;
    }
    public function setParam20($param20){
        $this->param20 = $param20;
    }
    public function getParam20(){
        return $this->param20;
    }
    public function setParam21($param21){
        $this->param21 = $param21;
    }
    public function getParam21(){
        return $this->param21;
    }
    public function setParam22($param22){
        $this->param22 = $param22;
    }
    public function getParam22(){
        return $this->param22;
    }
    public function setParam23($param23){
        $this->param23 = $param23;
    }
    public function getParam23(){
        return $this->param23;
    }
    public function setParam24($param24){
        $this->param24 = $param24;
    }
    public function getParam24(){
        return $this->param24;
    }
    public function setParam25($param25){
        $this->param25 = $param25;
    }
    public function getParam25(){
        return $this->param25;
    }
    public function setParam26($param26){
        $this->param26 = $param26;
    }
    public function getParam26(){
        return $this->param26;
    }
    public function setParam27($param27){
        $this->param27 = $param27;
    }
    public function getParam27(){
        return $this->param27;
    }
    public function setParam28($param28){
        $this->param28 = $param28;
    }
    public function getParam28(){
        return $this->param28;
    }
    public function setParam29($param29){
        $this->param29 = $param29;
    }
    public function getParam29(){
        return $this->param29;
    }
    public function setParam30($param30){
        $this->param30 = $param30;
    }
    public function getParam30(){
        return $this->param30;
    }
    public function setParam31($param31){
        $this->param31 = $param31;
    }
    public function getParam31(){
        return $this->param31;
    }
    public function setParam32($param32){
        $this->param32 = $param32;
    }
    public function getParam32(){
        return $this->param32;
    }
    public function setParam33($param33){
        $this->param33 = $param33;
    }
    public function getParam33(){
        return $this->param33;
    }
    public function setParam34($param34){
        $this->param34 = $param34;
    }
    public function getParam34(){
        return $this->param34;
    }
    public function setParam35($param35){
        $this->param35 = $param35;
    }
    public function getParam35(){
        return $this->param35;
    }
    public function setParam36($param36){
        $this->param36 = $param36;
    }
    public function getParam36(){
        return $this->param36;
    }
    public function setParam37($param37){
        $this->param37 = $param37;
    }
    public function getParam37(){
        return $this->param37;
    }
    public function setParam38($param38){
        $this->param38 = $param38;
    }
    public function getParam38(){
        return $this->param38;
    }
    public function setParam39($param39){
        $this->param39 = $param39;
    }
    public function getParam39(){
        return $this->param39;
    }
    public function setParam40($param40){
        $this->param40 = $param40;
    }
    public function getParam40(){
        return $this->param40;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->id !== null) && (trim($this->id)!=='') ){
            $campos .= "id,";
            $valores .= "'".$this->id."',";
        }
        if(($this->item !== null) && (trim($this->item)!=='') ){
            $campos .= "item,";
            $valores .= "'".$this->item."',";
        }
        if(($this->accion !== null) && (trim($this->accion)!=='') ){
            $campos .= "accion,";
            $valores .= "'".$this->accion."',";
        }
        if(($this->codigoaccion !== null) && (trim($this->codigoaccion)!=='') ){
            $campos .= "codigoaccion,";
            $valores .= "'".$this->codigoaccion."',";
        }
        if(($this->nodo !== null) && (trim($this->nodo)!=='') ){
            $campos .= "nodo,";
            $valores .= "'".$this->nodo."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $campos .= "ipv4,";
            $valores .= "'".$this->ipv4."',";
        }
        if(($this->puerto !== null) && (trim($this->puerto)!=='') ){
            $campos .= "puerto,";
            $valores .= "'".$this->puerto."',";
        }
        if(($this->prioridad !== null) && (trim($this->prioridad)!=='') ){
            $campos .= "prioridad,";
            $valores .= "'".$this->prioridad."',";
        }
        if(($this->param1 !== null) && (trim($this->param1)!=='') ){
            $campos .= "param1,";
            $valores .= "'".$this->param1."',";
        }
        if(($this->param2 !== null) && (trim($this->param2)!=='') ){
            $campos .= "param2,";
            $valores .= "'".$this->param2."',";
        }
        if(($this->param3 !== null) && (trim($this->param3)!=='') ){
            $campos .= "param3,";
            $valores .= "'".$this->param3."',";
        }
        if(($this->param4 !== null) && (trim($this->param4)!=='') ){
            $campos .= "param4,";
            $valores .= "'".$this->param4."',";
        }
        if(($this->param5 !== null) && (trim($this->param5)!=='') ){
            $campos .= "param5,";
            $valores .= "'".$this->param5."',";
        }
        if(($this->param6 !== null) && (trim($this->param6)!=='') ){
            $campos .= "param6,";
            $valores .= "'".$this->param6."',";
        }
        if(($this->param7 !== null) && (trim($this->param7)!=='') ){
            $campos .= "param7,";
            $valores .= "'".$this->param7."',";
        }
        if(($this->param8 !== null) && (trim($this->param8)!=='') ){
            $campos .= "param8,";
            $valores .= "'".$this->param8."',";
        }
        if(($this->param9 !== null) && (trim($this->param9)!=='') ){
            $campos .= "param9,";
            $valores .= "'".$this->param9."',";
        }
        if(($this->param10 !== null) && (trim($this->param10)!=='') ){
            $campos .= "param10,";
            $valores .= "'".$this->param10."',";
        }
        if(($this->param11 !== null) && (trim($this->param11)!=='') ){
            $campos .= "param11,";
            $valores .= "'".$this->param11."',";
        }
        if(($this->param12 !== null) && (trim($this->param12)!=='') ){
            $campos .= "param12,";
            $valores .= "'".$this->param12."',";
        }
        if(($this->param13 !== null) && (trim($this->param13)!=='') ){
            $campos .= "param13,";
            $valores .= "'".$this->param13."',";
        }
        if(($this->param14 !== null) && (trim($this->param14)!=='') ){
            $campos .= "param14,";
            $valores .= "'".$this->param14."',";
        }
        if(($this->param15 !== null) && (trim($this->param15)!=='') ){
            $campos .= "param15,";
            $valores .= "'".$this->param15."',";
        }
        if(($this->param16 !== null) && (trim($this->param16)!=='') ){
            $campos .= "param16,";
            $valores .= "'".$this->param16."',";
        }
        if(($this->param17 !== null) && (trim($this->param17)!=='') ){
            $campos .= "param17,";
            $valores .= "'".$this->param17."',";
        }
        if(($this->param18 !== null) && (trim($this->param18)!=='') ){
            $campos .= "param18,";
            $valores .= "'".$this->param18."',";
        }
        if(($this->param19 !== null) && (trim($this->param19)!=='') ){
            $campos .= "param19,";
            $valores .= "'".$this->param19."',";
        }
        if(($this->param20 !== null) && (trim($this->param20)!=='') ){
            $campos .= "param20,";
            $valores .= "'".$this->param20."',";
        }
        if(($this->param21 !== null) && (trim($this->param21)!=='') ){
            $campos .= "param21,";
            $valores .= "'".$this->param21."',";
        }
        if(($this->param22 !== null) && (trim($this->param22)!=='') ){
            $campos .= "param22,";
            $valores .= "'".$this->param22."',";
        }
        if(($this->param23 !== null) && (trim($this->param23)!=='') ){
            $campos .= "param23,";
            $valores .= "'".$this->param23."',";
        }
        if(($this->param24 !== null) && (trim($this->param24)!=='') ){
            $campos .= "param24,";
            $valores .= "'".$this->param24."',";
        }
        if(($this->param25 !== null) && (trim($this->param25)!=='') ){
            $campos .= "param25,";
            $valores .= "'".$this->param25."',";
        }
        if(($this->param26 !== null) && (trim($this->param26)!=='') ){
            $campos .= "param26,";
            $valores .= "'".$this->param26."',";
        }
        if(($this->param27 !== null) && (trim($this->param27)!=='') ){
            $campos .= "param27,";
            $valores .= "'".$this->param27."',";
        }
        if(($this->param28 !== null) && (trim($this->param28)!=='') ){
            $campos .= "param28,";
            $valores .= "'".$this->param28."',";
        }
        if(($this->param29 !== null) && (trim($this->param29)!=='') ){
            $campos .= "param29,";
            $valores .= "'".$this->param29."',";
        }
        if(($this->param30 !== null) && (trim($this->param30)!=='') ){
            $campos .= "param30,";
            $valores .= "'".$this->param30."',";
        }
        if(($this->param31 !== null) && (trim($this->param31)!=='') ){
            $campos .= "param31,";
            $valores .= "'".$this->param31."',";
        }
        if(($this->param32 !== null) && (trim($this->param32)!=='') ){
            $campos .= "param32,";
            $valores .= "'".$this->param32."',";
        }
        if(($this->param33 !== null) && (trim($this->param33)!=='') ){
            $campos .= "param33,";
            $valores .= "'".$this->param33."',";
        }
        if(($this->param34 !== null) && (trim($this->param34)!=='') ){
            $campos .= "param34,";
            $valores .= "'".$this->param34."',";
        }
        if(($this->param35 !== null) && (trim($this->param35)!=='') ){
            $campos .= "param35,";
            $valores .= "'".$this->param35."',";
        }
        if(($this->param36 !== null) && (trim($this->param36)!=='') ){
            $campos .= "param36,";
            $valores .= "'".$this->param36."',";
        }
        if(($this->param37 !== null) && (trim($this->param37)!=='') ){
            $campos .= "param37,";
            $valores .= "'".$this->param37."',";
        }
        if(($this->param38 !== null) && (trim($this->param38)!=='') ){
            $campos .= "param38,";
            $valores .= "'".$this->param38."',";
        }
        if(($this->param39 !== null) && (trim($this->param39)!=='') ){
            $campos .= "param39,";
            $valores .= "'".$this->param39."',";
        }
        if(($this->param40 !== null) && (trim($this->param40)!=='') ){
            $campos .= "param40,";
            $valores .= "'".$this->param40."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_sincronizar $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_sincronizar SET ";
        
        if(($this->item !== null) && (trim($this->item)!=='') ){
            $sql .= "item = '".$this->item."',";
        }
        if(($this->accion !== null) && (trim($this->accion)!=='') ){
            $sql .= "accion = '".$this->accion."',";
        }
        if(($this->codigoaccion !== null) && (trim($this->codigoaccion)!=='') ){
            $sql .= "codigoaccion = '".$this->codigoaccion."',";
        }
        if(($this->nodo !== null) && (trim($this->nodo)!=='') ){
            $sql .= "nodo = '".$this->nodo."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $sql .= "ipv4 = '".$this->ipv4."',";
        }
        if(($this->puerto !== null) && (trim($this->puerto)!=='') ){
            $sql .= "puerto = '".$this->puerto."',";
        }
        if(($this->prioridad !== null) && (trim($this->prioridad)!=='') ){
            $sql .= "prioridad = '".$this->prioridad."',";
        }
        if(($this->param1 !== null) && (trim($this->param1)!=='') ){
            $sql .= "param1 = '".$this->param1."',";
        }
        if(($this->param2 !== null) && (trim($this->param2)!=='') ){
            $sql .= "param2 = '".$this->param2."',";
        }
        if(($this->param3 !== null) && (trim($this->param3)!=='') ){
            $sql .= "param3 = '".$this->param3."',";
        }
        if(($this->param4 !== null) && (trim($this->param4)!=='') ){
            $sql .= "param4 = '".$this->param4."',";
        }
        if(($this->param5 !== null) && (trim($this->param5)!=='') ){
            $sql .= "param5 = '".$this->param5."',";
        }
        if(($this->param6 !== null) && (trim($this->param6)!=='') ){
            $sql .= "param6 = '".$this->param6."',";
        }
        if(($this->param7 !== null) && (trim($this->param7)!=='') ){
            $sql .= "param7 = '".$this->param7."',";
        }
        if(($this->param8 !== null) && (trim($this->param8)!=='') ){
            $sql .= "param8 = '".$this->param8."',";
        }
        if(($this->param9 !== null) && (trim($this->param9)!=='') ){
            $sql .= "param9 = '".$this->param9."',";
        }
        if(($this->param10 !== null) && (trim($this->param10)!=='') ){
            $sql .= "param10 = '".$this->param10."',";
        }
        if(($this->param11 !== null) && (trim($this->param11)!=='') ){
            $sql .= "param11 = '".$this->param11."',";
        }
        if(($this->param12 !== null) && (trim($this->param12)!=='') ){
            $sql .= "param12 = '".$this->param12."',";
        }
        if(($this->param13 !== null) && (trim($this->param13)!=='') ){
            $sql .= "param13 = '".$this->param13."',";
        }
        if(($this->param14 !== null) && (trim($this->param14)!=='') ){
            $sql .= "param14 = '".$this->param14."',";
        }
        if(($this->param15 !== null) && (trim($this->param15)!=='') ){
            $sql .= "param15 = '".$this->param15."',";
        }
        if(($this->param16 !== null) && (trim($this->param16)!=='') ){
            $sql .= "param16 = '".$this->param16."',";
        }
        if(($this->param17 !== null) && (trim($this->param17)!=='') ){
            $sql .= "param17 = '".$this->param17."',";
        }
        if(($this->param18 !== null) && (trim($this->param18)!=='') ){
            $sql .= "param18 = '".$this->param18."',";
        }
        if(($this->param19 !== null) && (trim($this->param19)!=='') ){
            $sql .= "param19 = '".$this->param19."',";
        }
        if(($this->param20 !== null) && (trim($this->param20)!=='') ){
            $sql .= "param20 = '".$this->param20."',";
        }
        if(($this->param21 !== null) && (trim($this->param21)!=='') ){
            $sql .= "param21 = '".$this->param21."',";
        }
        if(($this->param22 !== null) && (trim($this->param22)!=='') ){
            $sql .= "param22 = '".$this->param22."',";
        }
        if(($this->param23 !== null) && (trim($this->param23)!=='') ){
            $sql .= "param23 = '".$this->param23."',";
        }
        if(($this->param24 !== null) && (trim($this->param24)!=='') ){
            $sql .= "param24 = '".$this->param24."',";
        }
        if(($this->param25 !== null) && (trim($this->param25)!=='') ){
            $sql .= "param25 = '".$this->param25."',";
        }
        if(($this->param26 !== null) && (trim($this->param26)!=='') ){
            $sql .= "param26 = '".$this->param26."',";
        }
        if(($this->param27 !== null) && (trim($this->param27)!=='') ){
            $sql .= "param27 = '".$this->param27."',";
        }
        if(($this->param28 !== null) && (trim($this->param28)!=='') ){
            $sql .= "param28 = '".$this->param28."',";
        }
        if(($this->param29 !== null) && (trim($this->param29)!=='') ){
            $sql .= "param29 = '".$this->param29."',";
        }
        if(($this->param30 !== null) && (trim($this->param30)!=='') ){
            $sql .= "param30 = '".$this->param30."',";
        }
        if(($this->param31 !== null) && (trim($this->param31)!=='') ){
            $sql .= "param31 = '".$this->param31."',";
        }
        if(($this->param32 !== null) && (trim($this->param32)!=='') ){
            $sql .= "param32 = '".$this->param32."',";
        }
        if(($this->param33 !== null) && (trim($this->param33)!=='') ){
            $sql .= "param33 = '".$this->param33."',";
        }
        if(($this->param34 !== null) && (trim($this->param34)!=='') ){
            $sql .= "param34 = '".$this->param34."',";
        }
        if(($this->param35 !== null) && (trim($this->param35)!=='') ){
            $sql .= "param35 = '".$this->param35."',";
        }
        if(($this->param36 !== null) && (trim($this->param36)!=='') ){
            $sql .= "param36 = '".$this->param36."',";
        }
        if(($this->param37 !== null) && (trim($this->param37)!=='') ){
            $sql .= "param37 = '".$this->param37."',";
        }
        if(($this->param38 !== null) && (trim($this->param38)!=='') ){
            $sql .= "param38 = '".$this->param38."',";
        }
        if(($this->param39 !== null) && (trim($this->param39)!=='') ){
            $sql .= "param39 = '".$this->param39."',";
        }
        if(($this->param40 !== null) && (trim($this->param40)!=='') ){
            $sql .= "param40 = '".$this->param40."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_sincronizar  WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlSincronizars
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlSincronizars(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idBuscador"]){
            $in = null;
            foreach ($_REQUEST["idBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where id in (".$in.")";
        }
        
        $sql = "select id,item,accion,codigoaccion,nodo,ipv4,puerto,prioridad,param1,param2,param3,param4,param5,param6,param7,param8,param9,param10,param11,param12,param13,param14,param15,param16,param17,param18,param19,param20,param21,param22,param23,param24,param25,param26,param27,param28,param29,param30,param31,param32,param33,param34,param35,param36,param37,param38,param39,param40 from ctrl_sincronizar ".$arg." order by item,accion";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlSincronizar
     * @param int $id Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlSincronizar($id){
        $sql = "SELECT id,item,accion,codigoaccion,nodo,ipv4,puerto,prioridad,param1,param2,param3,param4,param5,param6,param7,param8,param9,param10,param11,param12,param13,param14,param15,param16,param17,param18,param19,param20,param21,param22,param23,param24,param25,param26,param27,param28,param29,param30,param31,param32,param33,param34,param35,param36,param37,param38,param39,param40 FROM ctrl_sincronizar WHERE id = '".$id."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>