<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'ctrl_usuario.tabla.php';

/**
 * Clase CtrlUsuarios{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlUsuarios extends CtrlUsuario{    
    
    /**
     * Consulta de Usuarios de Acceso
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlAccUsuarios(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idusuarioBuscador"]){
            $in = null;
            foreach ($_REQUEST["idusuarioBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and idusuario in (".$in.")";
        }
        
        $sql = "select /*start*/
                       distinct cu.idusuario,
                       cu.idgrupo,
                       cu.idzonadetiempo,
                       cu.iddepartamento,
                       cu.tipo_tarjeta,
                       cu.numero,
                       cu.codigo_pin,
                       cu.codigo_id,
                       cu.codigo_sitio,
                       cu.codigo_tarjeta,
                       cu.tarjetayopin,
                       cu.tienefechalimite,
                       cu.fechahasta,
                       cu.rondasguardias,
                       cu.skipfpcheck,
                       cu.modificable,
                       cu.nivel,
                       cu.antipassback,
                       cast(cu.cedula as :cast_int) as cedula,
                       cu.numero_visitante,
                       cu.nombres,
                       cu.apellidos,
                       cu.sexo,
                       cu.fecha_nacimiento,
                       cu.telefono,
                       cu.telefono_celular,
                       cu.direccion,
                       cu.email 
                       /*end*/
                from ctrl_usuario cu, 
                     ctrl_grupo_puerta cgp
                     left join ctrl_lectora cl      on (cgp.idpuerta = cl.idpuerta) 
                     left join ctrl_controladora cc on (cl.idcontroladora = cc.idcontroladora
                                                    and cc.idsegmento    in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."'))
                where cu.idgrupo = cgp.idgrupo                  
                ".$arg." 
                    order by cu.tipo_tarjeta,cast(cu.cedula as :cast_int), numero_visitante  ";
        
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Usuario de Acceso
     * @param int $idusuario Codigo de usuario
     * @return object Devuelve registros como objeto
     */
    function getCtrlAccUsuario($idusuario){
        $sql = "SELECT cu.idusuario,
                       cu.idgrupo,
                       cu.idzonadetiempo,
                       cu.iddepartamento,
                       cu.numero,
                       cu.tipo_tarjeta,
                       cu.codigo_pin,
                       cu.codigo_id,
                       cu.codigo_sitio,
                       cu.codigo_tarjeta,
                       cu.tarjetayopin,
                       cu.tienefechalimite,
                       cu.fechahasta,
                       cu.rondasguardias,
                       cu.skipfpcheck,
                       cu.modificable,
                       cu.nivel,
                       cu.antipassback,
                       cu.cedula,
                       cu.numero_visitante,
                       cu.nombres,
                       cu.apellidos,
                       cu.sexo,
                       cu.fecha_nacimiento,
                       cu.telefono,
                       cu.telefono_celular,
                       cu.direccion,
                       cu.email,
                       
                       /*** CARNET EMPLEADO ***/
                       ce.idcarnet_empleado as carnetEmpleado, 	

                       /*** CARNET VISITANTE ***/                       
                       cv.idcarnet_visitante as carnetVisitante
                       
                FROM ctrl_usuario cu
                    left join car_empleado ce on (ce.cedula = cu.cedula)
                    left join car_visitante cv on (cv.numero = cu.numero_visitante)
                WHERE idusuario = '".$idusuario."'";
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    
    /**
     * Consulta de Cedula ya existenta
     * @param array $arg argumentos de la consulta
     * @return object Devuelve registros como objeto
     */
    static function getCarnetChk($arg){
        if ($arg["tipo_tarjeta"]=="empleado"){
            $sql = "select count(cedula) as \"hasCarnet\",archivo_foto from car_empleado where cedula = '".$arg["cedula"]."' group by cedula ,archivo_foto";
        }
        if ($arg["tipo_tarjeta"]=="visitante"){
            $sql = "select count(numero) as \"hasCarnet\" from car_visitante where numero = '".$arg["numero_visitante"]."' group by numero";
        }
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    
    
    /**
     * Consulta de los numeros existentes de puertas
     * @return object Devuelve registros como objeto
     */
    function getNumerosUsuarios() {
        $sql = "SELECT numero,numero_visitante,tipo_tarjeta FROM ctrl_usuario";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        $fetchAll = $res->fetchAll(PDO::FETCH_CLASS);
        
        foreach ($fetchAll as $row){
            $usuario[] = $row->numero;
            
            if ($row->tipo_tarjeta=="visitante"){
                $visitante[] =  $row->numero_visitante;
            }
        }
        
        
        /*$link = DbLink::getLink();
        $result = mysql_query($sql, $link);
        if (!$result) {
            $this->mensaje = mysql_error($link);
            return false;
        }
        while($row = mysql_fetch_assoc($result)){
            
            $usuario[] = $row["numero"];
            
            if ($row["tipo_tarjeta"]=="visitante"){
                $visitante[] =  $row["numero_visitante"];
            }
        }    */    
        $ret = new stdClass();
        $ret->numero = (object) $usuario;
        $ret->visitante = (object)  $visitante;        
        return $ret;
    }
    
}
?>
