<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlUsuario {
    
    private $idusuario = null;
    private $idgrupo = null;
    private $idzonadetiempo = null;
    private $iddepartamento = null;
    private $numero = null;
    private $tipo_tarjeta = null;
    private $codigo_pin = null;
    private $codigo_id = null;
    private $codigo_sitio = null;
    private $codigo_tarjeta = null;
    private $tarjetayopin = null;
    private $tienefechalimite = null;
    private $fechahasta = null;
    private $rondasguardias = null;
    private $skipfpcheck = null;
    private $modificable = null;
    private $nivel = null;
    private $antipassback = null;
    private $cedula = null;
    private $numero_visitante = null;
    private $nombres = null;
    private $apellidos = null;
    private $sexo = null;
    private $fecha_nacimiento = null;
    private $telefono = null;
    private $telefono_celular = null;
    private $direccion = null;
    private $email = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdusuario($idusuario){
        $this->idusuario = $idusuario;
    }
    public function getIdusuario(){
        return $this->idusuario;
    }
    public function setIdgrupo($idgrupo){
        $this->idgrupo = $idgrupo;
    }
    public function getIdgrupo(){
        return $this->idgrupo;
    }
    public function setIdzonadetiempo($idzonadetiempo){
        $this->idzonadetiempo = $idzonadetiempo;
    }
    public function getIdzonadetiempo(){
        return $this->idzonadetiempo;
    }
    public function setIddepartamento($iddepartamento){
        $this->iddepartamento = $iddepartamento;
    }
    public function getIddepartamento(){
        return $this->iddepartamento;
    }
    public function setNumero($numero){
        $this->numero = $numero;
    }
    public function getNumero(){
        return $this->numero;
    }
    public function setTipo_tarjeta($tipo_tarjeta){
        $this->tipo_tarjeta = $tipo_tarjeta;
    }
    public function getTipo_tarjeta(){
        return $this->tipo_tarjeta;
    }
    public function setCodigo_pin($codigo_pin){
        $this->codigo_pin = $codigo_pin;
    }
    public function getCodigo_pin(){
        return $this->codigo_pin;
    }
    public function setCodigo_id($codigo_id){
        $this->codigo_id = $codigo_id;
    }
    public function getCodigo_id(){
        return $this->codigo_id;
    }
    public function setCodigo_sitio($codigo_sitio){
        $this->codigo_sitio = $codigo_sitio;
    }
    public function getCodigo_sitio(){
        return $this->codigo_sitio;
    }
    public function setCodigo_tarjeta($codigo_tarjeta){
        $this->codigo_tarjeta = $codigo_tarjeta;
    }
    public function getCodigo_tarjeta(){
        return $this->codigo_tarjeta;
    }
    public function setTarjetayopin($tarjetayopin){
        $this->tarjetayopin = $tarjetayopin;
    }
    public function getTarjetayopin(){
        return $this->tarjetayopin;
    }
    public function setTienefechalimite($tienefechalimite){
        $this->tienefechalimite = $tienefechalimite;
    }
    public function getTienefechalimite(){
        return $this->tienefechalimite;
    }
    public function setFechahasta($fechahasta){
        $this->fechahasta = $fechahasta;
    }
    public function getFechahasta(){
        return $this->fechahasta;
    }
    public function setRondasguardias($rondasguardias){
        $this->rondasguardias = $rondasguardias;
    }
    public function getRondasguardias(){
        return $this->rondasguardias;
    }
    public function setSkipfpcheck($skipfpcheck){
        $this->skipfpcheck = $skipfpcheck;
    }
    public function getSkipfpcheck(){
        return $this->skipfpcheck;
    }
    public function setModificable($modificable){
        $this->modificable = $modificable;
    }
    public function getModificable(){
        return $this->modificable;
    }
    public function setNivel($nivel){
        $this->nivel = $nivel;
    }
    public function getNivel(){
        return $this->nivel;
    }
    public function setAntipassback($antipassback){
        $this->antipassback = $antipassback;
    }
    public function getAntipassback(){
        return $this->antipassback;
    }
    public function setCedula($cedula){
        $this->cedula = $cedula;
    }
    public function getCedula(){
        return $this->cedula;
    }
    public function setNumero_visitante($numero_visitante){
        $this->numero_visitante = $numero_visitante;
    }
    public function getNumero_visitante(){
        return $this->numero_visitante;
    }
    public function setNombres($nombres){
        $this->nombres = $nombres;
    }
    public function getNombres(){
        return $this->nombres;
    }
    public function setApellidos($apellidos){
        $this->apellidos = $apellidos;
    }
    public function getApellidos(){
        return $this->apellidos;
    }
    public function setSexo($sexo){
        $this->sexo = $sexo;
    }
    public function getSexo(){
        return $this->sexo;
    }
    public function setFecha_nacimiento($fecha_nacimiento){
        $this->fecha_nacimiento = $fecha_nacimiento;
    }
    public function getFecha_nacimiento(){
        return $this->fecha_nacimiento;
    }
    public function setTelefono($telefono){
        $this->telefono = $telefono;
    }
    public function getTelefono(){
        return $this->telefono;
    }
    public function setTelefono_celular($telefono_celular){
        $this->telefono_celular = $telefono_celular;
    }
    public function getTelefono_celular(){
        return $this->telefono_celular;
    }
    public function setDireccion($direccion){
        $this->direccion = $direccion;
    }
    public function getDireccion(){
        return $this->direccion;
    }
    public function setEmail($email){
        $this->email = $email;
    }
    public function getEmail(){
        return $this->email;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idusuario !== null) && (trim($this->idusuario)!=='') ){
            $campos .= "idusuario,";
            $valores .= "'".$this->idusuario."',";
        }
        if(($this->idgrupo !== null) && (trim($this->idgrupo)!=='') ){
            $campos .= "idgrupo,";
            $valores .= "'".$this->idgrupo."',";
        }
        if(($this->idzonadetiempo !== null) && (trim($this->idzonadetiempo)!=='') ){
            $campos .= "idzonadetiempo,";
            $valores .= "'".$this->idzonadetiempo."',";
        }
        if(($this->iddepartamento !== null) && (trim($this->iddepartamento)!=='') ){
            $campos .= "iddepartamento,";
            $valores .= "'".$this->iddepartamento."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $campos .= "numero,";
            $valores .= "'".$this->numero."',";
        }
        if(($this->tipo_tarjeta !== null) && (trim($this->tipo_tarjeta)!=='') ){
            $campos .= "tipo_tarjeta,";
            $valores .= "'".$this->tipo_tarjeta."',";
        }
        if(($this->codigo_pin !== null) && (trim($this->codigo_pin)!=='') ){
            $campos .= "codigo_pin,";
            $valores .= "'".$this->codigo_pin."',";
        }
        if(($this->codigo_id !== null) && (trim($this->codigo_id)!=='') ){
            $campos .= "codigo_id,";
            $valores .= "'".$this->codigo_id."',";
        }
        if(($this->codigo_sitio !== null) && (trim($this->codigo_sitio)!=='') ){
            $campos .= "codigo_sitio,";
            $valores .= "'".$this->codigo_sitio."',";
        }
        if(($this->codigo_tarjeta !== null) && (trim($this->codigo_tarjeta)!=='') ){
            $campos .= "codigo_tarjeta,";
            $valores .= "'".$this->codigo_tarjeta."',";
        }
        if(($this->tarjetayopin !== null) && (trim($this->tarjetayopin)!=='') ){
            $campos .= "tarjetayopin,";
            $valores .= "'".$this->tarjetayopin."',";
        }
        if(($this->tienefechalimite !== null) && (trim($this->tienefechalimite)!=='') ){
            $campos .= "tienefechalimite,";
            $valores .= "'".$this->tienefechalimite."',";
        }
        if(($this->fechahasta !== null) && (trim($this->fechahasta)!=='') ){
            $campos .= "fechahasta,";
            $valores .= "'".$this->fechahasta."',";
        }
        if(($this->rondasguardias !== null) && (trim($this->rondasguardias)!=='') ){
            $campos .= "rondasguardias,";
            $valores .= "'".$this->rondasguardias."',";
        }
        if(($this->skipfpcheck !== null) && (trim($this->skipfpcheck)!=='') ){
            $campos .= "skipfpcheck,";
            $valores .= "'".$this->skipfpcheck."',";
        }
        if(($this->modificable !== null) && (trim($this->modificable)!=='') ){
            $campos .= "modificable,";
            $valores .= "'".$this->modificable."',";
        }
        if(($this->nivel !== null) && (trim($this->nivel)!=='') ){
            $campos .= "nivel,";
            $valores .= "'".$this->nivel."',";
        }
        if(($this->antipassback !== null) && (trim($this->antipassback)!=='') ){
            $campos .= "antipassback,";
            $valores .= "'".$this->antipassback."',";
        }
        if(($this->cedula !== null) && (trim($this->cedula)!=='') ){
            $campos .= "cedula,";
            $valores .= "'".$this->cedula."',";
        }
        if(($this->numero_visitante !== null) && (trim($this->numero_visitante)!=='') ){
            $campos .= "numero_visitante,";
            $valores .= "'".$this->numero_visitante."',";
        }
        if(($this->nombres !== null) && (trim($this->nombres)!=='') ){
            $campos .= "nombres,";
            $valores .= "'".$this->nombres."',";
        }
        if(($this->apellidos !== null) && (trim($this->apellidos)!=='') ){
            $campos .= "apellidos,";
            $valores .= "'".$this->apellidos."',";
        }
        if(($this->sexo !== null) && (trim($this->sexo)!=='') ){
            $campos .= "sexo,";
            $valores .= "'".$this->sexo."',";
        }
        if(($this->fecha_nacimiento !== null) && (trim($this->fecha_nacimiento)!=='') ){
            $campos .= "fecha_nacimiento,";
            $valores .= "'".$this->fecha_nacimiento."',";
        }
        if(($this->telefono !== null) && (trim($this->telefono)!=='') ){
            $campos .= "telefono,";
            $valores .= "'".$this->telefono."',";
        }
        if(($this->telefono_celular !== null) && (trim($this->telefono_celular)!=='') ){
            $campos .= "telefono_celular,";
            $valores .= "'".$this->telefono_celular."',";
        }
        if(($this->direccion !== null) && (trim($this->direccion)!=='') ){
            $campos .= "direccion,";
            $valores .= "'".$this->direccion."',";
        }
        if(($this->email !== null) && (trim($this->email)!=='') ){
            $campos .= "email,";
            $valores .= "'".$this->email."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_usuario $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_usuario SET ";
        
        if(($this->idgrupo !== null) && (trim($this->idgrupo)!=='') ){
            $sql .= "idgrupo = '".$this->idgrupo."',";
        }
        if(($this->idzonadetiempo !== null) && (trim($this->idzonadetiempo)!=='') ){
            $sql .= "idzonadetiempo = '".$this->idzonadetiempo."',";
        }
        if(($this->iddepartamento !== null) && (trim($this->iddepartamento)!=='') ){
            $sql .= "iddepartamento = '".$this->iddepartamento."',";
        }
        else{
            $sql .= "iddepartamento = NULL,";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $sql .= "numero = '".$this->numero."',";
        }
        if(($this->tipo_tarjeta !== null) && (trim($this->tipo_tarjeta)!=='') ){
            $sql .= "tipo_tarjeta = '".$this->tipo_tarjeta."',";
        }
        if(($this->codigo_pin !== null) && (trim($this->codigo_pin)!=='') ){
            $sql .= "codigo_pin = '".$this->codigo_pin."',";
        }
        else{
            $sql .= "codigo_pin = NULL,";
        }
        if(($this->codigo_id !== null) && (trim($this->codigo_id)!=='') ){
            $sql .= "codigo_id = '".$this->codigo_id."',";
        }
        if(($this->codigo_sitio !== null) && (trim($this->codigo_sitio)!=='') ){
            $sql .= "codigo_sitio = '".$this->codigo_sitio."',";
        }
        if(($this->codigo_tarjeta !== null) && (trim($this->codigo_tarjeta)!=='') ){
            $sql .= "codigo_tarjeta = '".$this->codigo_tarjeta."',";
        }
        if(($this->tarjetayopin !== null) && (trim($this->tarjetayopin)!=='') ){
            $sql .= "tarjetayopin = '".$this->tarjetayopin."',";
        }
        if(($this->tienefechalimite !== null) && (trim($this->tienefechalimite)!=='') ){
            $sql .= "tienefechalimite = '".$this->tienefechalimite."',";
        }
        if(($this->fechahasta !== null) && (trim($this->fechahasta)!=='') ){
            $sql .= "fechahasta = '".$this->fechahasta."',";
        }
        else{
            $sql .= "fechahasta = NULL,";
        }
        if(($this->rondasguardias !== null) && (trim($this->rondasguardias)!=='') ){
            $sql .= "rondasguardias = '".$this->rondasguardias."',";
        }
        if(($this->skipfpcheck !== null) && (trim($this->skipfpcheck)!=='') ){
            $sql .= "skipfpcheck = '".$this->skipfpcheck."',";
        }
        if(($this->modificable !== null) && (trim($this->modificable)!=='') ){
            $sql .= "modificable = '".$this->modificable."',";
        }
        if(($this->nivel !== null) && (trim($this->nivel)!=='') ){
            $sql .= "nivel = '".$this->nivel."',";
        }
        if(($this->antipassback !== null) && (trim($this->antipassback)!=='') ){
            $sql .= "antipassback = '".$this->antipassback."',";
        }
        if(($this->cedula !== null) && (trim($this->cedula)!=='') ){
            $sql .= "cedula = '".$this->cedula."',";
        }
        else{
            $sql .= "cedula = NULL,";
        }
        if(($this->numero_visitante !== null) && (trim($this->numero_visitante)!=='') ){
            $sql .= "numero_visitante = '".$this->numero_visitante."',";
        }
        else{
            $sql .= "numero_visitante = NULL,";
        }
        if(($this->nombres !== null) && (trim($this->nombres)!=='') ){
            $sql .= "nombres = '".$this->nombres."',";
        }
        else{
            $sql .= "nombres = NULL,";
        }
        if(($this->apellidos !== null) && (trim($this->apellidos)!=='') ){
            $sql .= "apellidos = '".$this->apellidos."',";
        }
        else{
            $sql .= "apellidos = NULL,";
        }
        if(($this->sexo !== null) && (trim($this->sexo)!=='') ){
            $sql .= "sexo = '".$this->sexo."',";
        }
        else{
            $sql .= "sexo = NULL,";
        }
        if(($this->fecha_nacimiento !== null) && (trim($this->fecha_nacimiento)!=='') ){
            $sql .= "fecha_nacimiento = '".$this->fecha_nacimiento."',";
        }
        else{
            $sql .= "fecha_nacimiento = NULL,";
        }
        if(($this->telefono !== null) && (trim($this->telefono)!=='') ){
            $sql .= "telefono = '".$this->telefono."',";
        }
        else{
            $sql .= "telefono = NULL,";
        }
        if(($this->telefono_celular !== null) && (trim($this->telefono_celular)!=='') ){
            $sql .= "telefono_celular = '".$this->telefono_celular."',";
        }
        else{
            $sql .= "telefono_celular = NULL,";
        }
        if(($this->direccion !== null) && (trim($this->direccion)!=='') ){
            $sql .= "direccion = '".$this->direccion."',";
        }
        else{
            $sql .= "direccion = NULL,";
        }
        if(($this->email !== null) && (trim($this->email)!=='') ){
            $sql .= "email = '".$this->email."',";
        }
        else{
            $sql .= "email = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idusuario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_usuario  WHERE idusuario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlUsuarios
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlUsuarios(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idusuarioBuscador"]){
            $in = null;
            foreach ($_REQUEST["idusuarioBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idusuario in (".$in.")";
        }
        
        $sql = "select /*start*/ idusuario,idgrupo,idzonadetiempo,iddepartamento,numero,tipo_tarjeta,codigo_pin,codigo_id,codigo_sitio,codigo_tarjeta,tarjetayopin,tienefechalimite,fechahasta,rondasguardias,skipfpcheck,modificable,nivel,antipassback,cedula,numero_visitante,nombres,apellidos,sexo,fecha_nacimiento,telefono,telefono_celular,direccion,email /*end*/ from ctrl_usuario ".$arg." order by numero,tipo_tarjeta";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlUsuario
     * @param int $idusuario Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlUsuario($idusuario){
        $sql = "SELECT idusuario,idgrupo,idzonadetiempo,iddepartamento,numero,tipo_tarjeta,codigo_pin,codigo_id,codigo_sitio,codigo_tarjeta,tarjetayopin,tienefechalimite,fechahasta,rondasguardias,skipfpcheck,modificable,nivel,antipassback,cedula,numero_visitante,nombres,apellidos,sexo,fecha_nacimiento,telefono,telefono_celular,direccion,email FROM ctrl_usuario WHERE idusuario = '".$idusuario."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>