var loadingMsg = '<img src="../images/loading51.gif" alt="" width="16" />';

$(function() {
    //------------------------------------------- SET UI OBJS ----------------------------------------------//
    $( "input:button, input:submit,button" ).button();
    $('#tabs').tabs();
    $('#radioSexo, #radio_tipo_tarjeta').buttonset();
    
    //--- IU DatePicker jQuery Calendar Plugin ---//
    $.datepicker.setDefaults( $.datepicker.regional['es'] );                
    $('#fecha_nacimiento').datepicker({
        inline: true,
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        maxDate:'d m y'
    }).attr('readonly',true).css({
        background: '#fff' ,
        border: '1px solid #d5d5d5',
        '-moz-border-radius': '4px',
        '-webkit-border-radius': '4px',
        'border-radius': '4px'
    });    
    $('#fechahasta').datepicker({
        inline: true,
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        minDate:'d m y'
    }).attr('readonly',true).css({
        background: '#fff' ,
        border: '1px solid #d5d5d5',
        '-moz-border-radius': '4px',
        '-webkit-border-radius': '4px',
        'border-radius': '4px'
    });
    //---------------------------------------------MENSAJE----------------------------------------------------------//
    $('#divmensaje').live({
        click: function() {
            $(this).fadeOut(500);
        }
    });
    //-------------------------------------------OPCION UNSET FECHA LIMITE------------------------------------------//
    $('#tienefechalimite').live({
        click: function() {
            if ($(this).attr('checked')==false){
                $('#fechahasta').val('');
            }
        }
    });

    //-------------------------------------------OBJS SEGUN TIPO TARJETA--------------------------------------------//
    setTipoTarjeta();
    $('.tipo_tarjeta').live({
        click: function(){
            setTipoTarjeta();
            carnetChk();
        }
    });
    //-------------------------------------------AJAX CEDULA/Nro Visitante-------------------------------------------//
    carnetChk();
    $('#cedula, #numero_visitante').live({
        change:function(){            
            carnetChk();
        }
    });
});
function setTipoTarjeta(){
    if ($('#tipo_tarjeta_empleado').is(':checked')){
        $('.visitante').hide();
        $('.empleado').show();
    }
    if ($('#tipo_tarjeta_visitante').is(':checked')){                    
        $('.empleado').hide();
        $('.visitante').show();
    }            
}

function carnetChk(){    
    
    var tipo_tarjeta = '';
    var str = '';
    if ($('#tipo_tarjeta_empleado').is(':checked')){
        tipo_tarjeta = 'empleado';
    }
    if ($('#tipo_tarjeta_visitante').is(':checked')){                    
        tipo_tarjeta = 'visitante';
    } 
    $.ajax({
        beforeSend: function(){                    
            $('#hasCarnet').html(loadingMsg);
        },
        type: 'POST',
        dataType:'json',
        url: 'ctrl_usuario.Op.php',
        data: 'tipo_tarjeta='+tipo_tarjeta+'&cedula='+$('#cedula').val()+'&numero_visitante='+$('#numero_visitante').val()+'&accion=carnetChk',
        success: function(data){
            $('#hasCarnet').html('');
            if (data.hasCarnet=='1'){
                if (tipo_tarjeta == 'empleado'){
                    str += '<img id="foto" class="ui-corner-all" src="../../carempleado/images/fotos/'+data.archivo_foto+'" alt="">';
                }              
                str += '<img id="carnet" src="../images/Carnet-32.png" alt="Carnet Asignado" title="Carnet Asignado" >';               
                $('#hasCarnet').html(str);
            }
            return true;    
        }
    });
}