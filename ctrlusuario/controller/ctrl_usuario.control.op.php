<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLUSR"); //Categoria del modulo
require_once "ctrl_usuario.control.php";// Class CONTROL ControlCtrlUsuario()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlUsuario extends ControlCtrlUsuario{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlUsuario(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();

        if (!$_REQUEST["tienefechalimite"]){
            $_REQUEST["tienefechalimite"] = "0";
        }
        if (!$_REQUEST["modificable"]){
            $_REQUEST["modificable"] = "0";
        }
        if (!$_REQUEST["rondasguardias"]){
            $_REQUEST["rondasguardias"] = "0";
        }
        if (!$_REQUEST["skipfpcheck"]){
            $_REQUEST["skipfpcheck"] = "0";
        }
        if (!$_REQUEST["antipassback"]){
            $_REQUEST["antipassback"] = "0";
        }

        if ($row["tipo_tarjeta"]=="empleado"){
            $row["numero_visitante"] = "";
        }
        if ($row["tipo_tarjeta"]=="visitante"){
            $row["cedula"]           = "";
            $row["apellidos"]        = "";
            $row["iddepartamento"]   = "";
            $row["sexo"]             = "";
            $row["fecha_nacimiento"] = "";
            $row["telefono"]         = "";
            $row["telefono_celular"] = "";
            $row["email"]            = "";
            $row["direccion"]        = "";
        }
        
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;
        
        //------------------ VALIDACION CODIGOS TARJETA -----------------//

        // Concatenacion hexadecimal de codigo sitio y codigo tarjeta para generar el cogido identificador
        $codigo_id = sprintf("%02X",$_REQUEST["codigo_sitio"]).sprintf("%02X",$_REQUEST["codigo_tarjeta"]);
        // Comparacion de codigo identificador generado, convertido de hexadecimal a decimal
        if (sprintf('%010d',hexdec($codigo_id)) != sprintf('%010d',$_REQUEST["codigo_id"])){
            $this->mensaje = "Existe un error de codificaci&oacute;n en la tarjeta.";
            return false;
        }
        $_REQUEST["codigo_id"]      = sprintf('%010d',$_REQUEST["codigo_id"]);
        $_REQUEST["codigo_sitio"]   = sprintf('%05d',$_REQUEST["codigo_sitio"]);
        $_REQUEST["codigo_tarjeta"] = sprintf('%05d',$_REQUEST["codigo_tarjeta"]);

        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlUsuario()) return false;

        //------------------ Sincronizar  -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar()){
            $this->mensaje = $sinc->mensaje;
            return false;
        }
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlUsuario(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        
        // Unset usuario en las controladoras
        $obj = new CtrlUsuarios();
        $data = $obj->getCtrlUsuario($_REQUEST["idusuario"]);
        $_REQUEST["idgrupo"]          = $data->idgrupo;
        $_REQUEST["numero"]           = $data->numero;
        $_REQUEST["codigo_pin"]       = "";         
        $_REQUEST["codigo_tarjeta"]   = "0"; 
        $_REQUEST["codigo_sitio"]     = "0"; 
        $_REQUEST["tarjetayopin"]     = "0";
        $_REQUEST["tienefechalimite"] = "0";
        $_REQUEST["modificable"]      = "0";
        $_REQUEST["rondasguardias"]   = "0";
        $_REQUEST["skipfpcheck"]      = "0";
        $_REQUEST["fechahasta"]       = "0";
        $_REQUEST["idzonadetiempo"]   = "0";
        $_REQUEST["nivel"]            = "0";
        $_REQUEST["antipassback"]     = "0";

        //------------------ Sincronizar  -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar()){
            $this->mensaje = $sinc->mensaje;
            return false;
        }
        
        
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlUsuario()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Usuario / Trajeta");
        $this->setCampos("numero","N&uacute;mero de usuario de acceso");
        $this->setCampos("idgrupo","Grupo de Puertas");
        $this->setCampos("idzonadetiempo","Horarios de Trabajo");
        $this->setCampos("codigo_pin","C&oacute;digo Pin");
        $this->setCampos("codigo_id","C&oacute;digo Identificador");
        $this->setCampos("codigo_sitio","C&oacute;digo Sitio de Tarjeta");
        $this->setCampos("codigo_tarjeta","C&oacute;digo Tarjeta");
        $this->setCampos("cedula","C&eacute;dula");
        $this->setCampos("nombres","Nombre");
        $this->setCampos("apellidos","Apellido");
        $this->setCampos("iddepartamento","Departamento");
        $this->setCampos("sexo","Sexo");
        $this->setCampos("fecha_nacimiento","Fecha de Nacimiento");
        $this->setCampos("telefono","Tel&eacute;fono");
        $this->setCampos("telefono_celular","Tel&eacute;fono Celular");
        $this->setCampos("direccion","Direcci&oacute;n");
        $this->setCampos("email","eMail");
        $this->setCampos("fechahasta","Fecha Limite de Acceso");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        
        $isRequired["empleado"] = false;
        $isRequired["visitante"] = false;
        if ($_REQUEST["tipo_tarjeta"]=="empleado"){
            $isRequired["empleado"]=true;
        }
        if ($_REQUEST["tipo_tarjeta"]=="visitante"){
            $isRequired["visitante"]=true;
        }
        
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"numero","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$isRequired["visitante"],"datoName"=>"numero_visitante","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$isRequired["empleado"],"datoName"=>"cedula","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombres","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$isRequired["empleado"],"datoName"=>"apellidos","tipoDato"=>"esAlfaNumericoConEspacios");        
        
        //------------------------------------------------------------------------------------------------------------//        
        $datos[] = array("isRequired"=>false,"datoName"=>"iddepartamento","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"sexo","tipoDato"=>"esAlfaNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"fecha_nacimiento","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"telefono","tipoDato"=>"esValidoTlf");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"telefono_celular","tipoDato"=>"esValidoTlf");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"email","tipoDato"=>"esValidoEmail");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"direccion","tipoDato"=>"esAlfaNumericoEspecial");


        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"tarjetayopin","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"codigo_id","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"codigo_sitio","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"codigo_tarjeta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $chkCodigoPin = false;
        if ($_REQUEST["tarjetayopin"]=="2" || $_REQUEST["tarjetayopin"]=="3"){
            $chkCodigoPin = true;
        }
        $datos[] = array("isRequired"=>$chkCodigoPin,"datoName"=>"codigo_pin","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idgrupo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idzonadetiempo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        if ($_REQUEST["tienefechalimite"]=="1"){
            $datos[] = array("isRequired"=>true,"datoName"=>"fechahasta","tipoDato"=>"");
        }
        //------------------------------------------------------------------------------------------------------------//
        
        if (!$this->validarDatos($datos))return false;
        
        return true;
    }
    
}
?>