<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../../ctrlsincronizar/controller/ctrl_sincronizar.control.php";// Class CONTROL ControlCtrlSincronizar()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlSincronizar extends ControlCtrlSincronizar{
    
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlSincronizar(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        //echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
        $this->setAccion("agregar");
        $_REQUEST["item"]         = "Usuario";           // Etiqueta de registro en el proceso de sincronizacion para zona de tiempo
        $_REQUEST["accion"]       = "Escribir Usuario";  // Etiqueta de crear zona de tiempo
        $_REQUEST["codigoaccion"] = "4";                 // Codigo de crear zona de tiempo en la controladora
        $_REQUEST["prioridad"]    = "100";                // Prioridad del proceso de sincronizacion para zona de tiempo
        
        // Consulta de todas las controladoras a sincronizar los horarios
        $data = CtrlSincronizars::getControladoras();
        $numeroGrupo = CtrlSincronizars::getNumeroGrupo($_REQUEST["idgrupo"]);
        if (count($data) > 0){
                        
            foreach ($data as $key => $row){

                // $_REQUEST[""]==""?"0":$_REQUEST[""]; //

                $_REQUEST["nodo"]    = $row->nodo;
                $_REQUEST["ipv4"]    = $row->ipv4;
                $_REQUEST["puerto"]  = $row->puerto;
                $_REQUEST["param1"]  = $_REQUEST["numero"];         // Numero de usuario de acceso
                $_REQUEST["param2"]  = $_REQUEST["codigo_pin"];     // Codigo Pin de acceso
                $_REQUEST["param3"]  = $_REQUEST["codigo_tarjeta"]; // Codigo de tarjeta
                $_REQUEST["param4"]  = $_REQUEST["codigo_sitio"];   // Codigo de sitio
                $_REQUEST["param5"]  = $_REQUEST["tarjetayopin"];   // Opciones tarjeta y/o pin
                $_REQUEST["param6"]  = $_REQUEST["tienefechalimite"]==""?"0":$_REQUEST["tienefechalimite"]; // Si tiene fecha limite
                $_REQUEST["param7"]  = $_REQUEST["modificable"];    // Modificable
                $_REQUEST["param8"]  = $_REQUEST["rondasguardias"]; // Rodas de Guardias
                $_REQUEST["param9"]  = $_REQUEST["skipfpcheck"];    // 
                $_REQUEST["param10"] = $numeroGrupo;                // numero de grupo de puertas
                $_REQUEST["param11"] = $_REQUEST["fechahasta"];     // fecha limite
                $_REQUEST["param12"] = $_REQUEST["idzonadetiempo"]; // numero de zona de tiempo
                $_REQUEST["param13"] = $_REQUEST["nivel"];          // Nivel de acceso
                $_REQUEST["param14"] = $_REQUEST["antipassback"];   // Anti-PassBack

                //------------------ Metodo Set  -----------------//
                if(!$this->setCtrlSincronizar()) return false;
            }           
        }
        return true;
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlSincronizar(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlSincronizar()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Sincronizacion");
        
        $this->setCampos("item","Item");
        $this->setCampos("accion","Accion");
        $this->setCampos("codigoaccion","Codigoaccion");
        $this->setCampos("nodo","Nodo");
        $this->setCampos("ipv4","Ipv4");
        $this->setCampos("puerto","Puerto");
        $this->setCampos("prioridad","Prioridad");
        $this->setCampos("param1","Param1");
        $this->setCampos("param2","Param2");
        $this->setCampos("param3","Param3");
        $this->setCampos("param4","Param4");
        $this->setCampos("param5","Param5");
        $this->setCampos("param6","Param6");
        $this->setCampos("param7","Param7");
        $this->setCampos("param8","Param9");
        $this->setCampos("param9","Param9");
        $this->setCampos("param10","Param10");
        $this->setCampos("param11","Param11");
        $this->setCampos("param12","Param12");
        $this->setCampos("param13","Param13");
        $this->setCampos("param14","Param14");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){

        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"item","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"accion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"codigoaccion","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nodo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"ipv4","tipoDato"=>"esValidaIP");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"puerto","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"prioridad","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param1","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        //$datos[] = array("isRequired"=>true,"datoName"=>"param2","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param3","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param4","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param5","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param6","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param7","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param8","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param9","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param10","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        //$datos[] = array("isRequired"=>true,"datoName"=>"param11","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param12","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param13","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"param14","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;

        return true;
    }
}
?>