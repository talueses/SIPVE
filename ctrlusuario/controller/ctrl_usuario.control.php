<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../model/ctrl_usuario.model.php"; // Class MODEL CtrlUsuario()
include_once "../../inicio/controller/controller.php";

/**
 * Description
 * @author David Concepcion
 */
class ControlCtrlUsuario extends Controller{
    /**
     * @var string mensaje de exito o error
     */
    var $mensaje = null;
    /**
     * @var string accion agregar, modificar o eliminar dato
     */
    var $accion  = null;
    /**
     * @var array nombre de campos campos
     */
    var $campos  = null;
    /**
     * @var string nombre de la entidad 
     */
    var $entidad  = null;

    /**
     * Establece la acción
     * @param string $accion Acción
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }

    /**
     * @return string Devuelve la accion establecida
     */
    public function getAccion(){
         return $this->accion;
    }

    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }

    /**
     * Establece la Entidad
     * @param string $entidad Entidad
     */
    public function setEntidad($entidad){
         $this->entidad = $entidad;
    }

    /**
     * @return string Devuelve la Entidad establecida
     */
    public function getEntidad(){
         return $this->entidad;
    }
    
    /**
     * Agregar o modificar un CtrlUsuario
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setCtrlUsuario(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;

        $obj = new CtrlUsuario();

        //--------------------- DATOS --------------------------//
        $obj->setIdusuario($_REQUEST["idusuario"]);
        
        $obj->setIdgrupo($_REQUEST["idgrupo"]);
        $obj->setIdzonadetiempo($_REQUEST["idzonadetiempo"]);
        $obj->setIddepartamento($_REQUEST["iddepartamento"]);
        $obj->setNumero($_REQUEST["numero"]);
        $obj->setTipo_tarjeta($_REQUEST["tipo_tarjeta"]);
        $obj->setCodigo_pin($_REQUEST["codigo_pin"]);
        $obj->setCodigo_id($_REQUEST["codigo_id"]);
        $obj->setCodigo_sitio($_REQUEST["codigo_sitio"]);
        $obj->setCodigo_tarjeta($_REQUEST["codigo_tarjeta"]);
        $obj->setTarjetayopin($_REQUEST["tarjetayopin"]);
        $obj->setTienefechalimite($_REQUEST["tienefechalimite"]);
        $obj->setFechahasta($_REQUEST["fechahasta"] ? self::formatoFecha($_REQUEST["fechahasta"]) : "");
        $obj->setRondasguardias($_REQUEST["rondasguardias"]);
        $obj->setSkipfpcheck($_REQUEST["skipfpcheck"]);
        $obj->setModificable($_REQUEST["modificable"]);
        $obj->setNivel($_REQUEST["nivel"]);
        $obj->setAntipassback($_REQUEST["antipassback"]);
        $obj->setCedula($_REQUEST["cedula"]);
        $obj->setNumero_visitante($_REQUEST["numero_visitante"]);
        $obj->setNombres($_REQUEST["nombres"]);
        $obj->setApellidos($_REQUEST["apellidos"]);
        $obj->setSexo($_REQUEST["sexo"]);
        $obj->setFecha_nacimiento($_REQUEST["fecha_nacimiento"] ? self::formatoFecha($_REQUEST["fecha_nacimiento"]) : "");
        $obj->setTelefono($_REQUEST["telefono"]);
        $obj->setTelefono_celular($_REQUEST["telefono_celular"]);
        $obj->setDireccion($_REQUEST["direccion"]);
        $obj->setEmail($_REQUEST["email"]);
        

        if ($this->getAccion()=="agregar"){
            $exito = $obj->insertarRegistro();
        }

        if ($this->getAccion()=="modificar"){
            $exito = $obj->modificarRegistro($obj->getIdusuario($_REQUEST["idusuario"]));
        }

        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            if ($this->getAccion()=="agregar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser creado: ".$obj->getMensaje();
            }
            if ($this->getAccion()=="modificar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser modificado: ".$obj->getMensaje();
            }
            return false;
        }
        // ------------------------------- MENSAJE EXITO --------------------------------//
        if ($this->getAccion()=="agregar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; creado exitosamente...";
        }
        if ($this->getAccion()=="modificar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; modificado exitosamente...";
        }


        return true;
    }

    /**
     * === Eliminar CtrlUsuario ===
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarCtrlUsuario(){
        // -------------------- ELIMINAR LECTORA ----------------------//
        $obj = new CtrlUsuario();
        //--------------------- DATOS --------------------------//
        $obj->setIdusuario($_REQUEST["idusuario"]);

        $exito = $obj->eliminarRegistro($obj->getIdusuario($_REQUEST["idusuario"]));
        if(!$exito){
            $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser eliminado: ".$obj->getMensaje();
            return false;
        }

        $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; eliminado exitosamente...";
        return true;
    }

    
    
}
?>
