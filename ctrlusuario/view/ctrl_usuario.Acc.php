<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/ctrl_usuario.control.op.php";// Class CONTROLLER
$obj = new CtrlUsuarios();
$numExist = $obj->getNumerosUsuarios();
if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){    
    $data = $obj->getCtrlAccUsuario($_REQUEST["idusuario"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.textarea.maxlength.js"></script>
        <!-- IU DatePicker jQuery Calendar Plugin -->
        <script type="text/javascript" src="../../inicio/js/jquery.ui.datepicker-es.js"></script>
        <script type="text/javascript" src="../js/car_usuario.local.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 550px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }                        
            #divmensaje{
                width:99%;
                position: absolute;
                top: -10px;
                opacity:0.9;
                z-index: 9999;
                cursor: pointer;
            }
            div.divTextarea{
                width: 230px;
            }
            
            #radio_tipo_tarjeta img{                
                margin: -2px -0px -2px -8px;
            }
            #hasCarnet{
                position: absolute;
                right: 20px;
            }
            img#foto{
                border: #000000 1px dashed;                
                position: absolute;
                right: 10px;
                width: 95px;
            }            
            img#carnet{
                position: absolute;
                left: -262px;
                top: -7px;
            }
        </style>
        <script type="text/javascript" language="javascript">            
                <?php
                if ($_REQUEST["accion"] == "visualizar"){
                    echo "$(function() {\n";
                    echo "    $( 'textarea' ).resizable({disabled: true});\n";
                    echo "    $('#radioSexo,.tipo_tarjeta').buttonset({disabled: true});\n";
                    echo "}\n";
                }
                ?>            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Usuario - Tarjeta</div>
                <br/>
                <div id="divmensaje"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="ctrl_usuario.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <div id="tabs">
                            <ul>
                                <li><a href="#datosBasicos">Datos B&aacute;sicos</a></li>
                                <li><a href="#datosAcceso">Datos Acceso</a></li>
                            </ul>
                            <div id="datosBasicos">
                                <table>
                                    <tr title="N&uacute;mero de usuario de acceso">
                                        <td width="100" align="right">N&uacute;mero:</td>
                                        <td width="450">
                                            <select name="numero" id="numero" onchange="" <?php echo $disabled;?> >
                                                
                                                <?php
                                                for ($i=1;$i<=5000;$i++){
                                                    $chkNum = false;
                                                    foreach ($numExist->numero as $numero){
                                                        if ($numero == $i){
                                                            $chkNum = true;
                                                        }
                                                    }
                                                    if (!$chkNum || $data->numero == $i){
                                                        echo '<option value="'.$i.'" '.ControlCtrlUsuario::busca_valor($data->numero, $i ).'>'.$i.'</option>';
                                                    }

                                                }
                                                ?>
                                            </select>
                                            <span id="hasCarnet"></span>
                                        </td>
                                    </tr>
                                    <tr title="Tipo">
                                        <td align="right">Tipo:</td>
                                        <td>
                                            <div id="radio_tipo_tarjeta">
                                                <input type="radio" id="tipo_tarjeta_empleado" class="tipo_tarjeta" name="tipo_tarjeta" value="empleado" <?php echo ($data->tipo_tarjeta=="empleado" || $data->idusuario=="")?"checked":"";?> <?php echo $disabled;?> /><label for="tipo_tarjeta_empleado"><img src="../images/User-32.png" alt="E" width="16" title="Empleado" />Empleado</label>
                                                <input type="radio" id="tipo_tarjeta_visitante" class="tipo_tarjeta" name="tipo_tarjeta" value="visitante" <?php echo ($data->tipo_tarjeta=="visitante" )?"checked":"";?> <?php echo $disabled;?> /><label for="tipo_tarjeta_visitante"><img src="../images/User-Gray-Go-32.png" alt="V" width="16" title="Visitante" />Visitante</label>
                                            </div>
                                            
                                        </td>
                                    </tr>
                                    <tr title="N&uacute;mero de tarjeta de visitante" class="visitante">
                                        <td align="right">N&deg; Visitante:</td>
                                        <td>
                                            <select name="numero_visitante" id="numero_visitante" <?php echo $disabled;?>>
                                                <?php                                                    
                                                for ($i=1;$i<=1000;$i++){
                                                    $chkNum = false;
                                                    foreach ($numExist->visitante as $numero_visitante){
                                                        if ($numero_visitante == $i){
                                                            $chkNum = true;
                                                        }
                                                    }
                                                    if (!$chkNum || $data->numero_visitante == $i){
                                                        echo '<option value="'.$i.'" '.Controller::busca_valor($data->numero_visitante, $i ).'>'.sprintf('%04d',$i).'</option>';
                                                    }
                                                }                                                    
                                                ?>
                                            </select>
                                        </td>
                                    </tr>                                    
                                    
                                    <tr title="C&eacute;dula" class="empleado">
                                        <td align="right">C&eacute;dula:</td>
                                        <td>
                                            <input type="text" name="cedula" id="cedula" maxlength="8" value="<?php echo $data->cedula;?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="Nombre" class="empleado visitante">
                                        <td align="right">Nombre:</td>
                                        <td>
                                            <input type="text" name="nombres" id="nombres" maxlength="50" value="<?php echo $data->nombres;?>" <?php echo $disabled;?>>
                                            
                                        </td>
                                    </tr>
                                    <tr title="Apellido" class="empleado">
                                        <td align="right">Apellido:</td>
                                        <td>
                                            <input type="text" name="apellidos" id="apellidos" maxlength="50" value="<?php echo $data->apellidos;?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="Departamento" class="empleado">
                                        <td align="right">Departamento:</td>
                                        <td>
                                            <select name="iddepartamento" id="iddepartamento" <?php echo $disabled;?> >
                                                <option value="">Seleccione</option>
                                                <?php echo ControlCtrlUsuario::make_combo("car_departamento","order by departamento_nombre", "iddepartamento", "departamento_nombre", $data->iddepartamento,false);?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr title="Sexo" class="empleado">
                                        <td align="right">Sexo:</td>
                                        <td>
                                            <div id="radioSexo">
                                                <input type="radio" name="sexo" id="radioSexo1" value="F" <?php echo ($data->sexo=="F")?"checked":""?> /><label for="radioSexo1">Femenino</label>
                                                <input type="radio" name="sexo" id="radioSexo2" value="M" <?php echo (!$data->sexo || $data->sexo=="M")?"checked":""?>/><label for="radioSexo2">Masculino</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr title="Fecha de Nacimiento" class="empleado">
                                        <td align="right">Fecha de Nacimiento:</td>
                                        <td>
                                            <input type="text" name="fecha_nacimiento" id="fecha_nacimiento" maxlength="10" value="<?php echo $data->fecha_nacimiento ? Controller::formatoFecha($data->fecha_nacimiento) : "";?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="Tel&eacute;fono" class="empleado">
                                        <td align="right">Tel&eacute;fono:</td>
                                        <td>
                                            <input type="text" name="telefono" id="telefono" maxlength="18" value="<?php echo $data->telefono;?>" <?php echo $disabled;?>>
                                            <small class="comment">Ejemplo: 212-1234567 &oacute; 0212-1234567</small>
                                        </td>
                                    </tr>
                                    <tr title="Tel&eacute;fono Celular" class="empleado">
                                        <td align="right">Tel&eacute;fono Celular:</td>
                                        <td>
                                            <input type="text" name="telefono_celular" id="telefono_celular" maxlength="18" value="<?php echo $data->telefono_celular;?>" <?php echo $disabled;?>>
                                            <small class="comment">Ejemplo: 426-1234567 &oacute; 0426-1234567</small>
                                        </td>
                                    </tr>
                                    <tr title="e-Mail" class="empleado">
                                        <td align="right">e-Mail:</td>
                                        <td>
                                            <input type="text" name="email" id="email" maxlength="45" value="<?php echo $data->email;?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="Direcci&oacute;n" class="empleado">
                                        <td align="right" valign="top">Direcci&oacute;n:</td>
                                        <td>
                                            <div class="divTextarea" >
                                                <textarea name="direccion" id="direccion" <?php echo $disabled;?> rows="2" cols="28" maxlength="250" <?php echo $disabled;?>><?php echo $data->direccion;?></textarea>
                                                <div class="charLeftDiv"  align="right"><input type="text" class="charLeft" id="charLeft_direccion" size="4" readonly > Caracteres Restantes</div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="datosAcceso">
                                <table>
                                    <tr title="Modo de acceso">
                                        <td align="right">Modo Acceso:</td>
                                        <td>
                                            <select name="tarjetayopin" id="tarjetayopin" <?php echo $disabled;?>>
                                                <option value="0" <?php echo ControlCtrlUsuario::busca_valor("0", $data->tarjetayopin)?>>Inv&aacute;lido (Bloqueado)</option>
                                                <option value="1" <?php echo $data->tarjetayopin==""?"selected":ControlCtrlUsuario::busca_valor("1", $data->tarjetayopin)?>>Tarjeta</option>
                                                <option value="2" <?php echo ControlCtrlUsuario::busca_valor("2", $data->tarjetayopin)?>>Tarjeta o C&oacute;digo Pin</option>
                                                <option value="3" <?php echo ControlCtrlUsuario::busca_valor("3", $data->tarjetayopin)?>>Tarjeta y C&oacute;digo Pin</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr title="C&oacute;digo Identificador">
                                        <td align="right">C&oacute;digo Identificador:</td>
                                        <td>
                                            <input type="text" name="codigo_id" id="codigo_id" maxlength="10" value="<?php echo $data->codigo_id;?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="C&oacute;digo Sitio de Tarjeta">
                                        <td align="right">C&oacute;digo Sitio:</td>
                                        <td>
                                            <input type="text" name="codigo_sitio" id="codigo_sitio" maxlength="5" value="<?php echo $data->codigo_sitio;?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="C&oacute;digo Tarjeta">
                                        <td align="right">C&oacute;digo Tarjeta:</td>
                                        <td>
                                            <input type="text" name="codigo_tarjeta" id="codigo_tarjeta" maxlength="5" value="<?php echo $data->codigo_tarjeta;?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="C&oacute;digo Pin">
                                        <td align="right">C&oacute;digo Pin:</td>
                                        <td>
                                            <input type="password" name="codigo_pin" id="codigo_pin" maxlength="4" value="<?php echo $data->codigo_pin;?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="Grupo de Puertas">
                                        <td align="right">Grupo de Puertas:</td>
                                        <td>
                                            <select name="idgrupo" id="idgrupo" <?php echo $disabled;?>>
                                                <option value="">Seleccione</option>
                                                <?php echo ControlCtrlUsuario::make_combo("ctrl_grupo cg, ctrl_grupo_puerta cgp, ctrl_lectora cl, ctrl_controladora cc","where cg.idgrupo = cgp.idgrupo and cgp.idpuerta = cl.idpuerta and cl.idcontroladora = cc.idcontroladora and cc.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') group by cg.idgrupo,cg.numero,cg.grupo order by cg.numero, cg.grupo", "cg.idgrupo", "cg.numero,cg.grupo", $data->idgrupo,false);?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr title="Horarios de Trabajo">
                                        <td align="right">Horarios de Trabajo:</td>
                                        <td>
                                            <select name="idzonadetiempo" id="idzonadetiempo" <?php echo $disabled;?>>
                                                <option value="">Seleccione</option>
                                                <?php echo ControlCtrlUsuario::make_combo("ctrl_zonadetiempo"," order by indice", "indice", "descripcion", $data->idzonadetiempo,false);?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr title="Nivel de Prioridad">
                                        <td align="right">Nivel:</td>
                                        <td>
                                            <select name="nivel" id="nivel" <?php echo $disabled;?>>
                                                <?php
                                                for ($i=0;$i<=63;$i++){

                                                    echo "<option value=\"".$i."\" ".ControlCtrlUsuario::busca_valor($data->nivel, $i).">".$i."</option>\n";

                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr title="Fecha Limite de Acceso">
                                        <td align="right">Fecha Limite:</td>
                                        <td>
                                            <input type="checkbox" name="tienefechalimite" id="tienefechalimite" <?php echo $data->tienefechalimite=="1"?"checked":""?> <?php echo $disabled;?> value="1">
                                            &nbsp;
                                            <input type="text" name="fechahasta" id="fechahasta" maxlength="10" size="16" value="<?php echo $data->fechahasta ? Controller::formatoFecha($data->fechahasta) : "";?>" <?php echo $disabled;?>>
                                        </td>
                                    </tr>
                                    <tr title="Modificable">
                                        <td align="right">Modificable:</td>
                                        <td>
                                            <input type="checkbox" name="modificable" id="modificable" <?php echo $data->modificable=="1"?"checked":""?> <?php echo $disabled;?> value="1">
                                        </td>
                                    </tr>
                                    <tr title="Guardias">
                                        <td align="right">Guardias:</td>
                                        <td>
                                            <input type="checkbox" name="rondasguardias" id="rondasguardias" <?php echo $data->rondasguardias=="1"?"checked":""?> <?php echo $disabled;?> value="1">
                                        </td>
                                    </tr>
                                    <tr title="Skip FP Check">
                                        <td align="right">Skip FP Check:</td>
                                        <td>
                                            <input type="checkbox" name="skipfpcheck" id="skipfpcheck" <?php echo ($data->skipfpcheck=="1" || $data->idusuario=="")?"checked":"";?> <?php echo $disabled;?> value="1">
                                        </td>
                                    </tr>
                                    <tr title="Anti-PassBack">
                                        <td align="right">Anti-PassBack:</td>
                                        <td>
                                            <input type="checkbox" name="antipassback" id="antipassback" <?php echo $data->antipassback=="1"?"checked":""?> <?php echo $disabled;?> value="1">
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idusuario" id="idusuario" value="<?php echo $data->idusuario ?>">
                        <?php
                        if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                            echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Usuario\" />";
                        }
                        ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>
