<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class VidTipoCamara {
    
    private $idtipo_camara = null;
    private $idmodelo_camara = null;
    private $nombre_tipo_camara = null;
    private $ptz = null;
    private $urlfoto = null;
    private $urlvideo = null;
    private $descripcion_tipo_camara = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdtipo_camara($idtipo_camara){
        $this->idtipo_camara = $idtipo_camara;
    }
    public function getIdtipo_camara(){
        return $this->idtipo_camara;
    }
    public function setIdmodelo_camara($idmodelo_camara){
        $this->idmodelo_camara = $idmodelo_camara;
    }
    public function getIdmodelo_camara(){
        return $this->idmodelo_camara;
    }
    public function setNombre_tipo_camara($nombre_tipo_camara){
        $this->nombre_tipo_camara = $nombre_tipo_camara;
    }
    public function getNombre_tipo_camara(){
        return $this->nombre_tipo_camara;
    }
    public function setPtz($ptz){
        $this->ptz = $ptz;
    }
    public function getPtz(){
        return $this->ptz;
    }
    public function setUrlfoto($urlfoto){
        $this->urlfoto = $urlfoto;
    }
    public function getUrlfoto(){
        return $this->urlfoto;
    }
    public function setUrlvideo($urlvideo){
        $this->urlvideo = $urlvideo;
    }
    public function getUrlvideo(){
        return $this->urlvideo;
    }
    public function setDescripcion_tipo_camara($descripcion_tipo_camara){
        $this->descripcion_tipo_camara = $descripcion_tipo_camara;
    }
    public function getDescripcion_tipo_camara(){
        return $this->descripcion_tipo_camara;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idtipo_camara !== null) && (trim($this->idtipo_camara)!=='') ){
            $campos .= "idtipo_camara,";
            $valores .= "'".$this->idtipo_camara."',";
        }
        if(($this->idmodelo_camara !== null) && (trim($this->idmodelo_camara)!=='') ){
            $campos .= "idmodelo_camara,";
            $valores .= "'".$this->idmodelo_camara."',";
        }
        if(($this->nombre_tipo_camara !== null) && (trim($this->nombre_tipo_camara)!=='') ){
            $campos .= "nombre_tipo_camara,";
            $valores .= "'".$this->nombre_tipo_camara."',";
        }
        if(($this->ptz !== null) && (trim($this->ptz)!=='') ){
            $campos .= "ptz,";
            $valores .= "'".$this->ptz."',";
        }
        if(($this->urlfoto !== null) && (trim($this->urlfoto)!=='') ){
            $campos .= "urlfoto,";
            $valores .= "'".$this->urlfoto."',";
        }
        if(($this->urlvideo !== null) && (trim($this->urlvideo)!=='') ){
            $campos .= "urlvideo,";
            $valores .= "'".$this->urlvideo."',";
        }
        if(($this->descripcion_tipo_camara !== null) && (trim($this->descripcion_tipo_camara)!=='') ){
            $campos .= "descripcion_tipo_camara,";
            $valores .= "'".$this->descripcion_tipo_camara."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_tipo_camara $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_tipo_camara SET ";
        
        if(($this->idmodelo_camara !== null) && (trim($this->idmodelo_camara)!=='') ){
            $sql .= "idmodelo_camara = '".$this->idmodelo_camara."',";
        }
        if(($this->nombre_tipo_camara !== null) && (trim($this->nombre_tipo_camara)!=='') ){
            $sql .= "nombre_tipo_camara = '".$this->nombre_tipo_camara."',";
        }
        if(($this->ptz !== null) && (trim($this->ptz)!=='') ){
            $sql .= "ptz = '".$this->ptz."',";
        }
        if(($this->urlfoto !== null) && (trim($this->urlfoto)!=='') ){
            $sql .= "urlfoto = '".$this->urlfoto."',";
        }
        if(($this->urlvideo !== null) && (trim($this->urlvideo)!=='') ){
            $sql .= "urlvideo = '".$this->urlvideo."',";
        }
        if(($this->descripcion_tipo_camara !== null) && (trim($this->descripcion_tipo_camara)!=='') ){
            $sql .= "descripcion_tipo_camara = '".$this->descripcion_tipo_camara."',";
        }
        else{
            $sql .= "descripcion_tipo_camara = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idtipo_camara = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_tipo_camara  WHERE idtipo_camara = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidTipoCamaras
     * @return object Devuelve un registro como objeto
     */
    function  getVidTipoCamaras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idtipo_camaraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idtipo_camaraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idtipo_camara in (".$in.")";
        }
        
        $sql = "select /*start*/ idtipo_camara,idmodelo_camara,nombre_tipo_camara,ptz,urlfoto,urlvideo,descripcion_tipo_camara /*end*/ from vid_tipo_camara ".$arg." order by nombre_tipo_camara,ptz";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidTipoCamara
     * @param int $idtipo_camara Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidTipoCamara($idtipo_camara){
        $sql = "SELECT idtipo_camara,idmodelo_camara,nombre_tipo_camara,ptz,urlfoto,urlvideo,descripcion_tipo_camara FROM vid_tipo_camara WHERE idtipo_camara = '".$idtipo_camara."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>