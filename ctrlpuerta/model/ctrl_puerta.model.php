<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'ctrl_puerta.tabla.php';

/**
 * Clase CtrlPuertas{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlPuertas extends CtrlPuerta{
    
    public static $mensaje = null;

        /**
     * Consulta de los numeros existentes de puertas
     * @return object Devuelve registros como objeto
     */
    function getNumerosPuertas() {
        $sql = "SELECT numero FROM ctrl_puerta";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de Puertas 
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlPuertasLectoras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idpuertaBuscador"]){
            $in = null;
            foreach ($_REQUEST["idpuertaBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where p.idpuerta in (".$in.")";
        }
        
        $sql = "select /*start*/
                       p.idpuerta,
                       p.numero,
                       p.puerta,
                       p.puerta_entrada,
                       (select idlectora from ctrl_lectora where p.idpuerta = idpuerta)as \"chkLetora\",
                       (
                        select count(cc.idcontroladora) 
                        from ctrl_lectora cl, ctrl_controladora cc 
                        where p.idpuerta = cl.idpuerta 
                          and cl.idcontroladora = cc.idcontroladora 
                          and cc.idsegmento in (
                                                select idsegmento 
                                                from segmento_usuario 
                                                where usuario = '".$_SESSION["usuario"]."'
                                               )
                       ) as \"chkSegmento\"
                       /*end*/
                from ctrl_puerta p 
                ".$arg."
                    order by cast(p.numero as :cast_int),p.puerta";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    
    public static function unsetPuertaEntrada(){
        $sql = "update ctrl_puerta set puerta_entrada = null where puerta_entrada='1'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return true;
    }    
}
?>
