<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlPuerta {
    
    private $idpuerta = null;
    private $numero = null;
    private $puerta = null;
    private $puerta_entrada = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdpuerta($idpuerta){
        $this->idpuerta = $idpuerta;
    }
    public function getIdpuerta(){
        return $this->idpuerta;
    }
    public function setNumero($numero){
        $this->numero = $numero;
    }
    public function getNumero(){
        return $this->numero;
    }
    public function setPuerta($puerta){
        $this->puerta = $puerta;
    }
    public function getPuerta(){
        return $this->puerta;
    }
    public function setPuerta_entrada($puerta_entrada){
        $this->puerta_entrada = $puerta_entrada;
    }
    public function getPuerta_entrada(){
        return $this->puerta_entrada;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idpuerta !== null) && (trim($this->idpuerta)!=='') ){
            $campos .= "idpuerta,";
            $valores .= "'".$this->idpuerta."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $campos .= "numero,";
            $valores .= "'".$this->numero."',";
        }
        if(($this->puerta !== null) && (trim($this->puerta)!=='') ){
            $campos .= "puerta,";
            $valores .= "'".$this->puerta."',";
        }
        if(($this->puerta_entrada !== null) && (trim($this->puerta_entrada)!=='') ){
            $campos .= "puerta_entrada,";
            $valores .= "'".$this->puerta_entrada."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_puerta $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_puerta SET ";
        
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $sql .= "numero = '".$this->numero."',";
        }
        if(($this->puerta !== null) && (trim($this->puerta)!=='') ){
            $sql .= "puerta = '".$this->puerta."',";
        }
        if(($this->puerta_entrada !== null) && (trim($this->puerta_entrada)!=='') ){
            $sql .= "puerta_entrada = '".$this->puerta_entrada."',";
        }
        else{
            $sql .= "puerta_entrada = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idpuerta = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_puerta  WHERE idpuerta = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlPuertas
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlPuertas(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idpuertaBuscador"]){
            $in = null;
            foreach ($_REQUEST["idpuertaBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idpuerta in (".$in.")";
        }
        
        $sql = "select /*start*/ idpuerta,numero,puerta,puerta_entrada /*end*/ from ctrl_puerta ".$arg." order by numero,puerta";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlPuerta
     * @param int $idpuerta Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlPuerta($idpuerta){
        $sql = "SELECT idpuerta,numero,puerta,puerta_entrada FROM ctrl_puerta WHERE idpuerta = '".$idpuerta."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>