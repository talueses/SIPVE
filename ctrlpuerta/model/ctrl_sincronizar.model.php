<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once '../../ctrlsincronizar/model/ctrl_sincronizar.tabla.php';

/**
 * Clase CtrlSincronizars{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlSincronizars extends CtrlSincronizar{

   /**
     * Consulta de Controladora de una puerta
     * @param int $idpuerta codigo de la puerta
     * @return object Devuelve un registro como objeto
     */
    static function getControladorasPuertas($idpuerta){
        $sql = "select  c.idcontroladora,
                        c.idtipo_controladora,
                        c.nodo,
                        c.ipv4,
                        c.puerto,
                        c.maximo_usuarios,
                        c.nombre,
                        c.descripcion,
                        c.ubicacion,
                        p.idpuerta,
                        p.numero as numero_puerta
                from ctrl_controladora c, ctrl_lectora l, ctrl_puerta p
                where c.idcontroladora = l.idcontroladora
                  and l.idpuerta = p.idpuerta
                  and p.idpuerta = '".$idpuerta."'
                    order by c.ipv4, c.nodo";
        $link = DbLink::getLink();
        $result = mysql_query($sql, $link);
        if (!$result) {
            $this->mensaje = mysql_error($link);
            return false;
        }
        while($row = mysql_fetch_assoc($result)){
            $ret[] = (object) $row;
        }
        return $ret;
    }
    /**
     * Consulta de Controladoras
     * @return object Devuelve un registro como objeto
     */
    static function getControladoras(){
        $sql = "select idcontroladora,idtipo_controladora,nodo,ipv4,puerto,maximo_usuarios,nombre,descripcion,ubicacion from ctrl_controladora order by ipv4, nodo";
        $link = DbLink::getLink();
        $result = mysql_query($sql, $link);
        if (!$result) {
            $this->mensaje = mysql_error($link);
            return false;
        }
        while($row = mysql_fetch_assoc($result)){
            $ret[] = (object) $row;
        }
        return $ret;
    }
    
}
?>
