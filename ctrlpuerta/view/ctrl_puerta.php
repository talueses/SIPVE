<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de horarios
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/ctrl_puerta.control.op.php";// Class CONTROLLER

$data = ControlOpCtrlPuerta::getPuertas($data);

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>CtrlPuertas</title>        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #tabla{
                float:left;
                width:30%;
                height:95%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }            
            div#buttonsOptions {
                width: 250px;
                padding: 9px;
                border:#c0c0c0 dashed 2px;
            }
            img#mainDoor{
                width: 16px;
                position: absolute;
                margin-left: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
            });
            function visualizar(fila){
                $('#idpuerta').val(fila.id);
                $('#accion').val('visualizar');
                $('#f1').attr('action','ctrl_puerta.Acc.php');
                $('#f1').submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    $('#'+fila.id).addClass('chequeada');
            }

            function omover(fila){                
                    if (fila.id===filaseleccionada.id ) return false;                                        
                    $('#'+fila.id).addClass('pasada');
            }
            
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;                    
                    $('#'+fila.id).removeClass('pasada');
            }
            function Accion(acc){
                $('#accion').val(acc);
                $('#f1').attr('action','ctrl_puerta.Acc.php');

                if (acc == "modificar" || acc == "eliminar" || acc == "enviarArchivo"){
                    if ($('#idpuerta').val()==""){
                        alert("Debe seleccionar un registro de la lista");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("\xbf Esta seguro que desea eliminar el registro ?")){
                            return false;
                        }
                        $('#f1').attr('action','ctrl_puerta.Op.php');
                    }
                }

                $('#f1').submit();
                return true;
            }
            function puertaAcc(op){
                
                if ($('#idpuerta').val() == "" && !$('#all').attr('checked')){
                    alert("Debe seleccionar un registro de la lista");
                    return false;
                }
                if ($('#idpuerta').val()!=""){
                    if ($('#'+$('#idpuerta').val()).attr('chkLetora')=="" && !$('#all').attr('checked')){
                        alert("La puerta seleccionada no tiene asignada una Lectora");
                        return false;
                    }
                }
                
                if ($('#all').attr('checked')==true && !confirm('\xbf Esta seguro de '+op+' todas las puertas ?')){
                    return false;                    
                }         
                $('#accion').val(op);
                $('#f1').attr('action','ctrl_puerta.Op.php');
                $('#f1').submit();
                return true;
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <div id="tabla" align="center">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                    echo Controller::$mensajePermisos;
                }else{
                    echo Buscador::getObjBuscador("idpuerta", "numero,puerta", "ctrl_puerta", "order by numero,puerta");
                    ?>
                    <table id="listado">
                            <tr>
                                <td id="titulo" colspan="4" align="center"><b>Puertas</b></td>
                            </tr>
                            <tr>
                                <th align="center" width="10%">N&deg;</th>
                                <th >N&uacute;mero</th>
                                <th>Puerta</th>
                                <th>&nbsp;</th>

                            </tr>
                            <?php
                            if (count($data) > 0){
                                foreach ($data as $key => $row){?>
                                    <tr id="<?php echo $row->idpuerta;?>" onclick="visualizar(this);oclic(this)" onmouseover="omover(this)" onmouseout="omout(this)" chkLetora="<?php echo $row->chkLetora ?>">
                                        <td align="center"><b><?php echo paginationSQL::$start+$key+1;?></b></td>
                                        <td align="center"><?php echo $row->numero;?></td>
                                        <td>
                                            <?php                                             
                                            echo $row->puerta;
                                            if ($row->puerta_entrada=="1"){
                                                ?><img src="../images/exchange-27.png" alt="" title="Puerta de Entrada Principal" id="mainDoor" /><?php
                                            }
                                            ?>
                                        </td>
                                        <?php                                        
                                        if ($row->chkLetora){
                                            $src = "Bullet-Green-16.png";
                                            $title = "Asignada a una Lectora";
                                        }
                                        if (!$row->chkLetora){
                                            $src = "Bullet-White-16.png";
                                            $title = "No Asignada a una Lectora";
                                        }
                                        ?>
                                        <td align="center" title="<?php echo $title;?>">                                            
                                            <img src="../images/<?php echo $src;?>" alt="" title="<?php echo $title;?>">
                                        </td>
                                    </tr>
                                    <?php                                    
                                }
                            }else{
                                ?>
                                <tr >
                                    <td colspan="4">
                                        <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                    </td>
                                </tr>

                                <?php
                            }
                            ?>

                    </table>
                    <?php echo paginationSQL::links();?>
                    <div id="botones" >
                        <center>
                            <form name="f1" id="f1" target="frm1" action="ctrl_puerta.Acc.php" method="POST">
                                <input type="hidden" name="idpuerta" id="idpuerta" value="">
                                <input type="hidden" name="accion" id="accion" value="">
                                <?php
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"agregar")){
                                    ?><input type="submit" value="Agregar" onclick="return Accion('agregar')" class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"modificar")){
                                    ?><input type="submit" value="Modificar" onclick="return Accion('modificar')" class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"eliminar")){
                                    ?><input type="button" value="Eliminar" onclick="return Accion('eliminar')" class="boton"><?php
                                }
                                ?>
                                <input type="submit" value="Ayuda" onclick="this.form.action='ctrl_puerta.Ayuda.php'" class="boton">
                                <br />&nbsp;<br />
                                <div id="buttonsOptions" class="ui-corner-all" >
                                    <?php
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"abrir")){
                                        ?>
                                         <button type="button" style="font-size: 10px;" onclick="puertaAcc('abrir')">
                                             <img src="../images/Unlock-32.png" alt="" style="margin-bottom: 5px;" />
                                            <br />
                                            Abrir Puertas
                                        </button>
                                        <?php
                                    }
                                    echo "&nbsp;&nbsp;";
                                    if (Controller::chkPermiso($_SESSION["usuario"],CATEGORIA, "cerrar")){
                                        ?>
                                        <button type="button" style="font-size: 10px;" onclick="puertaAcc('cerrar')">
                                            <img src="../images/Lock-32.png" alt=""  style="margin-bottom: 5px;" />
                                            <br />
                                            Cerrar Puertas
                                        </button>
                                        <?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"abrir") || Controller::chkPermiso($_SESSION["usuario"],CATEGORIA, "cerrar")){
                                        ?><br><input type="checkbox" name="all" id="all" value="true"  >Todas las Puertas<?php
                                    }
                                    ?>
                                </div>
                                <br /> &nbsp;
                            </form>
                        </center>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                <iframe name="frm1" id="frm1" frameborder="0" scrolling="no" style="width:100%; height:650px" src="ctrl_puerta.Acc.php?accion=agregar"></iframe>
            </div>
        </div>        
    </body>
</html>