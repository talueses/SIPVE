<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_puerta.control.op.php";// Class CONTROLLER
$obj = new CtrlPuertas();
$numExist = $obj->getNumerosPuertas();
if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){    
    $data = $obj->getCtrlPuerta($_REQUEST["idpuerta"]);
    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();                      
                $('#radioPuertaEntrada').buttonset();                
                <?php
                if ($_REQUEST["accion"] == "visualizar"){                                        
                    echo "    $('#radioPuertaEntrada').buttonset({disabled: true});\n";                    
                }
                ?>
                //--------------------------------------CHECK CHECKBOX --------------------------------------//
                /*$('div#radioPuertaEntrada input[type=radio]')
                    .live({
                        click: function(){
                            if($(this).val()=='1'){
                                var str;            
                                str  = '<center>';
                                str += '    ';
                                str += '        <div class="ui-widget ui-corner-all info" style="margin-top: 20px; padding: 0 .7em;">';
                                str += '            <span class="icon-info " style="float: left; margin-right: .3em;"></span>';
                                str += '            <p><b>Se establecer\xe1 esta puerta como entrada principal</b><br>Si ya hay otra puerta como principal, automaticamente cambiar\xe1 su configuraci\xf3n quedando esta como principal.</p>';            
                                str += '        </div>';
                                str += '    ';
                                str += '</center>';
                                $('#divmensaje').html(str);
                            }else{
                                $('#divmensaje').html('');
                            }
                        }
                    });*/
            });
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Puerta</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="mainForm" id="mainForm" action="ctrl_puerta.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr title="N&uacute;mero">
                                <td align="right">N&uacute;mero:</td>
                                <td>
                                    <select name="numero" id="numero" onchange="" <?php echo $disabled;?> >
                                        
                                        <?php
                                        for ($i=1;$i<=255;$i++){
                                            $chkNum = false;
                                            foreach ($numExist as $row){
                                                if ($row->numero == $i){
                                                    $chkNum = true;
                                                }
                                            }
                                            if (!$chkNum || $data->numero == $i){
                                                echo '<option value="'.$i.'" '.ControlCtrlPuerta::busca_valor($data->numero, $i ).'>'.$i.'</option>';
                                            }

                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="Puerta">
                                <td align="right">Puerta:</td>
                                <td>
                                    <input type="text" name="puerta" id="puerta" maxlength="100" value="<?php echo $data->puerta;?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Puerta de Entrada Principal">
                                <td align="right">Entrada Principal:</td>
                                <td>
                                    <div id="radioPuertaEntrada">
                                        <input type="radio" name="puerta_entrada" id="puerta_entrada1" value="1" <?php echo ($data->puerta_entrada=="1")?"checked":""?> /><label for="puerta_entrada1">Si</label>
                                        <input type="radio" name="puerta_entrada" id="puerta_entrada2" value="" <?php echo (!$data->puerta_entrada)?"checked":""?>/><label for="puerta_entrada2">No</label>
                                    </div>
                                    <small class="comment">Se toma en cuenta para el control de asistencia</small>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idpuerta" id="idpuerta" value="<?php echo $data->idpuerta ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Puerta\" />";
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>