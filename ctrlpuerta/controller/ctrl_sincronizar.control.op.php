<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php

require_once "../../ctrlsincronizar/controller/ctrl_sincronizar.control.php";// Class CONTROL ControlCtrlSincronizar()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlSincronizar extends ControlCtrlSincronizar{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlSincronizar(){
        
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        //echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
        $this->setAccion("agregar");
        $_REQUEST["item"] = "Puertas"; // Etiqueta de registro en el proceso de sincronizacion para accionar puerta
        if ($_REQUEST["accion"]=="abrir"){
            $_REQUEST["accion"] = "Abrir Por Numero de Puerta";
            if ($_REQUEST["all"]==""){
                $_REQUEST["codigoaccion"] = "2"; // Codigo de abrir puerta en la controladora   
            }elseif($_REQUEST["all"]=="true"){
                $_REQUEST["accion"] = "Abrir Todas Las Puertas";
                $_REQUEST["codigoaccion"] = "1"; // Codigo de abrir todas las puertas en la controladora
            }
            
        }
        if ($_REQUEST["accion"]=="cerrar"){
            $_REQUEST["accion"] = "Cerrar Por Numero de Puerta";
            if ($_REQUEST["all"]==""){
                $_REQUEST["codigoaccion"] = "16"; // Codigo de cerrar puerta en la controladora
            }elseif($_REQUEST["all"]=="true"){
                $_REQUEST["accion"] = "Cerrar Todas Las Puertas";
                $_REQUEST["codigoaccion"] = "15"; // Codigo de cerrar todas las puertas en la controladora
            }
        }
        $_REQUEST["prioridad"] = "100"; // Prioridad del proceso de sincronizacion para accionar puerta
        
        // Consulta de todas las controladoras a sincronizar las puertas
        if ($_REQUEST["all"]=="" && $_REQUEST["idpuerta"] != ""){
            $data = CtrlSincronizars::getControladorasPuertas($_REQUEST["idpuerta"]);
        }
        if($_REQUEST["all"]=="true"){
            $data = CtrlSincronizars::getControladoras();
        }

        
        if (count($data) > 0){
            //echo "<div align='left'><pre>".print_r($data,true)."</pre></div>";
            foreach ($data as $key => $row){

                $_REQUEST["nodo"]   = $row->nodo;
                $_REQUEST["ipv4"]   = $row->ipv4;
                $_REQUEST["puerto"] = $row->puerto;
                
                if($_REQUEST["all"] ==""){
                    $_REQUEST["param1"] = $row->numero_puerta; // Numero de la camara
                }

                //------------------ Metodo Set  -----------------//
                if(!$this->setCtrlSincronizar()) return false;

            }

            
        }
        return true;
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlSincronizar(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlSincronizar()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Sincronizacion");
        
        $this->setCampos("item","Item");
        $this->setCampos("accion","Accion");
        $this->setCampos("codigoaccion","Codigoaccion");
        $this->setCampos("nodo","Nodo");
        $this->setCampos("ipv4","Ipv4");
        $this->setCampos("puerto","Puerto");
        $this->setCampos("prioridad","Prioridad");
        $this->setCampos("param1","Param1");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"item","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"accion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"codigoaccion","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nodo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"ipv4","tipoDato"=>"esValidaIP");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"puerto","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"prioridad","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        if($_REQUEST["all"] ==""){
            $datos[] = array("isRequired"=>true,"datoName"=>"param1","tipoDato"=>"esAlfaNumericoConEspacios");
        }
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }
}
?>