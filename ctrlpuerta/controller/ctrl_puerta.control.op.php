<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLPTS"); //Categoria del modulo
require_once "ctrl_puerta.control.php";// Class CONTROL ControlCtrlPuerta()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlPuerta extends ControlCtrlPuerta{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlPuerta(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        /*if ($_REQUEST["puerta_entrada"]=="1"){
            if(!CtrlPuertas::unsetPuertaEntrada()){
                $this->mensaje=DB_Class::$dbErrorMsg;
                return false;
            }
        } */       
        
        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlPuerta()) return false;        
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlPuerta(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlPuerta()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Puerta");
        
        $this->setCampos("numero","N&uacute;mero");
        $this->setCampos("puerta","Puerta");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"numero","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"puerta","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }


    /**
     * Proceso de sincronizacion para abrir y cerrar puertas que pertenecen a una lectora de acceso
     * @return boolean devuelve verdadero si el proceso se ejecuta exitosamente
     */
    function puertaAcc(){
        
        //------------------ Sincronizar -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar()){
            $this->mensaje = $sinc->mensaje;
            return false;
        }

        $this->mensaje = "La petici&oacute;n de ".$this->getAccion();
        if ($_REQUEST["all"]=="true"){
            $this->mensaje .= " todas las puertas ";
        }else{
            $this->mensaje .= " puerta ";
        }
        $this->mensaje .= "fu&eacute; enviada exitosamente...";
        
        return true;
        
    }
        
    /**
     * Cosulta de Puertas
     * Discrimina las puertas que no pertenecen a segmentos del usuario logueado y deja puertas que no tengan asignadas lectoras
     * @param array $data Datos a partir de la consulta de puertas
     * @return array Devuelve el arreglo de datos sin las puertas respectivas 
     */
    public static function getPuertas($data){
        
        $obj  = new CtrlPuertas();
        $data = $obj->getCtrlPuertasLectoras();
        
        $dataAux = array();
        foreach ($data as $row){
            // No se toman en cuenta las puertas que sean de otro segmento al del usuario logueado
            // Se toman en cuenta las puertas que no tengan lectora asignada
            if (($row->chkSegmento > 0 && $row->chkLetora)||($row->chkSegmento == 0 && !$row->chkLetora)){
                $dataAux[] = $row;
            }
        }
        return $dataAux;
    }
}
?>