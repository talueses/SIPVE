<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'usuario.tabla.php';
require_once 'segmento_usuario.tabla.php';
require_once 'usuario_has_grupo.tabla.php';

/**
 * Clase Usuarios{}
 * @author David Concepcion CENIT-DIDI
 */
class Usuarios extends Usuario{

    /**
     * Consulta la coincidencia clave y usuario
     * @param string $usuario Usuario
     * @param string $clave Clave
     * @return int Devuelve numero de registros encontrados
     */
    static function chkUsuarioClave($usuario,$clave){
        $sql = "SELECT usuario FROM usuario WHERE usuario = '".$usuario."' and clave = '".md5(trim($usuario."_".$clave))."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return  $res->rowCount();
    }
    /**
     * Consulta de Usuario logueado para el listado de usuarios
     * @return object Devuelve un registro como objeto
     */
    function  getUsuarioLogin(){
        $sql = "select usuario,clave,nombre,apellido,activo,fcreado,fmodificado,nota,\"superUsuario\"  from usuario where usuario = '".$_SESSION["usuario"]."' order by activo desc , usuario ";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    /**
     * Consulta de Usuarios menos el usuario logueqdo y superUsuario para el listado de usuarios
     * @return  object Devuelve registros como objeto
     */
    function  getAllUsuarios(){
        // --- Valores del Buscador --- //
        if ($_REQUEST["usuarioBuscador"]){
            $in = null;
            foreach ($_REQUEST["usuarioBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and usuario in (".$in.")";
        }
        $sql = "select /*start*/ usuario,clave,nombre,apellido,activo,fcreado,fmodificado,nota,\"superUsuario\" /*end*/  from usuario where usuario <> '".$_SESSION["usuario"]."' and \"superUsuario\" = '0' ".$arg." order by activo desc , usuario ";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta los segmentos asociados a un usuario
     * @param string $usuario Usuario
     * @return object Devuelve registros como objeto
     */
    function getSegmentosUsr($usuario){
        $sql = "select idsegmento from segmento_usuario where usuario = '".$usuario."' ";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta los Grupos de Permisos asociados a un usuario
     * @param string $usuario Usuario
     * @return object Devuelve registros como objeto
     */
    function getGruposUsr($usuario){
        $sql = "select grupo_idgrupo from usuario_has_grupo where usuario_usuario = '".$usuario."' ";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    public function eliminarSegmentosUsuario($valor){
        $sql = "DELETE FROM segmento_usuario  WHERE usuario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar
}
?>
