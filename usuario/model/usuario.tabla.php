<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Usuario {
    
    private $usuario = null;
    private $clave = null;
    private $nombre = null;
    private $apellido = null;
    private $activo = null;
    private $fcreado = null;
    private $fmodificado = null;
    private $nota = null;
    private $superusuario = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setUsuario($usuario){
        $this->usuario = $usuario;
    }
    public function getUsuario(){
        return $this->usuario;
    }
    public function setClave($clave){
        $this->clave = $clave;
    }
    public function getClave(){
        return $this->clave;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setApellido($apellido){
        $this->apellido = $apellido;
    }
    public function getApellido(){
        return $this->apellido;
    }
    public function setActivo($activo){
        $this->activo = $activo;
    }
    public function getActivo(){
        return $this->activo;
    }
    public function setFcreado($fcreado){
        $this->fcreado = $fcreado;
    }
    public function getFcreado(){
        return $this->fcreado;
    }
    public function setFmodificado($fmodificado){
        $this->fmodificado = $fmodificado;
    }
    public function getFmodificado(){
        return $this->fmodificado;
    }
    public function setNota($nota){
        $this->nota = $nota;
    }
    public function getNota(){
        return $this->nota;
    }
    public function setSuperusuario($superUsuario){
        $this->superUsuario = $superUsuario;
    }
    public function getSuperusuario(){
        return $this->superUsuario;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->usuario !== null) && (trim($this->usuario)!=='') ){
            $campos .= "usuario,";
            $valores .= "'".$this->usuario."',";
        }
        if(($this->clave !== null) && (trim($this->clave)!=='') ){
            $campos .= "clave,";
            $valores .= "'".$this->clave."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->apellido !== null) && (trim($this->apellido)!=='') ){
            $campos .= "apellido,";
            $valores .= "'".$this->apellido."',";
        }
        if(($this->activo !== null) && (trim($this->activo)!=='') ){
            $campos .= "activo,";
            $valores .= "'".$this->activo."',";
        }
        if(($this->fcreado !== null) && (trim($this->fcreado)!=='') ){
            $campos .= "fcreado,";
            $valores .= "'".$this->fcreado."',";
        }
        if(($this->fmodificado !== null) && (trim($this->fmodificado)!=='') ){
            $campos .= "fmodificado,";
            $valores .= "'".$this->fmodificado."',";
        }
        if(($this->nota !== null) && (trim($this->nota)!=='') ){
            $campos .= "nota,";
            $valores .= "'".$this->nota."',";
        }
        if(($this->superUsuario !== null) && (trim($this->superUsuario)!=='') ){
            $campos .= "superUsuario,";
            $valores .= "'".$this->superUsuario."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO usuario $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE usuario SET ";
        
        if(($this->clave !== null) && (trim($this->clave)!=='') ){
            $sql .= "clave = '".$this->clave."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        if(($this->apellido !== null) && (trim($this->apellido)!=='') ){
            $sql .= "apellido = '".$this->apellido."',";
        }
        if(($this->activo !== null) && (trim($this->activo)!=='') ){
            $sql .= "activo = '".$this->activo."',";
        }
        if(($this->fcreado !== null) && (trim($this->fcreado)!=='') ){
            $sql .= "fcreado = '".$this->fcreado."',";
        }
        if(($this->fmodificado !== null) && (trim($this->fmodificado)!=='') ){
            $sql .= "fmodificado = '".$this->fmodificado."',";
        }
        else{
            $sql .= "fmodificado = NULL,";
        }
        if(($this->nota !== null) && (trim($this->nota)!=='') ){
            $sql .= "nota = '".$this->nota."',";
        }
        else{
            $sql .= "nota = NULL,";
        }
        if(($this->superUsuario !== null) && (trim($this->superUsuario)!=='') ){
            $sql .= "superUsuario = '".$this->superUsuario."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE usuario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM usuario  WHERE usuario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de Usuarios
     * @return object Devuelve un registro como objeto
     */
    function  getUsuarios(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["usuarioBuscador"]){
            $in = null;
            foreach ($_REQUEST["usuarioBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where usuario in (".$in.")";
        }
        
        $sql = "select /*start*/ usuario,clave,nombre,apellido,activo,fcreado,fmodificado,nota,superUsuario /*end*/ from usuario ".$arg." order by clave,nombre";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de Usuario
     * @param int $usuario Codigo
     * @return object Devuelve registros como objeto
     */
    function getUsuarioList($usuario){
        $sql = "SELECT usuario,clave,nombre,apellido,activo,fcreado,fmodificado,nota,\"superUsuario\" FROM usuario WHERE usuario = '".$usuario."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>