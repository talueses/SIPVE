<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class UsuarioHasGrupo {
    
    private $usuario_usuario = null;
    private $grupo_idgrupo = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setUsuario_usuario($usuario_usuario){
        $this->usuario_usuario = $usuario_usuario;
    }
    public function getUsuario_usuario(){
        return $this->usuario_usuario;
    }
    public function setGrupo_idgrupo($grupo_idgrupo){
        $this->grupo_idgrupo = $grupo_idgrupo;
    }
    public function getGrupo_idgrupo(){
        return $this->grupo_idgrupo;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->usuario_usuario !== null) && (trim($this->usuario_usuario)!=='') ){
            $campos .= "usuario_usuario,";
            $valores .= "'".$this->usuario_usuario."',";
        }
        if(($this->grupo_idgrupo !== null) && (trim($this->grupo_idgrupo)!=='') ){
            $campos .= "grupo_idgrupo,";
            $valores .= "'".$this->grupo_idgrupo."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO usuario_has_grupo $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE usuario_has_grupo SET ";
        
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE usuario_usuario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM usuario_has_grupo  WHERE usuario_usuario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de UsuarioHasGrupos
     * @return object Devuelve un registro como objeto
     */
    function  getUsuarioHasGrupos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["usuario_usuarioBuscador"]){
            $in = null;
            foreach ($_REQUEST["usuario_usuarioBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where usuario_usuario in (".$in.")";
        }
        
        $sql = "select usuario_usuario,grupo_idgrupo from usuario_has_grupo ".$arg." order by ,";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de UsuarioHasGrupo
     * @param int $usuario_usuario Codigo
     * @return object Devuelve registros como objeto
     */
    function getUsuarioHasGrupo($usuario_usuario){
        $sql = "SELECT usuario_usuario,grupo_idgrupo FROM usuario_has_grupo WHERE usuario_usuario = '".$usuario_usuario."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>