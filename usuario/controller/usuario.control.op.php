<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "ADMUSR"); //Categoria del modulo
require_once "usuario.control.php";// Class CONTROL ControlUsuario()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpUsuario extends ControlUsuario{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpUsuario(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        $_REQUEST["usuario"] = strtolower($_REQUEST["usuario"]);
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;

        if ($this->accion=="agregar" || ($this->accion=="modificar" && $_REQUEST["cambiarClave"]=="true")){
            if (strlen($_REQUEST["clave"])< 6){
                $this->mensaje = "El campo Clave debe tener al menos 6 caracteres.";
                return false;
            }
        }        

        if ($this->accion=="modificar"){
            
            if ($_REQUEST["cambiarClave"]=="false" && $_REQUEST["clave"]!=""){
                $this->mensaje = "Debe presionar el icono de la llave para cambiar la clave";
                return false;
            }
            if ($_REQUEST["cambiarClave"]=="true"){
                //echo $chkUsuarioClave = Usuarios::chkUsuarioClave($_REQUEST["usuario"],$_REQUEST["clave"]);
                if (Usuarios::chkUsuarioClave($_REQUEST["usuario"],$_REQUEST["clave"])==0){
                    $this->mensaje = "Clave incorrecta.";
                    return false;
                }

                if (md5(trim($_REQUEST["usuario"]."_".$_REQUEST["claveNueva"])) == md5(trim($_REQUEST["usuario"]."_".$_REQUEST["clave"]))){
                    $this->mensaje = "La clave nueva debe ser distinta a la anterior.";
                    return false;
                }
                if (strlen($_REQUEST["claveNueva"])< 6){
                    $this->mensaje = "El campo Clave Nueva debe tener al menos 6 caracteres.";
                    return false;
                }
                if ($_REQUEST["claveNueva"] != $_REQUEST["repetirClaveNueva"]){
                    $this->mensaje = "La clave nueva debe ser igual a la clave repetida de verificaci&oacute;n.";
                    return false;
                }                 
                $_REQUEST["clave"] = md5(trim($_REQUEST["usuario"]."_".$_REQUEST["claveNueva"]));
            }
            $_REQUEST["fmodificado"] = date("Y-m-d H:i:s");

            // ------------- SEGMENTOS -------------- //
            if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"segmentos")){
                
                $obj = new Usuarios();                
                $obj->eliminarSegmentosUsuario($_REQUEST["usuario"]);
                
                $obj = new SegmentoUsuario();            
                $obj->setUsuario($_REQUEST["usuario"]);                

                if (count($_REQUEST["segmento"]) > 0){
                    foreach ($_REQUEST["segmento"] as $idsegmento){
                        $obj->setIdsegmento($idsegmento);
                        $obj->insertarRegistro();
                    }
                }
            }

            // ------------- Grupos de Permisos -------------- //
            if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"gprPermiso")){
                $obj = new UsuarioHasGrupo();

                $obj->setUsuario_usuario($_REQUEST["usuario"]);

                $obj->eliminarRegistro($obj->getUsuario_usuario());

                if (count($_REQUEST["grupo"]) > 0){
                    foreach ($_REQUEST["grupo"] as $grupo_idgrupo){
                        $obj->setGrupo_idgrupo($grupo_idgrupo);
                        $obj->insertarRegistro();
                    }
                }                
            }
            
            
        }
        if ($this->accion=="agregar"){
            $_REQUEST["clave"] = md5(trim($_REQUEST["usuario"]."_".$_REQUEST["clave"]));
        }

        
        //------------------ Metodo Set  -----------------//
        if(!$this->setUsuario()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpUsuario(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarUsuario()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Usuario");

        $this->setCampos("usuario","Usuario");
        $this->setCampos("clave","Clave");
        $this->setCampos("claveNueva","Clave Nueva");
        $this->setCampos("repetirClaveNueva","Repetir Clave Nueva");
        $this->setCampos("nombre","Nombre");
        $this->setCampos("apellido","Apellido");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){

        $datos   = array();
        
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"usuario","tipoDato"=>"");
        
        //------------------------------------------------------------------------------------------------------------//
        if ($this->accion=="modificar" && $_REQUEST["cambiarClave"]=="true"){
            //------------------------------------------------------------------------------------------------------------//
            $datos[] = array("isRequired"=>true,"datoName"=>"clave","tipoDato"=>"");
            //--------------------------------------------------------------------------------------------------------//
            $datos[] = array("isRequired"=>true,"datoName"=>"claveNueva","tipoDato"=>"");
            //--------------------------------------------------------------------------------------------------------//
            $datos[] = array("isRequired"=>true,"datoName"=>"repetirClaveNueva","tipoDato"=>"");
        }
        if ($this->accion=="agregar"){
            //------------------------------------------------------------------------------------------------------------//
            $datos[] = array("isRequired"=>true,"datoName"=>"clave","tipoDato"=>"");
        }
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"apellido","tipoDato"=>"esAlfaNumericoConEspacios");

        //------------------------------------------------------------------------------------------------------------//
        if (!$this->validarDatos($datos))return false;

        return true;
    }

}
?>