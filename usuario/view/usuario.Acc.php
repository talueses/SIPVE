<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 *
 *
 *
 *
 *
 *
 *
 * @todo Campos fecha creado y modificado
 *       ficha datos extra solo ver y mod
 *
 *
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/usuario.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new Usuarios();
    $data = $obj->getUsuarioList($_REQUEST["usuario"]);

    //-------- Segmentos de un Usuario -----------------//
    $segUsr = $obj->getSegmentosUsr($_REQUEST["usuario"]);
    if (count($segUsr)){
        foreach ($segUsr as $row){
            $segmentos[] = $row->idsegmento;
        }
    }
    //-------- Segmentos de un Usuario -----------------//
    $gprUsr = $obj->getGruposUsr($_REQUEST["usuario"]);
    if (count($gprUsr)){
        foreach ($gprUsr as $row){
            $grupos[] = $row->grupo_idgrupo;
        }
    }
    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link type="text/css" rel="stylesheet" media="screen" href="../css/jquery.toChecklist.css" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.textarea.maxlength.js"></script>
        <script type="text/javascript" src="../js/jquery.toChecklist.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #cambiarClaveSquare{
                width:99%;
                background:#fff;
                margin: 2px 0px 5px 0px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            
}
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
                $('#radioActivo').buttonset();
                //$('#segmento, #grupo').toChecklist();
                <?php
                if ($_REQUEST["accion"] == "visualizar"){
                    echo "$( 'textarea' ).resizable({disabled: true});";
                    echo "$('#radioActivo').buttonset({disabled: true});";
                }
                ?>
                $('#keyButton').live({
                    click: function(){
                        $('#trClaveNueva').toggle('slow',function(){
                            if ($('#cambiarClave').val()=="true"){
                                $('#cambiarClave').val('false');                                
                                return false;
                            }
                            if ($('#cambiarClave').val()=="false"){
                                $('#cambiarClave').val('true');                                
                                return false;
                            }
                        });
                    }
                });
            });
            $(document).ready(function(){
                $('#tabs').tabs();
                if ($('#accion').val()=="visualizar"){
                    $("#segmento input[type='checkbox'], #grupo input[type='checkbox']").attr("disabled",true);
                }
                
            });
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Usuario</div>            
            <br/>
            <div id="divmensaje" style="width:99%;"></div>
            <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
            <form method="POST" name="f1" action="usuario.Op.php" target="ifrm1">
                <div id="datos"  align="center">
                    <div id="tabs">
                        <ul >
                            <li><a href="#datosBasicos">Datos B&aacute;sicos</a></li>
                            <li><a href="#datosExtra">Datos Extra</a></li>
                        </ul>
                        <div id="datosBasicos">
                            <table>
                                <tr title="Usuario">
                                    <td align="right" width="83">Usuario:</td>
                                    <td >
                                        <input type="text" name="usuario" id="usuario" maxlength="40" value="<?php echo $data->usuario;?>" <?php echo $disabled; echo $_REQUEST["accion"] == "modificar"?"readonly":"";?> onblur="$(this).val($(this).val().toLowerCase())">
                                    </td>
                                </tr>
                                <tr title="Clave">
                                    <td align="right" valign="top">Clave:</td>
                                    <td>
                                        <input type="password" name="clave" id="clave" maxlength="40" value="" <?php echo $disabled;?> >
                                        <?php
                                        if ($_REQUEST["accion"]=="modificar"){
                                            ?>
                                            <button type="button" style="font-size: 0px;float: right; height: 30px;" title="Modificar Clave" id="keyButton" >
                                                <img src="../images/Bullet-Key-32.png" alt=""/>
                                            </button>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr id="trClaveNueva" style="display: none">
                                    <td colspan="2">
                                        <fieldset id="cambiarClaveSquare">
                                            <legend>Modificar Clave</legend>
                                            <table >
                                                <tr title="Clave Nueva">
                                                    <td align="right" valign="top">Clave Nueva:</td>
                                                    <td>
                                                        <input type="password" name="claveNueva" id="claveNueva" maxlength="40" value=""  >
                                                    </td>
                                                </tr>
                                                <tr title="Repetir Clave Nueva">
                                                    <td align="right" valign="top">Repetir:</td>
                                                    <td>
                                                        <input type="password" name="repetirClaveNueva" id="repetirClaveNueva" maxlength="40" value=""  >
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr title="Nombre">
                                    <td align="right">Nombre:</td>
                                    <td>
                                        <input type="text" name="nombre" id="nombre" maxlength="50" value="<?php echo $data->nombre;?>" <?php echo $disabled;?>>
                                    </td>
                                </tr>
                                <tr title="Apellido">
                                    <td align="right">Apellido:</td>
                                    <td>
                                        <input type="text" name="apellido" id="apellido" maxlength="50" value="<?php echo $data->apellido;?>" <?php echo $disabled;?>>
                                    </td>
                                </tr>
                                <tr title="Nota">
                                    <td align="right" valign="top">Nota:</td>
                                    <td>
                                        <div class="divTextarea" >
                                            <textarea name="nota" id="nota" <?php echo $disabled;?> rows="6" cols="28" maxlength="250" <?php echo $disabled;?>><?php echo $data->nota;?></textarea>
                                            <div class="charLeftDiv"  align="right"><input type="text" class="charLeft" id="charLeft_nota" size="4" readonly > Caracteres Restantes</div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="datosExtra">
                            <table>
                                <tr title="Fecha de Creaci&oacute;n">
                                    <td align="right" valign="top">Fecha de Creaci&oacute;n:</td>
                                    <td>
                                        <input type="text" size="20" value="<?php echo $data->fcreado=="" ? date("d-m-Y H:i:s") : Controller::formatoFecha($data->fcreado);?>" readonly >
                                    </td>
                                </tr>
                                <tr title="Fecha de Modificaci&oacute;n">
                                    <td align="right" valign="top">Fecha de Modificaci&oacute;n:</td>
                                    <td>
                                        <input type="text" size="20" value="<?php echo Controller::formatoFecha($data->fmodificado);?>" readonly >
                                    </td>
                                </tr>
                                <?php
                                if ($_REQUEST["accion"]!="agregar" ){
                                    if ( Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"segmentos")){
                                        ?>
                                        <tr title="Segmentos">
                                            <td align="right" valign="top">Segmentos:</td>
                                            <td>
                                                <div class="divTextarea" >
                                                    <select name="segmento[]" id="segmento" style="width: 220px;" multiple <?php echo $disabled;?> size="7" >
                                                        <?php echo ControlUsuario::make_combo("segmento", "order by segmento", "idsegmento", "segmento", $segmentos,false);?>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    if ( Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"gprPermiso")){
                                        ?>
                                        <tr title="Activo">
                                            <td align="right">Activo:</td>
                                            <td>
                                                <div id="radioActivo">
                                                    <input type="radio" name="activo" id="radioActivo1" value="1" <?php echo (!$data->activo || $data->activo=="1")?"checked":""?> /><label for="radioActivo1">Si</label>
                                                    <input type="radio" name="activo" id="radioActivo2" value="0" <?php echo ($data->activo=="0")?"checked":""?>/><label for="radioActivo2">No</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr title="Grupos de Permisos">
                                            <td align="right" valign="top">Grupos Permisos:</td>
                                            <td>
                                                <div class="divTextarea" >
                                                    <select name="grupo[]" id="grupo" style="width: 220px;" multiple <?php echo $disabled;?> size="7" >
                                                        <?php echo ControlUsuario::make_combo("grupo", "order by nombre", "idgrupo", "nombre", $grupos,false);?>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="botones" style="clear:left">
                    <input type="hidden" name="cambiarClave" id="cambiarClave" value="false">
                    <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                    <input type="hidden" name="superUsuario" id="superUsuario" value="<?php echo $data->superUsuario;?>">
                        <?php
                        if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                            echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Usuario\" />";
                        }
                        ?>
                </div>
            </form>
        </div>
    </body>
</html>