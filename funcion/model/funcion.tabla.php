<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Funcion {
    
    private $id = null;
    private $idfuncion = null;
    private $nombre = null;
    private $descripcion = null;
    private $mobil = null;
    private $idcategoria = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function getId(){
        return $this->id;
    }
    public function setIdfuncion($idfuncion){
        $this->idfuncion = $idfuncion;
    }
    public function getIdfuncion(){
        return $this->idfuncion;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setMobil($mobil){
        $this->mobil = $mobil;
    }
    public function getMobil(){
        return $this->mobil;
    }
    public function setIdcategoria($idcategoria){
        $this->idcategoria = $idcategoria;
    }
    public function getIdcategoria(){
        return $this->idcategoria;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->id !== null) && (trim($this->id)!=='') ){
            $campos .= "id,";
            $valores .= "'".$this->id."',";
        }
        if(($this->idfuncion !== null) && (trim($this->idfuncion)!=='') ){
            $campos .= "idfuncion,";
            $valores .= "'".$this->idfuncion."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $campos .= "descripcion,";
            $valores .= "'".$this->descripcion."',";
        }
        if(($this->mobil !== null) && (trim($this->mobil)!=='') ){
            $campos .= "mobil,";
            $valores .= "'".$this->mobil."',";
        }
        if(($this->idcategoria !== null) && (trim($this->idcategoria)!=='') ){
            $campos .= "idcategoria,";
            $valores .= "'".$this->idcategoria."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO funcion $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE funcion SET ";
        
        if(($this->idfuncion !== null) && (trim($this->idfuncion)!=='') ){
            $sql .= "idfuncion = '".$this->idfuncion."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        else{
            $sql .= "nombre = NULL,";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $sql .= "descripcion = '".$this->descripcion."',";
        }
        else{
            $sql .= "descripcion = NULL,";
        }
        if(($this->mobil !== null) && (trim($this->mobil)!=='') ){
            $sql .= "mobil = '".$this->mobil."',";
        }
        else{
            $sql .= "mobil = NULL,";
        }
        if(($this->idcategoria !== null) && (trim($this->idcategoria)!=='') ){
            $sql .= "idcategoria = '".$this->idcategoria."',";
        }
        else{
            $sql .= "idcategoria = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM funcion  WHERE id = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de Funcions
     * @return object Devuelve un registro como objeto
     */
    function  getFuncions(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idBuscador"]){
            $in = null;
            foreach ($_REQUEST["idBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where id in (".$in.")";
        }
        
        $sql = "select /*start*/ id,idfuncion,nombre,descripcion,mobil,idcategoria /*end*/ from funcion ".$arg." order by nombre,descripcion";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de Funcion
     * @param int $id Codigo
     * @return object Devuelve registros como objeto
     */
    function getFuncion($id){
        $sql = "SELECT id,idfuncion,nombre,descripcion,mobil,idcategoria FROM funcion WHERE id = '".$id."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>