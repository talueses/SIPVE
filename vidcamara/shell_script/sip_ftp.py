#!/usr/bin/env python

import os, sys, time, getopt
import pexpect


def exit_with_usage():
	#print globals()['__doc__']
	os._exit(1)


def main():
    ######################################################################
    # Parse the options, arguments, get ready, etc.
    ######################################################################
	try:
		optlist, args = getopt.getopt(sys.argv[1:], 'h?s:d:u:p:f:', ['help','h','?'])
	except Exception, e:
		print str(e)
		exit_with_usage()
	options = dict(optlist)
	if len(args) > 1:
		exit_with_usage()
        
	if [elem for elem in options if elem in ['-h','--h','-?','--?','--help']]:
		print "Help:"
		exit_with_usage()
	command = 'sftp ' + options['-u'] + '@' + options['-s'] + ':' + options['-d']
	child = pexpect.spawn (command)
	j = child.expect(['authenticity', 'assword:'])
	if j==0:
		child.sendline("yes")
		j = child.expect(['authenticity', 'assword:'])
	if j==1:
		child.sendline (options['-p'])
		child.expect ('sftp>')
		child.sendline ('put '+options['-f'] )
                #+ ' motion.conf '
		child.expect ('sftp>')
		child.sendline ('bye')
	return 0


if __name__ == "__main__":
	try:
		main()
	except SystemExit, e:
		raise e
	except Exception, e:
		print "ERROR"
		print str(e)
		traceback.print_exc()
		os._exit(1)

