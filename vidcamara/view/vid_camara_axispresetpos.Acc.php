<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/vid_camara_axispresetpos.control.op.php";// Class CONTROLLER

$obj = new VidCamaraAxispresetposs();
$obj->idcamara = $_REQUEST["idcamara"];

$dataCam = VidCamaraAxispresetposs::getCamaraConf($obj->idcamara);

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    
    $data = $obj->getVidCamaraAxispresetpos($_REQUEST["idcamaraaxispresetpos"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        
        <style type="text/css">            
            #contenido{
                width:99%;
                border:  #aaaaaa solid 1px;
                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 80%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            input[type="image"]{
                cursor: crosshair;
                background:#fff;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();                
            });
            
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <div id="contenido" align="center">             
            <br/>
            <div id="divmensaje" style="width:99%;"></div>
            <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
            <form method="POST" name="fimg" id="fimg" action="vid_camara_axispresetpos.Op.php" target="ifrm1">
                <input type="image" name="<?php echo $_REQUEST["idcamara"];?>" id="<?php echo $_REQUEST["idcamara"];?>"  src="<?php echo $dataCam->videoUrl?>" onclick="" />
                <input type="hidden" name="accion" value="moveImg">
                <input type="hidden" name="idcamara" id="idcamara" value="<?php echo $_REQUEST["idcamara"];?>">
            </form>
            <form method="POST" name="f1" action="vid_camara_axispresetpos.Op.php" target="ifrm1" onsubmit="">
                
                <div id="datos"  align="center">
                    <table>                        
                        <tr title="Descripci&oacute;n Corta">
                            <td align="right">Descripci&oacute;n:</td>
                            <td>
                                <input type="text" name="presetname" id="presetname" maxlength="100" value="<?php echo $data->presetname;?>" <?php echo $disabled;?>>
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div id="botones" style="clear:left">
                    <input type="hidden" name="presetnbr" id="presetnbr" value="<?php echo $data->presetnbr;?>">
                    <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                    <input type="hidden" name="idcamara" id="idcamara" value="<?php echo $_REQUEST["idcamara"];?>">
                    <input type="hidden" name="idcamaraaxispresetpos" id="idcamaraaxispresetpos" value="<?php echo $data->idcamaraaxispresetpos ?>">
                    <?php
                    if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                        echo "<input type=\"submit\" id=\"botonSubmit\" value=\"".ucfirst($_REQUEST["accion"])." Posici&oacute;n \" />";
                    }
                    ?>
                    <input type="button" value="Cancelar"  onclick="document.location = 'vid_camara_axispresetpos.php?idcamara=<?php echo $_REQUEST["idcamara"];?>'" />
                </div>
            </form>
        </div>
    </body>
</html>