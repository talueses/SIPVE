<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

//================== INSTANCIACION DE CLASES ==================//
require_once "../controller/camaras_vid_camara.control.php"; // Class CONTROLLER


$data = CamaraPlantaFisica::getZonasPlantaFisicaCamara($_REQUEST["idcamara"],$_REQUEST["idplantafisica"]);

// ------------ IMAGEN DE PLANO --------------//
$image = "../../plantafisica/images/".$data[0]->file;
$imagesize = getimagesize($image);

/*
$zonasCoord[] = array("idzona"=>"1","zona"=>"Despachos","left"=>"16","top"=>"10","width"=>"100","height"=>"100","bgcolor"=>"#".random_color());
$zonasCoord[] = array("idzona"=>"2","zona"=>"Paso","left"=>"56","top"=>"60","width"=>"350","height"=>"350","bgcolor"=>"#".random_color());
$camarasCoord[] = array("idcamara" => "1","camara" => "uno","leftX" => "0","topY" => "0");
*/

foreach ($data as $row){
    if ($row->idplantafisicaCamara == $_REQUEST["idplantafisica"]){ // si las coordenadas de la zona estan registradas
        $camarasCoord[] = array("idcamara" => $row->idcamara,"camara" => $row->camara,"leftX" => $row->leftXCamara,"topY" => $row->topYCamara);
    }
    $zonasCoord[] = array("idzona"=>$row->idzona,"zona"=>$row->zona,"leftX"=>$row->leftXZona,"topY"=>$row->topYZona,"width"=>$row->width,"height"=>$row->height,"bgcolor"=>$row->bgcolor);
    $zonasID[] = "'".$row->idzona."'";
}

/*if (count($zonasCoord)){
    // ------ VALIDACION PERIMETRO DE ZONA ------------- //
    $perimetroChk = false;
    foreach ($zonasCoord as $row){
        if ($_POST["leftX"] >= $row["leftX"] && $_POST["topY"] >= $row["topY"] && $_POST["leftX"] <= ($row["leftX"]+$row["width"]) && $_POST["topY"] <= ($row["topY"] + $row["height"])){
            $perimetroChk = true;
        }
    }
    if (!$perimetroChk){
        echo "<font color=\"red\">Fuera de la zona</font>";
        $_POST["leftX"] = $row["leftX"];
        $_POST["topY"] = $row["topY"];
    }
}*/


?>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>    
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css">
            .title{
                width: <?php echo $imagesize[0]?>px;
                color: #770;
                font-style: oblique;
                background-color: #f0ebe2;
                border: 1px solid #ccc0a9;
            }
            #close{
                font-size: 10px;
                font: bolder;
                color: #000;
                border: double #000;
                width: 8px;
                cursor: pointer;
                float: right;
                text-align: center;
                font-style: normal;                
            }
            
            #close:hover{
                opacity:0.3;
            }
            /*---------PLANO PLANTA FISICA--------*/
            #map {
                background: #fff url(<?php echo $image?>) no-repeat center;
                width: <?php echo $imagesize[0]?>px;
                height: <?php echo $imagesize[1]?>px;
                border: ridge 3px #000000;
                position: relative;
                clear: both;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            /*--------------ZONAS------------------*/
            #map-part {
                /*left: <?php echo floor(($imagesize[0]-50)/2)?>px;
                top: <?php echo floor(($imagesize[1]-50)/2)?>px;
                width: 50px;
                height: 50px;
                background-color:#BCBCBC;*/
                opacity:0.5;
                border: dashed 1px #000000;
                position: absolute;
            }
            #map-part:hover { opacity:0.8; }

            #map-part span{ display: none; }

            #map-part:hover span{
                display: block;
                vertical-align: top;
                color: #000;
                background-color: #F4F4F4;
                position: absolute;
                border: solid 1px  #BCBCBC;
                bottom: 100%;
                width: 75%;
            }
            
            /*-------------CAMARAS--------------*/
            .camara {
                cursor: pointer;
                position: absolute;
            }
            #contenido{
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 80%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #divmensaje{
                width: 100%;
                position: absolute;

                top: -15px;
                opacity:0.9;
            }
        </style>
        <script type="text/javascript">
            // -------------- DIMENCION DEL iFRAME ------------------//
            var widthToSet = <?php echo $imagesize[0]+20?>;
            var heightToSet = <?php echo $imagesize[1]+200?>;
            $('#frameMap' ,parent.document).width(widthToSet);
            $('#frameMap' ,parent.document).height(heightToSet);
            // ------------- DIMENCION DEL CONTENEDOR DEL iFRAME ----//
            if ( parseInt($('#loadMap',parent.document).width()) < parseInt(widthToSet) ){
                $('#loadMap',parent.document).width(widthToSet+50);
            }
            if ( parseInt($('#loadMap',parent.document).height()) < parseInt(heightToSet) ){
                $('#loadMap',parent.document).height(heightToSet+50);
            }
            
            $(function(){   
                $( "input:button, input:submit, button" ).button();
                $('#divmensaje').live({
                    click: function() {
                        $(this).fadeOut('slow');
                    }
                });
            });
            
            $(function(){
                <?php

                if (count($zonasCoord) > 0){
                    // ----------------- SET CSS ZONAS ------------------//
                    foreach ($zonasCoord as $row){

                        echo "  $('.zona".$row["idzona"]."').css({\n";
                        echo "      'left':'".$row["leftX"]."px',\n";
                        echo "      'top' :'".$row["topY"]."px',\n";
                        echo "      'width':'".$row["width"]."px',\n";
                        echo "      'height':'".$row["height"]."px',\n";
                        echo "      'background-color':'".$row["bgcolor"]."'\n";
                        echo "  });\n";
                        if (count($camarasCoord) == 0){
                            echo "  $('.camara').css({\n";
                            echo "      'left':'".($row["leftX"]+20)."px',\n";
                            echo "      'top' :'".($row["topY"]+20)."px',\n";
                            echo "  });\n";
                            echo "  $('#leftX').val('".($row["leftX"]+20)."');\n";
                            echo "  $('#topY').val('".($row["topY"]+20)."');\n";
                        }
                    }
                }
                if (count($camarasCoord) > 0){
                    foreach ($camarasCoord as $row){
                         // ----------------- SET CSS CAMARA ------------------//
                        echo "  $('#camara".$row["idcamara"]."').css({\n";
                        echo "      'left':'".$row["leftX"]."px',\n";
                        echo "      'top' :'".$row["topY"]."px'\n";
                        echo "  });\n";
                        echo "  $('#leftX').val('".$row["leftX"]."');\n";
                        echo "  $('#topY').val('".$row["topY"]."');\n";
                    }
                }
               if ($_REQUEST["accion"]!='visualizar'){
                   ?>
                   $(".camara").draggable({
                        start: function() {
                            $(this).css('cursor','move');
                        },
                        drag: function(event,ui) {
                            $('#leftX').val(ui.position.left);
                            $('#topY').val(ui.position.top);
                        },
                        stop: function() {
                            $(this).css('cursor','pointer');
                            chkPerimeter();
                        },
                        containment:'#map'
                    });
                   <?php
               }
               ?>
                
            });
            function closeDoc(){
                parent.document.location.href = 'camarasAcc.php?idcamara=<?php echo $_REQUEST["idcamara"]?>&accion=<?php echo ($_REQUEST["accion"]!='visualizar')?'modificar':$_REQUEST["accion"];?>';
            }
            function chkPerimeter(){
                $('#divmensaje').css('border','').fadeOut('slow').html('');
                var chk = false;
                var str;
                var zonasId = [<?php echo implode(',', $zonasID)?>];
                var left;
                var top;
                for (var i in zonasId){
                    if (parseInt($(".camara").css('left')) >= parseInt($(".zona"+zonasId[i]).css('left'))&&
                        parseInt($(".camara").css('top'))  >= parseInt($(".zona"+zonasId[i]).css('top'))&&
                        parseInt($(".camara").css('left')) <= (parseInt($(".zona"+zonasId[i]).css('left'))+parseInt($(".zona"+zonasId[i]).css('width')))&&
                        parseInt($(".camara").css('top'))  <= (parseInt($(".zona"+zonasId[i]).css('top'))+parseInt($(".zona"+zonasId[i]).css('height')))){
                        chk = true;
                    }
                    left = parseInt($(".zona"+zonasId[i]).css('left'))+20;
                    top  = parseInt($(".zona"+zonasId[i]).css('top'))+20;
                }
                if (!chk){
                    str  = '<div class=\"ui-widget\">';
                    str += '    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';
                    str += '        <p>';
                    str += '            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>';
                    str += '                C&aacute;mara fuera de la Zona';
                    str += '        </p>';
                    str += '    </div>';
                    str += '</div>';
                    $('#divmensaje').html(str).fadeIn('slow');
                    $('.camara').css({
                        'left': left+'px',
                        'top' : top+'px'
                    });
                    $('#leftX').val(left);
                    $('#topY').val(top);
                }
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;" >
        
        <div id="contenido" align="center">
            
            <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
            <center>
                <div id="titulo">
                    <?php echo $data[0]->plantafisica?>
                    <div id="close" onclick="closeDoc();" title="Cerrar" >X</div>
                </div>
                
                <div id="map">
                    <div id="divmensaje" ></div>
                    <?php
                    if (count($zonasCoord)){
                        // ------------------- ZONAS -------------------//
                        foreach ($zonasCoord as $row){
                            echo "<div id=\"map-part\" class=\"zona".$row["idzona"]."\"><span>".$row["zona"]."</span></div>\n";
                        }
                    }
                    echo "<img src=\"../images/Video-Camera-32x15.png\"  border=\"0\" class=\"camara\" id=\"camara".$data[0]->idcamara."\" alt=\"".$data[0]->camara."\" title=\"".$data[0]->camara."\">";
                    ?>
                </div>
                <?php
                if ($_REQUEST["accion"]!="visualizar"){
                    echo "<button onclick=\"$('#form1').submit();\">Guardar</button>";
                }
                ?>
                <button onclick="closeDoc();">Cancelar</button>
            </center>
            <form name="form1" id="form1" method="post" action="mapaSetCamaraOp.php" target="ifrm1">
                <input type="hidden" name="leftX" id="leftX">
                <input type="hidden" name="topY" id="topY" >
                <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                <input type="hidden" name="idplantafisica" id="idplantafisica" value="<?php echo $_REQUEST["idplantafisica"];?>">
                <input type="hidden" name="idcamara" id="idcamara" value="<?php echo $_REQUEST["idcamara"];?>">
            </form>
            
        </div>
    </body>    
</html>