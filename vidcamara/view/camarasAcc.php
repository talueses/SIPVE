<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * @author David Concepcion 16-09-2010 CENIT
 * Formulario de acciones agregar, modificar y visualizar datos
 *
 * Modificado David Concepcion 07-10-2010 CENIT
 * Modulo Zonas de una camara en agregar, modificar y visualizar datos
 * Nuevo campo prioridad
 * Mensaje Cargando...
 *
 * @todo quitar la asociacion de las zonas por el sumit
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_POST)."</pre></div>";

//================== INSTANCIACION DE CLASES ==================//
require_once "../controller/camaras_vid_camara.control.php"; // Class CONTROLLER
$camara = new Camara();
//===============FIN INSTANCIACION DE CLASES ==================//



if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    //echo "getData";
    $cam     = $camara->get_Camara($_REQUEST["idcamara"]);

    //-------- Zonas de una camara -----------------//
    $zonaCam = ZonasCamara::getZonaCamaras($_REQUEST["idcamara"]);
    if (count($zonaCam)){
        foreach ($zonaCam as $row){
            $zonas[] = $row->idzona;
        }
    }
}

if ($_REQUEST["accion"] == "visualizar"){
    $disabled = " disabled = 'disabled' ";
    
}
if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    ?>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
            <title><?php echo ucfirst($_REQUEST["accion"]);?> C&aacute;mara de Video</title>
            
            <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
            <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
            <link type="text/css" rel="stylesheet" media="screen" href="../css/jquery.toChecklist.css" />
            <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
            <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
            <script type="text/javascript" src="../js/jquery.toChecklist.js"></script>
            <style type="text/css">
                div.savedItems { color: #770; font-size: .8em; list-style-position: outside; margin-left: 0;}
                div.savedItems .error { color: #ff0000;padding-left: 10px; }
                .list{
                    color: #000000;
                    font-style: oblique;
                    background-color: #f0ebe2;
                    width: 300px;
                    border: 1px solid #ccc0a9;
                }
                #contenido{
                    float:left;
                    width:99%;
                    height:95%;
                    padding: 2px;
                    border:  #aaaaaa solid 1px;
                    overflow-x:hidden
                    width:95%;

                    background:#fff;
                    -moz-border-radius: 6px;
                    -webkit-border-radius: 6px;
                                    border-radius: 6px;
                }
                #titulo{
                    height: 20px;
                    border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                    -moz-border-radius: 6px;
                    -webkit-border-radius: 6px;
                                    border-radius: 6px;
                }
                #datos{
                    width: 80%;
                    background:#fff;
                    margin: 5px 1px 0px 1px;
                    padding: 5px;
                    border:  #aaaaaa solid 1px;
                    -moz-border-radius: 6px;
                    -webkit-border-radius: 6px;
                                    border-radius: 6px;
                }
                #botones{
                    margin: 10px;
                }
                
                /* Para que se vean los Tabs cuando se cargue el mapa*/
                .ui-tabs, .ui-tabs .ui-tabs-nav li, .ui-button{
                    position: inherit;
                }
            </style>
            <script type="text/javascript" language="javascript">
                function hideLoading(){
                    $("#cargando",parent.document).fadeOut("slow");
                }
                var currentopt="op1";
                var currentdiv="conexion";
                function mostrar(pop, pdiv){
                    if(currentopt==pop) return false;
                    
                    $("#"+currentopt).attr("className","");
                    $("#"+pop).attr("className","current");
                    
                    $('#'+currentdiv).css('display','none');
                    $("#"+pdiv).css("display","");
                    
                    currentopt=pop;
                    currentdiv=pdiv;
                    return false;
                }                
                $(function() {
                    //$('#idzona').toChecklist();
                    
                    $( "input:button, input:submit" ).button();
                    $('#tabs').tabs();
                    $('#presetPosDialog').dialog({
                        autoOpen: false,
                        resizable: false,
                        draggable: false,
                        width: 410,//390
                        height:510,//460
                        position:'top',
                        modal:true,
                        open: function(event, ui) { 
                           $('#presetPosFrame').attr('src','vid_camara_axispresetpos.php?idcamara='+$('#idcamara').val());
                        },
                        close: function(event, ui) { 
                           $('#presetPosFrame').attr('src','');
                        }
                    }); 
                    $('#horarioDialog').dialog({
                        autoOpen: false,
                        resizable: false,
                        draggable: false,
                        width: 580,
                        height:250,
                        position:'top',
                        modal:true,
                        open: function(event, ui) { 
                           $('#ifrmH').attr('src','horariosAcc.php?idcamara='+$('#idcamara').val()+'&idservidor='+$('#idservidor').val()+'&accion='+$('#accion').val());
                        },
                        close: function(event, ui) { 
                           $('#ifrmH').attr('src','');
                        }
                    });
                    //horariosAcc.php?idcamara=<?php echo $cam->idcamara ?>&idservidor=<?php echo $cam->idservidor;?>&accion=<?php echo $_REQUEST["accion"];?>
                    $('#tipologia').val();
                    getPlantaFisica();
                });
                function getPlantaFisica(){
                    $.ajax({
                        type: "POST",
                        url: "getPlantaFisica.php",
                        data: 'idcamara='+$('#idcamara').val()+'&idplantafisica=<?php echo $cam->idplantafisica?>&accion='+$('#accion').val(),
                        success: function(html){
                            $('#getPlantaFisica').html(html);
                        }
                    });
                    
                }
                function setImage(idplantafisica){                    
                    var link = 'mapaSetCamara.php?idcamara='+$('#idcamara').val()+'&idplantafisica='+idplantafisica+'&accion='+$('#accion').val();
                    $("#frameMap").attr("src",link);
                    $("#loadMap").fadeIn("slow");
                }
            </script>
        </head>
        <body style="margin:0px;background:#ddd;">
            <div id="loadMap" align="center" style="position: absolute;background-color:#000000;opacity:.9;width: 100%;height: 100%;vertical-align: middle;color: #FFFFFF;font-size: 20px;display: none;">
                <iframe id="frameMap" src="" ></iframe>
            </div>
            <div id="contenido" align="center">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                    echo Controller::$mensajePermisos;
                }else{
                    ?>
                    <div id="titulo" style="padding: 5px"><?php echo ucfirst($_REQUEST["accion"] );?> C&aacute;mara de Video</div>
                    <br/>
                    <div id="divmensaje" style="width:99%;"></div>
                    <iframe name="ifrm1" id="ifrm1" frameborder="0" height="0" width="0" scrolling="no"></iframe>
                    <form method="POST" name="f1" action="camarasOp.php" target="ifrm1">
                        <div id="datos"  align="center">
                            <table>
                                <tr>
                                    <td align="right">Grabador de Video</td>
                                    <td>
                                        <select name="idservidor" id="idservidor" <?php echo $disabled;?>>
                                            <option value="">Seleccione</option>
                                            <?php
                                                echo $camara->makeCombo("vid_servidor vs, segmento s", "where vs.idsegmento = s.idsegmento and vs.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') order by s.segmento,vs.idservidor", "vs.idservidor", "s.segmento,vs.idservidor", $cam->idservidor, true);
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <div id="tabs">
                                <ul >
                                    <li><a href="#conexion">Conexi&oacute;n</a></li>
                                    <li><a href="#opcionesffmpeg">Opciones FFMPEG</a></li>
                                    <li><a href="#ubicacion">Ubicaci&oacute;n</a></li>
                                </ul>
                                <div id="conexion" >
                                    <table>
                                        <tr title="Nombre de la C&aacute;mara">
                                            <td align="right">C&aacute;mara:</td>
                                            <td><input type="text" name="camara" value="<?php echo $cam->camara;?>" <?php echo $disabled;?> title="Nombre de la C&aacute;mara"></td>
                                        </tr>
                                        <tr title="Tipo de C&aacute;mara">
                                            <td align="right">Tipo C&aacute;mara:</td>
                                            <td>
                                                <select name="idtipo_camara" id="idtipo_camara" <?php echo $disabled;?>>
                                                    <option value="">Seleccione</option>
                                                    <?php
                                                        echo $camara->makeCombo("vid_modelo_camara mc,vid_marca_camara mac, vid_tipo_camara tc ", "where mc.idmarca_camara = mac.idmarca_camara and mc.idmodelo_camara = tc.idmodelo_camara order by mac.nombre_marca_camara,mc.nombre_modelo_camara,tc.nombre_tipo_camara", "tc.idtipo_camara", "mac.nombre_marca_camara,mc.nombre_modelo_camara,tc.nombre_tipo_camara", $cam->idtipo_camara, true);
                                                    ?>
                                                </select>

                                            </td>
                                        </tr>
                                        <tr title="Rotaci&oacute;n">
                                            <td align="right">Rotaci&oacute;n:</td>
                                            <td>
                                                <select name="rotacion" id="rotacion" <?php echo $disabled;?>>
                                                    <option value="0">0&deg;</option>
                                                    <option value="90">90&deg;</option>
                                                    <option value="180">180&deg;</option>
                                                    <option value="270">270&deg;</option>
                                                </select>
                                                <br>
                                                <small class="comment">Posici&oacute;n de la imagen respecto a su base</small>
                                            </td>
                                        </tr>
                                        <tr title="N&uacute;mero de C&aacute;mara">
                                            <td align="right">N&uacute;mero de C&aacute;mara:</td>
                                            <td><select name="numero"  <?php echo $disabled;?> title="N&uacute;mero de C&aacute;mara">
                                                    <?php
                                                    for ($i=1;$i<=99;$i++){
                                                        echo "<option value=\"".$i."\" ".$camara->busca_valor($i, $cam->numero).">".$i."</option>";
                                                    }
                                                    ?>

                                                </select>
                                            </td>
                                        </tr>
                                        <tr title="Prioridad de monitoreo">
                                            <td align="right">Prioritaria:</td>
                                            <td>
                                                <input type="checkbox" name="prioridad" id="prioridad" value="1" <?php echo $disabled; echo $cam->prioridad == 1 ? " checked":""; ?> >
                                            </td>
                                        </tr>
                                        <tr title="Direcci&oacute;n IP">
                                            <td align="right">Direcci&oacute;n IP:</td>
                                            <td>
                                                <input type="text" name="ipv4" value="<?php echo $cam->ipv4;?>" <?php echo $disabled;?> title="Direcci&oacute;n IP">
                                                :
                                                <input type="text" name="puerto" maxlength="6" size="6" value="<?php echo $cam->puerto?$cam->puerto:"80";?>" <?php echo $disabled;?> title="Puerto">
                                            </td>
                                        </tr>
                                        <tr title="Usuario Administrador de la C&aacute;mara">
                                            <td align="right">Usuario Administrador:</td>
                                            <td><input type="text" name="cam_usuario" value="<?php echo $cam->cam_usuario;?>" <?php echo $disabled;?> title="Usuario Administrador de la C&aacute;mara"></td>
                                        </tr>
                                        <tr title="Clave Administrador de la C&aacute;mara">
                                            <td align="right">Clave Administrador:</td>
                                            <td><input type="password" name="cam_clave" value="<?php echo $cam->cam_clave;?>" <?php echo $disabled;?> title="Clave Administrador de la C&aacute;mara"></td>
                                        </tr>
                                        <tr title="Usuario de Video">
                                            <td align="right">Usuario de Video:</td>
                                            <td><input type="text" name="cam_usuario_video" value="<?php echo $cam->cam_usuario_video;?>" <?php echo $disabled;?> title="Usuario de Video"></td>
                                        </tr>
                                        <tr title="Clave de Video">
                                            <td align="right">Clave de Video:</td>
                                            <td><input type="password" name="cam_clave_video" value="<?php echo $cam->cam_clave_video;?>" <?php echo $disabled;?> title="Clave de Video"></td>
                                        </tr>
                                    </table>
                                    <br /> &nbsp;
                                    <?php
                                    if ($_REQUEST["accion"] != "agregar"  && Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"horarioAlr")){
                                            ?>
                                            <input type="button" value="Horario de Alarmas" onclick="$('#horarioDialog').dialog('open');" >
                                            <?php
                                        }
                                        echo "&nbsp;&nbsp;";
                                        if (($_REQUEST["accion"] == "modificar") && (strtolower($cam->tipologia)=="axis214ipptz") && (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"presetPos"))){
                                            ?>
                                            <input type="button" value="Posiciones Predefinidas" onclick="$('#presetPosDialog').dialog('open');" >
                                            <?php
                                        }
                                    ?>

                                </div>
                                <div id="opcionesffmpeg">
                                    <table>
                                        <tr>
                                            <td align="right">Crear video usando FFMPEG para videos por lapsos </td>
                                            <td>
                                                <input type="text" name="ffmpeg_timelapse" value="<?php echo $cam->ffmpeg_timelapse?$cam->ffmpeg_timelapse:"0";?>" size="2" maxlength="2" title="Usar FFMPEG para codificar videos por lapsos de tiempo (timelapse movie). Valor 0 (cero) es el valor por defecto para indicar que no se produciran esta clase de videos. Un vvalor distinto de cero, digamos 2 indica que se hara un video con frames tomado cada 2 segundos." <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">Generaci&oacute;n de archivos de video de lapsos de tiempo</td>
                                            <td>
                                            <select name="ffmpeg_timelapse_mode" title="Modo de generaci&oacute;n de los archivos de video por lapsos de tiempo" <?php echo $disabled;?>>
                                                <option value="hourly" <?php echo $camara->busca_valor($cam->ffmpeg_timelapse_mode, "hourly")?>>Cada Hora</option>
                                                <option value="daily" <?php echo $camara->busca_valor($cam->ffmpeg_timelapse_mode, "daily")?>>Diario</option>
                                                <option value="weekly-sunday" <?php echo $camara->busca_valor($cam->ffmpeg_timelapse_mode, "weekly-sunday")?>>Todos los Domingos</option>
                                                <option value="weekly-monday" <?php echo $camara->busca_valor($cam->ffmpeg_timelapse_mode, "weekly-monday")?>>Todos los Lunes</option>
                                                <option value="monthly" <?php echo $camara->busca_valor($cam->ffmpeg_timelapse_mode, "monthly")?>>Mensual</option>
                                                <option value="manual" <?php echo $camara->busca_valor($cam->ffmpeg_timelapse_mode, "manual")?>>Manual</option>
                                            </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="ubicacion">
                                    <table >
                                        <tr title="Pa&iacute;s">
                                            <td align="right">Pa&iacute;s:</td>
                                            <td>
                                                <input type="text" name="pais" size="50" maxlength="100" title="Pa&iacute;s" value="<?php echo $cam->pais;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Estado">
                                            <td align="right">Estado:</td>
                                            <td>
                                                <input type="text" name="estado" size="50" maxlength="100" title="Estado"  value="<?php echo $cam->estado;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Ciudad">
                                            <td align="right">Ciudad:</td>
                                            <td>
                                                <input type="text" name="ciudad" size="50" maxlength="100" title="Ciudad" value="<?php echo $cam->ciudad;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Avenida">
                                            <td align="right">Avenida:</td>
                                            <td>
                                                <input type="text" name="avenida" size="50" maxlength="100" title="Avenida" value="<?php echo $cam->avenida;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Edificio">
                                            <td align="right">Edificio:</td>
                                            <td>
                                                <input type="text" name="edificio" size="50" maxlength="100" title="Edificio" value="<?php echo $cam->edificio;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Piso">
                                            <td align="right">Piso:</td>
                                            <td>
                                                <input type="text" name="piso" size="50" maxlength="100" title="Piso" value="<?php echo $cam->piso;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Oficina">
                                            <td align="right">Oficina:</td>
                                            <td>
                                                <input type="text" name="oficina" size="50" maxlength="100" title="Oficina" value="<?php echo $cam->oficina;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <?php
                                        if ($_REQUEST["accion"] != "agregar"){
                                            ?>
                                            <tr>
                                                <td align="right" valign="top">Zonas:</td>
                                                <td>
                                                    <select name="idzona[]" id="idzona" style="width: 500px;" multiple <?php echo $disabled;?> size="8" >
                                                        <?php
                                                            echo $camara->makeCombo("vid_zona", "order by zona", "idzona", "zona,descripcion", $zonas,false);
                                                        ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" valign="top">Planta F&iacute;sica:</td>
                                                <td id="getPlantaFisica"></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <?php
                        if ($_REQUEST["accion"] != "agregar"){
                            ?>
                            <div id="horario">
                                <div id="horarioDialog" title="Horario de Alarmas C&aacute;mara <?php echo $cam->camara?>">
                                    <iframe name="ifrmH" id="ifrmH" frameborder="0" width="550px" height="200px" src=""></iframe>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div id="presetPosDialog" title="Posiciones Predefinidas C&aacute;mara <?php echo $cam->nombre_marca_camara." ".$cam->nombre_modelo_camara." ".$cam->nombre_tipo_camara?>">
                            <iframe name="presetPosFrame" id="presetPosFrame" width="390" height="460" frameborder="0" ></iframe>
                        </div>
                        <div id="botones" >
                            <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                            <input type="hidden" name="idcamara" id="idcamara" value="<?php echo $cam->idcamara ?>">
                            <input type="hidden" name="tipologia" id="tipologia" value="<?php echo strtolower($cam->tipologia) ?>">
                                <?php
                                if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                    echo "<input type=\"submit\" id=\"boton\" value=\"".ucfirst($_REQUEST["accion"])." C&aacute;mara de Video\" onclick=\"$('#cargando',parent.document).fadeIn('slow');\"></input>";
                                }

                                ?>

                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>        
        </body>
        <script type="text/javascript" language="javascript">
            $('body').ready(function(){
                //$('#ubicacion').css('display','none');
            });
            <?php
            /*$(function() {
                $("#idzona input[type='checkbox']").click(function(){
                    // -------------- VALORES SELECCIONADOS --------------------//
                    var allVals = [];
                    $('#idzona :checked').each(function() {
                        allVals.push($(this).val());
                    });
                    //----------------------------------------------------------//
                    var botonValue = $("#boton").val(); // VALOR BOTON SUBMIT
                    $.ajax({
                        beforeSend: function(){
                            // --------------- BOTON SUBMIT ---------------------//
                            $("#boton").val("Asociando Zonas...");
                            $("#boton").attr("disabled",true);
                        },
                        type: "POST",
                        url: "camarasOp.php",
                        data: 'idcamara='+$('#idcamara').val()+'&idzona='+allVals+'&accion=ajaxZonaCamara',
                        success: function(){
                            // --------------- MENSAJE DE EXITO ------------------//
                            $('#divmensaje').css('border','2px solid green');
                            $('#divmensaje').html('La zona se asoció exitosamente...');
                            setTimeout("$('#divmensaje').html('')",3000);
                            setTimeout("$('#divmensaje').css('border','');",3000);

                            // --------------- BOTON SUBMIT ---------------------//
                            $("#boton").val(botonValue);
                            $("#boton").attr("disabled",false);
                        }
                    });
                });
            });*/
            // ------------ FUNCION JS ASOCIA ZONAS A CAMARAS POR AJAX AL DARLE CLICK A UNA DE ELLAS -------- //
            if ($_REQUEST["accion"] != "visualizar" && $_REQUEST["accion"] != "agregar"){
                echo "$(function() {\n";
                echo "  $(\"#idzona \").click(function(){\n";
                            // -------------- VALORES SELECCIONADOS --------------------//
                echo "      var allVals = [];\n";
                echo "      $('#idzona :selected').each(function() {\n";
                echo "          allVals.push($(this).val());\n";
                echo "      });\n";
                            //----------------------------------------------------------//
                echo "      var botonValue = $('#boton').val(); // VALOR BOTON SUBMIT\n";
                echo "      $.ajax({\n";
                echo "          beforeSend: function(){\n";
                echo "              var text = 'Asociando Zonas...';\n";
                echo "              $('#boton').val(text);\n";
                echo "              $('#boton').attr('disabled',true);\n";
                echo "              $('#getPlantaFisica').html('<div class=\"savedItems\">'+text+'</div>');\n";
                echo "          },\n";
                echo "          type: 'POST',\n";
                echo "          url: 'camarasOp.php',\n";
                echo "          data: 'idcamara='+$('#idcamara').val()+'&idzona='+allVals+'&accion=ajaxZonaCamara',\n";
                echo "          success: function(){\n";
                                    // --------------- MENSAJE DE EXITO ------------------//
                echo "              var str;\n";
                echo "              str = '<div class=\"ui-widget\">';\n";
                echo "              str += '    <div class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';\n";
                echo "              str += '        <p>';\n";
                echo "              str += '            <span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>';\n";
                echo "              str += '                La zona se asoci&oacute; exitosamente...';\n";
                echo "              str += '        </p>';\n";
                echo "              str += '    </div>';\n";
                echo "              str += '</div>';\n";
                echo "              \n";                
                //echo "              $('#divmensaje').css('border','2px solid green');\n";
                echo "              $('#divmensaje').html(str);\n";
                echo "              setTimeout(\"$('#divmensaje').html('')\",3000);\n";
                echo "              setTimeout(\"$('#divmensaje').css('border','');\",3000);\n";
                echo "              \n";
                                    // --------------- BOTON SUBMIT ---------------------//
                echo "              $('#boton').val(botonValue);\n";
                echo "              $('#boton').attr('disabled',false);\n";
                echo "              getPlantaFisica();\n";
                echo "          }\n";                
                echo "      });\n";
                echo "  });\n";
                echo "});\n";
            }
            // ------- FUNCION JS DESABILITA EL LISTADO DE ZONAS CUANDO SE VISUALISAN LOS DATOS DE LAS CAMARAS -------- //
            if ($_REQUEST["accion"] == "visualizar"){
                echo "$(function() {";
                echo "  $(\"#idzona \").attr(\"disabled\",true);";
                echo "});";
            }
            ?>
            
        </script>
    </html>
<?php
}
?>