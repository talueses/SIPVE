<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de planta fisica de zonas asociadas a una camara
 * @author David Concepcion 13-12-2010 CENIT-DIDI
 */
session_start(); // start up your PHP session!
//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//================== INSTANCIACION DE CLASES ==================//
require_once "../controller/camaras_vid_camara.control.php"; // Class CONTROLLER

$data = CamaraPlantaFisica::getPlantaFisicaZonaCamara($_REQUEST["idcamara"]);
echo "<div class=\"savedItems\">\n";
if (count($data)>0){    
    foreach ($data as $row){
        
        $checked  = "";

        if ($_REQUEST["accion"]=="visualizar"){ // no se visualizan plantas fisicas no asociadas
            $disabled = "disabled";
        }
        if ($_REQUEST["accion"]=="visualizar" && $_REQUEST["idplantafisica"] == $row->idplantafisica){  // solo se visualizan plantas fisicas asociadas
            $disabled = "readonly";
        }
        if ($_REQUEST["idplantafisica"] == $row->idplantafisica){ // se chequean plantas fisicas asociadas
            $checked = "checked";            
        }
        
        echo "<div><input type=\"radio\" name=\"idplantafisica\" id=\"idplantafisica\" value=\"".$row->idplantafisica."\" onclick=\"setImage(this.value)\" ".$checked." ".$disabled.">".$row->plantafisica."</div>\n";
    }
}
if (count($data)<=0){
    echo "<div class=\"error\">No hay registros</div>\n";
}
echo "</div>\n";
?>