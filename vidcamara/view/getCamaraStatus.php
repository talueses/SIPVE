<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Obtencion de estado de ejecucion de proceso de captura de alarmas de las camaras de video
 * @author David Concepcion CENIT-DIDI
 */
session_start(); // start up your PHP session!
require_once "../controller/camaras_vid_camara.control.php";

//echo "<div align=\"left\"><pre>".print_r($_REQUEST,true)."</pre></div>";

$monitor = new ControlCamara();

$status = $monitor->getMotionStatus();

if ($status){
    echo "<img border=\"0\" src=\"../images/Video-Camera-Ok-32x15.png\" alt=\"Detecci&oacute;n de alarmas Activada\" title=\"Detecci&oacute;n de alarmas Activada\" >";
}
if (!$status){
    echo "<img border=\"0\" src=\"../images/Video-Camera-Error-32x15.png\" alt=\"Detecci&oacute;n de alarmas Desactivada\" title=\"Detecci&oacute;n de alarmas Desactivada\" >";
}
?>
