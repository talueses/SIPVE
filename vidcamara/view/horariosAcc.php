<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar, eliminar y visualizar horarios de ejecucion de eventos de una camara
 * @author David Concepcion 16-09-2010 CENIT * 
 */
session_start(); // start up your PHP session!

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/camaras_vid_camara.control.php"; // Class CONTROLLER

$horarios = CamaraHorario::getCamaraHorarios($_REQUEST["idcamara"]);
//echo "<div align='left'><pre>".print_r($horarios,true)."</pre></div>";
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/jquery.timeentry.js"></script>
        <style type="text/css" >
            @import "../css/jquery.timeentry.css";
            #tabla{
                float:left;
                width:30%;
                height:95%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 0px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                display: none;
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
            .trOp {
                display: none;
            }
            .trOp td{
                background:#dd0;
                padding: 10px 0px 10px 0px;
            }
            .timeRange {
                font-size: 11px;
                color: #000000;
            }
            .timeRangeList{
                font-size: 11px;
            }
            .img {
                cursor: pointer;
            }

            #divmensaje{
                width:99%;
                position: absolute;
                top: -15px;
                opacity:0.9;
                cursor: pointer;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();
                $('#divmensaje').live({
                    click: function() {
                        $(this).fadeOut(500);
                    }
                });                
            });
            // --- INSTACIA PLUGIN JQUERY TIME ENTRY --- //
            $(function () {
                $('.timeRange').timeEntry({
                    beforeShow: customRange,
                    show24Hours: true,
                    spinnerImage: ''
                });
            });
            function customRange(input) {
                return {minTime: (input.id == 'timeTo' ?
                    $('#timeFrom').timeEntry('getTime') : null),
                    maxTime: (input.id == 'timeFrom' ?
                    $('#timeTo').timeEntry('getTime') : null)};
            }
            
            // --- MOSTRAR U OCULTAR LA FILA DE OPERACION QUE CONTIENE CAMPOS A AGREGAR O MODIFICAR --- //
            var op = "show";
            function verHorariosAdd(){
                $("#imgCheckmark").unbind('click').click(function(){
                    setHorarios('agregar','');
                });
                // ---------------------- RESET MENSAJE -----------------//
                $('#divmensaje').css('border','');
                $('#divmensaje').html('');
                $('#divmensaje').hide();
                // ---------------------- RESET ROW OP ------------------//
                $("#timeFrom").val('');
                $("#timeTo").val('');
                $(".dias").each(function() {
                    $(this).attr('checked',false);
                });
                // ---------------------- SHOW ROW OP -------------------//
                if (op == "show"){
                    op = "hide";
                    $('#imgOp').attr('src','../images/sub_black_minus-20.png');
                    $('#imgOp').attr('title','Ocultar');
                    $('.trOp').fadeIn('slow');
                    return false;
                }                
                // ---------------------- HIDE ROW OP ------------------//
                if (op == "hide"){
                    op = "show";
                    $('#imgOp').attr('src','../images/sub_black_add-20.png');
                    $('#imgOp').attr('title','Mostrar');
                    $('.trOp').fadeOut('slow');
                    return false;
                }
                
                
            }

            // --- ESTABLECE LOS VALORES DEL ITEM SELECCIONADO PARA EDITAR DEL LISTADO A LOS CAMPOS DE LA FILA OPERACION --- //
            function setDataRowOp(obj){
                 <?php if ($_REQUEST["accion"] != "modificar"){ echo "return false;";}?>
                verHorariosAdd()
                op = "show";
                //--------------------------------------//
                var timeFromList = '#timeFrom-'+obj.id
                var timeToList = '#timeTo-'+obj.id
                $("#timeFrom").val($(timeFromList).val());
                $("#timeTo").val($(timeToList).val());

                                
                var objOp= {};
                $(".dias").each(function() {
                    objOp = this;
                    $(objOp).attr('checked',false);
                    $(".diasList-"+obj.id+":checked").each(function(){
                        if ($(this).val()==$(objOp).val()){
                            $(objOp).attr('checked',true);
                        }                        
                    });
                });
                $("#imgCheckmark").unbind('click').bind({
                    click: function(){
                        setHorarios('modificar',obj.id);
                    }
                });

            }
            
            function omover(obj){                            
                $('#'+obj.id).attr('class','pasada');
            }
            function omout(obj){
                $('#'+obj.id).attr('class','');
            }

            // --- AGREGA, MODIFICA O ELIMINA POR AJAX --- //
            function setHorarios(accion,idcamarahorario){
                var allVals = [];
                $(".dias:checked").each(function() {
                    allVals.push($(this).val());
                });
                var botonValue = $("#boton", parent.document).val(); // VALOR BOTON SUBMIT
                $.ajax({
                    beforeSend: function(){
                        // --------------- BOTON SUBMIT ---------------------//
                        $("#boton", parent.document).val("Agregando horario...");
                        $("#boton", parent.document).attr("disabled",true);
                        $('#divmensaje').hide();
                    },
                    type: "POST",
                    url: "horariosOp.php",
                    data: 'idcamara='+$('#idcamara').val()+'&timeFrom='+$('#timeFrom').val()+'&timeTo='+$('#timeTo').val()+'&dias='+allVals+'&idcamarahorario='+idcamarahorario+'&accion='+accion,
                    success: function(html){
                        //$('#res').html(html);

                        var res = jQuery.trim(html);
                        res = res.split("|");                        
                        
                        // --------------- BOTON SUBMIT ---------------------//
                        $("#boton", parent.document).val(botonValue);
                        $("#boton", parent.document).attr("disabled",false);

                        mensaje = res[1];

                        str = '<div class=\"ui-widget\">';
                        if (res[0]=="true"){
                            str += '    <div class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';
                            str += '        <p>';
                            str += '            <span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>';
                            str += '                '+mensaje;
                            str += '        </p>';
                            str += '    </div>';
                        }
                        if (res[0]=="false"){
                            str += '    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';
                            str += '        <p>';
                            str += '            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>';
                            str += '                '+mensaje;
                            str += '        </p>';
                            str += '    </div>';
                        }
                        str += '</div>';

                        $('#divmensaje').html(str).fadeIn(500); // mensaje a mostrar      
                        
                        setTimeout(function(){
                            document.location.reload();
                        }, 2000);                                      
                        
                    }
                });
            }

            // --- OBTIENE EL ESTADO DE EJECUCION DE PROCESO DE CAPTURA DE ALARMAS DE UNA CAMARA --- //
            function ajaxGetCamaraStatus(){
                $.ajax({
                    type: "POST",
                    url: "getCamaraStatus.php",
                    data: "idcamara=<?php echo $_REQUEST["idcamara"]?>&idservidor=<?php echo $_REQUEST["idservidor"]?>",
                    success: function(html){
                        $("#camaraStatus").html(html);
                    }
                });
                setTimeout("ajaxGetCamaraStatus();", 10*1000);
            }

            $(function(){
               ajaxGetCamaraStatus();
            });
                       
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"horarioAlr")){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="divmensaje" ></div>
                <table class="tabla" id="listado" align="center" border="1" >
                    <tr>
                        <th align="center" width="44%"><div id="camaraStatus" style="float:left;"></div>Horas</th>
                        <th align="center" width="7%" title="Lunes">L</th>
                        <th align="center" width="7%" title="Martes">Mar</th>
                        <th align="center" width="7%" title="Mi&eacute;rcoles">Mier</th>
                        <th align="center" width="7%" title="Jueves">J</th>
                        <th align="center" width="7%" title="Viernes">V</th>
                        <th align="center" width="7%" title="S&aacute;bado">S</th>
                        <th align="center" width="7%" title="Domingo">D</th>
                        <?
                        if ($_REQUEST["accion"] == "modificar"){
                            echo "<th align=\"center\" width=\"7%\" >";
                            echo "  <img src=\"../images/sub_black_add-20.png\" id=\"imgOp\" class=\"img\" alt=\"\" title=\"Mostrar\" border=\"0\" onclick=\"verHorariosAdd();\">";
                            echo "</th>";
                        }
                        ?>

                    </tr>
                    <tr class="trOp" >
                        <td align="center">
                            <input type="text" name="timeFrom" id="timeFrom" class="timeRange" size="10" title="Hora Desde">=><input type="text" name="timeTo" id="timeTo" class="timeRange " size="10" title="Hora Hasta">
                        </td>
                        <td align="center" title="Lunes">
                            <input type="checkbox" name="dias" class="dias" id="lunes" value="1">
                        </td>
                        <td align="center" title="Martes">
                            <input type="checkbox" name="dias" class="dias" id="martes" value="2">
                        </td>
                        <td align="center" title="Mi&eacute;rcoles">
                            <input type="checkbox" name="dias" class="dias" id="miercoles" value="3">
                        </td>
                        <td align="center" title="Jueves">
                            <input type="checkbox" name="dias" class="dias" id="jueves" value="4">
                        </td>
                        <td align="center" title="Viernes">
                            <input type="checkbox" name="dias" class="dias" id="viernes" value="5">
                        </td>
                        <td align="center" title="S&aacute;bado">
                            <input type="checkbox" name="dias" class="dias" id="sabado" value="6">
                        </td>
                        <td align="center" title="Domingo">
                            <input type="checkbox" name="dias" class="dias" id="domingo" value="7">
                        </td>
                        <td align="center"  title="Agregar Horario">
                            <img src="../images/Checkmark-20.png" alt="" border="0" class="img" id="imgCheckmark" >
                        </td>

                    </tr>
                    <?php
                    if (count($horarios)==0){
                        ?>
                        <tr id="no_horario" onmouseover="omover(this)" onmouseout="omout(this)">
                            <td align="center" colspan="9">No se encontraron horarios</td>
                        </tr>
                        <?php
                    }
                    if (count($horarios)>0){

                        foreach ($horarios as $row){
                        ?>
                        <tr class="trList" id="<?php echo $row->idcamarahorario;?>" onclick="setDataRowOp(this)" onmouseover="omover(this)" onmouseout="omout(this)" >
                            <td align="center" title="Horario">
                                <input type="text" class="timeRange" id="timeFrom-<?php echo $row->idcamarahorario;?>" size="10" value="<?php echo ControlCamara::formatHorarioToView($row->hora_desde,"24");?>" disabled onchange="customRangeList(this,'<?php echo $row->idcamarahorario;?>')">
                                =>
                                <input type="text" class="timeRange" id="timeTo-<?php echo $row->idcamarahorario;?>" size="10" value="<?php echo ControlCamara::formatHorarioToView($row->hora_hasta,"24");?>" disabled onchange="customRangeList(this,'<?php echo $row->idcamarahorario;?>')">
                            </td>
                            <?php
                            // Titulos dinamicos de dias en el checkbox
                            $diasStr = array("1"=>"Lunes","2"=>"Martes","3"=>"Mi&eacute;rcoles","4"=>"Jueves","5"=>"Viernes","6"=>"S&aacute;bado","7"=>"Domingo",);
                            // ID dinamicos de dias en el checkbox
                            $diasId = array("1"=>"lunes","2"=>"martes","3"=>"miercoles","4"=>"jueves","5"=>"viernes","6"=>"sabado","7"=>"domingo",);

                            // ------ CREACION DE CHECKBOXES DINAMICOS DE LISTADO DE HORARIOS REGISTRADOS ------------//
                            for ($i=1;$i<=7;$i++){

                                // --- SE MARCAN LOS DIAS REGISTRADOS PARA EL HORARIO --- //
                                $checked = "";
                                $dias = explode(",", $row->dias);
                                foreach ($dias as $rowDias){
                                    if ($rowDias == $i)
                                        $checked = "checked";
                                }
                                // -------------------------------------------------------//

                                echo "<td align=\"center\" title=\"".$diasStr[$i]."\">";
                                echo "  <input type=\"checkbox\" class=\"diasList-".$row->idcamarahorario."\" id=\"".$diasId[$i]."-".$row->idcamarahorario."\" ".$checked." disabled value=\"".$i."\">";
                                echo "</td>";
                            }
                            // --------------------------------------------------------------------------------------//

                            if ($_REQUEST["accion"] == "modificar"){
                                ?>
                                <td align="center"  title="Eliminar Horario">
                                    <img src="../images/sub_black_delete-20.png" alt="eliminar" border="0" class="img"  onclick="setHorarios('eliminar','<?php echo $row->idcamarahorario;?>');">
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                        <?php
                        }
                    }
                    ?>
                </table>

                <input type="hidden" name="idcamara" id="idcamara" value="<?php echo $_REQUEST["idcamara"];?>">
                <div id="res"></div>
                <script type="text/javascript" language="javascript">
                    <?php
                    if ($_REQUEST["accion"] != "modificar"){
                        ?>
                        $(function() {
                            $('.trList').unbind('click');
                        });
                        <?php
                    }
                    ?>
                </script>
                <?php
            }
            ?>
        </div>
    </body>
</html>