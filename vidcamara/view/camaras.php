<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de cámaras
 * Instancias modulos de gestion: agregar, modificar, eliminar y visualizar datos
 * @author David Concepcion CENIT-DIDI
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/camaras_vid_camara.control.php";
$cam_listado = Camara::getCamaras();

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>C&aacute;maras de Video</title>
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >

            #tabla{
                float:left;
                width:30%;
                height:95%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
            /* Para que no se vean los Botones cuando se ejecute la accion*/
            .ui-button{
                position: inherit;
            }
        </style>

        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
            });
            function visualizar(fila){
                    document.f1.idcamara.value = fila.id;
                    document.f1.action = 'camarasAcc.php';
                    document.getElementById('accion').value = 'visualizar';
                    document.f1.submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    fila.className = 'chequeada';
            }

            function omover(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    fila.className = 'pasada';


            }
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    fila.className = "";
            }

            function Accion(acc){
                document.getElementById('accion').value = acc;
                document.f1.action = 'camarasAcc.php';
                
                if (acc == "modificar" || acc == "eliminar" || acc == "enviarArchivo"){
                    if (document.f1.idcamara.value==""){
                        alert("Debe seleccionar una camara");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("¿ Esta seguro que desea eliminar la camara  ?")){
                            return false;
                        }
                        $("#cargando").fadeIn("slow");
                        document.f1.action = 'camarasOp.php';
                    }
                    if (acc == "enviarArchivo"){
                        document.f1.action = 'camarasOp.php';
                        if (!confirm("\xbf Está seguro que desea enviar la configuración de la camara ?")){
                            return false;
                        }
                    }
                }
                
                document.f1.submit();
                return true;

            }
            function hideLoading(){
                $("#cargando").fadeOut("slow");
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;" >
        <div id="cargando" align="center" style="position: absolute;background-color:#000000;opacity:.7;width: 100%;height: 100%;vertical-align: middle;color: #FFFFFF;font-size: 20px;display: none">
            <br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<img src="../images/loading51.gif" border="0"><br>&nbsp;<b>Cargando...</b>
        </div>
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <div id="tabla" align="center">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                    echo Controller::$mensajePermisos;
                }else{
                    echo Buscador::getObjBuscador("idcamara", "idservidor,camara", "vid_camara", "order by idservidor,camara");
                    ?>
                    <table id="listado" >
                            <tr>
                                <td id="titulo" colspan="4" align="center"><b>C&aacute;maras de Video</b></td>
                            </tr>
                            <tr>
                                <th align="center">N&deg;</th>
                                <th>Servidor</th>
                                <th>C&aacute;mara</th>
                                <th title="N&uacute;mero de C&aacute;mara para un servidor de video">#</th>
                            </tr>
                            <?php
                            if (count($cam_listado) > 0){
                                foreach ($cam_listado as $key => $cam){?>
                                    <tr id="<?php echo $cam->idcamara?>" onclick="visualizar(this);oclic(this)" onmouseover="omover(this)" onmouseout="omout(this)">
                                        <td align="center"><b><?php echo paginationSQL::$start+$key+1;?></b></td>
                                        <td><?php echo $cam->idservidor?></td>
                                        <td><?php echo $cam->camara?></td>
                                        <td title="N&uacute;mero de C&aacute;mara" align="center"><?php echo $cam->numero?></td>

                                    </tr>
                                <?php
                                }
                            }else{
                                ?>
                                <tr>
                                    <td colspan="4">
                                        <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>


                    </table>
                    <?php echo paginationSQL::links();?>
                    <div id="botones">
                        <center>
                            <form name="f1" target="frm1" action="camarasAcc.php" method="POST">
                                <input type="hidden" name="idcamara" value="">
                                <input type="hidden" name="accion" id="accion" value="">
                                <?php
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"agregar")){
                                    ?><input type="submit" value="Agregar" onclick="return Accion('agregar')"  class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"modificar")){
                                    ?><input type="submit" value="Modificar" onclick="return Accion('modificar')" class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"eliminar")){
                                    ?><input type="button" value="Eliminar" onclick="return Accion('eliminar')" class="boton"><?php
                                }
                                ?>
                                <input type="submit" value="Ayuda" 	onclick="this.form.action='camarasAyuda.php'" 	class="boton">
                            </form>
                        </center>
                    </div>

                    <?php
                }
                ?>
            </div>
            <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                <iframe name="frm1" id="frm1" frameborder="0" scrolling="yes" style="width:100%; height:650px" src="camarasAcc.php?accion=agregar"></iframe>
            </div>
        </div>
        
    </body>
</html>
