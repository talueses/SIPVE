<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de horarios
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/vid_camara_axispresetpos.control.op.php";// Class CONTROLLER

$obj  = new VidCamaraAxispresetposs();
$obj->idcamara = $_REQUEST["idcamara"];
$dataAll = $obj->getCamaraAxispresetposs();
$data = array();
if (count($dataAll) > 0){
    foreach ($dataAll as $row){
        if ($row->idcamaraaxispresetpos){
            $data[] = (object) $row;
        }
    }
}
//echo "<div align='left'><pre>".print_r($dataAll,true)."</pre></div>";
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>VidCamaraAxispresetposs</title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #listado{
                width:92%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
            .imgCamaras{
                background:#fff;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();
            });
            function visualizar(fila){
                $('#idcamaraaxispresetpos').val(fila.id);
                //$('#accion').val('modificar');
                //$('#f1').attr('action','vid_camara_axispresetpos.Acc.php');
                //$('#f1').submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    $('#'+fila.id).attr("className",'chequeada');
            }

            function omover(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    //fila.className = 'pasada';
                    $('#'+fila.id).attr("className",'pasada');


            }
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    $('#'+fila.id).attr("className",'');
            }
            function Accion(acc,value){
                $('#idcamaraaxispresetpos').val(value);
                $('#accion').val(acc);
                $('#f1').attr('target','');
                $('#f1').attr('action','vid_camara_axispresetpos.Acc.php');

                if (acc == "modificar" || acc == "eliminar" ){
                    if ($('#idcamaraaxispresetpos').val()==""){
                        alert("Debe seleccionar un registro de la lista");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("\xbf Esta seguro que desea eliminar el registro ?")){
                            return false;
                        }
                        $('#f1').attr('action','vid_camara_axispresetpos.Op.php');
                    }
                }
                if (acc == "gotoPosition"){
                    $('#f1').attr('action','vid_camara_axispresetpos.Op.php');
                    $('#f1').attr('target','frm1');                    
                }

                $('#f1').submit();
                return true;
            }
        </script>
    </head>
    <body style="margin:0px;background: #fff">
        <div id="principal" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"presetPos")){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <table id="listado">
                        <tr>
                            <td colspan="3" align="center"><img class="imgCamaras" src="<?php echo $dataAll[0]->videoUrl?>" ></td>
                        </tr>
                        <tr>
                            <th width="80%">Descripci&oacute;n</th>
                            <th width="20%" >
                                <?php
                                $onClick = "return Accion('agregar')";
                                if ($dataAll[0]->guardRunning > 0){
                                    $onClick = "alert('Existe una guardia activa.\\nDebe detener la guardia para agregar una posición.')";
                                }
                                ?>
                                <img title="Agregar" src="../images/sub_black_add-20.png" onclick="<?php echo $onClick; ?>" style="cursor: pointer;">
                                <?php
                                if ($dataAll[0]->guardRunning > 0){
                                    ?>
                                    <img src="../images/Info-red-20.png" title="Existe una guardia activa. Debe detener la guardia para agregar una posición" onclick="alert('Existe una guardia activa.\nDebe detener la guardia para agregar una posición.')" style="cursor: help;"/>
                                    <?php
                                }
                                ?>

                            </th>

                        </tr>
                        <?php
                        //onclick="visualizar(this);oclic(this)"
                        if (count($data) > 0){
                            foreach ($data as $row){?>
                                <tr id="<?php echo $row->idcamaraaxispresetpos;?>" onclick="oclic(this);" onmouseover="omover(this)" onmouseout="omout(this)">
                                    <td><?php echo $row->presetname;?></td>
                                    <td align="center">
                                        <img title="Ir a la Posici&oacute;n: <?php echo $row->presetname;?>" src="../images/Target-20.png" onclick="$('#presetname').val('<?php echo $row->presetname;?>');return Accion('gotoPosition','<?php echo $row->idcamaraaxispresetpos;?>');" />
                                        <?php
                                        $onClick = "$('#presetname').val('".$row->presetname."');return Accion('eliminar','".$row->idcamaraaxispresetpos."');";
                                        if ($row->tourPos > 0){
                                            $onClick = "alert('La posición ".$row->presetname." no puede ser eliminada\\nExiste una o mas guardias asociadas.')";
                                        }
                                        ?>
                                        <img title="Eliminar" src="../images/sub_black_delete-20.png" onclick="<?php echo $onClick; ?>" />
                                    </td>
                                </tr>
                            <?php
                            }
                        }else{
                            ?>
                            <tr>
                                <td colspan="3">
                                    <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                </td>
                            </tr>

                            <?php
                        }
                        ?>

                </table>
                <div id="botones" >
                    <center>
                        <form name="f1" id="f1" action="vid_camara_axispresetpos.Acc.php" method="POST">
                            <input type="hidden" name="idcamaraaxispresetpos" id="idcamaraaxispresetpos" value="">
                            <input type="hidden" name="accion" id="accion">
                            <input type="hidden" name="presetname" id="presetname" >
                            <input type="hidden" name="idcamara" id="idcamara" value="<?php echo $_REQUEST["idcamara"]?>" />
                            <!--<input type="submit" value="Agregar" 	onclick="return Accion('agregar')" >
                            <input type="submit" value="Modificar"	onclick="return Accion('modificar')">
                            <input type="button" value="Eliminar"	onclick="return Accion('eliminar')">-->
                        </form>
                    </center>
                </div>


                <div id="detalle" style="">
                    <iframe name="frm1" id="frm1" frameborder="0" scrolling="no" width="0" height="0" ></iframe>
                </div>
                <?php
            }
            ?>
        </div>        
    </body>
</html>