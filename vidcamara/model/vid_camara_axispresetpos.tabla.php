<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class VidCamaraAxispresetpos {
    
    private $idcamaraaxispresetpos = null;
    private $idcamara = null;
    private $presetnbr = null;
    private $presetname = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcamaraaxispresetpos($idcamaraaxispresetpos){
        $this->idcamaraaxispresetpos = $idcamaraaxispresetpos;
    }
    public function getIdcamaraaxispresetpos(){
        return $this->idcamaraaxispresetpos;
    }
    public function setIdcamara($idcamara){
        $this->idcamara = $idcamara;
    }
    public function getIdcamara(){
        return $this->idcamara;
    }
    public function setPresetnbr($presetnbr){
        $this->presetnbr = $presetnbr;
    }
    public function getPresetnbr(){
        return $this->presetnbr;
    }
    public function setPresetname($presetname){
        $this->presetname = $presetname;
    }
    public function getPresetname(){
        return $this->presetname;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcamaraaxispresetpos !== null) && (trim($this->idcamaraaxispresetpos)!=='') ){
            $campos .= "idcamaraaxispresetpos,";
            $valores .= "'".$this->idcamaraaxispresetpos."',";
        }
        if(($this->idcamara !== null) && (trim($this->idcamara)!=='') ){
            $campos .= "idcamara,";
            $valores .= "'".$this->idcamara."',";
        }
        if(($this->presetnbr !== null) && (trim($this->presetnbr)!=='') ){
            $campos .= "presetnbr,";
            $valores .= "'".$this->presetnbr."',";
        }
        if(($this->presetname !== null) && (trim($this->presetname)!=='') ){
            $campos .= "presetname,";
            $valores .= "'".$this->presetname."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_camara_axispresetpos $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_camara_axispresetpos SET ";
        
        if(($this->idcamara !== null) && (trim($this->idcamara)!=='') ){
            $sql .= "idcamara = '".$this->idcamara."',";
        }
        if(($this->presetnbr !== null) && (trim($this->presetnbr)!=='') ){
            $sql .= "presetnbr = '".$this->presetnbr."',";
        }
        if(($this->presetname !== null) && (trim($this->presetname)!=='') ){
            $sql .= "presetname = '".$this->presetname."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcamaraaxispresetpos = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_camara_axispresetpos  WHERE idcamaraaxispresetpos = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidCamaraAxispresetposs
     * @return object Devuelve un registro como objeto
     */
    function  getVidCamaraAxispresetposs(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcamaraaxispresetposBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcamaraaxispresetposBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcamaraaxispresetpos in (".$in.")";
        }
        
        $sql = "select idcamaraaxispresetpos,idcamara,presetnbr,presetname from vid_camara_axispresetpos ".$arg." order by presetnbr,presetname";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidCamaraAxispresetpos
     * @param int $idcamaraaxispresetpos Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidCamaraAxispresetpos($idcamaraaxispresetpos){
        $sql = "SELECT idcamaraaxispresetpos,idcamara,presetnbr,presetname FROM vid_camara_axispresetpos WHERE idcamaraaxispresetpos = '".$idcamaraaxispresetpos."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>