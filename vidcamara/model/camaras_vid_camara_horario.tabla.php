<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Vid_camara_horario {
    
    private $idcamarahorario = null;
    private $idcamara = null;
    private $hora_desde = null;
    private $hora_hasta = null;
    private $dias = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcamarahorario($idcamarahorario){
        $this->idcamarahorario = $idcamarahorario;
    }
    public function getIdcamarahorario(){
        return $this->idcamarahorario;
    }
    public function setIdcamara($idcamara){
        $this->idcamara = $idcamara;
    }
    public function getIdcamara(){
        return $this->idcamara;
    }
    public function setHora_desde($hora_desde){
        $this->hora_desde = $hora_desde;
    }
    public function getHora_desde(){
        return $this->hora_desde;
    }
    public function setHora_hasta($hora_hasta){
        $this->hora_hasta = $hora_hasta;
    }
    public function getHora_hasta(){
        return $this->hora_hasta;
    }
    public function setDias($dias){
        $this->dias = $dias;
    }
    public function getDias(){
        return $this->dias;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcamarahorario !== null) && (trim($this->idcamarahorario)!=='') ){
            $campos .= "idcamarahorario,";
            $valores .= "'".$this->idcamarahorario."',";
        }
        if(($this->idcamara !== null) && (trim($this->idcamara)!=='') ){
            $campos .= "idcamara,";
            $valores .= "'".$this->idcamara."',";
        }
        if(($this->hora_desde !== null) && (trim($this->hora_desde)!=='') ){
            $campos .= "hora_desde,";
            $valores .= "'".$this->hora_desde."',";
        }
        if(($this->hora_hasta !== null) && (trim($this->hora_hasta)!=='') ){
            $campos .= "hora_hasta,";
            $valores .= "'".$this->hora_hasta."',";
        }
        if(($this->dias !== null) && (trim($this->dias)!=='') ){
            $campos .= "dias,";
            $valores .= "'".$this->dias."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_camara_horario $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_camara_horario SET ";
        
        if(($this->idcamara !== null) && (trim($this->idcamara)!=='') ){
            $sql .= "idcamara = '".$this->idcamara."',";
        }
        if(($this->hora_desde !== null) && (trim($this->hora_desde)!=='') ){
            $sql .= "hora_desde = '".$this->hora_desde."',";
        }
        if(($this->hora_hasta !== null) && (trim($this->hora_hasta)!=='') ){
            $sql .= "hora_hasta = '".$this->hora_hasta."',";
        }
        if(($this->dias !== null) && (trim($this->dias)!=='') ){
            $sql .= "dias = '".$this->dias."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcamarahorario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_camara_horario  WHERE idcamarahorario = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidCamaraHorarios
     * @return object Devuelve un registro como objeto
     */
    function  getVidCamaraHorarios(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcamarahorarioBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcamarahorarioBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcamarahorario in (".$in.")";
        }
        
        $sql = "select idcamarahorario,idcamara,hora_desde,hora_hasta,dias from vid_camara_horario ".$arg." order by hora_desde,hora_hasta";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidCamaraHorario
     * @param int $idcamarahorario Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidCamaraHorario($idcamarahorario){
        $sql = "SELECT idcamarahorario,idcamara,hora_desde,hora_hasta,dias FROM vid_camara_horario WHERE idcamarahorario = '".$idcamarahorario."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>