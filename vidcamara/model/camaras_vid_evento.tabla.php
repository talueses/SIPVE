<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Vid_evento {
    private $idevento = null;
    private $idservidor = null;
    private $servidor = null;
    private $camara = null;
    private $evento = null;
    private $descripcion = null;
    private $file = null;
    private $fecha = null;
    private $status = null;
    private $observacion = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdevento($idevento){
        $this->idevento = $idevento;
    }
    public function getIdevento(){
        return $this->idevento;
    }
    public function setIdservidor($idservidor){
        $this->idservidor = $idservidor;
    }
    public function getIdservidor(){
        return $this->idservidor;
    }
    public function setServidor($servidor){
        $this->servidor = $servidor;
    }
    public function getServidor(){
        return $this->servidor;
    }
    public function setCamara($camara){
        $this->camara = $camara;
    }
    public function getCamara(){
        return $this->camara;
    }
    public function setEvento($evento){
        $this->evento = $evento;
    }
    public function getEvento(){
        return $this->evento;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setFile($file){
        $this->file = $file;
    }
    public function getFile(){
        return $this->file;
    }
    public function setFecha($fecha){
        $this->fecha = $fecha;
    }
    public function getFecha(){
        return $this->fecha;
    }
    public function setStatus($status){
        $this->status = $status;
    }
    public function getStatus(){
        return $this->status;
    }
    public function setObservacion($observacion){
        $this->observacion = $observacion;
    }
    public function getObservacion(){
        return $this->observacion;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idevento !== null) && (trim($this->idevento)!=='') ){
            $campos .= "idevento,";
            $valores .= "'".$this->idevento."',";
        }
        if(($this->idservidor !== null) && (trim($this->idservidor)!=='') ){
            $campos .= "idservidor,";
            $valores .= "'".$this->idservidor."',";
        }
        if(($this->servidor !== null) && (trim($this->servidor)!=='') ){
            $campos .= "servidor,";
            $valores .= "'".$this->servidor."',";
        }
        if(($this->camara !== null) && (trim($this->camara)!=='') ){
            $campos .= "camara,";
            $valores .= "'".$this->camara."',";
        }
        if(($this->evento !== null) && (trim($this->evento)!=='') ){
            $campos .= "evento,";
            $valores .= "'".$this->evento."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $campos .= "descripcion,";
            $valores .= "'".$this->descripcion."',";
        }
        if(($this->file !== null) && (trim($this->file)!=='') ){
            $campos .= "file,";
            $valores .= "'".$this->file."',";
        }
        if(($this->fecha !== null) && (trim($this->fecha)!=='') ){
            $campos .= "fecha,";
            $valores .= "'".$this->fecha."',";
        }
        if(($this->status !== null) && (trim($this->status)!=='') ){
            $campos .= "status,";
            $valores .= "'".$this->status."',";
        }
        if(($this->observacion !== null) && (trim($this->observacion)!=='') ){
            $campos .= "observacion,";
            $valores .= "'".$this->observacion."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_evento $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_evento SET ";
        
        if(($this->idservidor !== null) && (trim($this->idservidor)!=='') ){
            $sql .= "idservidor = '".$this->idservidor."',";
        }
        else{
            $sql .= "idservidor = NULL,";
        }
        if(($this->servidor !== null) && (trim($this->servidor)!=='') ){
            $sql .= "servidor = '".$this->servidor."',";
        }
        if(($this->camara !== null) && (trim($this->camara)!=='') ){
            $sql .= "camara = '".$this->camara."',";
        }
        if(($this->evento !== null) && (trim($this->evento)!=='') ){
            $sql .= "evento = '".$this->evento."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $sql .= "descripcion = '".$this->descripcion."',";
        }
        if(($this->file !== null) && (trim($this->file)!=='') ){
            $sql .= "file = '".$this->file."',";
        }
        else{
            $sql .= "file = NULL,";
        }
        if(($this->fecha !== null) && (trim($this->fecha)!=='') ){
            $sql .= "fecha = '".$this->fecha."',";
        }
        if(($this->status !== null) && (trim($this->status)!=='') ){
            $sql .= "status = '".$this->status."',";
        }
        if(($this->observacion !== null) && (trim($this->observacion)!=='') ){
            $sql .= "observacion = '".$this->observacion."',";
        }
        else{
            $sql .= "observacion = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idevento = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_evento  WHERE idevento = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidEventos
     * @return object Devuelve un registro como objeto
     */
    function  getVidEventos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["ideventoBuscador"]){
            $in = null;
            foreach ($_REQUEST["ideventoBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idevento in (".$in.")";
        }
        
        $sql = "select idevento,idservidor,servidor,camara,evento,descripcion,file,fecha,status,observacion from vid_evento ".$arg." order by camara,evento";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidEvento
     * @param int $idevento Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidEvento($idevento){
        $sql = "SELECT idevento,idservidor,servidor,camara,evento,descripcion,file,fecha,status,observacion FROM vid_evento WHERE idevento = '".$idevento."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>