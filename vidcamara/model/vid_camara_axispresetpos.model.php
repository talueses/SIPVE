<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'vid_camara_axispresetpos.tabla.php';

/**
 * Clase VidCamaraAxispresetposs{}
 * @author David Concepcion CENIT-DIDI
 */
class VidCamaraAxispresetposs extends VidCamaraAxispresetpos{
    private $data = array();

    public function __set($name, $value)
    {
        //echo "Estableciendo '$name' igual a '$value'<br>";
        $this->data[$name]=$value;
    }
    public function __get($name)
    {
        //echo "Obteniendo '$name'-'".$this->data[$name]."'<br>";
        return $this->data[$name];
    }
    /**
     * Consulta de posiciones de una camara
     * @return object Devuelve un registro como objeto
     */
    function  getCamaraAxispresetposs(){        
        $sql = "select  vc.ipv4,
                        vc.puerto,
                        vtc.urlvideo,
                        concat('http://',case when COALESCE(cam_usuario_video,cam_clave_video) is null then '' else concat(cam_usuario_video,':',cam_clave_video,'@') end,vc.ipv4,':',vc.puerto,vtc.urlvideo,'?resolution=320x240') as \"videoUrl\",
                        vcpos.idcamaraaxispresetpos,
                        vcpos.idcamara,
                        vcpos.presetnbr,
                        vcpos.presetname,
                        (select count(idcamaraaxisguard) from vid_camara_axisguard vg where vg.idcamara = vc.idcamara and vg.running = 'yes') as \"guardRunning\",
                        (select count(idvidcamaraaxisguardtour) from vid_camara_axisguard_tour vt where vt.idcamaraaxispresetpos = vcpos.idcamaraaxispresetpos) as \"tourPos\"
                from vid_camara vc
                    left join vid_camara_axispresetpos vcpos on (vc.idcamara = vcpos.idcamara),
                     vid_tipo_camara vtc
                where vc.idcamara = '".$this->idcamara."' 
                  and vc.idtipo_camara = vtc.idtipo_camara
                    order by vcpos.presetnbr,vcpos.presetname";
        //echo "<div align='left'><pre>".print_r($sql,true)."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    /**
     * Consulta de configuracion de una camara
     * @param int $idcamaraaxispresetpos Codigo
     * @return object Devuelve registros como objeto
     */
    static function getCamaraConf($idcamara){        
        $sql = "select  vc.ipv4,
                        vc.puerto,
                        vtc.urlvideo,
                        concat('http://',case when COALESCE(cam_usuario_video,cam_clave_video) is null then '' else concat(cam_usuario_video,':',cam_clave_video,'@') end,vc.ipv4,':',vc.puerto,vtc.urlvideo,'?resolution=320x240') as \"videoUrl\",
                        vc.cam_usuario,
                        vc.cam_clave,
                        vc.rotacion,
                        vtc.ptz
                from vid_camara vc,
                     vid_tipo_camara vtc
                where vc.idcamara = '".$idcamara."' 
                  and vc.idtipo_camara = vtc.idtipo_camara";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    
    
}
?>
