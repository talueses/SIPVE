<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 16-09-2010 CENIT
 * Clase Camara{}
 *  => Herencia clase Vid_camara{}
 *     Metodos Obtencion de datos especificos
 *
 * Modificado por David Concepcion 21-09-2010 CENIT
 * getConfiguracionServidor()
 *  => Order by en consulta
 *
 * Modificado por David Concepcion 07-10-2010 CENIT
 * Metodos nuevos:
 *  => getZonaCamaras()
 *     getTimezoneServidor()
 *     setEvento()
 *
 */
require_once "camaras_vid_camara.tabla.php";         // Tabla: Camaras
require_once "camaras_vid_zona_camara.tabla.php";    // Tabla: Zonas de una camara
require_once "camaras_vid_camara_horario.tabla.php"; // Tabla: Horarios de una camara
require_once "camaras_vid_evento.tabla.php";         // Tabla: Eventos de una camara
/**
 * Clase Model: Camaras
 */
class Camara extends VidCamara{
    
    /**
     * Consulta de camaras 
     * Solo se listan las Camaras pertenecientes al segmento del usuario logueado
     * @return object Devuelve registros como objeto
     */
    static function getCamaras(){
        $ret = array();
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcamaraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcamaraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and vc.idcamara in (".$in.")";
        }
        
        $sql = "SELECT /*start*/ * /*end*/ FROM vid_camara vc, vid_servidor vs where vc.idservidor = vs.idservidor ".$arg." and vs.idsegmento in (select su.idsegmento from segmento_usuario su where su.usuario = '".$_SESSION["usuario"]."') order by vc.idservidor,vc.numero";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de una camara 
     * @param int $idcamara Codigo de la camara
     * @return object Devuelve un registro como objeto
     */
    static function get_Camara($idcamara){
        $sql = "SELECT vc.* ,
                       mac.nombre_marca_camara,
                       mc.nombre_modelo_camara,
                       tc.nombre_tipo_camara,
                       concat(mac.nombre_marca_camara,mc.nombre_modelo_camara,tc.nombre_tipo_camara)as tipologia
                FROM vid_camara vc,
                     vid_tipo_camara tc,
                     vid_modelo_camara mc,
                     vid_marca_camara mac
                WHERE idcamara='".$idcamara."'                    
                  and vc.idtipo_camara = tc.idtipo_camara
                  and tc.idmodelo_camara = mc.idmodelo_camara
                  and mc.idmarca_camara = mac.idmarca_camara";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }    

    /**
     * Consulta de datos basicos y configuracion  servidor de video con sus camaras asociadas
     * @param string $arg Codigo del servidor
     * @return object Devuelve registros como objeto
     */
    function getConfiguracionServidor($arg){
        //$sql = "SELECT * FROM vid_motionconf WHERE idservidor='".$idservidor."'";
        $sql = "SELECT vsc.*,
                       vsc.ipv4 as \"ipv4Servidor\",
                       vs.idtimezone,
                       vs.so_usuario,
                       vs.so_clave,
                       vc.idcamara,
                       vc.ipv4 as \"ipv4Camara\",
                       vc.puerto as \"puertoCamara\",
                       vc.rotacion,
                       vc.numero,
                       vc.camara,
                       vc.cam_usuario,
                       vc.cam_clave,
                       vc.ffmpeg_timelapse as \"ffmpeg_timelapseCamara\",
                       vc.ffmpeg_timelapse_mode as \"ffmpeg_timelapse_modeCamara\", 
                       tc.urlfoto,
                       tc.urlvideo,
                       tc.ptz
                FROM vid_servidor vs
                        left join vid_camara vc on (vs.idservidor = vc.idservidor ".$arg["idcamara"].")
                        left join vid_tipo_camara tc on (vc.idtipo_camara = tc.idtipo_camara ),
                     vid_motionconf vsc
                WHERE vs.idservidor = ".$arg["idservidor"]."
                and vs.idservidor = vsc.idservidor
                order by vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Instanciacion de metodo busca_valor() en clase Controller{} (../controller/camaras.controller.php)
     */
    function busca_valor($equal, $valor){
        require_once "../controller/camaras.controller.php";
        $obj = new Controller();
        return $obj->busca_valor($equal, $valor);
    }


    /**
     * Instanciacion de metodo makeCombo() en clase Controller{} (../controller/camaras.controller.php)
     */
    function makeCombo($tabla, $arg, $value, $descripcion, $equal, $group){
        require_once "../controller/camaras.controller.php";
        $obj = new Controller();
        return $obj->make_combo($tabla, $arg, $value, $descripcion, $equal, $group);
    }   
}

/**
 * Clase Model: Zonas de una camara
 * @author David Concepcion CENIT-DIDI
 */
class ZonasCamara extends Vid_zona_camara{
    /**
     * Consulta zonas de una camara
     * @param int $idcamara Codigo de la camara
     * @return object Devuelve registros como objeto
     */
    static function getZonaCamaras($idcamara){
        $sql = "SELECT * FROM vid_zona_camara WHERE idcamara='".$idcamara."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
}

/**
 * Clase Model: Horarios de una camara
 * @author David Concepcion 02-11-2010 CENIT-DIDI
 */
class CamaraHorario extends Vid_camara_horario{
    /**
     * Consulta de horarios de una camara una camara
     * @param int $idcamara Codigo de la camara
     * @return object Devuelve registros como objeto
     */
    static function getCamaraHorarios($idcamara){
        $sql = "SELECT * FROM vid_camara_horario WHERE idcamara='".$idcamara."' order by hora_desde,hora_hasta";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    
}
/**
 * Clase Model: Eventos de una camara
 * @author David Concepcion 02-11-2010 CENIT-DIDI
 */
class CamaraEvento extends Vid_evento{
    
    /**
     * Consulta de horarios registrador de una camara segun un evento disparado
     * @param string $idservidor Codigo del servidor de video
     * @param string $numero numero de la camara
     * @param int $dia Representación numérica del día de la semana
     * @return object Devuelve registros como objeto
     */
    function getCamaraHorariosEvento($idservidor,$numero,$dia){
        $sql = "select vch.hora_desde, vch.hora_hasta
                from vid_camara vc, vid_camara_horario vch
                where vc.idservidor = '".$idservidor."'
                  and vc.numero = '".$numero."'
                  and vc.idcamara = vch.idcamara
                  and vch.dias like '%".$dia."%'";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de configuracion de una camara segun un evento disparado
     * @param string $idservidor Codigo del servidor de video
     * @param string $numero numero de la camara
     * @return object Devuelve registros como objeto
     */
    function getCamaraConf($idservidor,$numero){
        $sql = "select *
                from vid_camara vc
                where vc.idservidor = '".$idservidor."'
                  and vc.numero = '".$numero."'";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de uso horario de un servidor
     * @param int $idservidor Codigo del servidor
     * @return object Devuelve registros como objeto
     */
    static function getTimezoneServidor($idservidor){

        $sql = "select tz.location, tz.standard_time from vid_servidor vs, timezone tz where vs.idservidor = '".$idservidor."' and vs.idtimezone = tz.idtimezone";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }

    /**
     * Consulta de camaras registradas y que pueden estar horarios asociados a las mismas para el dia actual date("N")
     * @return object Devuelve registros como objeto
     */
    function getCamarasHorariosDia(){
        $sql = "select vs.idservidor, 
                       vc.idcamara,
                       vs.ipv4 as \"ipv4Serv\",
                       vs.so_usuario,
                       vs.so_clave,
                       vc.numero,
                       vc.ffmpeg_timelapse,
                       vch.idcamarahorario,
                       vch.hora_desde,
                       vch.hora_hasta
                from vid_camara vc
                     left join vid_camara_horario vch on (vch.idcamara = vc.idcamara and vch.dias like '%".date("N")."%'),
                     vid_servidor vs
                where vc.idservidor = vs.idservidor
                  order by vs.idservidor, vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de camaras asociadas a un servidor ordenada por numero
     * @param string $idservidor Codigo del servidor
     * @return object Devuelve registros como objeto
     */
    static function getCamarasServidor($idservidor){
        $sql = "select vs.idservidor,
                       vc.idcamara,
                       vs.ipv4 as \"ipv4Serv\",
                       vs.so_usuario,
                       vs.so_clave,
                       vc.numero
                from vid_camara vc,
                     vid_servidor vs
                where vc.idservidor = '".$idservidor."'
                  and vc.idservidor = vs.idservidor
                  order by vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    public function getServidores() {
        $sql = "select vs.idservidor,
                       vs.ipv4 as \"ipv4Servidor\",
                       vs.so_usuario,
                       vs.so_clave
                from vid_servidor vs                
                  order by vs.ipv4";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
}

/**
 * Clase Model: Eventos de una camara
 * @author David Concepion 13-12-2010 CENIT-DIDI
 */
class CamaraPlantaFisica {

    /**
     * Consulta de Plantas fisicas de zonas asociadas a una camara
     * @param int $idcamara Codigo de la camara
     * @return object object Devuelve registros como objeto
     */
    static function getPlantaFisicaZonaCamara($idcamara){
        $sql = "select pf.*
                from vid_zona_camara vzc,
                     vid_plantafisica_zona vpz,
                     plantafisica pf
                where vzc.idcamara        = '".$idcamara."'
                  and vzc.idzona         = vpz.idzona
                  and vpz.idplantafisica = pf.idplantafisica
                    group by pf.idplantafisica
                    order by pf.plantafisica";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de Zonas (de una planta fisica) asociada a una camara
     * @param int $idcamara Codigo de la camara
     * @param int $idplantafisica Codigo de planta fisica
     * @return object object Devuelve registros como objeto
     */
    static function getZonasPlantaFisicaCamara($idcamara,$idplantafisica){
        $sql = "select vc.idcamara,
                       vc.camara,
                       vc.numero,
                       vc.idplantafisica as \"idplantafisicaCamara\",
                       vc.\"leftX\" as \"leftXCamara\",
                       vc.\"topY\" as \"topYCamara\",
                       vz.idzona,
                       vz.zona,
                       vz.descripcion,
                       vpz.\"leftX\" as \"leftXZona\",
                       vpz.\"topY\" as \"topYZona\",
                       vpz.width,
                       vpz.height,
                       vpz.bgcolor,
                       pf.plantafisica,
                       pf.file
                from vid_camara vc,
                     vid_zona_camara vzc,
                     vid_zona vz,
                     vid_plantafisica_zona vpz,
                     plantafisica pf
                where vc.idcamara = '".$idcamara."'
                  and vc.idcamara = vzc.idcamara
                  and vzc.idzona  = vz.idzona
                  and vz.idzona   = vpz.idzona
                  and vpz.idplantafisica = pf.idplantafisica
                  and pf.idplantafisica = '".$idplantafisica."'";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
        
    }
}
?>