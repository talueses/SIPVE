<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class VidCamara {
    
    private $idcamara = null;
    private $idservidor = null;
    private $idtipo_camara = null;
    private $camara = null;
    private $numero = null;
    private $prioridad = null;
    private $ipv4 = null;
    private $puerto = null;
    private $cam_usuario = null;
    private $cam_clave = null;
    private $cam_usuario_video = null;
    private $cam_clave_video = null;
    private $pais = null;
    private $estado = null;
    private $ciudad = null;
    private $avenida = null;
    private $edificio = null;
    private $piso = null;
    private $oficina = null;
    private $idplantafisica = null;
    private $leftx = null;
    private $topy = null;
    private $rotacion = null;
    private $ffmpeg_timelapse = null;
    private $ffmpeg_timelapse_mode = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcamara($idcamara){
        $this->idcamara = $idcamara;
    }
    public function getIdcamara(){
        return $this->idcamara;
    }
    public function setIdservidor($idservidor){
        $this->idservidor = $idservidor;
    }
    public function getIdservidor(){
        return $this->idservidor;
    }
    public function setIdtipo_camara($idtipo_camara){
        $this->idtipo_camara = $idtipo_camara;
    }
    public function getIdtipo_camara(){
        return $this->idtipo_camara;
    }
    public function setCamara($camara){
        $this->camara = $camara;
    }
    public function getCamara(){
        return $this->camara;
    }
    public function setNumero($numero){
        $this->numero = $numero;
    }
    public function getNumero(){
        return $this->numero;
    }
    public function setPrioridad($prioridad){
        $this->prioridad = $prioridad;
    }
    public function getPrioridad(){
        return $this->prioridad;
    }
    public function setIpv4($ipv4){
        $this->ipv4 = $ipv4;
    }
    public function getIpv4(){
        return $this->ipv4;
    }
    public function setPuerto($puerto){
        $this->puerto = $puerto;
    }
    public function getPuerto(){
        return $this->puerto;
    }
    public function setCam_usuario($cam_usuario){
        $this->cam_usuario = $cam_usuario;
    }
    public function getCam_usuario(){
        return $this->cam_usuario;
    }
    public function setCam_clave($cam_clave){
        $this->cam_clave = $cam_clave;
    }
    public function getCam_clave(){
        return $this->cam_clave;
    }
    public function setCam_usuario_video($cam_usuario_video){
        $this->cam_usuario_video = $cam_usuario_video;
    }
    public function getCam_usuario_video(){
        return $this->cam_usuario_video;
    }
    public function setCam_clave_video($cam_clave_video){
        $this->cam_clave_video = $cam_clave_video;
    }
    public function getCam_clave_video(){
        return $this->cam_clave_video;
    }
    public function setPais($pais){
        $this->pais = $pais;
    }
    public function getPais(){
        return $this->pais;
    }
    public function setEstado($estado){
        $this->estado = $estado;
    }
    public function getEstado(){
        return $this->estado;
    }
    public function setCiudad($ciudad){
        $this->ciudad = $ciudad;
    }
    public function getCiudad(){
        return $this->ciudad;
    }
    public function setAvenida($avenida){
        $this->avenida = $avenida;
    }
    public function getAvenida(){
        return $this->avenida;
    }
    public function setEdificio($edificio){
        $this->edificio = $edificio;
    }
    public function getEdificio(){
        return $this->edificio;
    }
    public function setPiso($piso){
        $this->piso = $piso;
    }
    public function getPiso(){
        return $this->piso;
    }
    public function setOficina($oficina){
        $this->oficina = $oficina;
    }
    public function getOficina(){
        return $this->oficina;
    }
    public function setIdplantafisica($idplantafisica){
        $this->idplantafisica = $idplantafisica;
    }
    public function getIdplantafisica(){
        return $this->idplantafisica;
    }
    public function setLeftx($leftX){
        $this->leftX = $leftX;
    }
    public function getLeftx(){
        return $this->leftX;
    }
    public function setTopy($topY){
        $this->topY = $topY;
    }
    public function getTopy(){
        return $this->topY;
    }
    public function setRotacion($rotacion){
        $this->rotacion = $rotacion;
    }
    public function getRotacion(){
        return $this->rotacion;
    }
    public function setFfmpeg_timelapse($ffmpeg_timelapse){
        $this->ffmpeg_timelapse = $ffmpeg_timelapse;
    }
    public function getFfmpeg_timelapse(){
        return $this->ffmpeg_timelapse;
    }
    public function setFfmpeg_timelapse_mode($ffmpeg_timelapse_mode){
        $this->ffmpeg_timelapse_mode = $ffmpeg_timelapse_mode;
    }
    public function getFfmpeg_timelapse_mode(){
        return $this->ffmpeg_timelapse_mode;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcamara !== null) && (trim($this->idcamara)!=='') ){
            $campos .= "idcamara,";
            $valores .= "'".$this->idcamara."',";
        }
        if(($this->idservidor !== null) && (trim($this->idservidor)!=='') ){
            $campos .= "idservidor,";
            $valores .= "'".$this->idservidor."',";
        }
        if(($this->idtipo_camara !== null) && (trim($this->idtipo_camara)!=='') ){
            $campos .= "idtipo_camara,";
            $valores .= "'".$this->idtipo_camara."',";
        }
        if(($this->camara !== null) && (trim($this->camara)!=='') ){
            $campos .= "camara,";
            $valores .= "'".$this->camara."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $campos .= "numero,";
            $valores .= "'".$this->numero."',";
        }
        if(($this->prioridad !== null) && (trim($this->prioridad)!=='') ){
            $campos .= "prioridad,";
            $valores .= "'".$this->prioridad."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $campos .= "ipv4,";
            $valores .= "'".$this->ipv4."',";
        }
        if(($this->puerto !== null) && (trim($this->puerto)!=='') ){
            $campos .= "puerto,";
            $valores .= "'".$this->puerto."',";
        }
        if(($this->cam_usuario !== null) && (trim($this->cam_usuario)!=='') ){
            $campos .= "cam_usuario,";
            $valores .= "'".$this->cam_usuario."',";
        }
        if(($this->cam_clave !== null) && (trim($this->cam_clave)!=='') ){
            $campos .= "cam_clave,";
            $valores .= "'".$this->cam_clave."',";
        }
        if(($this->cam_usuario_video !== null) && (trim($this->cam_usuario_video)!=='') ){
            $campos .= "cam_usuario_video,";
            $valores .= "'".$this->cam_usuario_video."',";
        }
        if(($this->cam_clave_video !== null) && (trim($this->cam_clave_video)!=='') ){
            $campos .= "cam_clave_video,";
            $valores .= "'".$this->cam_clave_video."',";
        }
        if(($this->pais !== null) && (trim($this->pais)!=='') ){
            $campos .= "pais,";
            $valores .= "'".$this->pais."',";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $campos .= "estado,";
            $valores .= "'".$this->estado."',";
        }
        if(($this->ciudad !== null) && (trim($this->ciudad)!=='') ){
            $campos .= "ciudad,";
            $valores .= "'".$this->ciudad."',";
        }
        if(($this->avenida !== null) && (trim($this->avenida)!=='') ){
            $campos .= "avenida,";
            $valores .= "'".$this->avenida."',";
        }
        if(($this->edificio !== null) && (trim($this->edificio)!=='') ){
            $campos .= "edificio,";
            $valores .= "'".$this->edificio."',";
        }
        if(($this->piso !== null) && (trim($this->piso)!=='') ){
            $campos .= "piso,";
            $valores .= "'".$this->piso."',";
        }
        if(($this->oficina !== null) && (trim($this->oficina)!=='') ){
            $campos .= "oficina,";
            $valores .= "'".$this->oficina."',";
        }
        if(($this->idplantafisica !== null) && (trim($this->idplantafisica)!=='') ){
            $campos .= "idplantafisica,";
            $valores .= "'".$this->idplantafisica."',";
        }
        if(($this->leftX !== null) && (trim($this->leftX)!=='') ){
            $campos .= "\"leftX\",";
            $valores .= "'".$this->leftX."',";
        }
        if(($this->topY !== null) && (trim($this->topY)!=='') ){
            $campos .= "\"topY\",";
            $valores .= "'".$this->topY."',";
        }
        if(($this->rotacion !== null) && (trim($this->rotacion)!=='') ){
            $campos .= "rotacion,";
            $valores .= "'".$this->rotacion."',";
        }
        if(($this->ffmpeg_timelapse !== null) && (trim($this->ffmpeg_timelapse)!=='') ){
            $campos .= "ffmpeg_timelapse,";
            $valores .= "'".$this->ffmpeg_timelapse."',";
        }
        if(($this->ffmpeg_timelapse_mode !== null) && (trim($this->ffmpeg_timelapse_mode)!=='') ){
            $campos .= "ffmpeg_timelapse_mode,";
            $valores .= "'".$this->ffmpeg_timelapse_mode."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_camara $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_camara SET ";
        
        if(($this->idservidor !== null) && (trim($this->idservidor)!=='') ){
            $sql .= "idservidor = '".$this->idservidor."',";
        }
        if(($this->idtipo_camara !== null) && (trim($this->idtipo_camara)!=='') ){
            $sql .= "idtipo_camara = '".$this->idtipo_camara."',";
        }
        if(($this->camara !== null) && (trim($this->camara)!=='') ){
            $sql .= "camara = '".$this->camara."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $sql .= "numero = '".$this->numero."',";
        }
        if(($this->prioridad !== null) && (trim($this->prioridad)!=='') ){
            $sql .= "prioridad = '".$this->prioridad."',";
        }
        if(($this->ipv4 !== null) && (trim($this->ipv4)!=='') ){
            $sql .= "ipv4 = '".$this->ipv4."',";
        }
        if(($this->puerto !== null) && (trim($this->puerto)!=='') ){
            $sql .= "puerto = '".$this->puerto."',";
        }
        if(($this->cam_usuario !== null) && (trim($this->cam_usuario)!=='') ){
            $sql .= "cam_usuario = '".$this->cam_usuario."',";
        }
        else{
            $sql .= "cam_usuario = NULL,";
        }
        if(($this->cam_clave !== null) && (trim($this->cam_clave)!=='') ){
            $sql .= "cam_clave = '".$this->cam_clave."',";
        }
        else{
            $sql .= "cam_clave = NULL,";
        }
        if(($this->cam_usuario_video !== null) && (trim($this->cam_usuario_video)!=='') ){
            $sql .= "cam_usuario_video = '".$this->cam_usuario_video."',";
        }
        else{
            $sql .= "cam_usuario_video = NULL,";
        }
        if(($this->cam_clave_video !== null) && (trim($this->cam_clave_video)!=='') ){
            $sql .= "cam_clave_video = '".$this->cam_clave_video."',";
        }
        else{
            $sql .= "cam_clave_video = NULL,";
        }
        if(($this->pais !== null) && (trim($this->pais)!=='') ){
            $sql .= "pais = '".$this->pais."',";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $sql .= "estado = '".$this->estado."',";
        }
        if(($this->ciudad !== null) && (trim($this->ciudad)!=='') ){
            $sql .= "ciudad = '".$this->ciudad."',";
        }
        if(($this->avenida !== null) && (trim($this->avenida)!=='') ){
            $sql .= "avenida = '".$this->avenida."',";
        }
        if(($this->edificio !== null) && (trim($this->edificio)!=='') ){
            $sql .= "edificio = '".$this->edificio."',";
        }
        if(($this->piso !== null) && (trim($this->piso)!=='') ){
            $sql .= "piso = '".$this->piso."',";
        }
        if(($this->oficina !== null) && (trim($this->oficina)!=='') ){
            $sql .= "oficina = '".$this->oficina."',";
        }
        if(($this->idplantafisica !== null) && (trim($this->idplantafisica)!=='') ){
            $sql .= "idplantafisica = '".$this->idplantafisica."',";
        }
        else{
            $sql .= "idplantafisica = NULL,";
        }
        if(($this->leftX !== null) && (trim($this->leftX)!=='') ){
            $sql .= "\"leftX\" = '".$this->leftX."',";
        }
        else{
            $sql .= "\"leftX\" = NULL,";
        }
        if(($this->topY !== null) && (trim($this->topY)!=='') ){
            $sql .= "\"topY\" = '".$this->topY."',";
        }
        else{
            $sql .= "\"topY\" = NULL,";
        }
        if(($this->rotacion !== null) && (trim($this->rotacion)!=='') ){
            $sql .= "rotacion = '".$this->rotacion."',";
        }
        if(($this->ffmpeg_timelapse !== null) && (trim($this->ffmpeg_timelapse)!=='') ){
            $sql .= "ffmpeg_timelapse = '".$this->ffmpeg_timelapse."',";
        }
        if(($this->ffmpeg_timelapse_mode !== null) && (trim($this->ffmpeg_timelapse_mode)!=='') ){
            $sql .= "ffmpeg_timelapse_mode = '".$this->ffmpeg_timelapse_mode."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcamara = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_camara  WHERE idcamara = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidCamaras
     * @return object Devuelve un registro como objeto
     */
    function  getVidCamaras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcamaraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcamaraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcamara in (".$in.")";
        }
        
        $sql = "select idcamara,idservidor,idtipo_camara,camara,numero,prioridad,ipv4,puerto,cam_usuario,cam_clave,cam_usuario_video,cam_clave_video,pais,estado,ciudad,avenida,edificio,piso,oficina,idplantafisica,\"leftX\",\"topY\",rotacion,ffmpeg_timelapse,ffmpeg_timelapse_mode from vid_camara ".$arg." order by camara,numero";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidCamara
     * @param int $idcamara Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidCamara($idcamara){
        $sql = "SELECT idcamara,idservidor,idtipo_camara,camara,numero,prioridad,ipv4,puerto,cam_usuario,cam_clave,cam_usuario_video,cam_clave_video,pais,estado,ciudad,avenida,edificio,piso,oficina,idplantafisica,\"leftX\",\"topY\",rotacion,ffmpeg_timelapse,ffmpeg_timelapse_mode FROM vid_camara WHERE idcamara = '".$idcamara."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();
    }

}
?>