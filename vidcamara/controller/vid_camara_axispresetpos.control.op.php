<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDCAM"); //Categoria del modulo
require_once "../controller/vid_camara_axispresetpos.control.php";// Class CONTROL ControlVidCamaraAxispresetpos()
require_once "../../inicio/controller/camaraCommands.control.php";

/**
 * Description
 * @author David Concepcion
 */
class ControlOpVidCamaraAxispresetpos extends ControlVidCamaraAxispresetpos{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpVidCamaraAxispresetpos(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        
        
        $_REQUEST["presetnbr"]="999999";
        
        if(!$this->sonValidosDatos()) return false;
        
        if ($this->getAccion()=="agregar"){
            $obj = new CamaraCommands();
            
            // --- Agregar posicion en el dispositivo --- //
            $obj->option = "setPosition";
            $obj->value  = $_REQUEST["presetname"];
            $obj->axis214ipptz();
            $res = self::sendComandoCamara($_REQUEST["idcamara"]);
            
            // --- Consultar posiciones guardadas en el dispositivo --- //
            $obj->option = "getPosition";
            $obj->axis214ipptz();
            $res = self::sendComandoCamara($_REQUEST["idcamara"]);
            $aux = explode("\n", $res);
            
            foreach ($aux as $row){
                $regex = "/".$_REQUEST["presetname"]."/";
                $exito = preg_match($regex, $row);
                if ($exito){
                    $auxInt = explode("=", $row);
                    $_REQUEST["presetnbr"] =trim(preg_replace("/[A-Za-z]/", "", $auxInt[0]));
                    break;
                }
            }
            if (!trim($exito)){
                $this->mensaje = "Error al intentar guardar la posici&oacute;n en la c&aacute;mara";
                return false;
            }
        }
        //------------------ Metodo Set  -----------------//
        if(!$this->setVidCamaraAxispresetpos()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpVidCamaraAxispresetpos(){
        
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        // --- Eliminar posicion en el dispositivo --- //
        $obj = new CamaraCommands();
        $obj->option = "delPosition";
        $obj->value  = $_REQUEST["presetname"];
        $obj->axis214ipptz();
        $res = self::sendComandoCamara($_REQUEST["idcamara"]);
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarVidCamaraAxispresetpos()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Posici&oacute;n Predefinida");
        
        $this->setCampos("idcamara","C&aacute;mara");
        $this->setCampos("presetnbr","N&uacute;mero");
        $this->setCampos("presetname","Nombre");
        
    }
    
    function setComandoCamara(){
        $obj = new CamaraCommands();
        
        if ($this->getAccion()=="moveImg"){
            $obj->option = "moveImg";
            $obj->value  = $_REQUEST[$_REQUEST["idcamara"]."_x"].",".$_REQUEST[$_REQUEST["idcamara"]."_y"]."&imagewidth=320&imageheight=240";
        }
        if ($this->getAccion()=="gotoPosition"){
            $obj->option = "gotoPosition";
            $obj->value  = $_REQUEST["presetname"];
        }
        
        
        $obj->axis214ipptz();
        
        $response = self::sendComandoCamara($_REQUEST["idcamara"]);
        
    }
    
    
    /**
     * Envia comandos hacia una camara por httpRequest usando PHP-CURL
     * @param int $idcamara Codigo de la camara
     * @return string Devuelve la respuesta de la ejecucion del comando remoto 
     */
    static function sendComandoCamara($idcamara){        
        
        $param = VidCamaraAxispresetposs::getCamaraConf($idcamara);
        //echo "<div align='left'><pre>".print_r($param, true).CamaraCommands::$command."</pre></div>";
        $response = self::httpRequest($param->ipv4.":".$param->puerto.CamaraCommands::$command,$param->cam_usuario,$param->cam_clave);

        return $response;
    }
}
?>