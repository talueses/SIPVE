<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
include_once '../../inicio/controller/controller.php';
/*
 * Autor David Concepcion 16-09-2010 CENIT
 * Clase Controller{}
 *  => control de datos
 * Metodos set...() y get..()
 *  => parametros de metodos
 * Metodos sipFtp(), sipSsh(), escribirConfiguracion
 *  => ejecucion de comandos remotos
 * Metodos es...()
 *  => validacion de datos
 * Metodos busca_valor() y make_combo()
 *  => listas dinamicas
 *
 * Modificado por David Concepcion 21-09-2010 CENIT
 * escribirConfiguracion()
 *  => IP de servidor de aplicacion por parametros de archivos .conf
 *
 */

class ControllerLocal extends Controller {
    var $idservidor     = null;
    var $idcamara       = null;
    var $dataServer     = null;
    var $serverRemoto   = null;
    var $usuarioRemoto  = null;
    var $passwordRemoto = null;
    var $archivoSubir   = null;
    var $comandoRemoto  = null;
    var $conf           = null;
    var $contenido      = null;
    var $fileName       = null;

    public function setIdservidor($idservidor){
            $this->idservidor = $idservidor;
    }

    public function getIdservidor(){
             return $this->idservidor;
    }

    public function setIdcamara($idcamara){
            $this->idcamara = $idcamara;
    }

    public function getIdcamara(){
             return $this->idcamara;
    }

    public function setDataServer($dataServer){
            $this->dataServer = $dataServer;
    }

    public function getDataServer(){
             return $this->dataServer;
    }
    
    public function setServerRemoto($serverRemoto){
            $this->serverRemoto = $serverRemoto;
    }

    public function getServerRemoto(){
             return $this->serverRemoto;
    }

    public function setUsuarioRemoto($usuarioRemoto){
            $this->usuarioRemoto = $usuarioRemoto;
    }

    public function getUsuarioRemoto(){
             return $this->usuarioRemoto;
    }

    public function setPasswordRemoto($passwordRemoto){
            $this->passwordRemoto = $passwordRemoto;
    }

    public function getPasswordRemoto(){
             return $this->passwordRemoto;
    }

    public function setArchivoSubir($archivoSubir){
            $this->archivoSubir = $archivoSubir;
    }

    public function getArchivoSubir(){
             return $this->archivoSubir;
    }

    public function setComandoRemoto($comandoRemoto){
            $this->comandoRemoto = $comandoRemoto;
    }

    public function getComandoRemoto(){
             return $this->comandoRemoto;
    }

    public function setConf($conf){
            $this->conf = $conf;
    }

    public function getConf(){
             return $this->conf;
    }

    public function setContenido($contenido){
            $this->contenido = $contenido;
    }

    public function getContenido(){
             return $this->contenido;
    }

    public function setFileName($fileName){
            $this->fileName = $fileName;
    }

    public function getFileName(){
             return $this->fileName;
    }
    
    /*
     * Envia archivos al servidor mediante conexion ssh.
     * Debe estar instalado el paquete ssh
     */
    function sipFtp(){

        $server_remoto   = $this->getServerRemoto();
        $usuario_remoto  = $this->getUsuarioRemoto();
        $password_remoto = $this->getPasswordRemoto();
        $archivo_a_subir = $this->getArchivoSubir();
        if (!$server_remoto || !$usuario_remoto || !$password_remoto || !$archivo_a_subir){
            return false;
        }

        $script_folder = "../shell_script/";
        $script = "sip_ftp.py";

        if (!file_exists($script_folder.$script))
            return false;
            //return "El script $script_folder$script, para copia remota de archivos, no existe en el servidor...";

        
        if (!file_exists($archivo_a_subir))
            return false;
            //return "El archivo a subir: $archivos_a_subir no existe en el servidor...";

        $directorio_remoto = "/home/$usuario_remoto/motion";

        $command = $script_folder.$script;
        $parametros =" -s $server_remoto -d $directorio_remoto -u $usuario_remoto -p $password_remoto -f $archivo_a_subir";
        //echo $command.$parametros."<br>";
        $output = shell_exec($command.$parametros);

        return $output;
    }

    /*
     * Ejecuta comandos en el servidor mediante conexion ssh
     * Debe estar instalado el paquete ssh
     */
    function sipSsh(){

        $server_remoto   = $this->getServerRemoto();
        $usuario_remoto  = $this->getUsuarioRemoto();
        $password_remoto = $this->getPasswordRemoto();
        $comando_remoto  = $this->getComandoRemoto();
        if (!$server_remoto || !$usuario_remoto || !$password_remoto || !$comando_remoto){
            return false;
        }
        
        $script_folder = "../shell_script/";
        $script = "sip_ssh.py";

        if (!file_exists($script_folder.$script))
            return false;
            //"El script $script_folder$script, para la ejecucion remota de comandos, no existe en el servidor...";

        //EJEMPLO:  ./sip_ssh.py -s 150.186.193.179 -u simulab-5 -p cenit -c '/etc/init.d/motion stop'
        //$comando_remoto = "/etc/init.d/motion stop";
        //$comando_remoto = "\"motion -c ./motion/motion.conf\"";


        $command = $script_folder.$script;
        $parametros =" -s $server_remoto -u $usuario_remoto -p $password_remoto -c \"$comando_remoto\"";
        //echo $command.$parametros."<br>";
        $output = shell_exec($command.$parametros);

        return $output;
    }
    
    /*
     * Escribe en servidor de aplicacion el archivo de configuracion con datos guardados en bases de datos
     */
    function escribirConfiguracion(){
        
        $conf = $this->getConf();
        $contenido = $this->getContenido();
        $fileName = $this->getFileName();
        if (!$conf || !$contenido || !$fileName){
            return false;
        }
        
        foreach ($conf as $key => $value) {
            //echo $key . "--> ".$value."<br>";
            $contenido = str_replace("{".$key."}", $value, $contenido );
        }
        $contenido = str_replace("{SERVER_ADDR}", $_SERVER["SERVER_ADDR"], $contenido ); // IP ADDRESS APACHE SERVER
        $exito = file_put_contents('../motion/'.$fileName.'.conf', $contenido);
        return $exito;
    }
    
    /**
     * Eliminar archivo de configuracion con datos guardados en bases de datos
     */
    function eliminarConfiguracion(){
        $fileName = $this->getFileName();
        unlink('../motion/'.$fileName.'.conf');
    }
    
    /**
     * Formatea la hora militar a normal en formato srting 1800 => 06:00 p.m.
     * @param int(4) $hora hora a traducir
     * @param string $tipo tipo de horario: 12Hrs (a.m. o p.m.), 24Hrs (18:00)
     * @return string Devuelve la hora en formato hh:00 aa
     */
    static function formatHorarioToView($hora,$tipo){

        if (strlen($hora) == 3){
            $horas   = substr($hora, 0,1);
            $minutos = substr($hora, 1,2);
        }
        if (strlen($hora) == 4 || strlen($hora) == 6){
            $horas   = substr($hora, 0,2);
            $minutos = substr($hora, 2,2);
        }
        if (strlen($hora) == 6){
            $segundos = ":".substr($hora, 4,2);
        }
        if ($tipo=="24"){
            $hora = $horas.":".$minutos.$segundos;
        }
        if ($tipo=="12"){
            $am_pm = "AM";
            if ($horas>12){
                $horas -= 12;
                if ($horas < 10){
                    $horas = "0".$horas;
                }
                $am_pm = "PM";
            }
            $hora = $horas.":".$minutos.$segundos.$am_pm;
        }
        return $hora;
    }

    /**
     * Evalua el choque de horas entre dos horarios
     * @param int(4) $limiteInferiorAChequear Hora desde a comparar
     * @param int(4) $limiteSuperiorAChequear Hora hasta a comparar
     * @param int(4) $limiteInferiorExistente Hora desde con que se va acomparar
     * @param int(4) $limiteSuperiorExistente Hora hasta con que se va acomparar
     * @return boolean Devuelve true si hay choque de horario
     */
    static function choqueHorario ($limiteInferiorAChequear,$limiteSuperiorAChequear,$limiteInferiorExistente,$limiteSuperiorExistente){

        //------------------------------
        // Validamos que sean correctos
        //------------------------------

        if ($limiteInferiorAChequear > $limiteSuperiorAChequear) {
            //echo "1";
            return true;
        }
        if ($limiteInferiorExistente > $limiteSuperiorExistente) {
            //echo "2";
            return true;
        }

        //---------------------------------------------------------------
        // Verificamos primero los extremos
        //---------------------------------------------------------------

        if ($limiteSuperiorAChequear <=	$limiteInferiorExistente){
            //echo "caso extremo 1";
            return false;
        }
        if ($limiteInferiorAChequear >=	$limiteSuperiorExistente){
            //echo "caso extremo 2";
            return false;
        }

        //---------------------------------------------------------------
        // Dejamos fijo el limite inferior a chequear y variamos el limite
        // superior a chequear.
        //---------------------------------------------------------------

        if ($limiteInferiorAChequear >	$limiteInferiorExistente &&
            $limiteSuperiorAChequear == $limiteInferiorExistente){
            //echo "limite inferior caso 1";
            return false;
        }

        if ($limiteInferiorAChequear <=	$limiteInferiorExistente &&
            $limiteSuperiorAChequear > $limiteInferiorExistente){
            //echo "limite inferior caso 2";
            return true;
        }

        if ($limiteInferiorAChequear <=	$limiteInferiorExistente &&
            $limiteSuperiorAChequear == $limiteSuperiorExistente){
            //echo "limite inferior caso 3";
            return true;
        }

        if ($limiteInferiorAChequear <=	$limiteInferiorExistente &&
            $limiteSuperiorAChequear > $limiteSuperiorExistente){
            //echo "limite inferior caso 4";
            return true;
        }

        //---------------------------------------------------------------
        // Dejamos fijo el limite superior fijo y variamos el limite
        // inferior a chequear.
        //---------------------------------------------------------------

        if ($limiteSuperiorAChequear >	$limiteSuperiorExistente &&
            $limiteInferiorAChequear <= $limiteInferiorExistente){
            //echo "limite superior caso 1";
            return true;
        }

        if ($limiteSuperiorAChequear >= $limiteSuperiorExistente &&
            $limiteInferiorAChequear > $limiteInferiorExistente){
            //echo "limite superior caso 2";
            return true;
        }

        if ($limiteSuperiorAChequear >=	$limiteSuperiorExistente &&
            $limiteInferiorAChequear == $limiteSuperiorExistente){
            // echo "limite superior caso 3";
            return false;
        }

        if ($limiteSuperiorAChequear >=	$limiteSuperiorExistente &&
            $limiteInferiorAChequear > $limiteSuperiorExistente){
            //echo "limite superior caso 4";
            return false;
        }

        // Otros casos: limite inferior dentro del intervalo

        if ($limiteInferiorAChequear >=	$limiteInferiorExistente &&
            $limiteSuperiorAChequear >$limiteInferiorExistente){
            //echo "14";
            return true;
        }

        if ($limiteInferiorAChequear ==	$limiteSuperiorExistente &&
            $limiteSuperiorAChequear >$limiteSuperiorExistente){
            //echo "15";
            return false;
        }

        // Otros casos: limite superior dentro del intervalo

        if ($limiteSuperiorAChequear <	$limiteSuperiorExistente &&
            $limiteInferiorAChequear < $limiteInferiorExistente){
            //echo "16";
            return true;
        }

        if ($limiteSuperiorAChequear == $limiteInferiorExistente &&
            $limiteInferiorAChequear < $limiteInferiorExistente){
            //echo "17";
            return true;
        }else{
            // si hay otro caso no contemplado, regreso que hay choque
            //echo "18";
            return true;
        }
    }
    function formatHorarioToModel($hora){
        $horas   = substr($hora,0,2);
        $minutos = substr($hora,3,2);

        /*$meridian = substr($hora,5,2);
        if ($meridian=="PM"){
            $horas += 12;
        }*/
        return $horas.$minutos;
    }
    
}


?>

