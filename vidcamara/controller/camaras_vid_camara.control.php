<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDCAM"); //Categoria del modulo
require_once "../model/camaras_vid_camara.model.php";
require_once "camaras.controller.php";
/*
 * Autor David Concepcion 16-09-2010 CENIT
 * Clase ControlCamara{}
 *  => Herencia clase Controller{}
 *     Metodos de gestion de camaras
 * Metodos set...() y get..()
 *  => parametros de metodos
 * Metodos de validacion de datos
 * Se utiliza la clase Camara (../model/camaras_vid_camara.model.php) para ejecucion metodos de gestion y obtencion de datos
 *
 * Modificado por David Concepcion 21-09-2010 CENIT
 * Se agrego a los mensajes de $exito "Reiniciar el Servidor"
 *
 * Modificado por David Concepcion 07-10-2010 CENIT
 * Nuevo metodo e isntaciacion del mismo en otros metodos:
 *  => setZonas()
 * Nuevo campo prioridad
 * 
 */

class ControlCamara extends ControllerLocal{
    
    var $mensaje = null;
    var $accion  = null;

    public function setAccion($accion){
         $this->accion = $accion;
    }

    public function getAccion(){
         return $this->accion;
    }
   
    /**
     * === Eliminar camara ===
     * Se establecen las configuraciones en el servidor de video remoto setServer()
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarCamara(){
        
        $this->setIdcamara($_POST["idcamara"]);

        // ----------- ESTABLECER LAS CONFIGURACIONE EN EL SERVIDOR -----------//
        if (!$this->setServer()) return false;

        // ---------- ZONAS DE UNA CAMARA -----------//
        $this->setZonas();

        // -------------------- ELIMINAR CAMARA DE VIDEO ----------------------//
        $camara = new Camara();
        $camara->setIdcamara($_POST["idcamara"]);
        
        $exito = $camara->eliminarRegistro($camara->getIdcamara());
        if(!$exito){
            $this->mensaje = "La C&aacute;mara de Video no pudo ser eliminada: ".$camara->getMensaje();
            return false;
        }        
        
        $this->mensaje = "La C&aacute;mara de Video fué eliminada exitosamente...<br><b><i>Debe reiniciar el grabador de video</i></b>";
        return true;        
    }

    /**
     *  === Ejecucion de comandos remotos ===
     *  Configuraciones del servidor de video remoto
     *  Configuraciones de las camaras
     */
    function setServer(){
        
        $model = new Camara();

        if ($this->getIdcamara()){            
            $arg["idservidor"] = "(select idservidor from vid_camara where idcamara = '".$this->getIdcamara()."')";
            if ($this->getAccion()=="eliminar"){
                $arg["idcamara"] = " and vc.idcamara <> '".$this->getIdcamara()."'";
            }
        }
        if ($this->getIdservidor()){
            $arg["idservidor"] = "'".$this->getIdservidor()."'";
        }
        $this->setDataServer($model->getConfiguracionServidor($arg));

        if (!$this->setConfiguracionServidor()) return false;
        if (!$this->setConfiguracionCamaras()) return false;
        
        return true;
        
    }

    /**
     * === Gestion de camaras ===
     * Agregar y modificar
     * Se establecen las configuraciones en el servidor de video remoto setServer()
     */
    function setCamara(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->esValidoIdServidor()) return false;
        if(!$this->sonValidosParametrosConexion()) return false;
        if(!$this->sonValidosParametrosUbicacion()) return false;
        if(!$this->esValidoNumeroCamara($_POST["idservidor"],$_POST["numero"],$_POST["idcamara"])) return false;

        $this->setIdcamara($_POST["idcamara"]);
        $this->setIdservidor($_POST["idservidor"]);

        // --------------------  DATOS CONEXION ----------------------//
        
        $camara = new Camara();
        $camara->setIdservidor($_POST["idservidor"]);
        $camara->setIdcamara($_POST["idcamara"]);
        $camara->setCamara($_POST["camara"]);
        $camara->setIdtipo_camara($_POST["idtipo_camara"]);
        $camara->setRotacion($_POST["rotacion"]);
        $camara->setNumero($_POST["numero"]);
        $camara->setPrioridad($_POST["prioridad"]);
        $camara->setIpv4($_POST["ipv4"]);
        $camara->setPuerto($_POST["puerto"]);
        $camara->setCam_usuario($_POST["cam_usuario"]);
        $camara->setCam_clave($_POST["cam_clave"]);
        $camara->setCam_usuario_video($_REQUEST["cam_usuario_video"]);
        $camara->setCam_clave_video($_REQUEST["cam_clave_video"]);
        
        // ------------------ DATOS OPCIONES FFMPEG ------------------//
        
        $camara->setFfmpeg_timelapse($_POST["ffmpeg_timelapse"]);
        $camara->setFfmpeg_timelapse_mode($_POST["ffmpeg_timelapse_mode"]);
        
        // -------------------- DATOS UBICACION ----------------------//
        $camara->setPais(trim($_POST['pais']));
        $camara->setEstado(trim($_POST['estado']));
        $camara->setCiudad(trim($_POST['ciudad']));
        $camara->setAvenida(trim($_POST['avenida']));
        $camara->setEdificio(trim($_POST['edificio']));
        $camara->setPiso(trim($_POST['piso']));
        $camara->setOficina(trim($_POST['oficina']));

        

        if ($this->getAccion()=="agregar"){
            $exito = $camara->insertarRegistro();
        }
        if ($this->getAccion()=="modificar"){
            $exito = $camara->modificarRegistro($camara->getIdcamara());
        }
        
        // ---------- ZONAS DE UNA CAMARA -----------//
        //$this->setZonas();
        
        if(!$exito){
            if ($this->getAccion()=="agregar"){
                $this->mensaje = "La C&aacute;mara no pudo ser creada: ".$camara->getMensaje();
            }
            if ($this->getAccion()=="modificar"){
                $this->mensaje = "La C&aacute;mara no pudo ser modificada: ".$camara->getMensaje();
            }
            return false;
        }

        // ----------- ESTABLECER LAS CONFIGURACIONES EN EL SERVIDOR -----------//
        if (!$this->setServer()) return false;

        // ------------------------------- MENSAJE EXITO --------------------------------//
        if ($this->getAccion()=="agregar"){
            $this->mensaje = "La C&aacute;mara de video fu&eacute; creada exitosamente...<br><b><i>Debe reiniciar el grabador de video</i></b>";
        }
        if ($this->getAccion()=="modificar"){
            $this->mensaje = "La C&aacute;mara de video fu&eacute; modificada exitosamente...<br><b><i>Debe reiniciar el grabador de video</i></b>";
        }
        return true;
        // ----------------------------FIN MENSAJE EXITO --------------------------------//
    }
    
    /**
     * === Validacion IdServidor ===
     */
    function esValidoIdServidor(){
        $idservidor = (isset($_POST['idservidor']) && trim($_POST['idservidor'])!=='')? trim($_POST['idservidor']) : null;
        if($idservidor===null){
            $this->mensaje = "Falta seleccionar el Grabador de Video";
            return false;
        }
        return true;
    }

    /**
     * === Validacion Parametros Conexion ===
     */
    function sonValidosParametrosConexion(){
        //------------------------------------------------------------------------------------------------------------//
        $camara = (isset($_POST['camara']) && trim($_POST['camara'])!=='')? trim($_POST['camara']) : null;
        if($camara===null){
            $this->mensaje = "Falta introducir el nombre de la C&aacute;mara";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($camara)){
            $this->mensaje = "El nombre de la C&aacute;mara introducido no es v&aacute;lido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $idtipo_camara = (isset($_POST['idtipo_camara']) && trim($_POST['idtipo_camara'])!=='')? trim($_POST['idtipo_camara']) : null;
        if($idtipo_camara===null){
            $this->mensaje = "Falta seleccionar el tipo de C&aacute;mara";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $ipv4=	(isset($_POST['ipv4']) && trim($_POST['ipv4'])!=='')? trim($_POST['ipv4']) : null;
        if($ipv4===null){
            $this->mensaje = "Falta introducir la Dirección IP";
            return false;
        }
        if(!$this->esValidaIP($ipv4)){
            $this->mensaje = "La Dirección IP introducida no es valida";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $puerto = (isset($_POST['puerto']) && trim($_POST['puerto'])!=='')? trim($_POST['puerto']) : null;
        if($puerto===null){
            $this->mensaje = "Falta introducir el Puerto";
            return false;
        }
        if(!$this->esNumerico($puerto)){
            $this->mensaje = "El Puerto introducido no es valido";
            return false;
        }
        /*//------------------------------------------------------------------------------------------------------------//
        $cam_usuario=	(isset($_POST['cam_usuario']) && trim($_POST['cam_usuario'])!=='')? trim($_POST['cam_usuario']) : null;
        if($cam_usuario===null){
            $this->mensaje = "Falta introducir El Usuario de la C&aacute;mara.";
            return false;
        }
        if(!$this->esAlfaNumerico($cam_usuario)){
            $this->mensaje = "El Usuario de la C&aacute;mara tiene caracteres no v&aacute;lidos";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $cam_clave=	(isset($_POST['cam_clave']) && trim($_POST['cam_clave'])!=='')? trim($_POST['cam_clave']) : null;
        if($cam_clave===null){
            $this->mensaje = "Falta introducir la Clave del Usuario de la C&aacute;mara.";
            return false;
        }
        if(!$this->esAlfaNumerico($cam_clave)){
            $this->mensaje = "La Clave del Usuario de la C&aacute;mara tiene caracteres no v&aacute;lidos";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//*/
        return true;
    }

    /**
     * === Validacion Parametros Ubicacion ===
     */
    function sonValidosParametrosUbicacion(){
        $preMsj = "Debe introducir toda la informaci&oacute;n en la pestaña Ubicaci&oacute;n<br>";
        //------------------------------------------------------------------------------------------------------------//
        $pais = (isset($_POST['pais']) && trim($_POST['pais'])!=='')? trim($_POST['pais']) : null;
        if($pais===null){
            $this->mensaje = $preMsj."Falta introducir el Pa&iacute;s";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($pais)){
            $this->mensaje = "El nombre del Pa&iacute;s introducido no es v&aacute;lido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $estado = (isset($_POST['estado']) && trim($_POST['estado'])!=='')? trim($_POST['estado']) : null;
        if($estado===null){
            $this->mensaje = $preMsj."Falta introducir el Estado";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($estado)){
            $this->mensaje = "El nombre del Estado introducido no es v&aacute;lido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $ciudad = (isset($_POST['ciudad']) && trim($_POST['ciudad'])!=='')? trim($_POST['ciudad']) : null;
        if($ciudad===null){
            $this->mensaje = $preMsj."Falta introducir la Ciudad";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($ciudad)){
            $this->mensaje = "El nombre de la Ciudad introducida no es valido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $avenida = (isset($_POST['avenida']) && trim($_POST['avenida'])!=='')? trim($_POST['avenida']) : null;
        if($avenida===null){
            $this->mensaje = $preMsj."Falta introducir la Avenida";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($avenida)){
            $this->mensaje = "El nombre de la Avenida introducida no es valido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $edificio = (isset($_POST['edificio']) && trim($_POST['edificio'])!=='')? trim($_POST['edificio']) : null;
        if($edificio===null){
            $this->mensaje = $preMsj."Falta introducir el Edificio";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($edificio)){
            $this->mensaje = "El nombre del Edificio introducido no es v&aacute;lido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $piso = (isset($_POST['piso']) && trim($_POST['piso'])!=='')? trim($_POST['piso']) : null;
        if($piso===null){
            $this->mensaje = $preMsj."Falta introducir el Piso";
            return false;
        }
        if(!$this->esAlfaNumerico($piso)){
            $this->mensaje = "El nombre del Piso introducido no es v&aacute;lido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $oficina = (isset($_POST['oficina']) && trim($_POST['oficina'])!=='')? trim($_POST['oficina']) : null;
        if($oficina===null){
            $this->mensaje = $preMsj."Falta introducir la Oficina";
            return false;
        }
        if(!$this->esAlfaNumericoConEspacios($oficina)){
            $this->mensaje = "El nombre de la Oficina introducida no es valido";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        if (self::getAccion() != "agregar"){
            $idzona = (isset($_POST['idzona']) && $_POST['idzona']!=='')? $_POST['idzona'] : null;
            if($idzona===null){
                $this->mensaje = $preMsj."Falta seleccionar la(s) Zona(s)";
                return false;
            }
        }        
        //------------------------------------------------------------------------------------------------------------//
        return true;
    }

    /**
     * === Validacion numero unico de camara ===
     */
    function esValidoNumeroCamara($idservidor,$numero,$idcamara=false){
        $numero = (isset($numero) && trim($numero)!=='')? trim($numero) : null;
        if($numero===null){
            $this->mensaje = "Falta seleccionar el N&uacute;mero de la C&aacute;mara";
            return false;
        }
        if(!$this->esNumerico($numero)){
            $this->mensaje = "El N&uacute;mero de la C&aacute;mara seleccionado no es v&aacute;lido";
            return false;
        }
        if ($idcamara){
            $arg = " and idcamara <> '".$idcamara."'";
        }
        $sql = "SELECT idcamara FROM vid_camara WHERE idservidor = '".$idservidor."' and numero = '".$numero."' ".$arg;
        $res = DB_Class::DB_Query($sql);
        $nrows = $res->rowCount();
        if ($nrows > 0){
            $this->mensaje = "El N&uacute;mero de la C&aacute;mara seleccionado ya existe.";
            return false;
        }
        return true;
    }

    /**
     * === Configuracion de servidor de video remoto ===
     * Ejecucion de comandos en servidor remoto
     */
    function setConfiguracionServidor(){
        $this->setContenido(file_get_contents('../motion/motion_template.conf'));

        // --------------------- DATOS DEL SERVIDOR --------- ---------------------------//
        foreach ($this->getDataServer() as $key => $value) {
            
            $conf = (object) $value;

            //-------- SI LA CAMARA EXISTE SE ESTABLECEN LAS RUTAS DE LAS CAMARAS -------//
            if ($conf->idcamara){
                $numero = $conf->numero;
                if ($numero < 10){
                    $numero = "0".$numero;
                }
                $threads .= "thread /home/".$conf->so_usuario."/motion/thread".$numero.".conf\n";
            }            
        }
        $conf->threads = $threads;
        $this->setServerRemoto($conf->ipv4Servidor);
        $this->setUsuarioRemoto($conf->so_usuario);
        $this->setPasswordRemoto($conf->so_clave);
        $this->setConf($conf);
        //

        //echo "<div align='left'><pre>".print_r($conf, true)."</pre></div>";
        // ------------------FIN DATOS DEL SERVIDOR Y CAMARAS ---------------------------//
        
        // --------------------- GENERAR ARCHIVOS DE CONFIGURACION ----------------------//
        $this->setFileName("motion_".$conf->idservidor);
        $exito = $this->escribirConfiguracion();
        if (!$exito){
            $this->mensaje = "El archivo de configuraci&oacute;n del grabador de video no se a escrito";
            return false;
        }

        // --------------------- ENVIAR ARCHIVOS DE CONFIGURACION -----------------------//
        $this->setArchivoSubir("../motion/motion_".$conf->idservidor.".conf");
        $exito = $this->sipFtp();
        if ($exito){
            $this->mensaje = "El archivo de configuraci&oacute;n del grabador de video no se a enviado";
            return false;
        }

        // --------------------- COMANDOS REMOTOS ------------------------------------//
        $this->setComandoRemoto("mv ./motion/motion_".$conf->idservidor.".conf ./motion/motion.conf");
        $exito = $this->sipSsh();
        $this->eliminarConfiguracion(); // eliminar el archivo de configuracion generado local
        if ($exito){
            $this->mensaje = "El archivo de configuraci&oacute;n del grabador de video no se a renombrado";
            return false;
        }
        return true;

    }

    /**
     * === Configuracion de camaras ===
     * Ejecucion de comandos en servidor remoto
     */
    function setConfiguracionCamaras(){
        $this->setContenido(file_get_contents('../motion/thread_template.conf'));        
        
        foreach ($this->getDataServer() as $key => $value) {
            
            // -------------------- DATOS DE LAS CAMARAS ----- ---------------------------//
            $conf = (object) $value;            

            if ($conf->numero < 10){
                $conf->numero = "0".$conf->numero;
            }

            $this->setServerRemoto($conf->ipv4Servidor);
            $this->setUsuarioRemoto($conf->so_usuario);
            $this->setPasswordRemoto($conf->so_clave);
            $this->setConf($conf);
            
            if ($key == 0){ // se eliminan los archivos para ser creados nuevamente
                $this->setComandoRemoto("rm ./motion/thread*");
                $exito = $this->sipSsh();
                if ($exito){
                    $this->mensaje = "El archivo de configuraci&oacute;n de la c&aacute;mara <b>".$conf->camara." - ".$conf->numero."</b> no se a eliminado";
                    return false;
                }
            }
            
            //-------------------------------- EJECUCION DE METODOS SI LA CAMARA EXISTE ----------------------------------//
            if ($conf->idcamara){
                // --------------------- GENERAR ARCHIVOS DE CONFIGURACION ----------------------//
                $this->setFileName("thread".$conf->numero."_".$conf->idservidor);
                $exito = $this->escribirConfiguracion();
                if (!$exito){
                    $this->mensaje = "El archivo de configuraci&oacute;n de la c&aacute;mara <b>".$conf->camara." - ".$conf->numero."</b> no se a escrito";
                    return false;
                }

                // --------------------- ENVIAR ARCHIVOS DE CONFIGURACION -----------------------//
                $this->setArchivoSubir("../motion/thread".$conf->numero."_".$conf->idservidor.".conf");
                $exito = $this->sipFtp();
                if ($exito){
                    $this->mensaje = "El archivo de configuraci&oacute;n de la c&aacute;mara <b>".$conf->camara." - ".$conf->numero."</b> no se a enviado";
                    return false;
                }

                // --------------------- COMANDOS REMOTOS ---------------------------------------//
                $this->setComandoRemoto("mv ./motion/thread".$conf->numero."_".$conf->idservidor.".conf ./motion/thread".$conf->numero.".conf");
                $exito = $this->sipSsh();
                if ($exito){
                    $this->mensaje = "El archivo de configuraci&oacute;n de la c&aacute;mara <b>".$conf->camara." - ".$conf->numero."</b> no se a renombrado";
                    return false;
                }
                $this->setComandoRemoto("mkdir ./motion/cam".$conf->numero);
                $exito = $this->sipSsh();
                if ($exito){
                    $this->mensaje = "El directorio de la c&aacute;mara <b>".$conf->camara." - ".$conf->numero."</b> no se a creado";
                    return false;
                }
                $this->setComandoRemoto("mkdir ./motion/data");
                $exito = $this->sipSsh();
                if ($exito){
                    $this->mensaje = "El directorio para registro de datos de eventos de c&aacute;maras no se a creado";
                    return false;
                }
                $this->eliminarConfiguracion(); // eliminar el archivo de configuracion generado local
            }                        
        }
        // --------------------- REINICIAR SERVICIOS ------------------------------------//
        $this->setComandoRemoto("/etc/init.d/motion stop");
        $this->sipSsh();
        /*$this->setComandoRemoto("motion -c ./motion/motion.conf");
        $this->sipSsh();*/
        return true;
    }

    /**
     * === Zonas de una Camara ===
     * @return boolean Devuelve verdadero
     */
    function setZonas(){
        
        $zona = new ZonasCamara();
        
        if ($this->getAccion()=="agregar"){
            $_POST["idcamara"] = mysql_insert_id();
            //$_POST["idcamara"] = "(SELECT AUTO_INCREMENT-1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".DbLink::getDBname()."' AND TABLE_NAME='vid_camara')";
        }
        $zona->setIdcamara($_POST["idcamara"]);


        if ($this->getAccion()=="eliminar" || $this->getAccion()=="modificar"){
            $zona->eliminarRegistro($zona->getIdcamara());
        }
        if ($this->getAccion()=="agregar" || $this->getAccion()=="modificar"){
            foreach ($_POST['idzona'] as $idzona){
                $zona->setIdzona($idzona);
                $zona->insertarRegistro();
            }
        }
        return true;
    }       
    
    /**
     * === Agregar Horarios de ejecucion de eventos a una camara
     * @return boolean Devuelve verdadero si el proseco de insertar se ejecuta exitosamente
     */
    function addCamaraHorario(){
        if(!$this->esValidoHoras()) return false;
        if(!$this->esValidoDias()) return false;

        $horario = new CamaraHorario();        
        
        $horario->setIdcamara($_POST["idcamara"]);

        $horario->setHora_desde(self::formatHorarioToModel($_POST["timeFrom"]));
        $horario->setHora_hasta(self::formatHorarioToModel($_POST["timeTo"]));
        $horario->setDias($_POST["dias"]);
        
        //------------ VALIDAR CHOQUE DE HORARIOS ---------------------------//
        if(!$this->esValidoChoqueHorario($horario)) return false;
        
        $exito = $horario->insertarRegistro();
        
        if(!$exito){           
            $this->mensaje = "El horario pudo ser creado: ".$horario->getMensaje();
            return false;
        }

        $this->mensaje = "El horario fu&eacute; creado exitosamente";
        return true;
    }

    /**
     * Modificar un Horario de ejecucion de eventos a una camara
     * @return Devuelve verdadero si el proseco de modificar se ejecuta exitosamente
     */
    function modCamaraHorario(){
        if(!$this->esValidoHoras()) return false;
        if(!$this->esValidoDias()) return false;

        $horario = new CamaraHorario();

        $horario->setIdcamara($_POST["idcamara"]);
        $horario->setIdcamarahorario($_POST["idcamarahorario"]);
        $horario->setHora_desde(self::formatHorarioToModel($_POST["timeFrom"]));
        $horario->setHora_hasta(self::formatHorarioToModel($_POST["timeTo"]));
        $horario->setDias($_POST["dias"]);
        
        //------------ VALIDAR CHOQUE DE HORARIOS ---------------------------//
        if(!$this->esValidoChoqueHorario($horario)) return false;

        $exito = $horario->modificarRegistro($horario->getIdcamarahorario());

        if(!$exito){
            $this->mensaje = "El horario pudo ser modificado: ".$horario->getMensaje();
            return false;
        }

        $this->mensaje = "El horario fu&eacute; modificado exitosamente";
        return true;
        
        
    }

    /**
     * === Eliminar un Horario de ejecucion de eventos a una camara
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarCamaraHorario(){
        $horario = new CamaraHorario();
        $horario->setIdcamarahorario($_POST["idcamarahorario"]);
        $exito = $horario->eliminarRegistro($horario->getIdcamarahorario());
        if(!$exito){        
         
            $this->mensaje = "El horario pudo ser eliminado: ".$horario->getMensaje();         
            return false;
        }
        $this->mensaje = "El horario fu&eacute; eliminado exitosamente";
        return true;
    }
    /**
     * === Validacion de choque de horarios ===
     * @param object $horario Horario a chequear
     * @return boolean Devuelve falso si existe un choque de horario
     */
    function esValidoChoqueHorario($horario){
        
        $horariosExistentes = $horario->getCamaraHorarios($horario->getIdcamara());
        
        $limiteInferiorAChequear = $horario->getHora_desde();
        $limiteSuperiorAChequear = $horario->getHora_hasta();
        $diasAChequear           = explode(",", $horario->getDias());
        
        foreach ($diasAChequear as $diaAChequear){
            foreach ($horariosExistentes as $row){
                $diasExistente = explode(",", $row->dias);
                foreach ($diasExistente as $diaExistente){
                    
                    if ($diaAChequear == $diaExistente && $this->getAccion() == "agregar"){
                        $limiteInferiorExistente = $row->hora_desde;
                        $limiteSuperiorExistente = $row->hora_hasta;
                        if (self::choqueHorario($limiteInferiorAChequear,$limiteSuperiorAChequear,$limiteInferiorExistente,$limiteSuperiorExistente)){
                            $this->mensaje = "El Horario ya est&aacute; registrado o choca con uno existente";
                            return false;
                        }
                    }
                    if ($horario->getIdcamarahorario() != $row->idcamarahorario && $diaAChequear == $diaExistente && $this->getAccion() == "modificar"){
                        $limiteInferiorExistente = $row->hora_desde;
                        $limiteSuperiorExistente = $row->hora_hasta;
                        if (self::choqueHorario($limiteInferiorAChequear,$limiteSuperiorAChequear,$limiteInferiorExistente,$limiteSuperiorExistente)){
                            $this->mensaje = "El Horario ya est&aacute; registrado o choca con uno existente";
                            return false;
                        }
                    }                    
                }
            }
        }
        return true;
    }

    /**
     *  === Validacion Hora desde y hora hasta ===
     * @return boolean Devuelve verdadero si los datos de las horas estan correctos
     */
    function esValidoHoras(){
        //------------------------------------------------------------------------------------------------------------//
        $timeFrom = (isset($_POST['timeFrom']) && trim($_POST['timeFrom'])!=='')? trim($_POST['timeFrom']) : null;
        if($timeFrom===null){
            $this->mensaje = "Falta introducir la Hora Desde";
            return false;
        }
        //------------------------------------------------------------------------------------------------------------//
        $timeTo = (isset($_POST['timeTo']) && trim($_POST['timeTo'])!=='')? trim($_POST['timeTo']) : null;
        if($timeTo===null){
            $this->mensaje = "Falta introducir la Hora Hasta";
            return false;
        }
        if ($timeFrom == $timeTo){
            $this->mensaje = "La Hora Desde debe ser mayor a la Hora Hasta";
            return false;
        }
        return true;
    }

    /**
     *  === Validacion seleccion de dias ===
     * @return boolean Devuelve verdadero si los datos de los dias estan seleccionados
     */
    function esValidoDias(){
        //------------------------------------------------------------------------------------------------------------//
        $dias = (isset($_POST['dias']) && trim($_POST['dias'])!=='')? trim($_POST['dias']) : null;
        if($dias===null){
            $this->mensaje = "Falta seleccionar al menos un D&iacute;a";
            return false;
        }
        return true;
    }
    
    
    
    
    /**
     * === Registro de ejecucion de un evento capturado por MOTION ===
     * @return Boolean Devuelve falso si encuentra
     */
    public function setEvento(){  
        
        $obj = new CamaraEvento();
        
        // Nombre de archivo de log de error 
        $fileLog = "GET_Event_Motion_".date("Y-m-d").".txt";
        
        // -------- USO HORARIO DEL SERVIDOR DONDE SE EJECUTA EL EVENTO -------//
        $timezone = $obj->getTimezoneServidor($_REQUEST["idservidor"]);
        //echo $timezone->location." --- ".$timezone->standard_time;
        date_default_timezone_set($timezone->location);
        
        $data = $obj->getServidores();
        //echo "<div align='left'><pre>".print_r($data,true)."</pre></div>";
        
        // -------- Servidores registrados  -------//
        foreach ($data as $row){
            
            $this->setServerRemoto($row->ipv4Servidor);
            $this->setUsuarioRemoto($row->so_usuario);
            $this->setPasswordRemoto($row->so_clave);
            
            // Nombre de archivo de data a descargar desde el servidor
            $motionNewFile = "motion-".$row->idservidor."-".date("YmdHmi").".txt";
            
            // -------- Generacion de archivo a descargar en el server remoto  -------//
            $this->setComandoRemoto("mv /home/".$row->so_usuario."/motion/data/motion.txt /home/".$row->so_usuario."/motion/data/".$motionNewFile);
            $res = $this->sipSsh();
            if ($res){
                Controller::setLog($fileLog, "ERROR: creando nuevo archivo motion remoto \"".$motionNewFile."\" --- ".date("Y-m-d H:m:i"));
            }
            
            // -------- Descarga de arhivo desde el server remoto  -------//
            $cmd = "wget -q http://".$row->ipv4Servidor."/motion/data/".$motionNewFile." -O ".__DIR__."/../motion_events/".$motionNewFile;
            //echo "<div align='left'><pre>".$cmd."</pre></div>"; 
            $res = shell_exec($cmd);
            if ($res==null){
                Controller::setLog($fileLog, "ERROR: leyendo (wget) archivo motion remoto \"".$motionNewFile."\" --- ".date("Y-m-d H:m:i"));
            }
            
            // -------- Borrado de arhivo en el server remoto  -------//
            $this->setComandoRemoto("rm /home/".$row->so_usuario."/motion/data/".$motionNewFile);
            $res = $this->sipSsh();
            if ($res){
                Controller::setLog($fileLog, "ERROR: borrando archivo motion remoto \"".$motionNewFile."\" --- ".date("Y-m-d H:m:i"));
            }
            
            // -------- Listado de archivos descargados desde los servidores motion  -------//
            $dir = scandir("../motion_events/");
            //echo "<div align='left'><pre>".print_r($dir,true)."</pre></div>";
            foreach ($dir as $localFile) {
                
                // -------- Solo se toma en cuenta los archivos motion  -------//
                if (preg_match("/^motion/", $localFile)){
                    echo "<div align='left'><pre>".$localFile."</pre></div>";
                    
                    // -------- Lectura de contenido de archivo  -------//
                    $fileContent = file(__DIR__."/../motion_events/".$localFile);                    
                    foreach ($fileContent as $key =>$content) {
                        //echo "<div align='left'><pre>".$key." -_- ".$content."</pre></div>";
                        
                        list($descripcion,$idservidor,$camara,$evento,$fecha,$file) = explode("?", $content);
                        
                        //echo "<div align='left'><pre>descripcion=$descripcion, idservidor=$idservidor, camara=$camara, evento=$evento,fecha=$fecha, file=$file</pre></div>";
                        
                        
                        $obj->setIdservidor($idservidor);
                        $obj->setServidor($idservidor);
                        $obj->setCamara($camara);
                        $obj->setEvento($evento);
                        $obj->setDescripcion($descripcion);
                        $obj->setFile(trim($file));
                        $obj->setFecha($fecha);

                        // --------- CONFIRURACION DE UNA CAMARA ------------------------//
                        $camConf = $obj->getCamaraConf($obj->getIdservidor(), $obj->getCamara());

                        if ($camConf->ffmpeg_timelapse != "0"){
                            $obj->insertarRegistro();
                            //break;
                        }
                        if ($camConf->ffmpeg_timelapse == "0"){

                            $horas = $obj->getCamaraHorariosEvento($obj->getIdservidor(), $obj->getCamara(), date("N"));        

                            // SE EVALUA SI EL EVENTO DISPARADO ESTA DENTRO DEL ALGUN HORARIO CONFUGURADO DE LA CAMARA 
                            foreach ($horas as $row){
                                //echo "<div align='left'><pre>".print_r($row,true)."</pre></div>";
                                $limiteInferiorExistente = $row->hora_desde;
                                $limiteSuperiorExistente = $row->hora_hasta;

                                if (self::choqueHorario(date("Hi"),date("Hi"),$limiteInferiorExistente,$limiteSuperiorExistente)){
                                    //echo "registrar evento";
                                    $obj->insertarRegistro();
                                    break;
                                }
                            }
                        }                        
                    }
                    unlink(__DIR__."/../motion_events/".$localFile);                        
                }                
            }            
        }
        
    }

    /**
     * Ejecucion de comandos para activar y desactivar la deteccion de eventos en el servidor MOTION
     * @return string Devuelve respuesta de la ejecucion de APIs MOTION
     */
    static function setMotionStatus(){
        
        $obj = new CamaraEvento;
        $camarasHorarios = $obj->getCamarasHorariosDia();
        
        $nThread = 0;
        foreach ($camarasHorarios as $row){
            //echo "<div align='left'><pre>".print_r($row,true)."</pre></div>";
            
            // ---------- SET NUMBER OF THREAD ------------//
            if ($idservidor != $row->idservidor){
                $nThread = 0;
            }
            if ($idcamara != $row->idcamara){
                $nThread++;

                // ---------- PAUSA DETECCION DE MOVIMIENTO ------------//
                $url = "http://".$row->ipv4Serv.":18080/".$nThread."/detection/pause";
                //echo "<br>".$url
                $resp .= self::httpRequest($url, $row->so_usuario, $row->so_clave);
            }
            // -------END SET NUMBER OF THREAD ------------//           

            if ($row->idcamarahorario && $row->ffmpeg_timelapse=="0"){
                // -------- USO HORARIO DEL SERVIDOR DONDE SE EJECUTA EL EVENTO -------//
                $timezone = $obj->getTimezoneServidor($row->idservidor);
                //echo $timezone->location." --- ".$timezone->standard_time."<br><p>Hora Servidor ".$row->idservidor." => ".date("Hi")."</p>";
                date_default_timezone_set($timezone->location);
                // -------- FIN USO HORARIO... ----------------------------------------//
                
                // ------ VERIFICACION DE COINCIDENCIA DE HORAS CONFIGURADAS CON LA ACTUAL ----------//
                $limiteInferiorExistente = $row->hora_desde;
                $limiteSuperiorExistente = $row->hora_hasta;
                $choqueHorario = self::choqueHorario(date("Hi"),date("Hi"),$limiteInferiorExistente,$limiteSuperiorExistente);
                
                // --------------------- START DETECCION DE MOVIMIENTO ------------//
                if ($choqueHorario){  
                    $url = "http://".$row->ipv4Serv.":18080/".$nThread."/detection/start";
                    //echo "<br>".$url
                    $resp .= self::httpRequest($url, $row->so_usuario, $row->so_clave);
                }
            }
            $idservidor = $row->idservidor;
            $idcamara   = $row->idcamara;
        }
        return $resp;
    }
    
    /**
     * Verifica el estado de captura de eventos de una camara
     * @return boolean Devuelve verdadero si la camara esta activa y vis.
     */
    function getMotionStatus(){
        $camaras = CamaraEvento::getCamarasServidor($_REQUEST["idservidor"]);
        $nThread = 0;
        foreach ($camaras as $row){
            $nThread++;
            if ($row->idcamara == trim($_REQUEST["idcamara"])){
                // ---------- PAUSA DETECCION DE MOVIMIENTO ------------//
                $url = "http://".$row->ipv4Serv.":18080/".$nThread."/detection/status";
                //echo "<br>".$url;
                $resp = self::httpRequest($url, $row->so_usuario, $row->so_clave);
                break;
            }
        }
        //echo $resp;
        if (preg_match("/ACTIVE/", $resp)){
            return true;
        }
        if (!preg_match("/ACTIVE/", $resp)){
            return false;
        }
    }

    function setCamaraCoord(){
        $camara = new Camara();
        //--------------------- DATOS --------------------------//
        $camara->setIdcamara($_REQUEST["idcamara"]);
        $camara->setIdplantafisica($_REQUEST["idplantafisica"]);
        $camara->setLeftx($_REQUEST["leftX"]);
        $camara->setTopy($_REQUEST["topY"]);
        
        $exito = $camara->modificarRegistro($camara->getIdcamara());

        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            $this->mensaje = "La coordenada de la C&aacute;mara no pudo ser establecida: ".$zona->getMensaje();
            return false;
        }
        $this->mensaje = "La coordenada de la C&aacute;mara fu&eacute; establecida exitosamente...";
        return true;
    }
        
}
?>