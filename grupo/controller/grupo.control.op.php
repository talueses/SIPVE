<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "ADMGPR"); //Categoria del modulo
require_once "grupo.control.php";// Class CONTROL ControlGrupo()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpGrupo extends ControlGrupo{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpGrupo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        //------------------ Metodo Set  -----------------//
        if(!$this->setGrupo()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpGrupo(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarGrupo()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Grupo");
        
        $this->setCampos("nombre","Nombre");
        $this->setCampos("descripcion","Descripci&oacute;n");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"descripcion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

    /**
     * Agrega y elimina funciones de un grupo
     */
    function setGrupoFuncion(){

        // - Eliminar funciones asociadas al grupo de la categorias seleccionada -//
        Grupos::unsetFuncionesGrupo($_REQUEST["idgrupo"], $_REQUEST["idcategoria"]);
        
        $obj = new GrupoHasFuncion();

        $obj->setGrupo_idgrupo($_REQUEST["idgrupo"]);

//        $obj->eliminarRegistro($obj->getGrupo_idgrupo());

        $funciones = explode(",", $_REQUEST["funciones"]);

        foreach ($funciones as $idfuncion){
            $obj->setFuncion_id($idfuncion);

            $obj->insertarRegistro();
        }        
    }

    /**
     * Establece los datos para el listado de categorias - funciones
     * @var array $data Matriz que contiene las categorias y las funciones
     * @var array $catP Vector que contiene las categorias padres
     * @var array $catH Vector que contiene las categorias hijas
     * @var array $func Vector que contiene las funciones
     * @return array Devuelve la matriz $data con todos los datos
     */
    function loadList(){
        $data = array();
        $catP = array();
        $catH = array();
        $func = array();

        $data = Grupos::getGrupoFuncionLoaded($_REQUEST["idgrupo"]);
        
        if (count($data) > 0){
            foreach($data as $row){

                if ($idcatPadre != $row->idcatPadre){
                    $catP[] = (object) array("idcatPadre"=> $row->idcatPadre,
                                             "catPadre"  => $row->catPadre);
                }
                if ($idcatHijo != $row->idcatHijo){
                    $catH[] = (object) array("idcatPadre" => $row->idcatPadre,
                                             "idcatHijo"  => $row->idcatHijo,
                                             "catHijo"    => $row->catHijo,
                                             "mobil" => $row->mobil);
                }
                if ($idfuncion != $row->idfuncion){
                    $func[] = (object) array("idcatPadre"    => $row->idcatPadre,
                                             "idcatHijo"     => $row->idcatHijo,
                                             "idfuncion"     => $row->idfuncion,
                                             "nombreFuncion" => $row->nombreFuncion,
                                             "mobil" => $row->mobil,
                                             "grupoFuncion"  => $row->grupoFuncion);
                }
                $idcatPadre = $row->idcatPadre;
                $idcatHijo = $row->idcatHijo;
                $idfuncion = $row->idfuncion;
            }
            $data = (object) array("catP"=>$catP,"catH"=>$catH,"func"=>$func);
        }
        return $data;
    }
}
?>