<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/grupo.control.op.php";// Class CONTROLLER
$obj = new ControlOpGrupo();

$data = $obj->loadList();
//echo "<div align='left'><pre>".print_r($data,true)."</pre></div>";
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">

        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link rel="stylesheet" href="../css/jquery.treeview.css" >
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/jquery.cookie.js"></script>
        <script type="text/javascript" src="../js/jquery.treeview.js"></script>

        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            .contenido{
                width:97%;
                padding: 2px;
                border:  #aaaaaa solid 1px;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #controles{
                width: 100px;
                padding: 0px;
                position: absolute;
                opacity: 0.8;
                right: 25px;
                cursor: move;
            }
            .titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            #main { padding: 1em; }

            .funcionLoaded{
                background: #ffffa7;
                font-style: italic;
                padding: 3px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            img.mobil{
                margin-left: 5px; 
            }
            img.mobil:hover{
                width: 32px;
                height:  32px;
                position: absolute
            }
        </style>

        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
                $( "#controles" ).draggable({ containment: ".contenido" });
            });
            $(document).ready(function(){
                $("#list").treeview({
                    control: "#treecontrol",
                    collapsed: true,
                    animated: "medium",
                    persist: "cookie",
                    cookieId: "treeview-black"
                });

                $("#list input[type='checkbox']").die('click').live({                    
                    click: function() {
                        
                        var idcategoria = $(this).attr('idcategoria');
                        var allVals = [];
                        $('#list :checked ').each(function() {
                            if ($(this).attr('idcategoria')==idcategoria){
                                allVals.push($(this).val());
                            }
                        });
                        //alert(allVals);
                        $.ajax({
                            beforeSend: function(){
                                $('#divmensaje', parent.document).html('<img src="../images/loading51.gif" alt="" width="16" /> Cargando...');
                                $('#botonSubmit', parent.document).button({ disabled: true });
                                $("#list input[type='checkbox']").each(function() {
                                    if ($(this).attr('idcategoria')==idcategoria){
                                        $('#label-'+$(this).val()).removeClass("funcionLoaded");
                                    }
                                    $(this).attr("disabled",true);
                                });                                
                            },
                            type: 'POST',
                            url: 'grupo.Op.php',
                            data: 'idgrupo='+$('#idgrupo').val()+'&idcategoria='+idcategoria+'&funciones='+allVals+'&accion=setGrupoFuncion',
                            success: function(html){
                                $('#botonSubmit', parent.document).button({ disabled: false });
                                $('#divmensaje', parent.document).html('');
                                $("#list input[type='checkbox']").each(function() {
                                    if ($(this).attr('idcategoria')==idcategoria && $(this).attr('checked')){
                                        $('#label-'+$(this).val()).addClass("funcionLoaded");                                        
                                    }
                                    $(this).attr("disabled",false);
                                });
                            }
                        });
                    }
                });

                <?php
                if ($_REQUEST["accion"] == "visualizar"){
                    echo "$('input[type=\'checkbox\']').attr('disabled',true);";
                }
                ?>
                
            });
        </script>
    </head>
    <body style="margin:0px;background:#fff;" id="bodyList">
        <input type="hidden" name="idgrupo" id="idgrupo" value="<?php echo $_REQUEST["idgrupo"];?>">
        <div class="contenido" align="left">
            <div id="controles" class="contenido" >
                <div id="treecontrol" align="center" style="margin: 1px;">
                    <a title="" href="#"><button style="font-size: 1px;margin: 1px;"><img src="../images/Bullet-Toggle-Minus-32.png" /></button></a>
                    <a title="" href="#"><button style="font-size: 1px;margin: 1px;"><img src="../images/Bullet-Toggle-Plus-32.png" /></button></a>
                </div>
            </div>
            <ul id="list" class="treeview-black">                
                <?php
                if (count($data) > 0){
                    foreach ($data->catP as $catP){
                        ?>
                        <li>
                            <span>
                                <b><?php echo $catP->catPadre;?></b>
                            </span>
                            <ul>
                                <?php
                                foreach ($data->catH as $catH){
                                    if ($catP->idcatPadre == $catH->idcatPadre){
                                        ?>
                                        <li>
                                            <span>
                                                <?php echo $catH->catHijo;?>
                                            </span>
                                            <?php
                                            if ($catH->mobil){
                                                ?>
                                                <img src="../images/iPhone-32.png" class="mobil" width="16" height="16" title="Modulo Mobil">
                                                <?php
                                            }
                                            ?>
                                            
                                            <ul>
                                                <?php
                                                foreach ($data->func as $func){
                                                    if ($catP->idcatPadre == $func->idcatPadre && $catH->idcatHijo == $func->idcatHijo ){
                                                        ?>
                                                        <li >
                                                            <input type="checkbox" name="idfuncion-<?php echo $func->idfuncion?>" id="idfuncion-<?php echo $func->idfuncion?>" idcategoria="<?php echo $func->idcatHijo?>" value="<?php echo $func->idfuncion?>" <?php echo $func->grupoFuncion=="1"?"checked":"";?> />
                                                            <span id="label-<?php echo $func->idfuncion?>" class="<?php echo $func->grupoFuncion=="1"?"funcionLoaded":"";?>" >
                                                                <?php echo $func->nombreFuncion;?>
                                                            </span>                                                            
                                                            <?php
                                                            if ($func->mobil){
                                                                ?>
                                                                <img src="../images/iPhone-32.png" class="mobil" width="16" height="16" title="Modulo Mobil">
                                                                <?php
                                                            }
                                                            ?>
                                                        </li>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
    </body>
</html>