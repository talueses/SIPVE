<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Grupo {
    
    private $idgrupo = null;
    private $nombre = null;
    private $descripcion = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdgrupo($idgrupo){
        $this->idgrupo = $idgrupo;
    }
    public function getIdgrupo(){
        return $this->idgrupo;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idgrupo !== null) && (trim($this->idgrupo)!=='') ){
            $campos .= "idgrupo,";
            $valores .= "'".$this->idgrupo."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $campos .= "descripcion,";
            $valores .= "'".$this->descripcion."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO grupo $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE grupo SET ";
        
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $sql .= "descripcion = '".$this->descripcion."',";
        }
        else{
            $sql .= "descripcion = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idgrupo = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM grupo  WHERE idgrupo = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de Grupos
     * @return object Devuelve un registro como objeto
     */
    function  getGrupos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idgrupoBuscador"]){
            $in = null;
            foreach ($_REQUEST["idgrupoBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idgrupo in (".$in.")";
        }
        
        $sql = "select /*start*/ idgrupo,nombre,descripcion /*end*/ from grupo ".$arg." order by nombre,descripcion";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de Grupo
     * @param int $idgrupo Codigo
     * @return object Devuelve registros como objeto
     */
    function getGrupo($idgrupo){
        $sql = "SELECT idgrupo,nombre,descripcion FROM grupo WHERE idgrupo = '".$idgrupo."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>