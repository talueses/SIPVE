<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'grupo.tabla.php';
require_once 'grupo_has_funcion.tabla.php';
/**
 * Clase Grupos{}
 * @author David Concepcion CENIT-DIDI
 */
class Grupos extends Grupo{

    /**
     * Elimina solo a funciones de un grupo asociadas a una categoria
     * @param int $idgrupo Codigo del grupo
     * @param string $idcategoria Codigo de la categoria
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    static function unsetFuncionesGrupo($idgrupo,$idcategoria){
        /*$sql = "delete gf 
                from grupo g, grupo_has_funcion gf, funcion f
                where g.idgrupo = '".$idgrupo."'
                  and g.idgrupo = gf.grupo_idgrupo
                  and gf.funcion_id = f.id
                 and f.idcategoria = '".$idcategoria."'";*/
        $sql = "delete from grupo_has_funcion where grupo_idgrupo = '".$idgrupo."' and funcion_id in (select id from funcion where idcategoria = '".$idcategoria."')";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return true;
    }

    /**
     * Consulta de categorias padres - hijos y sus funciones asociadas, marcando las funciones guardades de un grupo
     * @param int $idgrupo Codigo del grupo
     * @return object Devuelve registros como objeto
     */
    static function getGrupoFuncionLoaded($idgrupo){
        $sql = "select catP.idcategoria as \"idcatPadre\",
                       catP.nombre as \"catPadre\",
                       catH.idcategoria as \"idcatHijo\",
                       catH.nombre as \"catHijo\",
                       f.id as idfuncion,
                       f.nombre as \"nombreFuncion\",
                       f.mobil,
                       (select 1 from grupo_has_funcion where grupo_idgrupo = '".$idgrupo."' and funcion_id = f.id) as \"grupoFuncion\"
                from categoria catH ,categoria catP , funcion f
                where catH.categoria_padre is not null
                  and catH.categoria_padre = catP.idcategoria
                  and catH.idcategoria = f.idcategoria
                    order by catP.nombre,catH.nombre,f.nombre";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
}
?>
