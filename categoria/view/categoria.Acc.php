<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/categoria.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new Categorias();
    $data = $obj->getCategoria($_REQUEST["idcategoria"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
            });
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Categoria</div>            
            <br/>
            <div id="divmensaje" style="width:99%;"></div>
            <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
            <form method="POST" name="f1" action="categoria.Op.php" target="ifrm1">
                <div id="datos"  align="center">
                    <table>
                        <tr title="ID">
                            <td align="right">ID:</td>
                            <td>
                                <input type="text" name="idcategoria" id="idcategoria" maxlength="10" value="<?php echo $data->idcategoria;?>" <?php echo $disabled;?> <?php echo $_REQUEST["accion"]=="modificar"?"readonly":"";?>>
                            </td>
                        </tr>
                        <tr title="Nombre">
                            <td align="right">Nombre:</td>
                            <td>
                                <input type="text" name="nombre" id="nombre" maxlength="60" value="<?php echo $data->nombre;?>" <?php echo $disabled;?>>
                            </td>
                        </tr>
                        <tr title="Descripcion">
                            <td align="right">Descripcion:</td>
                            <td>
                                <input type="text" name="descripcion" id="descripcion" maxlength="100" value="<?php echo $data->descripcion;?>" <?php echo $disabled;?>>
                            </td>
                        </tr>
                        <tr title="Categoria_padre">
                            <td align="right">Categoria_padre:</td>
                            <td>
                                <select name="categoria_padre" id="categoria_padre" <?php echo $disabled;?>>
                                    <option value="">Seleccione</option>
                                    <?php echo ControlCategoria::make_combo("categoria","where categoria_padre is null order by nombre", "idcategoria", "nombre", $data->categoria_padre,false);?>
                                </select>
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div id="botones" style="clear:left">
                    <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <?php
                        if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                            echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Categoria\" />";
                        }
                        ?>
                </div>
            </form>
        </div>
    </body>
</html>