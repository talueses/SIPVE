<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLLOGA"); //Categoria del modulo
require_once "../../inicio/controller/controller.php";// Class CONTROL ControlCtrlLogacceso()
require_once "../model/ctrl_logasistencia.model.php";

/**
 * Description
 * @author David Concepcion
 */
class ControlCtrlLogasistencia extends Controller{
    /**
     * @var array nombre de campos campos
     */
    var $campos  = null;
    
    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        
        $this->setCampos("fecha_desde","Fecha Desde");
        $this->setCampos("fecha_hasta","Fecha Hasta");
        
    }
    
    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        
        $datos   = array();
        if ($_REQUEST["fecha_desde"] == "" && $_REQUEST["fecha_hasta"] != "" || $_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] == ""){
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_desde","tipoDato"=>"");
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_hasta","tipoDato"=>"");
        }
        

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }
    
    public function loadLog(){
        if (!$this->sonValidosDatos()){
            $this->mensaje  = "<div class=\"ui-widget\">\n";
            $this->mensaje .= "    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">\n";
            $this->mensaje .= "        <p>\n";
            $this->mensaje .= "            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>\n";
            $this->mensaje .= "                ".$this->mensaje;
            $this->mensaje .= "        </p>\n";
            $this->mensaje .= "    </div>\n";
            $this->mensaje .= "</div>\n";
            $this->error = true;
            return false;
        }

        // ---- Argumento de la consulta segun parametros seleccionados en la vista ---- //
        $arg = array();

        //-------------------------------------------------------------------------------//
        if ($_REQUEST["numeroUsuario"] != "" ){
            $arg["campos"] .= " and cu.numero = '".$_REQUEST["numeroUsuario"]."'\n";
        }//-------------------------------------------------------------------------------//
        if ($_REQUEST["iddepartamento"] != "" ){
            $arg["campos"] .= " and cu.iddepartamento = '".$_REQUEST["iddepartamento"]."'\n";
        }
        //-------------------------------------------------------------------------------//
        if ($_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] != ""){
            $arg["campos"] .= " and cast(concat(lg.anio,'-',lg.mes,'-',lg.dia,' ',lg.hora,':',lg.minuto,':',lg.segundo) as datetime) between '".Controller::formatoFecha($_REQUEST["fecha_desde"])."' and '".Controller::formatoFecha($_REQUEST["fecha_hasta"])."'\n";
        }
        //-------------------------------------------------------------------------------//
        
        $obj = new CtrlLogasistencia();
        $data = $obj->getLogs($arg);

        return $data;
    }
    
}

?>
