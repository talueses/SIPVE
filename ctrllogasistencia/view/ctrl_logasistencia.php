<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de 
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/ctrl_logasistencia.control.op.php";// Class CONTROLLER

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>CtrlLogaccesos</title>        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>        
        <script type="text/javascript" src="../../inicio/js/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui-timepicker-es.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.ui.datepicker-es.js"></script>
        
        <style type="text/css" >
            #tabla{
                float:left;
                width:30%;
                height:95%;
            }
            #listado{
                width:99%;                
                margin: 0px 1px 0px 1px;
            }
            #titulo{
                height: 30px;
                
            }
            #listado tr th {                
                text-align: right;
            }
            
            #botones{
                margin: 10px;
            }
            select{
                width: 100%;
            }
            select option[value=""]{
                font-weight: bold;
                color: #0000FF;
            }            
            
            #botones{
                margin: 10px;
            }            
            div#buttonsOptions {
                width: 120px;
                padding: 9px;
                border:#c0c0c0 dashed 2px;
                margin-top: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button,input:reset" ).button();
                
                $('#listado tr th').addClass('ui-corner-left ui-state-focus');
                
                $('#fecha_desde').datetimepicker({
                    maxDate: 'd m Y',
                    hour: 8,
                    minute: 00,
                    onClose: function(dateText, inst) {
                        var endDateTextBox = $('#fecha_hasta');
                        if (endDateTextBox.val() != '') {
                            var testStartDate = new Date(dateText);
                            var testEndDate = new Date(endDateTextBox.val());
                            if (testStartDate > testEndDate)
                                endDateTextBox.val(dateText);
                        }
                        else {
                            endDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime){
                        var start = $(this).datetimepicker('getDate');
                        $('#fecha_hasta').datetimepicker('option', 'minDate', new Date(start.getTime()));
                    }
                }).attr('readonly',true);
                $('#fecha_hasta').datetimepicker({
                    maxDate: 'd m Y',
                    hour: 17,
                    minute: 00,
                    onClose: function(dateText, inst) {
                        var startDateTextBox = $('#fecha_desde');
                        if (startDateTextBox.val() != '') {
                            var testStartDate = new Date(startDateTextBox.val());
                            var testEndDate = new Date(dateText);
                            if (testStartDate > testEndDate)
                                startDateTextBox.val(dateText);
                        }
                        else {
                            startDateTextBox.val(dateText);
                        }
                    },
                    onSelect: function (selectedDateTime){
                        var end = $(this).datetimepicker('getDate');
                        $('#fecha_desde').datetimepicker('option', 'maxDate', new Date(end.getTime()) );
                    }
                }).attr('readonly',true);
                
            });
            
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <div id="tabla" align="center" class="ui-widget-content ui-corner-all">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                    echo Controller::$mensajePermisos;
                }else{
                    ?>
                    <form name="f1" id="f1" target="frm1" action="ctrl_logasistencia.Acc.php" method="GET">
                        <table id="listado" class="ui-widget-content ui-corner-all">
                            <tr>
                                <td id="titulo" class="ui-widget-header ui-corner-all" colspan="3" align="center"><b>Listado de Asistencia</b></td>
                            </tr>                            
                            <tr>
                                <th>Empleado</th>
                                <td>
                                    <select name="numeroUsuario" id="numeroUsuario" >
                                        <option value="">&laquo; Todos &raquo;</option>
                                        <?php echo Controller::make_combo("ctrl_usuario", "where tipo_tarjeta = 'empleado' order by nombres, apellidos", "numero", "nombres,apellidos", "");?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Direcci&oacute;n</th>
                                <td>
                                    <select name="iddepartamento" id="iddepartamento" >
                                        <option value="">&laquo; Todos &raquo;</option>
                                        <?php echo Controller::make_combo("car_departamento", "where iddepartamento in (SELECT distinct iddepartamento FROM ctrl_usuario) order by departamento_nombre", "iddepartamento", "departamento_nombre", "");?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Rango de Fechas</th>
                                <td>
                                    <input type="text" name="fecha_desde" id="fecha_desde" size="13">:<input type="text" name="fecha_hasta" id="fecha_hasta" size="13">
                                </td>
                            </tr>
                        </table>
                        <div id="botones" >
                            <center>                                
                                <input type="hidden" name="accion" id="accion" value="">
                                <input type="submit" value="Aceptar" class="boton" onclick="$('#accion').val('')">
                                <input type="reset" value="Cancelar" class="boton">
                                <input type="submit" value="Ayuda" onclick="this.form.action='vid_accesopc.Ayuda.php'" class="boton">
                            </center>                        
                            <div id="buttonsOptions" class="ui-corner-all" >
                                <?php
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"descargar")){
                                        ?>                                        
                                        <button type="submit" style="font-size: 10px;" onclick="$('#accion').val('descargar')">
                                            <img src="../images/Download-32.png" alt="" style="margin-bottom: 5px;" />
                                            <br />
                                            Descargar CSV
                                        </button>
                                        <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
            <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                <iframe name="frm1" id="frm1" frameborder="0" scrolling="yes" style="width:100%; height:650px;overflow-x: hidden;overflow-y: auto;" src=""></iframe>
            </div>
        </div>        
    </body>
</html>

