<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_logasistencia.control.op.php";// Class CONTROLLER

$obj= new ControlCtrlLogasistencia();
$data = $obj->loadLog();

if (!$obj->error){    
    
    if ($_REQUEST["accion"]=="descargar" && count($data) > 0){
        
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=asistencia_'.date("d-m-Y_H-i-s").'.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
        fputcsv($output, array('Fecha', ('Cédula'), 'Nombre',('Dirección')));
        
        foreach ($data as $key => $row){
            fputcsv($output, array(Controller::formatoFecha($row->fechaEvento), $row->cedula, $row->usuarioTrj,$row->departamento_nombre));
        }        
        
        die();
    }
    
    
    
    
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link href="../../inicio/css/jquery.qtip.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.qtip.js"></script>
        
        <style type="text/css">
            #tabla {
                width:900px;
                height: 99%;
            }
            #listado{
                width:99%;
                margin: 0px 1px 0px 1px;
            }
            #titulo{
                font-size: 14px;
                padding: 5px 0px 5px 0px;
            }
            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            
            #listado tr.fecha{
                background:#ddd;
            }
            .boton{
                font-size: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
                
                
                $('.rowsCarnets').qtip({
                    content: function(api) {                        
                        return '<img class="foto" src="../../carempleado/images/fotos/'+$(this).attr('archivo_foto')+'" alt="Foto" />';                        
                    },
                    position: {
                        my: 'top left',
                        target: 'mouse',
                        viewport: $(window), // Keep it on-screen at all times if possible
                        adjust: {
                            x: 10,  y: 10
                        }
                    },
                    hide: {
                        fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
                    },
                    style: {
                        classes: 'ui-tooltip-youtube'
                    }
                });
                
                
            });
            filaseleccionada = {};
            function omover(fila){
                if (fila.id===filaseleccionada.id ) return false;                
                $('#'+fila.id).removeClass('fecha').addClass('pasada');
            }
            function omout(fila){
                if (fila.id===filaseleccionada.id ) return false;
                if ($('#'+fila.id).attr('fecha')){
                    $('#'+fila.id).addClass('fecha');
                }
                $('#'+fila.id).removeClass('pasada');
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        
        <center>
            <div id="divmensaje" style="width:99%;"><?php echo $obj->mensaje?></div>
            <?php
            if (!$obj->error){
                ?>
                <div id="tabla" align="center" class="ui-corner-all ui-widget-content">
                    <br>
                    <table id="listado" align="center" class="ui-corner-all ui-widget-content">
                        <tr>
                            <td id="titulo" class="ui-widget-header ui-corner-all" colspan="6" align="center"><b>Listado de Asistencia</b></td>
                        </tr>
                        <tr>
                            <th class="ui-state-default ui-corner-left" align="center" width="40">N&deg;</th>
                            <th class="ui-state-default" width="160">Fecha</th>
                            <th class="ui-state-default" width="80">C&eacute;dula</th>
                            <th class="ui-state-default" width="210">Nombre</th>
                            <th class="ui-state-default ui-corner-right" width="410">Direcci&oacute;n</th>
                        </tr>
                        <?php                        
                        if (count($data) > 0){
                            $i=0;
                            foreach ($data as $key => $row){
                                
                                $classFecha = "";
                                if ($row->fechaEventoDate != $fechaEventoDate ){
                                    $i++;                                                                        
                                }                                
                                if (($i & 1)==0){
                                    $classFecha = " fecha";
                                }
                                $fechaEventoDate = $row->fechaEventoDate;
                                
                                ?>
                                <tr id="<?php echo $row->logID;?>" onmouseover="omover(this)" onmouseout="omout(this)" class="<?php echo $classFecha; echo $row->archivo_foto?" rowsCarnets ":""; ?>"  archivo_foto="<?php echo $row->archivo_foto;?>" fecha="<?php echo $classFecha?"true":""?>" idusuario="<?php echo $row->idusuario?>">
                                    <td align="center"><b><?php echo paginationSQL::$start+$key+1;?></b></td>
                                    <td align="center"><?php echo Controller::formatoFecha($row->fechaEvento);?></td>                                                                        
                                    <td align="center"><?php echo $row->cedula;?></td>
                                    <td>
                                        <?php 
                                        echo $row->usuarioTrj;
                                        if ($row->hasCarnetEmpleado && $row->tipo_tarjeta=="empleado" || ($row->hasCarnetVisitante && $row->tipo_tarjeta=="visitante")){
                                            ?><img src="../images/Carnet-32.png" alt="Carnet" title="Carnet Asignado" width="16" style="float: right;" /><?php
                                        }                        
                                        ?>
                                    </td>
                                    <td>
                                        <span style="<?php echo in_array($row->codigofuncion, Controller::$error_codes)?"font-weight: bold;color: #cd0a0a":"" ?>">
                                            <?php echo $row->departamento_nombre;?>
                                        </span>
                                    </td>

                                </tr>
                                <?
                            }
                        }else{
                            ?>
                            <tr>
                                <td colspan="6">
                                    <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                </td>
                            </tr>

                            <?php
                        }
                        ?>
                    </table>
                    <?php echo paginationSQL::links();?>
                </div>
                <?php    
            }
            ?>
        </center>
    </body>
</html>