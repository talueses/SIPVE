<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'vid_modelo_camara.tabla.php';

/**
 * Clase VidModeloCamaras{}
 * @author David Concepcion CENIT-DIDI
 */
class VidModeloCamaras extends VidModeloCamara{

    /**
     * Consulta de Modelo de camaras con las marcas asociadas
     * @return object Devuelve un registro como objeto
     */
    function  getModeloCamaras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idmodelo_camaraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idmodelo_camaraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and mc.idmodelo_camara in (".$in.")";
        }
        
        $sql = "select /*start*/
                       mc.idmodelo_camara,
                       mc.idmarca_camara,
                       mc.nombre_modelo_camara,
                       mac.nombre_marca_camara 
                       /*end*/
                from vid_modelo_camara mc,
                     vid_marca_camara mac
                     where mc.idmarca_camara = mac.idmarca_camara
                     ".$arg."
                    order by mac.nombre_marca_camara,mc.nombre_modelo_camara";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
}
?>
