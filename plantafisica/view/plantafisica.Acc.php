<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/plantafisica.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new Plantafisicas();
    $data = $obj->get_Plantafisica($_REQUEST["idplantafisica"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            #planoImg{
                width: 300px;
                background:#fff;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
        </style>
        <script type="text/javascript" language="javascript">

            $(function() {
                $( "input:button, input:submit" ).button();                
            });
            function previewImg(obj) {
                var rFilter = /^(image\/bmp|image\/cis-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x-cmu-raster|image\/x-cmx|image\/x-icon|image\/x-portable-anymap|image\/x-portable-bitmap|image\/x-portable-graymap|image\/x-portable-pixmap|image\/x-rgb|image\/x-xbitmap|image\/x-xpixmap|image\/x-xwindowdump)$/i;
                var str;
                $('#divmensaje').html('');                
                if (!rFilter.test(obj.files[0].type)){
                    str = '<div class="ui-widget">';                
                    str += '    <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
                    str += '        <p>';
                    str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
                    str += '                El archivo debe ser una imagen';
                    str += '        </p>';
                    str += '    </div>';
                    str += '</div>';
                    $('#divmensaje').html(str);                    
                    return false;
                }
                
                if (obj.files && obj.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#planoImg')
                            .attr('src', e.target.result)
                            .width(300);
                            //.height(200);
                    };

                    reader.readAsDataURL(obj.files[0]);
                }
            }
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Planta F&iacute;sica</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="0"></iframe>
                <form method="POST" name="f1" action="plantafisica.Op.php" target="ifrm1" enctype="multipart/form-data">
                    <div id="datos"  align="center">
                        <table>
                            <?php
                            //if ($_REQUEST["accion"] != "agregar"){
                                ?>
                                <tr>
                                    <td colspan="2">
                                        <img src="../images/<?php echo $data->file?$data->file:"Picture-file-256.png";?>" id="planoImg" alt="" />
                                    </td>
                                </tr>
                                <?php
                            //}
                            ?>
                            <tr title="Nombre de la Planta F&iacute;sica">
                                <td align="right">Planta F&iacute;sica:</td>
                                <td>
                                    <input type="text" name="plantafisica" id="plantafisica" maxlength="100" value="<?php echo $data->plantafisica;?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="File">
                                <td align="right">Archivo:</td>
                                <td>
                                    <input type="hidden" name="urlfile" id="urlfile" value="<?php echo $data->file;?>"  />
                                    <input name="file" type="file" id="file" size="5" onchange="previewImg(this)" <?php echo $disabled;?>/>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idplantafisica" id="idplantafisica" value="<?php echo $data->idplantafisica ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Planta F&iacute;sica\" id=\"botonSubmit\" />";
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>