<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "ADMPLT"); //Categoria del modulo
require_once "../controller/plantafisica.control.php";// Class CONTROL ControlPlantafisica()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpPlantafisica extends ControlPlantafisica{

    /**
     * @var string Ruta donde se guardan las imagenes. Valor por defecto "../images/"
     */
    private $uploaddir = "../images/";
    /**
     * @var int Ancho maximo de la imagen. Valor por defecto 800px
     */
    private $maxWidth   = 800;
    /**
     * @var int Alto maximo de la imagen. Valor por defecto 600px
     */
    private $maxHeight  = 600;
    /**
     * @var int Peso maximo en bits. Valor por defecto 2.097.152 Bits, equivalen a 2 MB.
     */
    private $maxSize    = 2097152;
    
    function setOpPlantafisica(){
        $exito = true;
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        $file = $_FILES['file']['name'];        
        
        
        //echo "<div align='left'><pre>".print_r($_FILES,true)."</pre></div>";
        //die();
        //Array ( [0] => 720 [1] => 480 [2] => 3 [3] => width="720" height="480" [bits] => 8 [mime] => image/png ) 
        
        
        
        if (!empty($file)){
            
            $imageInfo = getimagesize($_FILES['file']["tmp_name"]);
            if (!$imageInfo){
                $this->mensaje = "El archivo debe ser una imagen.";
                return false;
            }else{
                if ($imageInfo[0] > $this->maxWidth || $imageInfo[1] > $this->maxHeight){
                    $this->mensaje = "La imagen es muy grande. Debe tener un m&aacute;ximo de ".$this->maxWidth." x ".$this->maxHeight." pixeles.";
                    return false;
                }
            }
            
            $extencion      = substr($file,strlen($file)-4,4);           
            $size      = $_FILES['file']['size'];            
            if ($size > $this->maxSize){
                $this->mensaje = "La imagen es muy grande. Debe tener un m&aacute;ximo de ".$this->file_size($this->maxSize);
                return false;
            }
                
            // Eliminar archivo ya creado //
            if (!empty($_REQUEST['urlfile'])){                
                $exito = unlink($this->uploaddir.$_REQUEST['urlfile']);
            }
            
            if ($exito){                
                $urllogo_cb = $this->uploaddir . $file;
                $exito = move_uploaded_file($_FILES['file']['tmp_name'], $urllogo_cb);
            }                  
            
            
        }else{
            if (!empty($_REQUEST['urlfile'])){
                $file = $_REQUEST['urlfile'];
            }
        }
        
        $_REQUEST["file"]= $file;        
        
        if (!$exito){
            $this->mensaje = "Ocurri&oacute; un error al guardar la imagen.";
            return false;
        }

        //------------------ Metodo Set  -----------------//
        if(!$this->setPlantafisica()) return false;
        
        return true;
        
    }
    function eliminarOpPlantafisica(){

        
        
        $obj = new Plantafisica();        
        $data = $obj->get_Plantafisica($_REQUEST["idplantafisica"]);
        
        
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//        
        if(!$this->eliminarPlantafisica()) return false;
        
        $exito = unlink($this->uploaddir.$data->file);
        if (!$exito){
            $this->mensaje = "Ocurri&oacute; un error al eliminar la imagen.";
            return false;
        }
        
        return true;
    }

    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Planta F&iacute;sica");

        $this->setCampos("plantafisica","Planta F&iacute;sica");
        $this->setCampos("file","Archivo");

    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"plantafisica","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"file","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;

        return true;
    }

    

}

?>