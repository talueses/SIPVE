<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Plantafisica {
    
    private $idplantafisica = null;
    private $plantafisica = null;
    private $file = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdplantafisica($idplantafisica){
        $this->idplantafisica = $idplantafisica;
    }
    public function getIdplantafisica(){
        return $this->idplantafisica;
    }
    public function setPlantafisica($plantafisica){
        $this->plantafisica = $plantafisica;
    }
    public function getPlantafisica(){
        return $this->plantafisica;
    }
    public function setFile($file){
        $this->file = $file;
    }
    public function getFile(){
        return $this->file;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idplantafisica !== null) && (trim($this->idplantafisica)!=='') ){
            $campos .= "idplantafisica,";
            $valores .= "'".$this->idplantafisica."',";
        }
        if(($this->plantafisica !== null) && (trim($this->plantafisica)!=='') ){
            $campos .= "plantafisica,";
            $valores .= "'".$this->plantafisica."',";
        }
        if(($this->file !== null) && (trim($this->file)!=='') ){
            $campos .= "file,";
            $valores .= "'".$this->file."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO plantafisica $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE plantafisica SET ";
        
        if(($this->plantafisica !== null) && (trim($this->plantafisica)!=='') ){
            $sql .= "plantafisica = '".$this->plantafisica."',";
        }
        if(($this->file !== null) && (trim($this->file)!=='') ){
            $sql .= "file = '".$this->file."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idplantafisica = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM plantafisica  WHERE idplantafisica = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de Plantafisicas
     * @return object Devuelve un registro como objeto
     */
    function  getPlantafisicas(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idplantafisicaBuscador"]){
            $in = null;
            foreach ($_REQUEST["idplantafisicaBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idplantafisica in (".$in.")";
        }
        
        $sql = "select /*start*/ idplantafisica,plantafisica,file /*end*/ from plantafisica ".$arg." order by plantafisica,file";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de Plantafisica
     * @param int $idplantafisica Codigo
     * @return object Devuelve registros como objeto
     */
    function get_Plantafisica($idplantafisica){
        $sql = "SELECT idplantafisica,plantafisica,file FROM plantafisica WHERE idplantafisica = '".$idplantafisica."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>