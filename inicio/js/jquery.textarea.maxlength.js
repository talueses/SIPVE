/**
 * This library must be included after Jquery core library and User Interface Plugin
 */

jQuery(function($) {

  // ignore these keys
  var ignore = [8,9,13,33,34,35,36,37,38,39,40,46];//

  // use keypress instead of keydown as that's the only
  // place keystrokes could be canceled in Opera
  var eventName = 'keypress';

  // handle textareas with maxlength attribute
  $('textarea[maxlength]')

    // this is where the magic happens
    .live(eventName, function(event) {
      var self = $(this),
          maxlength = self.attr('maxlength'),
          code = $.data(this, 'keycode');

      // check if maxlength has a value.
      // The value must be greater than 0
      if (maxlength && maxlength > 0) {

        // continue with this keystroke if maxlength
        // not reached or one of the ignored keys were pressed.
        //alert(( self.val().length < maxlength )+' - '+self.val().length+' < '+maxlength);
        /*if ($('#charLeft_'+self.attr('id'))){
            $('#charLeft_'+self.attr('id')).val((maxlength - self.val().length));
        }*/
        
        if ( self.val().length < maxlength || $.inArray(code, ignore) !== -1 ){
            $('#charLeft_'+self.attr('id')).val((maxlength - self.val().length));
            return true;
        }else{
            return false;
        }
        //return ( self.val().length < maxlength || $.inArray(code, ignore) !== -1 );

      }
    })

    // store keyCode from keydown event for later use
    .live('keydown', function(event) {
      $.data(this, 'keycode', event.keyCode || event.which);
    });

    
    // User Interface Resizable Interaction for text area
    $( 'textarea' ).resizable({
        handles: 'se'
    });

    // Set initial value of the characters left in the textarea
    if ($('.charLeft')){
        $('.charLeft').each(function(){
            var textareaObj = $('#'+$(this).attr('id').substring(9));
            var charLeft = textareaObj.attr('maxlength')-textareaObj.val().length;
            $('#'+$(this).attr('id')).val(charLeft);
        });
    }
    
});