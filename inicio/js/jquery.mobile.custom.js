/**
 * Custom jMobile Scripts
 */



$(document).bind('mobileinit', function(){
    $.mobile.pageLoadErrorMessage= 'Error: Cargando P\xe1gina';
    
});

$(function(){
    $('#userInfo').hide();
    $('div#headerMobile').live({
        click: function() {
            $('#userInfo').fadeIn(500);            
        }
    });
    $('a#userInfoClose').live({
        click: function() {
            $('#userInfo').fadeOut(500);
            return false;
        }
    });
});
