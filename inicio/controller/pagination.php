<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/************************************************************\
  *
  *	  PHP Array Pagination Copyright 2007 - Derek Harvey
  *	  www.lotsofcode.com
  *
  *	  This file is part of PHP Array Pagination .
  *
  *	  PHP Array Pagination is free software; you can redistribute it and/or modify
  *	  it under the terms of the GNU General Public License as published by
  *	  the Free Software Foundation; either version 2 of the License, or
  *	  (at your option) any later version.
  *
  *	  PHP Array Pagination is distributed in the hope that it will be useful,
  *	  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
  *	  GNU General Public License for more details.
  *
  *	  You should have received a copy of the GNU General Public License
  *	  along with PHP Array Pagination ; if not, write to the Free Software
  *	  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
  *
  \************************************************************/

  class pagination
  {
    var $page = 1; // Current Page of the pagination
    var $perPage = 10; // Items on each page of the pagination, defaulted to 10

    function generate($array)
    {
            
      if ($_SESSION['PHP_SELF']!=$_SERVER["PHP_SELF"]){
          $_SESSION['PHP_SELF'] = $_SERVER["PHP_SELF"];
          $_SESSION['perPage'] = $_SESSION['page'] = null;
      }
        
      // Assign the items per page variable
      if (!empty($_GET['perPage'])){
        $this->perPage = $_GET['perPage'];
      } elseif (!empty($_SESSION['perPage'])){
          $this->perPage = $_SESSION['perPage'];
      }        
      
      $_SESSION['perPage'] = $this->perPage;
      
      // Assign the page variable
      if (!empty($_GET['page'])) {
        $this->page = $_GET['page']; // using the get method
      } elseif (!empty($_SESSION['page'])) {
        $this->page = $_SESSION['page']; // using the session method
      }  else {
          $this->page = 1; // if we don't have a page number then assume we are on the first page
      }
      
      $_SESSION['page'] = $this->page;
      
      // Take the length of the array
      $this->length = count($array);

      // Get the number of pages
      $this->pages = ceil($this->length / $this->perPage);

      // Calculate the starting point
      $this->start  = ceil(($this->page - 1) * $this->perPage);

      // Reset the starting point if the combo perPage set the start value greater than the length value
      if ($this->start>$this->length){
          $this->page = 1;
          $this->start=0;
      }

      if ($this->length==0){
          return array();
      }

      // Return the part of the array we have requested
      return array_slice($array, $this->start, $this->perPage);
    }

    function links()
    {
      // Initiate the links array
      $plinks = array();
      $links  = array();
      $slinks = array();
      $max    = array();

      // Concatenate the get variables to add to the page numbering string
      if (count($_GET)) {
        $queryURL = '';
        foreach ($_GET as $key => $value) {
          // Query URL for links
          if ($key != 'page') {
            $queryURL .= '&'.$key.'='.$value;
          }
          // Query URL for combo perPage
          if ($key != 'page' && $key != 'perPage') {
            $queryURLMax .= '&'.$key.'='.$value;
          }
        }
      }

      // If we have more then one pages
      if (($this->pages) > 1)
      {
        // Assign the 'previous page' link into the array if we are not on the first page
        if ($this->page != 1) {
          $plinks[] = ' <button type="button" title="Primero" onclick="document.location.href=\'?page=1'.$queryURL.'\'">&laquo;&laquo;</button> ';
          $plinks[] = ' <button type="button" title="Anterior" onclick="document.location.href=\'?page='.($this->page - 1).$queryURL.'\'">&laquo;</button> ';
        }

        // Assign the previous 4 page numbers of the current page to the array $links
        for ($i=($this->page-4);$i<=$this->page-1;$i++){
            if ($i>0){
                //$links[] = ' <a href="?page='.$i.$queryURL.'">'.$i.'</a> '; // add the link to the array
                $links[] = '<button type="button" title="'.$i.'" onclick="document.location.href=\'?page='.$i.$queryURL.'\'">'.$i.'</button> ';
            }
        }

        // Assign dots separators previous of the current page number to the array $links
        if (($this->page)>1)$links[] =  "...";

        // Assign the current page to the array $links
        $links[] =  "&nbsp;<strong>(&nbsp;".($this->page )."&nbsp;)</strong>&nbsp;"; // pagina actual

        // Assign dots separators after of the current page number to the array $links
        if (($this->page)<($this->pages))$links[] =  "...";

        // Assign the after 4 page numbers of the current page to the array $links
        for ($i=($this->page+1);$i<=(($this->page)+4);$i++){
            if ($i<=($this->pages)){
                $links[] = '<button type="button" title="'.$i.'" onclick="document.location.href=\'?page='.$i.$queryURL.'\'">'.$i.'</button> ';
            }
        }

        // Assign the 'next page' if we are not on the last page
        if ($this->page < $this->pages) {
          $slinks[] = '<button type="button" title="Siguiente" onclick="document.location.href=\'?page='.($this->page + 1).$queryURL.'\'">&raquo;</button> ';
          $slinks[] = '<button type="button" title="&Uacute;ltimo" onclick="document.location.href=\'?page='.($this->pages).$queryURL.'\'">&raquo;&raquo;</button> ';
          
        }

        // Set perPage dinamic value
        
        $max[]=  "<b style=\"font-size:10px;\" title=\"Total de Registros\">&nbsp;Registros: ".$this->length."&nbsp;</b>";
        $max[]=  "<b>";
        $max[]=  "&hellip;";        
        $max[]=  "<script type=\"text/javascript\" language=\"javascript\"> \n";
        $max[]=  "    function setPerPage(perPage){ \n";
        $max[]=  "        window.location = \"?page=".($this->page).$queryURLMax."&perPage=\"+perPage+\"\"; \n";
        $max[]=  "    } \n";
        $max[]=  "</script> \n";
        $max[]=  "[&nbsp;<select name=\"perPage\" id=\"perPage\" title=\"Registros por p&aacute;gina\" onchange=\"javascript:setPerPage(this.value);\">";
                for ($i=10;$i<=25;$i+=5){
                    $selected = "";
                    if ($this->perPage==$i){
                        $selected = "selected";
                    }
                    $max[]=  "<option value=\"".$i."\" ".$selected.">".$i."</option>\n";
                }
        $max[]=  "</select>&nbsp;]";
        $max[]=  "&hellip;";
        $max[]=  "</b>";
        $max[]=  "&nbsp;<b style=\"font-size:10px;\" title=\"Total de P&aacute;ginas\">&nbsp;P&aacute;ginas: ".$this->pages."</b>";
        $max[]=  "<br />\n";

        // Push the array into a string using any some glue
        return "<p><div id=\"paginado\"  class=\"ui-corner-all\" align=\"center\">".implode(' ', $max).implode(' ', $plinks).implode($this->implodeBy, $links).implode(' ', $slinks)."</div></p>";
      }
      return;
    }
  }

?>