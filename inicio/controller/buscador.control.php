<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Clase Buscador 
 * @author David Concepcion CENIT-DIDI
 * Establece los objetos HTML de trascripcion de datos de busqueda y los datos a buscar
 * 
 * Como usar:
 *  
 * --- Consulta Dinamica ---
 * 
 * En la consulta principal del listado agregar como argumento dinamico del buscador:
 * 
 * // --- Valores del Buscador --- //
 * if ($_REQUEST["idcampoBuscador"]){
 *     $in = null;
 *     foreach ($_REQUEST["idcampoBuscador"] as $row){
 *         $in.= "'".$row."',";
 *     }
 *     $in = substr($in, 0,(strlen($in)-1));
 *     $arg = " where idcampo in (".$in.")";
 * }
 * 
 * Donde idcampo se refiere al campo clave primaria de la tabla a consultar
 *      - Se debe concatenar la variable $arg en la consulta como argumento de la misma.
 *      - Si la consulta ya tiene argumento, sustituir el where por and, y colocar la variagle $arg como ultimo argumento
 *      - Si la tabla de idcampo refiere un alias, colocarlo a idcampo
 * 
 * --- Objetos HTML ---
 * 
 * Instanciar el metodo getObjBuscador() de la calse Buscador e imprimir el valor de retorno:
 * 
 * echo Buscador::getObjBuscador("idcampo", "campo1,campo2,...", "tabla", "order by campo1,campo2,...");
 * 
 * idcampo:           campo clave primaria
 * campo1,campo2,...: campos que se desean comparar
 * tabla:             tabla a consultar
 * 
 * 
 */
class Buscador {

    /**
     * Establece los objetos HTML y JS de trascripcion de datos de busqueda
     * Los parametros de busqueda son encriptados por BASE-64
     * @param string $value valor de codigo ID a bucsar
     * @param string $description valores de descripcion a buscar
     * @param string $table tabla de BD a consultar
     * @param string $arg argumentos sobre la busqueda
     * @return string Devuelve objetos HTML y JS para instanciar el motor de busqueda
     */
    public static function getObjBuscador($value,$description, $table, $arg) {
        $table = base64_encode($table);
        $arg   = base64_encode($arg);
        $str   = null;
        
        $ntab = Controller::getTabulation(8);//        
        $str .= "<script type=\"text/javascript\" src=\"../../inicio/js/jquery.fcbkcomplete.js\"></script>\n";
        $str .= $ntab."\n";
        $str .= $ntab."\n";
        $str .= $ntab."<script type=\"text/javascript\">\n";        
        $str .= $ntab."    $(function() {\n";
        $str .= $ntab."        $('#".Controller::setNameMakeCombo($value)."Buscador').fcbkcomplete({\n";
        $str .= $ntab."            json_url: '../../inicio/view/buscadorData.php?value=".base64_encode($value)."&description=".base64_encode($description)."&tabla=".$table."&arg=".$arg."',\n";
        $str .= $ntab."            addontab: true,\n";
        $str .= $ntab."            maxitems: 10,\n";
        $str .= $ntab."            input_min_size: 0,\n";
        $str .= $ntab."            height: 10,\n";
        $str .= $ntab."            cache: false,\n";
        $str .= $ntab."            newel: false,\n";
        $str .= $ntab."            select_all_text: '',\n";
        $str .= $ntab."            complete_text: '',\n";
        $str .= $ntab."            width: 'auto',\n";
        $str .= $ntab."            delay: 0\n";        
        $str .= $ntab."        });\n";
        $str .= $ntab."        $('#buscadorDiv').hide(0);\n";
        $str .= $ntab."        $('#buscadorLegend').live({\n";
        $str .= $ntab."            click:function(){\n";
        $str .= $ntab."                $('#buscadorDiv').toggle('blind',500);\n";
        $str .= $ntab."            }\n";
        $str .= $ntab."        });\n";
        $str .= $ntab."    });\n";
        $str .= $ntab."</script>\n";
        $str .= $ntab."\n";
        $str .= $ntab."\n";
        $str .= $ntab."<form method=\"POST\" >\n";
        $str .= $ntab."    <fieldset id=\"buscador\">\n";
        $str .= $ntab."        <legend id=\"buscadorLegend\">Buscador</legend>\n";
        $str .= $ntab."        <div id=\"buscadorDiv\" align=\"left\">\n";
        $str .= $ntab."            <input type=\"submit\" id=\"buscadorButton\" value=\"Buscar\" title=\"Presione para buscar o restablecer el listado\" />\n";
        $str .= $ntab."            <select id=\"".Controller::setNameMakeCombo($value)."Buscador\" name=\"".Controller::setNameMakeCombo($value)."Buscador\">\n";
        $str .= $ntab."            </select>\n";
        $str .= $ntab."            <small class=\"comment\">Escriba lo que desee buscar</small>\n";
        $str .= $ntab."        </div>\n";
        $str .= $ntab."    </fieldset>\n";
        $str .= $ntab."</form>\n";
        
        return $str;
    }
    
    /**
     * Consulta de datos para motor de busqueda
     * Los parametros de la busqueda son desencriptados por BASE-64
     * @param string $key valor de codigo ID a bucsar
     * @param string $description valores de descripcion a buscar
     * @param string $table tabla de BD a consultar
     * @param string $arg argumentos sobre la busqueda
     * @return json Devuelve datos en formato JSON
     */
    public static function setDataBuscador($key,$description, $table, $arg){
        $key         = base64_decode($key);
        $description = base64_decode($description);
        $table       = base64_decode($table);
        $arg         = base64_decode($arg);             $str         = array();
        
        $data = Model::getDataBuscador($key, $description, $table, $arg);
        
        $descriptions = explode(",", $description);
        
        foreach ($data as $row){            
            $value = null;
            foreach ($descriptions as $description){
                $description = Controller::setNameMakeCombo($description);
                $value .= $row->$description." ";
            }            
            $key = Controller::setNameMakeCombo($key);            
            $str[] = array("key"=>strval($row->$key),"value"=>$value);
        }
        //echo "<div align='left'><pre>".  print_r( $str , true)."</pre></div>";
        
        return json_encode($str);        
    }
}
?>