<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
//error_reporting(0); // Errors and Warnings off 
require_once '../model/dblink.php';
include_once 'buscador.control.php';
include_once 'pagination.php';
include_once 'pagination.sql.php';

// Constantes de valores configuracion ARCHIVO .ini
$conf = (object) parse_ini_file(dirname(__FILE__)."/../conf.ini", true);

// Configuraciones 
define("VERSION", "v".$conf->SYS_CONF["version"]);
define("DEFAULT_WIDTH", $conf->CARNETS_CONF["defaultWidth"]); 
define("DEFAULT_HEIGHT", $conf->CARNETS_CONF["defaultHeight"]); 


/**
 * Clase Controller{}
 * Clase controlador global
 * Contiene metodos utilizados por mas de un modulo
 * @author David Concepcion CENIT-DIDI
 */
class Controller{
    
    /**
     * @var object Arreglo de Objetos sobre contenido del archivo configuracion
     */
    private static $conf     = null;
    /**
     * @var array Matriz de datos de permisos del ususario
     */
    private static $dataPermisos = null;
    /**
     * @var array Matriz de datos de accesos del ususario
     */
    private static $dataAcc = null;
    /**
     * @var string Mensaje por defecto de error de permisos para una funcion
     */
    public static $mensajePermisos = null;
    /**
     * @var string array Informacion basica del usuario
     */
    public static $datosUsr = null;
    /**
     * @var array Verficacion de acceso 
     */
    public static $chkCookie = array();
    /**
     * @var array Version del sistema
     */
    public static $version = VERSION;
    /**
     * @var array Mensaje de pie de pagina
     */
    public static $titleMessage = 'Sistema Integrado de Protecci&oacute;n Venezolano';
    /**
     * @var array Mensaje de pie de pagina
     */
    public static $footerMessage = 'GNU GPLv3 - Copyright (C) 2012 Centro Nacional de Innovaci&oacute;n Tecnol&oacute;gica (CENIT)';
    /**
     * @var string mensaje de exito o error
     */
    public $mensaje = null;

    // ================ VALIDACIONES MEDIANTE EXPRESIONES REGULARES ============== //

    /**
     * Validacion de datos en formato de exprecion regular
     * @param string $text cadena de caracteres a evaluar
     * @return bool Devuelve verdadero si el dato es alfanumerico
     */
    function esAlfaNumerico($text){
        $regex = "/^[\w-]*[\d\w]*$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }
    /**
     * Validacion de datos en formato de exprecion regular
     * @param string $text cadena de caracteres a evaluar
     * @return bool Devuelve verdadero si el dato es alfanumerico
     */
    function esAlfaNumericoEspecial($text){        
        $regex = "/^[\d\w]{1,1}[\d\w\-\s\,\.áéíóúÁÉÍÓÚÑñ]{1,}$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }
    /**
     * Validacion de datos en formato de exprecion regular
     * @param string $text cadena de caracteres a evaluar
     * @return bool Devuelve verdadero si el dato es alfabetico
     */
    function esAlfabetico($text){
        $regex = "/^[\D\sáéíóúÁÉÍÓÚÑñ]{1,}$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }
    /**
     * Validacion de datos en formato de exprecion regular
     * @param string $text cadena de caracteres a evaluar
     * @return bool Devuelve verdadero si el dato es alfanumerico con espacios
     */
    function esAlfaNumericoConEspacios($text){
        //$regex = "/^[A-Za-z]{1,1}[A-Za-z0-9_\s\-]{1,}$/";
        $regex = "/^[\d\w_\s\-áéíóúÁÉÍÓÚÑñ]{1,}$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }
    /**
     * Validacion de datos en formato de exprecion regular
     * @param string $text cadena de caracteres a evaluar
     * @return bool Devuelve verdadero si el dato es alfanumerico con espacios
     */
    function esAlfaNumericoSinEspacios($text){
        //$regex = "/^[A-Za-z]{1,1}[A-Za-z0-9_\s\-]{1,}$/";
        $regex = "/^[\d\w_\-áéíóúÁÉÍÓÚÑñ]{1,}$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }

    /**
     * Validacion de datos en formato de exprecion regular
     * @param string $text cadena de caracteres a evaluar
     * @return bool Devuelve verdadero si el dato es numerico
     */
    function esNumerico($text){
        $regex = "/^[\d]{1,1}[\d]{0,}$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }
    /**
     * Formato de direccion IP
     */
    function esValidaIP($ipaddress){
        $regex = "/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/";
        if (!preg_match($regex, $ipaddress)) return FALSE;
        list($a,$b,$c,$d) = explode(".",$ipaddress);
        if($a>255 || $b>255 || $c>255 || $d>255 ) return false;
        return true;
    }
    /**
     * Formato de email
     */
    function esValidoEmail($text){
        $regex = "/^[\d\w_-]{2,}@[\d\w_-]{2,}\.[\w]{2,4}(\.[\w]{2,4})?$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }
    /**
     * Formato de tlf
     */
    function esValidoTlf($text){
        $regex = "/^[\d]{3,4}-[\d]{6,7}$/";
        if (!preg_match($regex, $text)) return FALSE;
        return true;
    }
    
    /**
    * Autenticacion de usuario y clave
    * @return boolean Retorna verdadero su los datos son correctos
    */
    public function userLogin(){
        
        if(!isset($_POST['usuario']) || trim($_POST['usuario'])===""){
            $this->mensaje = "Falta introducir el campo Usuario";
            return false;
        }
        if(!isset($_POST['clave']) || trim($_POST['clave'])===""){
            $this->mensaje = "Falta introducir el campo Contrase&ntilde;a";
            return false;
        }
        
        $row = Model::getUsuarioToLogin($_POST['usuario'], $_POST['clave']);
        if(!$row){                        
            if (DB_Class::$error){
                $this->mensaje = addslashes(DB_Class::$dbErrorMsg);
            }else{
                $this->mensaje = "Usuario o Clave incorrecta.";
            }            
            return false;
        }
        if($row->activo!="1"){
            $this->mensaje = "Usuario inactivo.";
            return false;
        }
        $_SESSION['usuario'] = trim($row->usuario);
        return true;
    }
    
    /**
     * Valida datos obligatorios y tipo de datos
     * @param array $datos Matriz que contiene los datos y el tipo de datos a validar
     * <br>@var boolean $row["isRequired"] Si es requerido  
     * <br>@var string $row["datoName"] Nombre del campo
     * <br>@var string $row["tipoDato"] Tipo de dato a validar
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function validarDatos($datos){
        foreach ($datos as $row){
            $dato = (isset($_REQUEST[$row["datoName"]]) && trim($_REQUEST[$row["datoName"]])!=='')? trim($_REQUEST[$row["datoName"]]) : null;
            if($dato===null && $row["isRequired"]===true){
                $this->mensaje = "Falta introducir el campo ".$this->getCampos($row["datoName"])." ";
                return false;
            }
            if ($row["tipoDato"]){
                if(!$this->$row["tipoDato"]($dato) && $dato != null){
                    $this->mensaje = "El campo ".$this->getCampos($row["datoName"])." introducido no es v&aacute;lido";
                    return false;
                }
            }            
        }
        return true;        
    }
    
    /**
     * Establece propiedad SELECT en objeto OPTION segun comparacion de valores
     * @param string $equal valor a comparar
     * @param string $valor valor a comparar
     * @return string Devuelve SELECTED si los valores $equal y $valor son iguales
     */
    static function busca_valor($equal, $valor ){
        //Si el parametro es un Arreglo
        if(is_array($equal)){            
            for ($i=0;$i< count($equal); $i++){
                if (trim($equal[$i]) == trim($valor)){
                    return "SELECTED";
                }
            }
        //Si el parametro es una variable simple
        }else{
            if (trim($equal) == trim($valor) ){
                return "SELECTED";
            }
        }
        return "";
    }

    /**
     * Generacion de listas dinamicas segun datos de base de datos
     * @param string $tabla tabla a consultar en base de datos
     * @param string $arg argumento consulta de base de datos
     * @param string $value campo de base de datos para valor de la opcion
     * @param string $descripcion campo de base de datos para descripcion que se muestra
     * @param string $equal valor a comparar por metodo busca_valor()
     * @param boolean $group valor de etiqueta de agrupacion <optgroup>
     * @return string Devuelve opciones de listas dinamicas HTML
     */
    static function make_combo($tabla,$arg, $value, $descripcion, $equal,$group=false){        
        $sql  = "select $value,$descripcion from $tabla $arg";
        // echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        $nrows = $res->rowCount();
        $fetchArray = $res->fetchAll();
        
        $resp = "";
        $vals = explode(',',$descripcion);
        if (count($vals)==1){ // si nada mas tiene un solo campo de descripcion a mostrar
            $group=false;
        }
        if ($nrows>0){
            foreach ($fetchArray as $row){

                if ($group){
                    $des_row = self::setNameMakeCombo($vals[0]);
                    if ($desc != $row[$des_row]){
                        $resp   .= "<optgroup label=\"".$row[$des_row]."\">";
                    }
                }

                $value = self::setNameMakeCombo($value);
                $resp .= "<option value=\"".trim($row[$value])."\" ".self::busca_valor($equal, $row[$value])." >";


                foreach ($vals as  $i => $des_row){
                    if ($group){
                        $i++;
                    }
                    $des_row = self::setNameMakeCombo($vals[$i]);
                    $resp .= "$row[$des_row] &nbsp;";
                }

                $resp .= "</option>\n";

                if ($group){
                    $des_row = self::setNameMakeCombo($vals[0]);
                    $desc = $row[$des_row];
                    if ($desc != $row[$des_row]){
                        $resp .= "</optgroup>";
                    }
                }
            }
        }else{
            $resp .= "<option value=\"\">No hay registros</option>";
        }
        return $resp;
    }

    /**
     * Quita el alias de la tabla o toma en cuenta el alias establecido para el nombre del campo
     * @param string $val Nombre del campo completo
     * @return string Devuelve el nombre del campo
     */
    public static function setNameMakeCombo($val){
        if (preg_match("/\sas\s/", $val)){
            $aux = @explode(" as ", $val);
        }else{
            $aux = @explode(".", $val);
        }
        return $aux[1] != "" ? @trim($aux[1]) : $val;
    }


    /**
     * Establece el formato de la fecha Y-m-d => d-m-Y o viseversa.
     * Horas opcional separadas por un espacio Y-m-d h:i:s => d-m-Y h:i:s
     * @param string $fecha Fecha
     * @return string Devuelve la fecha en el formato contrario
     */
    static function formatoFecha($fecha){
        if ($fecha){
            $fechaHora = explode(" ", trim($fecha));
            $aux = explode("-", $fechaHora[0]);
            $fecha = $aux[2]."-".$aux[1]."-".$aux[0];
        }        
        return trim($fecha." ".$fechaHora[1]);
    }
    
    /**
     * Process HTTP request.
     * @param string $url direccion a ejecutar
     * @param <type> $user Usuario Motion
     * @param <type> $pass Clave de usuario Motion
     * @param array $post of post data (or FALSE = cached get)
     * @return string Respuesta HTML del servidor 
     */
    static function httpRequest($url,$user,$pass, $post = NULL)
    {
        //echo "<p>$url,$user,$pass</p>";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERPWD, $user.":".$pass);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); // no echo, just return result
        if ($post) {
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        }

        $result = curl_exec($curl);
        
        if (curl_errno($curl) === 0) {
            //return "<p>Server response: <i>".$url." &lt;&ndash; ".strip_tags($result)."</i></p>";
            return $result;
        }
        return "<p>Invalid server response: <i>".$url."</i></p>";
    }

    /**
     * Verifica los permisos de un usuario para una catagoria y/o funcion
     * @param string $usuario Usuario logueado
     * @param string $categoria Categoria a verifiacar
     * @param string $idfuncion Funcion a verivicar
     * @return boolean Devuelve verdadero si el permiso existe
     */
    static function chkPermiso($usuario,$categoria,$idfuncion=""){
        //echo "$usuario,$categoria,$idfuncion";
        self::$mensajePermisos = null;
        $chk = false;
        $mensaje = null;
        $realPath = null;
        
        if (!self::$dataPermisos){
            //echo "<p>.:.Carga dataPermisos.:.</p>";
            self::$dataPermisos = Model::getGrupoFuncionUsr($usuario);
            self::$datosUsr = (object) array("usuario"=>self::$dataPermisos[0]->usuario,"nombre"=>self::$dataPermisos[0]->nombre,"apellido"=>self::$dataPermisos[0]->apellido,"superUsuario"=>self::$dataPermisos[0]->superUsuario);
        }
        
        //echo "<div align='left'><pre>".print_r(self::$dataPermisos,true)."</pre></div>";
        foreach (self::$dataPermisos as $row){
            if ($row->chkSuperUsuario > 1 || $row->activo == "0"){
                
                echo "<script type=\"text/javascript\">\n";
                if ($row->chkSuperUsuario > 1){
                    echo "    alert('Existe mas de un Super Usuario.\\nComun\\xedquese con el administrador del sistema.');\n";
                }elseif ($row->activo == "0"){
                    echo "    alert('Usuario Inactivo.');\n";
                }
                echo "    top.location.href = './login.php';";
                echo "</script>";
                
                $chk = false;
                break;
            }
            
            if ($row->superUsuario==1){
                $chk = true;
                break;
            }
            if ($categoria === $row->idcatPadre || ($categoria === $row->idcatHijo && $idfuncion=="") || ($categoria === $row->idcatHijo && $idfuncion === $row->idfuncion)){
                $chk = true;
                break;
            }
            
        }

        if (!$chk){
            $mensaje = "No tiene permisos para esta opci&oacute;n <b><i>".ucfirst($idfuncion)."</i></b>";
        }

        /**
         * Verificacion de Cookie para modulo de Monitoreo de Camaras
         * @todo Verificar permisologia de acceso por tipo de acceso: MonitorVid, AlarmasCtrl, Carnets
         */
        self::$dataAcc = Model::getAccesosPcsUsr($usuario);
        //echo "<div align='left'><pre>".print_r(self::$dataAcc,true)."</pre></div>";
        foreach (self::$dataAcc as $row){
            self::$chkCookie[$row->idaccesopc_tipo] = true;
            
            $row->cookie = $row->cookie ==""?"false":$row->cookie;            
            if (($_COOKIE[md5($_SESSION["usuario"]."-".$row->idaccesopc_tipo.$_SERVER["REMOTE_ADDR"])] != $row->cookie) &&                
                (self::$dataPermisos[0]->superUsuario==0) &&
                ($chk)){
                $realPath = implode("/",explode("/", $_SERVER["PHP_SELF"],-2))."/";
                if ($realPath==$row->path){
                    $mensaje = "El usuario <b><i>".$_SESSION["usuario"]."</i></b> no tiene Acceso a ".($row->descripcion)." en &eacute;ste computador\n";
                }                
                self::$chkCookie[$row->idaccesopc_tipo] = false;
                
            }
        }                
        
        self::$mensajePermisos  = "<div class=\"ui-widget\">\n";
        self::$mensajePermisos .= "    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">\n";
        self::$mensajePermisos .= "        <p>\n";
        self::$mensajePermisos .= "            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>\n";
        self::$mensajePermisos .= "                ".$mensaje;
        self::$mensajePermisos .= "        </p>\n";
        self::$mensajePermisos .= "    </div>\n";
        self::$mensajePermisos .= "</div>\n";
        
        return $chk;
    }

    public static function chkDirs($dir){
        
        if ($dir == "ADM"){
            $MainDirs = array("accesopc","grupo","plantafisica","segmento","usuario");//,"categoria","funcion"
        }
        if ($dir == "CAR"){
            $MainDirs = array("carcargo","cardepartamento","carplantilla","carempleado","carvisitante","carempleadolist");
        }
        if ($dir == "CTRL"){
            $MainDirs = array("ctrlcontroladora","ctrllectora","ctrlpuerta","ctrlgrupo","ctrlzonadetiempo","ctrldiafestivo","ctrlusuario","ctrllogacceso","ctrlmonitoracceso","ctrllogasistencia","ctrlmonitorasistencia");
        }
        if ($dir == "VID"){
            $MainDirs = array("videovigilancia","vidcamara","vidmarcacamara","vidmodelocamara","vidtipocamara","vidcamaraaxisguard","monitorremoto","monitorasoc","zonas","vidalarmaslist");
        }
        if ($dir == "ADM" || $dir == "CAR" || $dir == "CTRL" || $dir == "VID"){
            foreach ($MainDirs as $row){
                if (self::chkDirs($row)){
                    return true;
                }
            }
            return false;
        }
        
        $allDirs = scandir("../../");        
        if (in_array($dir,$allDirs)){
            return true;
        }        
        return false;
        
    }
    
    /**
     * Convierte unidades de medida desde el Byte hasta en YottaByte
     * @param int $size Numero de Medida a calcular
     * @return string Devuelve la medida calculada en KiloBytes, MegaBytes, GigaBytes... segun sea el caso
     */
    function file_size($size)
    {
        $filesizename = array(" Bytes", " KiloBytes", " MegaBytes", " GigaBytes", " TeraBytes", " PetaBytes", " ExaBytes", " ZettaBytes", " YottaBytes");
        return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
    }
    
    /**
     * Guarda archivos de logs en la carpeta /sip/inicio/logs/
     * @param string $filename Nombre del archivo a guardar
     * @param string $data Datos a guardar
     */
    public static function setLog($filename, $data){        
        $filename = dirname(__FILE__)."/../logs/".$filename;
        file_put_contents($filename, "[*Start*]\n".$data." --- ".date("Y-m-d H:i:s")."\n\n",FILE_APPEND);

        // Delete Duplicate Lines 
        $file = file_get_contents($filename, true);
        $file = explode("[*Start*]", $file);
        $file = array_unique($file);
        $file = implode("[*Start*]", $file);            
        file_put_contents($filename, $file);
    }
    
    /**
     *          *************************************
     *          *************************************
     *          *** METODOS MODULOS CONTROL ACCESO ***
     *          *************************************
     *          *************************************
     * 
     *          @author David Concepcion
     */
    /**
     * @var array grupo Codigos de Error del log de acceso
     */
    public static $error_codes = array("0","1","2","3","4","5","6","8","9","17","25","27","30","31","34","40","45","56","60","63","64","66","101");
    /**
     * @var array grupo Grupos de Acceso de puertas en hexadecimal
     */
    public $grupo = array( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
    /**
     * Instancia metodo agregarPuerta por cada fila del array
     * @param array $puertas Data de puetas a convertir 
     */
    public function agregarPuertas($puertas){
        foreach($puertas as $puerta) {
            $this->agregarPuerta($puerta);
        }
    }

    /**
     * Agrega la puerta al grupo 
     * @param int $puerta Numero de puerta
     * @return boolean Devuelve dalso si el numero de puerta esta fuera de rango
     */
    public function agregarPuerta($puerta){
        if($puerta<1 || $puerta>255) return null;
        $nbyte = floor($puerta/8);
        $puertas = $this->grupo[$nbyte];
        $npuerta = $puerta%8;
        $npuerta = pow(2,$npuerta);
        $puertas = $puertas | $npuerta;
        $this->grupo[$nbyte]=$puertas;
    }

    /**
     * Obtiene el valor del grupo de puertas
     * @return <type> 
     */
    public function getGrupoDePuertas(){
        return $this->grupo;
    }
    
    public function unsetGrupoDePuertas(){
        $this->grupo = array( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
    }

    /**
     * Empacado en un string de caracteres hexadecimales el grupo de puertas.
     * @return string Devuelve el grupo en caracteres hexadecimales
     */
    public function getGrupoStringEmpaquetado(){
        $packet = "";
        foreach ($this->grupo as $word) $packet .= sprintf("%02X",$word);
        return $packet;
    }
    /**
     *          *************************************
     *          *************************************
     *          *** METODOS MODULOS CARNETIZACION ***
     *          *************************************
     *          *************************************
     * 
     *          @author David Concepcion
     */
    
    /**
     * @var int defaultWidth Ancho del carnet por defecto
     */
    public static $defaultWidth  = DEFAULT_WIDTH;
    /**
     * @var int defaultWidth Alto del carnet por defecto
     */
    public static $defaultHeight  = DEFAULT_HEIGHT;    
    /**
     * @var string fixSize valor para redimencionar imagenes al imprimir
     */
    private $fixSize  = 2;
    /**
     * @var string folder carpeta de imagenes
     */
    private $folders  = null;
    /**
     * Establece la fixSize
     * @param string $fixSize fixSize
     */
    public function setFixSize($fixSize){
         $this->fixSize = $fixSize;
    }

    /**
     * @return string Devuelve la fixSize establecida
     */
    public function getFixSize(){
         return $this->fixSize;
    }
    /**
     * Establece la carpetas de imagenes
     * @param string $folders nombre de la carpetas
     */
    public function setFolders($folders){
         $this->folders = $folders;
    }

    /**
     * @return string Devuelve la carpeta de imagenes establecida
     */
    public function getFolders($op){
         return $this->folders[$op];
    }
    /**
     * Generacion de tabla de opciones de Estilos para el HTML para carnet
     * Invocada por el modulos para generacion de carnets
     * @param array $ops Vector de objetos a generar
     * @param object $data Datos almacenados en la BD
     * @return string Devuelve las filas de la tabla en HTML
     */
    static function setStyleInputs($ops,$data){

        $t = self::getTabulation("10");

        // --- Objs --- //
        $objs = array("_color","_fuentetamano","_fuenteletra","_bgcolor_chk","_bgcolor","_fuentealign","_fuentevalign");

        $str = "";
        foreach ($ops as $op){
            $str .= "\n".$t;
            $str .=  "<!------------------------------------------------------- zzzzzzzzzzz".strtoupper($op)." ------------------------------------------------------->\n".$t;            
            foreach ($objs as $obj){

                $class = "";
                if ($obj=="_bgcolor_chk"){
                    $class = "chkObj";
                }
                if ($obj!="_bgcolor_chk"){
                    $class = "styleObjs";
                }
                
                $objName  = $op.$obj;
                $str .= "<input type=\"hidden\" name=\"".$objName."\" id=\"".$objName."\" value=\"".$data->$objName."\" class=\"".$class."\" />\n".$t;
            }
        }
        return $str;
    }

    /**
     * Sets tabulation to 4 spaces
     * @param int $ntab Number of tabulations to return
     * @return string Tabulations in spaces
     */
    public static function getTabulation($ntab){
        for ($i=0;$i<($ntab*4);$i+=4){
            $tab .= "    ";
        }
        return $tab;
    }
    /**
     * Ensablaje de Dos imagenes segun parametros
     * @param array $param Parametros para ensamblaje de la imagen
     * @var array $param["data"] Datos de la consulta
     * @var resource $src_image Resource de la imagen origen
     * @var resource $param["dst_image"] Resource de la imagen destino
     * @var int $src_x coordenada x de la imagen origen
     * @var int $src_y coordenada y de la imagen origen
     * @var int $src_w Ancho de la imagen origen
     * @var int $src_h Alto de la imagen origen
     * @var int $dst_x coordenada x de la imagen destino
     * @var int $dst_y coordenada y de la imagen destino
     * @var int $dst_w Ancho de la imagen destino
     * @var int $dst_h Alto de la imagen destino
     * @return resourceId Devuelve la imagen ensamblada
     */
    function setImgage($param){

        if ($param["src_image"]){ // si la imagen es creada dinamicamente
            $src_image = $param["src_image"];
        }else{ // imagen alacenada en disco
            $src_image = imagecreatefromstring(file_get_contents($param["archivo"]));
        }       

        $src_x = 0;
        $src_y = 0;
        
        $nameOp = $param["obj"]."_left";
        $dst_x  = $param["data"]->$nameOp * $this->getFixSize(); // Coordenada X dentro de la imagen a mezclar

        $nameOp = $param["obj"]."_top";
        $dst_y  = $param["data"]->$nameOp * $this->getFixSize(); // Coordenada Y dentro de la imagen a mezclar

        $nameOp = $param["obj"]."_w";
        $dst_w  = $param["data"]->$nameOp * $this->getFixSize(); // Ancho dentro de la imagen a mezclar
        
        $nameOp = $param["obj"]."_h";
        $dst_h  = $param["data"]->$nameOp * $this->getFixSize(); // Alto dentro de la imagen a mezclar

        // Ancho y Alto de la imagen "fuente"
        if ($param["src_image"]){ // si la imagen es creada dinamicamente
            $src_w = imagesx($src_image) ;
            $src_h = imagesy($src_image) ;
        }else{ // imagen alacenada en disco
            list($src_w,$src_h) = getimagesize($param["archivo"]);
        }
        
        imagecopyresampled ( $param["dst_image"] , $src_image , $dst_x , $dst_y , $src_x , $src_y , $dst_w , $dst_h , $src_w , $src_h );

        return $param["dst_image"];
    }

    /**
     * Ensamblaje de una imagen con un texto
     * @param array $param Parametros para ensamblaje de la imagen
     * @var array $param["data"] Datos de la consulta
     * @var resource $param["dst_image"] Resource de la imagen destino
     * @var string $param["text"] Texto
     * @var int $text_x1 coordenada x superior izquierda del texto
     * @var int $text_y1 coordenada y superior izquierda del texto
     * @var int $text_x2 coordenada x inferior derecha del texto
     * @var int $text_y2 coordenada y inferior derecha del texto
     * @var int $text_w  Ancho del texto
     * @var int $text_h  Alto del texto
     * @var int $fontSize Tamaño del texto en Puntos
     * @var string $fontAlign Alineacion Horisontal del texto
     * @var string $fontValign Alineacion Vertical del texto
     * @var string $font Nombre de la fuente del texto
     * @var int $color Color de la fuente del texto
     * @var int $bgcolor Color del fondo de la fuente del texto
     * @return resourceId Devuelve la imagen ensamblada
     */
    function setText($param){
        
        $nameOp  = $param["obj"]."_left";
        $text_x1 = $param["data"]->$nameOp * $this->getFixSize();

        $nameOp  = $param["obj"]."_top";
        $text_y1 = $param["data"]->$nameOp * $this->getFixSize();

        $nameOp = $param["obj"]."_w";
        $text_w = $param["data"]->$nameOp * $this->getFixSize();

        $nameOp = $param["obj"]."_h";
        $text_h = $param["data"]->$nameOp * $this->getFixSize();
        
        $text_x2 = $text_x1 + $text_w;
        $text_y2 = $text_y1 + $text_h;
        
        $nameOp   = $param["obj"]."_fuentetamano";
        $fontSize = $param["data"]->$nameOp * $this->getFixSize();

        $nameOp    = $param["obj"]."_fuentealign";
        $fontAlign = $param["data"]->$nameOp;

        $nameOp     = $param["obj"]."_fuentevalign";
        $fontValign = $param["data"]->$nameOp;

        $nameOp = $param["obj"]."_fuenteletra";
        $font   = $param["data"]->$nameOp;
        
        $nameOp = $param["obj"]."_color";
        $color = $this->setHexColor($param["data"]->$nameOp);

        $nameOp = $param["obj"]."_bgcolor_chk";
        if ($param["data"]->$nameOp=="1"){
            $nameOp = $param["obj"]."_bgcolor";
            $bgcolor = $this->setHexColor($param["data"]->$nameOp);
            imagefilledrectangle($param["dst_image"], $text_x1, $text_y1, $text_x2, $text_y2, imagecolorallocate($param["dst_image"], hexdec($bgcolor["red"]), hexdec($bgcolor["green"]), hexdec($bgcolor["blue"])));
        }        
        $param["dst_image"] = $this->imageTextWrapped($param["dst_image"], $text_x1, $text_y1, $text_w,$text_h, $font.".ttf", $color, $param["text"], $fontSize, $fontAlign,$fontValign);

        return $param["dst_image"];
    }
    
    /**
     * Ensamblaje de texto preciso por piexeles en una imagen
     * @name imageTextWrapped
     * @link http://us.php.net/manual/en/function.imagettfbbox.php#60678
     * @param resource $img Resource de la imagen destino
     * @param int $x coordenada x del texto
     * @param int $y coordenada y del texto
     * @param int $width Ancho del texto
     * @param int $height Alto del texto
     * @param string $font Nombre de la fuente del texto
     * @param array $color Color de la fuente del texto en RGB
     * @param string $text Texto
     * @param int $textSize Tamaño del texto en puntos
     * @param string $align Alineacion Horisontal del texto
     * @param string $valign Alineacion Vertical del texto
     * @return resource Devuelve la imagen ensamblada
     */
    private function imageTextWrapped($img, $x, $y, $width,$height, $font, $color, $text, $textSize, $align="l", $valign="t") {

        $font = $this->getFolders("fonts").$font; // Folder that contains the fonts

        $width += $textSize/2; // fix position (DCF)
        $height+= $textSize/2; // fix position (DCF)
        $height -=5;           // fix position (DCF)

        //Recalculate X and Y to have the proper top/left coordinates instead of TTF base-point
        $y += $textSize ;
        $dimensions = imagettfbbox($textSize, 0, $font, " "); //use a custom string to get a fixed height.
        $x -= $dimensions[4]-$dimensions[0];

        $text = str_replace ("\r", '', $text); //Remove windows line-breaks
        $srcLines = explode ("\n", $text); //Split text into "lines"
        $dstLines = Array(); // The destination lines array.
        foreach ($srcLines as $currentL) {
            $line = '';
            $words = explode (" ", $currentL); //Split line into words.
            foreach ($words as $word) {
                $dimensions = imagettfbbox($textSize, 0, $font, $line.$word);
                $lineWidth = $dimensions[4] - $dimensions[0]; // get the length of this line, if the word is to be included
                if ($lineWidth > $width && !empty($line) ) { // check if it is too big if the word was added, if so, then move on.
                    $dstLines[] = ' '.trim($line); //Add the line like it was without spaces.
                    $line = '';
                }
                $line .= $word.' ';
            }
            $dstLines[] =  ' '.trim($line); //Add the line when the line ends.
        }
        //Calculate lineheight by common characters.
        $dimensions = imagettfbbox($textSize, 0, $font, "MXQJPmxqjp123"); //use a custom string to get a fixed height.
        $lineHeight = $dimensions[1] - $dimensions[5]; // get the heightof this line

        $lineCount = floor($height / $lineHeight);
        $lineCount = ($lineCount > count($dstLines)) ? (count($dstLines)) : ($lineCount);

        $align = strtolower(substr($align,0,1)); //Takes the first letter and converts to lower string. Support for Left, left and l etc.
        $valign = strtolower(substr($valign,0,1)); //Takes the first letter and converts to lower string. Support for Top => t, middle => m, etc.



        foreach ($dstLines as $nr => $line) {
            if ($align != "l") {
                $dimensions = imagettfbbox($textSize, 0, $font, $line);
                $lineWidth = $dimensions[4] - $dimensions[0]; // get the length of this line
                if ($align == "r") { //If the align is Right
                    $locX = $x + $width - $lineWidth;
                } else { //If the align is Center
                    $locX = $x + ($width/2) - ($lineWidth/2);
                }
            } else { //if the align is Left
                $locX = $x;
            }
            if ($valign != "t") {
                if ($valign == "m"){ //If the valign is Middle

                    $locY = $y + (($height - ($lineCount * $lineHeight)) / 2)+($nr * $lineHeight);

                }else{ //If the valign is Bottom
                    $locY = ($y + $height) - ($lineCount * $lineHeight) + ($nr * $lineHeight);
                }

            }else { //If the valign is Top
                $locY = $y + ($nr * $lineHeight);
            }


            //Print the line.            
            imagettftext($img, $textSize, 0, $locX, $locY, imagecolorallocate($img, hexdec($color["red"]), hexdec($color["green"]), hexdec($color["blue"])), $font, $line);
        }
        return $img;
    }

    /**
     * Descomposicion de un color en codigo RGB en un vector, una posicion por cada parte
     * @param int $color Color en RGB
     * @return array Devuelve un vector de 3 posiciones red, green y blue respectivamente
     */
    private function setHexColor($color){
        return array("red"=>substr($color, 0, 2),"green"=>substr($color, 2, 2),"blue"=>substr($color, 4, 2));
    }
    
    /**
     * Establece el alto y el ancho segun la orientacion del carnet
     * @param string $orientacion Orientacion preconfigurada
     * @return int Devuelve el alto y el ancho segun la orientacion
     */
    public function getFixedFormatCarnet($orientacion){
        
        $values = array();                
        $values["widthV"]  = self::$defaultWidth * $this->getFixSize();
        $values["heightV"] = self::$defaultHeight * $this->getFixSize();
        $values["widthH"]  = $values["heightV"];
        $values["heightH"] = $values["widthV"];        
        
        return array($values["width".$orientacion],$values["height".$orientacion]);
    }
    
    /**
     * Verifica si el navegador es Mobil 
     * @link http://detectmobilebrowsers.com/
     * @return boolean Devuelve verdadero si el navegador es Mobil
     */
    public static function chkMobilBrowser(){
        
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        
        return preg_match('/Android|android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
        
        //return true;
        
    }
   
    /**
     * Verifica si el modulo corresponde con el dispositivo que lo ejecuta 
     */
    public static function chkModuleDevice(){        
        if (preg_match("/^m./", basename($_SERVER["PHP_SELF"]))){
            if (!self::chkMobilBrowser()){
                //echo "go desktop (login)";
                header("location: login.php",true);
                return false;
            }            
        }else{
            if (self::chkMobilBrowser()){
                //echo "go mobile (login)";
                header("location: m.login.php",true);
                return false;
            }
        }
    }

    /**
     * Establece un contenedor para los mensajes informativos de error, informacion o exito
     * @return string Devuelve el contenedor HTML
     */
    public static function getMobilDivMensaje(){
        $str  = "<div data-role=\"page\" id=\"divmensaje\">\n";
        //$str .= "    <div data-role=\"header\" id=\"divmensajeHeader\"><h1></h1></div>\n";
        $str .= "    <div data-role=\"content\" id=\"divmensajeMain\">\n";
        $str .= "        <div id=\"divmensajeContent\" class=\"ui-body ui-body-c ui-corner-all\" ></div>\n";
        $str .= "        <a href=\"".basename($_SERVER["PHP_SELF"])."\" data-role=\"button\" data-theme=\"d\">Aceptar</a>\n";
        $str .= "    </div>\n";
        $str .= "</div>\n";
        return $str;
    }
    
    public static function getMobilUserInfo(){
        self::chkPermiso($_SESSION["usuario"], '');
        $datosUsr = self::$datosUsr;
       
        $str   = "<div id=\"userInfo\" class=\"ui-bar-a ui-corner-left\">\n";
        $str  .= "    ".$datosUsr->nombre." ".$datosUsr->apellido."\n";
        $str  .= "    <br/><small>".$datosUsr->usuario."</small>\n";
        $str  .= "    <a id=\"userInfoClose\" href=\"#\" data-role=\"button\" data-iconpos=\"notext\" data-icon=\"delete\"></a>\n";
        $str  .= "</div>\n";
        return $str;
    }
    
    /**
     * Establece un color RGB al azar sobre la paleta de colores estandar HTML
     * @return string Codigo Hexadecimal del Color RGB
     */
    public static function random_color(){
        mt_srand((double)microtime()*1000000);
        $code = '';
        while(strlen($code)<6){
            $code .= sprintf("%02X", mt_rand(0, 255));
        }
        return $code;
    }
}
?>