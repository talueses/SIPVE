<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php

class CamaraCommands {
    
    static $command;
    
    private $data = array();

    public function __set($name, $value)
    {
        //echo "Estableciendo '$name' igual a '$value'<br>";
        $this->data[$name]=$value;
    }
    public function __get($name)
    {
        //echo "Obteniendo '$name'-'".$this->data[$name]."'<br>";
        return $this->data[$name];
    }
    
    /**
     * Establece valores de opciones de comandos API para coontroles principales para una camara Axis 214 IPPTZ
     */
    public function axis214ipptz(){
        
        $this->scriptUrl = "/axis-cgi/com/ptz.cgi?";
        $this->maxPan    = "180";
        $this->minPan    = "-180";
        $this->maxTilt   = "90";
        $this->minTilt   = "-30";
        $this->maxZoom   = "9999";
        $this->minZoom   = "1";
        $this->maxFocus  = "9999";
        $this->minFocus  = "1";
        $this->maxIris   = "9999";
        $this->minIris   = "1";
        
        switch ($this->option){
            case "moveImg":
                $this->option = "center";
                break;
            case"upleft":
                $this->option = "move";
                $this->value = "upleft";
                break;
            case"up":
                $this->option = "move";
                $this->value = "up";
                break;
            case"upright":
                $this->option = "move";
                $this->value = "upright";                
                break;
            case"right":
                $this->option = "move";
                $this->value = "right";
                break;
            case"left":
                $this->option = "move";
                $this->value = "left";
                break;
            case"downleft":
                $this->option = "move";
                $this->value = "downleft";
                break;
            case"down":
                $this->option = "move";
                $this->value = "down";
                break;
            case"downright":
                $this->option = "move";
                $this->value = "downright";
                break;
            case"pan":
                $this->option = "pan";
                break;
            case"tilt":
                $this->option = "tilt";
                break;
            case"zoom":
                $this->option = "zoom";
                break;
            case"rzoom":
                $this->option = "rzoom";
                break;
            case"focus":
                $this->option = "focus";
                break;
            case"iris":
                $this->option = "iris";
                break;
            case"autofocus":
                $this->option = "autofocus";
                break;
            case"autoiris":
                $this->option = "autoiris";
                break;
            case"ircutfilter":
                $this->option = "ircutfilter";
                break;            
            case"position":
                $this->option = "query";
                $this->value  = "position";
                break;            
            case"setPosition":
                $this->scriptUrl = "/axis-cgi/com/ptzconfig.cgi?";
                $this->option = "setserverpresetname";
                break;
            case"getPosition":
                $this->option = "query";
                $this->value  = "presetposcam";
                break;
            case"gotoPosition":
                $this->option = "gotoserverpresetname";                
                break;
            case"delPosition":
                $this->scriptUrl = "/axis-cgi/com/ptzconfig.cgi?";
                $this->option = "removeserverpresetname";
                break;
        }
        
        $this->queryString = $this->option."=".$this->value;
        
        self::$command = $this->scriptUrl.$this->queryString;
        
    }
    
    /**
     * Establece valores de opciones de comandos de coontroles extra para una camara Axis 214 IPPTZ
     */
    function axis214ipptz_extra(){ 
        $this->axis214ipptz();
    }
    
    /**
     * Establece valores de opciones de comandos API para las Guardias de una camara Axis 214 IPPTZ
     */
    public function axis214ipptz_guardTour(){
        $this->scriptUrl = "/axis-cgi/admin/param.cgi?";
        
        if (count($this->value)>0){
            foreach ($this->value as $this->option => $value){
                //echo "<p>$this->option => $value</p>";
                $this->queryString .= "&".$this->option."=".urlencode($value);
            }
            $this->queryString = substr($this->queryString, 1);
        }else{
            return false;
        }        
        
        self::$command = $this->scriptUrl.$this->queryString;
        return true;
    }
}
?>