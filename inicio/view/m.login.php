<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../controller/controller.php";
Controller::chkModuleDevice(); // Verificar si el modulo Corresponde al dispositivo 
?>
<!DOCTYPE html> 
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1;text/html; charset=UTF-8;">         
        <title>Sistema Integrado de Seguridad</title>        
        <link rel="stylesheet" href="../css/mobile/jquery.mobile.structure.css" />           
        <link rel="stylesheet" href="../css/mobile/jquery.mobile.theme.css" />
        <link rel="stylesheet" href="../css/mobile/mobile.custom.css" />
        <link rel="stylesheet" href="../css/comunes.css" />
        <script src="../js/jquery.js"></script>        
        <script type="text/javascript" src="../js/jquery.mobile.detectmobile.js"></script> <!-- Detect Mobile -->
        <script src="../js/jquery.mobile.custom.js"></script>        
        <script src="../js/jquery.mobile.min.js"></script>              
        <link rel="stylesheet" href="../css/mobile/mobile.custom.css" /> <!-- Custom Style code -->                 
    </head>
    <body>
        <div data-role="page" data-theme="c" >
            <div id="headerMobile" data-role="header" data-theme="c" data-position="fixed">
                <img src="./../images/m.gobierno_bolivariano-1.png" width="135px">                   
                <img src="./../images/m.ministerio_energia_petroleo-1.png" width="179px">                
            </div>
            <div data-role="content" data-theme="d" >
                <div class="ui-body ui-body-d ui-corner-all">
                    <center>
                        <img src="../images/Video_surveillance-75x75.png" alt="" />
                        <br />
                        Sistema Integrado de Protecci&oacute;n
                    </center>
                    <br/>
                    
                    <div class=" ui-body-c ui-corner-all"> 
                        <div class=" ui-body-b ui-mini ui-corner-all titulo">Introduzca Usuario y Contrase&ntilde;a</div>
                        <div class="ui-body">
                            <form action="login.Op.php" name="f1" method="POST" target="ifrm1">                            
                                <div data-role="fieldcontain" >
                                    <label for="usuario">Usuario:</label>
                                    <input type="text" name="usuario" id="usuario"  data-mini="true"/>
                                </div>	
                                <div data-role="fieldcontain" >
                                    <label for="clave">Contrase&ntilde;a:</label>
                                    <input type="password" name="clave" id="clave" data-mini="true"/>
                                </div>
                                <input type="submit" name="acceder" id="acceder" value="Entrar" data-mini="true">
                            </form>
                        </div>                        
                    </div>                    
                </div>                
            </div>
            <div data-role="footer"  id="footerMobile"  data-mini="true" data-position="fixed" data-theme="c" > 
                <p><center><small><?php echo Controller::$footerMessage?></small></small></center></p>
            </div>
        </div>
        
        <?php echo Controller::getMobilDivMensaje(); ?>
        <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0" hidden ></iframe>
    </body>
</html>
