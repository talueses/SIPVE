<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../controller/controller.php";
session_start();
$_SESSION["usuasrio"] = $_SESSION["perPage"] = $_SESSION["page"] = $_SESSION['PHP_SELF'] = "";
Controller::chkModuleDevice(); // Verificar si el modulo Corresponde al dispositivo 
session_destroy (); 
?>
<html>
    <head>
        <title><?php echo Controller::$titleMessage." ".Controller::$version?></title>
        <link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link type="text/css" href="../css/jquery-ui.css" rel="stylesheet" />
        <link href="../css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        
        <style type="text/css">
            body{
                background:#dfdfdf;
                font-size:14px;
                font-family:arial,verdana,sans-serif;
            }

            #principal{
                width:1028px;
            }
            #titulo{
                
                height: 30px;
            }            
            div#cabecera{
                text-align: left;
                width:100%; 
                background: url('../images/separador-1.png') repeat-x ;
                height: 47px;
            }
            #divmensaje{
                width:1028px;
                position: absolute;
                top: 246px;
                opacity:0.9;
                cursor: pointer;
            }
        </style>
        <script type="text/javascript" >
            $(function() {
                $( "input:button, input:submit,button" ).button();
                $('#divmensaje').live({
                    click: function() {
                        $(this).fadeOut(500);
                    }
                });
            });
        </script>
        </head>
    <body >
        <center>
            <div id="principal" class="ui-corner-all  ui-widget-content" >
                <div id="cabecera" style="width:1028px;margin-top: 6px;">
                    <img src="./../images/gobierno_bolivariano-1.png" width="224px">
                    <img src="./../images/ministerio_energia_petroleo-1.png" width="291px">
                </div>
                <div style="padding-top:30px;font-size:150%; text-align:center;">
                    <img src="../images/SecurityCamera-150.png" alt="" />
                    <br />
                    <b><i>SIPve</i></b>
                    <br />
                    Sistema Integrado de Protecci&oacute;n Venezolano
                </div>
                <div id="divmensaje" ></div>
                <div id="formulario" style="padding-top:50px;padding-bottom:100px;"  align="center">
                    
                    <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no" hidden></iframe>
                    <form action="login.Op.php" name="f1" method="POST" target="ifrm1">
                        <table id="tabla" class="ui-corner-all ui-widget-content" border="0" cellpadding="2" cellspacing="2" style="width: 300px;">
                            <tr>
                                <td colspan="2" id="titulo" class="ui-widget-header ui-corner-all" align="center">
                                    Introduzca Usuario y Contrase&ntilde;a
                                </td>
                            </tr>
                            <tr>
                                <td width="50%"><div align="right">Usuario:</div></td>
                                <td width="50%"><input type="text" name="usuario" id="usuario"></td>
                            </tr>
                            <tr>
                                <td><div align="right">Contrase&ntilde;a:</div></td>
                                <td><input type="password" name="clave" id="clave"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="submit" name="acceder" id="acceder" value="Entrar" ></td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div id="piepagina" style="border-top:3px solid #d02433; width:100%; padding-bottom:50px; text-align:center">
                    <small><?php echo Controller::$footerMessage." ".Controller::$version?></small></small>
                </div>
            </div>
        </center>
    </body>
</html>
