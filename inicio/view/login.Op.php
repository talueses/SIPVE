<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
session_start();
require_once "../controller/controller.php";
$obj = new Controller();
$exito = $obj->userLogin();
?>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo ucfirst($_POST["accion"]);?> Servidores Grabadores de Video</title>
    <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
    <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
    
    <!-- Librerias Mobil -->
    <link rel="stylesheet" href="../css/mobile/jquery.mobile.structure.css" />           
    <link rel="stylesheet" href="../css/mobile/jquery.mobile.theme.css" />
    <script type="text/javascript" src="../js/jquery.mobile.detectmobile.js"></script> <!-- Detect Mobile -->
    <script src="../js/jquery.mobile.custom.js"></script>        
    <script src="../js/jquery.mobile.min.js"></script>
        
        
    <script type="text/javascript" language="javascript">
        $(function() {
            // --- Interfaz Mobil --- //
            if (jQuery.browser.mobile){
                <?php
                if(!$exito){
                    ?>                
                    $('#divmensajeMain',parent.document).addClass('ui-body-g');
                    $('#divmensajeContent',parent.document).html('<h3><center><?php echo $obj->mensaje; ?></center></h3>');
                    parent.$.mobile.changePage('#divmensaje', {role: 'dialog'});
                    <?php
                }
                if($exito){
                    echo "top.location.href='m.index.php';";
                }
                ?>                
            }else{
                // --- Interfaz PC --- //
                <?php
                echo "var mensaje = \"".$obj->mensaje."\";\n";
                echo "var str;\n";
                if($exito){
                    echo "top.location.href='index.php';";
                }
                if(!$exito){
                    echo "str  = '<div class=\"ui-widget\">';\n";
                    echo "str += '    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';\n";
                    echo "str += '        <p>';\n";
                    echo "str += '            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>';\n";
                    echo "str += '                '+mensaje;\n";
                    echo "str += '        </p>';\n";
                    echo "str += '    </div>';\n";
                    echo "str += '</div>';\n";

                }
                echo "$('#divmensaje',parent.document).html(str).fadeIn(500);\n";
                ?>
            }            
        });
    </script>
    </head>
</html>