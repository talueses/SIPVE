<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  * 
 \*******************************************************************************/
?>
<?php
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$opt = isset($_REQUEST['op']) ? "op".trim($_REQUEST['op']) : 'opInicio';
require_once "../controller/controller.php";
Controller::chkModuleDevice(); // Verificar si el modulo Corresponde al dispositivo 
//echo "<div align='left'><pre>".print_r($_SESSION,true)."</pre></div>";
?>
<html>
    <head>
        <title><?php echo Controller::$titleMessage." ".Controller::$version?></title>
        <link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        
        <link type="text/css" href="../css/jquery-ui.css" rel="stylesheet" />
        <link type="text/css" href="../css/ddsmoothmenu.css" rel="stylesheet" />
        <link type="text/css" href="../css/ddsmoothmenu-v.css" rel="stylesheet" />
        <link type="text/css" href="../css/comunes.css" rel="stylesheet" />
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/ddsmoothmenu.js"></script>
        
        <style type="text/css">
            body{
                background:#dfdfdf;
                font-size:14px; font-family:arial,verdana,sans-serif;                
            }

            #principal{
                width: 1300px;
                background:#ffffff;  
                border: 1px solid #cccccc;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            
            .mattblacktabs{
                width: 100%;
                overflow: hidden;
                border-bottom: 1px solid #d02433; /*bottom horizontal line that runs beneath tabs*/
            }

            .mattblacktabs ul{
                margin: 0;
                padding: 0;
                padding-left: 10px; /*offset of tabs relative to browser left edge*/
                /*font: bold 12px Verdana;*/
                list-style-type: none;
            }

            .mattblacktabs li{
                display: inline;
                margin: 0;
            }

            .mattblacktabs li a{
                float: left;
                display: block;
                text-decoration: none;
                margin: 0;
                padding: 7px 8px; /*padding inside each tab*/
                border-right: 1px solid white; /*right divider between tabs*/
                color: white;
                background: #414141; /*background of tabs (default state)*/
            }

            .mattblacktabs li a:visited{
                color: white;
            }

            .mattblacktabs li a:hover, .mattblacktabs li.selected a{
                color:red;
                background: #dfdfdf; /*background of tabs for hover state, plus tab with "selected" class assigned to its LI */
            }

            #cabecera {
                width:100%; 
                margin-top: 6px;
                background:#ffffff url('./../../inicio/images/separador.jpg') repeat-x right top;       
            }
            #menu{
                border-bottom: 1px solid #d02433; /*bottom horizontal line that runs beneath tabs*/
            }
            #menu li a.current{color:red;background: #dfdfdf;}

            #usrInfo{
                float: right;
                color: #fff;
                font-size: 11px ;
                font-style: oblique;
                /*background-color: #f0ebe2;*/
                opacity:0.9;
                width: 200px;
                height: 28px;
                border: 2px solid #ccc0a9;
                border-right: 0px;
                
                -moz-border-radius-bottomleft: 12px;
                -moz-border-radius-topleft: 12px;
                -webkit-border-bottom-left-radius:12px;
                -webkit-border-top-left-radius:12px;
                border-bottom-left-radius: 12px;
                border-top-left-radius: 12px;
            }
            #usrInfo:hover{opacity:1;background-color: #000;}

            #usrInfo small{color: #D3D3D3;}
            #usrInfo small#usuario a:hover{color: #ffffa7;cursor: pointer; }
            a:link, :visited { text-decoration: none; color: #D3D3D3;}            
            div#cabecera{
                text-align: left;
                width:100%; 
                background: url('../images/separador-1.png') repeat-x ;
                height: 47px;
            }         
        </style>
        <script type="text/javascript">
            ddsmoothmenu.init({
                    mainmenuid: "menu", //menu DIV id
                    orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
                    classname: 'ddsmoothmenu', //class added to menu's outer DIV
                    //customtheme: ["#1c5a80", "#18374a"],
                    contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
                    
            });
            $(function(){
                $("#menu a").unbind('click').click(function(){
                    $("#menu a").each(function(){
                        $(this).removeClass('selected');
                    });
                    $(this).addClass('selected');
                    $('#selectedModule').val($(this).attr('rel'));
                    
                    $('#mainFrame').attr('src',$(this).attr('href'));
                });
                //$('#opInicio').addClass('selected');
                var op = '<?php echo $opt?>';
                $('body').ready(function(){
                    //alert($('#'+op).attr('rel'));
                    
                    $('#selectedModule').val($('#'+op).attr('rel'));
                    $($('#'+op).attr('rel')).addClass('selected');
                    $('#'+op).addClass('selected');
                    //$('#'+op).trigger('click');
                    //$($('#'+op).attr('rel')).trigger('click');
                    $('#mainFrame').attr('src',$('#'+op).attr('href'));
                    
                });
            });
        </script>
    </head>

    <body>
        <center>
            <div id="principal" >
                <div id="cabecera" >                    
                    <img src="./../images/gobierno_bolivariano-1.png" width="224px">
                    <img src="./../images/ministerio_energia_petroleo-1.png" width="291px">
                    
                </div>	
                <?php
                //$dirs = scandir("../../");
                //echo "<div align='left'><pre>".print_r($dirs,true)."</pre></div>";
                ?>
                
                <div id="menu" >
                    <ul>
                        <li><a id="opInicio" href="./welcome.php" rel="#opInicio"  onclick="return false;" >Inicio</a></li>
                        <li>
                            <?php
                            if ((Controller::chkPermiso($_SESSION["usuario"], "ADM") || 1==1 )&&(Controller::chkDirs("ADM")) ){
                                ?>
                                <a id="opAdministracion" href="#" >Administraci&oacute;n</a>
                                <ul>
                                    <?                                    
                                    if (Controller::chkPermiso($_SESSION["usuario"], "ADMACCPC") && Controller::chkDirs("accesopc")){
                                        ?><li><a id="opAccesopc" href="../../accesopc/view/accesopc.php" onclick="return false;" rel="#opAdministracion">Acceso PC</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "ADMGPR") && Controller::chkDirs("grupo")){
                                        ?><li><a id="opGrupo" href="../../grupo/view/grupo.php" onclick="return false;" rel="#opAdministracion">Grupos Permisos Usr</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "ADMPLT") && Controller::chkDirs("plantafisica")){
                                        ?><li><a id="opPlantaFisica" href="../../plantafisica/view/plantafisica.php" onclick="return false;" rel="#opAdministracion">Planta F&iacute;sica</a></li><?php
                                    }
                                    
                                    if (Controller::chkPermiso($_SESSION["usuario"], "ADMSEG") && Controller::chkDirs("segmento")){
                                        ?><li><a id="opSegmento" href="../../segmento/view/segmento.php" onclick="return false;" rel="#opAdministracion">Segmentos</a></li><?php
                                    }
                                    if ((Controller::chkPermiso($_SESSION["usuario"], "ADMUSR") || 1==1 ) && (Controller::chkDirs("usuario"))){
                                        ?><li><a id="opUsuario" href="../../usuario/view/usuario.php" onclick="return false;" rel="#opAdministracion">Usuarios</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "")){                                        
                                        if ( Controller::chkDirs("categoria")){
                                            ?><li><a id="opCategoria" href="../../categoria/view/categoria.php" onclick="return false;" rel="#opAdministracion">Categoria</a></li><?php
                                        }
                                        if ( Controller::chkDirs("funcion")){
                                            ?><li><a id="opFuncion" href="../../funcion/view/funcion.php" onclick="return false;" rel="#opAdministracion">Funcion</a></li><?php
                                        }                                        
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                            
                            
                        </li>
                        <li>
                            <?php
                            if (Controller::chkPermiso($_SESSION["usuario"], "CAR") && Controller::chkDirs("CAR")){
                                ?>
                                <a id="opCarnetizacion" href="#" >Carnetizaci&oacute;n</a>
                                <ul>
                                    <?php
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CARCRG") && Controller::chkDirs("carcargo")){
                                        ?><li><a id="opCarCargo" href="../../carcargo/view/car_cargo.php" onclick="return false;" rel="#opCarnetizacion">Cargos</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CARDPT") && Controller::chkDirs("cardepartamento")){
                                        ?><li><a id="opCarDepartamento" href="../../cardepartamento/view/car_departamento.php" onclick="return false;" rel="#opCarnetizacion">Departamentos</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CARPLT") && Controller::chkDirs("carplantilla")){
                                        ?><li><a id="opCarPlantilla" href="../../carplantilla/view/car_plantilla.php" onclick="return false;" rel="#opCarnetizacion">Plantilla</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CAREMP") && Controller::chkDirs("carempleado")){
                                        ?><li><a id="opCarEmpleado" href="../../carempleado/view/car_empleado.php" onclick="return false;" rel="#opCarnetizacion">Carnets Empleados</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CARVIS") && Controller::chkDirs("carvisitante")){
                                        ?><li><a id="opCarVisitante" href="../../carvisitante/view/car_visitante.php" onclick="return false;" rel="#opCarnetizacion">Carnets Visitantes</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CARLISTEMP") && Controller::chkDirs("carempleadolist")){
                                        ?><li><a id="opCarVisitante" href="../../carempleadolist/view/car_empleadolist.php" onclick="return false;" rel="#opCarnetizacion">Listado Carnets Emp</a></li><?php
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </li>
                        <li>
                            <?php
                            if (Controller::chkPermiso($_SESSION["usuario"], "CTRL") && Controller::chkDirs("CTRL")){
                                ?>
                                <a id="opControlAcceso" href="#" >Control de Acceso</a>
                                <ul>
                                    <?php
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRLCTRL") && Controller::chkDirs("ctrlcontroladora")){
                                        ?><li><a id="opControladoras" href="../../ctrlcontroladora/view/ctrl_controladora.php" onclick="return false;" rel="#opControlAcceso">Controladoras</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRLLEC") && Controller::chkDirs("ctrllectora")){
                                        ?><li><a id="opLectoras" href="../../ctrllectora/view/ctrl_lectora.php" onclick="return false;" rel="#opControlAcceso">Lectoras</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRLPTS") && Controller::chkDirs("ctrlpuerta")){
                                        ?><li><a id="opPuertas" href="../../ctrlpuerta/view/ctrl_puerta.php" onclick="return false;" rel="#opControlAcceso">Puertas</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRLGPR") && Controller::chkDirs("ctrlgrupo")){
                                        ?><li><a id="opGrupos" href="../../ctrlgrupo/view/ctrl_grupo.php" onclick="return false;" rel="#opControlAcceso">Grupos</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRLHRS") && Controller::chkDirs("ctrlzonadetiempo")){
                                        ?><li><a id="opCtrlZonadetiempo" href="../../ctrlzonadetiempo/view/ctrl_zonadetiempo.php" onclick="return false;" rel="#opControlAcceso">Horarios de Trabajo</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRLDS") && Controller::chkDirs("ctrldiafestivo")){
                                        ?><li><a id="opCtrlDiafestivo" href="../../ctrldiafestivo/view/ctrl_diafestivo.php" onclick="return false;" rel="#opControlAcceso">D&iacute;as Festivos</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRLUSR") && Controller::chkDirs("ctrlusuario")){
                                        ?><li><a id="opCtrlUsuario" href="../../ctrlusuario/view/ctrl_usuario.php" onclick="return false;" rel="#opControlAcceso">Usuarios - Tarjetas</a></li><?php
                                    }
                                                                       
                                    //<li><a id="opHorariosTrabajo" href="../../ctrlhorario/view/ctrl_horario.php" onclick="return false;" rel="#opControlAcceso">Horarios de Trabajo</a></li>
                                    if (
                                        (Controller::chkPermiso($_SESSION["usuario"], "CTRLLOG") || Controller::chkPermiso($_SESSION["usuario"], "CTRLMON"))
                                        &&
                                        (Controller::chkDirs("ctrllogacceso") || Controller::chkDirs("ctrlmonitoracceso"))
                                       ){
                                    ?>
                                    <li>
                                        <a href="#">Acceso</a>
                                        <ul>
                                            <?php
                                            if (Controller::chkPermiso($_SESSION["usuario"], "CTRLLOG") && Controller::chkDirs("ctrllogacceso")){
                                                ?><li><a id="opCtrlLogacceso" href="../../ctrllogacceso/view/ctrl_logacceso.php" onclick="return false;" rel="#opControlAcceso">Listado de Eventos</a></li><?php
                                            }
                                            if (Controller::chkPermiso($_SESSION["usuario"], "CTRLMON") && Controller::chkDirs("ctrlmonitoracceso")){
                                                ?><li><a id="opCtrlMonitoracceso" href="../../ctrlmonitoracceso/view/ctrl_monitoracceso.php" onclick="return false;" rel="#opControlAcceso">Monitor Acceso</a></li><?php
                                            } 
                                            ?>
                                        </ul>
                                    </li>
                                    <?php
                                    }
                                    if (
                                        (Controller::chkPermiso($_SESSION["usuario"], "CTRLLOGA")||Controller::chkPermiso($_SESSION["usuario"], "CTRLMONA"))
                                         &&
                                        (Controller::chkDirs("ctrllogasistencia") || Controller::chkDirs("ctrlmonitorasistencia"))
                                       ){
                                    ?>
                                    <li>
                                        <a href="#">Asistencia</a>
                                        <ul>
                                            <?php
                                            if (Controller::chkPermiso($_SESSION["usuario"], "CTRLLOGA") && Controller::chkDirs("ctrllogasistencia")){
                                                ?><li><a id="opCtrlLogasistencia" href="../../ctrllogasistencia/view/ctrl_logasistencia.php" onclick="return false;" rel="#opControlAcceso">Listado de Asistencia</a></li><?php
                                            }
                                            if (Controller::chkPermiso($_SESSION["usuario"], "CTRLMONA") && Controller::chkDirs("ctrlmonitorasistencia")){
                                                ?><li><a id="opCtrlMonitorasistencia" href="../../ctrlmonitorasistencia/view/ctrl_monitorasistencia.php" onclick="return false;" rel="#opControlAcceso">Monitor Asistencia</a></li><?php
                                            } 
                                            ?>
                                        </ul>
                                    </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </li>                            
                        <li>
                            <?php
                            if (Controller::chkPermiso($_SESSION["usuario"], "VID") && Controller::chkDirs("VID")){
                                ?>
                                <a id="opVideoVigilancia" href="#" >Video Vigilancia</a>
                                <ul>
                                    <?php
                                    if (Controller::chkPermiso($_SESSION["usuario"], "VIDSRV") && Controller::chkDirs("videovigilancia")){
                                        ?><li><a id="opGrabadorVideo" href="../../videovigilancia/view/grabadorVideo.php" onclick="return false;" rel="#opVideoVigilancia" >Grabador de Video</a></li><?php
                                    }
                                    ?>                                    
                                    <li>
                                        <?php
                                        if (Controller::chkPermiso($_SESSION["usuario"], "VIDCAM") && Controller::chkDirs("vidcamara")){
                                            ?><a id="opCamaras" href="../../vidcamara/view/camaras.php" onclick="return false;" rel="#opVideoVigilancia">C&aacute;maras</a><?php
                                        }elseif ((Controller::chkPermiso($_SESSION["usuario"], "VIDCAMMRC") || Controller::chkPermiso($_SESSION["usuario"], "VIDCAMMOD") || Controller::chkPermiso($_SESSION["usuario"], "VIDCAMTIP") || Controller::chkPermiso($_SESSION["usuario"], "VIDGRD"))
                                                 &&
                                                 (Controller::chkDirs("vidmarcacamara") || Controller::chkDirs("vidmodelocamara") || Controller::chkDirs("vidtipocamara") || Controller::chkDirs("vidcamaraaxisguard"))
                                                ){
                                            ?><a id="opCamaras"  href="#" rel="#opVideoVigilancia">C&aacute;maras</a><?
                                        }
                                        
                                        if ((Controller::chkPermiso($_SESSION["usuario"], "VIDCAMMRC") || Controller::chkPermiso($_SESSION["usuario"], "VIDCAMMOD") || Controller::chkPermiso($_SESSION["usuario"], "VIDCAMTIP") || Controller::chkPermiso($_SESSION["usuario"], "VIDGRD"))
                                            &&
                                            (Controller::chkDirs("vidmarcacamara") || Controller::chkDirs("vidmodelocamara") || Controller::chkDirs("vidtipocamara") || Controller::chkDirs("vidcamaraaxisguard"))
                                           ){
                                            ?>
                                            <ul>
                                                <li>
                                                    <a href="#">Tipolog&iacute;a</a>
                                                    <ul>
                                                        <?php
                                                        if (Controller::chkPermiso($_SESSION["usuario"], "VIDCAMMRC") && Controller::chkDirs("vidmarcacamara")){
                                                            ?><li><a id="opMarca" href="../../vidmarcacamara/view/vid_marca_camara.php" onclick="return false;" rel="#opVideoVigilancia">Marca</a></li><?php
                                                        }
                                                        if (Controller::chkPermiso($_SESSION["usuario"], "VIDCAMMOD") && Controller::chkDirs("vidmodelocamara")){
                                                            ?><li><a id="opModelo" href="../../vidmodelocamara/view/vid_modelo_camara.php" onclick="return false;" rel="#opVideoVigilancia">Modelo</a></li><?php
                                                        }
                                                        if (Controller::chkPermiso($_SESSION["usuario"], "VIDCAMTIP") &&  Controller::chkDirs("vidtipocamara")){
                                                            ?><li><a id="opTipo" href="../../vidtipocamara/view/vid_tipo_camara.php" onclick="return false;" rel="#opVideoVigilancia">Tipo</a></li><?php
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>
                                                <?php
                                                if (Controller::chkPermiso($_SESSION["usuario"], "VIDGRD") && Controller::chkDirs("vidcamaraaxisguard")){
                                                    ?><li><a id="opVidCamaraAxisguard" href="../../vidcamaraaxisguard/view/vid_camara_axisguard.php" onclick="return false;" rel="#opVideoVigilancia">Guardias</a></li><?php
                                                }
                                                ?>
                                            </ul>
                                            <?php
                                            }
                                        ?>
                                    </li>                                    
                                    <?php
                                    /*if (Controller::chkPermiso($_SESSION["usuario"], "VIDACCPC")){
                                        ?><li><a id="opVidAccesopc" href="../../vidaccesopc/view/vid_accesopc.php" onclick="return false;" rel="#opVideoVigilancia">Acceso Monitoreo</a></li><?php
                                    }*/
                                    if (Controller::chkPermiso($_SESSION["usuario"], "VIDMNTR") && Controller::chkDirs("monitorremoto")){
                                        ?><li><a id="opMonitorRemoto" href="../../monitorremoto/view/setPlantilla.php" onclick="return false;" rel="#opVideoVigilancia">Monitor Remoto</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "VIDASCMNTR") && Controller::chkDirs("monitorasoc")){
                                        ?><li><a id="opAsociarMonitoreo" href="../../monitorasoc/view/monitorasoc.php" onclick="return false;" rel="#opVideoVigilancia">Asociar Monitoreo</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "VIDZN") && Controller::chkDirs("zonas")){
                                        ?><li><a id="opZonas" href="../../zonas/view/zonas.php" onclick="return false;" rel="#opVideoVigilancia">Zonas de Monitoreo</a></li><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], "VIDALARM") && Controller::chkDirs("vidalarmaslist")){
                                        ?><li><a id="opAlarmasList" href="../../vidalarmaslist/view/vid_alarmaslist.php" onclick="return false;" rel="#opVideoVigilancia">Listado de Alarmas</a></li><?php
                                    }
                                    ?>
                                    
                                </ul>
                                <?php
                            }                            
                            ?>
                        </li>
                        <?php
                        
                        if (Controller::chkPermiso($_SESSION["usuario"], "") && Controller::chkDirs("generateObj")){
                            ?><li><a target="_blank" href="../../generateObj/view/generateAcc.php">Code Generator</a></li><?php
                            
                        }
                        ?>
                        
                        <li><a href="./login.php" >Salir</a></li>
                    </ul>
                    <div id="usrInfo" align="right">
                        <?php
                        $datosUsr = Controller::$datosUsr;
                        echo $datosUsr->nombre." ".$datosUsr->apellido
                        ?>
                        <small style="float: left;margin-left: 5px;margin-top: 0px;"><?php echo $datosUsr->superUsuario=="1"?"<img src=\"../images/User-32.png\" alt=\"\" title=\"Super Usuario\" />":"";?></small>
                        <br>
                        
                        <small id="usuario" title="Modificar Usuario"><a href="../../usuario/view/usuario.php" onclick="return false;" rel="#opAdministracion"><?php echo $datosUsr->usuario;?></a></small>
                    </div>
                    
                    <br style="clear: left" />

                </div>
                <input type="hidden" name="selectedModule" id="selectedModule">
                <div id="iframe">
                    <iframe name="mainFrame" id="mainFrame" frameborder="0" scrolling="no" style="width:100%; height:650px" src="./welcome.php"></iframe>
                </div>
                <div id="piepagina" style="border-top:3px solid #d02433; width:100%; padding-bottom:50px; text-align:center">
                    <small><?php echo Controller::$footerMessage." ".Controller::$version?></small></small>
                </div>	
            </div>
        </center>
    </body>

</html>