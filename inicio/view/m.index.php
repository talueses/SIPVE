<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}	
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
require_once "../controller/controller.php";
Controller::chkModuleDevice(); // Verificar si el modulo Corresponde al dispositivo 
?>
<!DOCTYPE html> 
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1;text/html; charset=UTF-8;">         
        <title>Sistema Integrado de Seguridad</title>        
        <link rel="stylesheet" href="../css/mobile/jquery.mobile.structure.css" />           
        <link rel="stylesheet" href="../css/mobile/jquery.mobile.theme.css" />
        <link rel="stylesheet" href="../css/mobile/mobile.custom.css" />
        <link rel="stylesheet" href="../css/comunes.css" />
        <script src="../js/jquery.js"></script>        
        <script type="text/javascript" src="../js/jquery.mobile.detectmobile.js"></script> <!-- Detect Mobile -->
        <script src="../js/jquery.mobile.custom.js"></script>        
        <script src="../js/jquery.mobile.min.js"></script>              
        <link rel="stylesheet" href="../css/mobile/mobile.custom.css" /> <!-- Custom Style code -->                 
        <style type="text/css">
        </style>
    </head>
    <body>
        <div data-role="page" data-theme="c" >
            <div id="headerMobile" data-role="header" data-theme="c" data-position="fixed" class="ui-header-fixed"> 
                <img src="./../images/m.gobierno_bolivariano-1.png" width="135px">                   
                <img src="./../images/m.ministerio_energia_petroleo-1.png" width="179px"> 
                <?php echo Controller::getMobilUserInfo();?>
            </div>
            <div data-role="content" data-theme="d" >
                <div data-role="collapsible-set">
                    <?php
                    if (Controller::chkPermiso($_SESSION["usuario"], "ADM")){
                        ?>
                        <div data-role="collapsible" data-theme="a" data-content-theme="a" data-mini="true" data-iconpos="right">
                            <h3>Administraci&oacute;n</h3>
                        </div>
                        <?php
                    }
                    if (Controller::chkPermiso($_SESSION["usuario"], "CAR")){
                        ?>                    
                        <div data-role="collapsible" data-theme="a" data-content-theme="a" data-mini="true" data-iconpos="right">
                            <h3>Carnetizaci&oacute;n</h3>
                        </div>
                        <?php                    
                    }
                    if (Controller::chkPermiso($_SESSION["usuario"], "CTRL")){
                        ?>
                        <div data-role="collapsible" data-theme="a" data-content-theme="a" data-mini="true" data-iconpos="right">
                            <h3>Control de Acceso</h3>
                        </div>
                        <?php
                    }
                    if (Controller::chkPermiso($_SESSION["usuario"], "VID")){
                        ?>
                        <div data-role="collapsible" data-theme="a" data-content-theme="a" data-mini="true" data-iconpos="right">
                            <h3>Video Vigilancia</h3>   
                            <ul data-role="listview" data-theme="a" data-inset="true" >
                                <?                                    
                                if (Controller::chkPermiso($_SESSION["usuario"], "VIDEXCAM")){
                                    ?><li><a href="../../m.vidcamaraExport/view/m.vidcamaraExport.php" rel="external">Exportar C&aacute;maras</a></li><?php
                                }
                                ?>                                    
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <ul data-role="listview" data-theme="a" class="ui-corner-all" data-inset="true">
                    <li><a href="m.login.php" rel="external">Salir</a></li>
                </ul>
            </div>            
            <div data-role="footer" id="footerMobile"  data-mini="true" data-position="fixed" data-theme="c"> 
                <p><center><small><?php echo Controller::$footerMessage?></small></center></p>
            </div>
        </div>
    </body>
</html>
