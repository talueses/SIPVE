======== PAQUETES A INSTALAR ===============

- php5
- apache2 (servidor web y servidores motion)
- postgresql
- php5-pgsql
- mysql-server
- php5-mysql
- ssh (en servidor web y servidores motion)
- motion (en servidores motion)
- python
    - pexpect
- php5-curl
- php5-gd

- Permisologia a todos los directorios

    chown -R www-data:www-data /var/www/sipve/
    chmod -R 764 /var/www/sipve/

    * Espesificamente www-data debe tener permisologia de lectura y escritura para estos directorios:
        - /var/www/sip/videovigilancia/motion
        - /var/www/sip/vidcamara/motion
        - /var/www/sip/vidcamara/motion_events
        - /var/www/sip/plantafisica/images
        - /var/www/sip/inicio/logs

======== CONFIGURAR =======================
- crontab en el servidor web 
    =>  * * * * *  wget  -q --spider http://DIRECCION DNS O IP/NOMBRE-DIRECTORIO-SIPVE/inicio/shell_script/cron.scripts.php;
        Cada un minuto se ejecutara script de establecer la ejecucion de eventos por horarios para cada camara del sistema
        Las camaras que no tengan horarios establecidos no guardaran eventos al estar activo este script
- en el servidor de aplicaciones:
    nano /etc/wgetrc
    use_proxy = off

- crear enlase sinbolico en los servidores motion a la carpeta donde se guarden los videos:
    ln -s "/home/user/motion" /var/www/
 
  Reiniciar el apache

===================

La tipologia especifica para las camaras PTZ probadas en el desarrollo es:

Marca:  axis
Modelo: 214
Tipo:   ipptz

La librerias de control tienen este nombre par que los controles funcionen



