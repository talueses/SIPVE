#! /bin/bash
script='access_controller_comunicator'

process="false"


start(){
    
    if [ "${process}" = "false" ]
    then
        $(./$script) & 2>/dev/null 
    fi
    chkProcess
    if [ "${process}" = "false" ]
    then    
        printf "\n\t[ERROR] *** Process not Started ***\n\n"
    else
        printf "\n\t*** Process Started ***\n\n"
    fi
}

stop(){

    if [ "${process}" = "true" ]    
    then
        $(killall ${script})
    fi
    chkProcess
    if [ "${process}" = "true" ]
    then    
        printf "\n\t[ERROR] *** Process not Stopped ***\n\n"
    else
        printf "\n\t*** Process Stopped ***\n\n"
    fi
}
restart(){
    stop
    start    
}
status(){    
        
    if [ "${process}" = "true" ]
    then        
        printf "\nProcess Running\n\n"
    else
        printf "\nProcess Stopped\n\n"
    fi
}

chkProcess(){
    if ps ax | grep -v grep | grep ${script} > /dev/null
    then
        process="true"
    else
        process="false"
    fi
    
}

##########################################
# Purpose: Describe how the script works
# Arguments:
#   None
##########################################
usage()
{
    #echo ""
    printf "\n\t Usage: $0 start|stop|restart|status \n\n"
    #SELF=$(cd $(dirname $0); pwd -P)/$(basename $0)
    #echo "Usage: $SELF start|stop|restart|reload|force-reload|status"
    
}

### Evaluate the options passed on the command line
#clear
chkProcess
case "${1}" 
in
    start) start
        ;;
    stop) stop
        ;;
    status) status
        ;;
    restart) restart
        ;;
    *) usage
        exit 1
        ;;
esac


### Exit with a success indicator
exit 0