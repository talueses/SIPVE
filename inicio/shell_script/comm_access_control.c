
/*
comm_access_control.c
para compilar use...
g++ comm_access_control.c -o access_controller_comunicator -I/usr/include/mysql -lmysqlclient

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <math.h>

#include <sys/socket.h>
#include <unistd.h>
#include <mysql.h>

#define buflen 1024
#define debuga 1

void dbconnect(MYSQL *conn); //funcion para la conexion a la base de datos...
void dbping(MYSQL *conn);
void escribirlog(MYSQL *conn, unsigned char log[], int len);
void damelogtraducido(int funcion, char mensaje[]);
void mainfork(char ipv4[], int puerto);
int dameUltimolog(unsigned char nodo, int sd, MYSQL *conn);
void logNodoNoDisponible(MYSQL *conn, int nodo, char ipv4[], int puerto);
int sincronizarConBasedatos(char ipv4[], int puerto, unsigned char nodo, int sd, MYSQL *conn);
int crear_diaferiado(int sd, unsigned char nodo, char param1[], char param2[], char param3[]);
int timezones(	int sd, unsigned char nodo, char param1[], char param2[], char param3[], char domingodesde[], char domingohasta[], char lunesdesde[], char luneshasta[], char martesdesde[], char marteshasta[], char miercolesdesde[], char miercoleshasta[], char juevesdesde[], char jueveshasta[], char viernesdesde[], char vierneshasta[], char sabadodesde[], char sabadohasta[], char enlace[]);
int sincronizarRelog(int sd, unsigned char nodo, MYSQL *conn);
int enviarTrama(int sd, unsigned char nodo, unsigned char Tx[], int Txlen, unsigned char buf[]);
int escribirusuario(int sd, unsigned char nodo, char idusuario[], char pin[], char idcard[], char idsite[], char card_pin[], char limitefecha[], char changeable[], char guard[], char skipfpcheck[], char grupopuerta[], char fechahasta[], char zonadetiempo[], char niveldelusuario[], char antipassback[]);




int abrirpuerta(int sd, unsigned char nodo,  unsigned char numlectora);
int abrirPuertaDadoNumLectora(int sd, unsigned char nodo, char lectora[], char idsincronizacion[], MYSQL *conn);
int abrirPuertaDadoNumPuerta(int sd, unsigned char nodo, char puerta[], char idsincronizacion[], MYSQL *conn);
int abrirtodaslaspuerta(int sd, unsigned char nodo, char idsincronizacion[], MYSQL *conn);
int armarlectora(int sd, unsigned char nodo, char idsincronizacion[], char numlectora[], MYSQL *conn);
int desarmarlectora(int sd, unsigned char nodo, char idsincronizacion[], char numlectora[], MYSQL *conn);

int abrirpuertapermanente(int sd, unsigned char nodo,  unsigned char numlectora);
int cerrarpuerta(int sd, unsigned char nodo,  unsigned char numlectora);
int cerrarPuertaDadoNumPuerta(int sd, unsigned char nodo, char puerta[], char idsincronizacion[], MYSQL *conn);
int cerrarPuertaDadoNumLectora(int sd, unsigned char nodo, char lectora[], char idsincronizacion[], MYSQL *conn);
int cerrartodaslaspuerta(int sd, unsigned char nodo, char idsincronizacion[], MYSQL *conn);
int estadodelectora(int sd, unsigned char nodo, char idsincronizacion[], char lectora[], MYSQL *conn);

int enviarcomandorelay( int sd, unsigned char nodo, unsigned char lectora, unsigned char parametro, unsigned char buf[]);




//entrega de julio - agosto del 2011
int obtenerEstadoDeLectoras(int sd, unsigned char nodo, unsigned char buf[]);

//entrega de julio - agosto del 2011
int dameUltimolog(unsigned char nodo, int sd, MYSQL *conn);

//entrega de julio - agosto del 2011
int config(void);

//entrega de julio - agosto del 2011
int enviarGrupoDePuertas(int sd, unsigned char nodo, char idx[], char link[], char level[], char grupo1[], char grupo2[]);

//entrega de julio - agosto del 2011
int leerConfiguracion(int sd, unsigned char nodo, unsigned char buf[]);

//entrega de julio - agosto del 2011
int asociarLectoraConPuerta(int sd, unsigned char nodo, char p1[], char p2[], char p3[], char p4[], char p5[], char p6[], char p7[], char p8[], char p9[], char p10[], char p11[], char p12[], char p13[], char p14[], char p15[], char p16[]);

//entrega de julio - agosto del 2011
int obtenerLectorasOnline(int sd, unsigned char nodo, MYSQL *conn);


char dbhost[100];
char dbuser[100];
char dbpassword[100];
char dbname[100];

main() {
	config();


	MYSQL *conn;
	MYSQL_RES *result;
	MYSQL_ROW row;
	int num_fields;
	if(debuga) printf("debuga:%d",debuga);else printf("no hay debuga");
	conn = mysql_init(NULL);
	if (conn == NULL) {
		printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	dbconnect(conn);
	if(debuga) printf("conectado a la base de datos..para el proceso padre\n");

	/*
	Aqui hay que hacer una busqueda en la base de datos para traerse los nodos que existen para crear un socket por cada uno de ellos.
	y crear un thread para cada uno de los nodos para que se encargue de la comunicacion nodo base de datos.
	Por cada uno de los nodos voy a crear un thread.
	Quiero utilizar una sola conexion de base de datos para todos los threads. Vamos a ver como hago esto...

	*/
	if(mysql_query(conn, "SELECT DISTINCT ipv4, puerto FROM ctrl_controladora")){
		printf("Error: %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	result = mysql_store_result(conn);
	num_fields = mysql_num_fields(result);
	int puerto=0;
	char ipv4[100];
	char str[10];
	while ((row = mysql_fetch_row(result)))	{
		if(debuga) printf("leido de la base de datos: ipv4=%s, puerto: %s\n",row[0],row[1]);

		/* por cada direccion ip y puerto debo establecer un socket para que se establezca el canal para extraer logs y para hacer download hacia la controladora.*/
		
		// asignacion de la direccion ip proveniente del query a la variable ipv4
		sprintf(ipv4,"%s",row[0]);
		if(debuga) printf("asignacion: ipv4=%s\n",ipv4);

		// asignacion del numero de puerto proveniente del query a la variable puerto.
		sprintf(str,"%s",row[1]);
		sscanf(str,"%d",&puerto);
		
		if(fork()){
			if(debuga) printf("Arrancando proceso para atender al nodo %s por el puerto %d\n",ipv4,puerto);
			mainfork(ipv4,puerto);
			exit(0);
		}
	}

	mysql_free_result(result);
	mysql_close(conn);
	if(debuga) printf("cerrada la conexion con la base de datos para el padre de los procesos\n");
	exit(0);
}




void mainfork(char ipv4[], int puerto){
	MYSQL *conn;
	MYSQL_RES *result;
	MYSQL_ROW row;
	int num_fields;

	conn = mysql_init(NULL);
	if (conn == NULL) {
		printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	dbconnect(conn);
	if(debuga) printf("establecida la conexion con la base de datos para un proceso hijo\n");
	//hay que buscar en la base de datos cuantos nodos estan atados a esta direccion IP y a este puerto....
	//ya que el sistema tiene que extraer informacion de esos nodos.
	//mysql_query(conn, "SELECT nodo, ipv4, puerto FROM ctrl_nodo");
	char query[1024];
	sprintf(query, "SELECT nodo FROM ctrl_controladora where ipv4='%s' and puerto=%d", ipv4, puerto);
	if(mysql_query(conn, query)){
		printf("Error1 %u: %s\n", mysql_errno(conn), mysql_error(conn));
		return;
	}
	result = mysql_store_result(conn);
	unsigned int nodos[250];
	int numnodos=0;
	while ((row = mysql_fetch_row(result)))	{
		// asignacion del nodo proveniente del query a la variable nodo.
		int nodo;
		char str[3];
		sprintf(str,"%s",row[0]);
		sscanf(str,"%d",&nodo);
		nodos[numnodos]=nodo;
		numnodos++;
	}
	mysql_free_result(result);
	// ya extraje de la base de datos los nodos asociados a la direccion ip y al puerto. Estos estan almacenados en el arreglo nodos[]

	struct sockaddr_in sin;
	struct hostent *host = gethostbyname(ipv4);

	/*** COLOCA LA DATA EN sockaddr_in struct ***/
	memcpy(&sin.sin_addr.s_addr, host->h_addr, host->h_length);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(puerto);

	// En vista de que tengo conexion con la base de datos voy a crear un socket para comunicarme con la controladora..
	int sd = socket(AF_INET, SOCK_STREAM, 0);  /* init socket descriptor */
	/*** CONECTA EL SOCKET AL SERVICIO DESCRITO POR sockaddr_in struct ***/
	if (connect(sd, (struct sockaddr *)&sin, sizeof(sin)) < 0)     {
		char error[50];
		sprintf(error, "\nERROR CONECTANDO A %s POR EL PUERTO %d",ipv4,puerto);
		perror(error);
		exit(1);
	}
	sleep(1);   /* give server time to reply */
	printf("\nCONECTANDO A %s POR EL PUERTO %d\n",ipv4,puerto);
	// AQUI YA ESTA ESTABLECIDA LA CONEXION CON LA CONTROLDORA POR MEDIO DEL SOCKET.

	int pingvar =0;
	while(1){
		// verificar que tengo nodos a quien preguntar, sino entonces salgo del programa...
		int numnodosactivos = 0;
		for(int k=0; k<numnodos && nodos[k]!=0; k++){
			numnodosactivos++;
			unsigned char nodo = nodos[k];

			unsigned char buf[buflen];
			int len;

			int t=dameUltimolog(nodo, sd, conn);
			if(t==-1){
				// escribir a la base de datos que este nodo no esta disponible en la ip y puerto especificado..
				logNodoNoDisponible(conn, nodo, ipv4, puerto);

				// quito al nodo que esta dando problemas del arreglo de nodos.
				nodos[k]=0;

				// se tiene que reestablecer la conexion con el socket remoto.
				close(sd);
				sd = socket(AF_INET, SOCK_STREAM, 0);  /* init socket descriptor */
				/*** CONECTA EL SOCKET AL SERVICIO DESCRITO POR sockaddr_in struct ***/
				if (connect(sd, (struct sockaddr *)&sin, sizeof(sin)) < 0)     {
					perror("conectando");
					exit(1);
				}
			}else{
				// busco en la base de datos por informacion a bajar al nodo k (nodo que se procesa en esta iteracion)
				t=sincronizarConBasedatos(ipv4, puerto, nodo, sd, conn);
				sleep(3);
			}
		}		
		if(numnodosactivos==0) break;
		if(pingvar++>10){ dbping(conn); pingvar=0;}

	}
	close(sd);
	mysql_close(conn);
	printf("cerrada la conexion con la base de datos para un proceso hijo\n");
	exit(0);
}


int sincronizarConBasedatos(char ipv4[], int puerto, unsigned char nodo, int sd, MYSQL *conn){
	MYSQL_RES *result;
	MYSQL_ROW row;	

	int num_fields;
	char query[1024];
	char str[3];
	/*
	este procedimiento o funcion hay que arreglarlo, repararlo para que busque los items en la base de datos por prioridad y fecha de creacion. de manera que se atiendan los prioritarios primeros 
	como el caso de abrir puertas. las puertas no deberian esperar para ser abiertas...
	SELECT * FROM `ctrl_sincronizar` ORDER BY prioridad desc, id  asc
	*/
	//sprintf(query, "SELECT * FROM ctrl_sincronizar where ipv4='%s' and puerto=%d and nodo=%d order by prioridad desc, id  asc LIMIT 0,1", ipv4, puerto, nodo);//este es el query definitivo
	sprintf(query, "SELECT * FROM ctrl_sincronizar where ipv4='%s' and puerto=%d and nodo=%d order by ID desc LIMIT 0,1", ipv4, puerto, nodo);//este es el query mientras se desarrolla..
	//mysql_query(conn, query);

	if(mysql_query(conn, query)){
		printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
		return 0;
	}
	result = mysql_store_result(conn);
	row = mysql_fetch_row(result);
	if(row==NULL) {// En este caso salimos porque en la base de datos no hay registros para sincronizar con este nodo... 
		printf("No hay nada que sincronizar para el nodo %d. Saliendo de sincronizar\n", nodo);
		return 1; 
	}
	int id;
	int comandovalido = 1;
	int codigoaccion;
	int retorno;
	char idsinc[50], p1[50], p2[50], p3[50], p4[50], p5[50], p6[50], p7[50], p8[50], p9[50], p10[50], p11[50], p12[50], p13[50], p14[50], p15[50], p16[50], p17[50], p18[50];

	sprintf(idsinc,"%s",row[0]);	
	sprintf(p1,"%s",row[8]); sprintf(p2,"%s",row[9]); sprintf(p3,"%s",row[10]);

	sprintf(p4,"%s",row[11]); sprintf(p5,"%s",row[12]);
	sprintf(p6,"%s",row[13]); sprintf(p7,"%s",row[14]);
	sprintf(p8,"%s",row[15]); sprintf(p9,"%s",row[16]);
	sprintf(p10,"%s",row[17]); sprintf(p11,"%s",row[18]);
	sprintf(p12,"%s",row[19]); sprintf(p13,"%s",row[20]);
	sprintf(p14,"%s",row[21]); sprintf(p15,"%s",row[22]);
	sprintf(p16,"%s",row[23]); sprintf(p17,"%s",row[24]);
	sprintf(p18,"%s",row[25]);

	sprintf(str,"%s",row[3]);sscanf(str,"%d",&codigoaccion);

	mysql_free_result(result);


	//sprintf(p1,"%s","3");




	//abrirpuertapermanente(sd, nodo, p1);

	//exit(0);
	

	switch(codigoaccion){
            case 1  : retorno	=abrirtodaslaspuerta(sd, nodo, idsinc, conn);
                     break;
            case 2  : retorno	=abrirPuertaDadoNumPuerta(sd, nodo, p1, idsinc, conn);	// p1 es el numerodelapuerta, p2 es el id del comando de sincronizacion.
                     break;
            case 3  : retorno	=abrirPuertaDadoNumLectora(sd, nodo, p1, idsinc, conn);	// p1 es el numerodelalectora en el nodo, p2 es el id del comando de sincronizacion.
                     break;
            case 4 : retorno	=escribirusuario(sd, nodo, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14);
                     break;
            case 5 : retorno	=sincronizarRelog(sd, nodo, conn);
                     break;
            case 6 : retorno	=crear_diaferiado(sd, nodo, p1, p2, p3);//param1 es el indice del dia feriado, param2 es el dia, y param3 es el mes.
                     break;
            case 7 : retorno	=timezones(sd, nodo, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18);
		//1 es el indice del timezone, p2 dice si va a estar disponible en dias festivos, y param2 es el nivel de prioridad.
                     break;
            case 8 : retorno	=enviarGrupoDePuertas(sd, nodo, p1, p2, p3, p4, p5);
                     break;
            case 9 : retorno	=asociarLectoraConPuerta(sd, nodo, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16);
                     break;
            case 10 : retorno	=obtenerLectorasOnline(sd, nodo, conn);
                     break;
            case 11 : retorno	=armarlectora(sd, nodo, idsinc, p1, conn);
                     break;
            case 12 : retorno	=desarmarlectora(sd, nodo, idsinc, p1, conn);
                     break;
            case 13 : retorno	=estadodelectora(sd, nodo, idsinc, p1, conn);
                     break;
            case 14 : retorno	=abrirtodaslaspuerta(sd, nodo, idsinc, conn);
                     break;
            case 15 : retorno	=cerrartodaslaspuerta(sd, nodo, idsinc, conn);
                     break;
            case 16 : retorno	=cerrarPuertaDadoNumPuerta(sd, nodo, p1, idsinc, conn);
                     break;
            case 17 : retorno	=cerrarPuertaDadoNumLectora(sd, nodo, p1, idsinc, conn);
                     break;

	    default : comandovalido=0;
        }
//esta pendiente por hacer una funcion que permita abrir todas las puertas de modo indefinido.




	int numsincro; sscanf(idsinc,"%d",&numsincro);
	if(comandovalido==0){
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar) VALUES (%d,'El Comando de Sincronizacion: %d no es Valido ',%d)",nodo,numsincro,numsincro);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
	}

	sprintf(query, "DELETE FROM ctrl_sincronizar WHERE id=%d",numsincro);
	if (mysql_query(conn, query)) {
		printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	return 1;
}





//entrega de julio - agosto del 2011
int asociarLectoraConPuerta(int sd, unsigned char nodo, char p1[], char p2[], char p3[], char p4[], char p5[], char p6[], char p7[], char p8[], char p9[], char p10[], char p11[], char p12[], char p13[], char p14[], char p15[], char p16[]){
	unsigned char buf[buflen], m;
	int num;
	int j= leerConfiguracion(sd, nodo, buf);
	if(j==0) return j;


	unsigned char Tx[29];
	Tx[0]  = 0x7E;								// Head Code
	Tx[1]  = 0x1B;								// LONGITUD DEL PAQUETE 27 EN ESTE CASO
	Tx[2]  = nodo;								// DIRECCION DEL NODO DESTINO
	Tx[3]  = 0x20;								// COMANDO PARA ASOCIAR PUERTA CON LECTORA 32 EN ESTE CASO

	Tx[4]  = 0x02;								// NUEVA DIRECCION DEL NODO - RECOMIENDO NO CAMBIARLO... SE CAMBIA SOLO EN EL CASO EN QUE SEA LECTORA Y CONTROLADORA AL MISMO TIEMPO...
	Tx[5]  = 0x03;								// no se que es esto
	Tx[6]  = 0x5f;								// UN BYTE. SE ESPECIFICA BIT A BIT ABAJO. COMO BYTE[6] .
	Tx[7]  = 0x01;								// UN BYTE. BIT N EN 1 INDICA LECTORA N+1 ONLINE
	Tx[8]  = 0x00;								// UN BYTE. BIT N EN 1 INDICA LECTORA N+1+8 ONLINE. EJEMPLO: BIT 0 EN 1 INDICA LECTORA 9 ONLINE.

	Tx[9]  = 0x03;								// NUMERO DE PUERTA EN LECTORA 01 DEL NODO 
	Tx[10] = 0x01;								// NUMERO DE PUERTA EN LECTORA 02 DEL NODO 
	Tx[11] = 0x02;								// NUMERO DE PUERTA EN LECTORA 03 DEL NODO 
	Tx[12] = 0x04;								// NUMERO DE PUERTA EN LECTORA 04 DEL NODO 
	Tx[13] = 0x05;								// NUMERO DE PUERTA EN LECTORA 05 DEL NODO 
	Tx[14] = 0x06;								// NUMERO DE PUERTA EN LECTORA 06 DEL NODO 
	Tx[15] = 0x07;								// NUMERO DE PUERTA EN LECTORA 07 DEL NODO 
	Tx[16] = 0x08;								// NUMERO DE PUERTA EN LECTORA 08 DEL NODO 
	Tx[17] = 0x09;								// NUMERO DE PUERTA EN LECTORA 09 DEL NODO 
	Tx[18] = 0x0A;								// NUMERO DE PUERTA EN LECTORA 10 DEL NODO 
	Tx[19] = 0x0B;								// NUMERO DE PUERTA EN LECTORA 11 DEL NODO 
	Tx[20] = 0x0C;								// NUMERO DE PUERTA EN LECTORA 12 DEL NODO 
	Tx[21] = 0x0D;								// NUMERO DE PUERTA EN LECTORA 13 DEL NODO 
	Tx[22] = 0x0E;								// NUMERO DE PUERTA EN LECTORA 14 DEL NODO 
	Tx[23] = 0x0F;								// NUMERO DE PUERTA EN LECTORA 15 DEL NODO 
	Tx[24] = 0x10;								// NUMERO DE PUERTA EN LECTORA 16 DEL NODO 

	Tx[25] = 0x00;								// On K2 While Reader Off Line: especificado en BYTE[25] ABAJO.
	Tx[26] = 0x00;								// no se que es esto

	Tx[27] = 0x92;								// XOR
	Tx[28] = 0xA1;								// SUM


	





// BYTE[6]: BIT O EN 1: MASTER CONTROLLER. When 7XXE series and 8XXE series networking controller are used at the same time, 8XXE series must be set Master Node. (Setting on the 821E/829E Parameter Setting)
// BYTE[6]: BIT 1 EN 1: K3 ANTI PASSBACK Err / K4 alarm
// BYTE[6]: BIT 2 EN 1: REASSIGN TIME ZONE
// BYTE[6]: BIT 3 EN 1: ENABLE AUTO OPEN (ZONE:63)
// BYTE[6]: BIT 4 EN 1: ENABLE AUTO DISARMING (ZONE:62).
// BYTE[6]: BIT 5 EN 1: READER LCD SHOW: DAY / MONTH
// BYTE[6]: BIT 6 EN 1: DI1 ACTIVE RELEASE ALL DOORS.
// BYTE[6]: BIT 7 EN 1: AUTO RESET ANTI-PASS (ZONA 61).
// BYTE[25]: On K2 While Reader Off Line: Once this item is chose, and the k2 relay connect to alarm device. When on-line reader which connect to controller is off-line with the controller, K2 will be active alarm.

	sscanf(p1,"%d",&num); Tx[9]=num;
	sscanf(p2,"%d",&num); Tx[10]=num;
	sscanf(p3,"%d",&num); Tx[11]=num;
	sscanf(p4,"%d",&num); Tx[12]=num;
	sscanf(p5,"%d",&num); Tx[13]=num;
	sscanf(p6,"%d",&num); Tx[14]=num;
	sscanf(p7,"%d",&num); Tx[15]=num;
	sscanf(p8,"%d",&num); Tx[16]=num;
	sscanf(p9,"%d",&num); Tx[17]=num;
	sscanf(p10,"%d",&num); Tx[18]=num;
	sscanf(p11,"%d",&num); Tx[19]=num;
	sscanf(p12,"%d",&num); Tx[20]=num;
	sscanf(p13,"%d",&num); Tx[21]=num;
	sscanf(p14,"%d",&num); Tx[22]=num;
	sscanf(p15,"%d",&num); Tx[23]=num;
	sscanf(p16,"%d",&num); Tx[24]=num;


	Tx[4]  = buf[4];							// SE COPIA LA CONFIGURACION QUE EXISTE EN LA CONTROLADORA PARA ESTOS CAMPOS...
	Tx[5]  = buf[5];							// SE COPIA LA CONFIGURACION QUE EXISTE EN LA CONTROLADORA PARA ESTOS CAMPOS...
	Tx[6]  = buf[6];							// SE COPIA LA CONFIGURACION QUE EXISTE EN LA CONTROLADORA PARA ESTOS CAMPOS...
	Tx[7]  = buf[7];							// SE COPIA LA CONFIGURACION QUE EXISTE EN LA CONTROLADORA PARA ESTOS CAMPOS...
	Tx[8]  = buf[8];							// SE COPIA LA CONFIGURACION QUE EXISTE EN LA CONTROLADORA PARA ESTOS CAMPOS...
	//Tx[25] = buf[25];							// SE COPIA LA CONFIGURACION QUE EXISTE EN LA CONTROLADORA PARA ESTOS CAMPOS...
	//Tx[26] = buf[26];							// SE COPIA LA CONFIGURACION QUE EXISTE EN LA CONTROLADORA PARA ESTOS CAMPOS...

	m=0xFF; for(int i=2; i<27; i++) m = m ^ Tx[i]; 
	Tx[27] = m;								// XOR 
	m=0x00; for(int i=2; i<28; i++)	m = m + Tx[i]; 
	Tx[28] = m;								// SUM

	printf("\nENVIANDO COMANDO APARA ASOCIAR NUMEROS DE PUERTAS CON IDENTIFICADOR DE LECTORAS AL NODO %d\n",nodo);
	return enviarTrama(sd, nodo, Tx, 29, buf);
}



//entrega de julio - agosto del 2011
int leerConfiguracion(int sd, unsigned char nodo, unsigned char buf[]){
	unsigned char Tx[6];
	Tx[0] = 0x7E;								// Head Code
	Tx[1] = 0x04;								// LONGITUD DEL PAQUETE 27 EN ESTE CASO
	Tx[2] = nodo;								// DIRECCION DEL NODO DESTINO
	Tx[3] = 0x12;								// COMANDO PARA LEER CONFIGURACION DEL NODO
	Tx[4] = 0xFF ^ Tx[2] ^ Tx[3];						// XOR
	Tx[5] = 0x00 + Tx[2] + Tx[3] + Tx[4];					// SUM
	printf("\nENVIANDO COMANDO PARA LEER CONFIGURACION AL NODO %d\n",nodo);
	return enviarTrama(sd, nodo, Tx, 6, buf);
}


//entrega de julio - agosto del 2011
int obtenerLectorasOnline(int sd, unsigned char nodo, MYSQL *conn){
	unsigned char buf[buflen], Tx[6], a, m;
	char value[12];
	Tx[0] = 0x7E;								// Head Code
	Tx[1] = 0x04;								// LONGITUD DEL PAQUETE 27 EN ESTE CASO
	Tx[2] = nodo;								// DIRECCION DEL NODO DESTINO
	Tx[3] = 0x15;								// COMANDO PARA LEER LAS LECTORAS EN LINEA.
	Tx[4] = 0xFF ^ Tx[2] ^ Tx[3];						// XOR
	Tx[5] = 0x00 + Tx[2] + Tx[3] + Tx[4];					// SUM
	printf("\nENVIANDO COMANDO PARA OBTENER LAS LECTORAS EN LINEA PARA EL NODO %d\n",nodo);
	// Ejemplo de trama a recibida...
	// 7e 17 00 03 02 ff ff 87 87 00 87 04 04 04 04 87 87 87 87 87 87 87 87 79 59
	int recibida = enviarTrama(sd, nodo, Tx, 6, buf);
	if(!recibida) return 0;


	char query[1024] ="INSERT INTO ctrl_lectora (idcontroladora,numero,canal,estado) VALUES";
	a=0x01, m = buf[5];
	for(int j=0; j<8; j++, a=a<<1){
		if((a & m)==0){
			sprintf(value, "(%d,%d,1,%d)",nodo, j+1, 2); // nodo j+1 no esta conectado a la controladora...
		}else{
			if(buf[7+j]==135){//87H
				sprintf(value, "(%d,%d,1,%d)",nodo, j+1, 0);// nodo j+1 esta conectado a la controladora pero esta offline
			}else if(buf[7+j]==0){//00H
				sprintf(value, "(%d,%d,1,%d)",nodo, j+1, 1);// nodo j+1 esta conectado a la controladora y esta online
			}else{
				sprintf(value, "(%d,%d,1,%d)",nodo, j+1, 3);// nodo j+1 esta conectado a la controladora con estatus desconocido
			}
		}
		if(j==0){
			sprintf(query, "%s %s", query, value );
		}else{
			sprintf(query, "%s, %s", query, value );
		}
	}

	a=1, m = buf[6];
	for(int j=0; j<8; j++, a=a<<1){
		if((a & m)==0){
			sprintf(value, "(%d,%d,2,%d)",nodo, j+8+1, 2); // nodo j+8+1 no esta conectado a la controladora...
		}else{
			if(buf[7+j+8]==135){//87H
				sprintf(value, "(%d,%d,2,%d)",nodo, j+1+8, 0);// nodo j+8+1 esta conectado a la controladora pero esta offline
			}else if(buf[7+j+8]==0){//00H
				sprintf(value, "(%d,%d,2,%d)",nodo, j+1+8, 1);// nodo j+8+1 esta conectado a la controladora y esta online
			}else{//04H, y algunos otros codigos que no conozco..
				sprintf(value, "(%d,%d,2,%d)",nodo, j+1+8, 3);// nodo j+8+1 esta conectado a la controladora con estatus desconocido
			}
		}
		sprintf(query, "%s, %s", query, value );
	}
	sprintf(query, "%s ON DUPLICATE KEY UPDATE estado=VALUES(estado), canal=VALUES(canal)", query);
	//printf("%s\n\n",query);

	// ejemplo de query que se esta armando
	// INSERT INTO ctrl_lectora (idcontroladora,numero,estado) 
	// VALUES (2,1,0), (2,2,0), (2,3,1), (2,4,0), (2,5,2), (2,6,0), (2,7,0), (2,8,2), 
	//	(2,9,0), (2,10,0), (2,11,0), (2,12,0), (2,13,0), (2,14,0), (2,15,0), (2,16,2) 
	// ON DUPLICATE KEY UPDATE estado=VALUES(estado);
	
	if(mysql_query(conn, query)){
		printf("Error:OBTENER LECTORAS ONLINE: %u: %s\n", mysql_errno(conn), mysql_error(conn));
		return 0;
	}
	return 1;
}







int enviarGrupoDePuertas(int sd, unsigned char nodo, char idx[], char link[], char level[], char grupo1[], char grupo2[]){
	unsigned char buf[buflen];
	char numstr[2], str[3];
	unsigned int j, idx2, link2, level2;
	unsigned char m;

	sprintf(str,"%s",idx); sscanf(str,"%d",&idx2);
	sprintf(str,"%s",link); sscanf(str,"%d",&link2);
	sprintf(str,"%s",level); sscanf(str,"%d",&level2);

	unsigned char Tx[42];
	Tx[0] = 0x7E;								// Head Code
	Tx[1] = 0x28;								// Length of package
	Tx[2] = nodo;								// Destination Node Address
	Tx[3] = 0x2B;								// Setting Door Groups
	Tx[4] = idx2;								// IDX: Group Number.
	Tx[5] = 0x01;								// No se que es esto, lo investigare.
	Tx[6] = link2;								// Enlace, esto es cuando un grupo de puerta esta enlazado con otro grupo de puertas.
	Tx[7] = level2;								// Nivel de prioridad.
	for(int i=0, k=8;  i<40; i=i+2){
		strncpy (numstr,grupo1+i,2); numstr[2]='\0';
		sscanf(numstr,"%X",&j); m=j;
		Tx[k++]=m;
	}
	for(int i=0, k=28;  i<24; i=i+2){
		strncpy (numstr,grupo2+i,2); numstr[2]='\0';
		sscanf(numstr,"%X",&j); m=j;
		Tx[k++]=m;
	}
	m=0xFF; for(int i=2; i<40; i++) m = m ^ Tx[i]; Tx[40] = m;		// XOR 
	m=0x00; for(int i=2; i<41; i++)	m = m + Tx[i]; Tx[41] = m;		// SUM

	printf("\nENVIANDO GRUPO DE PUERTA AL NODO %d\n",nodo);
	return enviarTrama(sd, nodo, Tx, 42, buf);
}


int cerrarPuertaDadoNumPuerta(int sd, unsigned char nodo, char puerta[], char idsincronizacion[], MYSQL *conn){
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int numpuerta; sscanf(puerta,"%d",&numpuerta);
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int numlectora =0;
	int j =leerConfiguracion(sd, nodo, buf);
	/* Ejemplo:
	7e 04 02 12 ef 03 00
	7e 1d 00 03 02 03 02 04 00 02 01 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 a7 00 00 00 4c 89
	*/
	for(int i=1; j==1 && i<=16; i++){ // buscando el numero de la puerta en la secuencia almacenada en buf, siempre y cuando la llamada a leerConfiguracion() devuelva 1.
		if(buf[i+8]==numpuerta){
			numlectora = i;
			break;
		}
	}
	if(numlectora==0){ // no se encontro el numero de puerta dado en las lectoras atadas a la controladora.
		sprintf(mensaje, "Abrir Puerta desde la Aplicación de monitoreo FALLO. La Puerta %d no esta conectada a la Controladora o nodo %d", numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}
	// Verificar mediante los bytes 7 y 8 que la lectora 'numlectora' este activa.......
	unsigned char activa =0x00;
	if(numlectora<9){
		activa = buf[7];
		activa = activa << (8-numlectora);
		activa = activa >> 7;
	}else{
		activa = buf[8];
		activa = activa << (16-numlectora);
		activa = activa >> 7;
	}
	if(activa==0x00){ // LA LECTORA A CONSULTAR NO ESTA ACTIVA EN EL NODO .........
		sprintf(mensaje, "Abrir Puerta desde la Aplicación de monitoreo FALLO. La lectora %d correspondiente a la Puerta %d no esta activa en la Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}
	j=cerrarpuerta(sd, nodo,  numlectora);
	if(j==1){
		sprintf(mensaje, "Puerta cerrada exitosamente. Lectora %d, Puerta %d, Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 1;
	}else{
		sprintf(mensaje, "La Puerta NO FUE CERRADA Exitosamente. Lectora %d, Puerta %d, Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}

}


//14 de septiembre
int cerrarPuertaDadoNumLectora(int sd, unsigned char nodo, char lectora[], char idsincronizacion[], MYSQL *conn){
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int numlectora; sscanf(lectora,"%d",&numlectora);
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int j =leerConfiguracion(sd, nodo, buf);
	int numpuerta = buf[numlectora+8];
	/* Ejemplo:
	7e 04 02 12 ef 03 00
	7e 1d 00 03 02 03 02 04 00 02 01 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 a7 00 00 00 4c 89
	*/

	// Verificar mediante los bytes 7 y 8 que la lectora 'numlectora' este activa.......
	unsigned char activa =0x00;
	if(numlectora<9){
		activa = buf[7];
		activa = activa << (8-numlectora);
		activa = activa >> 7;
	}else{
		activa = buf[8];
		activa = activa << (16-numlectora);
		activa = activa >> 7;
	}
	if(activa==0x00){ // LA LECTORA A CONSULTAR NO ESTA ACTIVA EN EL NODO .........
		sprintf(mensaje, "Abrir Puerta desde la Aplicación de monitoreo FALLO. La lectora %d correspondiente a la Puerta %d no esta activa en la Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}
	j=cerrarpuerta(sd, nodo,  numlectora);
	if(j==1){
		sprintf(mensaje, "Puerta cerrada exitosamente. Lectora %d, Puerta %d, Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 1;
	}else{
		sprintf(mensaje, "La Puerta NO FUE CERRADA Exitosamente. Lectora %d, Puerta %d, Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}

}




// 6 de septiembre
int cerrarpuerta(int sd, unsigned char nodo,  unsigned char numlectora){ // dado el numero de la lectora.
	unsigned char buf[buflen];
	int j;
	j=enviarcomandorelay(sd, nodo, numlectora, 0x83, buf);
							/* Es enviada una trama como esta: 7e 08 02 23 03 03 21 83 7c 4b
							Y la respuesta puede ser una de estas 2 dependiendo si la lectora esta realmente conectada a la controladora:
							7E 06 00 00 02 00 FD FF			Esto fue con la lectora 2. No esta conectada a la controladora.
							7e 09 00 03 03 66 11 00 18 90 25	Esto fue con la lectora 3. Si esta conectada a la controladora.
							El 11 aqui indica el estatus de la lectora 03. Especificado en pdf de soyal pag:9 especificaciones de data0
							El 0x11 es tomado como el estadofinal de la lectora 3.*/

	if(buf[1]!=0x09){				// La lectora no esta conectada a la controladora....
		return 0;				
	}
	unsigned char puertacerrada= buf[6] << 1;
	puertacerrada = puertacerrada >> 7;
	return puertacerrada == 0x00;
}












/* 	
Se recibe el numero de la puerta a abrir. Hay que recordar que el numero de la puerta no es el mismo numero de la lectora.
El numero de lector para estas controladoras es un numero del 1 al 16. Mientras que el numero de puerta es un numero que va 
desde 1 hasta el 255.
Lo que si debemos mantener en claro es saber cual lectora opera que puerta, y a que controladora esta conectada esta lectora.
*/
int abrirPuertaDadoNumPuerta(int sd, unsigned char nodo, char puerta[], char idsincronizacion[], MYSQL *conn){ // Dado el numero de la puerta.
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int numpuerta; sscanf(puerta,"%d",&numpuerta);
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int numlectora =0;
	int j =leerConfiguracion(sd, nodo, buf);
	/* Ejemplo:
	7e 04 02 12 ef 03 00
	7e 1d 00 03 02 03 02 04 00 02 01 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 a7 00 00 00 4c 89
	*/
	for(int i=1; j==1 && i<=16; i++){ // buscando el numero de la puerta en la secuencia almacenada en buf, siempre y cuando la llamada a leerConfiguracion() devuelva 1.
		if(buf[i+8]==numpuerta){
			numlectora = i;
			break;
		}
	}
	if(numlectora==0){ // no se encontro el numero de puerta dado en las lectoras atadas a la controladora.
		sprintf(mensaje, "Abrir Puerta desde la Aplicación de monitoreo FALLO. La Puerta %d no esta conectada a la Controladora o nodo %d", numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}
	// Verificar mediante los bytes 7 y 8 que la lectora 'numlectora' este activa.......
	unsigned char activa =0x00;
	if(numlectora<9){
		activa = buf[7];
		activa = activa << (8-numlectora);
		activa = activa >> 7;
	}else{
		activa = buf[8];
		activa = activa << (16-numlectora);
		activa = activa >> 7;
	}
	if(activa==0x00){ // LA LECTORA A CONSULTAR NO ESTA ACTIVA EN EL NODO .........
		sprintf(mensaje, "Abrir Puerta desde la Aplicación de monitoreo FALLO. La lectora %d correspondiente a la Puerta %d no esta activa en la Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}
	int puertaabierta = abrirpuerta(sd, nodo, numlectora);
	if(puertaabierta == 0x01){
		sprintf(mensaje, "Puerta abierta desde aplicación de monitoreo. Controladora o nodo %d, lectora %d, puerta número %d",nodo,numlectora,numpuerta);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
	}else{
		sprintf(mensaje, "Puerta abierta desde aplicación de monitoreo FALLO. Controladora o nodo %d, lectora %d, puerta número %d",nodo,numlectora,numpuerta);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
	}
	return puertaabierta;
}










int abrirPuertaDadoNumLectora(int sd, unsigned char nodo, char lectora[], char idsincronizacion[], MYSQL *conn){ // Dado el numero de la puerta.
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int numlectora; sscanf(lectora,"%d",&numlectora);
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int j =leerConfiguracion(sd, nodo, buf);
	int numpuerta = 0x00;
	if(j==1) numpuerta = buf[numlectora+8];
	// Verificar mediante los bytes 7 y 8 que la lectora 'numlectora' este activa.......
	unsigned char activa =0x00;
	if(numlectora<9){
		activa = buf[7];
		activa = activa << (8-numlectora);
		activa = activa >> 7;
	}else{
		activa = buf[8];
		activa = activa << (16-numlectora);
		activa = activa >> 7;
	}
	if(activa==0x00){ // LA LECTORA A CONSULTAR NO ESTA ACTIVA EN EL NODO .........
		sprintf(mensaje, "Abrir Puerta desde la Aplicación de monitoreo FALLO. La lectora %d correspondiente a la Puerta %d no esta activa en la Controladora o Nodo %d", numlectora, numpuerta, nodo);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 0;
	}
	int puertaabierta = abrirpuerta(sd, nodo, numlectora);
	if(puertaabierta == 0x01){
		sprintf(mensaje, "Puerta abierta desde aplicación de monitoreo. Controladora o Nodo %d, lectora %d, puerta número %d",nodo,numlectora,numpuerta);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
	}else{
		sprintf(mensaje, "Puerta abierta desde aplicación de monitoreo FALLO. Controladora o nodo %d, lectora %d, puerta número %d",nodo,numlectora,numpuerta);
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora,numeropuerta) VALUES (%d,'%s',%d,%d,%d)",nodo,mensaje,numsincro,numlectora,numpuerta);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
	}
	return puertaabierta;
}


int abrirpuerta(int sd, unsigned char nodo,  unsigned char numlectora){ // dado el numero de la lectora.
	unsigned char buf[buflen];
	int j;
	unsigned char puertaabierta=0x00;
	for(int i=0; i<10; i++){
		j=enviarcomandorelay(sd, nodo, numlectora, 0x84, buf);
								/* Es enviada una trama como esta: 7E 08 02 23 03 02 21 84 7A 49
								Y la respuesta puede ser una de estas 2 dependiendo si la lectora esta realmente conectada a la controladora:
								7E 06 00 00 02 00 FD FF			Esto fue con la lectora 2. No esta conectada a la controladora.
								7E 09 00 03 03 66 41 00 18 C0 85	Esto fue con la lectora 3. Si esta conectada a la controladora.
								El 41 aqui indica el estatus de la lectora 03. Especificado en pdf de soyal pag:9 especificaciones de data0
								El 0x41 es tomado como el estadofinal de la lectora 3.*/

		if(buf[1]!=0x09){				// La lectora no esta conectada a la controladora....
			return 0;				
		}

		j=obtenerEstadoDeLectoras(sd, nodo, buf);   /*	Es enviada una trama como esta: 7E 04 02 3F C2 03 para mostrar el estado de todas las lectoras conectadas a la controladora 2.
								La respuesta recibida es como alguna de estas: 
								7E 19 00 03 02 01 01 01 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 58 B7
								7E 19 00 03 02 01 01 41 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 18 B7
								La posicion que contiene el 41 es donde se coloca el estado de la lectora 3. La lectora 3 tiene un estado descrito por 0x41.*/


		puertaabierta = buf[4+numlectora];	/*	Se examina si byte correspondiente al estado de la lectora 3 contiene un 1 en el bit 7. En caso de tenerlo es indicativo
								de que el relay de la puerta se activo. Sino hay que intentar de nuevo.*/

		puertaabierta = puertaabierta << 1;
		puertaabierta = puertaabierta >> 7;
		if(puertaabierta == 0x01) break;
	}
	return puertaabierta;
}



int abrirpuertapermanente(int sd, unsigned char nodo,  unsigned char numlectora){ // dado el numero de la lectora.
	unsigned char buf[buflen];
	int j;
	unsigned char puertaabierta=0x00;
	for(int i=0; i<10; i++){
		j=enviarcomandorelay(sd, nodo, numlectora, 0x82, buf);
								/* Es enviada una trama como esta: 7E 08 02 23 03 02 21 84 7A 49
								Y la respuesta puede ser una de estas 2 dependiendo si la lectora esta realmente conectada a la controladora:
								7E 06 00 00 02 00 FD FF			Esto fue con la lectora 2. No esta conectada a la controladora.
								7E 09 00 03 03 66 41 00 18 C0 85	Esto fue con la lectora 3. Si esta conectada a la controladora.
								El 41 aqui indica el estatus de la lectora 03. Especificado en pdf de soyal pag:9 especificaciones de data0
								El 0x41 es tomado como el estadofinal de la lectora 3.*/

		if(buf[1]!=0x09){				// La lectora no esta conectada a la controladora....
			return 0;				
		}

		j=obtenerEstadoDeLectoras(sd, nodo, buf);   /*	Es enviada una trama como esta: 7E 04 02 3F C2 03 para mostrar el estado de todas las lectoras conectadas a la controladora 2.
								La respuesta recibida es como alguna de estas: 
								7E 19 00 03 02 01 01 01 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 58 B7
								7E 19 00 03 02 01 01 41 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 18 B7
								La posicion que contiene el 41 es donde se coloca el estado de la lectora 3. La lectora 3 tiene un estado descrito por 0x41.*/


		puertaabierta = buf[4+numlectora];	/*	Se examina si byte correspondiente al estado de la lectora 3 contiene un 1 en el bit 7. En caso de tenerlo es indicativo
								de que el relay de la puerta se activo. Sino hay que intentar de nuevo.*/

		puertaabierta = puertaabierta << 1;
		puertaabierta = puertaabierta >> 7;
		if(puertaabierta == 0x01) break;
	}
	return puertaabierta;
}







// 7 de septiembre..
int abrirtodaslaspuerta(int sd, unsigned char nodo, char idsincronizacion[], MYSQL *conn){ // dado el numero de la lectora.
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int cant_puertas =0;
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int j =leerConfiguracion(sd, nodo, buf);
	//7e 04 02 12 ef 03 00
	//7e 1d 00 03 02 03 02 04 00 02 01 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 a7 00 00 00 4c 89
	if(j==0 || buf[3]!=0x03) return 0;
	for(int i=1; i<9; i++){// verificando las lectoras activas desde lectora 1 hasta lectora 8...
		unsigned char activa = buf[7] << (8-i);
		activa = activa >> 7;
		if(activa == 0x01){
			unsigned char numlectora =i;
			j=abrirpuerta(sd, nodo,  numlectora);
			if(j==1){
				int puertaabierta = buf[8+i];
				cant_puertas++;
				sprintf(mensaje, "%s %d,", mensaje, puertaabierta );
			}
		}
	}
	for(int i=1; i<9; i++){// verificando las lectoras activas desde lectora 9 hasta lectora 16...
		unsigned char activa = buf[8] << (8-i);
		activa = activa >> 7;
		if(activa == 0x01){
			unsigned char numlectora =i+8;
			j=abrirpuerta(sd, nodo,  numlectora);
			if(j==1){
				int puertaabierta = buf[8+8+i];
				cant_puertas++;
				sprintf(mensaje, "%s %d,", mensaje, puertaabierta );
			}
		}
	}
	if(cant_puertas==0){
		sprintf(mensaje, "No se abrieron puertas en la controladora %d para orden de apertura de todas las puertas enviada desde consola de monitoreo", nodo );
	}else{
		sprintf(query, "Se abrieron desde la consola de monitoreo las puertas %s para la controladora %d", mensaje, nodo );
		sprintf(mensaje, "%s", query);
	}
	sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar) VALUES (%d,'%s',%d)",nodo,mensaje,numsincro);
	if (mysql_query(conn, query)) {
		printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	return 1;
}		





// 9 de septiembre
int cerrartodaslaspuerta(int sd, unsigned char nodo, char idsincronizacion[], MYSQL *conn){ // dado el numero de la lectora.
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int cant_puertas =0;
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int j =leerConfiguracion(sd, nodo, buf);
	//7e 04 02 12 ef 03 00
	//7e 1d 00 03 02 03 02 04 00 02 01 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 a7 00 00 00 4c 89
	if(j==0 || buf[3]!=0x03) return 0;
	for(int i=1; i<9; i++){// verificando las lectoras activas desde lectora 1 hasta lectora 8...
		unsigned char activa = buf[7] << (8-i);
		activa = activa >> 7;
		if(activa == 0x01){
			unsigned char numlectora =i;
			j=cerrarpuerta(sd, nodo, numlectora);
			if(j==1){
				int puertacerrada = buf[8+i];
				cant_puertas++;
				sprintf(mensaje, "%s %d,", mensaje, puertacerrada );
			}
		}
	}
	for(int i=1; i<9; i++){// verificando las lectoras activas desde lectora 9 hasta lectora 16...
		unsigned char activa = buf[8] << (8-i);
		activa = activa >> 7;
		if(activa == 0x01){
			unsigned char numlectora =i+8;
			j=cerrarpuerta(sd, nodo, numlectora);
			if(j==1){
				int puertacerrada = buf[8+8+i];
				cant_puertas++;
				sprintf(mensaje, "%s %d,", mensaje, puertacerrada );
			}
		}
	}
	if(cant_puertas==0){
		sprintf(mensaje, "No se cerro ninguna de las puertas para la controladora %d ", nodo );
	}else{
		sprintf(query, "Fueron cerradas exitosamente las puertas %s para la controladora %d", mensaje, nodo );
		sprintf(mensaje, "%s", query);
	}
	sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar) VALUES (%d,'%s',%d)",nodo,mensaje,numsincro);
	if (mysql_query(conn, query)) {
		printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	return 1;
}







int armarlectora(int sd, unsigned char nodo, char idsincronizacion[], char lectora[], MYSQL *conn){ // dado el numero de la lectora.
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int numlectora; sscanf(lectora,"%d",&numlectora);

	int j;
	j=enviarcomandorelay(sd, nodo, numlectora, 0x80, buf);
							/* Es enviada una trama como esta: 7e 08 02 23 03 03 21 80 7f 4b
							Y la respuesta puede ser una de estas 2 dependiendo si la lectora esta realmente conectada a la controladora:
							7E 06 00 00 02 00 FD FF			Esto fue con la lectora 2. No esta conectada a la controladora.
							7e 09 00 03 03 66 11 00 18 90 25	Esto fue con la lectora 3. Si esta conectada a la controladora.
							El 11 aqui indica el estatus de la lectora 03. Especificado en pdf de soyal pag:9 especificaciones de data0
							El 0x11 es tomado como el estadofinal de la lectora 3.*/

	if(buf[1]!=0x09){				// La lectora no esta conectada a la controladora....
		return 0;				
	}

	j=obtenerEstadoDeLectoras(sd, nodo, buf);   	/* Es enviada una trama como esta: 7E 04 02 3F C2 03 para mostrar el estado de todas las lectoras conectadas a la controladora 2.
							La respuesta recibida es como alguna de estas: 
							7E 19 00 03 02 01 01 01 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 58 B7
							7E 19 00 03 02 01 01 41 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 18 B7
							La posicion que contiene el 41 es donde se coloca el estado de la lectora 3. La lectora 3 tiene un estado descrito por 0x41.*/
	sleep(1);

	unsigned char armada = buf[4+numlectora];	/* Se examina si byte correspondiente al estado de la lectora contiene un 1 en el bit 4. En caso de tenerlo es indicativo
							de que arming esta activo. Sino hay que intentar de nuevo.*/
	armada = armada << 3;
	armada = armada >> 7;
	if(armada == 0x01){
		sprintf(mensaje, "armado exitoso de la lectora %d conectada a la controladora %d",numlectora, nodo );
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora) VALUES (%d,'%s',%d,%d)",nodo,mensaje,numsincro,numlectora);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 1;
	}else{
		sprintf(mensaje, "No se logro realizar el armado de la lectora %d conectada a la controladora %d",numlectora, nodo );
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora) VALUES (%d,'%s',%d,%d)",nodo,mensaje,numsincro,numlectora);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
	}
	return armada;
}

int estadodelectora(int sd, unsigned char nodo, char idsincronizacion[], char lectora[], MYSQL *conn){ // dado el numero de la lectora.
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int numlectora; sscanf(lectora,"%d",&numlectora);
	int j=obtenerEstadoDeLectoras(sd, nodo, buf);  	/* Es enviada una trama como esta: 7E 04 02 3F C2 03 para mostrar el estado de todas las lectoras conectadas a la controladora 2.
							La respuesta recibida es como alguna de estas: 
							7E 19 00 03 02 01 01 01 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 58 B7
							7E 19 00 03 02 01 01 41 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 18 B7
							La posicion que contiene el 41 es donde se coloca el estado de la lectora 3. La lectora 3 tiene un estado descrito por 0x41.*/

	unsigned char lectoraarmada = buf[4+numlectora];// Se examina si byte correspondiente al estado de la lectora contiene un 0 en el bit 4. En caso de tenerlo es indicativo
	lectoraarmada = lectoraarmada << 3;		// de que arming no esta activo. Sino hay que intentar de nuevo.
	lectoraarmada = lectoraarmada >> 7;

	unsigned char puertaabierta = buf[4+numlectora];
	puertaabierta = puertaabierta << 1;		// Se examina si byte correspondiente al estado de la lectora 3 contiene un 1 en el bit 7. En caso de tenerlo es indicativo
	puertaabierta = puertaabierta >> 7;		// de que el relay de la puerta se activo. Sino hay que intentar de nuevo.

	sprintf(mensaje, "controladora=%d, lectora=%d, lectoraarmada=%d, puertaabierta=%d",nodo, numlectora, lectoraarmada, puertaabierta);
	sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora) VALUES (%d,'%s',%d,%d)",nodo,mensaje,numsincro,numlectora);
	if (mysql_query(conn, query)) {
		printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	return 1;
}


int desarmarlectora(int sd, unsigned char nodo, char idsincronizacion[], char lectora[], MYSQL *conn){ // dado el numero de la lectora.
	char query[1024];
	char mensaje[1024];
	unsigned char buf[buflen];
	int numsincro; sscanf(idsincronizacion,"%d",&numsincro);
	int numlectora; sscanf(lectora,"%d",&numlectora);

	int j;
	j=enviarcomandorelay(sd, nodo, numlectora, 0x81, buf);
							/* Es enviada una trama como esta: 7e 08 02 23 03 03 21 81 7e 4b
							Y la respuesta puede ser una de estas 2 dependiendo si la lectora esta realmente conectada a la controladora:
							7E 06 00 00 02 00 FD FF			Esto fue con la lectora 2. No esta conectada a la controladora.
							7e 09 00 03 03 66 01 00 18 80 05	Esto fue con la lectora 3. Si esta conectada a la controladora.
							El 11 aqui indica el estatus de la lectora 03. Especificado en pdf de soyal pag:9 especificaciones de data0
							El 0x11 es tomado como el estadofinal de la lectora 3.*/

	if(buf[1]!=0x09){				// La lectora no esta conectada a la controladora....
		return 0;				
	}
	sleep(1);

	j=obtenerEstadoDeLectoras(sd, nodo, buf);   	/* Es enviada una trama como esta: 7E 04 02 3F C2 03 para mostrar el estado de todas las lectoras conectadas a la controladora 2.
							La respuesta recibida es como alguna de estas: 
							7E 19 00 03 02 01 01 01 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 58 B7
							7E 19 00 03 02 01 01 41 00 00 00 00 00 00 00 00 00 00 00 00 00 FF 00 00 58 18 B7
							La posicion que contiene el 41 es donde se coloca el estado de la lectora 3. La lectora 3 tiene un estado descrito por 0x41.*/

	unsigned char desarmada = buf[4+numlectora];	/* Se examina si byte correspondiente al estado de la lectora contiene un 0 en el bit 4. En caso de tenerlo es indicativo
							de que arming no esta activo. Sino hay que intentar de nuevo.*/
	desarmada = desarmada << 3;
	desarmada = desarmada >> 7;
	if(desarmada == 0x00){
		sprintf(mensaje, "DESARMADO exitoso de la lectora %d conectada a la controladora %d",numlectora, nodo );
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora) VALUES (%d,'%s',%d,%d)",nodo,mensaje,numsincro,numlectora);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
		return 1;
	}else{
		sprintf(mensaje, "No se logro realizar el DESARMADO de la lectora %d conectada a la controladora %d",numlectora, nodo );
		sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log,idsincronizar,nodolectora) VALUES (%d,'%s',%d,%d)",nodo,mensaje,numsincro,numlectora);
		if (mysql_query(conn, query)) {
			printf("Error %u: %s\n", mysql_errno(conn), mysql_error(conn));
			exit(1);
		}
	}
	return 0;
}


int obtenerEstadoDeLectoras(int sd, unsigned char nodo, unsigned char buf[]){
	unsigned char Tx[6];							// 7e 04 02 3f c2 03 00
	Tx[0]	= 0x7E;								// Head Code
	Tx[1]	= 0x04;								// Length of package
	Tx[2]	= nodo;								// Destination Node Address
	Tx[3]	= 0x3f;								// esperando por respuesta
	Tx[4]	= 0xFF ^ Tx[2] ^ Tx[3];						// XOR
	Tx[5]	= 0x00 + Tx[2] + Tx[3] + Tx[4];					// SUM

	printf("\nENVIANDO - ESTADO DE LECTORAS DEL NODO:%d\n",nodo);
	return enviarTrama(sd, nodo, Tx, 6, buf);
}




int enviarcomandorelay(int sd, unsigned char nodo, unsigned char lectora, unsigned char parametro, unsigned char buf[]){
	unsigned char Tx[10];							// ejemplo: 7e 08 02 23 03 03 21 84 7b 4b 
	Tx[0]	= 0x7E;								// Head Code
	Tx[1]	= 0x08;								// Length of package
	Tx[2]	= nodo;								// Destination Node Address
	Tx[3]	= 0x23;								// activando el relay de puerta
	Tx[4]	= 0x03;								// numero de solicitud
	Tx[5]	= lectora;							// lectora a la cual se le va a activar el relay de puerta
	Tx[6]	= 0x21;								// comando de control de relay para la lectora
	Tx[7]	= parametro;							// parametro para la activacion de relay por un tiempo determnado.
	Tx[8]	= 0xFF ^ Tx[2] ^ Tx[3] ^ Tx[4] ^ Tx[5] ^ Tx[6] ^ Tx[7];		// XOR
	Tx[9]	= 0x00 + Tx[2] + Tx[3] + Tx[4] + Tx[5] + Tx[6] + Tx[7] + Tx[8];	// SUM

	printf("\nENVIANDO RELAY DE PUERTA, NODO:%d, LECTORA:%d, PARAMETRO:%02X\n",nodo,lectora,parametro);
	return enviarTrama(sd, nodo, Tx, 10, buf);
}



int escribirusuario(int sd, unsigned char nodo, char idusuario[], char pin[], char idcard[], char idsite[], char card_pin[], 
		char limitefecha[], char changeable[], char guard[], char skipfpcheck[], char grupopuerta[], char fechahasta[], 
		char zonadetiempo[], char niveldelusuario[], char antipassback[]){

	int num, num1, num2, idusuarioalto, idusuariobajo, pinalto, pinbajo, idcardalto, idcardbajo, idsitealto, idsitebajo, eldoce, gpuerta, dia, mes, anio, zt, nu, apb; 
	char tempstr[2];
	unsigned char buf[buflen];

	sscanf(idusuario,"%d",&num); num1=num>>8; idusuarioalto=num1;		//extrayendo el idusuario
	num1=num1<<8; num2 =num-num1; idusuariobajo=num2;			//extrayendo el idusuario

	sscanf(pin,"%d",&num); num1=num>>8; pinalto=num1;			//extrayendo el pin
	num1=num1<<8; num2 =num-num1; pinbajo=num2;				//extrayendo el pin

	sscanf(idcard,"%d",&num); num1=num>>8; idcardalto=num1;			//extrayendo el idcard
	num1=num1<<8; num2 =num-num1; idcardbajo=num2;				//extrayendo el idcard
	
	sscanf(idsite,"%d",&num); num1=num>>8; idsitealto=num1;			//extrayendo el idsite
	num1=num1<<8; num2 =num-num1; idsitebajo=num2;				//extrayendo el idsite


	sscanf(card_pin,"%d",&num); eldoce=(num<<6);				// extrayendo si se acepta solo tarjeta, tarjeta o pin, o tarjeta y pin, o nada para este usuario. (bit 7 y bit 8)
	sscanf(limitefecha,"%d",&num); eldoce=eldoce+(num<<2);			// extrayendo si se contempla fecha de expiracion.. limite de fecha.. (bit 3)
	sscanf(changeable,"%d",&num); eldoce=eldoce+num;			// extrayendo si se canjeable (bit 1)
	sscanf(guard,"%d",&num); eldoce=eldoce+(num<<5);	        	// extrayendo si se comtempla la guard (bit 6)
	sscanf(skipfpcheck,"%d",&num); eldoce=eldoce+(num<<3);	        	// extrayendo si se comtempla skip chequeo fc (bit 4)

	sscanf(grupopuerta,"%d",&gpuerta);					// extrayendo el grupo de puertas para este usuario

	strncpy (tempstr,fechahasta+2,2);					// extrayendo el año. la fecha viene asi 20110625 para 25/06/2011 
	tempstr[2]='\0'; 
	sscanf(tempstr,"%d",&anio);	
	strncpy (tempstr,fechahasta+4,2);					// extrayendo el mes
	tempstr[2]='\0'; 
	sscanf(tempstr,"%d",&mes);	
	strncpy (tempstr,fechahasta+6,2);					// extrayendo el dia
	tempstr[2]='\0'; 
	sscanf(tempstr,"%d",&dia);	
	
	sscanf(zonadetiempo,"%d",&zt);						// extrayendo la zona de tiempo.
	sscanf(niveldelusuario,"%d",&nu);					// extrayendo el nivel de prioridad del usuario.
	sscanf(antipassback,"%d",&apb);						// extrayendo el anti passback.
	

	unsigned char Tx[23];
	Tx[0]	= 0x7E;								// Head Code
	Tx[1]	= 0x15;								// Length of package
	Tx[2]	= nodo;								// Destination Node Address
	Tx[3]	= 0x33;								// Setting users
	Tx[4]	= idusuarioalto;						// idusuario
	Tx[5]	= idusuariobajo;						// idusuario
	Tx[6]	= pinalto;							// pin
	Tx[7]	= pinbajo;							// pin
	Tx[8]	= idcardalto;							// idcard parte alta, no es el codigo site
	Tx[9]	= idcardbajo;							// idcard parte baja, no es el codigo site
	Tx[10]	= idsitealto;							// idsite parte alta
	Tx[11]	= idsitebajo;							// idsite parte baja
	Tx[12]	= eldoce;							// card or/and pin, card only, limite de fecha, guard, changeable, skip fd check
	Tx[13]	= gpuerta;							// grupo de puerta
	Tx[14]	= anio;								// año hasta
	Tx[15]	= mes;								// mes hasta
	Tx[16]	= dia;								// dia hasta
	Tx[17]	= zt;								// zona de tiempo (timezone)
	Tx[18]	= nu;								// nivel del usuario (user level)
	Tx[19]	= 0xC7;								// no se de que se trata este campo
	Tx[20]	= apb;								// anti passback
	Tx[21]	= 0xFF ^ Tx[2] ^ Tx[3] ^ Tx[4] ^ Tx[5] ^ Tx[6] ^ Tx[7] ^ Tx[8] ^ Tx[9] ^ Tx[10] ^ Tx[11] ^ Tx[12] ^ Tx[13] ^ Tx[14] ^ Tx[15] ^ Tx[16] ^ Tx[17] ^ Tx[18] ^ Tx[19] ^ Tx[20];// XOR
	Tx[22]	= 0x00 + Tx[2] + Tx[3] + Tx[4] + Tx[5] + Tx[6] + Tx[7] + Tx[8] + Tx[9] + Tx[10] + Tx[11] + Tx[12] + Tx[13] + Tx[14] + Tx[15] + Tx[16] + Tx[17] + Tx[18] + Tx[19] + Tx[20] + Tx[21];// SUM
	printf("\nENVIANDO DATA DE USUARIO AL NODO %d:\n",nodo);
	return enviarTrama(sd, nodo, Tx, 23, buf);

/*
01 7e head
02 15 length
03 02 target node
04 33 command code
05 00 high iduser
06 01 low iduser
07 27 high pin
08 0f low pin
09 9f low high card id
10 c5 low low card id
11 07 high high card id
12 35 high low card id

13 40 card only:40, invalid:00, card or pin:80, card and pin:c0, card and pin for a year:C4; 40:01000000, 80:10000000, C0:11000000, C4:11000100
14 01 door group
15 0C to year
16 07 to month
17 18 to day
18 00 timezone
19 00 user level
20 c7 
21 01 anti passback:01, 
22 6b 
23 e5 
24 00

*/




// 1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 34
// 7e 15 02 33 13 87 00 00 00 00 00 00 00 00 63 01 01 00 00 c7 00 fe f9 00
// 7e 15 02 33 13 87 00 00 8a ce 00 ff 00 00 63 01 01 00 00 c7 00 45 97 00
// 7e 15 02 33 13 87 00 00 8a ce 00 ff 00 00 63 01 01 00 00 c7 00 45 97 00
// 7e 15 02 33 13 87 00 00 8a ce 00 ff 40 00 63 01 01 00 00 c7 00 05 97 00 card only
// 7e 15 02 33 13 87 00 00 8a ce 00 ff 80 00 63 01 01 00 00 c7 00 c5 97 00 card or pin
// 7e 15 02 33 13 87 00 00 8a ce 00 ff c0 00 63 01 01 00 00 c7 00 85 97 00 card and pin
// 7e 15 02 33 13 87 00 00 8a ce 00 ff c0 00 63 01 01 01 00 c7 00 84 97 00 timezone 01
// 7e 15 02 33 13 87 00 00 8a ce 00 ff c0 00 63 01 01 01 08 c7 00 8c a7 00 level 08
// 7e 15 02 33 13 87 00 00 8a ce 00 ff c0 04 63 01 01 01 08 c7 00 88 a7 00 door group 04
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff c0 04 63 01 01 01 08 c7 00 5e 53 00 pin 1234
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff c4 04 0c 07 18 01 08 c7 00 2a e9 00 date limit from 23/06/2011 to 24/07/2012
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff c4 04 0c 07 18 01 08 c7 00 2a e9 00 date limit from 23/12/2011 to 24/07/2012
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff c4 04 0c 07 18 01 08 c7 00 2a e9 00 date limit from 23/05/2011 to 24/07/2012
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff c4 04 0c 07 18 01 08 c7 01 2b eb 00 anti passback
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff c5 04 0c 07 18 01 08 c7 00 2b eb 00 changeable
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff 64 04 0c 07 18 01 08 c7 00 8a e9 00 guard, card only
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff e4 04 0c 07 18 01 08 c7 00 0a e9 00 guard, card and pin
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff a4 04 0c 07 18 01 08 c7 00 4a e9 00 guard card or pin
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff a5 04 0c 07 18 01 08 c7 00 4b eb 00 guard card or pin, changeable
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff a5 04 0c 07 18 01 08 c7 01 4a eb 00 guard card or pin, changeable, anti passback
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad 04 0c 07 18 01 08 c7 01 42 eb 00 card or pin, changeable, skip fp check
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad 04 0c 07 18 01 08 c7 01 42 eb 00 card or pin, changeable, skip fp check, lock
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad 04 0c 07 18 01 08 c7 01 42 eb 00 card or pin, changeable, skip fp check, lock, duty_20
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad 04 0c 07 18 01 08 c7 01 42 eb 00 auto save
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad 04 0c 07 18 01 08 c7 01 42 eb 00 departamento 10
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad 04 0c 07 18 01 08 c7 01 42 eb 00 card id 12345678901
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad fe 0c 07 18 01 08 c7 01 b8 5b 00 door group 
// 7e 15 02 33 13 87 04 d2 8a ce 00 ff ad 00 0c 07 18 01 08 c7 01 46 eb 00
// 7e 15 02 33 00 00 00 00 00 00 00 00 00 00 63 01 01 01 00 c7 00 6b cd 00
// 7e 15 02 33 00 01 27 0f 9f c5 07 35 40 01 63 01 01 00 00 c7 01 6b e5 00
// 7E 15 02 33 00 01 27 0F 9F C5 07 35 6D 00 0B 06 1D 00 01 C7 00 34 A3
// 7E 15 02 33 00 01 27 0F 9F C5 07 35 ED 00 0B 06 1D 00 01 C7 00 B4 A3


/*
PARA LA TABLA DE SINCRONIZACION TENEMOS QUE:
SI EL CODIGOACCION=51 ENTONCES
PARAM1: ES ELCODIGO DEL USUARIO O TARJETA HABIENTE.
PARAM2: ES EL PIN ASIGNADO AL USUARIO
PARAM3: ES EL NUMERO BAJO DE LA TARJETA, ES EL NUMERO DE LA TARJETA
PARAM4: ES EL NUMERO DEL SITIO
PARAM5: ES LA COMBINACION DE PIN Y TARJETA A UTILIZAR, ESTO ES 1 ES SOLO TARJETA, 2 ES TARJETA Y PIN, 3 ES TARJETA O PIN, SI ES 0 NO TIENE ACCESO PARA NADA.
PARAM6: INDICA SI LA ENTRADA DEL USUARIO TIENE FECHA DE EXPIRACION. SI ES 1 ENTONCES TIENE FECHA DE EXPIRACION, Y TIENE 0 EN CASO CONTRARIO.
PARAM7: INDICA CHANGEABLE. NO SE DE QUE SE TRATA.
PARAM8: INDICA SI SERA UTILIZADA PARA GUARDIAS.
PARAM9: INDICA QUE SE DEJA DE HACER EL CHEQUEO FP. NO SE DE QUE SE TRATA.
PARAM10: INDICA EL GRUPO DE PUERTAS AL CUAL SE LE CONCEDE PASO. SI ES 00 TIENE TODOS LOS GP, 239 NO TIENE GP, 255 SOLO GP FREE, CUALQUIER OTRO VALOR DEBE CORRESPONDER CON UN GP.
PARAM11: INDICA LA FECHA HASTA LA CUAL EL USUARIO SE LE CONCEDERA PASO. FORMATO 20110625, ESTO ES 25 DE JUNIO DEL 2011.
PARAM12: INDICA LA ZONA DE TIEMPO EN LA CUAL SE LE CONCEDERÁ PASO. ESTO ES UN HORARIO DADO PARA LOS DIAS DE LA SEMANA.
PARAM13: INDICA EL NIVEL DE PRIORIDAD DEL USUARIO CON RESPECTO A LAS ZONAS DE TIEMPO.
PARAM14: INDICA SI EL USUARIO VA A EXISTIR CONTROL ANTI PASSBACK.
*/


/*
01 7e head
02 15 length
03 02 target node
04 33 command code
05 00 high iduser
06 01 low iduser
07 27 high pin
08 0f low pin
09 9f low high card id
10 c5 low low card id
11 07 high high card id
12 35 high high card id
13 40 card only:40, invalid:00, card or pin:80, card and pin:c0, card and pin for a year:C4; 40:01000000, 80:10000000, C0:11000000, C4:11000100
14 01 door group
15 0C to year
16 07 to month
17 18 to day
18 00 timezone
19 00 user level
20 c7 
21 01 anti passback:01, 
22 6b 
23 e5 
24 00

caracter 14 door group: si es EF entonces no tiene grupos de puertas
caracter 14 door group: si es 00 entonces tiene todos los grupos de puertas
caracter 14 door group: si es FF entonces tiene todos los grupos de puertas que son free

caracter 13 bit 8 en 1 y bit 7 en 0 indica que puede usar card or pin
caracter 13 bit 8 en 1 y bit 7 en 1 indica que tiene que usar card y pin
caracter 13 bit 8 en 0 y bit 7 en 1 indica que puede usar card only
caracter 13 bit 8 en 0 y bit 7 en 0 indica invalid
caracter 13 bit 3 en 1 indica que se tiene limite de fecha
caracter 13 bit 1 en 1 indica que es changeable
caracter 13 bit 6 en 1 indica guard. 
caracter 13 bit 4 en 1 indica skip fp check. 

64:01100100 guard, card only
e4:11100100 guard, card and pin
a4:10100100 guard, card or pin
a5:10100101 guard, card or pin, changeable
ad:10101101 guard, card or pin, changeable, skip fp check


*/

}



int sincronizarRelog(int sd, unsigned char nodo, MYSQL *conn){
	unsigned char buf[buflen];
	MYSQL_RES *result;
	MYSQL_ROW row;	
	char str[1024];
	char tempstr[2];
	unsigned int anio, mes, dia, hora, minutos, segundos, diadesemana;

	sprintf(str, "SELECT DATE_FORMAT(sysdate(),'%sY-%sm-%sd %sH:%si:%ss %sw')","%","%","%","%","%","%","%");
	//mysql_query(conn, str);
	if(mysql_query(conn, str)){
		printf("Error1 %u: %s\n", mysql_errno(conn), mysql_error(conn));
		return 0;
	}

	result = mysql_store_result(conn);
	row = mysql_fetch_row(result);

	sprintf(str,"%s",row[0]); 
	mysql_free_result(result);

	strncpy (tempstr,str+2,2);tempstr[2]='\0';sscanf(tempstr,"%d",&anio);
	strncpy (tempstr,str+5,2);tempstr[2]='\0';sscanf(tempstr,"%d",&mes);
	strncpy (tempstr,str+8,2);tempstr[2]='\0';sscanf(tempstr,"%d",&dia);
	strncpy (tempstr,str+11,2);tempstr[2]='\0';sscanf(tempstr,"%d",&hora);
	strncpy (tempstr,str+14,2);tempstr[2]='\0';sscanf(tempstr,"%d",&minutos);
	strncpy (tempstr,str+17,2);tempstr[2]='\0';sscanf(tempstr,"%d",&segundos);
	strncpy (tempstr,str+20,1);tempstr[1]='\0';sscanf(tempstr,"%d",&diadesemana);
	

	unsigned char Tx[13];
	Tx[0]	= 0x7E;								// Head Code
	Tx[1]	= 0x0B;								// Length of package
	Tx[2]	= nodo;								// Destination Node Address
	Tx[3]	= 0x28;								// Setting clock
	Tx[4]	= segundos;							// segundos
	Tx[5]	= minutos;							// minutos
	Tx[6]	= hora;								// horas
	Tx[7]	= diadesemana;							// dia de la semana 1 es domingo, 2 es lunes, y 7 es sabado.
	Tx[8]	= dia;								// dia del mes
	Tx[9]	= mes;								// mes
	Tx[10]	= anio;								// año en 2 digitos 11=2011
	Tx[11]	= 0xFF ^ Tx[2] ^ Tx[3] ^ Tx[4] ^ Tx[5] ^ Tx[6] ^ Tx[7] ^ Tx[8] ^ Tx[9] ^ Tx[10];		// XOR
	Tx[12]	= 0x00 + Tx[2] + Tx[3] + Tx[4] + Tx[5] + Tx[6] + Tx[7] + Tx[8] + Tx[9] + Tx[10] + Tx[11];	// SUM

	printf("\nENVIANDO EL CLOCK AL NODO %d:\n",nodo);
	return enviarTrama(sd, nodo, Tx, 13, buf);
}





int timezones(	int sd, unsigned char nodo, char param1[], char param2[], char param3[], 
		char domingodesde[], char domingohasta[], char lunesdesde[], char luneshasta[], char martesdesde[], char marteshasta[], char miercolesdesde[], char miercoleshasta[],
		char juevesdesde[], char jueveshasta[], char viernesdesde[], char vierneshasta[], char sabadodesde[], char sabadohasta[], char enlace[]){
	unsigned char buf[buflen];
	int len;
	char str[6];
	unsigned int num,num1,num2;
	unsigned char charnum;
	unsigned int indicedetimezone,disponibleendiasfestivos,niveldeprioridad,timezoneenlace ;
	unsigned int domingominutosdesde[2],	domingominutoshasta[2];
	unsigned int lunesminutosdesde[2],	lunesminutoshasta[2];
	unsigned int martesminutosdesde[2],	martesminutoshasta[2];
	unsigned int miercolesminutosdesde[2],	miercolesminutoshasta[2];
	unsigned int juevesminutosdesde[2],	juevesminutoshasta[2];
	unsigned int viernesminutosdesde[2],	viernesminutoshasta[2];
	unsigned int sabadominutosdesde[2],	sabadominutoshasta[2];
	
	sprintf(str,"%s",param1); sscanf(str,"%d",&indicedetimezone);		// se captura el indice del timezone.
	sprintf(str,"%s",param3); sscanf(str,"%d",&niveldeprioridad);		// Se captura el nivel de prioridad. Solo usuarios con un nivel de prioridad mayor que este podran pasar.
	sprintf(str,"%s",enlace); sscanf(str,"%d",&timezoneenlace);		// Se captura el timezone con el cual este timezone va a estar enlazado.

	//captura de 'disponible en dias festivos' y de 'timezone enlace'
	sscanf(param2,"%d",&num); num1=num<<7;					
	sscanf(enlace,"%d",&num);						
	disponibleendiasfestivos = num1 + num;

	//captura horario del domingo
	sscanf(domingodesde,"%d",&num); num1=num>>8; domingominutosdesde[0]=num1;
	num1=num1<<8; num2 =num-num1; domingominutosdesde[1]=num2;
	sscanf(domingohasta,"%d",&num); num1=num>>8; domingominutoshasta[0]=num1;
	num1=num1<<8; num2 =num-num1; domingominutoshasta[1]=num2;
	
	//captura el horario del lunes
	sscanf(lunesdesde,"%d",&num); num1=num>>8; lunesminutosdesde[0]=num1;
	num1=num1<<8; num2 =num-num1; lunesminutosdesde[1]=num2;
	sscanf(luneshasta,"%d",&num); num1=num>>8; lunesminutoshasta[0]=num1;
	num1=num1<<8; num2 =num-num1; lunesminutoshasta[1]=num2;

	//captura el horario del martes
	sscanf(martesdesde,"%d",&num); num1=num>>8; martesminutosdesde[0]=num1;
	num1=num1<<8; num2 =num-num1; martesminutosdesde[1]=num2;
	sscanf(marteshasta,"%d",&num); num1=num>>8; martesminutoshasta[0]=num1;
	num1=num1<<8; num2 =num-num1; martesminutoshasta[1]=num2;

	//captura el horario del miercoles
	sscanf(miercolesdesde,"%d",&num); num1=num>>8; miercolesminutosdesde[0]=num1;
	num1=num1<<8; num2 =num-num1; miercolesminutosdesde[1]=num2;
	sscanf(miercoleshasta,"%d",&num); num1=num>>8; miercolesminutoshasta[0]=num1;
	num1=num1<<8; num2 =num-num1; miercolesminutoshasta[1]=num2;

	//captura el horario del jueves
	sscanf(juevesdesde,"%d",&num); num1=num>>8; juevesminutosdesde[0]=num1;
	num1=num1<<8; num2 =num-num1; juevesminutosdesde[1]=num2;
	sscanf(jueveshasta,"%d",&num); num1=num>>8; juevesminutoshasta[0]=num1;
	num1=num1<<8; num2 =num-num1; juevesminutoshasta[1]=num2;

	//captura el horario del viernes
	sscanf(viernesdesde,"%d",&num); num1=num>>8; viernesminutosdesde[0]=num1;
	num1=num1<<8; num2 =num-num1; viernesminutosdesde[1]=num2;
	sscanf(vierneshasta,"%d",&num); num1=num>>8; viernesminutoshasta[0]=num1;
	num1=num1<<8; num2 =num-num1; viernesminutoshasta[1]=num2;

	//captura el horario del sabado
	sscanf(sabadodesde,"%d",&num); num1=num>>8; sabadominutosdesde[0]=num1;
	num1=num1<<8; num2 =num-num1; sabadominutosdesde[1]=num2;
	sscanf(sabadohasta,"%d",&num); num1=num>>8; sabadominutoshasta[0]=num1;
	num1=num1<<8; num2 =num-num1; sabadominutoshasta[1]=num2;




	unsigned char Tx[38];
	Tx[0]	= 0x7E;								// Head Code
	Tx[1]	= 0x24;								// Length of package
	Tx[2]	= nodo;								// Destination Node Address
	Tx[3]	= 0x2A;								// Setting Holidays
	Tx[4]	= indicedetimezone;						// IDX: First set of timezones 
	Tx[5]	= 0x01;								// SETs:Number of timezones to write
	Tx[6]	= disponibleendiasfestivos;					// Si el timezone esta disponible en dias festivo se el bit 7 esta en 1, sino esta en cero 
	Tx[7]	= niveldeprioridad;						// Se permite el paso de usuarios con niveles de prioridad mayor a este.
	Tx[8]	= domingominutosdesde[0];					// minutos trascurridos desde las 00:00 hasta la hora definida como entrda
	Tx[9]	= domingominutosdesde[1];
	Tx[10]	= domingominutoshasta[0];					// minutos trascurridos desde las 00:00 hasta la hora definida como salida
	Tx[11]	= domingominutoshasta[1];
	Tx[12]	= lunesminutosdesde[0];
	Tx[13]	= lunesminutosdesde[1];
	Tx[14]	= lunesminutoshasta[0];
	Tx[15]	= lunesminutoshasta[1];
	Tx[16]	= martesminutosdesde[0];
	Tx[17]	= martesminutosdesde[1];
	Tx[18]	= martesminutoshasta[0];
	Tx[19]	= martesminutoshasta[1];
	Tx[20]	= miercolesminutosdesde[0];
	Tx[21]	= miercolesminutosdesde[1];
	Tx[22]	= miercolesminutoshasta[0];
	Tx[23]	= miercolesminutoshasta[1];
	Tx[24]	= juevesminutosdesde[0];
	Tx[25]	= juevesminutosdesde[1];
	Tx[26]	= juevesminutoshasta[0];
	Tx[27]	= juevesminutoshasta[1];
	Tx[28]	= viernesminutosdesde[0];
	Tx[29]	= viernesminutosdesde[1];
	Tx[30]	= viernesminutoshasta[0];
	Tx[31]	= viernesminutoshasta[1];
	Tx[32]	= sabadominutosdesde[0];
	Tx[33]	= sabadominutosdesde[1];
	Tx[34]	= sabadominutoshasta[0];
	Tx[35]	= sabadominutoshasta[1];
	Tx[36] = 0xFF ^ Tx[2] ^ Tx[3] ^ Tx[4] ^ Tx[5] ^ Tx[6] ^ Tx[7] ^ Tx[8] ^ Tx[9] ^ Tx[10] ^ Tx[11] ^ Tx[12] ^ Tx[13] ^ Tx[14] ^ Tx[15] ^ Tx[16] ^ Tx[17] ^ Tx[18] ^ Tx[19] ^ Tx[20] ^ Tx[21] ^ Tx[22] ^ Tx[23] ^ Tx[24] ^ Tx[25] ^ Tx[26] ^ Tx[27] ^ Tx[28] ^ Tx[29] ^ Tx[30] ^ Tx[31] ^ Tx[32] ^ Tx[33] ^ Tx[34] ^ Tx[35];		// XOR
	Tx[37] = 0x00 + Tx[2] + Tx[3] + Tx[4] + Tx[5] + Tx[6] + Tx[7] + Tx[8] + Tx[9] + Tx[10] + Tx[11] + Tx[12] + Tx[13] + Tx[14] + Tx[15] + Tx[16] + Tx[17] + Tx[18] + Tx[19] + Tx[20] + Tx[21] + Tx[22] + Tx[23] + Tx[24] + Tx[25] + Tx[26] + Tx[27] + Tx[28] + Tx[29] + Tx[30] + Tx[31] + Tx[32] + Tx[33] + Tx[34] + Tx[35] + Tx[36];	// SUM

	printf("\nENVIANDO TIMEZONE AL NODO %d\n",nodo);
	return enviarTrama(sd, nodo, Tx, 38, buf);
}




int crear_diaferiado(int sd, unsigned char nodo, char param1[], char param2[], char param3[]){
	unsigned char buf[buflen];
	char str[3];
	unsigned int idx,mes,dia;
	int len;
	
	sprintf(str,"%s",param1);
	sscanf(str,"%d",&idx);

	sprintf(str,"%s",param2);
	sscanf(str,"%d",&dia);

	sprintf(str,"%s",param3);
	sscanf(str,"%d",&mes);

	unsigned char Tx[6];
	Tx[0] = 0x7E;								// Head Code
	Tx[1] = 0x08;								// Length of package
	Tx[2] = nodo;								// Destination Node Address
	Tx[3] = 0x2C;								// Setting Holidays
	Tx[4] = idx;								// IDX: First set of holidays 
	Tx[5] = 0x01;								// SETs:Number of holidays to write
	Tx[6] = mes;								// Month
	Tx[7] = dia;								// Day
	Tx[8] = 0xFF ^ Tx[2] ^ Tx[3] ^ Tx[4] ^ Tx[5] ^ Tx[6] ^ Tx[7];		// XOR
	Tx[9] = 0x00 + Tx[2] + Tx[3] + Tx[4] + Tx[5] + Tx[6] + Tx[7] + Tx[8];	// SUM

	printf("\nENVIANDO DIA FERIADO AL NODO %d\n",nodo);
	return enviarTrama(sd, nodo, Tx, 10, buf);
}

int dameUltimolog(unsigned char nodo, int sd, MYSQL *conn){
	unsigned char buf[buflen];
	unsigned char borrarbuf[buflen];
	int len, len2;

	unsigned char TxBfPoll[6];
	TxBfPoll[0] = 0x7E;						// Head Code
	TxBfPoll[1] = 0x04;						// Length of package
	TxBfPoll[2] = nodo;						// Destination Node Address
	TxBfPoll[3] = 0x25;						// Polling message
	TxBfPoll[4] = 0xFF ^ TxBfPoll[2] ^ TxBfPoll[3];			// XOR
	TxBfPoll[5] = 0x00 + TxBfPoll[2] + TxBfPoll[3] + TxBfPoll[4];	// SUM

	unsigned char TxBfDel[10];
	TxBfDel[0] = 0x7E;					// Head Code
	TxBfDel[1] = 0x07;					// Length of package
	TxBfDel[2] = nodo;					// Destination Node Address
	TxBfDel[3] = 0x37;					// Deleting lasted message
	TxBfDel[4] = 0x44;					// This is the letter "D";
	TxBfDel[5] = 0x45;					// This is the letter "E";
	TxBfDel[6] = 0x4c;					// This is the letter "L";
	TxBfDel[7] = 0xFF ^ TxBfDel[2] ^ TxBfDel[3] ^ TxBfDel[4] ^ TxBfDel[5] ^ TxBfDel[6];			// XOR
	TxBfDel[8] = 0x00 + TxBfDel[2] + TxBfDel[3] + TxBfDel[4] + TxBfDel[5] + TxBfDel[6] + TxBfDel[7];	// SUM

	printf("\nSOLICITANDO LOGS DE ACCESO AL NODO %d\n",nodo);
	int ret =enviarTrama(sd, nodo, TxBfPoll, 6, buf);
	if(ret==-1) return -1; // sucedio una condicion de borde cuando se enviaba la trama al nodo....

	int hayregistros = !(buf[1]==0x0a && buf[3]==0x01);
	if(hayregistros){ //Hay mas registros, se procede a borrarlos de la controladora
		printf("\nELIMINANDO LOG DE ACCESOS AL NODO %d\n",nodo);
		// La informacion que tengo en el BUFFER 'buf' la salvo en la base de datos cuando este completamente seguro que se logro borrar en la controladora. 
		// Esto es cuando se analice la informacion que viene en BUFFER 'borrarbuf'.
		ret = enviarTrama(sd, nodo, TxBfDel, 9, borrarbuf);
		if(ret==-1) return -1; // sucedio una condicion de borde cuando se enviaba la trama al nodo....
		int logborrado =borrarbuf[3]==1 && borrarbuf[5]==0;
		if(logborrado) escribirlog(conn, buf, len);
	}
	return 1; //todo bien..
}



void dbconnect(MYSQL *conn){ 
	if(debuga) printf("dbconnect entrando\n");
	//if (mysql_real_connect(conn, "localhost", "churunmeru", "guaratara", "sip2", 0, NULL, 0) == NULL) {
	if (mysql_real_connect(conn, dbhost, dbuser, dbpassword, dbname, 0, NULL, 0) == NULL) {
		printf("Error1 %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
}


void logNodoNoDisponible(MYSQL *conn, int nodo, char ipv4[], int puerto){ 
	char query[1024];
	char mensaje[1024];
	sprintf(mensaje, "El nodo %d no esta disponible a traves de la direccion ip %s y el puerto %d",nodo,ipv4,puerto);
	sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,log) VALUES (%d,'%s')",nodo,mensaje);
	if (mysql_query(conn, query)) {
		printf("Error2 %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}

}


void escribirlog(MYSQL *conn, unsigned char log[], int len){ 
	int codigofuncion = log[3];
	int dirnodofuente = log[4];
	int segundo = log[5];
	int minuto = log[6];
	int hora = log[7];
	int diasemana = log[8];
	int dia = log[9];
	int mes = log[10];
	int anio = log[11];
	int nodolectora = log[12];
	int indiceusuario=0;			//log[13]+log[14]

	int subcodigo = log[15];
	int subfuncion = log[16];
	int codigoext = log[17];
	int nivelusuario = log[18];

	int numeroaltotarjeta=0;		//log[19]+log[20]

	int numeropuerta = log[21];
	int revision = log[22];

	int numerobajotarjeta=0;		//log[23]+log[24]
	int numerotarjeta=0;			//log[19]+log[20]+log[23]+[24]
	int valortransferido=0;			//log[25]+log[26]






	char str[10];

	if(subcodigo==21){
		sprintf(str,"%02X%02X",log[19],log[20]);
		sscanf(str,"%x",&numeroaltotarjeta);

		sprintf(str,"%02X%02X",log[23],log[24]);
		sscanf(str,"%x",&numerobajotarjeta);

		sprintf(str,"%02X%02X%02X%02X",log[19],log[20],log[23],log[24]);
		sscanf(str,"%x",&numerotarjeta);
	}



	sprintf(str,"%02X%02X",log[13],log[14]);
	sscanf(str,"%x",&indiceusuario);

	sprintf(str,"%02X%02X",log[25],log[26]);
	sscanf(str,"%x",&valortransferido);


	char mensaje[1024];
	damelogtraducido(codigofuncion, mensaje);
	printf("%s\n",mensaje);


	char query[1024];

	sprintf(query, "INSERT INTO ctrl_logacceso (dirnodofuente,nodolectora,numeropuerta,revision,codigofuncion,valortransferido,indiceusuario,numerotarjeta,numeroaltotarjeta,numerobajotarjeta,subcodigo,subfuncion,codigoext,nivelusuario,anio,mes,dia,diasemana,hora,minuto,segundo,log) VALUES (%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,'%s')",dirnodofuente,nodolectora,numeropuerta,revision,codigofuncion,valortransferido,indiceusuario,numerotarjeta,numeroaltotarjeta,numerobajotarjeta,subcodigo,subfuncion,codigoext,nivelusuario,anio,mes,dia,diasemana,hora,minuto,segundo,mensaje);
	if (mysql_query(conn, query)) {
		printf("Error2 %u: %s\n", mysql_errno(conn), mysql_error(conn));
		return;
	}

/*
Cuando se analiza un log este puede ser provocado por pasar una tarjeta o puede ser culquier evento no relacionado con pasar tarjetas por las lectoras.
Ejemplos de eventos que no son relacionados con pasos de tarjetas por lectoras:
 7e 20 00 14 02 32 07 12 06 1d 07 0b 08 00 00 00 00 00 3d 00 00 08 58 00 00 d5 ff 58 e4 02 00 00 28 75
 7e 20 00 18 02 10 25 09 02 01 08 0b 00 00 00 fc 00 00 3d 00 00 fb 58 00 00 d5 ff 58 e4 02 00 00 2f 3b
 7e 20 00 11 02 13 25 09 02 01 08 0b 03 00 00 00 00 00 3d 00 00 02 58 00 00 d5 ff 58 e4 02 00 00 23 39
Aqui el evento es señalado como 14, 18 y 11. en estos 3 casos los campos correspondientes con usuarios y tarjetas estan en 00.

Ejemplos de eventos asociados a paso de tarjetas por lectoras.
 7e 20 00 05 02 21 02 0a 02 01 08 0b 03 00 02 05 01 10 00 07 42 02 58 34 85 d5 ff 58 e4 02 00 00 fe d1
 7e 20 00 06 02 26 02 0a 02 01 08 0b 03 00 01 05 01 10 01 07 35 02 58 9f c5 d5 ff 58 e4 02 00 00 64 db
Aqui los eventos señalados son: 05 y 06. Los campos '9f c5' y '34 85' corresponde a tarjetas.
 7e 20 00 05 02 34 2d 0c 02 01 08 0b 03 00 02 05 01 10 00 07 42 02 58 34 85 d5 ff 58 e4 02 00 00 c2 d5
 7e 20 00 05 02 22 30 0c 02 01 08 0b 03 00 02 05 01 10 00 07 42 02 58 34 85 d5 ff 58 e4 02 00 00 c9 cd

*/
}


void damelogtraducido(int funcion, char mensaje[]){ 
	switch(funcion){
            case 0 : sprintf(mensaje,"Site Code Error");
                     break;
            case 1 : sprintf(mensaje,"Access via User Address and PIN or PIN Only Error");
                     break;
            case 2 : sprintf(mensaje,"Key Pad Locked ");
                     break;
            case 3 : sprintf(mensaje,"Invalid Card Access");
                     break;
            case 4 : sprintf(mensaje,"Acceso no Permitido por Restriccion en Horario");
                     break;
            case 5 : sprintf(mensaje,"Access door group is error");
                     break;
            case 6 : sprintf(mensaje,"Expire date");
                     break;
            case 7 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 8 : sprintf(mensaje,"User input PIN code incorrect ");
                     break;
            case 9 : sprintf(mensaje,"Duress Message. (Emergency Help)");
                     break;
            case 10: sprintf(mensaje,"Acceso Normal con Tarjeta");
                     break;
            case 11: sprintf(mensaje,"Acceso Normal con Tarjeta");
                     break;
            case 12 : sprintf(mensaje,"Force Controller Relay On/Off [12/13] ");
                     break;
            case 13 : sprintf(mensaje,"Force Controller Relay On/Off [12/13] ");
                     break;
            case 14 : sprintf(mensaje,"Enter Arming status ");
                     break;
            case 15 : sprintf(mensaje,"Enter Disarming status (Exit Arming) ");
                     break;
            case 16: sprintf(mensaje,"Door open via Egress Button ");
                     break;
            case 17: sprintf(mensaje,"Alarma por Evento");
                     break;
            case 18 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 19 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 20 : sprintf(mensaje,"Controller power off ");
                     break;
            case 21 : sprintf(mensaje,"Duress event ");
                     break;
            case 22 : sprintf(mensaje,"Help message of guest ");
                     break;
            case 23 : sprintf(mensaje,"Cleaner access message ");
                     break;
            case 24 : sprintf(mensaje,"Controller power on ");
                     break;
            case 25 : sprintf(mensaje,"Force Controller Relay On/Off failed ");
                     break;
            case 26 : sprintf(mensaje,"Return to normal status ( Analog I/O event ) ");
                     break;
            case 27 : sprintf(mensaje,"Panic Button Event ");
                     break;
            case 28 : sprintf(mensaje,"Normal Access via PIN code ");
                     break;
            case 29 : sprintf(mensaje,"DI status has changed,SubCode:00:Off, 01:On. Reader Node ID: DI number ");
                     break;
            case 30 : sprintf(mensaje,"Anti-pass-back error ");
                     break;
            case 31: sprintf(mensaje,"Reader disconnected with controller ");
                     break;
            case 32 : sprintf(mensaje,"Reader reconnected with controller ");
                     break;
            case 33 : sprintf(mensaje,"User change PIN code");
                     break;
            case 34 : sprintf(mensaje,"User change PIN code error");
                     break;
            case 35 : sprintf(mensaje,"Controller Enter Auto Open Procedure");
                     break;
            case 36 : sprintf(mensaje,"Controller Exit from Auto Open Procedure");
                     break;
            case 37 : sprintf(mensaje,"Disarming by auto time procedure");
                     break;
            case 38 : sprintf(mensaje,"Arming by auto time procedure");
                     break;
            case 39 : sprintf(mensaje,"Access by Fingerprint");
                     break;
            case 40 : sprintf(mensaje,"Fingerprint image failed");
                     break;
            case 41 : sprintf(mensaje,"");
                     break;
            case 42 : sprintf(mensaje,"Remote Key:Up");
                     break;
            case 43 : sprintf(mensaje,"Disable Reader");
                     break;
            case 44 : sprintf(mensaje,"Enable Reader");
                     break;
            case 45 : sprintf(mensaje,"Remote Key:Panic");
                     break;
            case 46 : sprintf(mensaje,"Entrance");
                     break;
            case 47 : sprintf(mensaje,"Appearance");
                     break;
            case 48 : sprintf(mensaje,"Triger counter");
                     break;
            case 49 : sprintf(mensaje,"Reader Relay Latch On/Off. Sub Function:00:Door Relay ON,01:Door relay OFF");
                     break;
            case 50 : sprintf(mensaje,"DI point Close/Open(50/51), The Pp number put in(sub-function)");
                     break;
            case 51 : sprintf(mensaje,"DI point Close/Open(50/51), The Pp number put in(sub-function)");
                     break;
            case 52 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 53 : sprintf(mensaje,"Enter/Exit Edit Mode");
                     break;
            case 54 : sprintf(mensaje,"Remote controller message");
                     break;
            case 55 : sprintf(mensaje,"Global free access (Controller in global free mode)");
                     break;
            case 56 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 57 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 58 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 59 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 60 : sprintf(mensaje,"Flash Card But No Open Door");
                     break;
            case 61 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 62 : sprintf(mensaje,"yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");
                     break;
            case 63 : sprintf(mensaje,"Soyal Open System Rule : Instruction Failed");
                     break;
            case 64 : sprintf(mensaje,"Soyal Open System Rule : Deduct value failed");
                     break;
            case 65 : sprintf(mensaje,"Soyal Open System Rule : Global Tag access ok");
                     break;
            case 66 : sprintf(mensaje,"Soyal Open System Rule : License layer feiled");
                     break;
            case 67 : sprintf(mensaje,"Soyal Open System Rule : Before begin date");
                     break;
            case 68 : sprintf(mensaje,"Soyal Open System Rule : Expiry date");
                     break;
            case 69 : sprintf(mensaje,"Soyal Open System Rule : Balance not enough");
                     break;
            case 70 : sprintf(mensaje,"Soyal Open System Rule : Deduct value ok");
                     break;
            case 71 : sprintf(mensaje,"Soyal Open System Rule : Read floor data from tag failed");
                     break;
            case 72 : sprintf(mensaje,"Soyal Open System Rule : Global access tag deduct value success");
                     break;
            case 73 : sprintf(mensaje,"Soyal Open System Rule : Global access tag balance not enough");
                     break;
            case 74 : sprintf(mensaje,"Soyal Open System Rule : Global access tag deduct value failed");
                     break;
            case 100 : sprintf(mensaje,"Access OK via Vein");
                     break;
            case 101 : sprintf(mensaje,"Access failed via Vein");
                     break;
        }
}



void dbping(MYSQL *conn){ 
	printf("PINGING A MYSQL\n\n");
	int id1 = mysql_thread_id(conn);
	if (mysql_ping(conn)) {
		printf("Error:Ping %u: %s\n", mysql_errno(conn), mysql_error(conn));
		exit(1);
	}
	int id2 = mysql_thread_id(conn);
	if(id1 != id2){
		printf("PINGING A MYSQL: CONEXION RECONECTADA\n");
	}
}


int enviarTrama(int sd, unsigned char nodo, unsigned char Tx[], int Txlen, unsigned char buf[]){
	int len;
	for(int k=0; k<Txlen; k++) printf("%02X ",Tx[k]); 
	printf("\n");

	write(sd, Tx, Txlen);

	fd_set fds;
	struct timeval timeout;
	int rc;
	timeout.tv_sec = 15;
	timeout.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(sd, &fds);

	rc = select(sizeof(fds)*8, &fds, NULL, NULL, &timeout);
	if (rc==-1) {
		//printf("\nEL NODO %d EN LA DIRECCION IP %s Y PUERTO %d NO ESTA ACCESIBLE\n",nodo,ipv4,puerto);
		printf("\nEL NODO %d NO ESTA ACCESIBLE\n",nodo);
		return -1;
	}

	//len = read(sd, buf, buflen);
	len = recv(sd, buf, buflen, 0);
	if(len==0){
		printf("El nodo %d no esta accesible\n",nodo);
		return -1;
	}
	buf[len] = '\0';

	printf("RECIBIDO:\n");		
	for(int k=0; k<len; k++) printf("%02X ",buf[k]);
	printf("\n");

	return 1;
}

/*
7E head
0A length
00 destination id
01 command code
02 source node id
00 EVENT: Neither card nor key event, just echo device I/O status.
07 D0: 
08 D1:
FF D2:
00 D3:
0C xor
1D sum
*/

/*
7e 
20 
00 
0b COMMAND CODE
02 source node id
1a EVENT
27 D0
0a D1
04 D2
16 D3
06 
0b 
03 00 01 05 01 10 00 07 35 02 58 9f c5 d5 ff 58 e4 02 00 38 56 37
*/






/*
socat -x TCP-L:1621 TCP:192.168.1.127:1621
7e 20 00 03 02 09 30 11 06 03 06 0b 03 8a ce 15 33 10 00 00 10 02 58 8a ce d5 ff 58 e4 02 00 00 35 25

   LE      FU  NODO            DIA             NODO  INDICE   SUB  SUB  EXT  USER    TARJET  NUME       TARJET  TRANSF   TAG
   NG      NC  FUEN  SS:MM:HH SEMAN  DD/MM/YY  LECT  USUARIO  COD  FUNC CODE LEVE    ALTO    PUER       BAJO    VALUE    BALANCE             XOR  SUM
===========================================================================================================================================
7E 20  00  01   02   1F 29 09  04    08 06 0B  03    3A 98    00   33   10   00      00 10   02     58  8A CE   10 E1    58 E4    02 00 00   01   7B//PIN 4321 NO VALIDO, TRANS VALUE X:162E=D:4321
7E 20  00  0A   02   12 34 09  04    08 06 0B  03    00 01    00   33   10   00      00 10   02     58  8A CE   D5 FF    58 E4    02 00 00   62   F5//PIN 9999 ES VALIDO, TRANS VALUE X:D5FF=D:54783
7E 20  00  01   02   0F 38 09  04    08 06 0B  03    3A 98    00   33   10   00      00 10   02     58  8A CE   27 0E    58 E4    02 00 00   D8   95//PIN 9998 NO VALIDO, TRANS VALUE X:270E=D:9998
7E 20  00  0A   02   0A 05 0A  04    08 06 0B  03    00 02    00   33   10   00      00 10   02     58  8A CE   D5 FF    58 E4    02 00 00   4B   A9//PIN 1234 ES VALIDO, TRANS VALUE X:D5FF=D:54783

7E 20  00  0B   02   1A 16 0B  04    08 06 0B  03    00 01    15   01   10   00      07 35   02     58  9F C5   D5 FF    58 E4    02 00 00   50   EB//con tarjeta
7E 20  00  03   02   1D 16 0B  04    08 06 0B  03    8A CE    15   33   10   00      00 10   02     58  8A CE   D5 FF    58 E4    02 00 00   14   FB//con tarjeta
7E 20  00  0B   02   20 16 0B  04    08 06 0B  03    00 02    15   01   10   00      07 42   02     58  34 85   D5 FF    58 E4    02 00 00   F5   F9//con tarjeta

7e 20  00  0a   02   0d 2f 0b  04    08 06 0b  03    00 02    00   01   10   00      07 42   02     58  34 85   d5 ff    58 e4    02 00 00   f5   e9
7e 20  00  0a   02   10 2f 0b  04    08 06 0b  03    00 01    00   01   10   00      07 42   02     58  34 85   d5 ff    58 e4    02 00 00   eb   e1
7e 20  00  03   02   1b 2f 0b  04    08 06 0b  03    8a ce    15   33   10   00      00 10   02     58  8a ce   d5 ff    58 e4    02 00 00   2b   29
=============================================================================================================================================
0  1   2   3     4    5  6  7   8    9  10 11  12    13 14    15   16   17   18      19 20   21     22  23 24   25 26    27 28    29 30 31   32   33//POSICION EN LA TRAMA.


TAG BALANCE :58E4=22756
TRANFS VALUE:D5FF=54783, 10E10=>4321
CUANDO SE COLOCA UN PIN EL VALOR DE ESE PIN VIENE EN TRANFS VALUE.
======================
LA CONTROLADORA SOBREESCRIBE SOLO LOS CAMPOS DEL REGISTRO QUE SON AFECTADOS.
CUANDO EN EL REGISTRO SE TIENE UNA TRANSACCION DE TARJETA SE MODIFICAN TODOS LOS REGISTROS.
CUANDO HABIA EN EL REGISTRO UNA TRANSACCION DE TARJETA Y SE PRODUCE UNA TRANSACCION POR PIN, 
ENTONCES SE MODIFICAN SOLO LOS CAMPOS QUE TIENEN QUE VER CON PIN, MIENTRAS QUE PERMANECEN INTANCTOS 
CON LOS DATOS DE LA TRANSACCION ANTERIOR AQUELLOS CAMPOS QUE NO ESTAN RELACIONADOS CON PIN.
EJEMPLO:
7E 20  00  0B   02   20 16 0B  04  08 06 0B  03   00 02    15 01 10 00  07 42   02     58  34 85   D5 FF    58 E4    02 00 00   F5   F9//CON TARJETA
7e 20  00  0a   02   0d 2f 0b  04  08 06 0b  03   00 02    00 01 10 00  07 42   02     58  34 85   d5 ff    58 e4    02 00 00   f5   e9//CON PIN
PERMANECE 0742, 3485
===========
PARA DIFERENCIAR SI SE ESTA HACIENDO REFERENCIA A UNA TARJETA O A UN PIN UTILIZO EL CAMPO 'SUB COD'
SUBCODE=00 => PIN
SUBCODE=15 => TARJETA
============



DROP TABLE IF EXISTS `ctrl_logacceso`;
CREATE TABLE IF NOT EXISTS `ctrl_logacceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dirnodofuente` int(3) NOT NULL,
  `nodolectora` int(3) NOT NULL,
  `numeropuerta` int(3) NOT NULL,
  `revision` int(3) NOT NULL,
  `indiceusuario` int(6) NOT NULL,
  `valortransferido` int(6) NOT NULL,
  `numerotarjeta` int(11) NOT NULL,
  `numeroaltotarjeta` int(6) NOT NULL,
  `numerobajotarjeta` int(6) NOT NULL,
  `codigofuncion` int(3) NOT NULL,
  `subcodigo` int(3) NOT NULL,
  `subfuncion` int(3) NOT NULL,
  `codigoext` int(3) NOT NULL,
  `nivelusuario` int(3) NOT NULL,
  `anio` int(2) NOT NULL,
  `dia` int(2) NOT NULL,
  `mes` int(2) NOT NULL,
  `diasemana` int(1) NOT NULL,
  `hora` int(2) DEFAULT NULL,
  `minuto` int(2) DEFAULT NULL,
  `segundo` int(2) DEFAULT NULL,
  `log` varchar(255) NOT NULL,
  `fcreado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


*/



int config(void){
	FILE *fp;
	if((fp=fopen("comm.conf", "r"))==NULL) {
		printf("No Se pudo abrir el archivo de configuracion 'comm.conf'.\n");
		exit(1);
	}
	char str[250];
	dbhost[0]='\0';
	dbuser[0]='\0';
	dbpassword[0]='\0';
	dbname[0]='\0';
	while(!feof(fp)) {
		if(fgets(str, 126, fp)){
			str[strlen(str)-1]='\0';
			char *p;
			if(str[0]=='#') continue; 
			p = strstr(str, "dbhost=");
			if(p!=NULL) strcpy (dbhost,p+7); 
			p = strstr(str, "dbuser=");
			if(p!=NULL) strcpy (dbuser,p+7); 
			p = strstr(str, "dbpassword=");
			if(p!=NULL) strcpy (dbpassword,p+11); 
			p = strstr(str, "dbname=");
			if(p!=NULL) strcpy (dbname,p+7); 
		}
	}
	fclose (fp);
	if(dbhost[0]=='\0'){
		printf("El parametro dbhost no pudo ser leido\n");
		exit(1);
	}
	if(dbuser[0]=='\0'){
		printf("El parametro dbuser no pudo ser leido\n");
		exit(1);
	}
	if(dbpassword[0]=='\0'){
		printf("El parametro dbpassword no pudo ser leido\n");
		exit(1);
	}
	if(dbname[0]=='\0'){
		printf("El parametro dbname no pudo ser leido\n");
		exit(1);
	}
	printf("dbhost:%s\n",dbhost);
	printf("dbname:%s\n",dbname);
	printf("dbuser:%s\n",dbuser);
	printf("dbpassword:%s\n",dbpassword);





	return 0;
}

/*
Con esta trama, el numero 25 se esta pidiendo el ultimo evento o log a la controladora
7e 04 02 25 d8 ff 00

En esta trama el 11 indica que se origino una alarma por un evento. en este caso paso que se abrio la puerta desde la pc, pero no se cerro nunca, entonces se mostraba cada ves esta alarma.
7e 20 00 11 02 00 00 00 04 14 07 0b 03 00 00 00 00 10 01 07 35 02 58 9f c5 d5 ff 58 e4 02 00 38 7c 11

*/






