-- mysql -h <IP ADDRESS> -u <USER> -p < MYSQL_sip_base.sql
--

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Table structure for table `accesopc`
--

CREATE TABLE IF NOT EXISTS `accesopc` (
  `idaccesopc` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de Acceso PC',
  `idaccesopc_tipo` varchar(20) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Codigo tipo de acceso',
  `usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Usuario',
  `ipv4_pc` varchar(15) COLLATE utf8_spanish_ci NOT NULL COMMENT 'IP de la PC',
  `fecha_expiracion` date NOT NULL COMMENT 'Fecha de expiracion',
  `cookie` varchar(40) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Cookie de seguridad',
  PRIMARY KEY (`idaccesopc`),
  UNIQUE KEY `usuario_2` (`usuario`,`ipv4_pc`,`idaccesopc_tipo`),
  KEY `usuario` (`usuario`),
  KEY `idaccesopc_tipo` (`idaccesopc_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `accesopc_tipo`
--

CREATE TABLE IF NOT EXISTS `accesopc_tipo` (
  `idaccesopc_tipo` varchar(20) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Codigo tipo de acceso',
  `path` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Descripcion',
  PRIMARY KEY (`idaccesopc_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tipo de acceso';

--
-- Dumping data for table `accesopc_tipo`
--

INSERT INTO `accesopc_tipo` (`idaccesopc_tipo`, `path`, `descripcion`) VALUES
('CarnetsEmp', '/sip/carempleado/', 'GeneraciÃ³n de Carnets de Empleados'),
('CarnetsVis', '/sip/carvisitante/', 'GeneraciÃ³n de Carnets para Visitantes'),
('CtrlAcceso', '/sip/ctrlmonitoracceso/', 'Monitor de Control de Accesos'),
('CtrlAsistencia', '/sip/ctrlmonitorasistencia/', 'Monitor de Control de Asistencia'),
('MonitorVid', '/sip/monitorremoto/', 'Monitor Remoto de Video'),
('Visitantes', '/sip/visitantes/', 'Registro de Visitantes');

-- --------------------------------------------------------

--
-- Table structure for table `car_cargo`
--

CREATE TABLE IF NOT EXISTS `car_cargo` (
  `idcargo` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del cargo carnet',
  `idplantilla` int(11) DEFAULT NULL COMMENT 'Codigo de la plantilla',
  `cargo_nombre_chk` int(1) DEFAULT NULL COMMENT 'Opcion para ver el nombre del cargo en la plantilla',
  `cargo_nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre',
  `cargo_top` int(3) DEFAULT NULL COMMENT 'Coordenada y1',
  `cargo_left` int(3) DEFAULT NULL COMMENT 'Coordenada x1',
  `cargo_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `cargo_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `cargo_color` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color de la fuente',
  `cargo_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color del fondo',
  `cargo_fuentetamano` float DEFAULT NULL COMMENT 'Tamaño de la fuente',
  `cargo_fuenteletra` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Tipo de letra',
  `cargo_fuentealign` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Alineacion horizontal',
  `cargo_fuentevalign` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Alineacion vertical',
  PRIMARY KEY (`idcargo`),
  KEY `idplantilla` (`idplantilla`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Cargos de un carnet' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_departamento`
--

CREATE TABLE IF NOT EXISTS `car_departamento` (
  `iddepartamento` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo departamento carnet',
  `idplantilla` int(11) DEFAULT NULL COMMENT 'Codigo de la plantilla',
  `departamento_nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre',
  `departamento_top` int(3) DEFAULT NULL COMMENT 'Coordenada y1',
  `departamento_left` int(3) DEFAULT NULL COMMENT 'Coordenada x1',
  `departamento_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `departamento_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `departamento_color` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color de la fuente',
  `departamento_fuentetamano` float DEFAULT NULL COMMENT 'Tamaño de la Fuente',
  `departamento_fuenteletra` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Tipo de letra',
  `departamento_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establecer color de fondo',
  `departamento_bgcolor` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color de fonfo',
  `departamento_fuentealign` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Alineacion horizontal',
  `departamento_fuentevalign` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Alineacion vertical',
  PRIMARY KEY (`iddepartamento`),
  KEY `idplantilla` (`idplantilla`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Departamentos de un carnet' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_empleado`
--

CREATE TABLE IF NOT EXISTS `car_empleado` (
  `idcarnet_empleado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del carnet del empleado',
  `idplantilla` int(11) NOT NULL COMMENT 'Codigo de la plantilla',
  `iddepartamento` int(11) NOT NULL COMMENT 'Codigo del departamento',
  `idcargo` int(11) NOT NULL COMMENT 'Codigo del cargo',
  `cedula` varchar(8) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Cedula del empleado',
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Primer nombre del empleado',
  `apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Primer apellido del empleado',
  `archivo_foto` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Archivo de la foto del carnet',
  `fecha_vencimiento` date DEFAULT NULL COMMENT 'Fecha de vencimiento (opcional)',
  PRIMARY KEY (`idcarnet_empleado`),
  UNIQUE KEY `cedula` (`cedula`),
  KEY `idplantilla` (`idplantilla`),
  KEY `iddepartamento` (`iddepartamento`),
  KEY `idcargo` (`idcargo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Registro de carnets de los empleados' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_empleado_impresion`
--

CREATE TABLE IF NOT EXISTS `car_empleado_impresion` (
  `idcarnet_empleado_impresion` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo impresion carnet',
  `idcarnet_empleado` int(11) NOT NULL COMMENT 'Codigo carnet',
  `usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Usuario ',
  `modo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Impresion del frente o reverso',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Marca de tiempo',
  PRIMARY KEY (`idcarnet_empleado_impresion`),
  KEY `idcarnet_empleado` (`idcarnet_empleado`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Log de impresion de carnets' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_plantilla`
--

CREATE TABLE IF NOT EXISTS `car_plantilla` (
  `idplantilla` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo plantilla',
  `plantilla` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre de la plantilla',
  `orientacion` enum('V','H') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'V' COMMENT 'Orientacion del carnet (horizontal - vertical)',
  `archivo_plantilla` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'nombre del archivo',
  `archivoback_plantilla` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'nombre del archivo de reverso',
  `logo_chk` int(1) DEFAULT NULL COMMENT 'Opcion para establecer el logo',
  `archivo_logo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Archivo del logo',
  `foto_top` int(3) NOT NULL COMMENT 'cordenada y1 de la foto',
  `foto_left` int(3) NOT NULL COMMENT 'cordenada x1 de la foto',
  `foto_w` int(3) NOT NULL COMMENT 'ancho de la foto',
  `foto_h` int(3) NOT NULL COMMENT 'alto de la foto',
  `barcode_chk` int(1) DEFAULT NULL COMMENT 'Opcion para establecer el codigo de barras',
  `barcode_front` int(1) DEFAULT NULL COMMENT 'Opcion para establecer el codigo de barras en el frente',
  `barcode_back` int(1) DEFAULT NULL COMMENT 'Opcion para establecer el codigo de barras en el reverso',
  `barcode_top` int(3) DEFAULT NULL COMMENT 'cordenada y1',
  `barcode_left` int(3) DEFAULT NULL COMMENT 'cordenada x1',
  `barcode_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `barcode_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `logo_top` int(3) DEFAULT NULL COMMENT 'cordenada y1 del logo',
  `logo_left` int(3) DEFAULT NULL COMMENT 'cordenada x1 del logo',
  `logo_w` int(3) DEFAULT NULL COMMENT 'Ancho del Logo',
  `logo_h` int(3) DEFAULT NULL COMMENT 'Alto del logo',
  `logoback_chk` int(1) DEFAULT NULL COMMENT 'Opcion para establecer la etiqueta',
  `archivoback_logo` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Nombre del archivo del logo de reverso',
  `logoback_top` int(3) DEFAULT NULL COMMENT 'cordenada y1',
  `logoback_left` int(3) DEFAULT NULL COMMENT 'cordenada x1',
  `logoback_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `logoback_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `institucion_chk` int(1) DEFAULT NULL COMMENT 'Opcion para establecer la institucion',
  `institucion_nombre` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'nombre de la institucion',
  `institucion_top` int(3) DEFAULT NULL COMMENT 'cordenada y1 del nombre de la institucion',
  `institucion_left` int(3) DEFAULT NULL COMMENT 'cordenada x1 del nombre de la institucion',
  `institucion_w` int(3) DEFAULT NULL COMMENT 'Ancho de la institucion',
  `institucion_h` int(3) DEFAULT NULL COMMENT 'Alto de la institucion',
  `institucion_color` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color fuente institucion',
  `institucion_fuentetamano` float NOT NULL COMMENT 'Tamaño de la fuente',
  `institucion_fuenteletra` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de letra',
  `institucion_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establer color de fonfo',
  `institucion_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fonfo',
  `institucion_fuentealign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion horizontal',
  `institucion_fuentevalign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion vertical',
  `institucionback_chk` int(1) DEFAULT NULL COMMENT 'Opcion para establecer la etiqueta',
  `institucionback_top` int(3) DEFAULT NULL COMMENT 'cordenada y1',
  `institucionback_left` int(3) DEFAULT NULL COMMENT 'cordenada x1',
  `institucionback_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `institucionback_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `institucionback_color` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color de la fuente',
  `institucionback_fuentetamano` float NOT NULL COMMENT 'Tamaño de la fuente',
  `institucionback_fuenteletra` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de letra',
  `institucionback_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establer color de fonfo',
  `institucionback_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fonfo',
  `institucionback_fuentealign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion horizontal',
  `institucionback_fuentevalign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion vertical',
  `nombreapellido_top` int(3) NOT NULL COMMENT 'cordenada y1 de nombre y apellido',
  `nombreapellido_left` int(3) NOT NULL COMMENT 'cordenada x1 de nombre y apellido',
  `nombreapellido_w` int(3) NOT NULL COMMENT 'Ancho de nombreapellido',
  `nombreapellido_h` int(3) NOT NULL COMMENT 'Alto de nombreapellido',
  `nombreapellido_color` varchar(12) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color fuente nombreapellido',
  `nombreapellido_fuentetamano` float NOT NULL COMMENT 'Tamaño de la fuente',
  `nombreapellido_fuenteletra` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de letra',
  `nombreapellido_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establer color de fonfo',
  `nombreapellido_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fonfo',
  `nombreapellido_fuentealign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion horizontal',
  `nombreapellido_fuentevalign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion vertical',
  `cedula_top` int(3) NOT NULL COMMENT 'cordenada y1 de la cedula',
  `cedula_left` int(3) NOT NULL COMMENT 'cordenada x1 de la cedula',
  `cedula_w` int(3) NOT NULL COMMENT 'Ancho de la cedula',
  `cedula_h` int(3) NOT NULL COMMENT 'Alto de la cedula',
  `cedula_color` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color fuente cedula',
  `cedula_fuentetamano` float NOT NULL COMMENT 'Tamaño de la fuente',
  `cedula_fuenteletra` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de letra',
  `cedula_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establer color de fonfo',
  `cedula_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fonfo',
  `cedula_fuentealign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion horizontal',
  `cedula_fuentevalign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion vertical',
  `fecha_chk` int(1) DEFAULT NULL COMMENT 'Opcion para establecer la etiqueta',
  `fecha_top` int(3) DEFAULT NULL COMMENT 'cordenada y1',
  `fecha_left` int(3) DEFAULT NULL COMMENT 'cordenada x1',
  `fecha_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `fecha_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `fecha_color` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color de la fuente',
  `fecha_fuentetamano` float NOT NULL COMMENT 'Tamaño de la fuente',
  `fecha_fuenteletra` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de letra',
  `fecha_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establer color de fonfo',
  `fecha_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fonfo',
  `fecha_fuentealign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion horizontal',
  `fecha_fuentevalign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion vertical',
  `visitante_top` int(3) DEFAULT NULL COMMENT 'cordenada y1',
  `visitante_left` int(3) DEFAULT NULL COMMENT 'cordenada x1',
  `visitante_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `visitante_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `visitante_color` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color de la fuente',
  `visitante_fuentetamano` float NOT NULL COMMENT 'Tamaño de la fuente',
  `visitante_fuenteletra` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de letra',
  `visitante_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establer color de fonfo',
  `visitante_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fonfo',
  `visitante_fuentealign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion horizontal',
  `visitante_fuentevalign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion vertical',
  `visitantenro_top` int(3) DEFAULT NULL COMMENT 'cordenada y1',
  `visitantenro_left` int(3) DEFAULT NULL COMMENT 'cordenada x1',
  `visitantenro_w` int(3) DEFAULT NULL COMMENT 'Ancho',
  `visitantenro_h` int(3) DEFAULT NULL COMMENT 'Alto',
  `visitantenro_color` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Color de la fuente',
  `visitantenro_fuentetamano` float NOT NULL COMMENT 'Tamaño de la fuente',
  `visitantenro_fuenteletra` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de letra',
  `visitantenro_bgcolor_chk` int(1) DEFAULT NULL COMMENT 'Establer color de fonfo',
  `visitantenro_bgcolor` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fonfo',
  `visitantenro_fuentealign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion horizontal',
  `visitantenro_fuentevalign` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Alineacion vertical',
  PRIMARY KEY (`idplantilla`),
  UNIQUE KEY `plantilla` (`plantilla`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Plantilla del carnet' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_visitante`
--

CREATE TABLE IF NOT EXISTS `car_visitante` (
  `idcarnet_visitante` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del carnet para el vistante',
  `idplantilla` int(11) NOT NULL COMMENT 'Codigo de la plantilla',
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre ',
  `numero` int(11) NOT NULL COMMENT 'Numero de carnet',
  PRIMARY KEY (`idcarnet_visitante`),
  KEY `idplantilla` (`idplantilla`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Carnets para visitantes' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `car_visitante_impresion`
--

CREATE TABLE IF NOT EXISTS `car_visitante_impresion` (
  `idcarnet_visitante_impresion` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo impresion carnet',
  `idcarnet_visitante` int(11) NOT NULL COMMENT 'Codigo carnet',
  `usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Usuario',
  `modo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Impresion del frente o reverso',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Marca de tiempo',
  PRIMARY KEY (`idcarnet_visitante_impresion`),
  KEY `idcarnet_visitante` (`idcarnet_visitante`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Log de impresion de carnets' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `idcategoria` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(60) CHARACTER SET utf8 NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `categoria_padre` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoria`),
  KEY `fk_categoria_categoria1` (`categoria_padre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`idcategoria`, `nombre`, `descripcion`, `categoria_padre`) VALUES
('ADM', 'AdministraciÃ³n', NULL, NULL),
('ADMACCPC', 'Acceso PC', NULL, 'ADM'),
('ADMGPR', 'Grupos de Permisos Usr', NULL, 'ADM'),
('ADMPLT', 'Planta FÃ­sica', NULL, 'ADM'),
('ADMSEG', 'Segmentos', NULL, 'ADM'),
('ADMUSR', 'Uruarios de Sistema', NULL, 'ADM'),
('CAR', 'CarnetizaciÃ³n ', NULL, NULL),
('CARCRG', 'Cargos', NULL, 'CAR'),
('CARDPT', 'Departamentos', NULL, 'CAR'),
('CAREMP', 'Carnets Empleados', 'Generacion de carnet a los empleados', 'CAR'),
('CARLISTEMP', 'Listado Carnets Empleados', NULL, 'CAR'),
('CARPLT', 'Plantilla', NULL, 'CAR'),
('CARVIS', 'Carnets Visitantes', NULL, 'CAR'),
('CTRL', 'Control de Acceso', NULL, NULL),
('CTRLCTRL', 'Controladoras de Acceso', NULL, 'CTRL'),
('CTRLDS', 'DÃ­as Festivos', 'DÃ­as Feriados', 'CTRL'),
('CTRLGPR', 'Grupos de Acceso', NULL, 'CTRL'),
('CTRLHRS', 'Horarios de Trabajo', 'Time Zones - Zonas de tiempo', 'CTRL'),
('CTRLLEC', 'Lectoras de Acceso', NULL, 'CTRL'),
('CTRLLOG', 'Listado de Eventos', NULL, 'CTRL'),
('CTRLLOGA', 'Listado de Asistencia', NULL, 'CTRL'),
('CTRLMON', 'Monitor Acceso', NULL, 'CTRL'),
('CTRLMONA', 'Monitor Asistencia', NULL, 'CTRL'),
('CTRLPTS', 'Puertas de Acceso', NULL, 'CTRL'),
('CTRLUSR', 'Usuarios Tarjetahabientes', NULL, 'CTRL'),
('VID', 'Video Vigilancia', NULL, NULL),
('VIDACCPC', 'Acceso Monitoreo', NULL, 'VID'),
('VIDALARM', 'Listado de Alarmas', NULL, 'VID'),
('VIDASCMNTR', 'Asociar Monitoreo', NULL, 'VID'),
('VIDCAM', 'CÃ¡maras', NULL, 'VID'),
('VIDCAMMOD', 'Modelos de CÃ¡maras', NULL, 'VID'),
('VIDCAMMRC', 'Marcas de CÃ¡maras', NULL, 'VID'),
('VIDCAMTIP', 'Tipos de CÃ¡maras', NULL, 'VID'),
('VIDEXCAM', 'Exportar Camaras', 'Exportar Camaras para mobil', 'VID'),
('VIDGRD', 'Guardias de cÃ¡maras', NULL, 'VID'),
('VIDMNTR', 'Monitor Remoto', NULL, 'VID'),
('VIDSRV', 'Grabadores de Video', NULL, 'VID'),
('VIDZN', 'Zonas de Monitoreo', NULL, 'VID');

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_controladora`
--

CREATE TABLE IF NOT EXISTS `ctrl_controladora` (
  `idcontroladora` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la controladora',
  `idtipo_controladora` int(11) NOT NULL COMMENT 'Tipo de Controladora',
  `idsegmento` int(11) NOT NULL COMMENT 'Codigo de segmento',
  `nodo` int(3) NOT NULL COMMENT 'Numero de nodo',
  `ipv4` varchar(15) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Direccion IP',
  `puerto` varchar(4) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1621' COMMENT 'Puerto de la controladora',
  `maximo_usuarios` int(5) NOT NULL DEFAULT '5000' COMMENT 'Maximo de usuarios permitidos por controladora',
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre',
  `descripcion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Descripcion',
  `ubicacion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Ubicacion',
  `estado` int(1) NOT NULL DEFAULT '0' COMMENT 'Estado de la controladora',
  `fecha_peticion_estado` datetime DEFAULT NULL COMMENT 'Fecha de peticion de estado',
  `fecha_respuesta_estado` datetime DEFAULT NULL COMMENT 'Fecha de respuesta de estado',
  PRIMARY KEY (`idcontroladora`),
  UNIQUE KEY `index2` (`nodo`,`ipv4`,`puerto`),
  KEY `fk_ctrl_controladora_1` (`idtipo_controladora`),
  KEY `idsegmento` (`idsegmento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Controladoras de acceso' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_diafestivo`
--

CREATE TABLE IF NOT EXISTS `ctrl_diafestivo` (
  `id` int(2) NOT NULL COMMENT '0 al 63',
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `mes` int(2) NOT NULL,
  `dia` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ctrl_diafestivo`
--

INSERT INTO `ctrl_diafestivo` (`id`, `nombre`, `mes`, `dia`) VALUES
(0, '-', 0, 0),
(1, '-', 0, 0),
(2, '-', 0, 0),
(3, '-', 0, 0),
(4, '-', 0, 0),
(5, '-', 0, 0),
(6, '-', 0, 0),
(7, '-', 0, 0),
(8, '-', 0, 0),
(9, '-', 0, 0),
(10, '-', 0, 0),
(11, '-', 0, 0),
(12, '-', 0, 0),
(13, '-', 0, 0),
(14, '-', 0, 0),
(15, '-', 0, 0),
(16, '-', 0, 0),
(17, '-', 0, 0),
(18, '-', 0, 0),
(19, '-', 0, 0),
(20, '-', 0, 0),
(21, '-', 0, 0),
(22, '-', 0, 0),
(23, '-', 0, 0),
(24, '-', 0, 0),
(25, '-', 0, 0),
(26, '-', 0, 0),
(27, '-', 0, 0),
(28, '-', 0, 0),
(29, '-', 0, 0),
(30, '-', 0, 0),
(31, '-', 0, 0),
(32, '-', 0, 0),
(33, '-', 0, 0),
(34, '-', 0, 0),
(35, '-', 0, 0),
(36, '-', 0, 0),
(37, '-', 0, 0),
(38, '-', 0, 0),
(39, '-', 0, 0),
(40, '-', 0, 0),
(41, '-', 0, 0),
(42, '-', 0, 0),
(43, '-', 0, 0),
(44, '-', 0, 0),
(45, '-', 0, 0),
(46, '-', 0, 0),
(47, '-', 0, 0),
(48, '-', 0, 0),
(49, '-', 0, 0),
(50, '-', 0, 0),
(51, '-', 0, 0),
(52, '-', 0, 0),
(53, '-', 0, 0),
(54, '-', 0, 0),
(55, '-', 0, 0),
(56, '-', 0, 0),
(57, '-', 0, 0),
(58, '-', 0, 0),
(59, '-', 0, 0),
(60, '-', 0, 0),
(61, '-', 0, 0),
(62, '-', 0, 0),
(63, '-', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_grupo`
--

CREATE TABLE IF NOT EXISTS `ctrl_grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(5) NOT NULL COMMENT 'Numero de grupo',
  `grupo` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre del grupo',
  `idgrupo_padre` int(11) DEFAULT NULL COMMENT 'Codigo del grupo padre',
  `nivel` int(2) NOT NULL COMMENT 'Nivel de Prioridad',
  PRIMARY KEY (`idgrupo`),
  KEY `fk_ctrl_grupo_1` (`idgrupo_padre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Grupos de Acceso' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_grupo_puerta`
--

CREATE TABLE IF NOT EXISTS `ctrl_grupo_puerta` (
  `idgrupo` int(11) NOT NULL COMMENT 'Codigo del grupo',
  `idpuerta` int(11) NOT NULL COMMENT 'Codigo de la puerta',
  PRIMARY KEY (`idgrupo`,`idpuerta`),
  KEY `fk_ctrl_grupo_puerta_1` (`idgrupo`),
  KEY `fk_ctrl_grupo_puerta_2` (`idpuerta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Relacion muchos muchos de grupos de acceso con puertas';

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_lectora`
--

CREATE TABLE IF NOT EXISTS `ctrl_lectora` (
  `idlectora` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo',
  `idcontroladora` int(11) NOT NULL COMMENT 'Codigo de la controladora',
  `idpuerta` int(11) NOT NULL COMMENT 'Puerta asignada a la controladora',
  `numero` int(3) NOT NULL COMMENT 'Numero establecido por teclado lectora',
  `canal` int(1) NOT NULL COMMENT 'Canal (Channel) fisico donde esta conectada',
  `estado` int(1) NOT NULL DEFAULT '0' COMMENT 'Estado de la lectora',
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre de la lectora',
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Breve descripcion de la lectora',
  PRIMARY KEY (`idlectora`),
  UNIQUE KEY `idcontroladora` (`idcontroladora`,`numero`),
  UNIQUE KEY `idcontroladora_2` (`idpuerta`),
  KEY `index2` (`idcontroladora`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Lectora de Tarjetas' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_logacceso`
--

CREATE TABLE IF NOT EXISTS `ctrl_logacceso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dirnodofuente` int(3) NOT NULL,
  `nodolectora` int(3) NOT NULL,
  `numeropuerta` int(3) NOT NULL,
  `revision` int(3) NOT NULL,
  `indiceusuario` int(6) NOT NULL,
  `valortransferido` int(6) NOT NULL,
  `numerotarjeta` int(11) NOT NULL,
  `numeroaltotarjeta` int(6) NOT NULL,
  `numerobajotarjeta` int(6) NOT NULL,
  `codigofuncion` int(3) NOT NULL,
  `subcodigo` int(3) NOT NULL,
  `subfuncion` int(3) NOT NULL,
  `codigoext` int(3) NOT NULL,
  `nivelusuario` int(3) NOT NULL,
  `anio` int(2) NOT NULL,
  `dia` int(2) NOT NULL,
  `mes` int(2) NOT NULL,
  `diasemana` int(1) NOT NULL,
  `hora` int(2) DEFAULT NULL,
  `minuto` int(2) DEFAULT NULL,
  `segundo` int(2) DEFAULT NULL,
  `log` tinytext NOT NULL,
  `fcreado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idsincronizar` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_puerta`
--

CREATE TABLE IF NOT EXISTS `ctrl_puerta` (
  `idpuerta` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `puerta` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Descripcion de la puerta',
  `puerta_entrada` int(1) DEFAULT NULL COMMENT 'Puerta de entrada para control de asistencia',
  PRIMARY KEY (`idpuerta`),
  UNIQUE KEY `numero` (`numero`),
  UNIQUE KEY `puerta_entrada` (`puerta_entrada`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Puertas a controlar' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_sincronizar`
--

CREATE TABLE IF NOT EXISTS `ctrl_sincronizar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(100) NOT NULL,
  `accion` varchar(25) NOT NULL,
  `codigoaccion` int(3) NOT NULL,
  `nodo` int(3) NOT NULL,
  `ipv4` varchar(15) NOT NULL,
  `puerto` int(6) NOT NULL,
  `prioridad` int(2) NOT NULL,
  `param1` varchar(50) DEFAULT NULL,
  `param2` varchar(50) DEFAULT NULL,
  `param3` varchar(50) DEFAULT NULL,
  `param4` varchar(50) DEFAULT NULL,
  `param5` varchar(50) DEFAULT NULL,
  `param6` varchar(50) DEFAULT NULL,
  `param7` varchar(50) DEFAULT NULL,
  `param8` varchar(50) DEFAULT NULL,
  `param9` varchar(50) DEFAULT NULL,
  `param10` varchar(50) DEFAULT NULL,
  `param11` varchar(50) DEFAULT NULL,
  `param12` varchar(50) DEFAULT NULL,
  `param13` varchar(50) DEFAULT NULL,
  `param14` varchar(50) DEFAULT NULL,
  `param15` varchar(50) DEFAULT NULL,
  `param16` varchar(50) DEFAULT NULL,
  `param17` varchar(50) DEFAULT NULL,
  `param18` varchar(50) DEFAULT NULL,
  `param19` varchar(50) DEFAULT NULL,
  `param20` varchar(50) DEFAULT NULL,
  `param21` varchar(50) DEFAULT NULL,
  `param22` varchar(50) DEFAULT NULL,
  `param23` varchar(50) DEFAULT NULL,
  `param24` varchar(50) DEFAULT NULL,
  `param25` varchar(50) DEFAULT NULL,
  `param26` varchar(50) DEFAULT NULL,
  `param27` varchar(50) DEFAULT NULL,
  `param28` varchar(50) DEFAULT NULL,
  `param29` varchar(50) DEFAULT NULL,
  `param30` varchar(50) DEFAULT NULL,
  `param31` varchar(50) DEFAULT NULL,
  `param32` varchar(50) DEFAULT NULL,
  `param33` varchar(50) DEFAULT NULL,
  `param34` varchar(50) DEFAULT NULL,
  `param35` varchar(50) DEFAULT NULL,
  `param36` varchar(50) DEFAULT NULL,
  `param37` varchar(50) DEFAULT NULL,
  `param38` varchar(50) DEFAULT NULL,
  `param39` varchar(50) DEFAULT NULL,
  `param40` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_tipo_controladora`
--

CREATE TABLE IF NOT EXISTS `ctrl_tipo_controladora` (
  `idtipo_controladora` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_controladora` varchar(45) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Descripcion',
  PRIMARY KEY (`idtipo_controladora`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tipo de Controladora' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ctrl_tipo_controladora`
--

INSERT INTO `ctrl_tipo_controladora` (`idtipo_controladora`, `tipo_controladora`) VALUES
(1, '716E V3/E');

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_usuario`
--

CREATE TABLE IF NOT EXISTS `ctrl_usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del usuario',
  `idgrupo` int(11) NOT NULL COMMENT 'Codigo de grupo de puertas',
  `idzonadetiempo` int(11) NOT NULL COMMENT 'Codigo de horario de entrada y salida',
  `iddepartamento` int(11) DEFAULT NULL COMMENT 'Codigo del departamento',
  `numero` int(5) NOT NULL COMMENT 'Numero de usuario de acceso',
  `tipo_tarjeta` enum('empleado','visitante') COLLATE utf8_spanish_ci NOT NULL COMMENT 'Tipo de tarjeta',
  `codigo_pin` int(4) DEFAULT NULL COMMENT 'Codigo clave pin de acceso',
  `codigo_id` varchar(10) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Codigo identificador de la tarjeta',
  `codigo_sitio` varchar(5) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Codigo sitio de identificacion de la tarjeta',
  `codigo_tarjeta` varchar(5) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Codigo Tarjeta de identificacion de la tarjeta',
  `tarjetayopin` int(1) NOT NULL DEFAULT '1' COMMENT 'Tarjeta o Pin',
  `tienefechalimite` int(1) NOT NULL DEFAULT '0' COMMENT 'Tiene fecha limite de acceso',
  `fechahasta` date DEFAULT NULL COMMENT 'Fecha limite de acceso',
  `rondasguardias` int(1) NOT NULL DEFAULT '0' COMMENT 'Rondas de guardias',
  `skipfpcheck` int(1) NOT NULL DEFAULT '0',
  `modificable` int(1) NOT NULL DEFAULT '0' COMMENT 'Modificable',
  `nivel` int(2) NOT NULL DEFAULT '0' COMMENT 'Nivel de acceso',
  `antipassback` int(1) NOT NULL DEFAULT '0' COMMENT 'Anti Pass Back',
  `cedula` varchar(8) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Cedula ',
  `numero_visitante` int(11) DEFAULT NULL COMMENT 'Numero de tarjeta de visitante',
  `nombres` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Nombres',
  `apellidos` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Appelidos',
  `sexo` enum('F','M') COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Genetero masculino o femenino',
  `fecha_nacimiento` date DEFAULT NULL COMMENT 'Fecha de Nacimiento',
  `telefono` varchar(18) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Numero de telefono',
  `telefono_celular` varchar(18) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Numero de telefono celular',
  `direccion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `numero` (`numero`),
  UNIQUE KEY `codigo_sitio` (`codigo_sitio`,`codigo_tarjeta`),
  UNIQUE KEY `codigo_id` (`codigo_id`),
  UNIQUE KEY `cedula` (`cedula`),
  KEY `fk_ctrl_usuario_1` (`idgrupo`),
  KEY `fk_ctrl_usuario_2` (`idzonadetiempo`),
  KEY `iddepartamento` (`iddepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tarjetahabiente que se le va asignar el acceso' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ctrl_zonadetiempo`
--

CREATE TABLE IF NOT EXISTS `ctrl_zonadetiempo` (
  `indice` int(2) NOT NULL COMMENT '0 al 63',
  `descripcion` varchar(200) NOT NULL,
  `disponibleenferiado` tinyint(1) NOT NULL DEFAULT '0',
  `nivel` int(2) NOT NULL DEFAULT '0' COMMENT 'Nivel de Prioridad',
  `indicelink` int(2) DEFAULT NULL,
  `domingodesde` int(4) NOT NULL DEFAULT '0',
  `domingohasta` int(4) NOT NULL DEFAULT '0',
  `lunesdesde` int(4) NOT NULL DEFAULT '0',
  `luneshasta` int(4) NOT NULL DEFAULT '0',
  `martesdesde` int(4) NOT NULL DEFAULT '0',
  `marteshasta` int(4) NOT NULL DEFAULT '0',
  `miercolesdesde` int(4) NOT NULL DEFAULT '0',
  `miercoleshasta` int(4) NOT NULL DEFAULT '0',
  `juevesdesde` int(4) NOT NULL DEFAULT '0',
  `jueveshasta` int(4) NOT NULL DEFAULT '0',
  `viernesdesde` int(4) NOT NULL DEFAULT '0',
  `vierneshasta` int(4) NOT NULL DEFAULT '0',
  `sabadodesde` int(4) NOT NULL DEFAULT '0',
  `sabadohasta` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`indice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='almacena los 64 zonas de tiempo';

--
-- Dumping data for table `ctrl_zonadetiempo`
--

INSERT INTO `ctrl_zonadetiempo` (`indice`, `descripcion`, `disponibleenferiado`, `nivel`, `indicelink`, `domingodesde`, `domingohasta`, `lunesdesde`, `luneshasta`, `martesdesde`, `marteshasta`, `miercolesdesde`, `miercoleshasta`, `juevesdesde`, `jueveshasta`, `viernesdesde`, `vierneshasta`, `sabadodesde`, `sabadohasta`) VALUES
(0, 'Horario - Uno', 0, 0, NULL, 480, 1080, 480, 540, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(1, 'Horario - 1', 0, 0, NULL, 480, 1020, 480, 1020, 480, 1020, 480, 1020, 480, 1020, 480, 1020, 480, 1020),
(2, 'Horario - 2', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(3, 'Horario - 3', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(4, 'Horario - 4', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(5, 'Horario - 5', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(6, 'Horario - 6', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(7, 'Horario - 7', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(8, 'Horario - 8', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(9, 'Horario - 9', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(10, 'Horario - 10', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(11, 'Horario - 11', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(12, 'Horario - 12', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(13, 'Horario - 13', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(14, 'Horario - 14', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(15, 'Horario - 15', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(16, 'Horario - 16', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(17, 'Horario - 17', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(18, 'Horario - 18', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(19, 'Horario - 19', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(20, 'Horario - 20', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(21, 'Horario - 21', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(22, 'Horario - 22', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(23, 'Horario - 23', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(24, 'Horario - 24', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(25, 'Horario - 25', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(26, 'Horario - 26', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(27, 'Horario - 27', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(28, 'Horario - 28', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(29, 'Horario - 29', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(30, 'Horario - 30', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(31, 'Horario - 31', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(32, 'Horario - 32', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(33, 'Horario - 33', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(34, 'Horario - 34', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(35, 'Horario - 35', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(36, 'Horario - 36', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(37, 'Horario - 37', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(38, 'Horario - 38', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(39, 'Horario - 39', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(40, 'Horario - 40', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(41, 'Horario - 41', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(42, 'Horario - 42', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(43, 'Horario - 43', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(44, 'Horario - 44', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(45, 'Horario - 45', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(46, 'Horario - 46', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(47, 'Horario - 47', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(48, 'Horario - 48', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(49, 'Horario - 49', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(50, 'Horario - 50', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(51, 'Horario - 51', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(52, 'Horario - 52', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(53, 'Horario - 53', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(54, 'Horario - 54', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(55, 'Horario - 55', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(56, 'Horario - 56', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(57, 'Horario - 57', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(58, 'Horario - 58', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(59, 'Horario - 59', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(60, 'Horario - 60', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(61, 'Horario - 61', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(62, 'Horario - 62', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080),
(63, 'Horario - 63', 0, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);

-- --------------------------------------------------------

--
-- Table structure for table `funcion`
--

CREATE TABLE IF NOT EXISTS `funcion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idfuncion` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mobil` int(1) DEFAULT NULL COMMENT 'Para ejecutar en mobil',
  `idcategoria` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idfuncion` (`idfuncion`,`idcategoria`),
  KEY `fk_funcion_categoria4` (`idcategoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Almacena todas las funciones del sistema.' AUTO_INCREMENT=152 ;

--
-- Dumping data for table `funcion`
--

INSERT INTO `funcion` (`id`, `idfuncion`, `nombre`, `descripcion`, `mobil`, `idcategoria`) VALUES
(1, 'listar', 'Listar', NULL, NULL, 'ADMPLT'),
(2, 'agregar', 'Agregar', NULL, NULL, 'ADMPLT'),
(3, 'modificar', 'Modificar', NULL, NULL, 'ADMPLT'),
(4, 'eliminar', 'Eliminar', NULL, NULL, 'ADMPLT'),
(5, 'agregar', 'Agregar', NULL, NULL, 'ADMUSR'),
(6, 'modificar', 'Modificar Todos los Usuarios', NULL, NULL, 'ADMUSR'),
(7, 'eliminar', 'Eliminar', NULL, NULL, 'ADMUSR'),
(9, 'agregar', 'Agregar', NULL, NULL, 'CTRLCTRL'),
(10, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLCTRL'),
(11, 'listar', 'Listar', NULL, NULL, 'CTRLCTRL'),
(12, 'modificar', 'Modificar', NULL, NULL, 'CTRLCTRL'),
(13, 'estadoCnx', 'Estado de ConexiÃ³n', NULL, NULL, 'CTRLCTRL'),
(14, 'listar', 'Listar', NULL, NULL, 'CTRLDS'),
(15, 'modificar', 'Modificar', NULL, NULL, 'CTRLDS'),
(16, 'reset', 'Restablecer Valor', NULL, NULL, 'CTRLDS'),
(17, 'calendario', 'Utilizar Calendario', NULL, NULL, 'CTRLDS'),
(18, 'agregar', 'Agregar', NULL, NULL, 'CTRLGPR'),
(19, 'modificar', 'Modificar', NULL, NULL, 'CTRLGPR'),
(20, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLGPR'),
(21, 'listar', 'Listar', NULL, NULL, 'CTRLGPR'),
(22, 'listar', 'Listar', NULL, NULL, 'CTRLHRS'),
(23, 'modificar', 'Modificar', NULL, NULL, 'CTRLHRS'),
(24, 'agregar', 'Agregar', NULL, NULL, 'CTRLLEC'),
(25, 'modificar', 'Modificar', NULL, NULL, 'CTRLLEC'),
(26, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLLEC'),
(27, 'listar', 'Listar', NULL, NULL, 'CTRLLEC'),
(28, 'agregar', 'Agregar', NULL, NULL, 'CTRLPTS'),
(29, 'modificar', 'Modificar', NULL, NULL, 'CTRLPTS'),
(30, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLPTS'),
(31, 'listar', 'Listar', NULL, NULL, 'CTRLPTS'),
(32, 'agregar', 'Agregar', NULL, NULL, 'CTRLUSR'),
(33, 'modificar', 'Modificar', NULL, NULL, 'CTRLUSR'),
(34, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLUSR'),
(35, 'listar', 'Listar', NULL, NULL, 'CTRLUSR'),
(37, 'listar', 'Listar', NULL, NULL, 'VIDASCMNTR'),
(38, 'asociar', 'Asociar', NULL, NULL, 'VIDASCMNTR'),
(39, 'agregar', 'Agregar', NULL, NULL, 'VIDCAM'),
(40, 'modificar', 'Modificar', NULL, NULL, 'VIDCAM'),
(41, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAM'),
(42, 'listar', 'Listar', NULL, NULL, 'VIDCAM'),
(43, 'horarioAlr', 'Horario de Alarmas', NULL, NULL, 'VIDCAM'),
(44, 'presetPos', 'Posiciones Predefinidas', NULL, NULL, 'VIDCAM'),
(45, 'agregar', 'Agregar', NULL, NULL, 'VIDSRV'),
(46, 'modificar', 'Modificar', NULL, NULL, 'VIDSRV'),
(47, 'eliminar', 'Eliminar', NULL, NULL, 'VIDSRV'),
(48, 'listar', 'Listar', NULL, NULL, 'VIDSRV'),
(49, 'reiniciar', 'Reiniciar Grabador', NULL, NULL, 'VIDSRV'),
(50, 'detener', 'Detener Grabador', NULL, NULL, 'VIDSRV'),
(51, 'agregar', 'Agregar', NULL, NULL, 'VIDGRD'),
(52, 'modificar', 'Modificar', NULL, NULL, 'VIDGRD'),
(53, 'eliminar', 'Eliminar', NULL, NULL, 'VIDGRD'),
(54, 'listar', 'Listar', NULL, NULL, 'VIDGRD'),
(55, 'grdPos', 'Establecer Posiciones Predefinidas', NULL, NULL, 'VIDGRD'),
(56, 'agregar', 'Agregar', NULL, NULL, 'VIDCAMMRC'),
(57, 'modificar', 'Modificar', NULL, NULL, 'VIDCAMMRC'),
(58, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAMMRC'),
(59, 'listar', 'Listar', NULL, NULL, 'VIDCAMMRC'),
(60, 'agregar', 'Agregar', NULL, NULL, 'VIDCAMMOD'),
(61, 'modificar', 'Modificar', NULL, NULL, 'VIDCAMMOD'),
(62, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAMMOD'),
(63, 'listar', 'Listar', NULL, NULL, 'VIDCAMMOD'),
(64, 'verCamaras', 'Monitorear Camaras', NULL, NULL, 'VIDMNTR'),
(65, 'agregar', 'Agregar', NULL, NULL, 'VIDCAMTIP'),
(66, 'modificar', 'Modificar', NULL, NULL, 'VIDCAMTIP'),
(67, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAMTIP'),
(68, 'listar', 'Listar', NULL, NULL, 'VIDCAMTIP'),
(69, 'agregar', 'Agregar', NULL, NULL, 'VIDZN'),
(70, 'modificar', 'Modificar', NULL, NULL, 'VIDZN'),
(71, 'eliminar', 'Eliminar', NULL, NULL, 'VIDZN'),
(72, 'listar', 'Listar', NULL, NULL, 'VIDZN'),
(74, 'agregar', 'Agregar', NULL, NULL, 'ADMGPR'),
(75, 'modificar', 'Modificar', NULL, NULL, 'ADMGPR'),
(76, 'eliminar', 'Eliminar', NULL, NULL, 'ADMGPR'),
(77, 'listar', 'Listar', NULL, NULL, NULL),
(78, 'listar', 'Listar', NULL, NULL, 'ADMGPR'),
(79, 'visualizar', 'Visualizar Registro', NULL, NULL, 'ADMGPR'),
(80, 'visualizar', 'Visualizar Registro', NULL, NULL, 'ADMPLT'),
(83, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLCTRL'),
(84, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLDS'),
(85, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLGPR'),
(86, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLHRS'),
(87, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLLEC'),
(88, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLPTS'),
(89, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLUSR'),
(90, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAM'),
(91, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDSRV'),
(92, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDGRD'),
(93, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAMMRC'),
(94, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAMMOD'),
(95, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAMTIP'),
(96, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDZN'),
(97, 'segmentos', 'Asignar Segmentos', NULL, NULL, 'ADMUSR'),
(98, 'gprPermiso', 'Grupos de Permisos', NULL, NULL, 'ADMUSR'),
(99, 'agregar', 'Generar', NULL, NULL, 'VIDACCPC'),
(100, 'visualizar', 'Visualizar', NULL, NULL, 'VIDACCPC'),
(101, 'eliminar', 'Eliminar', NULL, NULL, 'VIDACCPC'),
(102, 'listar', 'Listar', NULL, NULL, 'VIDACCPC'),
(103, 'listar', 'Listar', NULL, NULL, 'VIDALARM'),
(104, 'abrir', 'Abrir Puertas', NULL, NULL, 'CTRLPTS'),
(105, 'cerrar', 'Cerrar Puertas', NULL, NULL, 'CTRLPTS'),
(106, 'listar', 'Listar', NULL, NULL, 'CTRLLOG'),
(107, 'agregar', 'Agregar', NULL, NULL, 'CARPLT'),
(108, 'modificar', 'Modificar', NULL, NULL, 'CARPLT'),
(109, 'eliminar', 'Eliminar', NULL, NULL, 'CARPLT'),
(110, 'listar', 'Listar', NULL, NULL, 'CARPLT'),
(111, 'agregar', 'Agregar', NULL, NULL, 'CARCRG'),
(112, 'agregar', 'Agregar', NULL, NULL, 'CARDPT'),
(113, 'modificar', 'Modificar', NULL, NULL, 'CARCRG'),
(114, 'modificar', 'Modificar', NULL, NULL, 'CARDPT'),
(115, 'eliminar', 'Eliminar', NULL, NULL, 'CARCRG'),
(116, 'eliminar', 'Eliminar', NULL, NULL, 'CARDPT'),
(117, 'listar', 'Listar', NULL, NULL, 'CARCRG'),
(118, 'listar', 'Listar', NULL, NULL, 'CARDPT'),
(119, 'agregar', 'Agregar', NULL, NULL, 'CAREMP'),
(120, 'modificar', 'Modificar', NULL, NULL, 'CAREMP'),
(121, 'listar', 'Listar', NULL, NULL, 'CAREMP'),
(122, 'eliminar', 'Eliminar', NULL, NULL, 'CAREMP'),
(123, 'cargarFoto', 'Cargar Foto', NULL, NULL, 'CAREMP'),
(124, 'imprimir', 'Imprimir', NULL, NULL, 'CAREMP'),
(125, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARCRG'),
(126, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CAREMP'),
(127, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARDPT'),
(128, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARPLT'),
(129, 'agregar', 'Agregar', NULL, NULL, 'CARVIS'),
(130, 'modificar', 'Modificar', NULL, NULL, 'CARVIS'),
(131, 'eliminar', 'Eliminar', NULL, NULL, 'CARVIS'),
(132, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARVIS'),
(133, 'listar', 'Listar', NULL, NULL, 'CARVIS'),
(134, 'imprimir', 'Imprimir Carnet', NULL, NULL, 'CARVIS'),
(135, 'agregar', 'Agregar', NULL, NULL, 'ADMACCPC'),
(136, 'modificar', 'Modificar', NULL, NULL, 'ADMACCPC'),
(137, 'eliminar', 'Eliminar', NULL, NULL, 'ADMACCPC'),
(138, 'visualizar', 'Visualizar', NULL, NULL, 'ADMACCPC'),
(139, 'listar', 'Listar', NULL, NULL, 'ADMACCPC'),
(140, 'listar', 'listar', NULL, NULL, 'CARLISTEMP'),
(141, 'agregar', 'Agregar', NULL, NULL, 'ADMSEG'),
(142, 'modificar', 'Modificar', NULL, NULL, 'ADMSEG'),
(143, 'eliminar', 'Eliminar', NULL, NULL, 'ADMSEG'),
(144, 'listar', 'Listar', NULL, NULL, 'ADMSEG'),
(145, 'visualizar', 'Visualizar', NULL, NULL, 'ADMSEG'),
(146, 'exportar', 'Exportar Camaras', 'Exportar Camaras para mobil', 1, 'VIDEXCAM'),
(147, 'syncDatos', 'Sincronizar a Controladora', 'Descargar los datos a la controladora', NULL, 'CTRLCTRL'),
(148, 'monitor', 'Monitor Acceso', NULL, NULL, 'CTRLMON'),
(149, 'listar', 'Listar', NULL, NULL, 'CTRLLOGA'),
(150, 'monitor', 'Monitor Asistencia', NULL, NULL, 'CTRLMONA'),
(151, 'descargar', 'Descargar CSV', NULL, NULL, 'CTRLLOGA');

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE IF NOT EXISTS `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grupo_has_funcion`
--

CREATE TABLE IF NOT EXISTS `grupo_has_funcion` (
  `grupo_idgrupo` int(11) NOT NULL,
  `funcion_id` int(11) NOT NULL,
  PRIMARY KEY (`grupo_idgrupo`,`funcion_id`),
  KEY `funcion_id` (`funcion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plantafisica`
--

CREATE TABLE IF NOT EXISTS `plantafisica` (
  `idplantafisica` int(11) NOT NULL AUTO_INCREMENT,
  `plantafisica` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `file` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idplantafisica`),
  UNIQUE KEY `file` (`file`),
  UNIQUE KEY `plantafisica` (`plantafisica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='imagen de una planta fisica' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `segmento`
--

CREATE TABLE IF NOT EXISTS `segmento` (
  `idsegmento` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo Segmento',
  `segmento` varchar(50) CHARACTER SET latin1 NOT NULL COMMENT 'Nombre',
  `descripcion` varchar(100) CHARACTER SET latin1 NOT NULL COMMENT 'Breve descripcion',
  PRIMARY KEY (`idsegmento`),
  UNIQUE KEY `segmento` (`segmento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Areas de la organizacion' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `segmento_usuario`
--

CREATE TABLE IF NOT EXISTS `segmento_usuario` (
  `idsegmento` int(11) NOT NULL COMMENT 'Codigo Segmento',
  `usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Login de usuario',
  UNIQUE KEY `idsegmento` (`idsegmento`,`usuario`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Asociacion zonas - usuarios';

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE IF NOT EXISTS `timezone` (
  `idtimezone` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(100) NOT NULL COMMENT 'Localidad',
  `standard_time` varchar(10) NOT NULL COMMENT 'Horario normal',
  `summer_time` varchar(10) NOT NULL COMMENT 'Horario verano',
  PRIMARY KEY (`idtimezone`),
  UNIQUE KEY `location` (`location`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=406 ;

--
-- Dumping data for table `timezone`
--

INSERT INTO `timezone` (`idtimezone`, `location`, `standard_time`, `summer_time`) VALUES
(1, 'Africa/Abidjan', 'UTC+00', '-'),
(2, 'Africa/Accra', 'UTC+00', '-'),
(3, 'Africa/Addis_Ababa', 'UTC+03', '-'),
(4, 'Africa/Algiers', 'UTC+01', '-'),
(5, 'Africa/Asmara', 'UTC+03', '-'),
(6, 'Africa/Bamako', 'UTC+00', '-'),
(7, 'Africa/Bangui', 'UTC+01', '-'),
(8, 'Africa/Banjul', 'UTC+00', '-'),
(9, 'Africa/Bissau', 'UTC+00', '-'),
(10, 'Africa/Blantyre', 'UTC+02', '-'),
(11, 'Africa/Brazzaville', 'UTC+01', '-'),
(12, 'Africa/Bujumbura', 'UTC+02', '-'),
(13, 'Africa/Cairo', 'UTC+02', 'UTC+03'),
(14, 'Africa/Casablanca', 'UTC+00', '-'),
(15, 'Africa/Ceuta', 'UTC+01', 'UTC+02'),
(16, 'Africa/Conakry', 'UTC+00', '-'),
(17, 'Africa/Dakar', 'UTC+00', '-'),
(18, 'Africa/Dar_es_Salaam', 'UTC+03', '-'),
(19, 'Africa/Djibouti', 'UTC+03', '-'),
(20, 'Africa/Douala', 'UTC+01', '-'),
(21, 'Africa/El_Aaiun', 'UTC+00', '-'),
(22, 'Africa/Freetown', 'UTC+00', '-'),
(23, 'Africa/Gaborone', 'UTC+02', '-'),
(24, 'Africa/Harare', 'UTC+02', '-'),
(25, 'Africa/Johannesburg', 'UTC+02', '-'),
(26, 'Africa/Kampala', 'UTC+03', '-'),
(27, 'Africa/Khartoum', 'UTC+03', '-'),
(28, 'Africa/Kigali', 'UTC+02', '-'),
(29, 'Africa/Kinshasa', 'UTC+01', '-'),
(30, 'Africa/Lagos', 'UTC+01', '-'),
(31, 'Africa/Libreville', 'UTC+01', '-'),
(32, 'Africa/Lome', 'UTC+00', '-'),
(33, 'Africa/Luanda', 'UTC+01', '-'),
(34, 'Africa/Lubumbashi', 'UTC+02', '-'),
(35, 'Africa/Lusaka', 'UTC+02', '-'),
(36, 'Africa/Malabo', 'UTC+01', '-'),
(37, 'Africa/Maputo', 'UTC+02', '-'),
(38, 'Africa/Maseru', 'UTC+02', '-'),
(39, 'Africa/Mbabane', 'UTC+02', '-'),
(40, 'Africa/Mogadishu', 'UTC+03', '-'),
(41, 'Africa/Monrovia', 'UTC+00', '-'),
(42, 'Africa/Nairobi', 'UTC+03', '-'),
(43, 'Africa/Ndjamena', 'UTC+01', '-'),
(44, 'Africa/Niamey', 'UTC+01', '-'),
(45, 'Africa/Nouakchott', 'UTC+00', '-'),
(46, 'Africa/Ouagadougou', 'UTC+00', '-'),
(47, 'Africa/Porto-Novo', 'UTC+01', '-'),
(48, 'Africa/Sao_Tome', 'UTC+00', '-'),
(49, 'Africa/Tripoli', 'UTC+02', '-'),
(50, 'Africa/Tunis', 'UTC+01', 'UTC+02'),
(51, 'Africa/Windhoek', 'UTC+01', 'UTC+02'),
(52, 'America/Adak', 'UTC-10', 'UTC-09'),
(53, 'America/Anchorage', 'UTC-09', 'UTC-08'),
(54, 'America/Anguilla', 'UTC-04', '-'),
(55, 'America/Antigua', 'UTC-04', '-'),
(56, 'America/Araguaina', 'UTC-03', '-'),
(57, 'America/Argentina/Buenos_Aires', 'UTC-03', 'UTC-02'),
(58, 'America/Argentina/Catamarca', 'UTC-03', '-'),
(59, 'America/Argentina/Cordoba', 'UTC-03', 'UTC-02'),
(60, 'America/Argentina/Jujuy', 'UTC-03', '-'),
(61, 'America/Argentina/La_Rioja', 'UTC-03', '-'),
(62, 'America/Argentina/Mendoza', 'UTC-03', '-'),
(63, 'America/Argentina/Rio_Gallegos', 'UTC-03', '-'),
(64, 'America/Argentina/Salta', 'UTC-03', '-'),
(65, 'America/Argentina/San_Juan', 'UTC-03', '-'),
(66, 'America/Argentina/San_Luis', 'UTC-04', 'UTC-03'),
(67, 'America/Argentina/Tucuman', 'UTC-03', 'UTC-02'),
(68, 'America/Argentina/Ushuaia', 'UTC-03', '-'),
(69, 'America/Aruba', 'UTC-04', '-'),
(70, 'America/Asuncion', 'UTC-04', 'UTC-03'),
(71, 'America/Atikokan', 'UTC-05', '-'),
(72, 'America/Bahia', 'UTC-03', '-'),
(73, 'America/Barbados', 'UTC-04', '-'),
(74, 'America/Belem', 'UTC-03', '-'),
(75, 'America/Belize', 'UTC-06', '-'),
(76, 'America/Blanc-Sablon', 'UTC-04', '-'),
(77, 'America/Boa_Vista', 'UTC-04', '-'),
(78, 'America/Bogota', 'UTC-05', '-'),
(79, 'America/Boise', 'UTC-07', 'UTC-06'),
(80, 'America/Cambridge_Bay', 'UTC-07', 'UTC-06'),
(81, 'America/Campo_Grande', 'UTC-04', 'UTC-03'),
(82, 'America/Cancun', 'UTC-06', 'UTC-05'),
(83, 'America/Caracas', 'UTC-04:30', '-'),
(84, 'America/Cayenne', 'UTC-03', '-'),
(85, 'America/Cayman', 'UTC-05', '-'),
(86, 'America/Chicago', 'UTC-06', 'UTC-05'),
(87, 'America/Chihuahua', 'UTC-07', 'UTC-06'),
(88, 'America/Costa_Rica', 'UTC-06', '-'),
(89, 'America/Cuiaba', 'UTC-04', 'UTC-03'),
(90, 'America/Curacao', 'UTC-04', '-'),
(91, 'America/Danmarkshavn', 'UTC+00', '-'),
(92, 'America/Dawson', 'UTC-08', 'UTC-07'),
(93, 'America/Dawson_Creek', 'UTC-07', '-'),
(94, 'America/Denver', 'UTC-07', 'UTC-06'),
(95, 'America/Detroit', 'UTC-05', 'UTC-04'),
(96, 'America/Dominica', 'UTC-04', '-'),
(97, 'America/Edmonton', 'UTC-07', 'UTC-06'),
(98, 'America/Eirunepe', 'UTC-04', '-'),
(99, 'America/El_Salvador', 'UTC-06', '-'),
(100, 'America/Fortaleza', 'UTC-03', '-'),
(101, 'America/Glace_Bay', 'UTC-04', 'UTC-03'),
(102, 'America/Godthab', 'UTC-03', 'UTC-02'),
(103, 'America/Goose_Bay', 'UTC-04', 'UTC-03'),
(104, 'America/Grand_Turk', 'UTC-05', 'UTC-04'),
(105, 'America/Grenada', 'UTC-04', '-'),
(106, 'America/Guadeloupe', 'UTC-04', '-'),
(107, 'America/Guatemala', 'UTC-06', '-'),
(108, 'America/Guayaquil', 'UTC-05', '-'),
(109, 'America/Guyana', 'UTC-04', '-'),
(110, 'America/Halifax', 'UTC-04', 'UTC-03'),
(111, 'America/Havana', 'UTC-05', 'UTC-04'),
(112, 'America/Hermosillo', 'UTC-07', '-'),
(113, 'America/Indiana/Indianapolis', 'UTC-05', 'UTC-04'),
(114, 'America/Indiana/Knox', 'UTC-06', 'UTC-05'),
(115, 'America/Indiana/Marengo', 'UTC-05', 'UTC-04'),
(116, 'America/Indiana/Petersburg', 'UTC-05', 'UTC-04'),
(117, 'America/Indiana/Tell_City', 'UTC-06', 'UTC-05'),
(118, 'America/Indiana/Vevay', 'UTC-05', 'UTC-04'),
(119, 'America/Indiana/Vincennes', 'UTC-05', 'UTC-04'),
(120, 'America/Indiana/Winamac', 'UTC-05', 'UTC-04'),
(121, 'America/Inuvik', 'UTC-07', 'UTC-06'),
(122, 'America/Iqaluit', 'UTC-05', 'UTC-04'),
(123, 'America/Jamaica', 'UTC-05', '-'),
(124, 'America/Juneau', 'UTC-09', 'UTC-08'),
(125, 'America/Kentucky/Louisville', 'UTC-05', 'UTC-04'),
(126, 'America/Kentucky/Monticello', 'UTC-05', 'UTC-04'),
(127, 'America/La_Paz', 'UTC-04', '-'),
(128, 'America/Lima', 'UTC-05', '-'),
(129, 'America/Los_Angeles', 'UTC-08', 'UTC-07'),
(130, 'America/Maceio', 'UTC-03', '-'),
(131, 'America/Managua', 'UTC-06', '-'),
(132, 'America/Manaus', 'UTC-04', '-'),
(133, 'America/Marigot', 'UTC-04', '-'),
(134, 'America/Martinique', 'UTC-04', '-'),
(135, 'America/Matamoros', 'UTC-06', 'UTC-05'),
(136, 'America/Mazatlan', 'UTC-07', 'UTC-06'),
(137, 'America/Menominee', 'UTC-06', 'UTC-05'),
(138, 'America/Merida', 'UTC-06', 'UTC-05'),
(139, 'America/Mexico_City', 'UTC-06', 'UTC-05'),
(140, 'America/Miquelon', 'UTC-03', 'UTC-02'),
(141, 'America/Moncton', 'UTC-04', 'UTC-03'),
(142, 'America/Monterrey', 'UTC-06', 'UTC-05'),
(143, 'America/Montevideo', 'UTC-03', 'UTC-02'),
(144, 'America/Montreal', 'UTC-05', 'UTC-04'),
(145, 'America/Montserrat', 'UTC-04', '-'),
(146, 'America/Nassau', 'UTC-05', 'UTC-04'),
(147, 'America/New_York', 'UTC-05', 'UTC-04'),
(148, 'America/Nipigon', 'UTC-05', 'UTC-04'),
(149, 'America/Nome', 'UTC-09', 'UTC-08'),
(150, 'America/Noronha', 'UTC-02', '-'),
(151, 'America/North_Dakota/Center', 'UTC-06', 'UTC-05'),
(152, 'America/North_Dakota/New_Salem', 'UTC-06', 'UTC-05'),
(153, 'America/Ojinaga', 'UTC-07', 'UTC-06'),
(154, 'America/Panama', 'UTC-05', '-'),
(155, 'America/Pangnirtung', 'UTC-05', 'UTC-04'),
(156, 'America/Paramaribo', 'UTC-03', '-'),
(157, 'America/Phoenix', 'UTC-07', '-'),
(158, 'America/Port_of_Spain', 'UTC-04', '-'),
(159, 'America/Port-au-Prince', 'UTC-05', '-'),
(160, 'America/Porto_Velho', 'UTC-04', '-'),
(161, 'America/Puerto_Rico', 'UTC-04', '-'),
(162, 'America/Rainy_River', 'UTC-06', 'UTC-05'),
(163, 'America/Rankin_Inlet', 'UTC-06', 'UTC-05'),
(164, 'America/Recife', 'UTC-03', '-'),
(165, 'America/Regina', 'UTC-06', '-'),
(166, 'America/Resolute', 'UTC-05', 'UTC-04'),
(167, 'America/Rio_Branco', 'UTC-04', '-'),
(168, 'America/Santa_Isabel', 'UTC-08', 'UTC-07'),
(169, 'America/Santarem', 'UTC-03', '-'),
(170, 'America/Santiago', 'UTC-04', 'UTC-03'),
(171, 'America/Santo_Domingo', 'UTC-04', '-'),
(172, 'America/Sao_Paulo', 'UTC-03', 'UTC-02'),
(173, 'America/Scoresbysund', 'UTC-01', 'UTC+00'),
(174, 'America/Shiprock', 'UTC-07', 'UTC-06'),
(175, 'America/St_Barthelemy', 'UTC-04', '-'),
(176, 'America/St_Johns', 'UTC-03:30', 'UTC-02:30'),
(177, 'America/St_Kitts', 'UTC-04', '-'),
(178, 'America/St_Lucia', 'UTC-04', '-'),
(179, 'America/St_Thomas', 'UTC-04', '-'),
(180, 'America/St_Vincent', 'UTC-04', '-'),
(181, 'America/Swift_Current', 'UTC-06', '-'),
(182, 'America/Tegucigalpa', 'UTC-06', '-'),
(183, 'America/Thule', 'UTC-04', 'UTC-03'),
(184, 'America/Thunder_Bay', 'UTC-05', 'UTC-04'),
(185, 'America/Tijuana', 'UTC-08', 'UTC-07'),
(186, 'America/Toronto', 'UTC-05', 'UTC-04'),
(187, 'America/Tortola', 'UTC-04', '-'),
(188, 'America/Vancouver', 'UTC-08', 'UTC-07'),
(189, 'America/Whitehorse', 'UTC-08', 'UTC-07'),
(190, 'America/Winnipeg', 'UTC-06', 'UTC-05'),
(191, 'America/Yakutat', 'UTC-09', 'UTC-08'),
(192, 'America/Yellowknife', 'UTC-07', 'UTC-06'),
(193, 'Antarctica/Casey', 'UTC+08', '-'),
(194, 'Antarctica/Davis', 'UTC+07', '-'),
(195, 'Antarctica/DumontDUrville', 'UTC+10', '-'),
(196, 'Antarctica/Mawson', 'UTC+06', '-'),
(197, 'Antarctica/McMurdo', 'UTC+12', 'UTC+13'),
(198, 'Antarctica/Palmer', 'UTC-04', 'UTC-03'),
(199, 'Antarctica/Rothera', 'UTC-03', '-'),
(200, 'Antarctica/South_Pole', 'UTC+12', 'UTC+13'),
(201, 'Antarctica/Syowa', 'UTC+03', '-'),
(202, 'Antarctica/Vostok', 'UTC+00', '-'),
(203, 'Arctic/Longyearbyen', 'UTC+01', 'UTC+02'),
(204, 'Asia/Aden', 'UTC+03', '-'),
(205, 'Asia/Almaty', 'UTC+06', '-'),
(206, 'Asia/Amman', 'UTC+02', 'UTC+03'),
(207, 'Asia/Anadyr', 'UTC+11', 'UTC+12'),
(208, 'Asia/Aqtau', 'UTC+05', '-'),
(209, 'Asia/Aqtobe', 'UTC+05', '-'),
(210, 'Asia/Ashgabat', 'UTC+05', '-'),
(211, 'Asia/Baghdad', 'UTC+03', '-'),
(212, 'Asia/Bahrain', 'UTC+03', '-'),
(213, 'Asia/Baku', 'UTC+04', 'UTC+05'),
(214, 'Asia/Bangkok', 'UTC+07', '-'),
(215, 'Asia/Beirut', 'UTC+02', 'UTC+03'),
(216, 'Asia/Bishkek', 'UTC+06', '-'),
(217, 'Asia/Brunei', 'UTC+08', '-'),
(218, 'Asia/Choibalsan', 'UTC+08', '-'),
(219, 'Asia/Chongqing', 'UTC+08', '-'),
(220, 'Asia/Colombo', 'UTC+05:30', '-'),
(221, 'Asia/Damascus', 'UTC+02', 'UTC+03'),
(222, 'Asia/Dhaka', 'UTC+06', '-'),
(223, 'Asia/Dili', 'UTC+09', '-'),
(224, 'Asia/Dubai', 'UTC+04', '-'),
(225, 'Asia/Dushanbe', 'UTC+05', '-'),
(226, 'Asia/Gaza', 'UTC+02', 'UTC+03'),
(227, 'Asia/Harbin', 'UTC+08', '-'),
(228, 'Asia/Ho_Chi_Minh', 'UTC+07', '-'),
(229, 'Asia/Hong_Kong', 'UTC+08', '-'),
(230, 'Asia/Hovd', 'UTC+07', '-'),
(231, 'Asia/Irkutsk', 'UTC+08', 'UTC+09'),
(232, 'Asia/Jakarta', 'UTC+07', '-'),
(233, 'Asia/Jayapura', 'UTC+09', '-'),
(234, 'Asia/Jerusalem', 'UTC+02', 'UTC+03'),
(235, 'Asia/Kabul', 'UTC+04:30', '-'),
(236, 'Asia/Kamchatka', 'UTC+11', 'UTC+12'),
(237, 'Asia/Karachi', 'UTC+06', '-'),
(238, 'Asia/Kashgar', 'UTC+08', '-'),
(239, 'Asia/Kathmandu', 'UTC+05:45', '-'),
(240, 'Asia/Kolkata', 'UTC+05:30', '-'),
(241, 'Asia/Krasnoyarsk', 'UTC+07', 'UTC+08'),
(242, 'Asia/Kuala_Lumpur', 'UTC+08', '-'),
(243, 'Asia/Kuching', 'UTC+08', '-'),
(244, 'Asia/Kuwait', 'UTC+03', '-'),
(245, 'Asia/Macau', 'UTC+08', '-'),
(246, 'Asia/Magadan', 'UTC+11', 'UTC+12'),
(247, 'Asia/Makassar', 'UTC+08', '-'),
(248, 'Asia/Manila', 'UTC+08', '-'),
(249, 'Asia/Muscat', 'UTC+04', '-'),
(250, 'Asia/Nicosia', 'UTC+02', 'UTC+03'),
(251, 'Asia/Novokuznetsk', 'UTC+06', 'UTC+07'),
(252, 'Asia/Novosibirsk', 'UTC+06', 'UTC+07'),
(253, 'Asia/Omsk', 'UTC+06', 'UTC+07'),
(254, 'Asia/Oral', 'UTC+05', '-'),
(255, 'Asia/Phnom_Penh', 'UTC+07', '-'),
(256, 'Asia/Pontianak', 'UTC+07', '-'),
(257, 'Asia/Pyongyang', 'UTC+09', '-'),
(258, 'Asia/Qatar', 'UTC+03', '-'),
(259, 'Asia/Qyzylorda', 'UTC+06', '-'),
(260, 'Asia/Rangoon', 'UTC+06:30', '-'),
(261, 'Asia/Riyadh', 'UTC+03', '-'),
(262, 'Asia/Sakhalin', 'UTC+10', 'UTC+11'),
(263, 'Asia/Samarkand', 'UTC+05', '-'),
(264, 'Asia/Seoul', 'UTC+09', '-'),
(265, 'Asia/Shanghai', 'UTC+08', '-'),
(266, 'Asia/Singapore', 'UTC+08', '-'),
(267, 'Asia/Taipei', 'UTC+08', '-'),
(268, 'Asia/Tashkent', 'UTC+05', '-'),
(269, 'Asia/Tbilisi', 'UTC+04', '-'),
(270, 'Asia/Tehran', 'UTC+03:30', 'UTC+04:30'),
(271, 'Asia/Thimphu', 'UTC+06', '-'),
(272, 'Asia/Tokyo', 'UTC+09', '-'),
(273, 'Asia/Ulaanbaatar', 'UTC+08', '-'),
(274, 'Asia/Urumqi', 'UTC+08', '-'),
(275, 'Asia/Vientiane', 'UTC+07', '-'),
(276, 'Asia/Vladivostok', 'UTC+10', 'UTC+11'),
(277, 'Asia/Yakutsk', 'UTC+09', 'UTC+10'),
(278, 'Asia/Yekaterinburg', 'UTC+05', 'UTC+06'),
(279, 'Asia/Yerevan', 'UTC+04', 'UTC+05'),
(280, 'Atlantic/Azores', 'UTC-01', 'UTC+00'),
(281, 'Atlantic/Bermuda', 'UTC-04', 'UTC-03'),
(282, 'Atlantic/Canary', 'UTC+00', 'UTC+01'),
(283, 'Atlantic/Cape_Verde', 'UTC-01', '-'),
(284, 'Atlantic/Faroe', 'UTC+00', 'UTC+01'),
(285, 'Atlantic/Madeira', 'UTC+00', 'UTC+01'),
(286, 'Atlantic/Reykjavik', 'UTC+00', '-'),
(287, 'Atlantic/South_Georgia', 'UTC-02', '-'),
(288, 'Atlantic/St_Helena', 'UTC+00', '-'),
(289, 'Atlantic/Stanley', 'UTC-04', 'UTC-03'),
(290, 'Australia/Adelaide', 'UTC+09:30', 'UTC+10:30'),
(291, 'Australia/Brisbane', 'UTC+10', '-'),
(292, 'Australia/Broken_Hill', 'UTC+09:30', 'UTC+10:30'),
(293, 'Australia/Currie', 'UTC+10', 'UTC+11'),
(294, 'Australia/Darwin', 'UTC+09:30', '-'),
(295, 'Australia/Eucla', 'UTC+08:45', 'UTC+09:45'),
(296, 'Australia/Hobart', 'UTC+10', 'UTC+11'),
(297, 'Australia/Lindeman', 'UTC+10', '-'),
(298, 'Australia/Lord_Howe', 'UTC+10:30', 'UTC+11'),
(299, 'Australia/Melbourne', 'UTC+10', 'UTC+11'),
(300, 'Australia/Perth', 'UTC+08', '-'),
(301, 'Australia/Sydney', 'UTC+10', 'UTC+11'),
(302, 'Europe/Amsterdam', 'UTC+01', 'UTC+02'),
(303, 'Europe/Andorra', 'UTC+01', 'UTC+02'),
(304, 'Europe/Athens', 'UTC+02', 'UTC+03'),
(305, 'Europe/Belgrade', 'UTC+01', 'UTC+02'),
(306, 'Europe/Berlin', 'UTC+01', 'UTC+02'),
(307, 'Europe/Bratislava', 'UTC+01', 'UTC+02'),
(308, 'Europe/Brussels', 'UTC+01', 'UTC+02'),
(309, 'Europe/Bucharest', 'UTC+02', 'UTC+03'),
(310, 'Europe/Budapest', 'UTC+01', 'UTC+02'),
(311, 'Europe/Chisinau', 'UTC+02', 'UTC+03'),
(312, 'Europe/Copenhagen', 'UTC+01', 'UTC+02'),
(313, 'Europe/Dublin', 'UTC+00', 'UTC+01'),
(314, 'Europe/Gibraltar', 'UTC+01', 'UTC+02'),
(315, 'Europe/Guernsey', 'UTC+00', 'UTC+01'),
(316, 'Europe/Helsinki', 'UTC+02', 'UTC+03'),
(317, 'Europe/Isle_of_Man', 'UTC+00', 'UTC+01'),
(318, 'Europe/Istanbul', 'UTC+02', 'UTC+03'),
(319, 'Europe/Jersey', 'UTC+00', 'UTC+01'),
(320, 'Europe/Kaliningrad', 'UTC+02', 'UTC+03'),
(321, 'Europe/Kiev', 'UTC+02', 'UTC+03'),
(322, 'Europe/Lisbon', 'UTC+00', 'UTC+01'),
(323, 'Europe/Ljubljana', 'UTC+01', 'UTC+02'),
(324, 'Europe/London', 'UTC+00', 'UTC+01'),
(325, 'Europe/Luxembourg', 'UTC+01', 'UTC+02'),
(326, 'Europe/Madrid', 'UTC+01', 'UTC+02'),
(327, 'Europe/Malta', 'UTC+01', 'UTC+02'),
(328, 'Europe/Mariehamn', 'UTC+02', 'UTC+03'),
(329, 'Europe/Minsk', 'UTC+02', 'UTC+03'),
(330, 'Europe/Monaco', 'UTC+01', 'UTC+02'),
(331, 'Europe/Moscow', 'UTC+03', 'UTC+04'),
(332, 'Europe/Oslo', 'UTC+01', 'UTC+02'),
(333, 'Europe/Paris', 'UTC+01', 'UTC+02'),
(334, 'Europe/Podgorica', 'UTC+01', 'UTC+02'),
(335, 'Europe/Prague', 'UTC+01', 'UTC+02'),
(336, 'Europe/Riga', 'UTC+02', 'UTC+03'),
(337, 'Europe/Rome', 'UTC+01', 'UTC+02'),
(338, 'Europe/Samara', 'UTC+03', 'UTC+04'),
(339, 'Europe/San_Marino', 'UTC+01', 'UTC+02'),
(340, 'Europe/Sarajevo', 'UTC+01', 'UTC+02'),
(341, 'Europe/Simferopol', 'UTC+02', 'UTC+03'),
(342, 'Europe/Skopje', 'UTC+01', 'UTC+02'),
(343, 'Europe/Sofia', 'UTC+02', 'UTC+03'),
(344, 'Europe/Stockholm', 'UTC+01', 'UTC+02'),
(345, 'Europe/Tallinn', 'UTC+02', 'UTC+03'),
(346, 'Europe/Tirane', 'UTC+01', 'UTC+02'),
(347, 'Europe/Uzhgorod', 'UTC+02', 'UTC+03'),
(348, 'Europe/Vaduz', 'UTC+01', 'UTC+02'),
(349, 'Europe/Vatican', 'UTC+01', 'UTC+02'),
(350, 'Europe/Vienna', 'UTC+01', 'UTC+02'),
(351, 'Europe/Vilnius', 'UTC+02', 'UTC+03'),
(352, 'Europe/Volgograd', 'UTC+03', 'UTC+04'),
(353, 'Europe/Warsaw', 'UTC+01', 'UTC+02'),
(354, 'Europe/Zagreb', 'UTC+01', 'UTC+02'),
(355, 'Europe/Zaporozhye', 'UTC+02', 'UTC+03'),
(356, 'Europe/Zurich', 'UTC+01', 'UTC+02'),
(357, 'Indian/Antananarivo', 'UTC+03', '-'),
(358, 'Indian/Chagos', 'UTC+06', '-'),
(359, 'Indian/Christmas', 'UTC+07', '-'),
(360, 'Indian/Cocos', 'UTC+06:30', '-'),
(361, 'Indian/Comoro', 'UTC+03', '-'),
(362, 'Indian/Kerguelen', 'UTC+05', '-'),
(363, 'Indian/Mahe', 'UTC+04', '-'),
(364, 'Indian/Maldives', 'UTC+05', '-'),
(365, 'Indian/Mauritius', 'UTC+04', '-'),
(366, 'Indian/Mayotte', 'UTC+03', '-'),
(367, 'Indian/Reunion', 'UTC+04', '-'),
(368, 'Pacific/Apia', 'UTC-11', 'UTC-10'),
(369, 'Pacific/Auckland', 'UTC+12', 'UTC+13'),
(370, 'Pacific/Chatham', 'UTC+12:45', 'UTC+13:45'),
(371, 'Pacific/Easter', 'UTC-06', 'UTC-05'),
(372, 'Pacific/Efate', 'UTC+11', '-'),
(373, 'Pacific/Enderbury', 'UTC+13', '-'),
(374, 'Pacific/Fakaofo', 'UTC-10', '-'),
(375, 'Pacific/Fiji', 'UTC+12', 'UTC+13'),
(376, 'Pacific/Funafuti', 'UTC+12', '-'),
(377, 'Pacific/Galapagos', 'UTC-06', '-'),
(378, 'Pacific/Gambier', 'UTC-09', '-'),
(379, 'Pacific/Guadalcanal', 'UTC+11', '-'),
(380, 'Pacific/Guam', 'UTC+10', '-'),
(381, 'Pacific/Honolulu', 'UTC-10', '-'),
(382, 'Pacific/Johnston', 'UTC-10', '-'),
(383, 'Pacific/Kiritimati', 'UTC+14', '-'),
(384, 'Pacific/Kosrae', 'UTC+11', '-'),
(385, 'Pacific/Kwajalein', 'UTC+12', '-'),
(386, 'Pacific/Majuro', 'UTC+12', '-'),
(387, 'Pacific/Marquesas', 'UTC-09:30', '-'),
(388, 'Pacific/Midway', 'UTC-11', '-'),
(389, 'Pacific/Nauru', 'UTC+12', '-'),
(390, 'Pacific/Niue', 'UTC-11', '-'),
(391, 'Pacific/Norfolk', 'UTC+11:30', '-'),
(392, 'Pacific/Noumea', 'UTC+11', '-'),
(393, 'Pacific/Pago_Pago', 'UTC-11', '-'),
(394, 'Pacific/Palau', 'UTC+09', '-'),
(395, 'Pacific/Pitcairn', 'UTC-08', '-'),
(396, 'Pacific/Ponape', 'UTC+11', '-'),
(397, 'Pacific/Port_Moresby', 'UTC+10', '-'),
(398, 'Pacific/Rarotonga', 'UTC-10', '-'),
(399, 'Pacific/Saipan', 'UTC+10', '-'),
(400, 'Pacific/Tahiti', 'UTC-10', '-'),
(401, 'Pacific/Tarawa', 'UTC+12', '-'),
(402, 'Pacific/Tongatapu', 'UTC+13', '-'),
(403, 'Pacific/Truk', 'UTC+10', '-'),
(404, 'Pacific/Wake', 'UTC+12', '-'),
(405, 'Pacific/Wallis', 'UTC+12', '-');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `fcreado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fmodificado` datetime DEFAULT NULL,
  `nota` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `superUsuario` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'SI el usuario es Supero Usuario, solo debe haber 1',
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`usuario`, `clave`, `nombre`, `apellido`, `activo`, `fcreado`, `fmodificado`, `nota`, `superUsuario`) VALUES
('admin', 'e2b43749d602142dd089f31a6bc03ab4', 'Admin', 'Admin', 1, '2011-10-10 20:34:27', '2012-05-22 08:57:05', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `usuario_has_grupo`
--

CREATE TABLE IF NOT EXISTS `usuario_has_grupo` (
  `usuario_usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `grupo_idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`usuario_usuario`,`grupo_idgrupo`),
  KEY `fk_usuario_has_grupo_usuario1` (`usuario_usuario`),
  KEY `fk_usuario_has_grupo_grupo1` (`grupo_idgrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vid_camara`
--

CREATE TABLE IF NOT EXISTS `vid_camara` (
  `idcamara` int(11) NOT NULL AUTO_INCREMENT,
  `idservidor` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Identificador Servidor Video',
  `idtipo_camara` int(11) NOT NULL COMMENT 'Codigo del tipo de camara',
  `camara` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre de la camara',
  `numero` int(2) NOT NULL COMMENT 'Numero de camara',
  `prioridad` tinyint(1) NOT NULL COMMENT 'Prioridad de monitoreo',
  `ipv4` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Direccion IP',
  `puerto` int(6) NOT NULL DEFAULT '80' COMMENT 'Puerto TCP',
  `cam_usuario` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Usuario de la camara',
  `cam_clave` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Clave de la camara',
  `cam_usuario_video` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Usuario para ver el video',
  `cam_clave_video` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Clave para ver el video',
  `pais` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Datos Ubicacion',
  `estado` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Datos Ubicacion',
  `ciudad` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Datos Ubicacion',
  `avenida` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Datos Ubicacion',
  `edificio` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Datos Ubicacion',
  `piso` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Datos Ubicacion',
  `oficina` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Datos Ubicacion',
  `idplantafisica` int(11) DEFAULT NULL COMMENT 'Codigo de planta fisica',
  `leftX` int(11) DEFAULT NULL COMMENT 'Coordenada X',
  `topY` int(11) DEFAULT NULL COMMENT 'Coordenada Y',
  `rotacion` int(3) NOT NULL DEFAULT '0' COMMENT 'Grados de rotacion',
  `ffmpeg_timelapse` int(11) NOT NULL DEFAULT '0' COMMENT 'Cideo usando FFMPEG para videos por lapsos',
  `ffmpeg_timelapse_mode` enum('hourly','daily','weekly-sunday','weekly-monday','monthly','manual') CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'hourly' COMMENT 'Frecuencia de generación de archivos de video de lapsos de tiempo',
  PRIMARY KEY (`idcamara`),
  UNIQUE KEY `unk_num_vc` (`idservidor`,`numero`),
  UNIQUE KEY `unk_ip_vc` (`ipv4`,`idservidor`),
  KEY `ind_idserviodr_vc` (`idservidor`),
  KEY `idplantafisica` (`idplantafisica`),
  KEY `idtipo_camara` (`idtipo_camara`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vid_camara_axisguard`
--

CREATE TABLE IF NOT EXISTS `vid_camara_axisguard` (
  `idcamaraaxisguard` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo primario',
  `idcamara` int(11) NOT NULL COMMENT 'Codigo de la camara',
  `guardnbr` int(2) NOT NULL COMMENT 'Numero de guardia establecido por la camara',
  `guardname` varchar(100) NOT NULL COMMENT 'Nombre de la guardia',
  `running` enum('no','yes') NOT NULL DEFAULT 'no' COMMENT 'Ejecucion de la guardia',
  `camnbr` int(1) NOT NULL DEFAULT '1' COMMENT 'Numero de la camara establecido por ella misma',
  `randomenabled` enum('no','yes') NOT NULL DEFAULT 'no' COMMENT 'Ejecucion de guardia al azar',
  `timebetweensequences` int(2) NOT NULL DEFAULT '0' COMMENT 'Tiempo de espera para ejecucion entre guardias',
  PRIMARY KEY (`idcamaraaxisguard`),
  KEY `fk_vid_camara_axisguard_1` (`idcamara`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Guardia de una camara AXIS' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vid_camara_axisguard_tour`
--

CREATE TABLE IF NOT EXISTS `vid_camara_axisguard_tour` (
  `idvidcamaraaxisguardtour` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo primario',
  `idcamaraaxisguard` int(11) NOT NULL COMMENT 'Codigo de la guardia',
  `idcamaraaxispresetpos` int(11) NOT NULL COMMENT 'Codigo de la posicion',
  `tournbr` int(2) NOT NULL COMMENT 'Numero de tour',
  `position` int(2) NOT NULL COMMENT 'Numero de secuencia de ejecucion',
  `movespeed` int(2) NOT NULL DEFAULT '5' COMMENT 'Velocidad del recorrido ',
  `waittime` int(2) NOT NULL DEFAULT '1' COMMENT 'tiempo de espera entre ejecucion de recorridos',
  `WaitTimeViewType` enum('seconds','minutes') CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'seconds' COMMENT 'tipo de tiempo de espera entre recorridos',
  PRIMARY KEY (`idvidcamaraaxisguardtour`),
  UNIQUE KEY `idcamaraaxisguard` (`idcamaraaxisguard`,`position`),
  KEY `fk_vid_camara_axisguard_tour_1` (`idcamaraaxisguard`),
  KEY `fk_vid_camara_axisguard_tour_2` (`idcamaraaxispresetpos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Recorrido de una guardia de una camara AXIS' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vid_camara_axispresetpos`
--

CREATE TABLE IF NOT EXISTS `vid_camara_axispresetpos` (
  `idcamaraaxispresetpos` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo primario',
  `idcamara` int(11) NOT NULL COMMENT 'Codigo de la camara',
  `presetnbr` int(2) NOT NULL COMMENT 'Numero de la posicion establecido por la camara',
  `presetname` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idcamaraaxispresetpos`),
  UNIQUE KEY `presetname` (`presetname`,`idcamara`),
  KEY `fk_vid_camara_axispresetpos_1` (`idcamara`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Posiciones predefinidas de una camara AXIS' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vid_camara_horario`
--

CREATE TABLE IF NOT EXISTS `vid_camara_horario` (
  `idcamarahorario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del horario',
  `idcamara` int(11) NOT NULL COMMENT 'Codigo de la camara',
  `hora_desde` varchar(4) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Hora Desde',
  `hora_hasta` varchar(4) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Hora Hasta',
  `dias` varchar(15) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Dias de la semana',
  PRIMARY KEY (`idcamarahorario`),
  KEY `idcamara` (`idcamara`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Horarios de ejecucion de eventos de una camara' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vid_evento`
--

CREATE TABLE IF NOT EXISTS `vid_evento` (
  `idevento` int(11) NOT NULL AUTO_INCREMENT,
  `idservidor` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Codigo Servidor',
  `servidor` varchar(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'idsevidor no referenciado',
  `camara` varchar(3) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Numero de camara',
  `evento` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Numero de evento',
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Breve Descripcion',
  `file` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Archivo generado por el evento',
  `fecha` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Estado del evento',
  `observacion` text CHARACTER SET utf8 COLLATE utf8_spanish_ci COMMENT 'Observacion al resolver el evento',
  PRIMARY KEY (`idevento`),
  UNIQUE KEY `unq_datedesc_ve` (`fecha`,`descripcion`),
  KEY `idservidor` (`idservidor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registro de Eventos' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vid_marca_camara`
--

CREATE TABLE IF NOT EXISTS `vid_marca_camara` (
  `idmarca_camara` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo de la marca',
  `nombre_marca_camara` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre de la marca',
  PRIMARY KEY (`idmarca_camara`),
  UNIQUE KEY `nombre_marca_camara` (`nombre_marca_camara`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Marca de la camara' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `vid_marca_camara`
--

INSERT INTO `vid_marca_camara` (`idmarca_camara`, `nombre_marca_camara`) VALUES
(12, '3gpp'),
(11, 'AvTech'),
(8, 'Axis');

-- --------------------------------------------------------

--
-- Table structure for table `vid_modelo_camara`
--

CREATE TABLE IF NOT EXISTS `vid_modelo_camara` (
  `idmodelo_camara` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del modelo',
  `idmarca_camara` int(11) NOT NULL COMMENT 'Codigo de la marca',
  `nombre_modelo_camara` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre del modelo',
  PRIMARY KEY (`idmodelo_camara`),
  KEY `idmarca_camara` (`idmarca_camara`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Modelo de la camara' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `vid_modelo_camara`
--

INSERT INTO `vid_modelo_camara` (`idmodelo_camara`, `idmarca_camara`, `nombre_modelo_camara`) VALUES
(2, 8, '214'),
(7, 11, '211'),
(8, 12, '3gpp-IP'),
(9, 8, '210');

-- --------------------------------------------------------

--
-- Table structure for table `vid_motionconf`
--

CREATE TABLE IF NOT EXISTS `vid_motionconf` (
  `idservidor` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `ipv4` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `daemon` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'on',
  `process_id_file` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'motion.pid',
  `setup_mode` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `videodevice` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/dev/video0',
  `tunerdevice` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/dev/tuner0',
  `input` int(3) NOT NULL DEFAULT '8',
  `norm` enum('0','1','2','3') COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `frequency` int(4) NOT NULL DEFAULT '0',
  `rotate` enum('0','90','180','270') COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `width` int(11) NOT NULL DEFAULT '352',
  `height` int(11) NOT NULL DEFAULT '288',
  `framerate` smallint(6) NOT NULL DEFAULT '100',
  `minimum_frame_time` smallint(6) NOT NULL DEFAULT '0',
  `netcam_url` varchar(150) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'value',
  `netcam_userpass` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'user:password',
  `netcam_proxy` varchar(150) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'http://myproxy:1234',
  `auto_brightness` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `brightness` int(3) NOT NULL DEFAULT '30',
  `contrast` int(3) NOT NULL DEFAULT '0',
  `saturation` int(3) NOT NULL DEFAULT '0',
  `hue` int(3) NOT NULL DEFAULT '0',
  `roundrobin_frames` tinyint(4) NOT NULL DEFAULT '1',
  `roundrobin_skip` tinyint(4) NOT NULL DEFAULT '1',
  `switchfilter` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `threshold` int(11) NOT NULL DEFAULT '1500',
  `threshold_tune` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `noise_level` int(11) NOT NULL DEFAULT '32',
  `noise_tune` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'on',
  `night_compensate` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `despeckle` varchar(10) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'EedDl',
  `mask_file` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `smart_mask_speed` enum('0','1','2','3','4','5','6','7','8','9','10') COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `lightswitch` int(3) NOT NULL DEFAULT '0',
  `minimum_motion_frames` int(11) NOT NULL DEFAULT '1',
  `pre_capture` int(11) NOT NULL DEFAULT '0',
  `post_capture` int(11) NOT NULL DEFAULT '0',
  `gap` int(11) NOT NULL DEFAULT '60',
  `max_mpeg_time` int(11) NOT NULL DEFAULT '3600',
  `low_cpu` int(11) NOT NULL DEFAULT '1',
  `output_all` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `output_normal` enum('on','off','first','best') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'first',
  `output_motion` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `quality` int(11) NOT NULL DEFAULT '75',
  `ppm` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `ffmpeg_cap_new` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'on',
  `ffmpeg_cap_motion` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `ffmpeg_timelapse` int(11) NOT NULL DEFAULT '0',
  `ffmpeg_timelapse_mode` enum('hourly','daily','weekly-sunday','weekly-monday','monthly','manual') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'hourly',
  `ffmpeg_bps` int(11) NOT NULL DEFAULT '500000',
  `ffmpeg_variable_bitrate` int(11) NOT NULL DEFAULT '0' COMMENT '0-31',
  `ffmpeg_video_codec` varchar(15) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'flv',
  `ffmpeg_deinterlace` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `snapshot_interval` int(11) NOT NULL DEFAULT '0',
  `locate` enum('on','off','preview') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `text_right` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '%Y-%m-%d\\n%T-%q',
  `text_left` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'CENIT - DIDI - CAMARA %t',
  `text_changes` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `text_event` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '%Y%m%d%H%M%S',
  `text_double` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `target_dir` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/home/sip/motion/camara%t ',
  `snapshot_filename` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '%v-%Y%m%d%H%M%S-snapshot',
  `jpeg_filename` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '%v-%Y%m%d%H%M%S-%q',
  `movie_filename` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '%v-%Y%m%d%H%M%S',
  `timelapse_filename` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '%Y%m%d-timelapse',
  `webcam_port` int(5) NOT NULL DEFAULT '8081',
  `webcam_quality` int(2) NOT NULL DEFAULT '50',
  `webcam_motion` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `webcam_maxrate` int(2) NOT NULL DEFAULT '1',
  `webcam_localhost` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `webcam_limit` int(3) NOT NULL DEFAULT '0',
  `control_port` int(5) NOT NULL DEFAULT '8080',
  `control_localhost` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `control_html_output` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'on',
  `control_authentication` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'admin:admin',
  `track_type` enum('0','1','2','3','4','5') COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `track_auto` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `track_port` varchar(10) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'ttyS5',
  `track_motorx` int(3) NOT NULL DEFAULT '-1',
  `track_motory` int(3) NOT NULL DEFAULT '-1',
  `track_maxx` int(3) NOT NULL DEFAULT '0',
  `track_maxy` int(3) NOT NULL DEFAULT '0',
  `track_iomojo_id` int(2) NOT NULL DEFAULT '0',
  `track_step_angle_x` int(2) NOT NULL DEFAULT '10',
  `track_step_angle_y` int(2) NOT NULL DEFAULT '10',
  `track_move_wait` int(2) NOT NULL DEFAULT '10',
  `track_speed` int(4) NOT NULL DEFAULT '255',
  `track_stepsize` int(2) NOT NULL DEFAULT '40',
  `quiet` enum('on','off') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'off',
  `on_event_start` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/home/sip/event_start.sh %t %Y-%m-%d_%H:%M:%S %C',
  `on_event_end` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/home/sip/event_end.sh %t %Y-%m-%d_%H:%M:%S %C',
  `on_picture_save` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/home/sip/picture_save.sh %t %Y-%m-%d_%H:%M:%S %C',
  `on_motion_detected` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/home/sip/motion_detected.sh %t %Y-%m-%d_%H:%M:%S %C',
  `on_movie_start` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/home/sip/movie_start.sh %t %Y-%m-%d %T %C %f',
  `on_movie_end` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '/home/sip/movie_end.sh %t %Y-%m-%d_%T %C %f',
  `video_pipe` varchar(15) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'value',
  `motion_video_pipe` varchar(15) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'value',
  PRIMARY KEY (`idservidor`),
  UNIQUE KEY `ipv4` (`ipv4`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vid_plantafisica_zona`
--

CREATE TABLE IF NOT EXISTS `vid_plantafisica_zona` (
  `idplantafisica` int(11) NOT NULL COMMENT 'codigo de planta fisica',
  `idzona` int(11) NOT NULL COMMENT 'codigo de zona',
  `leftX` int(11) NOT NULL COMMENT 'Coordenada X',
  `topY` int(11) NOT NULL COMMENT 'Coordenada Y',
  `width` int(11) NOT NULL COMMENT 'Ancho',
  `height` int(11) NOT NULL COMMENT 'Alto',
  `bgcolor` varchar(8) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Color de fondo',
  UNIQUE KEY `idplantafisica` (`idplantafisica`,`idzona`),
  KEY `idzona` (`idzona`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='asociacion planta fisica - zona';

-- --------------------------------------------------------

--
-- Table structure for table `vid_servidor`
--

CREATE TABLE IF NOT EXISTS `vid_servidor` (
  `idservidor` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `idsegmento` int(11) NOT NULL COMMENT 'Codigo Segmento',
  `computador` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tiposervidor` varchar(20) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'motion',
  `ipv4` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `enlinea` tinyint(1) NOT NULL DEFAULT '0',
  `idtimezone` int(11) NOT NULL DEFAULT '83' COMMENT 'codigo zona horaria',
  `so_usuario` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `so_clave` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `conexioncamaras` tinyint(1) NOT NULL DEFAULT '1',
  `pais` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Datos Ubicacion',
  `estado` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Datos Ubicacion',
  `ciudad` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Datos Ubicacion',
  `avenida` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Datos Ubicacion',
  `edificio` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Datos Ubicacion',
  `piso` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Datos Ubicacion',
  `oficina` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Datos Ubicacion',
  PRIMARY KEY (`idservidor`),
  UNIQUE KEY `ipv4` (`ipv4`),
  KEY `ind_tz_vs` (`idtimezone`),
  KEY `idsegmento` (`idsegmento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vid_tipo_camara`
--

CREATE TABLE IF NOT EXISTS `vid_tipo_camara` (
  `idtipo_camara` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo del tipo de camara',
  `idmodelo_camara` int(11) NOT NULL COMMENT 'codigo del Modelo',
  `nombre_tipo_camara` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre del tipo',
  `ptz` tinyint(1) NOT NULL COMMENT 'Indicador de PTZ',
  `urlfoto` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Script y query string de la captura de foto',
  `urlvideo` varchar(255) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Script y query string de la captura de video',
  `descripcion_tipo_camara` text COLLATE utf8_spanish_ci COMMENT 'Descripcion opcional del tipo',
  PRIMARY KEY (`idtipo_camara`),
  KEY `idmodelo_camara` (`idmodelo_camara`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tipo de camara' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `vid_tipo_camara`
--

INSERT INTO `vid_tipo_camara` (`idtipo_camara`, `idmodelo_camara`, `nombre_tipo_camara`, `ptz`, `urlfoto`, `urlvideo`, `descripcion_tipo_camara`) VALUES
(1, 2, 'IPPTZ', 1, '/axis-cgi/jpg/image.cgi', '/axis-cgi/mjpg/video.cgi', 'ptz que se mueve para \r\nacÃ¡ y para allÃ¡...'),
(3, 7, 'IP', 0, '/cgi-bin/guest/Video.cgi?media=MJPEG', '/cgi-bin/guest/Video.cgi?media=MJPEG', 'Camara China'),
(4, 8, '3gpp-IP-CAMARA', 0, '/videostream.cgi', '/videostream.cgi', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vid_zona`
--

CREATE TABLE IF NOT EXISTS `vid_zona` (
  `idzona` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Codigo Zona',
  `zona` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Nombre',
  `descripcion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Breve descripcion',
  PRIMARY KEY (`idzona`),
  UNIQUE KEY `zona` (`zona`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Zonificacion de monitoreo' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vid_zona_camara`
--

CREATE TABLE IF NOT EXISTS `vid_zona_camara` (
  `idzona` int(11) NOT NULL COMMENT 'Codigo Zona',
  `idcamara` int(11) NOT NULL COMMENT 'Codigo Camara',
  UNIQUE KEY `idzona` (`idzona`,`idcamara`),
  KEY `idcamara` (`idcamara`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Asociacion zonas - camaras';

-- --------------------------------------------------------

--
-- Table structure for table `vid_zona_usuario`
--

CREATE TABLE IF NOT EXISTS `vid_zona_usuario` (
  `idzona` int(11) NOT NULL COMMENT 'Codigo Zona',
  `usuario` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL COMMENT 'Login de usuario',
  UNIQUE KEY `idzona` (`idzona`,`usuario`),
  KEY `usuario` (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Asociacion zonas - segmentos - usuarios';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accesopc`
--
ALTER TABLE `accesopc`
  ADD CONSTRAINT `accesopc_ibfk_3` FOREIGN KEY (`idaccesopc_tipo`) REFERENCES `accesopc_tipo` (`idaccesopc_tipo`),
  ADD CONSTRAINT `accesopc_ibfk_4` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `accesopc_ibfk_5` FOREIGN KEY (`idaccesopc_tipo`) REFERENCES `accesopc_tipo` (`idaccesopc_tipo`),
  ADD CONSTRAINT `accesopc_ibfk_6` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `car_cargo`
--
ALTER TABLE `car_cargo`
  ADD CONSTRAINT `car_cargo_ibfk_1` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`) ON DELETE SET NULL,
  ADD CONSTRAINT `car_cargo_ibfk_2` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`) ON DELETE SET NULL;

--
-- Constraints for table `car_departamento`
--
ALTER TABLE `car_departamento`
  ADD CONSTRAINT `car_departamento_ibfk_1` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`) ON DELETE SET NULL,
  ADD CONSTRAINT `car_departamento_ibfk_2` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`) ON DELETE SET NULL;

--
-- Constraints for table `car_empleado`
--
ALTER TABLE `car_empleado`
  ADD CONSTRAINT `car_empleado_ibfk_11` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`),
  ADD CONSTRAINT `car_empleado_ibfk_12` FOREIGN KEY (`iddepartamento`) REFERENCES `car_departamento` (`iddepartamento`),
  ADD CONSTRAINT `car_empleado_ibfk_13` FOREIGN KEY (`idcargo`) REFERENCES `car_cargo` (`idcargo`),
  ADD CONSTRAINT `car_empleado_ibfk_14` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`),
  ADD CONSTRAINT `car_empleado_ibfk_15` FOREIGN KEY (`iddepartamento`) REFERENCES `car_departamento` (`iddepartamento`),
  ADD CONSTRAINT `car_empleado_ibfk_16` FOREIGN KEY (`idcargo`) REFERENCES `car_cargo` (`idcargo`);

--
-- Constraints for table `car_empleado_impresion`
--
ALTER TABLE `car_empleado_impresion`
  ADD CONSTRAINT `car_empleado_impresion_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `car_empleado_impresion_ibfk_3` FOREIGN KEY (`idcarnet_empleado`) REFERENCES `car_empleado` (`idcarnet_empleado`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `car_empleado_impresion_ibfk_4` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `car_empleado_impresion_ibfk_5` FOREIGN KEY (`idcarnet_empleado`) REFERENCES `car_empleado` (`idcarnet_empleado`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `car_visitante`
--
ALTER TABLE `car_visitante`
  ADD CONSTRAINT `car_visitante_ibfk_1` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`),
  ADD CONSTRAINT `car_visitante_ibfk_2` FOREIGN KEY (`idplantilla`) REFERENCES `car_plantilla` (`idplantilla`);

--
-- Constraints for table `car_visitante_impresion`
--
ALTER TABLE `car_visitante_impresion`
  ADD CONSTRAINT `car_visitante_impresion_ibfk_1` FOREIGN KEY (`idcarnet_visitante`) REFERENCES `car_visitante` (`idcarnet_visitante`) ON DELETE CASCADE,
  ADD CONSTRAINT `car_visitante_impresion_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `car_visitante_impresion_ibfk_3` FOREIGN KEY (`idcarnet_visitante`) REFERENCES `car_visitante` (`idcarnet_visitante`) ON DELETE CASCADE,
  ADD CONSTRAINT `car_visitante_impresion_ibfk_4` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `categoria_ibfk_1` FOREIGN KEY (`categoria_padre`) REFERENCES `categoria` (`idcategoria`) ON DELETE SET NULL,
  ADD CONSTRAINT `categoria_ibfk_2` FOREIGN KEY (`categoria_padre`) REFERENCES `categoria` (`idcategoria`) ON DELETE SET NULL;

--
-- Constraints for table `ctrl_controladora`
--
ALTER TABLE `ctrl_controladora`
  ADD CONSTRAINT `ctrl_controladora_ibfk_1` FOREIGN KEY (`idsegmento`) REFERENCES `segmento` (`idsegmento`),
  ADD CONSTRAINT `ctrl_controladora_ibfk_2` FOREIGN KEY (`idsegmento`) REFERENCES `segmento` (`idsegmento`),
  ADD CONSTRAINT `ctrl_controladora_ibfk_3` FOREIGN KEY (`idtipo_controladora`) REFERENCES `ctrl_tipo_controladora` (`idtipo_controladora`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_controladora_1` FOREIGN KEY (`idtipo_controladora`) REFERENCES `ctrl_tipo_controladora` (`idtipo_controladora`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ctrl_grupo`
--
ALTER TABLE `ctrl_grupo`
  ADD CONSTRAINT `ctrl_grupo_ibfk_1` FOREIGN KEY (`idgrupo_padre`) REFERENCES `ctrl_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_grupo_1` FOREIGN KEY (`idgrupo_padre`) REFERENCES `ctrl_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ctrl_grupo_puerta`
--
ALTER TABLE `ctrl_grupo_puerta`
  ADD CONSTRAINT `ctrl_grupo_puerta_ibfk_1` FOREIGN KEY (`idgrupo`) REFERENCES `ctrl_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ctrl_grupo_puerta_ibfk_2` FOREIGN KEY (`idpuerta`) REFERENCES `ctrl_puerta` (`idpuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_grupo_puerta_1` FOREIGN KEY (`idgrupo`) REFERENCES `ctrl_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_grupo_puerta_2` FOREIGN KEY (`idpuerta`) REFERENCES `ctrl_puerta` (`idpuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ctrl_lectora`
--
ALTER TABLE `ctrl_lectora`
  ADD CONSTRAINT `ctrl_lectora_ibfk_1` FOREIGN KEY (`idcontroladora`) REFERENCES `ctrl_controladora` (`idcontroladora`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ctrl_lectora_ibfk_2` FOREIGN KEY (`idpuerta`) REFERENCES `ctrl_puerta` (`idpuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_lectora_1` FOREIGN KEY (`idcontroladora`) REFERENCES `ctrl_controladora` (`idcontroladora`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_lectora_2` FOREIGN KEY (`idpuerta`) REFERENCES `ctrl_puerta` (`idpuerta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ctrl_usuario`
--
ALTER TABLE `ctrl_usuario`
  ADD CONSTRAINT `ctrl_usuario_ibfk_1` FOREIGN KEY (`iddepartamento`) REFERENCES `car_departamento` (`iddepartamento`),
  ADD CONSTRAINT `ctrl_usuario_ibfk_2` FOREIGN KEY (`iddepartamento`) REFERENCES `car_departamento` (`iddepartamento`),
  ADD CONSTRAINT `ctrl_usuario_ibfk_3` FOREIGN KEY (`idgrupo`) REFERENCES `ctrl_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ctrl_usuario_ibfk_4` FOREIGN KEY (`idzonadetiempo`) REFERENCES `ctrl_zonadetiempo` (`indice`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_usuario_1` FOREIGN KEY (`idgrupo`) REFERENCES `ctrl_grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ctrl_usuario_2` FOREIGN KEY (`idzonadetiempo`) REFERENCES `ctrl_zonadetiempo` (`indice`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `funcion`
--
ALTER TABLE `funcion`
  ADD CONSTRAINT `funcion_ibfk_1` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE CASCADE,
  ADD CONSTRAINT `funcion_ibfk_2` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE CASCADE;

--
-- Constraints for table `grupo_has_funcion`
--
ALTER TABLE `grupo_has_funcion`
  ADD CONSTRAINT `grupo_has_funcion_ibfk_3` FOREIGN KEY (`funcion_id`) REFERENCES `funcion` (`id`),
  ADD CONSTRAINT `grupo_has_funcion_ibfk_4` FOREIGN KEY (`grupo_idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE CASCADE,
  ADD CONSTRAINT `grupo_has_funcion_ibfk_5` FOREIGN KEY (`funcion_id`) REFERENCES `funcion` (`id`),
  ADD CONSTRAINT `grupo_has_funcion_ibfk_6` FOREIGN KEY (`grupo_idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE CASCADE;

--
-- Constraints for table `segmento_usuario`
--
ALTER TABLE `segmento_usuario`
  ADD CONSTRAINT `segmento_usuario_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE,
  ADD CONSTRAINT `segmento_usuario_ibfk_3` FOREIGN KEY (`idsegmento`) REFERENCES `segmento` (`idsegmento`) ON DELETE CASCADE,
  ADD CONSTRAINT `segmento_usuario_ibfk_4` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE,
  ADD CONSTRAINT `segmento_usuario_ibfk_5` FOREIGN KEY (`idsegmento`) REFERENCES `segmento` (`idsegmento`) ON DELETE CASCADE;

--
-- Constraints for table `usuario_has_grupo`
--
ALTER TABLE `usuario_has_grupo`
  ADD CONSTRAINT `usuario_has_grupo_ibfk_1` FOREIGN KEY (`usuario_usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE,
  ADD CONSTRAINT `usuario_has_grupo_ibfk_2` FOREIGN KEY (`grupo_idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE CASCADE,
  ADD CONSTRAINT `usuario_has_grupo_ibfk_3` FOREIGN KEY (`usuario_usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE,
  ADD CONSTRAINT `usuario_has_grupo_ibfk_4` FOREIGN KEY (`grupo_idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE CASCADE;

--
-- Constraints for table `vid_camara`
--
ALTER TABLE `vid_camara`
  ADD CONSTRAINT `vid_camara_ibfk_2` FOREIGN KEY (`idplantafisica`) REFERENCES `plantafisica` (`idplantafisica`),
  ADD CONSTRAINT `vid_camara_ibfk_3` FOREIGN KEY (`idtipo_camara`) REFERENCES `vid_tipo_camara` (`idtipo_camara`),
  ADD CONSTRAINT `vid_camara_ibfk_4` FOREIGN KEY (`idservidor`) REFERENCES `vid_servidor` (`idservidor`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_camara_ibfk_5` FOREIGN KEY (`idplantafisica`) REFERENCES `plantafisica` (`idplantafisica`),
  ADD CONSTRAINT `vid_camara_ibfk_6` FOREIGN KEY (`idtipo_camara`) REFERENCES `vid_tipo_camara` (`idtipo_camara`),
  ADD CONSTRAINT `vid_camara_ibfk_7` FOREIGN KEY (`idservidor`) REFERENCES `vid_servidor` (`idservidor`) ON DELETE CASCADE;

--
-- Constraints for table `vid_camara_axisguard`
--
ALTER TABLE `vid_camara_axisguard`
  ADD CONSTRAINT `fk_vid_camara_axisguard_1` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `vid_camara_axisguard_ibfk_1` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vid_camara_axisguard_tour`
--
ALTER TABLE `vid_camara_axisguard_tour`
  ADD CONSTRAINT `fk_vid_camara_axisguard_tour_2` FOREIGN KEY (`idcamaraaxispresetpos`) REFERENCES `vid_camara_axispresetpos` (`idcamaraaxispresetpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `vid_camara_axisguard_tour_ibfk_1` FOREIGN KEY (`idcamaraaxisguard`) REFERENCES `vid_camara_axisguard` (`idcamaraaxisguard`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `vid_camara_axisguard_tour_ibfk_2` FOREIGN KEY (`idcamaraaxispresetpos`) REFERENCES `vid_camara_axispresetpos` (`idcamaraaxispresetpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `vid_camara_axisguard_tour_ibfk_3` FOREIGN KEY (`idcamaraaxisguard`) REFERENCES `vid_camara_axisguard` (`idcamaraaxisguard`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `vid_camara_axispresetpos`
--
ALTER TABLE `vid_camara_axispresetpos`
  ADD CONSTRAINT `fk_vid_camara_axispresetpos_1` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `vid_camara_axispresetpos_ibfk_1` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vid_camara_horario`
--
ALTER TABLE `vid_camara_horario`
  ADD CONSTRAINT `vid_camara_horario_ibfk_1` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_camara_horario_ibfk_2` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE CASCADE;

--
-- Constraints for table `vid_evento`
--
ALTER TABLE `vid_evento`
  ADD CONSTRAINT `vid_evento_ibfk_1` FOREIGN KEY (`idservidor`) REFERENCES `vid_servidor` (`idservidor`) ON DELETE SET NULL,
  ADD CONSTRAINT `vid_evento_ibfk_2` FOREIGN KEY (`idservidor`) REFERENCES `vid_servidor` (`idservidor`) ON DELETE SET NULL;

--
-- Constraints for table `vid_modelo_camara`
--
ALTER TABLE `vid_modelo_camara`
  ADD CONSTRAINT `vid_modelo_camara_ibfk_1` FOREIGN KEY (`idmarca_camara`) REFERENCES `vid_marca_camara` (`idmarca_camara`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_modelo_camara_ibfk_2` FOREIGN KEY (`idmarca_camara`) REFERENCES `vid_marca_camara` (`idmarca_camara`) ON DELETE CASCADE;

--
-- Constraints for table `vid_motionconf`
--
ALTER TABLE `vid_motionconf`
  ADD CONSTRAINT `vid_motionconf_ibfk_1` FOREIGN KEY (`idservidor`) REFERENCES `vid_servidor` (`idservidor`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_motionconf_ibfk_2` FOREIGN KEY (`idservidor`) REFERENCES `vid_servidor` (`idservidor`) ON DELETE CASCADE;

--
-- Constraints for table `vid_plantafisica_zona`
--
ALTER TABLE `vid_plantafisica_zona`
  ADD CONSTRAINT `vid_plantafisica_zona_ibfk_1` FOREIGN KEY (`idplantafisica`) REFERENCES `plantafisica` (`idplantafisica`),
  ADD CONSTRAINT `vid_plantafisica_zona_ibfk_2` FOREIGN KEY (`idzona`) REFERENCES `vid_zona` (`idzona`),
  ADD CONSTRAINT `vid_plantafisica_zona_ibfk_3` FOREIGN KEY (`idplantafisica`) REFERENCES `plantafisica` (`idplantafisica`),
  ADD CONSTRAINT `vid_plantafisica_zona_ibfk_4` FOREIGN KEY (`idzona`) REFERENCES `vid_zona` (`idzona`);

--
-- Constraints for table `vid_servidor`
--
ALTER TABLE `vid_servidor`
  ADD CONSTRAINT `vid_servidor_ibfk_1` FOREIGN KEY (`idsegmento`) REFERENCES `segmento` (`idsegmento`),
  ADD CONSTRAINT `vid_servidor_ibfk_2` FOREIGN KEY (`idtimezone`) REFERENCES `timezone` (`idtimezone`),
  ADD CONSTRAINT `vid_servidor_ibfk_3` FOREIGN KEY (`idsegmento`) REFERENCES `segmento` (`idsegmento`),
  ADD CONSTRAINT `vid_servidor_ibfk_4` FOREIGN KEY (`idtimezone`) REFERENCES `timezone` (`idtimezone`);

--
-- Constraints for table `vid_tipo_camara`
--
ALTER TABLE `vid_tipo_camara`
  ADD CONSTRAINT `vid_tipo_camara_ibfk_1` FOREIGN KEY (`idmodelo_camara`) REFERENCES `vid_modelo_camara` (`idmodelo_camara`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_tipo_camara_ibfk_2` FOREIGN KEY (`idmodelo_camara`) REFERENCES `vid_modelo_camara` (`idmodelo_camara`) ON DELETE CASCADE;

--
-- Constraints for table `vid_zona_camara`
--
ALTER TABLE `vid_zona_camara`
  ADD CONSTRAINT `vid_zona_camara_ibfk_1` FOREIGN KEY (`idzona`) REFERENCES `vid_zona` (`idzona`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_zona_camara_ibfk_2` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_zona_camara_ibfk_3` FOREIGN KEY (`idzona`) REFERENCES `vid_zona` (`idzona`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_zona_camara_ibfk_4` FOREIGN KEY (`idcamara`) REFERENCES `vid_camara` (`idcamara`) ON DELETE CASCADE;

--
-- Constraints for table `vid_zona_usuario`
--
ALTER TABLE `vid_zona_usuario`
  ADD CONSTRAINT `vid_zona_usuario_ibfk_1` FOREIGN KEY (`idzona`) REFERENCES `vid_zona` (`idzona`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_zona_usuario_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_zona_usuario_ibfk_3` FOREIGN KEY (`idzona`) REFERENCES `vid_zona` (`idzona`) ON DELETE CASCADE,
  ADD CONSTRAINT `vid_zona_usuario_ibfk_4` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`usuario`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
