<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php

class Model{
    
    
    
    public static function getUsuarioToLogin($usuario, $clave){
        $sql = "SELECT activo, usuario FROM usuario where clave= '".md5(trim($usuario."_".$clave))."' and usuario='$usuario'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        if ($res->rowCount() == 0){
            return false;
        }
        return $res->fetchObject();        
    }
    
    /**
     * Consulta de permisos de un usuarios
     * @param string $usuario Usuario
     * @return object Devuelve registros como objeto
     */
    public static function getGrupoFuncionUsr($usuario){
        
        if (Controller::chkMobilBrowser()){
            $arg["funcionMobil"] = " and f.mobil='1'";
        }
        
        $sql = "select u.\"superUsuario\",
                       (select count(usuario) from usuario where \"superUsuario\" = '1') as \"chkSuperUsuario\",
                       acc.cookie,
                       u.usuario,
                       u.nombre,
                       u.apellido,
                       u.activo,
                       catP.idcategoria as \"idcatPadre\",
                       catP.nombre as \"catPadre\",
                       catH.idcategoria as \"idcatHijo\",
                       catH.nombre as \"catHijo\",
                       f.idfuncion,
                       f.nombre as \"nombreFuncion\"
                from usuario u
                    left join  usuario_has_grupo ug on (u.usuario        = ug.usuario_usuario )
                    left join grupo_has_funcion gf on (ug.grupo_idgrupo = gf.grupo_idgrupo)
                    left join funcion f            on (gf.funcion_id    = f.id ".$arg["funcionMobil"].")
                    left join categoria catH       on (f.idcategoria    = catH.idcategoria and catH.categoria_padre is not null)
                    left join categoria catP       on (catH.categoria_padre = catP.idcategoria )
                    left join accesopc acc on (u.usuario = acc.usuario and acc.ipv4_pc = '".$_SERVER["REMOTE_ADDR"]."')
                where u.usuario = '".$usuario."'
                    order by catP.nombre,catH.nombre,f.nombre";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }
    
    /**
     * Consulta de Accesos a PCs de un usuario
     * @return object Devuelve un registro como objeto
     */
    public static function  getAccesosPcsUsr($usuario){
        $sql = "select acct.idaccesopc_tipo,
                       acct.path,
                       acct.descripcion,
                       
                       acc.idaccesopc,
                       acc.usuario,
                       acc.ipv4_pc,
                       acc.fecha_expiracion,
                       acc.cookie
                from accesopc_tipo acct
                        left join accesopc acc on (acc.usuario = '".$usuario."' and acc.idaccesopc_tipo = acct.idaccesopc_tipo)
                    order by acct.descripcion,acc.ipv4_pc";        
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);    
    }
    
    /**
     * Consulta dinamica de motor de busqueda
     * @param string $value       valor de codigo ID a bucsar
     * @param string $description valores de descripcion a buscar
     * @param string $tabla       tabla de BD a consultar
     * @param string $arg         argumentos sobre la busqueda
     * @return object Devuelve registros como objeto
     */
    public static function getDataBuscador($value,$description, $tabla, $arg){
        $sql = "select ".$value.",".$description." from ".$tabla." ".$arg;    
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }
}
?>
