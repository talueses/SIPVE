<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'model.php';
/**
 * Clase de interface a la base de datos
 * Se utiliza en Objeto PDO
 * Se utilizan todos los drivers de base de datos que PDO permite 
 */
class DB_Class {
    
    
    /*** Variables Publicas ***/    
    
    /**
     * @var object Objeto PDO 
     */
    public static $PDO        = null;
    /**
     * @var string Errores generados
     */
    public static $error      = null;
    /**
     * @var string Errores generados
     */
    public static $dbErrorMsg = "<p class='dbErrorMsg ui-state-error ui-corner-all'>Ocurri&oacute; un error o adverencia de Base de Datos.<br> Contacte al Administrador del sistema.</p>";

    /*** Variables Privadas ***/
    
    /**
     * @var object Arreglo de Objetos sobre contenido del archivo configuracion
     */
    private static $conf     = null;
    /**
     * @var string Tipo de base de datos (Manejador) 
     */
    private static $dbtype   = null;    
    /**
     * @var string Nombre de la base de datos
     */
    private static $dbname   = null;
    /**
     * @var string Usuario de base de datos
     */
    private static $user     = null;
    /**
     * @var string Clave del usuario de base de datos
     */
    private static $password = null;
    /**
     * @var string Servidor donde reside la base de datos
     */
    private static $host     = null;

    /**
     * Establece los parametros de conexion de la base de datos 
     * segun valores en archivo de configuracion conf.ini
     * @return boolean Devuelve Verdadero si se logra establecer la conexion
     */
    private static function DB_Init() {
        
        self::$conf = (object) parse_ini_file(dirname(__FILE__)."/../conf.ini", true);
                
        foreach (self::$conf as $key => $conf){
            
            if ($key=="DB_CONF"){                
                foreach ($conf as $key => $db_conf){
                    self::$$key = $db_conf;                    
                }
            }
        }        
        return self::DB_Connect();

    }

    /**
     * Establece la conexion a las base de datos mediante el objeto PDO
     * Se utilizan todos los drivers de base de datos que PDO permite
     * @return boolean Devuelve Verdadero si se establece la conexion
     */
    private static function DB_Connect(){        
        try {
            self::$PDO = new PDO(self::$dbtype.":host=".self::$host.";dbname=".self::$dbname, self::$user, self::$password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode="ANSI"'));                
            //self::$PDO = new PDO(self::$dbtype.":host=".self::$host.";dbname=".self::$dbname, self::$user, self::$password);                
            self::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);                     
            
        }
        catch (PDOException $err){            
            self::$error = $err->getMessage();             
            
            $filename = "DB_Connection_".date("Y-m-d").".txt";
            Controller::setLog($filename, self::$error);
            
            return false;
        }       
        return true;            
    }

    /**
     * Ejecuta un comando SQL en el DBMS
     * @param string $sql Comando SQL a ser ejecutado en el DBMS
     * @return boolean Devuelve Verdadero si se ejecuta con exito el Query
     */
    public static function DB_Query($sql = ""){
        
        if (!self::$PDO){
            if (!self::DB_Init()){
                //echo "<div align='left'><pre>".addcslashes(self::$error)."</pre></div>";
                return false;
            }
        }        
        
        try {
            $Query_ID = self::$PDO->prepare(self::setSqlStandards($sql));
            $Query_ID->execute();
            return $Query_ID;
        }
        catch (PDOException $err){
            
            /**
             * SQLSTATE[23000]: Integrity constraint violation
             */
            if ($err->getCode() == "23000"){ 
                self::$dbErrorMsg = "<p class='dbErrorMsg ui-state-error ui-corner-all'>Existe un dato asociado; verifique &oacute;<br> Contacte al Administrador del sistema.</p>";
            }
            
            self::$error = $err->getMessage(); 
            
            $filename = "DB_Query_".date("Y-m-d").".txt";            
            $data = self::$error." --- File: ".$_SERVER["PHP_SELF"];
            Controller::setLog($filename, $data);
            //echo "<div align='left'><pre>".$err->getMessage()."</pre></div>";
            return false;
        }         
    }
    
    private static function setSqlStandards($sql){
        
        if (self::$dbtype=="mysql"){                        
            $cast_int =  "unsigned";            
            $cast_datetime = "datetime";
        }
                
        if (self::$dbtype=="pgsql"){
            $cast_int =  "integer";
            $cast_datetime = "timestamp";
        }
        
        $sql = str_replace(":cast_int", $cast_int, $sql);     
        $sql = str_replace(":cast_datetime", $cast_datetime, $sql);     
        
        return $sql;
    }
}
?>
