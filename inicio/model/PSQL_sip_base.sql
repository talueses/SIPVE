-- psql -h <IP ADDRESS> -U <USER> < PSQL_sip_base.sql
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 142 (class 1259 OID 36972)
-- Dependencies: 3
-- Name: accesopc_idaccesopc_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE accesopc_idaccesopc_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accesopc_idaccesopc_seq OWNER TO sip;

--
-- TOC entry 2522 (class 0 OID 0)
-- Dependencies: 142
-- Name: accesopc_idaccesopc_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('accesopc_idaccesopc_seq', 1, true);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 143 (class 1259 OID 36974)
-- Dependencies: 2021 3
-- Name: accesopc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accesopc (
    idaccesopc integer DEFAULT nextval('accesopc_idaccesopc_seq'::regclass) NOT NULL,
    idaccesopc_tipo character varying(20) NOT NULL,
    usuario character varying(12) NOT NULL,
    ipv4_pc character varying(15) NOT NULL,
    fecha_expiracion date NOT NULL,
    cookie character varying(40) NOT NULL
);


ALTER TABLE public.accesopc OWNER TO sip;

--
-- TOC entry 144 (class 1259 OID 36978)
-- Dependencies: 3
-- Name: accesopc_tipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accesopc_tipo (
    idaccesopc_tipo character varying(20) NOT NULL,
    path character varying(250) NOT NULL,
    descripcion character varying(200) NOT NULL
);


ALTER TABLE public.accesopc_tipo OWNER TO sip;

--
-- TOC entry 145 (class 1259 OID 36981)
-- Dependencies: 3
-- Name: car_cargo_idcargo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE car_cargo_idcargo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_cargo_idcargo_seq OWNER TO sip;

--
-- TOC entry 2523 (class 0 OID 0)
-- Dependencies: 145
-- Name: car_cargo_idcargo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('car_cargo_idcargo_seq', 1, true);


--
-- TOC entry 146 (class 1259 OID 36983)
-- Dependencies: 2022 3
-- Name: car_cargo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE car_cargo (
    idcargo integer DEFAULT nextval('car_cargo_idcargo_seq'::regclass) NOT NULL,
    idplantilla integer,
    cargo_nombre_chk integer,
    cargo_nombre character varying(100) NOT NULL,
    cargo_top integer,
    cargo_left integer,
    cargo_w integer,
    cargo_h integer,
    cargo_color character varying(10),
    cargo_bgcolor character varying(10) NOT NULL,
    cargo_fuentetamano numeric(10,0) ,
    cargo_fuenteletra character varying(50) ,
    cargo_fuentealign character varying(10) ,
    cargo_fuentevalign character varying(10)
);


ALTER TABLE public.car_cargo OWNER TO sip;

--
-- TOC entry 147 (class 1259 OID 36987)
-- Dependencies: 3
-- Name: car_departamento_iddepartamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE car_departamento_iddepartamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_departamento_iddepartamento_seq OWNER TO sip;

--
-- TOC entry 2524 (class 0 OID 0)
-- Dependencies: 147
-- Name: car_departamento_iddepartamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('car_departamento_iddepartamento_seq', 1, true);


--
-- TOC entry 148 (class 1259 OID 36989)
-- Dependencies: 2023 3
-- Name: car_departamento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE car_departamento (
    iddepartamento integer DEFAULT nextval('car_departamento_iddepartamento_seq'::regclass) NOT NULL,
    idplantilla integer,
    departamento_nombre character varying(100) NOT NULL,
    departamento_top integer,
    departamento_left integer,
    departamento_w integer,
    departamento_h integer,
    departamento_color character varying(10),
    departamento_fuentetamano numeric(10,0) ,
    departamento_fuenteletra character varying(50),
    departamento_bgcolor_chk integer,
    departamento_bgcolor character varying(10),
    departamento_fuentealign character varying(10),
    departamento_fuentevalign character varying(10) 
);


ALTER TABLE public.car_departamento OWNER TO sip;

--
-- TOC entry 149 (class 1259 OID 36993)
-- Dependencies: 3
-- Name: car_empleado_idcarnet_empleado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE car_empleado_idcarnet_empleado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_empleado_idcarnet_empleado_seq OWNER TO sip;

--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 149
-- Name: car_empleado_idcarnet_empleado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('car_empleado_idcarnet_empleado_seq', 1, true);


--
-- TOC entry 150 (class 1259 OID 36995)
-- Dependencies: 2024 3
-- Name: car_empleado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE car_empleado (
    idcarnet_empleado integer DEFAULT nextval('car_empleado_idcarnet_empleado_seq'::regclass) NOT NULL,
    idplantilla integer NOT NULL,
    iddepartamento integer NOT NULL,
    idcargo integer NOT NULL,
    cedula character varying(8) NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    archivo_foto character varying(255) NOT NULL,
    fecha_vencimiento date
);


ALTER TABLE public.car_empleado OWNER TO sip;

--
-- TOC entry 151 (class 1259 OID 36999)
-- Dependencies: 3
-- Name: car_empleado_impresion_idcarnet_empleado_impresion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE car_empleado_impresion_idcarnet_empleado_impresion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_empleado_impresion_idcarnet_empleado_impresion_seq OWNER TO sip;

--
-- TOC entry 2526 (class 0 OID 0)
-- Dependencies: 151
-- Name: car_empleado_impresion_idcarnet_empleado_impresion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('car_empleado_impresion_idcarnet_empleado_impresion_seq', 1, true);


--
-- TOC entry 152 (class 1259 OID 37001)
-- Dependencies: 2025 2026 3
-- Name: car_empleado_impresion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE car_empleado_impresion (
    idcarnet_empleado_impresion integer DEFAULT nextval('car_empleado_impresion_idcarnet_empleado_impresion_seq'::regclass) NOT NULL,
    idcarnet_empleado integer NOT NULL,
    usuario character varying(12) NOT NULL,
    modo character(1) NOT NULL,
    fecha timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.car_empleado_impresion OWNER TO sip;

--
-- TOC entry 153 (class 1259 OID 37006)
-- Dependencies: 3
-- Name: car_plantilla_idplantilla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE car_plantilla_idplantilla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_plantilla_idplantilla_seq OWNER TO sip;

--
-- TOC entry 2527 (class 0 OID 0)
-- Dependencies: 153
-- Name: car_plantilla_idplantilla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('car_plantilla_idplantilla_seq', 1, true);


--
-- TOC entry 154 (class 1259 OID 37008)
-- Dependencies: 2027 2028 2029 3
-- Name: car_plantilla; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE car_plantilla (
    idplantilla integer DEFAULT nextval('car_plantilla_idplantilla_seq'::regclass) NOT NULL,
    plantilla character varying(100) NOT NULL,
    orientacion character varying(1) DEFAULT 'V'::character varying NOT NULL,
    archivo_plantilla character varying(255) NOT NULL,
    archivoback_plantilla character varying(255) NOT NULL,
    logo_chk integer,
    archivo_logo character varying(255),
    foto_top integer NOT NULL,
    foto_left integer NOT NULL,
    foto_w integer NOT NULL,
    foto_h integer NOT NULL,
    barcode_chk integer,
    barcode_front integer,
    barcode_back integer,
    barcode_top integer,
    barcode_left integer,
    barcode_w integer,
    barcode_h integer,
    logo_top integer,
    logo_left integer,
    logo_w integer,
    logo_h integer,
    logoback_chk integer,
    archivoback_logo character varying(255),
    logoback_top integer,
    logoback_left integer,
    logoback_w integer,
    logoback_h integer,
    institucion_chk integer,
    institucion_nombre character varying(150),
    institucion_top integer,
    institucion_left integer,
    institucion_w integer,
    institucion_h integer,
    institucion_color character varying(10),
    institucion_fuentetamano numeric(10,0) NOT NULL,
    institucion_fuenteletra character varying(50) NOT NULL,
    institucion_bgcolor_chk integer,
    institucion_bgcolor character varying(10) NOT NULL,
    institucion_fuentealign character varying(10) NOT NULL,
    institucion_fuentevalign character varying(10) NOT NULL,
    institucionback_chk integer,
    institucionback_top integer,
    institucionback_left integer,
    institucionback_w integer,
    institucionback_h integer,
    institucionback_color character varying(10),
    institucionback_fuentetamano numeric(10,0) NOT NULL,
    institucionback_fuenteletra character varying(50) NOT NULL,
    institucionback_bgcolor_chk integer,
    institucionback_bgcolor character varying(10) NOT NULL,
    institucionback_fuentealign character varying(10) NOT NULL,
    institucionback_fuentevalign character varying(10) NOT NULL,
    nombreapellido_top integer NOT NULL,
    nombreapellido_left integer NOT NULL,
    nombreapellido_w integer NOT NULL,
    nombreapellido_h integer NOT NULL,
    nombreapellido_color character varying(12) NOT NULL,
    nombreapellido_fuentetamano numeric(12,0) NOT NULL,
    nombreapellido_fuenteletra character varying(50) NOT NULL,
    nombreapellido_bgcolor_chk integer,
    nombreapellido_bgcolor character varying(10) NOT NULL,
    nombreapellido_fuentealign character varying(10) NOT NULL,
    nombreapellido_fuentevalign character varying(10) NOT NULL,
    cedula_top integer NOT NULL,
    cedula_left integer NOT NULL,
    cedula_w integer NOT NULL,
    cedula_h integer NOT NULL,
    cedula_color character varying(10) NOT NULL,
    cedula_fuentetamano numeric(10,0) NOT NULL,
    cedula_fuenteletra character varying(50) NOT NULL,
    cedula_bgcolor_chk integer,
    cedula_bgcolor character varying(10) NOT NULL,
    cedula_fuentealign character varying(10) NOT NULL,
    cedula_fuentevalign character varying(10) NOT NULL,
    fecha_chk integer,
    fecha_top integer,
    fecha_left integer,
    fecha_w integer,
    fecha_h integer,
    fecha_color character varying(10),
    fecha_fuentetamano numeric(10,0) NOT NULL,
    fecha_fuenteletra character varying(50) NOT NULL,
    fecha_bgcolor_chk integer,
    fecha_bgcolor character varying(10) NOT NULL,
    fecha_fuentealign character varying(10) NOT NULL,
    fecha_fuentevalign character varying(10) NOT NULL,
    visitante_top integer,
    visitante_left integer,
    visitante_w integer,
    visitante_h integer,
    visitante_color character varying(10),
    visitante_fuentetamano numeric(10,0) NOT NULL,
    visitante_fuenteletra character varying(50) NOT NULL,
    visitante_bgcolor_chk integer,
    visitante_bgcolor character varying(10) NOT NULL,
    visitante_fuentealign character varying(10) NOT NULL,
    visitante_fuentevalign character varying(10) NOT NULL,
    visitantenro_top integer,
    visitantenro_left integer,
    visitantenro_w integer,
    visitantenro_h integer,
    visitantenro_color character varying(10),
    visitantenro_fuentetamano numeric(10,0) NOT NULL,
    visitantenro_fuenteletra character varying(50) NOT NULL,
    visitantenro_bgcolor_chk integer,
    visitantenro_bgcolor character varying(10) NOT NULL,
    visitantenro_fuentealign character varying(10) NOT NULL,
    visitantenro_fuentevalign character varying(10) NOT NULL,
    CONSTRAINT car_plantilla_orientacion_check CHECK (((orientacion)::text = ANY ((ARRAY['V'::character varying, 'H'::character varying])::text[])))
);


ALTER TABLE public.car_plantilla OWNER TO sip;

--
-- TOC entry 155 (class 1259 OID 37017)
-- Dependencies: 3
-- Name: car_visitante_idcarnet_visitante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE car_visitante_idcarnet_visitante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_visitante_idcarnet_visitante_seq OWNER TO sip;

--
-- TOC entry 2528 (class 0 OID 0)
-- Dependencies: 155
-- Name: car_visitante_idcarnet_visitante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('car_visitante_idcarnet_visitante_seq', 1, true);


--
-- TOC entry 156 (class 1259 OID 37019)
-- Dependencies: 2030 3
-- Name: car_visitante; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE car_visitante (
    idcarnet_visitante integer DEFAULT nextval('car_visitante_idcarnet_visitante_seq'::regclass) NOT NULL,
    idplantilla integer NOT NULL,
    nombre character varying(100) NOT NULL,
    numero integer NOT NULL
);


ALTER TABLE public.car_visitante OWNER TO sip;

--
-- TOC entry 157 (class 1259 OID 37023)
-- Dependencies: 3
-- Name: car_visitante_impresion_idcarnet_visitante_impresion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE car_visitante_impresion_idcarnet_visitante_impresion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_visitante_impresion_idcarnet_visitante_impresion_seq OWNER TO sip;

--
-- TOC entry 2529 (class 0 OID 0)
-- Dependencies: 157
-- Name: car_visitante_impresion_idcarnet_visitante_impresion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('car_visitante_impresion_idcarnet_visitante_impresion_seq', 1, true);


--
-- TOC entry 158 (class 1259 OID 37025)
-- Dependencies: 2031 2032 3
-- Name: car_visitante_impresion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE car_visitante_impresion (
    idcarnet_visitante_impresion integer DEFAULT nextval('car_visitante_impresion_idcarnet_visitante_impresion_seq'::regclass) NOT NULL,
    idcarnet_visitante integer NOT NULL,
    usuario character varying(12) NOT NULL,
    modo character(1) NOT NULL,
    fecha timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.car_visitante_impresion OWNER TO sip;

--
-- TOC entry 159 (class 1259 OID 37030)
-- Dependencies: 3
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    idcategoria character varying(10) NOT NULL,
    nombre character varying(60) NOT NULL,
    descripcion character varying(100),
    categoria_padre character varying(10)
);


ALTER TABLE public.categoria OWNER TO sip;

--
-- TOC entry 160 (class 1259 OID 37033)
-- Dependencies: 3
-- Name: ctrl_controladora_idcontroladora_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_controladora_idcontroladora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_controladora_idcontroladora_seq OWNER TO sip;

--
-- TOC entry 2530 (class 0 OID 0)
-- Dependencies: 160
-- Name: ctrl_controladora_idcontroladora_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_controladora_idcontroladora_seq', 1, true);


--
-- TOC entry 161 (class 1259 OID 37035)
-- Dependencies: 2033 2034 2035 2036 3
-- Name: ctrl_controladora; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_controladora (
    idcontroladora integer DEFAULT nextval('ctrl_controladora_idcontroladora_seq'::regclass) NOT NULL,
    idtipo_controladora integer NOT NULL,
    idsegmento integer NOT NULL,
    nodo integer NOT NULL,
    ipv4 character varying(15) NOT NULL,
    puerto character varying(4) DEFAULT '1621'::character varying NOT NULL,
    maximo_usuarios integer DEFAULT 5000 NOT NULL,
    nombre character varying(100) NOT NULL,
    descripcion character varying(250),
    ubicacion character varying(250),
    estado integer DEFAULT 0 NOT NULL,
    fecha_peticion_estado timestamp without time zone,
    fecha_respuesta_estado timestamp without time zone
);


ALTER TABLE public.ctrl_controladora OWNER TO sip;

--
-- TOC entry 162 (class 1259 OID 37045)
-- Dependencies: 3
-- Name: ctrl_diafestivo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_diafestivo (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    mes integer NOT NULL,
    dia integer NOT NULL
);


ALTER TABLE public.ctrl_diafestivo OWNER TO sip;

--
-- TOC entry 163 (class 1259 OID 37048)
-- Dependencies: 3
-- Name: ctrl_grupo_idgrupo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_grupo_idgrupo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_grupo_idgrupo_seq OWNER TO sip;

--
-- TOC entry 2531 (class 0 OID 0)
-- Dependencies: 163
-- Name: ctrl_grupo_idgrupo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_grupo_idgrupo_seq', 1, true);


--
-- TOC entry 164 (class 1259 OID 37050)
-- Dependencies: 2037 3
-- Name: ctrl_grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_grupo (
    idgrupo integer DEFAULT nextval('ctrl_grupo_idgrupo_seq'::regclass) NOT NULL,
    numero integer NOT NULL,
    grupo character varying(100) NOT NULL,
    idgrupo_padre integer,
    nivel integer NOT NULL
);


ALTER TABLE public.ctrl_grupo OWNER TO sip;

--
-- TOC entry 165 (class 1259 OID 37054)
-- Dependencies: 3
-- Name: ctrl_grupo_puerta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_grupo_puerta (
    idgrupo integer NOT NULL,
    idpuerta integer NOT NULL
);


ALTER TABLE public.ctrl_grupo_puerta OWNER TO sip;

--
-- TOC entry 166 (class 1259 OID 37057)
-- Dependencies: 3
-- Name: ctrl_lectora_idlectora_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_lectora_idlectora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_lectora_idlectora_seq OWNER TO sip;

--
-- TOC entry 2532 (class 0 OID 0)
-- Dependencies: 166
-- Name: ctrl_lectora_idlectora_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_lectora_idlectora_seq', 1, true);


--
-- TOC entry 167 (class 1259 OID 37059)
-- Dependencies: 2038 2039 3
-- Name: ctrl_lectora; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_lectora (
    idlectora integer DEFAULT nextval('ctrl_lectora_idlectora_seq'::regclass) NOT NULL,
    idcontroladora integer NOT NULL,
    idpuerta integer NOT NULL,
    numero integer NOT NULL,
    canal integer NOT NULL,
    estado integer DEFAULT 0 NOT NULL,
    nombre character varying(45) NOT NULL,
    descripcion character varying(255)
);


ALTER TABLE public.ctrl_lectora OWNER TO sip;

--
-- TOC entry 168 (class 1259 OID 37064)
-- Dependencies: 3
-- Name: ctrl_logacceso_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_logacceso_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_logacceso_id_seq OWNER TO sip;

--
-- TOC entry 2533 (class 0 OID 0)
-- Dependencies: 168
-- Name: ctrl_logacceso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_logacceso_id_seq', 1, true);


--
-- TOC entry 169 (class 1259 OID 37066)
-- Dependencies: 2040 2041 3
-- Name: ctrl_logacceso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_logacceso (
    id integer DEFAULT nextval('ctrl_logacceso_id_seq'::regclass) NOT NULL,
    dirnodofuente integer NOT NULL,
    nodolectora integer NOT NULL,
    numeropuerta integer NOT NULL,
    revision integer NOT NULL,
    indiceusuario integer NOT NULL,
    valortransferido integer NOT NULL,
    numerotarjeta integer NOT NULL,
    numeroaltotarjeta integer NOT NULL,
    numerobajotarjeta integer NOT NULL,
    codigofuncion integer NOT NULL,
    subcodigo integer NOT NULL,
    subfuncion integer NOT NULL,
    codigoext integer NOT NULL,
    nivelusuario integer NOT NULL,
    anio integer NOT NULL,
    dia integer NOT NULL,
    mes integer NOT NULL,
    diasemana integer NOT NULL,
    hora integer,
    minuto integer,
    segundo integer,
    log text NOT NULL,
    fcreado timestamp without time zone DEFAULT now() NOT NULL,
    idsincronizar integer
);


ALTER TABLE public.ctrl_logacceso OWNER TO sip;

--
-- TOC entry 170 (class 1259 OID 37074)
-- Dependencies: 3
-- Name: ctrl_puerta_idpuerta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_puerta_idpuerta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_puerta_idpuerta_seq OWNER TO sip;

--
-- TOC entry 2534 (class 0 OID 0)
-- Dependencies: 170
-- Name: ctrl_puerta_idpuerta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_puerta_idpuerta_seq', 1, true);


--
-- TOC entry 171 (class 1259 OID 37076)
-- Dependencies: 2042 3
-- Name: ctrl_puerta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_puerta (
    idpuerta integer DEFAULT nextval('ctrl_puerta_idpuerta_seq'::regclass) NOT NULL,
    numero character varying(5) NOT NULL,
    puerta character varying(100) NOT NULL,
    puerta_entrada integer
);


ALTER TABLE public.ctrl_puerta OWNER TO sip;

--
-- TOC entry 172 (class 1259 OID 37080)
-- Dependencies: 3
-- Name: ctrl_sincronizar_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_sincronizar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_sincronizar_id_seq OWNER TO sip;

--
-- TOC entry 2535 (class 0 OID 0)
-- Dependencies: 172
-- Name: ctrl_sincronizar_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_sincronizar_id_seq', 21, true);


--
-- TOC entry 173 (class 1259 OID 37082)
-- Dependencies: 2043 3
-- Name: ctrl_sincronizar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_sincronizar (
    id integer DEFAULT nextval('ctrl_sincronizar_id_seq'::regclass) NOT NULL,
    item character varying(100) NOT NULL,
    accion character varying(25) NOT NULL,
    codigoaccion integer NOT NULL,
    nodo integer NOT NULL,
    ipv4 character varying(15) NOT NULL,
    puerto integer NOT NULL,
    prioridad integer NOT NULL,
    param1 character varying(50),
    param2 character varying(50),
    param3 character varying(50),
    param4 character varying(50),
    param5 character varying(50),
    param6 character varying(50),
    param7 character varying(50),
    param8 character varying(50),
    param9 character varying(50),
    param10 character varying(50),
    param11 character varying(50),
    param12 character varying(50),
    param13 character varying(50),
    param14 character varying(50),
    param15 character varying(50),
    param16 character varying(50),
    param17 character varying(50),
    param18 character varying(50),
    param19 character varying(50),
    param20 character varying(50),
    param21 character varying(50),
    param22 character varying(50),
    param23 character varying(50),
    param24 character varying(50),
    param25 character varying(50),
    param26 character varying(50),
    param27 character varying(50),
    param28 character varying(50),
    param29 character varying(50),
    param30 character varying(50),
    param31 character varying(50),
    param32 character varying(50),
    param33 character varying(50),
    param34 character varying(50),
    param35 character varying(50),
    param36 character varying(50),
    param37 character varying(50),
    param38 character varying(50),
    param39 character varying(50),
    param40 character varying(50)
);


ALTER TABLE public.ctrl_sincronizar OWNER TO sip;

--
-- TOC entry 174 (class 1259 OID 37089)
-- Dependencies: 3
-- Name: ctrl_tipo_controladora_idtipo_controladora_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_tipo_controladora_idtipo_controladora_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_tipo_controladora_idtipo_controladora_seq OWNER TO sip;

--
-- TOC entry 2536 (class 0 OID 0)
-- Dependencies: 174
-- Name: ctrl_tipo_controladora_idtipo_controladora_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_tipo_controladora_idtipo_controladora_seq', 2, true);


--
-- TOC entry 175 (class 1259 OID 37091)
-- Dependencies: 2044 3
-- Name: ctrl_tipo_controladora; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_tipo_controladora (
    idtipo_controladora integer DEFAULT nextval('ctrl_tipo_controladora_idtipo_controladora_seq'::regclass) NOT NULL,
    tipo_controladora character varying(45) NOT NULL
);


ALTER TABLE public.ctrl_tipo_controladora OWNER TO sip;

--
-- TOC entry 176 (class 1259 OID 37095)
-- Dependencies: 3
-- Name: ctrl_usuario_idusuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ctrl_usuario_idusuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ctrl_usuario_idusuario_seq OWNER TO sip;

--
-- TOC entry 2537 (class 0 OID 0)
-- Dependencies: 176
-- Name: ctrl_usuario_idusuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ctrl_usuario_idusuario_seq', 1, true);


--
-- TOC entry 177 (class 1259 OID 37097)
-- Dependencies: 2045 2046 2047 2048 2049 2050 2051 2052 2053 2054 3
-- Name: ctrl_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_usuario (
    idusuario integer DEFAULT nextval('ctrl_usuario_idusuario_seq'::regclass) NOT NULL,
    idgrupo integer NOT NULL,
    idzonadetiempo integer NOT NULL,
    iddepartamento integer,
    numero integer NOT NULL,
    tipo_tarjeta character varying(9) NOT NULL,
    codigo_pin integer,
    codigo_id character varying(10) NOT NULL,
    codigo_sitio character varying(5) NOT NULL,
    codigo_tarjeta character varying(5) NOT NULL,
    tarjetayopin integer DEFAULT 1 NOT NULL,
    tienefechalimite integer DEFAULT 0 NOT NULL,
    fechahasta date,
    rondasguardias integer DEFAULT 0 NOT NULL,
    skipfpcheck integer DEFAULT 0 NOT NULL,
    modificable integer DEFAULT 0 NOT NULL,
    nivel integer DEFAULT 0 NOT NULL,
    antipassback integer DEFAULT 0 NOT NULL,
    cedula character varying(8),
    numero_visitante integer,
    nombres character varying(50),
    apellidos character varying(50),
    sexo character varying(1),
    fecha_nacimiento date,
    telefono character varying(18),
    telefono_celular character varying(18),
    direccion character varying(255),
    email character varying(45),
    CONSTRAINT ctrl_usuario_sexo_check CHECK (((sexo)::text = ANY ((ARRAY['F'::character varying, 'M'::character varying])::text[]))),
    CONSTRAINT ctrl_usuario_tipo_tarjeta_check CHECK (((tipo_tarjeta)::text = ANY ((ARRAY['empleado'::character varying, 'visitante'::character varying])::text[])))
);


ALTER TABLE public.ctrl_usuario OWNER TO sip;

--
-- TOC entry 178 (class 1259 OID 37110)
-- Dependencies: 2055 2056 2057 2058 2059 2060 2061 2062 2063 2064 2065 2066 2067 2068 2069 2070 3
-- Name: ctrl_zonadetiempo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ctrl_zonadetiempo (
    indice integer NOT NULL,
    descripcion character varying(200) NOT NULL,
    disponibleenferiado boolean DEFAULT false NOT NULL,
    nivel integer DEFAULT 0 NOT NULL,
    indicelink integer,
    domingodesde integer DEFAULT 0 NOT NULL,
    domingohasta integer DEFAULT 0 NOT NULL,
    lunesdesde integer DEFAULT 0 NOT NULL,
    luneshasta integer DEFAULT 0 NOT NULL,
    martesdesde integer DEFAULT 0 NOT NULL,
    marteshasta integer DEFAULT 0 NOT NULL,
    miercolesdesde integer DEFAULT 0 NOT NULL,
    miercoleshasta integer DEFAULT 0 NOT NULL,
    juevesdesde integer DEFAULT 0 NOT NULL,
    jueveshasta integer DEFAULT 0 NOT NULL,
    viernesdesde integer DEFAULT 0 NOT NULL,
    vierneshasta integer DEFAULT 0 NOT NULL,
    sabadodesde integer DEFAULT 0 NOT NULL,
    sabadohasta integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ctrl_zonadetiempo OWNER TO sip;

--
-- TOC entry 179 (class 1259 OID 37129)
-- Dependencies: 3
-- Name: funcion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE funcion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.funcion_id_seq OWNER TO sip;

--
-- TOC entry 2538 (class 0 OID 0)
-- Dependencies: 179
-- Name: funcion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('funcion_id_seq', 152, true);


--
-- TOC entry 180 (class 1259 OID 37131)
-- Dependencies: 2071 3
-- Name: funcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE funcion (
    id integer DEFAULT nextval('funcion_id_seq'::regclass) NOT NULL,
    idfuncion character varying(10) NOT NULL,
    nombre character varying(45),
    descripcion character varying(100),
    mobil integer,
    idcategoria character varying(10)
);


ALTER TABLE public.funcion OWNER TO sip;

--
-- TOC entry 181 (class 1259 OID 37135)
-- Dependencies: 3
-- Name: grupo_idgrupo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE grupo_idgrupo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grupo_idgrupo_seq OWNER TO sip;

--
-- TOC entry 2539 (class 0 OID 0)
-- Dependencies: 181
-- Name: grupo_idgrupo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('grupo_idgrupo_seq', 1, true);


--
-- TOC entry 182 (class 1259 OID 37137)
-- Dependencies: 2072 3
-- Name: grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE grupo (
    idgrupo integer DEFAULT nextval('grupo_idgrupo_seq'::regclass) NOT NULL,
    nombre character varying(50) NOT NULL,
    descripcion character varying(100)
);


ALTER TABLE public.grupo OWNER TO sip;

--
-- TOC entry 183 (class 1259 OID 37141)
-- Dependencies: 3
-- Name: grupo_has_funcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE grupo_has_funcion (
    grupo_idgrupo integer NOT NULL,
    funcion_id integer NOT NULL
);


ALTER TABLE public.grupo_has_funcion OWNER TO sip;

--
-- TOC entry 184 (class 1259 OID 37144)
-- Dependencies: 3
-- Name: plantafisica_idplantafisica_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE plantafisica_idplantafisica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plantafisica_idplantafisica_seq OWNER TO sip;

--
-- TOC entry 2540 (class 0 OID 0)
-- Dependencies: 184
-- Name: plantafisica_idplantafisica_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('plantafisica_idplantafisica_seq', 1, true);


--
-- TOC entry 185 (class 1259 OID 37146)
-- Dependencies: 2073 3
-- Name: plantafisica; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE plantafisica (
    idplantafisica integer DEFAULT nextval('plantafisica_idplantafisica_seq'::regclass) NOT NULL,
    plantafisica character varying(100) NOT NULL,
    file character varying(100) NOT NULL
);


ALTER TABLE public.plantafisica OWNER TO sip;

--
-- TOC entry 186 (class 1259 OID 37150)
-- Dependencies: 3
-- Name: segmento_idsegmento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE segmento_idsegmento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.segmento_idsegmento_seq OWNER TO sip;

--
-- TOC entry 2541 (class 0 OID 0)
-- Dependencies: 186
-- Name: segmento_idsegmento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('segmento_idsegmento_seq', 1, true);


--
-- TOC entry 187 (class 1259 OID 37152)
-- Dependencies: 2074 3
-- Name: segmento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE segmento (
    idsegmento integer DEFAULT nextval('segmento_idsegmento_seq'::regclass) NOT NULL,
    segmento character varying(50) NOT NULL,
    descripcion character varying(100) NOT NULL
);


ALTER TABLE public.segmento OWNER TO sip;

--
-- TOC entry 188 (class 1259 OID 37156)
-- Dependencies: 3
-- Name: segmento_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE segmento_usuario (
    idsegmento integer NOT NULL,
    usuario character varying(12) NOT NULL
);


ALTER TABLE public.segmento_usuario OWNER TO sip;

--
-- TOC entry 189 (class 1259 OID 37159)
-- Dependencies: 3
-- Name: timezone_idtimezone_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE timezone_idtimezone_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.timezone_idtimezone_seq OWNER TO sip;

--
-- TOC entry 2542 (class 0 OID 0)
-- Dependencies: 189
-- Name: timezone_idtimezone_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('timezone_idtimezone_seq', 406, true);


--
-- TOC entry 190 (class 1259 OID 37161)
-- Dependencies: 2075 3
-- Name: timezone; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE timezone (
    idtimezone integer DEFAULT nextval('timezone_idtimezone_seq'::regclass) NOT NULL,
    location character varying(100) NOT NULL,
    standard_time character varying(10) NOT NULL,
    summer_time character varying(10) NOT NULL
);


ALTER TABLE public.timezone OWNER TO sip;

--
-- TOC entry 191 (class 1259 OID 37165)
-- Dependencies: 2076 2077 3
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    usuario character varying(12) NOT NULL,
    clave character varying(40) NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    fcreado timestamp without time zone DEFAULT now() NOT NULL,
    fmodificado timestamp without time zone,
    nota character varying(255),
    "superUsuario" boolean NOT NULL DEFAULT false
);


ALTER TABLE public.usuario OWNER TO sip;

--
-- TOC entry 192 (class 1259 OID 37170)
-- Dependencies: 3
-- Name: usuario_has_grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario_has_grupo (
    usuario_usuario character varying(12) NOT NULL,
    grupo_idgrupo integer NOT NULL
);


ALTER TABLE public.usuario_has_grupo OWNER TO sip;

--
-- TOC entry 193 (class 1259 OID 37173)
-- Dependencies: 3
-- Name: vid_camara_idcamara_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_camara_idcamara_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_camara_idcamara_seq OWNER TO sip;

--
-- TOC entry 2543 (class 0 OID 0)
-- Dependencies: 193
-- Name: vid_camara_idcamara_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_camara_idcamara_seq', 1, true);


--
-- TOC entry 194 (class 1259 OID 37175)
-- Dependencies: 2078 2079 2080 2081 2082 2083 3
-- Name: vid_camara; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_camara (
    idcamara integer DEFAULT nextval('vid_camara_idcamara_seq'::regclass) NOT NULL,
    idservidor character varying(15) NOT NULL,
    idtipo_camara integer NOT NULL,
    camara character varying(100) NOT NULL,
    numero integer NOT NULL,
    prioridad boolean NOT NULL DEFAULT false,
    ipv4 character varying(15) NOT NULL,
    puerto integer DEFAULT 80 NOT NULL,
    cam_usuario character varying(12),
    cam_clave character varying(12),
    cam_usuario_video character varying(12),
    cam_clave_video character varying(12),
    pais character varying(100) NOT NULL,
    estado character varying(100) NOT NULL,
    ciudad character varying(100) NOT NULL,
    avenida character varying(100) NOT NULL,
    edificio character varying(100) NOT NULL,
    piso character varying(100) NOT NULL,
    oficina character varying(100) NOT NULL,
    idplantafisica integer,
    "leftX" integer,
    "topY" integer,
    rotacion integer DEFAULT 0 NOT NULL,
    ffmpeg_timelapse integer DEFAULT 0 NOT NULL,
    ffmpeg_timelapse_mode character varying(13) DEFAULT 'hourly'::character varying NOT NULL,
    CONSTRAINT vid_camara_ffmpeg_timelapse_mode_check CHECK (((ffmpeg_timelapse_mode)::text = ANY ((ARRAY['hourly'::character varying, 'daily'::character varying, 'weekly-sunday'::character varying, 'weekly-monday'::character varying, 'monthly'::character varying, 'manual'::character varying])::text[])))
);


ALTER TABLE public.vid_camara OWNER TO sip;

--
-- TOC entry 195 (class 1259 OID 37187)
-- Dependencies: 3
-- Name: vid_camara_axisguard_idcamaraaxisguard_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_camara_axisguard_idcamaraaxisguard_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_camara_axisguard_idcamaraaxisguard_seq OWNER TO sip;

--
-- TOC entry 2544 (class 0 OID 0)
-- Dependencies: 195
-- Name: vid_camara_axisguard_idcamaraaxisguard_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_camara_axisguard_idcamaraaxisguard_seq', 1, true);


--
-- TOC entry 196 (class 1259 OID 37189)
-- Dependencies: 2084 2085 2086 2087 2088 2089 2090 3
-- Name: vid_camara_axisguard; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_camara_axisguard (
    idcamaraaxisguard integer DEFAULT nextval('vid_camara_axisguard_idcamaraaxisguard_seq'::regclass) NOT NULL,
    idcamara integer NOT NULL,
    guardnbr integer NOT NULL,
    guardname character varying(100) NOT NULL,
    running character varying(3) DEFAULT 'no'::character varying NOT NULL,
    camnbr integer DEFAULT 1 NOT NULL,
    randomenabled character varying(3) DEFAULT 'no'::character varying NOT NULL,
    timebetweensequences integer DEFAULT 0 NOT NULL,
    CONSTRAINT vid_camara_axisguard_randomenabled_check CHECK (((randomenabled)::text = ANY ((ARRAY['no'::character varying, 'yes'::character varying])::text[]))),
    CONSTRAINT vid_camara_axisguard_running_check CHECK (((running)::text = ANY ((ARRAY['no'::character varying, 'yes'::character varying])::text[])))
);


ALTER TABLE public.vid_camara_axisguard OWNER TO sip;

--
-- TOC entry 140 (class 1259 OID 36955)
-- Dependencies: 3
-- Name: vid_camara_axisguard_tour_idvidcamaraaxisguardtour_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_camara_axisguard_tour_idvidcamaraaxisguardtour_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_camara_axisguard_tour_idvidcamaraaxisguardtour_seq OWNER TO sip;

--
-- TOC entry 2545 (class 0 OID 0)
-- Dependencies: 140
-- Name: vid_camara_axisguard_tour_idvidcamaraaxisguardtour_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_camara_axisguard_tour_idvidcamaraaxisguardtour_seq', 1, true);


--
-- TOC entry 141 (class 1259 OID 36964)
-- Dependencies: 2016 2017 2018 2019 2020 3
-- Name: vid_camara_axisguard_tour; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_camara_axisguard_tour (
    idvidcamaraaxisguardtour integer DEFAULT nextval('vid_camara_axisguard_tour_idvidcamaraaxisguardtour_seq'::regclass) NOT NULL,
    idcamaraaxisguard integer NOT NULL,
    idcamaraaxispresetpos integer NOT NULL,
    tournbr integer NOT NULL,
    "position" integer NOT NULL,
    movespeed integer DEFAULT 5 NOT NULL,
    waittime integer DEFAULT 1 NOT NULL,
    "WaitTimeViewType" character varying(7) DEFAULT 'seconds'::character varying NOT NULL,
    CONSTRAINT "vid_camara_axisguard_tour_WaitTimeViewType_check" CHECK ((("WaitTimeViewType")::text = ANY ((ARRAY['seconds'::character varying, 'minutes'::character varying])::text[])))
);


ALTER TABLE public.vid_camara_axisguard_tour OWNER TO sip;

--
-- TOC entry 197 (class 1259 OID 37199)
-- Dependencies: 3
-- Name: vid_camara_axispresetpos_idcamaraaxispresetpos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_camara_axispresetpos_idcamaraaxispresetpos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_camara_axispresetpos_idcamaraaxispresetpos_seq OWNER TO sip;

--
-- TOC entry 2546 (class 0 OID 0)
-- Dependencies: 197
-- Name: vid_camara_axispresetpos_idcamaraaxispresetpos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_camara_axispresetpos_idcamaraaxispresetpos_seq', 1, true);


--
-- TOC entry 198 (class 1259 OID 37201)
-- Dependencies: 2091 3
-- Name: vid_camara_axispresetpos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_camara_axispresetpos (
    idcamaraaxispresetpos integer DEFAULT nextval('vid_camara_axispresetpos_idcamaraaxispresetpos_seq'::regclass) NOT NULL,
    idcamara integer NOT NULL,
    presetnbr integer NOT NULL,
    presetname character varying(100) NOT NULL
);


ALTER TABLE public.vid_camara_axispresetpos OWNER TO sip;

--
-- TOC entry 199 (class 1259 OID 37205)
-- Dependencies: 3
-- Name: vid_camara_horario_idcamarahorario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_camara_horario_idcamarahorario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_camara_horario_idcamarahorario_seq OWNER TO sip;

--
-- TOC entry 2547 (class 0 OID 0)
-- Dependencies: 199
-- Name: vid_camara_horario_idcamarahorario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_camara_horario_idcamarahorario_seq', 1, true);


--
-- TOC entry 200 (class 1259 OID 37207)
-- Dependencies: 2092 3
-- Name: vid_camara_horario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_camara_horario (
    idcamarahorario integer DEFAULT nextval('vid_camara_horario_idcamarahorario_seq'::regclass) NOT NULL,
    idcamara integer NOT NULL,
    hora_desde character varying(4) NOT NULL,
    hora_hasta character varying(4) NOT NULL,
    dias character varying(15) NOT NULL
);


ALTER TABLE public.vid_camara_horario OWNER TO sip;

--
-- TOC entry 201 (class 1259 OID 37211)
-- Dependencies: 3
-- Name: vid_evento_idevento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_evento_idevento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_evento_idevento_seq OWNER TO sip;

--
-- TOC entry 2548 (class 0 OID 0)
-- Dependencies: 201
-- Name: vid_evento_idevento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_evento_idevento_seq', 1, true);


--
-- TOC entry 202 (class 1259 OID 37213)
-- Dependencies: 2093 3
-- Name: vid_evento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_evento (
    idevento integer DEFAULT nextval('vid_evento_idevento_seq'::regclass) NOT NULL,
    idservidor character varying(15),
    servidor character varying(15) NOT NULL,
    camara character varying(3) NOT NULL,
    evento character varying(50) NOT NULL,
    descripcion character varying(100) NOT NULL,
    file character varying(100),
    fecha timestamp without time zone NOT NULL,
    status boolean NOT NULL DEFAULT false,
    observacion text
);


ALTER TABLE public.vid_evento OWNER TO sip;

--
-- TOC entry 203 (class 1259 OID 37220)
-- Dependencies: 3
-- Name: vid_marca_camara_idmarca_camara_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_marca_camara_idmarca_camara_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_marca_camara_idmarca_camara_seq OWNER TO sip;

--
-- TOC entry 2549 (class 0 OID 0)
-- Dependencies: 203
-- Name: vid_marca_camara_idmarca_camara_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_marca_camara_idmarca_camara_seq', 13, true);


--
-- TOC entry 204 (class 1259 OID 37222)
-- Dependencies: 2094 3
-- Name: vid_marca_camara; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_marca_camara (
    idmarca_camara integer DEFAULT nextval('vid_marca_camara_idmarca_camara_seq'::regclass) NOT NULL,
    nombre_marca_camara character varying(100) NOT NULL
);


ALTER TABLE public.vid_marca_camara OWNER TO sip;

--
-- TOC entry 205 (class 1259 OID 37226)
-- Dependencies: 3
-- Name: vid_modelo_camara_idmodelo_camara_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_modelo_camara_idmodelo_camara_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_modelo_camara_idmodelo_camara_seq OWNER TO sip;

--
-- TOC entry 2550 (class 0 OID 0)
-- Dependencies: 205
-- Name: vid_modelo_camara_idmodelo_camara_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_modelo_camara_idmodelo_camara_seq', 10, true);


--
-- TOC entry 206 (class 1259 OID 37228)
-- Dependencies: 2095 3
-- Name: vid_modelo_camara; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_modelo_camara (
    idmodelo_camara integer DEFAULT nextval('vid_modelo_camara_idmodelo_camara_seq'::regclass) NOT NULL,
    idmarca_camara integer NOT NULL,
    nombre_modelo_camara character varying(100) NOT NULL
);


ALTER TABLE public.vid_modelo_camara OWNER TO sip;

--
-- TOC entry 207 (class 1259 OID 37232)
-- Dependencies: 2096 2097 2098 2099 2100 2101 2102 2103 2104 2105 2106 2107 2108 2109 2110 2111 2112 2113 2114 2115 2116 2117 2118 2119 2120 2121 2122 2123 2124 2125 2126 2127 2128 2129 2130 2131 2132 2133 2134 2135 2136 2137 2138 2139 2140 2141 2142 2143 2144 2145 2146 2147 2148 2149 2150 2151 2152 2153 2154 2155 2156 2157 2158 2159 2160 2161 2162 2163 2164 2165 2166 2167 2168 2169 2170 2171 2172 2173 2174 2175 2176 2177 2178 2179 2180 2181 2182 2183 2184 2185 2186 2187 2188 2189 2190 2191 2192 2193 2194 2195 2196 2197 2198 2199 2200 2201 2202 2203 2204 2205 2206 2207 2208 2209 2210 2211 2212 2213 2214 2215 2216 2217 2218 3
-- Name: vid_motionconf; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_motionconf (
    idservidor character varying(15) NOT NULL,
    ipv4 character varying(15) NOT NULL,
    daemon character varying(3) DEFAULT 'on'::character varying NOT NULL,
    process_id_file character varying(50) DEFAULT 'motion.pid'::character varying NOT NULL,
    setup_mode character varying(3) DEFAULT 'off'::character varying NOT NULL,
    videodevice character varying(50) DEFAULT '/dev/video0'::character varying NOT NULL,
    tunerdevice character varying(50) DEFAULT '/dev/tuner0'::character varying NOT NULL,
    input integer DEFAULT 8 NOT NULL,
    norm character varying(1) DEFAULT '0'::character varying NOT NULL,
    frequency integer DEFAULT 0 NOT NULL,
    rotate character varying(3) DEFAULT '0'::character varying NOT NULL,
    width integer DEFAULT 352 NOT NULL,
    height integer DEFAULT 288 NOT NULL,
    framerate integer DEFAULT 100 NOT NULL,
    minimum_frame_time integer DEFAULT 0 NOT NULL,
    netcam_url character varying(150) DEFAULT 'value'::character varying NOT NULL,
    netcam_userpass character varying(50) DEFAULT 'user:password'::character varying NOT NULL,
    netcam_proxy character varying(150) DEFAULT 'http://myproxy:1234'::character varying NOT NULL,
    auto_brightness character varying(3) DEFAULT 'off'::character varying NOT NULL,
    brightness integer DEFAULT 30 NOT NULL,
    contrast integer DEFAULT 0 NOT NULL,
    saturation integer DEFAULT 0 NOT NULL,
    hue integer DEFAULT 0 NOT NULL,
    roundrobin_frames smallint DEFAULT 1 NOT NULL,
    roundrobin_skip smallint DEFAULT 1 NOT NULL,
    switchfilter character varying(3) DEFAULT 'off'::character varying NOT NULL,
    threshold integer DEFAULT 1500 NOT NULL,
    threshold_tune character varying(3) DEFAULT 'off'::character varying NOT NULL,
    noise_level integer DEFAULT 32 NOT NULL,
    noise_tune character varying(3) DEFAULT 'on'::character varying NOT NULL,
    night_compensate character varying(3) DEFAULT 'off'::character varying NOT NULL,
    despeckle character varying(10) DEFAULT 'EedDl'::character varying NOT NULL,
    mask_file character varying(50),
    smart_mask_speed character varying(2) DEFAULT '0'::character varying NOT NULL,
    lightswitch integer DEFAULT 0 NOT NULL,
    minimum_motion_frames integer DEFAULT 1 NOT NULL,
    pre_capture integer DEFAULT 0 NOT NULL,
    post_capture integer DEFAULT 0 NOT NULL,
    gap integer DEFAULT 60 NOT NULL,
    max_mpeg_time integer DEFAULT 3600 NOT NULL,
    low_cpu integer DEFAULT 1 NOT NULL,
    output_all character varying(3) DEFAULT 'off'::character varying NOT NULL,
    output_normal character varying(5) DEFAULT 'first'::character varying NOT NULL,
    output_motion character varying(3) DEFAULT 'off'::character varying NOT NULL,
    quality integer DEFAULT 75 NOT NULL,
    ppm character varying(3) DEFAULT 'off'::character varying NOT NULL,
    ffmpeg_cap_new character varying(3) DEFAULT 'on'::character varying NOT NULL,
    ffmpeg_cap_motion character varying(3) DEFAULT 'off'::character varying NOT NULL,
    ffmpeg_timelapse integer DEFAULT 0 NOT NULL,
    ffmpeg_timelapse_mode character varying(13) DEFAULT 'hourly'::character varying NOT NULL,
    ffmpeg_bps integer DEFAULT 500000 NOT NULL,
    ffmpeg_variable_bitrate integer DEFAULT 0 NOT NULL,
    ffmpeg_video_codec character varying(15) DEFAULT 'flv'::character varying NOT NULL,
    ffmpeg_deinterlace character varying(3) DEFAULT 'off'::character varying NOT NULL,
    snapshot_interval integer DEFAULT 0 NOT NULL,
    locate character varying(7) DEFAULT 'off'::character varying NOT NULL,
    text_right character varying(50) DEFAULT '%Y-%m-%d\\n%T-%q'::character varying NOT NULL,
    text_left character varying(50) DEFAULT 'CENIT - DIDI - CAMARA %t'::character varying NOT NULL,
    text_changes character varying(3) DEFAULT 'off'::character varying NOT NULL,
    text_event character varying(30) DEFAULT '%Y%m%d%H%M%S'::character varying NOT NULL,
    text_double character varying(3) DEFAULT 'off'::character varying NOT NULL,
    target_dir character varying(50) DEFAULT '/home/sip/motion/camara%t '::character varying NOT NULL,
    snapshot_filename character varying(50) DEFAULT '%v-%Y%m%d%H%M%S-snapshot'::character varying NOT NULL,
    jpeg_filename character varying(50) DEFAULT '%v-%Y%m%d%H%M%S-%q'::character varying NOT NULL,
    movie_filename character varying(50) DEFAULT '%v-%Y%m%d%H%M%S'::character varying NOT NULL,
    timelapse_filename character varying(50) DEFAULT '%Y%m%d-timelapse'::character varying NOT NULL,
    webcam_port integer DEFAULT 8081 NOT NULL,
    webcam_quality integer DEFAULT 50 NOT NULL,
    webcam_motion character varying(3) DEFAULT 'off'::character varying NOT NULL,
    webcam_maxrate integer DEFAULT 1 NOT NULL,
    webcam_localhost character varying(3) DEFAULT 'off'::character varying NOT NULL,
    webcam_limit integer DEFAULT 0 NOT NULL,
    control_port integer DEFAULT 8080 NOT NULL,
    control_localhost character varying(3) DEFAULT 'off'::character varying NOT NULL,
    control_html_output character varying(3) DEFAULT 'on'::character varying NOT NULL,
    control_authentication character varying(50) DEFAULT 'admin:admin'::character varying NOT NULL,
    track_type character varying(1) DEFAULT '0'::character varying NOT NULL,
    track_auto character varying(3) DEFAULT 'off'::character varying NOT NULL,
    track_port character varying(10) DEFAULT 'ttyS5'::character varying NOT NULL,
    track_motorx integer DEFAULT (-1) NOT NULL,
    track_motory integer DEFAULT (-1) NOT NULL,
    track_maxx integer DEFAULT 0 NOT NULL,
    track_maxy integer DEFAULT 0 NOT NULL,
    track_iomojo_id integer DEFAULT 0 NOT NULL,
    track_step_angle_x integer DEFAULT 10 NOT NULL,
    track_step_angle_y integer DEFAULT 10 NOT NULL,
    track_move_wait integer DEFAULT 10 NOT NULL,
    track_speed integer DEFAULT 255 NOT NULL,
    track_stepsize integer DEFAULT 40 NOT NULL,
    quiet character varying(3) DEFAULT 'off'::character varying NOT NULL,
    on_event_start character varying(100) DEFAULT '/home/sip/event_start.sh %t %Y-%m-%d_%H:%M:%S %C'::character varying NOT NULL,
    on_event_end character varying(100) DEFAULT '/home/sip/event_end.sh %t %Y-%m-%d_%H:%M:%S %C'::character varying NOT NULL,
    on_picture_save character varying(100) DEFAULT '/home/sip/picture_save.sh %t %Y-%m-%d_%H:%M:%S %C'::character varying NOT NULL,
    on_motion_detected character varying(100) DEFAULT '/home/sip/motion_detected.sh %t %Y-%m-%d_%H:%M:%S %C'::character varying NOT NULL,
    on_movie_start character varying(100) DEFAULT '/home/sip/movie_start.sh %t %Y-%m-%d %T %C %f'::character varying NOT NULL,
    on_movie_end character varying(100) DEFAULT '/home/sip/movie_end.sh %t %Y-%m-%d_%T %C %f'::character varying NOT NULL,
    video_pipe character varying(15) DEFAULT 'value'::character varying NOT NULL,
    motion_video_pipe character varying(15) DEFAULT 'value'::character varying NOT NULL,
    CONSTRAINT vid_motionconf_auto_brightness_check CHECK (((auto_brightness)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_control_html_output_check CHECK (((control_html_output)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_control_localhost_check CHECK (((control_localhost)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_daemon_check CHECK (((daemon)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_ffmpeg_cap_motion_check CHECK (((ffmpeg_cap_motion)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_ffmpeg_cap_new_check CHECK (((ffmpeg_cap_new)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_ffmpeg_deinterlace_check CHECK (((ffmpeg_deinterlace)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_ffmpeg_timelapse_mode_check CHECK (((ffmpeg_timelapse_mode)::text = ANY ((ARRAY['hourly'::character varying, 'daily'::character varying, 'weekly-sunday'::character varying, 'weekly-monday'::character varying, 'monthly'::character varying, 'manual'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_locate_check CHECK (((locate)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying, 'preview'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_night_compensate_check CHECK (((night_compensate)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_noise_tune_check CHECK (((noise_tune)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_norm_check CHECK (((norm)::text = ANY ((ARRAY['0'::character varying, '1'::character varying, '2'::character varying, '3'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_output_all_check CHECK (((output_all)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_output_motion_check CHECK (((output_motion)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_output_normal_check CHECK (((output_normal)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying, 'first'::character varying, 'best'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_ppm_check CHECK (((ppm)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_quiet_check CHECK (((quiet)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_rotate_check CHECK (((rotate)::text = ANY ((ARRAY['0'::character varying, '90'::character varying, '180'::character varying, '270'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_setup_mode_check CHECK (((setup_mode)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_smart_mask_speed_check CHECK (((smart_mask_speed)::text = ANY ((ARRAY['0'::character varying, '1'::character varying, '2'::character varying, '3'::character varying, '4'::character varying, '5'::character varying, '6'::character varying, '7'::character varying, '8'::character varying, '9'::character varying, '10'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_switchfilter_check CHECK (((switchfilter)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_text_changes_check CHECK (((text_changes)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_text_double_check CHECK (((text_double)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_threshold_tune_check CHECK (((threshold_tune)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_track_auto_check CHECK (((track_auto)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_track_type_check CHECK (((track_type)::text = ANY ((ARRAY['0'::character varying, '1'::character varying, '2'::character varying, '3'::character varying, '4'::character varying, '5'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_webcam_localhost_check CHECK (((webcam_localhost)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[]))),
    CONSTRAINT vid_motionconf_webcam_motion_check CHECK (((webcam_motion)::text = ANY ((ARRAY['on'::character varying, 'off'::character varying])::text[])))
);


ALTER TABLE public.vid_motionconf OWNER TO sip;

--
-- TOC entry 208 (class 1259 OID 37361)
-- Dependencies: 3
-- Name: vid_plantafisica_zona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_plantafisica_zona (
    idplantafisica integer NOT NULL,
    idzona integer NOT NULL,
    "leftX" integer NOT NULL,
    "topY" integer NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    bgcolor character varying(8) NOT NULL
);


ALTER TABLE public.vid_plantafisica_zona OWNER TO sip;

--
-- TOC entry 209 (class 1259 OID 37364)
-- Dependencies: 2219 2220 2221 2222 3
-- Name: vid_servidor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_servidor (
    idservidor character varying(15) NOT NULL,
    idsegmento integer NOT NULL,
    computador character varying(15) NOT NULL,
    tiposervidor character varying(20) DEFAULT 'motion'::character varying NOT NULL,
    ipv4 character varying(15) NOT NULL,
    enlinea boolean DEFAULT false NOT NULL,
    idtimezone integer DEFAULT 83 NOT NULL,
    so_usuario character varying(12) NOT NULL,
    so_clave character varying(12) NOT NULL,
    conexioncamaras boolean DEFAULT true NOT NULL,
    pais character varying(100),
    estado character varying(100),
    ciudad character varying(100),
    avenida character varying(100),
    edificio character varying(100),
    piso character varying(100),
    oficina character varying(100)
);


ALTER TABLE public.vid_servidor OWNER TO sip;

--
-- TOC entry 210 (class 1259 OID 37374)
-- Dependencies: 3
-- Name: vid_tipo_camara_idtipo_camara_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_tipo_camara_idtipo_camara_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_tipo_camara_idtipo_camara_seq OWNER TO sip;

--
-- TOC entry 2551 (class 0 OID 0)
-- Dependencies: 210
-- Name: vid_tipo_camara_idtipo_camara_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_tipo_camara_idtipo_camara_seq', 5, true);


--
-- TOC entry 211 (class 1259 OID 37376)
-- Dependencies: 2223 3
-- Name: vid_tipo_camara; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_tipo_camara (
    idtipo_camara integer DEFAULT nextval('vid_tipo_camara_idtipo_camara_seq'::regclass) NOT NULL,
    idmodelo_camara integer NOT NULL,
    nombre_tipo_camara character varying(100) NOT NULL,
    ptz boolean NOT NULL,
    urlfoto character varying(255) NOT NULL,
    urlvideo character varying(255) NOT NULL,
    descripcion_tipo_camara text
);


ALTER TABLE public.vid_tipo_camara OWNER TO sip;

--
-- TOC entry 212 (class 1259 OID 37383)
-- Dependencies: 3
-- Name: vid_zona_idzona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vid_zona_idzona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vid_zona_idzona_seq OWNER TO sip;

--
-- TOC entry 2552 (class 0 OID 0)
-- Dependencies: 212
-- Name: vid_zona_idzona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vid_zona_idzona_seq', 1, true);


--
-- TOC entry 213 (class 1259 OID 37385)
-- Dependencies: 2224 3
-- Name: vid_zona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_zona (
    idzona integer DEFAULT nextval('vid_zona_idzona_seq'::regclass) NOT NULL,
    zona character varying(50) NOT NULL,
    descripcion character varying(100)
);


ALTER TABLE public.vid_zona OWNER TO sip;

--
-- TOC entry 214 (class 1259 OID 37389)
-- Dependencies: 3
-- Name: vid_zona_camara; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_zona_camara (
    idzona integer NOT NULL,
    idcamara integer NOT NULL
);


ALTER TABLE public.vid_zona_camara OWNER TO sip;

--
-- TOC entry 215 (class 1259 OID 37392)
-- Dependencies: 3
-- Name: vid_zona_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vid_zona_usuario (
    idzona integer NOT NULL,
    usuario character varying(12) NOT NULL
);


ALTER TABLE public.vid_zona_usuario OWNER TO sip;

--
-- TOC entry 2473 (class 0 OID 36974)
-- Dependencies: 143
-- Data for Name: accesopc; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2474 (class 0 OID 36978)
-- Dependencies: 144
-- Data for Name: accesopc_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO accesopc_tipo VALUES ('CarnetsEmp', '/sip/carempleado/', 'Generación de Carnets de Empleados');
INSERT INTO accesopc_tipo VALUES ('CarnetsVis', '/sip/carvisitante/', 'Generación de Carnets para Visitantes');
INSERT INTO accesopc_tipo VALUES ('CtrlAcceso', '/sip/ctrlmonitoracceso/', 'Monitor de Control de Accesos');
INSERT INTO accesopc_tipo VALUES ('CtrlAsistencia', '/sip/ctrlmonitorasistencia/', 'Monitor de Control de Asistencia');
INSERT INTO accesopc_tipo VALUES ('MonitorVid', '/sip/monitorremoto/', 'Monitor Remoto de Video');
INSERT INTO accesopc_tipo VALUES ('Visitantes', '/sip/visitantes/', 'Registro de Visitantes');


--
-- TOC entry 2475 (class 0 OID 36983)
-- Dependencies: 146
-- Data for Name: car_cargo; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2476 (class 0 OID 36989)
-- Dependencies: 148
-- Data for Name: car_departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2477 (class 0 OID 36995)
-- Dependencies: 150
-- Data for Name: car_empleado; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2478 (class 0 OID 37001)
-- Dependencies: 152
-- Data for Name: car_empleado_impresion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2479 (class 0 OID 37008)
-- Dependencies: 154
-- Data for Name: car_plantilla; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2480 (class 0 OID 37019)
-- Dependencies: 156
-- Data for Name: car_visitante; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2481 (class 0 OID 37025)
-- Dependencies: 158
-- Data for Name: car_visitante_impresion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2482 (class 0 OID 37030)
-- Dependencies: 159
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categoria VALUES ('ADM', 'Administración', NULL, NULL);
INSERT INTO categoria VALUES ('ADMACCPC', 'Acceso PC', NULL, 'ADM');
INSERT INTO categoria VALUES ('ADMGPR', 'Grupos de Permisos Usr', NULL, 'ADM');
INSERT INTO categoria VALUES ('ADMPLT', 'Planta Física', NULL, 'ADM');
INSERT INTO categoria VALUES ('ADMSEG', 'Segmentos', NULL, 'ADM');
INSERT INTO categoria VALUES ('ADMUSR', 'Uruarios de Sistema', NULL, 'ADM');
INSERT INTO categoria VALUES ('CAR', 'Carnetización ', NULL, NULL);
INSERT INTO categoria VALUES ('CARCRG', 'Cargos', NULL, 'CAR');
INSERT INTO categoria VALUES ('CARDPT', 'Departamentos', NULL, 'CAR');
INSERT INTO categoria VALUES ('CAREMP', 'Carnets Empleados', 'Generacion de carnet a los empleados', 'CAR');
INSERT INTO categoria VALUES ('CARLISTEMP', 'Listado Carnets Empleados', NULL, 'CAR');
INSERT INTO categoria VALUES ('CARPLT', 'Plantilla', NULL, 'CAR');
INSERT INTO categoria VALUES ('CARVIS', 'Carnets Visitantes', NULL, 'CAR');
INSERT INTO categoria VALUES ('CTRL', 'Control de Acceso', NULL, NULL);
INSERT INTO categoria VALUES ('CTRLCTRL', 'Controladoras de Acceso', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLDS', 'Días Festivos', 'Días Feriados', 'CTRL');
INSERT INTO categoria VALUES ('CTRLGPR', 'Grupos de Acceso', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLHRS', 'Horarios de Trabajo', 'Time Zones - Zonas de tiempo', 'CTRL');
INSERT INTO categoria VALUES ('CTRLLEC', 'Lectoras de Acceso', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLLOG', 'Listado de Eventos', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLLOGA', 'Listado de Asistencia', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLMON', 'Monitor Acceso', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLMONA', 'Monitor Asistencia', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLPTS', 'Puertas de Acceso', NULL, 'CTRL');
INSERT INTO categoria VALUES ('CTRLUSR', 'Usuarios Tarjetahabientes', NULL, 'CTRL');
INSERT INTO categoria VALUES ('VID', 'Video Vigilancia', NULL, NULL);
INSERT INTO categoria VALUES ('VIDACCPC', 'Acceso Monitoreo', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDALARM', 'Listado de Alarmas', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDASCMNTR', 'Asociar Monitoreo', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDCAM', 'Cámaras', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDCAMMOD', 'Modelos de Cámaras', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDCAMMRC', 'Marcas de Cámaras', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDCAMTIP', 'Tipos de Cámaras', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDEXCAM', 'Exportar Camaras', 'Exportar Camaras para mobil', 'VID');
INSERT INTO categoria VALUES ('VIDGRD', 'Guardias de cámaras', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDMNTR', 'Monitor Remoto', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDSRV', 'Grabadores de Video', NULL, 'VID');
INSERT INTO categoria VALUES ('VIDZN', 'Zonas de Monitoreo', NULL, 'VID');


--
-- TOC entry 2483 (class 0 OID 37035)
-- Dependencies: 161
-- Data for Name: ctrl_controladora; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2484 (class 0 OID 37045)
-- Dependencies: 162
-- Data for Name: ctrl_diafestivo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ctrl_diafestivo VALUES (0, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (1, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (2, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (3, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (4, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (5, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (6, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (7, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (8, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (9, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (10, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (11, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (12, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (13, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (14, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (15, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (16, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (17, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (18, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (19, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (20, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (21, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (22, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (23, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (24, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (25, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (26, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (27, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (28, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (29, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (30, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (31, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (32, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (33, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (34, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (35, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (36, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (37, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (38, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (39, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (40, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (41, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (42, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (43, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (44, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (45, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (46, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (47, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (48, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (49, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (50, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (51, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (52, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (53, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (54, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (55, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (56, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (57, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (58, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (59, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (60, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (61, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (62, '-', 0, 0);
INSERT INTO ctrl_diafestivo VALUES (63, '-', 0, 0);


--
-- TOC entry 2485 (class 0 OID 37050)
-- Dependencies: 164
-- Data for Name: ctrl_grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2486 (class 0 OID 37054)
-- Dependencies: 165
-- Data for Name: ctrl_grupo_puerta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2487 (class 0 OID 37059)
-- Dependencies: 167
-- Data for Name: ctrl_lectora; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2488 (class 0 OID 37066)
-- Dependencies: 169
-- Data for Name: ctrl_logacceso; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2489 (class 0 OID 37076)
-- Dependencies: 171
-- Data for Name: ctrl_puerta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2490 (class 0 OID 37082)
-- Dependencies: 173
-- Data for Name: ctrl_sincronizar; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2491 (class 0 OID 37091)
-- Dependencies: 175
-- Data for Name: ctrl_tipo_controladora; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ctrl_tipo_controladora VALUES (1, '716E V3/E');


--
-- TOC entry 2492 (class 0 OID 37097)
-- Dependencies: 177
-- Data for Name: ctrl_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2493 (class 0 OID 37110)
-- Dependencies: 178
-- Data for Name: ctrl_zonadetiempo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ctrl_zonadetiempo VALUES (0, 'Horario - Uno', false, 0, NULL, 480, 1080, 480, 540, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (1, 'Horario - 1', false, 0, NULL, 480, 1020, 480, 1020, 480, 1020, 480, 1020, 480, 1020, 480, 1020, 480, 1020);
INSERT INTO ctrl_zonadetiempo VALUES (2, 'Horario - 2', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (3, 'Horario - 3', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (4, 'Horario - 4', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (5, 'Horario - 5', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (6, 'Horario - 6', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (7, 'Horario - 7', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (8, 'Horario - 8', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (9, 'Horario - 9', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (10, 'Horario - 10', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (11, 'Horario - 11', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (12, 'Horario - 12', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (13, 'Horario - 13', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (14, 'Horario - 14', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (15, 'Horario - 15', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (16, 'Horario - 16', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (17, 'Horario - 17', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (18, 'Horario - 18', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (19, 'Horario - 19', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (20, 'Horario - 20', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (21, 'Horario - 21', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (22, 'Horario - 22', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (23, 'Horario - 23', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (24, 'Horario - 24', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (25, 'Horario - 25', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (26, 'Horario - 26', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (27, 'Horario - 27', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (28, 'Horario - 28', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (29, 'Horario - 29', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (30, 'Horario - 30', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (31, 'Horario - 31', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (32, 'Horario - 32', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (33, 'Horario - 33', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (34, 'Horario - 34', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (35, 'Horario - 35', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (36, 'Horario - 36', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (37, 'Horario - 37', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (38, 'Horario - 38', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (39, 'Horario - 39', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (40, 'Horario - 40', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (41, 'Horario - 41', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (42, 'Horario - 42', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (43, 'Horario - 43', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (44, 'Horario - 44', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (45, 'Horario - 45', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (46, 'Horario - 46', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (47, 'Horario - 47', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (48, 'Horario - 48', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (49, 'Horario - 49', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (50, 'Horario - 50', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (51, 'Horario - 51', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (52, 'Horario - 52', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (53, 'Horario - 53', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (54, 'Horario - 54', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (55, 'Horario - 55', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (56, 'Horario - 56', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (57, 'Horario - 57', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (58, 'Horario - 58', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (59, 'Horario - 59', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (60, 'Horario - 60', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (61, 'Horario - 61', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (62, 'Horario - 62', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);
INSERT INTO ctrl_zonadetiempo VALUES (63, 'Horario - 63', false, 0, NULL, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080, 480, 1080);


--
-- TOC entry 2494 (class 0 OID 37131)
-- Dependencies: 180
-- Data for Name: funcion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO funcion VALUES (1, 'listar', 'Listar', NULL, NULL, 'ADMPLT');
INSERT INTO funcion VALUES (2, 'agregar', 'Agregar', NULL, NULL, 'ADMPLT');
INSERT INTO funcion VALUES (3, 'modificar', 'Modificar', NULL, NULL, 'ADMPLT');
INSERT INTO funcion VALUES (4, 'eliminar', 'Eliminar', NULL, NULL, 'ADMPLT');
INSERT INTO funcion VALUES (5, 'agregar', 'Agregar', NULL, NULL, 'ADMUSR');
INSERT INTO funcion VALUES (6, 'modificar', 'Modificar Todos los Usuarios', NULL, NULL, 'ADMUSR');
INSERT INTO funcion VALUES (7, 'eliminar', 'Eliminar', NULL, NULL, 'ADMUSR');
INSERT INTO funcion VALUES (9, 'agregar', 'Agregar', NULL, NULL, 'CTRLCTRL');
INSERT INTO funcion VALUES (10, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLCTRL');
INSERT INTO funcion VALUES (11, 'listar', 'Listar', NULL, NULL, 'CTRLCTRL');
INSERT INTO funcion VALUES (12, 'modificar', 'Modificar', NULL, NULL, 'CTRLCTRL');
INSERT INTO funcion VALUES (13, 'estadoCnx', 'Estado de Conexión', NULL, NULL, 'CTRLCTRL');
INSERT INTO funcion VALUES (14, 'listar', 'Listar', NULL, NULL, 'CTRLDS');
INSERT INTO funcion VALUES (15, 'modificar', 'Modificar', NULL, NULL, 'CTRLDS');
INSERT INTO funcion VALUES (16, 'reset', 'Restablecer Valor', NULL, NULL, 'CTRLDS');
INSERT INTO funcion VALUES (17, 'calendario', 'Utilizar Calendario', NULL, NULL, 'CTRLDS');
INSERT INTO funcion VALUES (18, 'agregar', 'Agregar', NULL, NULL, 'CTRLGPR');
INSERT INTO funcion VALUES (19, 'modificar', 'Modificar', NULL, NULL, 'CTRLGPR');
INSERT INTO funcion VALUES (20, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLGPR');
INSERT INTO funcion VALUES (21, 'listar', 'Listar', NULL, NULL, 'CTRLGPR');
INSERT INTO funcion VALUES (22, 'listar', 'Listar', NULL, NULL, 'CTRLHRS');
INSERT INTO funcion VALUES (23, 'modificar', 'Modificar', NULL, NULL, 'CTRLHRS');
INSERT INTO funcion VALUES (24, 'agregar', 'Agregar', NULL, NULL, 'CTRLLEC');
INSERT INTO funcion VALUES (25, 'modificar', 'Modificar', NULL, NULL, 'CTRLLEC');
INSERT INTO funcion VALUES (26, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLLEC');
INSERT INTO funcion VALUES (27, 'listar', 'Listar', NULL, NULL, 'CTRLLEC');
INSERT INTO funcion VALUES (28, 'agregar', 'Agregar', NULL, NULL, 'CTRLPTS');
INSERT INTO funcion VALUES (29, 'modificar', 'Modificar', NULL, NULL, 'CTRLPTS');
INSERT INTO funcion VALUES (30, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLPTS');
INSERT INTO funcion VALUES (31, 'listar', 'Listar', NULL, NULL, 'CTRLPTS');
INSERT INTO funcion VALUES (32, 'agregar', 'Agregar', NULL, NULL, 'CTRLUSR');
INSERT INTO funcion VALUES (33, 'modificar', 'Modificar', NULL, NULL, 'CTRLUSR');
INSERT INTO funcion VALUES (34, 'eliminar', 'Eliminar', NULL, NULL, 'CTRLUSR');
INSERT INTO funcion VALUES (35, 'listar', 'Listar', NULL, NULL, 'CTRLUSR');
INSERT INTO funcion VALUES (37, 'listar', 'Listar', NULL, NULL, 'VIDASCMNTR');
INSERT INTO funcion VALUES (38, 'asociar', 'Asociar', NULL, NULL, 'VIDASCMNTR');
INSERT INTO funcion VALUES (39, 'agregar', 'Agregar', NULL, NULL, 'VIDCAM');
INSERT INTO funcion VALUES (40, 'modificar', 'Modificar', NULL, NULL, 'VIDCAM');
INSERT INTO funcion VALUES (41, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAM');
INSERT INTO funcion VALUES (42, 'listar', 'Listar', NULL, NULL, 'VIDCAM');
INSERT INTO funcion VALUES (43, 'horarioAlr', 'Horario de Alarmas', NULL, NULL, 'VIDCAM');
INSERT INTO funcion VALUES (44, 'presetPos', 'Posiciones Predefinidas', NULL, NULL, 'VIDCAM');
INSERT INTO funcion VALUES (45, 'agregar', 'Agregar', NULL, NULL, 'VIDSRV');
INSERT INTO funcion VALUES (46, 'modificar', 'Modificar', NULL, NULL, 'VIDSRV');
INSERT INTO funcion VALUES (47, 'eliminar', 'Eliminar', NULL, NULL, 'VIDSRV');
INSERT INTO funcion VALUES (48, 'listar', 'Listar', NULL, NULL, 'VIDSRV');
INSERT INTO funcion VALUES (49, 'reiniciar', 'Reiniciar Grabador', NULL, NULL, 'VIDSRV');
INSERT INTO funcion VALUES (50, 'detener', 'Detener Grabador', NULL, NULL, 'VIDSRV');
INSERT INTO funcion VALUES (51, 'agregar', 'Agregar', NULL, NULL, 'VIDGRD');
INSERT INTO funcion VALUES (52, 'modificar', 'Modificar', NULL, NULL, 'VIDGRD');
INSERT INTO funcion VALUES (53, 'eliminar', 'Eliminar', NULL, NULL, 'VIDGRD');
INSERT INTO funcion VALUES (54, 'listar', 'Listar', NULL, NULL, 'VIDGRD');
INSERT INTO funcion VALUES (55, 'grdPos', 'Establecer Posiciones Predefinidas', NULL, NULL, 'VIDGRD');
INSERT INTO funcion VALUES (56, 'agregar', 'Agregar', NULL, NULL, 'VIDCAMMRC');
INSERT INTO funcion VALUES (57, 'modificar', 'Modificar', NULL, NULL, 'VIDCAMMRC');
INSERT INTO funcion VALUES (58, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAMMRC');
INSERT INTO funcion VALUES (59, 'listar', 'Listar', NULL, NULL, 'VIDCAMMRC');
INSERT INTO funcion VALUES (60, 'agregar', 'Agregar', NULL, NULL, 'VIDCAMMOD');
INSERT INTO funcion VALUES (61, 'modificar', 'Modificar', NULL, NULL, 'VIDCAMMOD');
INSERT INTO funcion VALUES (62, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAMMOD');
INSERT INTO funcion VALUES (63, 'listar', 'Listar', NULL, NULL, 'VIDCAMMOD');
INSERT INTO funcion VALUES (64, 'verCamaras', 'Monitorear Camaras', NULL, NULL, 'VIDMNTR');
INSERT INTO funcion VALUES (65, 'agregar', 'Agregar', NULL, NULL, 'VIDCAMTIP');
INSERT INTO funcion VALUES (66, 'modificar', 'Modificar', NULL, NULL, 'VIDCAMTIP');
INSERT INTO funcion VALUES (67, 'eliminar', 'Eliminar', NULL, NULL, 'VIDCAMTIP');
INSERT INTO funcion VALUES (68, 'listar', 'Listar', NULL, NULL, 'VIDCAMTIP');
INSERT INTO funcion VALUES (69, 'agregar', 'Agregar', NULL, NULL, 'VIDZN');
INSERT INTO funcion VALUES (70, 'modificar', 'Modificar', NULL, NULL, 'VIDZN');
INSERT INTO funcion VALUES (71, 'eliminar', 'Eliminar', NULL, NULL, 'VIDZN');
INSERT INTO funcion VALUES (72, 'listar', 'Listar', NULL, NULL, 'VIDZN');
INSERT INTO funcion VALUES (74, 'agregar', 'Agregar', NULL, NULL, 'ADMGPR');
INSERT INTO funcion VALUES (75, 'modificar', 'Modificar', NULL, NULL, 'ADMGPR');
INSERT INTO funcion VALUES (76, 'eliminar', 'Eliminar', NULL, NULL, 'ADMGPR');
INSERT INTO funcion VALUES (77, 'listar', 'Listar', NULL, NULL, NULL);
INSERT INTO funcion VALUES (78, 'listar', 'Listar', NULL, NULL, 'ADMGPR');
INSERT INTO funcion VALUES (79, 'visualizar', 'Visualizar Registro', NULL, NULL, 'ADMGPR');
INSERT INTO funcion VALUES (80, 'visualizar', 'Visualizar Registro', NULL, NULL, 'ADMPLT');
INSERT INTO funcion VALUES (83, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLCTRL');
INSERT INTO funcion VALUES (84, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLDS');
INSERT INTO funcion VALUES (85, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLGPR');
INSERT INTO funcion VALUES (86, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLHRS');
INSERT INTO funcion VALUES (87, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLLEC');
INSERT INTO funcion VALUES (88, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLPTS');
INSERT INTO funcion VALUES (89, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CTRLUSR');
INSERT INTO funcion VALUES (90, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAM');
INSERT INTO funcion VALUES (91, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDSRV');
INSERT INTO funcion VALUES (92, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDGRD');
INSERT INTO funcion VALUES (93, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAMMRC');
INSERT INTO funcion VALUES (94, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAMMOD');
INSERT INTO funcion VALUES (95, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDCAMTIP');
INSERT INTO funcion VALUES (96, 'visualizar', 'Visualizar Registro', NULL, NULL, 'VIDZN');
INSERT INTO funcion VALUES (97, 'segmentos', 'Asignar Segmentos', NULL, NULL, 'ADMUSR');
INSERT INTO funcion VALUES (98, 'gprPermiso', 'Grupos de Permisos', NULL, NULL, 'ADMUSR');
INSERT INTO funcion VALUES (99, 'agregar', 'Generar', NULL, NULL, 'VIDACCPC');
INSERT INTO funcion VALUES (100, 'visualizar', 'Visualizar', NULL, NULL, 'VIDACCPC');
INSERT INTO funcion VALUES (101, 'eliminar', 'Eliminar', NULL, NULL, 'VIDACCPC');
INSERT INTO funcion VALUES (102, 'listar', 'Listar', NULL, NULL, 'VIDACCPC');
INSERT INTO funcion VALUES (103, 'listar', 'Listar', NULL, NULL, 'VIDALARM');
INSERT INTO funcion VALUES (104, 'abrir', 'Abrir Puertas', NULL, NULL, 'CTRLPTS');
INSERT INTO funcion VALUES (105, 'cerrar', 'Cerrar Puertas', NULL, NULL, 'CTRLPTS');
INSERT INTO funcion VALUES (106, 'listar', 'Listar', NULL, NULL, 'CTRLLOG');
INSERT INTO funcion VALUES (107, 'agregar', 'Agregar', NULL, NULL, 'CARPLT');
INSERT INTO funcion VALUES (108, 'modificar', 'Modificar', NULL, NULL, 'CARPLT');
INSERT INTO funcion VALUES (109, 'eliminar', 'Eliminar', NULL, NULL, 'CARPLT');
INSERT INTO funcion VALUES (110, 'listar', 'Listar', NULL, NULL, 'CARPLT');
INSERT INTO funcion VALUES (111, 'agregar', 'Agregar', NULL, NULL, 'CARCRG');
INSERT INTO funcion VALUES (112, 'agregar', 'Agregar', NULL, NULL, 'CARDPT');
INSERT INTO funcion VALUES (113, 'modificar', 'Modificar', NULL, NULL, 'CARCRG');
INSERT INTO funcion VALUES (114, 'modificar', 'Modificar', NULL, NULL, 'CARDPT');
INSERT INTO funcion VALUES (115, 'eliminar', 'Eliminar', NULL, NULL, 'CARCRG');
INSERT INTO funcion VALUES (116, 'eliminar', 'Eliminar', NULL, NULL, 'CARDPT');
INSERT INTO funcion VALUES (117, 'listar', 'Listar', NULL, NULL, 'CARCRG');
INSERT INTO funcion VALUES (118, 'listar', 'Listar', NULL, NULL, 'CARDPT');
INSERT INTO funcion VALUES (119, 'agregar', 'Agregar', NULL, NULL, 'CAREMP');
INSERT INTO funcion VALUES (120, 'modificar', 'Modificar', NULL, NULL, 'CAREMP');
INSERT INTO funcion VALUES (121, 'listar', 'Listar', NULL, NULL, 'CAREMP');
INSERT INTO funcion VALUES (122, 'eliminar', 'Eliminar', NULL, NULL, 'CAREMP');
INSERT INTO funcion VALUES (123, 'cargarFoto', 'Cargar Foto', NULL, NULL, 'CAREMP');
INSERT INTO funcion VALUES (124, 'imprimir', 'Imprimir', NULL, NULL, 'CAREMP');
INSERT INTO funcion VALUES (125, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARCRG');
INSERT INTO funcion VALUES (126, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CAREMP');
INSERT INTO funcion VALUES (127, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARDPT');
INSERT INTO funcion VALUES (128, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARPLT');
INSERT INTO funcion VALUES (129, 'agregar', 'Agregar', NULL, NULL, 'CARVIS');
INSERT INTO funcion VALUES (130, 'modificar', 'Modificar', NULL, NULL, 'CARVIS');
INSERT INTO funcion VALUES (131, 'eliminar', 'Eliminar', NULL, NULL, 'CARVIS');
INSERT INTO funcion VALUES (132, 'visualizar', 'Visualizar Registro', NULL, NULL, 'CARVIS');
INSERT INTO funcion VALUES (133, 'listar', 'Listar', NULL, NULL, 'CARVIS');
INSERT INTO funcion VALUES (134, 'imprimir', 'Imprimir Carnet', NULL, NULL, 'CARVIS');
INSERT INTO funcion VALUES (135, 'agregar', 'Agregar', NULL, NULL, 'ADMACCPC');
INSERT INTO funcion VALUES (136, 'modificar', 'Modificar', NULL, NULL, 'ADMACCPC');
INSERT INTO funcion VALUES (137, 'eliminar', 'Eliminar', NULL, NULL, 'ADMACCPC');
INSERT INTO funcion VALUES (138, 'visualizar', 'Visualizar', NULL, NULL, 'ADMACCPC');
INSERT INTO funcion VALUES (139, 'listar', 'Listar', NULL, NULL, 'ADMACCPC');
INSERT INTO funcion VALUES (140, 'listar', 'listar', NULL, NULL, 'CARLISTEMP');
INSERT INTO funcion VALUES (141, 'agregar', 'Agregar', NULL, NULL, 'ADMSEG');
INSERT INTO funcion VALUES (142, 'modificar', 'Modificar', NULL, NULL, 'ADMSEG');
INSERT INTO funcion VALUES (143, 'eliminar', 'Eliminar', NULL, NULL, 'ADMSEG');
INSERT INTO funcion VALUES (144, 'listar', 'Listar', NULL, NULL, 'ADMSEG');
INSERT INTO funcion VALUES (145, 'visualizar', 'Visualizar', NULL, NULL, 'ADMSEG');
INSERT INTO funcion VALUES (146, 'exportar', 'Exportar Camaras', 'Exportar Camaras para mobil', 1, 'VIDEXCAM');
INSERT INTO funcion VALUES (147, 'syncDatos', 'Sincronizar a Controladora', 'Descargar los datos a la controladora', NULL, 'CTRLCTRL');
INSERT INTO funcion VALUES (148, 'monitor', 'Monitor Acceso', NULL, NULL, 'CTRLMON');
INSERT INTO funcion VALUES (149, 'listar', 'Listar', NULL, NULL, 'CTRLLOGA');
INSERT INTO funcion VALUES (150, 'monitor', 'Monitor Asistencia', NULL, NULL, 'CTRLMONA');
INSERT INTO funcion VALUES (151, 'descargar', 'Descargar CSV', NULL, NULL, 'CTRLLOGA');


--
-- TOC entry 2495 (class 0 OID 37137)
-- Dependencies: 182
-- Data for Name: grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2496 (class 0 OID 37141)
-- Dependencies: 183
-- Data for Name: grupo_has_funcion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2497 (class 0 OID 37146)
-- Dependencies: 185
-- Data for Name: plantafisica; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2498 (class 0 OID 37152)
-- Dependencies: 187
-- Data for Name: segmento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2499 (class 0 OID 37156)
-- Dependencies: 188
-- Data for Name: segmento_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2500 (class 0 OID 37161)
-- Dependencies: 190
-- Data for Name: timezone; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO timezone VALUES (1, 'Africa/Abidjan', 'UTC+00', '-');
INSERT INTO timezone VALUES (2, 'Africa/Accra', 'UTC+00', '-');
INSERT INTO timezone VALUES (3, 'Africa/Addis_Ababa', 'UTC+03', '-');
INSERT INTO timezone VALUES (4, 'Africa/Algiers', 'UTC+01', '-');
INSERT INTO timezone VALUES (5, 'Africa/Asmara', 'UTC+03', '-');
INSERT INTO timezone VALUES (6, 'Africa/Bamako', 'UTC+00', '-');
INSERT INTO timezone VALUES (7, 'Africa/Bangui', 'UTC+01', '-');
INSERT INTO timezone VALUES (8, 'Africa/Banjul', 'UTC+00', '-');
INSERT INTO timezone VALUES (9, 'Africa/Bissau', 'UTC+00', '-');
INSERT INTO timezone VALUES (10, 'Africa/Blantyre', 'UTC+02', '-');
INSERT INTO timezone VALUES (11, 'Africa/Brazzaville', 'UTC+01', '-');
INSERT INTO timezone VALUES (12, 'Africa/Bujumbura', 'UTC+02', '-');
INSERT INTO timezone VALUES (13, 'Africa/Cairo', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (14, 'Africa/Casablanca', 'UTC+00', '-');
INSERT INTO timezone VALUES (15, 'Africa/Ceuta', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (16, 'Africa/Conakry', 'UTC+00', '-');
INSERT INTO timezone VALUES (17, 'Africa/Dakar', 'UTC+00', '-');
INSERT INTO timezone VALUES (18, 'Africa/Dar_es_Salaam', 'UTC+03', '-');
INSERT INTO timezone VALUES (19, 'Africa/Djibouti', 'UTC+03', '-');
INSERT INTO timezone VALUES (20, 'Africa/Douala', 'UTC+01', '-');
INSERT INTO timezone VALUES (21, 'Africa/El_Aaiun', 'UTC+00', '-');
INSERT INTO timezone VALUES (22, 'Africa/Freetown', 'UTC+00', '-');
INSERT INTO timezone VALUES (23, 'Africa/Gaborone', 'UTC+02', '-');
INSERT INTO timezone VALUES (24, 'Africa/Harare', 'UTC+02', '-');
INSERT INTO timezone VALUES (25, 'Africa/Johannesburg', 'UTC+02', '-');
INSERT INTO timezone VALUES (26, 'Africa/Kampala', 'UTC+03', '-');
INSERT INTO timezone VALUES (27, 'Africa/Khartoum', 'UTC+03', '-');
INSERT INTO timezone VALUES (28, 'Africa/Kigali', 'UTC+02', '-');
INSERT INTO timezone VALUES (29, 'Africa/Kinshasa', 'UTC+01', '-');
INSERT INTO timezone VALUES (30, 'Africa/Lagos', 'UTC+01', '-');
INSERT INTO timezone VALUES (31, 'Africa/Libreville', 'UTC+01', '-');
INSERT INTO timezone VALUES (32, 'Africa/Lome', 'UTC+00', '-');
INSERT INTO timezone VALUES (33, 'Africa/Luanda', 'UTC+01', '-');
INSERT INTO timezone VALUES (34, 'Africa/Lubumbashi', 'UTC+02', '-');
INSERT INTO timezone VALUES (35, 'Africa/Lusaka', 'UTC+02', '-');
INSERT INTO timezone VALUES (36, 'Africa/Malabo', 'UTC+01', '-');
INSERT INTO timezone VALUES (37, 'Africa/Maputo', 'UTC+02', '-');
INSERT INTO timezone VALUES (38, 'Africa/Maseru', 'UTC+02', '-');
INSERT INTO timezone VALUES (39, 'Africa/Mbabane', 'UTC+02', '-');
INSERT INTO timezone VALUES (40, 'Africa/Mogadishu', 'UTC+03', '-');
INSERT INTO timezone VALUES (41, 'Africa/Monrovia', 'UTC+00', '-');
INSERT INTO timezone VALUES (42, 'Africa/Nairobi', 'UTC+03', '-');
INSERT INTO timezone VALUES (43, 'Africa/Ndjamena', 'UTC+01', '-');
INSERT INTO timezone VALUES (44, 'Africa/Niamey', 'UTC+01', '-');
INSERT INTO timezone VALUES (45, 'Africa/Nouakchott', 'UTC+00', '-');
INSERT INTO timezone VALUES (46, 'Africa/Ouagadougou', 'UTC+00', '-');
INSERT INTO timezone VALUES (47, 'Africa/Porto-Novo', 'UTC+01', '-');
INSERT INTO timezone VALUES (48, 'Africa/Sao_Tome', 'UTC+00', '-');
INSERT INTO timezone VALUES (49, 'Africa/Tripoli', 'UTC+02', '-');
INSERT INTO timezone VALUES (50, 'Africa/Tunis', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (51, 'Africa/Windhoek', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (52, 'America/Adak', 'UTC-10', 'UTC-09');
INSERT INTO timezone VALUES (53, 'America/Anchorage', 'UTC-09', 'UTC-08');
INSERT INTO timezone VALUES (54, 'America/Anguilla', 'UTC-04', '-');
INSERT INTO timezone VALUES (55, 'America/Antigua', 'UTC-04', '-');
INSERT INTO timezone VALUES (56, 'America/Araguaina', 'UTC-03', '-');
INSERT INTO timezone VALUES (57, 'America/Argentina/Buenos_Aires', 'UTC-03', 'UTC-02');
INSERT INTO timezone VALUES (58, 'America/Argentina/Catamarca', 'UTC-03', '-');
INSERT INTO timezone VALUES (59, 'America/Argentina/Cordoba', 'UTC-03', 'UTC-02');
INSERT INTO timezone VALUES (60, 'America/Argentina/Jujuy', 'UTC-03', '-');
INSERT INTO timezone VALUES (61, 'America/Argentina/La_Rioja', 'UTC-03', '-');
INSERT INTO timezone VALUES (62, 'America/Argentina/Mendoza', 'UTC-03', '-');
INSERT INTO timezone VALUES (63, 'America/Argentina/Rio_Gallegos', 'UTC-03', '-');
INSERT INTO timezone VALUES (64, 'America/Argentina/Salta', 'UTC-03', '-');
INSERT INTO timezone VALUES (65, 'America/Argentina/San_Juan', 'UTC-03', '-');
INSERT INTO timezone VALUES (66, 'America/Argentina/San_Luis', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (67, 'America/Argentina/Tucuman', 'UTC-03', 'UTC-02');
INSERT INTO timezone VALUES (68, 'America/Argentina/Ushuaia', 'UTC-03', '-');
INSERT INTO timezone VALUES (69, 'America/Aruba', 'UTC-04', '-');
INSERT INTO timezone VALUES (70, 'America/Asuncion', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (71, 'America/Atikokan', 'UTC-05', '-');
INSERT INTO timezone VALUES (72, 'America/Bahia', 'UTC-03', '-');
INSERT INTO timezone VALUES (73, 'America/Barbados', 'UTC-04', '-');
INSERT INTO timezone VALUES (74, 'America/Belem', 'UTC-03', '-');
INSERT INTO timezone VALUES (75, 'America/Belize', 'UTC-06', '-');
INSERT INTO timezone VALUES (76, 'America/Blanc-Sablon', 'UTC-04', '-');
INSERT INTO timezone VALUES (77, 'America/Boa_Vista', 'UTC-04', '-');
INSERT INTO timezone VALUES (78, 'America/Bogota', 'UTC-05', '-');
INSERT INTO timezone VALUES (79, 'America/Boise', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (80, 'America/Cambridge_Bay', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (81, 'America/Campo_Grande', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (82, 'America/Cancun', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (83, 'America/Caracas', 'UTC-04:30', '-');
INSERT INTO timezone VALUES (84, 'America/Cayenne', 'UTC-03', '-');
INSERT INTO timezone VALUES (85, 'America/Cayman', 'UTC-05', '-');
INSERT INTO timezone VALUES (86, 'America/Chicago', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (87, 'America/Chihuahua', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (88, 'America/Costa_Rica', 'UTC-06', '-');
INSERT INTO timezone VALUES (89, 'America/Cuiaba', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (90, 'America/Curacao', 'UTC-04', '-');
INSERT INTO timezone VALUES (91, 'America/Danmarkshavn', 'UTC+00', '-');
INSERT INTO timezone VALUES (92, 'America/Dawson', 'UTC-08', 'UTC-07');
INSERT INTO timezone VALUES (93, 'America/Dawson_Creek', 'UTC-07', '-');
INSERT INTO timezone VALUES (94, 'America/Denver', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (95, 'America/Detroit', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (96, 'America/Dominica', 'UTC-04', '-');
INSERT INTO timezone VALUES (97, 'America/Edmonton', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (98, 'America/Eirunepe', 'UTC-04', '-');
INSERT INTO timezone VALUES (99, 'America/El_Salvador', 'UTC-06', '-');
INSERT INTO timezone VALUES (100, 'America/Fortaleza', 'UTC-03', '-');
INSERT INTO timezone VALUES (101, 'America/Glace_Bay', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (102, 'America/Godthab', 'UTC-03', 'UTC-02');
INSERT INTO timezone VALUES (103, 'America/Goose_Bay', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (104, 'America/Grand_Turk', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (105, 'America/Grenada', 'UTC-04', '-');
INSERT INTO timezone VALUES (106, 'America/Guadeloupe', 'UTC-04', '-');
INSERT INTO timezone VALUES (107, 'America/Guatemala', 'UTC-06', '-');
INSERT INTO timezone VALUES (108, 'America/Guayaquil', 'UTC-05', '-');
INSERT INTO timezone VALUES (109, 'America/Guyana', 'UTC-04', '-');
INSERT INTO timezone VALUES (110, 'America/Halifax', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (111, 'America/Havana', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (112, 'America/Hermosillo', 'UTC-07', '-');
INSERT INTO timezone VALUES (113, 'America/Indiana/Indianapolis', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (114, 'America/Indiana/Knox', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (115, 'America/Indiana/Marengo', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (116, 'America/Indiana/Petersburg', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (117, 'America/Indiana/Tell_City', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (118, 'America/Indiana/Vevay', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (119, 'America/Indiana/Vincennes', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (120, 'America/Indiana/Winamac', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (121, 'America/Inuvik', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (122, 'America/Iqaluit', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (123, 'America/Jamaica', 'UTC-05', '-');
INSERT INTO timezone VALUES (124, 'America/Juneau', 'UTC-09', 'UTC-08');
INSERT INTO timezone VALUES (125, 'America/Kentucky/Louisville', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (126, 'America/Kentucky/Monticello', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (127, 'America/La_Paz', 'UTC-04', '-');
INSERT INTO timezone VALUES (128, 'America/Lima', 'UTC-05', '-');
INSERT INTO timezone VALUES (129, 'America/Los_Angeles', 'UTC-08', 'UTC-07');
INSERT INTO timezone VALUES (130, 'America/Maceio', 'UTC-03', '-');
INSERT INTO timezone VALUES (131, 'America/Managua', 'UTC-06', '-');
INSERT INTO timezone VALUES (132, 'America/Manaus', 'UTC-04', '-');
INSERT INTO timezone VALUES (133, 'America/Marigot', 'UTC-04', '-');
INSERT INTO timezone VALUES (134, 'America/Martinique', 'UTC-04', '-');
INSERT INTO timezone VALUES (135, 'America/Matamoros', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (136, 'America/Mazatlan', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (137, 'America/Menominee', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (138, 'America/Merida', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (139, 'America/Mexico_City', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (140, 'America/Miquelon', 'UTC-03', 'UTC-02');
INSERT INTO timezone VALUES (141, 'America/Moncton', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (142, 'America/Monterrey', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (143, 'America/Montevideo', 'UTC-03', 'UTC-02');
INSERT INTO timezone VALUES (144, 'America/Montreal', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (145, 'America/Montserrat', 'UTC-04', '-');
INSERT INTO timezone VALUES (146, 'America/Nassau', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (147, 'America/New_York', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (148, 'America/Nipigon', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (149, 'America/Nome', 'UTC-09', 'UTC-08');
INSERT INTO timezone VALUES (150, 'America/Noronha', 'UTC-02', '-');
INSERT INTO timezone VALUES (151, 'America/North_Dakota/Center', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (152, 'America/North_Dakota/New_Salem', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (153, 'America/Ojinaga', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (154, 'America/Panama', 'UTC-05', '-');
INSERT INTO timezone VALUES (155, 'America/Pangnirtung', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (156, 'America/Paramaribo', 'UTC-03', '-');
INSERT INTO timezone VALUES (157, 'America/Phoenix', 'UTC-07', '-');
INSERT INTO timezone VALUES (158, 'America/Port_of_Spain', 'UTC-04', '-');
INSERT INTO timezone VALUES (159, 'America/Port-au-Prince', 'UTC-05', '-');
INSERT INTO timezone VALUES (160, 'America/Porto_Velho', 'UTC-04', '-');
INSERT INTO timezone VALUES (161, 'America/Puerto_Rico', 'UTC-04', '-');
INSERT INTO timezone VALUES (162, 'America/Rainy_River', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (163, 'America/Rankin_Inlet', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (164, 'America/Recife', 'UTC-03', '-');
INSERT INTO timezone VALUES (165, 'America/Regina', 'UTC-06', '-');
INSERT INTO timezone VALUES (166, 'America/Resolute', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (167, 'America/Rio_Branco', 'UTC-04', '-');
INSERT INTO timezone VALUES (168, 'America/Santa_Isabel', 'UTC-08', 'UTC-07');
INSERT INTO timezone VALUES (169, 'America/Santarem', 'UTC-03', '-');
INSERT INTO timezone VALUES (170, 'America/Santiago', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (171, 'America/Santo_Domingo', 'UTC-04', '-');
INSERT INTO timezone VALUES (172, 'America/Sao_Paulo', 'UTC-03', 'UTC-02');
INSERT INTO timezone VALUES (173, 'America/Scoresbysund', 'UTC-01', 'UTC+00');
INSERT INTO timezone VALUES (174, 'America/Shiprock', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (175, 'America/St_Barthelemy', 'UTC-04', '-');
INSERT INTO timezone VALUES (176, 'America/St_Johns', 'UTC-03:30', 'UTC-02:30');
INSERT INTO timezone VALUES (177, 'America/St_Kitts', 'UTC-04', '-');
INSERT INTO timezone VALUES (178, 'America/St_Lucia', 'UTC-04', '-');
INSERT INTO timezone VALUES (179, 'America/St_Thomas', 'UTC-04', '-');
INSERT INTO timezone VALUES (180, 'America/St_Vincent', 'UTC-04', '-');
INSERT INTO timezone VALUES (181, 'America/Swift_Current', 'UTC-06', '-');
INSERT INTO timezone VALUES (182, 'America/Tegucigalpa', 'UTC-06', '-');
INSERT INTO timezone VALUES (183, 'America/Thule', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (184, 'America/Thunder_Bay', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (185, 'America/Tijuana', 'UTC-08', 'UTC-07');
INSERT INTO timezone VALUES (186, 'America/Toronto', 'UTC-05', 'UTC-04');
INSERT INTO timezone VALUES (187, 'America/Tortola', 'UTC-04', '-');
INSERT INTO timezone VALUES (188, 'America/Vancouver', 'UTC-08', 'UTC-07');
INSERT INTO timezone VALUES (189, 'America/Whitehorse', 'UTC-08', 'UTC-07');
INSERT INTO timezone VALUES (190, 'America/Winnipeg', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (191, 'America/Yakutat', 'UTC-09', 'UTC-08');
INSERT INTO timezone VALUES (192, 'America/Yellowknife', 'UTC-07', 'UTC-06');
INSERT INTO timezone VALUES (193, 'Antarctica/Casey', 'UTC+08', '-');
INSERT INTO timezone VALUES (194, 'Antarctica/Davis', 'UTC+07', '-');
INSERT INTO timezone VALUES (195, 'Antarctica/DumontDUrville', 'UTC+10', '-');
INSERT INTO timezone VALUES (196, 'Antarctica/Mawson', 'UTC+06', '-');
INSERT INTO timezone VALUES (197, 'Antarctica/McMurdo', 'UTC+12', 'UTC+13');
INSERT INTO timezone VALUES (198, 'Antarctica/Palmer', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (199, 'Antarctica/Rothera', 'UTC-03', '-');
INSERT INTO timezone VALUES (200, 'Antarctica/South_Pole', 'UTC+12', 'UTC+13');
INSERT INTO timezone VALUES (201, 'Antarctica/Syowa', 'UTC+03', '-');
INSERT INTO timezone VALUES (202, 'Antarctica/Vostok', 'UTC+00', '-');
INSERT INTO timezone VALUES (203, 'Arctic/Longyearbyen', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (204, 'Asia/Aden', 'UTC+03', '-');
INSERT INTO timezone VALUES (205, 'Asia/Almaty', 'UTC+06', '-');
INSERT INTO timezone VALUES (206, 'Asia/Amman', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (207, 'Asia/Anadyr', 'UTC+11', 'UTC+12');
INSERT INTO timezone VALUES (208, 'Asia/Aqtau', 'UTC+05', '-');
INSERT INTO timezone VALUES (209, 'Asia/Aqtobe', 'UTC+05', '-');
INSERT INTO timezone VALUES (210, 'Asia/Ashgabat', 'UTC+05', '-');
INSERT INTO timezone VALUES (211, 'Asia/Baghdad', 'UTC+03', '-');
INSERT INTO timezone VALUES (212, 'Asia/Bahrain', 'UTC+03', '-');
INSERT INTO timezone VALUES (213, 'Asia/Baku', 'UTC+04', 'UTC+05');
INSERT INTO timezone VALUES (214, 'Asia/Bangkok', 'UTC+07', '-');
INSERT INTO timezone VALUES (215, 'Asia/Beirut', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (216, 'Asia/Bishkek', 'UTC+06', '-');
INSERT INTO timezone VALUES (217, 'Asia/Brunei', 'UTC+08', '-');
INSERT INTO timezone VALUES (218, 'Asia/Choibalsan', 'UTC+08', '-');
INSERT INTO timezone VALUES (219, 'Asia/Chongqing', 'UTC+08', '-');
INSERT INTO timezone VALUES (220, 'Asia/Colombo', 'UTC+05:30', '-');
INSERT INTO timezone VALUES (221, 'Asia/Damascus', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (222, 'Asia/Dhaka', 'UTC+06', '-');
INSERT INTO timezone VALUES (223, 'Asia/Dili', 'UTC+09', '-');
INSERT INTO timezone VALUES (224, 'Asia/Dubai', 'UTC+04', '-');
INSERT INTO timezone VALUES (225, 'Asia/Dushanbe', 'UTC+05', '-');
INSERT INTO timezone VALUES (226, 'Asia/Gaza', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (227, 'Asia/Harbin', 'UTC+08', '-');
INSERT INTO timezone VALUES (228, 'Asia/Ho_Chi_Minh', 'UTC+07', '-');
INSERT INTO timezone VALUES (229, 'Asia/Hong_Kong', 'UTC+08', '-');
INSERT INTO timezone VALUES (230, 'Asia/Hovd', 'UTC+07', '-');
INSERT INTO timezone VALUES (231, 'Asia/Irkutsk', 'UTC+08', 'UTC+09');
INSERT INTO timezone VALUES (232, 'Asia/Jakarta', 'UTC+07', '-');
INSERT INTO timezone VALUES (233, 'Asia/Jayapura', 'UTC+09', '-');
INSERT INTO timezone VALUES (234, 'Asia/Jerusalem', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (235, 'Asia/Kabul', 'UTC+04:30', '-');
INSERT INTO timezone VALUES (236, 'Asia/Kamchatka', 'UTC+11', 'UTC+12');
INSERT INTO timezone VALUES (237, 'Asia/Karachi', 'UTC+06', '-');
INSERT INTO timezone VALUES (238, 'Asia/Kashgar', 'UTC+08', '-');
INSERT INTO timezone VALUES (239, 'Asia/Kathmandu', 'UTC+05:45', '-');
INSERT INTO timezone VALUES (240, 'Asia/Kolkata', 'UTC+05:30', '-');
INSERT INTO timezone VALUES (241, 'Asia/Krasnoyarsk', 'UTC+07', 'UTC+08');
INSERT INTO timezone VALUES (242, 'Asia/Kuala_Lumpur', 'UTC+08', '-');
INSERT INTO timezone VALUES (243, 'Asia/Kuching', 'UTC+08', '-');
INSERT INTO timezone VALUES (244, 'Asia/Kuwait', 'UTC+03', '-');
INSERT INTO timezone VALUES (245, 'Asia/Macau', 'UTC+08', '-');
INSERT INTO timezone VALUES (246, 'Asia/Magadan', 'UTC+11', 'UTC+12');
INSERT INTO timezone VALUES (247, 'Asia/Makassar', 'UTC+08', '-');
INSERT INTO timezone VALUES (248, 'Asia/Manila', 'UTC+08', '-');
INSERT INTO timezone VALUES (249, 'Asia/Muscat', 'UTC+04', '-');
INSERT INTO timezone VALUES (250, 'Asia/Nicosia', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (251, 'Asia/Novokuznetsk', 'UTC+06', 'UTC+07');
INSERT INTO timezone VALUES (252, 'Asia/Novosibirsk', 'UTC+06', 'UTC+07');
INSERT INTO timezone VALUES (253, 'Asia/Omsk', 'UTC+06', 'UTC+07');
INSERT INTO timezone VALUES (254, 'Asia/Oral', 'UTC+05', '-');
INSERT INTO timezone VALUES (255, 'Asia/Phnom_Penh', 'UTC+07', '-');
INSERT INTO timezone VALUES (256, 'Asia/Pontianak', 'UTC+07', '-');
INSERT INTO timezone VALUES (257, 'Asia/Pyongyang', 'UTC+09', '-');
INSERT INTO timezone VALUES (258, 'Asia/Qatar', 'UTC+03', '-');
INSERT INTO timezone VALUES (259, 'Asia/Qyzylorda', 'UTC+06', '-');
INSERT INTO timezone VALUES (260, 'Asia/Rangoon', 'UTC+06:30', '-');
INSERT INTO timezone VALUES (261, 'Asia/Riyadh', 'UTC+03', '-');
INSERT INTO timezone VALUES (262, 'Asia/Sakhalin', 'UTC+10', 'UTC+11');
INSERT INTO timezone VALUES (263, 'Asia/Samarkand', 'UTC+05', '-');
INSERT INTO timezone VALUES (264, 'Asia/Seoul', 'UTC+09', '-');
INSERT INTO timezone VALUES (265, 'Asia/Shanghai', 'UTC+08', '-');
INSERT INTO timezone VALUES (266, 'Asia/Singapore', 'UTC+08', '-');
INSERT INTO timezone VALUES (267, 'Asia/Taipei', 'UTC+08', '-');
INSERT INTO timezone VALUES (268, 'Asia/Tashkent', 'UTC+05', '-');
INSERT INTO timezone VALUES (269, 'Asia/Tbilisi', 'UTC+04', '-');
INSERT INTO timezone VALUES (270, 'Asia/Tehran', 'UTC+03:30', 'UTC+04:30');
INSERT INTO timezone VALUES (271, 'Asia/Thimphu', 'UTC+06', '-');
INSERT INTO timezone VALUES (272, 'Asia/Tokyo', 'UTC+09', '-');
INSERT INTO timezone VALUES (273, 'Asia/Ulaanbaatar', 'UTC+08', '-');
INSERT INTO timezone VALUES (274, 'Asia/Urumqi', 'UTC+08', '-');
INSERT INTO timezone VALUES (275, 'Asia/Vientiane', 'UTC+07', '-');
INSERT INTO timezone VALUES (276, 'Asia/Vladivostok', 'UTC+10', 'UTC+11');
INSERT INTO timezone VALUES (277, 'Asia/Yakutsk', 'UTC+09', 'UTC+10');
INSERT INTO timezone VALUES (278, 'Asia/Yekaterinburg', 'UTC+05', 'UTC+06');
INSERT INTO timezone VALUES (279, 'Asia/Yerevan', 'UTC+04', 'UTC+05');
INSERT INTO timezone VALUES (280, 'Atlantic/Azores', 'UTC-01', 'UTC+00');
INSERT INTO timezone VALUES (281, 'Atlantic/Bermuda', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (282, 'Atlantic/Canary', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (283, 'Atlantic/Cape_Verde', 'UTC-01', '-');
INSERT INTO timezone VALUES (284, 'Atlantic/Faroe', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (285, 'Atlantic/Madeira', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (286, 'Atlantic/Reykjavik', 'UTC+00', '-');
INSERT INTO timezone VALUES (287, 'Atlantic/South_Georgia', 'UTC-02', '-');
INSERT INTO timezone VALUES (288, 'Atlantic/St_Helena', 'UTC+00', '-');
INSERT INTO timezone VALUES (289, 'Atlantic/Stanley', 'UTC-04', 'UTC-03');
INSERT INTO timezone VALUES (290, 'Australia/Adelaide', 'UTC+09:30', 'UTC+10:30');
INSERT INTO timezone VALUES (291, 'Australia/Brisbane', 'UTC+10', '-');
INSERT INTO timezone VALUES (292, 'Australia/Broken_Hill', 'UTC+09:30', 'UTC+10:30');
INSERT INTO timezone VALUES (293, 'Australia/Currie', 'UTC+10', 'UTC+11');
INSERT INTO timezone VALUES (294, 'Australia/Darwin', 'UTC+09:30', '-');
INSERT INTO timezone VALUES (295, 'Australia/Eucla', 'UTC+08:45', 'UTC+09:45');
INSERT INTO timezone VALUES (296, 'Australia/Hobart', 'UTC+10', 'UTC+11');
INSERT INTO timezone VALUES (297, 'Australia/Lindeman', 'UTC+10', '-');
INSERT INTO timezone VALUES (298, 'Australia/Lord_Howe', 'UTC+10:30', 'UTC+11');
INSERT INTO timezone VALUES (299, 'Australia/Melbourne', 'UTC+10', 'UTC+11');
INSERT INTO timezone VALUES (300, 'Australia/Perth', 'UTC+08', '-');
INSERT INTO timezone VALUES (301, 'Australia/Sydney', 'UTC+10', 'UTC+11');
INSERT INTO timezone VALUES (302, 'Europe/Amsterdam', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (303, 'Europe/Andorra', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (304, 'Europe/Athens', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (305, 'Europe/Belgrade', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (306, 'Europe/Berlin', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (307, 'Europe/Bratislava', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (308, 'Europe/Brussels', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (309, 'Europe/Bucharest', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (310, 'Europe/Budapest', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (311, 'Europe/Chisinau', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (312, 'Europe/Copenhagen', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (313, 'Europe/Dublin', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (314, 'Europe/Gibraltar', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (315, 'Europe/Guernsey', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (316, 'Europe/Helsinki', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (317, 'Europe/Isle_of_Man', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (318, 'Europe/Istanbul', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (319, 'Europe/Jersey', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (320, 'Europe/Kaliningrad', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (321, 'Europe/Kiev', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (322, 'Europe/Lisbon', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (323, 'Europe/Ljubljana', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (324, 'Europe/London', 'UTC+00', 'UTC+01');
INSERT INTO timezone VALUES (325, 'Europe/Luxembourg', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (326, 'Europe/Madrid', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (327, 'Europe/Malta', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (328, 'Europe/Mariehamn', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (329, 'Europe/Minsk', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (330, 'Europe/Monaco', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (331, 'Europe/Moscow', 'UTC+03', 'UTC+04');
INSERT INTO timezone VALUES (332, 'Europe/Oslo', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (333, 'Europe/Paris', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (334, 'Europe/Podgorica', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (335, 'Europe/Prague', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (336, 'Europe/Riga', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (337, 'Europe/Rome', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (338, 'Europe/Samara', 'UTC+03', 'UTC+04');
INSERT INTO timezone VALUES (339, 'Europe/San_Marino', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (340, 'Europe/Sarajevo', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (341, 'Europe/Simferopol', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (342, 'Europe/Skopje', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (343, 'Europe/Sofia', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (344, 'Europe/Stockholm', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (345, 'Europe/Tallinn', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (346, 'Europe/Tirane', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (347, 'Europe/Uzhgorod', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (348, 'Europe/Vaduz', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (349, 'Europe/Vatican', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (350, 'Europe/Vienna', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (351, 'Europe/Vilnius', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (352, 'Europe/Volgograd', 'UTC+03', 'UTC+04');
INSERT INTO timezone VALUES (353, 'Europe/Warsaw', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (354, 'Europe/Zagreb', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (355, 'Europe/Zaporozhye', 'UTC+02', 'UTC+03');
INSERT INTO timezone VALUES (356, 'Europe/Zurich', 'UTC+01', 'UTC+02');
INSERT INTO timezone VALUES (357, 'Indian/Antananarivo', 'UTC+03', '-');
INSERT INTO timezone VALUES (358, 'Indian/Chagos', 'UTC+06', '-');
INSERT INTO timezone VALUES (359, 'Indian/Christmas', 'UTC+07', '-');
INSERT INTO timezone VALUES (360, 'Indian/Cocos', 'UTC+06:30', '-');
INSERT INTO timezone VALUES (361, 'Indian/Comoro', 'UTC+03', '-');
INSERT INTO timezone VALUES (362, 'Indian/Kerguelen', 'UTC+05', '-');
INSERT INTO timezone VALUES (363, 'Indian/Mahe', 'UTC+04', '-');
INSERT INTO timezone VALUES (364, 'Indian/Maldives', 'UTC+05', '-');
INSERT INTO timezone VALUES (365, 'Indian/Mauritius', 'UTC+04', '-');
INSERT INTO timezone VALUES (366, 'Indian/Mayotte', 'UTC+03', '-');
INSERT INTO timezone VALUES (367, 'Indian/Reunion', 'UTC+04', '-');
INSERT INTO timezone VALUES (368, 'Pacific/Apia', 'UTC-11', 'UTC-10');
INSERT INTO timezone VALUES (369, 'Pacific/Auckland', 'UTC+12', 'UTC+13');
INSERT INTO timezone VALUES (370, 'Pacific/Chatham', 'UTC+12:45', 'UTC+13:45');
INSERT INTO timezone VALUES (371, 'Pacific/Easter', 'UTC-06', 'UTC-05');
INSERT INTO timezone VALUES (372, 'Pacific/Efate', 'UTC+11', '-');
INSERT INTO timezone VALUES (373, 'Pacific/Enderbury', 'UTC+13', '-');
INSERT INTO timezone VALUES (374, 'Pacific/Fakaofo', 'UTC-10', '-');
INSERT INTO timezone VALUES (375, 'Pacific/Fiji', 'UTC+12', 'UTC+13');
INSERT INTO timezone VALUES (376, 'Pacific/Funafuti', 'UTC+12', '-');
INSERT INTO timezone VALUES (377, 'Pacific/Galapagos', 'UTC-06', '-');
INSERT INTO timezone VALUES (378, 'Pacific/Gambier', 'UTC-09', '-');
INSERT INTO timezone VALUES (379, 'Pacific/Guadalcanal', 'UTC+11', '-');
INSERT INTO timezone VALUES (380, 'Pacific/Guam', 'UTC+10', '-');
INSERT INTO timezone VALUES (381, 'Pacific/Honolulu', 'UTC-10', '-');
INSERT INTO timezone VALUES (382, 'Pacific/Johnston', 'UTC-10', '-');
INSERT INTO timezone VALUES (383, 'Pacific/Kiritimati', 'UTC+14', '-');
INSERT INTO timezone VALUES (384, 'Pacific/Kosrae', 'UTC+11', '-');
INSERT INTO timezone VALUES (385, 'Pacific/Kwajalein', 'UTC+12', '-');
INSERT INTO timezone VALUES (386, 'Pacific/Majuro', 'UTC+12', '-');
INSERT INTO timezone VALUES (387, 'Pacific/Marquesas', 'UTC-09:30', '-');
INSERT INTO timezone VALUES (388, 'Pacific/Midway', 'UTC-11', '-');
INSERT INTO timezone VALUES (389, 'Pacific/Nauru', 'UTC+12', '-');
INSERT INTO timezone VALUES (390, 'Pacific/Niue', 'UTC-11', '-');
INSERT INTO timezone VALUES (391, 'Pacific/Norfolk', 'UTC+11:30', '-');
INSERT INTO timezone VALUES (392, 'Pacific/Noumea', 'UTC+11', '-');
INSERT INTO timezone VALUES (393, 'Pacific/Pago_Pago', 'UTC-11', '-');
INSERT INTO timezone VALUES (394, 'Pacific/Palau', 'UTC+09', '-');
INSERT INTO timezone VALUES (395, 'Pacific/Pitcairn', 'UTC-08', '-');
INSERT INTO timezone VALUES (396, 'Pacific/Ponape', 'UTC+11', '-');
INSERT INTO timezone VALUES (397, 'Pacific/Port_Moresby', 'UTC+10', '-');
INSERT INTO timezone VALUES (398, 'Pacific/Rarotonga', 'UTC-10', '-');
INSERT INTO timezone VALUES (399, 'Pacific/Saipan', 'UTC+10', '-');
INSERT INTO timezone VALUES (400, 'Pacific/Tahiti', 'UTC-10', '-');
INSERT INTO timezone VALUES (401, 'Pacific/Tarawa', 'UTC+12', '-');
INSERT INTO timezone VALUES (402, 'Pacific/Tongatapu', 'UTC+13', '-');
INSERT INTO timezone VALUES (403, 'Pacific/Truk', 'UTC+10', '-');
INSERT INTO timezone VALUES (404, 'Pacific/Wake', 'UTC+12', '-');
INSERT INTO timezone VALUES (405, 'Pacific/Wallis', 'UTC+12', '-');


--
-- TOC entry 2501 (class 0 OID 37165)
-- Dependencies: 191
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario VALUES ('admin', 'e2b43749d602142dd089f31a6bc03ab4', 'Admin', 'Admin', true, '2011-10-10 16:04:27', '2012-05-22 08:57:05', NULL, true);


--
-- TOC entry 2502 (class 0 OID 37170)
-- Dependencies: 192
-- Data for Name: usuario_has_grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2503 (class 0 OID 37175)
-- Dependencies: 194
-- Data for Name: vid_camara; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2504 (class 0 OID 37189)
-- Dependencies: 196
-- Data for Name: vid_camara_axisguard; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2472 (class 0 OID 36964)
-- Dependencies: 141
-- Data for Name: vid_camara_axisguard_tour; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2505 (class 0 OID 37201)
-- Dependencies: 198
-- Data for Name: vid_camara_axispresetpos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2506 (class 0 OID 37207)
-- Dependencies: 200
-- Data for Name: vid_camara_horario; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2507 (class 0 OID 37213)
-- Dependencies: 202
-- Data for Name: vid_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2508 (class 0 OID 37222)
-- Dependencies: 204
-- Data for Name: vid_marca_camara; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO vid_marca_camara VALUES (12, '3gpp');
INSERT INTO vid_marca_camara VALUES (11, 'AvTech');
INSERT INTO vid_marca_camara VALUES (8, 'Axis');


--
-- TOC entry 2509 (class 0 OID 37228)
-- Dependencies: 206
-- Data for Name: vid_modelo_camara; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO vid_modelo_camara VALUES (2, 8, '214');
INSERT INTO vid_modelo_camara VALUES (7, 11, '211');
INSERT INTO vid_modelo_camara VALUES (8, 12, '3gpp-IP');
INSERT INTO vid_modelo_camara VALUES (9, 8, '210');


--
-- TOC entry 2510 (class 0 OID 37232)
-- Dependencies: 207
-- Data for Name: vid_motionconf; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2511 (class 0 OID 37361)
-- Dependencies: 208
-- Data for Name: vid_plantafisica_zona; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2512 (class 0 OID 37364)
-- Dependencies: 209
-- Data for Name: vid_servidor; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2513 (class 0 OID 37376)
-- Dependencies: 211
-- Data for Name: vid_tipo_camara; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO vid_tipo_camara VALUES (1, 2, 'IPPTZ', true, '/axis-cgi/jpg/image.cgi', '/axis-cgi/mjpg/video.cgi', 'ptz que se mueve para 
acá y para allá...');
INSERT INTO vid_tipo_camara VALUES (3, 7, 'IP', false, '/cgi-bin/guest/Video.cgi?media=MJPEG', '/cgi-bin/guest/Video.cgi?media=MJPEG', 'Camara China');
INSERT INTO vid_tipo_camara VALUES (4, 8, '3gpp-IP-CAMARA', false, '/videostream.cgi', '/videostream.cgi', NULL);


--
-- TOC entry 2514 (class 0 OID 37385)
-- Dependencies: 213
-- Data for Name: vid_zona; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2515 (class 0 OID 37389)
-- Dependencies: 214
-- Data for Name: vid_zona_camara; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2516 (class 0 OID 37392)
-- Dependencies: 215
-- Data for Name: vid_zona_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2232 (class 2606 OID 37396)
-- Dependencies: 143 143
-- Name: accesopc_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accesopc
    ADD CONSTRAINT accesopc_pkey PRIMARY KEY (idaccesopc);


--
-- TOC entry 2237 (class 2606 OID 37399)
-- Dependencies: 144 144
-- Name: accesopc_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accesopc_tipo
    ADD CONSTRAINT accesopc_tipo_pkey PRIMARY KEY (idaccesopc_tipo);


--
-- TOC entry 2234 (class 2606 OID 43946)
-- Dependencies: 143 143 143 143
-- Name: accesopc_usuario_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accesopc
    ADD CONSTRAINT accesopc_usuario_ukey UNIQUE (usuario, ipv4_pc, idaccesopc_tipo);


--
-- TOC entry 2239 (class 2606 OID 37401)
-- Dependencies: 146 146
-- Name: car_cargo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_cargo
    ADD CONSTRAINT car_cargo_pkey PRIMARY KEY (idcargo);


--
-- TOC entry 2241 (class 2606 OID 37404)
-- Dependencies: 148 148
-- Name: car_departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_departamento
    ADD CONSTRAINT car_departamento_pkey PRIMARY KEY (iddepartamento);


--
-- TOC entry 2243 (class 2606 OID 43948)
-- Dependencies: 150 150
-- Name: car_empleado_cedula_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_empleado
    ADD CONSTRAINT car_empleado_cedula_ukey UNIQUE (cedula);


--
-- TOC entry 2248 (class 2606 OID 37413)
-- Dependencies: 152 152
-- Name: car_empleado_impresion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_empleado_impresion
    ADD CONSTRAINT car_empleado_impresion_pkey PRIMARY KEY (idcarnet_empleado_impresion);


--
-- TOC entry 2245 (class 2606 OID 37407)
-- Dependencies: 150 150
-- Name: car_empleado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_empleado
    ADD CONSTRAINT car_empleado_pkey PRIMARY KEY (idcarnet_empleado);


--
-- TOC entry 2251 (class 2606 OID 37416)
-- Dependencies: 154 154
-- Name: car_plantilla_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_plantilla
    ADD CONSTRAINT car_plantilla_pkey PRIMARY KEY (idplantilla);


--
-- TOC entry 2253 (class 2606 OID 43950)
-- Dependencies: 154 154
-- Name: car_plantilla_plantilla_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_plantilla
    ADD CONSTRAINT car_plantilla_plantilla_ukey UNIQUE (plantilla);


--
-- TOC entry 2259 (class 2606 OID 37422)
-- Dependencies: 158 158
-- Name: car_visitante_impresion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_visitante_impresion
    ADD CONSTRAINT car_visitante_impresion_pkey PRIMARY KEY (idcarnet_visitante_impresion);


--
-- TOC entry 2256 (class 2606 OID 37419)
-- Dependencies: 156 156
-- Name: car_visitante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY car_visitante
    ADD CONSTRAINT car_visitante_pkey PRIMARY KEY (idcarnet_visitante);


--
-- TOC entry 2262 (class 2606 OID 37425)
-- Dependencies: 159 159
-- Name: categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (idcategoria);


--
-- TOC entry 2265 (class 2606 OID 43944)
-- Dependencies: 161 161 161 161
-- Name: ctrl_controladora_nodo_unk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_controladora
    ADD CONSTRAINT ctrl_controladora_nodo_unk UNIQUE (nodo, ipv4, puerto);


--
-- TOC entry 2267 (class 2606 OID 37428)
-- Dependencies: 161 161
-- Name: ctrl_controladora_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_controladora
    ADD CONSTRAINT ctrl_controladora_pkey PRIMARY KEY (idcontroladora);


--
-- TOC entry 2270 (class 2606 OID 37433)
-- Dependencies: 162 162
-- Name: ctrl_diafestivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_diafestivo
    ADD CONSTRAINT ctrl_diafestivo_pkey PRIMARY KEY (id);


--
-- TOC entry 2272 (class 2606 OID 37435)
-- Dependencies: 164 164
-- Name: ctrl_grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_grupo
    ADD CONSTRAINT ctrl_grupo_pkey PRIMARY KEY (idgrupo);


--
-- TOC entry 2275 (class 2606 OID 37439)
-- Dependencies: 165 165 165
-- Name: ctrl_grupo_puerta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_grupo_puerta
    ADD CONSTRAINT ctrl_grupo_puerta_pkey PRIMARY KEY (idgrupo, idpuerta);


--
-- TOC entry 2279 (class 2606 OID 43952)
-- Dependencies: 167 167 167
-- Name: ctrl_lectora_numero_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_lectora
    ADD CONSTRAINT ctrl_lectora_numero_ukey UNIQUE (idcontroladora, numero);


--
-- TOC entry 2281 (class 2606 OID 37443)
-- Dependencies: 167 167
-- Name: ctrl_lectora_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_lectora
    ADD CONSTRAINT ctrl_lectora_pkey PRIMARY KEY (idlectora);


--
-- TOC entry 2283 (class 2606 OID 43954)
-- Dependencies: 167 167
-- Name: ctrl_lectora_puerta_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_lectora
    ADD CONSTRAINT ctrl_lectora_puerta_ukey UNIQUE (idpuerta);


--
-- TOC entry 2288 (class 2606 OID 37448)
-- Dependencies: 169 169
-- Name: ctrl_logacceso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_logacceso
    ADD CONSTRAINT ctrl_logacceso_pkey PRIMARY KEY (id);


--
-- TOC entry 2290 (class 2606 OID 43956)
-- Dependencies: 171 171
-- Name: ctrl_puerta_numero_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_puerta
    ADD CONSTRAINT ctrl_puerta_numero_ukey UNIQUE (numero);


--
-- TOC entry 2292 (class 2606 OID 37450)
-- Dependencies: 171 171
-- Name: ctrl_puerta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_puerta
    ADD CONSTRAINT ctrl_puerta_pkey PRIMARY KEY (idpuerta);


--
-- TOC entry 2294 (class 2606 OID 43958)
-- Dependencies: 171 171
-- Name: ctrl_puerta_puerta_entrada_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_puerta
    ADD CONSTRAINT ctrl_puerta_puerta_entrada_ukey UNIQUE (puerta_entrada);


--
-- TOC entry 2297 (class 2606 OID 37454)
-- Dependencies: 173 173
-- Name: ctrl_sincronizar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_sincronizar
    ADD CONSTRAINT ctrl_sincronizar_pkey PRIMARY KEY (id);


--
-- TOC entry 2299 (class 2606 OID 37456)
-- Dependencies: 175 175
-- Name: ctrl_tipo_controladora_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_tipo_controladora
    ADD CONSTRAINT ctrl_tipo_controladora_pkey PRIMARY KEY (idtipo_controladora);


--
-- TOC entry 2304 (class 2606 OID 43970)
-- Dependencies: 177 177
-- Name: ctrl_usuario_cedula_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_cedula_ukey UNIQUE (cedula);


--
-- TOC entry 2306 (class 2606 OID 43968)
-- Dependencies: 177 177
-- Name: ctrl_usuario_codigo_id_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_codigo_id_ukey UNIQUE (codigo_id);


--
-- TOC entry 2308 (class 2606 OID 43964)
-- Dependencies: 177 177
-- Name: ctrl_usuario_numero_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_numero_key UNIQUE (numero);


--
-- TOC entry 2310 (class 2606 OID 37458)
-- Dependencies: 177 177
-- Name: ctrl_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_pkey PRIMARY KEY (idusuario);


--
-- TOC entry 2312 (class 2606 OID 43966)
-- Dependencies: 177 177 177
-- Name: ctrl_usuario_tarjeta_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_tarjeta_ukey UNIQUE (codigo_sitio, codigo_tarjeta);


--
-- TOC entry 2318 (class 2606 OID 37467)
-- Dependencies: 178 178
-- Name: ctrl_zonadetiempo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ctrl_zonadetiempo
    ADD CONSTRAINT ctrl_zonadetiempo_pkey PRIMARY KEY (indice);


--
-- TOC entry 2321 (class 2606 OID 43977)
-- Dependencies: 180 180 180
-- Name: funcion_idcategoria_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_idcategoria_ukey UNIQUE (idfuncion, idcategoria);


--
-- TOC entry 2323 (class 2606 OID 37469)
-- Dependencies: 180 180
-- Name: funcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_pkey PRIMARY KEY (id);


--
-- TOC entry 2329 (class 2606 OID 37475)
-- Dependencies: 183 183 183
-- Name: grupo_has_funcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grupo_has_funcion
    ADD CONSTRAINT grupo_has_funcion_pkey PRIMARY KEY (grupo_idgrupo, funcion_id);


--
-- TOC entry 2326 (class 2606 OID 37473)
-- Dependencies: 182 182
-- Name: grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grupo
    ADD CONSTRAINT grupo_pkey PRIMARY KEY (idgrupo);


--
-- TOC entry 2332 (class 2606 OID 43989)
-- Dependencies: 185 185
-- Name: plantafisica_file_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plantafisica
    ADD CONSTRAINT plantafisica_file_ukey UNIQUE (file);


--
-- TOC entry 2335 (class 2606 OID 43991)
-- Dependencies: 185 185
-- Name: plantafisica_mame_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plantafisica
    ADD CONSTRAINT plantafisica_mame_ukey UNIQUE (plantafisica);


--
-- TOC entry 2337 (class 2606 OID 37478)
-- Dependencies: 185 185
-- Name: plantafisica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plantafisica
    ADD CONSTRAINT plantafisica_pkey PRIMARY KEY (idplantafisica);


--
-- TOC entry 2340 (class 2606 OID 37482)
-- Dependencies: 187 187
-- Name: segmento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY segmento
    ADD CONSTRAINT segmento_pkey PRIMARY KEY (idsegmento);


--
-- TOC entry 2342 (class 2606 OID 43993)
-- Dependencies: 187 187
-- Name: segmento_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY segmento
    ADD CONSTRAINT segmento_ukey UNIQUE (segmento);


--
-- TOC entry 2344 (class 2606 OID 44005)
-- Dependencies: 188 188 188
-- Name: segmento_usuario_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY segmento_usuario
    ADD CONSTRAINT segmento_usuario_ukey UNIQUE (idsegmento, usuario);


--
-- TOC entry 2347 (class 2606 OID 44019)
-- Dependencies: 190 190
-- Name: timezone_location_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY timezone
    ADD CONSTRAINT timezone_location_ukey UNIQUE (location);


--
-- TOC entry 2349 (class 2606 OID 37486)
-- Dependencies: 190 190
-- Name: timezone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY timezone
    ADD CONSTRAINT timezone_pkey PRIMARY KEY (idtimezone);


--
-- TOC entry 2355 (class 2606 OID 44007)
-- Dependencies: 192 192 192
-- Name: usuario_grupo_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario_has_grupo
    ADD CONSTRAINT usuario_grupo_ukey UNIQUE (usuario_usuario, grupo_idgrupo);


--
-- TOC entry 2357 (class 2606 OID 37491)
-- Dependencies: 192 192 192
-- Name: usuario_has_grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario_has_grupo
    ADD CONSTRAINT usuario_has_grupo_pkey PRIMARY KEY (usuario_usuario, grupo_idgrupo);


--
-- TOC entry 2351 (class 2606 OID 37489)
-- Dependencies: 191 191
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (usuario);


--
-- TOC entry 2370 (class 2606 OID 37502)
-- Dependencies: 196 196
-- Name: vid_camara_axisguard_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara_axisguard
    ADD CONSTRAINT vid_camara_axisguard_pkey PRIMARY KEY (idcamaraaxisguard);


--
-- TOC entry 2228 (class 2606 OID 44041)
-- Dependencies: 141 141
-- Name: vid_camara_axisguard_tour_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara_axisguard_tour
    ADD CONSTRAINT vid_camara_axisguard_tour_pkey PRIMARY KEY (idvidcamaraaxisguardtour);


--
-- TOC entry 2230 (class 2606 OID 44043)
-- Dependencies: 141 141 141
-- Name: vid_camara_axisguard_tour_position_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara_axisguard_tour
    ADD CONSTRAINT vid_camara_axisguard_tour_position_pkey UNIQUE (idcamaraaxisguard, "position");


--
-- TOC entry 2374 (class 2606 OID 44057)
-- Dependencies: 198 198 198
-- Name: vid_camara_axispresetpos_camara_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara_axispresetpos
    ADD CONSTRAINT vid_camara_axispresetpos_camara_ukey UNIQUE (idcamara, presetname);


--
-- TOC entry 2376 (class 2606 OID 37505)
-- Dependencies: 198 198
-- Name: vid_camara_axispresetpos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara_axispresetpos
    ADD CONSTRAINT vid_camara_axispresetpos_pkey PRIMARY KEY (idcamaraaxispresetpos);


--
-- TOC entry 2378 (class 2606 OID 37509)
-- Dependencies: 200 200
-- Name: vid_camara_horario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara_horario
    ADD CONSTRAINT vid_camara_horario_pkey PRIMARY KEY (idcamarahorario);


--
-- TOC entry 2363 (class 2606 OID 44023)
-- Dependencies: 194 194 194
-- Name: vid_camara_ipv4_uk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara
    ADD CONSTRAINT vid_camara_ipv4_uk UNIQUE (idservidor, ipv4);


--
-- TOC entry 2365 (class 2606 OID 44021)
-- Dependencies: 194 194 194
-- Name: vid_camara_numero_uk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara
    ADD CONSTRAINT vid_camara_numero_uk UNIQUE (idservidor, numero);


--
-- TOC entry 2367 (class 2606 OID 37495)
-- Dependencies: 194 194
-- Name: vid_camara_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_camara
    ADD CONSTRAINT vid_camara_pkey PRIMARY KEY (idcamara);


--
-- TOC entry 2382 (class 2606 OID 44064)
-- Dependencies: 202 202 202
-- Name: vid_evento_fecha_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_evento
    ADD CONSTRAINT vid_evento_fecha_ukey UNIQUE (fecha, descripcion);


--
-- TOC entry 2384 (class 2606 OID 37512)
-- Dependencies: 202 202
-- Name: vid_evento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_evento
    ADD CONSTRAINT vid_evento_pkey PRIMARY KEY (idevento);


--
-- TOC entry 2387 (class 2606 OID 44071)
-- Dependencies: 204 204
-- Name: vid_marca_camara_nombre_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_marca_camara
    ADD CONSTRAINT vid_marca_camara_nombre_ukey UNIQUE (nombre_marca_camara);


--
-- TOC entry 2389 (class 2606 OID 37516)
-- Dependencies: 204 204
-- Name: vid_marca_camara_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_marca_camara
    ADD CONSTRAINT vid_marca_camara_pkey PRIMARY KEY (idmarca_camara);


--
-- TOC entry 2392 (class 2606 OID 37519)
-- Dependencies: 206 206
-- Name: vid_modelo_camara_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_modelo_camara
    ADD CONSTRAINT vid_modelo_camara_pkey PRIMARY KEY (idmodelo_camara);


--
-- TOC entry 2394 (class 2606 OID 44078)
-- Dependencies: 207 207
-- Name: vid_motionconf_ipv4_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_motionconf
    ADD CONSTRAINT vid_motionconf_ipv4_ukey UNIQUE (ipv4);


--
-- TOC entry 2396 (class 2606 OID 37522)
-- Dependencies: 207 207
-- Name: vid_motionconf_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_motionconf
    ADD CONSTRAINT vid_motionconf_pkey PRIMARY KEY (idservidor);


--
-- TOC entry 2399 (class 2606 OID 44085)
-- Dependencies: 208 208 208
-- Name: vid_plantafisica_zona_plantafisica_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_plantafisica_zona
    ADD CONSTRAINT vid_plantafisica_zona_plantafisica_ukey UNIQUE (idplantafisica, idzona);


--
-- TOC entry 2404 (class 2606 OID 44097)
-- Dependencies: 209 209
-- Name: vid_servidor_ipv4_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_servidor
    ADD CONSTRAINT vid_servidor_ipv4_ukey UNIQUE (ipv4);


--
-- TOC entry 2406 (class 2606 OID 37527)
-- Dependencies: 209 209
-- Name: vid_servidor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_servidor
    ADD CONSTRAINT vid_servidor_pkey PRIMARY KEY (idservidor);


--
-- TOC entry 2409 (class 2606 OID 37532)
-- Dependencies: 211 211
-- Name: vid_tipo_camara_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_tipo_camara
    ADD CONSTRAINT vid_tipo_camara_pkey PRIMARY KEY (idtipo_camara);


--
-- TOC entry 2417 (class 2606 OID 44116)
-- Dependencies: 214 214 214
-- Name: vid_zona_camara_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_zona_camara
    ADD CONSTRAINT vid_zona_camara_ukey UNIQUE (idzona, idcamara);


--
-- TOC entry 2411 (class 2606 OID 37535)
-- Dependencies: 213 213
-- Name: vid_zona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_zona
    ADD CONSTRAINT vid_zona_pkey PRIMARY KEY (idzona);


--
-- TOC entry 2413 (class 2606 OID 44114)
-- Dependencies: 213 213
-- Name: vid_zona_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_zona
    ADD CONSTRAINT vid_zona_ukey UNIQUE (zona);


--
-- TOC entry 2420 (class 2606 OID 44128)
-- Dependencies: 215 215 215
-- Name: vid_zona_usuario_ukey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vid_zona_usuario
    ADD CONSTRAINT vid_zona_usuario_ukey UNIQUE (idzona, usuario);


--
-- TOC entry 2300 (class 1259 OID 37462)
-- Dependencies: 177
-- Name: cedula; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX cedula ON ctrl_usuario USING btree (cedula);


--
-- TOC entry 2301 (class 1259 OID 37461)
-- Dependencies: 177
-- Name: codigo_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX codigo_id ON ctrl_usuario USING btree (codigo_id);


--
-- TOC entry 2302 (class 1259 OID 37460)
-- Dependencies: 177 177
-- Name: codigo_sitio; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX codigo_sitio ON ctrl_usuario USING btree (codigo_sitio, codigo_tarjeta);


--
-- TOC entry 2330 (class 1259 OID 37479)
-- Dependencies: 185
-- Name: file; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX file ON plantafisica USING btree (file);


--
-- TOC entry 2263 (class 1259 OID 37426)
-- Dependencies: 159
-- Name: fk_categoria_categoria1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_categoria_categoria1 ON categoria USING btree (categoria_padre);


--
-- TOC entry 2268 (class 1259 OID 37430)
-- Dependencies: 161
-- Name: fk_ctrl_controladora_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_ctrl_controladora_1 ON ctrl_controladora USING btree (idtipo_controladora);


--
-- TOC entry 2273 (class 1259 OID 37437)
-- Dependencies: 164
-- Name: fk_ctrl_grupo_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_ctrl_grupo_1 ON ctrl_grupo USING btree (idgrupo_padre);


--
-- TOC entry 2276 (class 1259 OID 37440)
-- Dependencies: 165
-- Name: fk_ctrl_grupo_puerta_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_ctrl_grupo_puerta_1 ON ctrl_grupo_puerta USING btree (idgrupo);


--
-- TOC entry 2277 (class 1259 OID 37441)
-- Dependencies: 165
-- Name: fk_ctrl_grupo_puerta_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_ctrl_grupo_puerta_2 ON ctrl_grupo_puerta USING btree (idpuerta);


--
-- TOC entry 2313 (class 1259 OID 37463)
-- Dependencies: 177
-- Name: fk_ctrl_usuario_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_ctrl_usuario_1 ON ctrl_usuario USING btree (idgrupo);


--
-- TOC entry 2314 (class 1259 OID 37464)
-- Dependencies: 177
-- Name: fk_ctrl_usuario_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_ctrl_usuario_2 ON ctrl_usuario USING btree (idzonadetiempo);


--
-- TOC entry 2319 (class 1259 OID 37471)
-- Dependencies: 180
-- Name: fk_funcion_categoria4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_funcion_categoria4 ON funcion USING btree (idcategoria);


--
-- TOC entry 2352 (class 1259 OID 37493)
-- Dependencies: 192
-- Name: fk_usuario_has_grupo_grupo1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_usuario_has_grupo_grupo1 ON usuario_has_grupo USING btree (grupo_idgrupo);


--
-- TOC entry 2353 (class 1259 OID 37492)
-- Dependencies: 192
-- Name: fk_usuario_has_grupo_usuario1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_usuario_has_grupo_usuario1 ON usuario_has_grupo USING btree (usuario_usuario);


--
-- TOC entry 2368 (class 1259 OID 37503)
-- Dependencies: 196
-- Name: fk_vid_camara_axisguard_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_vid_camara_axisguard_1 ON vid_camara_axisguard USING btree (idcamara);


--
-- TOC entry 2371 (class 1259 OID 37507)
-- Dependencies: 198
-- Name: fk_vid_camara_axispresetpos_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fk_vid_camara_axispresetpos_1 ON vid_camara_axispresetpos USING btree (idcamara);


--
-- TOC entry 2225 (class 1259 OID 44049)
-- Dependencies: 141
-- Name: fki_vid_camara_axisguard_tour_idcamaraaxisguard_fkey; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_vid_camara_axisguard_tour_idcamaraaxisguard_fkey ON vid_camara_axisguard_tour USING btree (idcamaraaxisguard);


--
-- TOC entry 2226 (class 1259 OID 44055)
-- Dependencies: 141
-- Name: fki_vid_camara_axisguard_tour_idcamaraaxispresetpos_fkey; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_vid_camara_axisguard_tour_idcamaraaxispresetpos_fkey ON vid_camara_axisguard_tour USING btree (idcamaraaxispresetpos);


--
-- TOC entry 2327 (class 1259 OID 37476)
-- Dependencies: 183
-- Name: funcion_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX funcion_id ON grupo_has_funcion USING btree (funcion_id);


--
-- TOC entry 2415 (class 1259 OID 37538)
-- Dependencies: 214
-- Name: idcamara; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idcamara ON vid_zona_camara USING btree (idcamara);


--
-- TOC entry 2246 (class 1259 OID 37411)
-- Dependencies: 150
-- Name: idcargo; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idcargo ON car_empleado USING btree (idcargo);


--
-- TOC entry 2249 (class 1259 OID 37414)
-- Dependencies: 152
-- Name: idcarnet_empleado; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idcarnet_empleado ON car_empleado_impresion USING btree (idcarnet_empleado);


--
-- TOC entry 2260 (class 1259 OID 37423)
-- Dependencies: 158
-- Name: idcarnet_visitante; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idcarnet_visitante ON car_visitante_impresion USING btree (idcarnet_visitante);


--
-- TOC entry 2284 (class 1259 OID 37444)
-- Dependencies: 167 167
-- Name: idcontroladora; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX idcontroladora ON ctrl_lectora USING btree (idcontroladora, numero);


--
-- TOC entry 2285 (class 1259 OID 37445)
-- Dependencies: 167
-- Name: idcontroladora_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX idcontroladora_2 ON ctrl_lectora USING btree (idpuerta);


--
-- TOC entry 2315 (class 1259 OID 37465)
-- Dependencies: 177
-- Name: iddepartamento; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX iddepartamento ON ctrl_usuario USING btree (iddepartamento);


--
-- TOC entry 2324 (class 1259 OID 37470)
-- Dependencies: 180 180
-- Name: idfuncion; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX idfuncion ON funcion USING btree (idfuncion, idcategoria);


--
-- TOC entry 2390 (class 1259 OID 37520)
-- Dependencies: 206
-- Name: idmarca_camara; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idmarca_camara ON vid_modelo_camara USING btree (idmarca_camara);


--
-- TOC entry 2407 (class 1259 OID 37533)
-- Dependencies: 211
-- Name: idmodelo_camara; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idmodelo_camara ON vid_tipo_camara USING btree (idmodelo_camara);


--
-- TOC entry 2397 (class 1259 OID 37524)
-- Dependencies: 208 208
-- Name: idplantafisica; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX idplantafisica ON vid_plantafisica_zona USING btree (idplantafisica, idzona);


--
-- TOC entry 2257 (class 1259 OID 37420)
-- Dependencies: 156
-- Name: idplantilla; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idplantilla ON car_visitante USING btree (idplantilla);


--
-- TOC entry 2400 (class 1259 OID 37530)
-- Dependencies: 209
-- Name: idsegmento; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idsegmento ON vid_servidor USING btree (idsegmento);


--
-- TOC entry 2379 (class 1259 OID 37514)
-- Dependencies: 202
-- Name: idservidor; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idservidor ON vid_evento USING btree (idservidor);


--
-- TOC entry 2358 (class 1259 OID 37500)
-- Dependencies: 194
-- Name: idtipo_camara; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idtipo_camara ON vid_camara USING btree (idtipo_camara);


--
-- TOC entry 2418 (class 1259 OID 37539)
-- Dependencies: 215 215
-- Name: idzona; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX idzona ON vid_zona_usuario USING btree (idzona, usuario);


--
-- TOC entry 2359 (class 1259 OID 37498)
-- Dependencies: 194
-- Name: ind_idserviodr_vc; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ind_idserviodr_vc ON vid_camara USING btree (idservidor);


--
-- TOC entry 2401 (class 1259 OID 37529)
-- Dependencies: 209
-- Name: ind_tz_vs; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ind_tz_vs ON vid_servidor USING btree (idtimezone);


--
-- TOC entry 2286 (class 1259 OID 37446)
-- Dependencies: 167
-- Name: index2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index2 ON ctrl_lectora USING btree (idcontroladora);


--
-- TOC entry 2402 (class 1259 OID 37528)
-- Dependencies: 209
-- Name: ipv4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ipv4 ON vid_servidor USING btree (ipv4);


--
-- TOC entry 2345 (class 1259 OID 37487)
-- Dependencies: 190
-- Name: location; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX location ON timezone USING btree (location);


--
-- TOC entry 2385 (class 1259 OID 37517)
-- Dependencies: 204
-- Name: nombre_marca_camara; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX nombre_marca_camara ON vid_marca_camara USING btree (nombre_marca_camara);


--
-- TOC entry 2316 (class 1259 OID 37459)
-- Dependencies: 177
-- Name: numero; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX numero ON ctrl_usuario USING btree (numero);


--
-- TOC entry 2333 (class 1259 OID 37480)
-- Dependencies: 185
-- Name: plantafisica_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX plantafisica_index ON plantafisica USING btree (plantafisica);


--
-- TOC entry 2254 (class 1259 OID 37417)
-- Dependencies: 154
-- Name: plantilla; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX plantilla ON car_plantilla USING btree (plantilla);


--
-- TOC entry 2372 (class 1259 OID 37506)
-- Dependencies: 198 198
-- Name: presetname; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX presetname ON vid_camara_axispresetpos USING btree (presetname, idcamara);


--
-- TOC entry 2295 (class 1259 OID 37452)
-- Dependencies: 171
-- Name: puerta_entrada; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX puerta_entrada ON ctrl_puerta USING btree (puerta_entrada);


--
-- TOC entry 2338 (class 1259 OID 37483)
-- Dependencies: 187
-- Name: segmento_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX segmento_index ON segmento USING btree (segmento);


--
-- TOC entry 2360 (class 1259 OID 37497)
-- Dependencies: 194 194
-- Name: unk_ip_vc; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unk_ip_vc ON vid_camara USING btree (ipv4, idservidor);


--
-- TOC entry 2361 (class 1259 OID 37496)
-- Dependencies: 194 194
-- Name: unk_num_vc; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unk_num_vc ON vid_camara USING btree (idservidor, numero);


--
-- TOC entry 2380 (class 1259 OID 37513)
-- Dependencies: 202 202
-- Name: unq_datedesc_ve; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unq_datedesc_ve ON vid_evento USING btree (fecha, descripcion);


--
-- TOC entry 2235 (class 1259 OID 37397)
-- Dependencies: 143 143 143
-- Name: usuario_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX usuario_2 ON accesopc USING btree (usuario, ipv4_pc, idaccesopc_tipo);


--
-- TOC entry 2414 (class 1259 OID 37536)
-- Dependencies: 213
-- Name: zona; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX zona ON vid_zona USING btree (zona);


--
-- TOC entry 2423 (class 2606 OID 37540)
-- Dependencies: 144 143 2236
-- Name: accesopc_idaccesopc_tipo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesopc
    ADD CONSTRAINT accesopc_idaccesopc_tipo_fkey FOREIGN KEY (idaccesopc_tipo) REFERENCES accesopc_tipo(idaccesopc_tipo);


--
-- TOC entry 2424 (class 2606 OID 43883)
-- Dependencies: 2350 191 143
-- Name: accesopc_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesopc
    ADD CONSTRAINT accesopc_usuario_fkey FOREIGN KEY (usuario) REFERENCES usuario(usuario) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2425 (class 2606 OID 43893)
-- Dependencies: 2250 154 146
-- Name: car_cargo_idplantilla_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_cargo
    ADD CONSTRAINT car_cargo_idplantilla_fkey FOREIGN KEY (idplantilla) REFERENCES car_plantilla(idplantilla) ON UPDATE SET NULL ON DELETE RESTRICT;


--
-- TOC entry 2426 (class 2606 OID 43898)
-- Dependencies: 2250 154 148
-- Name: car_departamento_idplantilla_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_departamento
    ADD CONSTRAINT car_departamento_idplantilla_fkey FOREIGN KEY (idplantilla) REFERENCES car_plantilla(idplantilla) ON UPDATE SET NULL ON DELETE RESTRICT;


--
-- TOC entry 2427 (class 2606 OID 43903)
-- Dependencies: 2238 150 146
-- Name: car_empleado_idcargo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_empleado
    ADD CONSTRAINT car_empleado_idcargo_fkey FOREIGN KEY (idcargo) REFERENCES car_cargo(idcargo) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2428 (class 2606 OID 43908)
-- Dependencies: 2240 150 148
-- Name: car_empleado_iddepartamento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_empleado
    ADD CONSTRAINT car_empleado_iddepartamento_fkey FOREIGN KEY (iddepartamento) REFERENCES car_departamento(iddepartamento) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2429 (class 2606 OID 43913)
-- Dependencies: 150 2250 154
-- Name: car_empleado_idplantilla_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_empleado
    ADD CONSTRAINT car_empleado_idplantilla_fkey FOREIGN KEY (idplantilla) REFERENCES car_plantilla(idplantilla) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2431 (class 2606 OID 43918)
-- Dependencies: 152 150 2244
-- Name: car_empleado_impresion_idcarnet_empleado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_empleado_impresion
    ADD CONSTRAINT car_empleado_impresion_idcarnet_empleado_fkey FOREIGN KEY (idcarnet_empleado) REFERENCES car_empleado(idcarnet_empleado) ON DELETE CASCADE;


--
-- TOC entry 2430 (class 2606 OID 37610)
-- Dependencies: 2350 152 191
-- Name: car_empleado_impresion_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_empleado_impresion
    ADD CONSTRAINT car_empleado_impresion_usuario_fkey FOREIGN KEY (usuario) REFERENCES usuario(usuario);


--
-- TOC entry 2432 (class 2606 OID 43923)
-- Dependencies: 156 154 2250
-- Name: car_visitante_idplantilla_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_visitante
    ADD CONSTRAINT car_visitante_idplantilla_fkey FOREIGN KEY (idplantilla) REFERENCES car_plantilla(idplantilla) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2434 (class 2606 OID 43928)
-- Dependencies: 156 158 2255
-- Name: car_visitante_impresion_idcarnet_visitante_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_visitante_impresion
    ADD CONSTRAINT car_visitante_impresion_idcarnet_visitante_fkey FOREIGN KEY (idcarnet_visitante) REFERENCES car_visitante(idcarnet_visitante) ON DELETE CASCADE;


--
-- TOC entry 2433 (class 2606 OID 37645)
-- Dependencies: 2350 158 191
-- Name: car_visitante_impresion_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY car_visitante_impresion
    ADD CONSTRAINT car_visitante_impresion_usuario_fkey FOREIGN KEY (usuario) REFERENCES usuario(usuario);


--
-- TOC entry 2435 (class 2606 OID 43933)
-- Dependencies: 159 2261 159
-- Name: categoria_categoria_padre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_categoria_padre_fkey FOREIGN KEY (categoria_padre) REFERENCES categoria(idcategoria) ON UPDATE RESTRICT ON DELETE SET NULL;


--
-- TOC entry 2437 (class 2606 OID 43938)
-- Dependencies: 187 161 2339
-- Name: ctrl_controladora_idsegmento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_controladora
    ADD CONSTRAINT ctrl_controladora_idsegmento_fkey FOREIGN KEY (idsegmento) REFERENCES segmento(idsegmento) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2436 (class 2606 OID 37680)
-- Dependencies: 161 2298 175
-- Name: ctrl_controladora_idtipo_controladora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_controladora
    ADD CONSTRAINT ctrl_controladora_idtipo_controladora_fkey FOREIGN KEY (idtipo_controladora) REFERENCES ctrl_tipo_controladora(idtipo_controladora);


--
-- TOC entry 2438 (class 2606 OID 37690)
-- Dependencies: 2271 164 164
-- Name: ctrl_grupo_idgrupo_padre_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_grupo
    ADD CONSTRAINT ctrl_grupo_idgrupo_padre_fkey FOREIGN KEY (idgrupo_padre) REFERENCES ctrl_grupo(idgrupo);


--
-- TOC entry 2439 (class 2606 OID 37700)
-- Dependencies: 164 165 2271
-- Name: ctrl_grupo_puerta_idgrupo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_grupo_puerta
    ADD CONSTRAINT ctrl_grupo_puerta_idgrupo_fkey FOREIGN KEY (idgrupo) REFERENCES ctrl_grupo(idgrupo);


--
-- TOC entry 2440 (class 2606 OID 37705)
-- Dependencies: 165 2291 171
-- Name: ctrl_grupo_puerta_idpuerta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_grupo_puerta
    ADD CONSTRAINT ctrl_grupo_puerta_idpuerta_fkey FOREIGN KEY (idpuerta) REFERENCES ctrl_puerta(idpuerta);


--
-- TOC entry 2441 (class 2606 OID 37720)
-- Dependencies: 2266 161 167
-- Name: ctrl_lectora_idcontroladora_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_lectora
    ADD CONSTRAINT ctrl_lectora_idcontroladora_fkey FOREIGN KEY (idcontroladora) REFERENCES ctrl_controladora(idcontroladora);


--
-- TOC entry 2442 (class 2606 OID 37725)
-- Dependencies: 167 2291 171
-- Name: ctrl_lectora_idpuerta_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_lectora
    ADD CONSTRAINT ctrl_lectora_idpuerta_fkey FOREIGN KEY (idpuerta) REFERENCES ctrl_puerta(idpuerta);


--
-- TOC entry 2443 (class 2606 OID 43971)
-- Dependencies: 177 148 2240
-- Name: ctrl_usuario_iddepartamento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_iddepartamento_fkey FOREIGN KEY (iddepartamento) REFERENCES car_departamento(iddepartamento) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2444 (class 2606 OID 37750)
-- Dependencies: 164 177 2271
-- Name: ctrl_usuario_idgrupo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_idgrupo_fkey FOREIGN KEY (idgrupo) REFERENCES ctrl_grupo(idgrupo);


--
-- TOC entry 2445 (class 2606 OID 37755)
-- Dependencies: 2317 178 177
-- Name: ctrl_usuario_idzonadetiempo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ctrl_usuario
    ADD CONSTRAINT ctrl_usuario_idzonadetiempo_fkey FOREIGN KEY (idzonadetiempo) REFERENCES ctrl_zonadetiempo(indice);


--
-- TOC entry 2446 (class 2606 OID 37770)
-- Dependencies: 180 159 2261
-- Name: funcion_idcategoria_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_idcategoria_fkey FOREIGN KEY (idcategoria) REFERENCES categoria(idcategoria);


--
-- TOC entry 2447 (class 2606 OID 43978)
-- Dependencies: 183 180 2322
-- Name: grupo_has_funcion_funcion_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY grupo_has_funcion
    ADD CONSTRAINT grupo_has_funcion_funcion_id_fkey FOREIGN KEY (funcion_id) REFERENCES funcion(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2448 (class 2606 OID 43983)
-- Dependencies: 183 182 2325
-- Name: grupo_has_funcion_grupo_idgrupo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY grupo_has_funcion
    ADD CONSTRAINT grupo_has_funcion_grupo_idgrupo_fkey FOREIGN KEY (grupo_idgrupo) REFERENCES grupo(idgrupo) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2449 (class 2606 OID 43994)
-- Dependencies: 188 187 2339
-- Name: segmento_usuario_idsegmento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY segmento_usuario
    ADD CONSTRAINT segmento_usuario_idsegmento_fkey FOREIGN KEY (idsegmento) REFERENCES segmento(idsegmento) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2450 (class 2606 OID 43999)
-- Dependencies: 188 191 2350
-- Name: segmento_usuario_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY segmento_usuario
    ADD CONSTRAINT segmento_usuario_usuario_fkey FOREIGN KEY (usuario) REFERENCES usuario(usuario) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2451 (class 2606 OID 44008)
-- Dependencies: 192 182 2325
-- Name: usuario_has_grupo_grupo_idgrupo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario_has_grupo
    ADD CONSTRAINT usuario_has_grupo_grupo_idgrupo_fkey FOREIGN KEY (grupo_idgrupo) REFERENCES grupo(idgrupo) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2452 (class 2606 OID 44013)
-- Dependencies: 192 191 2350
-- Name: usuario_has_grupo_usuario_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario_has_grupo
    ADD CONSTRAINT usuario_has_grupo_usuario_usuario_fkey FOREIGN KEY (usuario_usuario) REFERENCES usuario(usuario) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2456 (class 2606 OID 37870)
-- Dependencies: 196 194 2366
-- Name: vid_camara_axisguard_idcamara_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara_axisguard
    ADD CONSTRAINT vid_camara_axisguard_idcamara_fkey FOREIGN KEY (idcamara) REFERENCES vid_camara(idcamara);


--
-- TOC entry 2422 (class 2606 OID 44044)
-- Dependencies: 141 196 2369
-- Name: vid_camara_axisguard_tour_idcamaraaxisguard_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara_axisguard_tour
    ADD CONSTRAINT vid_camara_axisguard_tour_idcamaraaxisguard_fkey FOREIGN KEY (idcamaraaxisguard) REFERENCES vid_camara_axisguard(idcamaraaxisguard) ON DELETE CASCADE;


--
-- TOC entry 2421 (class 2606 OID 44050)
-- Dependencies: 141 198 2375
-- Name: vid_camara_axisguard_tour_idcamaraaxispresetpos_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara_axisguard_tour
    ADD CONSTRAINT vid_camara_axisguard_tour_idcamaraaxispresetpos_fkey FOREIGN KEY (idcamaraaxispresetpos) REFERENCES vid_camara_axispresetpos(idcamaraaxispresetpos);


--
-- TOC entry 2457 (class 2606 OID 37880)
-- Dependencies: 198 2366 194
-- Name: vid_camara_axispresetpos_idcamara_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara_axispresetpos
    ADD CONSTRAINT vid_camara_axispresetpos_idcamara_fkey FOREIGN KEY (idcamara) REFERENCES vid_camara(idcamara);


--
-- TOC entry 2458 (class 2606 OID 37885)
-- Dependencies: 2366 194 198
-- Name: vid_camara_axispresetpos_idcamara_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara_axispresetpos
    ADD CONSTRAINT vid_camara_axispresetpos_idcamara_fkey1 FOREIGN KEY (idcamara) REFERENCES vid_camara(idcamara);


--
-- TOC entry 2459 (class 2606 OID 44058)
-- Dependencies: 200 194 2366
-- Name: vid_camara_horario_idcamara_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara_horario
    ADD CONSTRAINT vid_camara_horario_idcamara_fkey FOREIGN KEY (idcamara) REFERENCES vid_camara(idcamara) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2455 (class 2606 OID 44034)
-- Dependencies: 194 185 2336
-- Name: vid_camara_idplantafisica_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara
    ADD CONSTRAINT vid_camara_idplantafisica_fkey FOREIGN KEY (idplantafisica) REFERENCES plantafisica(idplantafisica) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2453 (class 2606 OID 44024)
-- Dependencies: 194 209 2405
-- Name: vid_camara_idservidor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara
    ADD CONSTRAINT vid_camara_idservidor_fkey FOREIGN KEY (idservidor) REFERENCES vid_servidor(idservidor) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2454 (class 2606 OID 44029)
-- Dependencies: 194 211 2408
-- Name: vid_camara_idtipo_camara_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_camara
    ADD CONSTRAINT vid_camara_idtipo_camara_fkey FOREIGN KEY (idtipo_camara) REFERENCES vid_tipo_camara(idtipo_camara) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2460 (class 2606 OID 44065)
-- Dependencies: 202 209 2405
-- Name: vid_evento_idservidor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_evento
    ADD CONSTRAINT vid_evento_idservidor_fkey FOREIGN KEY (idservidor) REFERENCES vid_servidor(idservidor) ON UPDATE RESTRICT ON DELETE SET NULL;


--
-- TOC entry 2461 (class 2606 OID 44072)
-- Dependencies: 206 204 2388
-- Name: vid_modelo_camara_idmarca_camara_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_modelo_camara
    ADD CONSTRAINT vid_modelo_camara_idmarca_camara_fkey FOREIGN KEY (idmarca_camara) REFERENCES vid_marca_camara(idmarca_camara) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2462 (class 2606 OID 44079)
-- Dependencies: 207 209 2405
-- Name: vid_motionconf_idservidor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_motionconf
    ADD CONSTRAINT vid_motionconf_idservidor_fkey FOREIGN KEY (idservidor) REFERENCES vid_servidor(idservidor) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2463 (class 2606 OID 44086)
-- Dependencies: 208 2336 185
-- Name: vid_plantafisica_zona_idplantafisica_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_plantafisica_zona
    ADD CONSTRAINT vid_plantafisica_zona_idplantafisica_fkey FOREIGN KEY (idplantafisica) REFERENCES plantafisica(idplantafisica) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2464 (class 2606 OID 44091)
-- Dependencies: 208 2410 213
-- Name: vid_plantafisica_zona_idzona_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_plantafisica_zona
    ADD CONSTRAINT vid_plantafisica_zona_idzona_fkey FOREIGN KEY (idzona) REFERENCES vid_zona(idzona) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2465 (class 2606 OID 44098)
-- Dependencies: 187 209 2339
-- Name: vid_servidor_idsegmento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_servidor
    ADD CONSTRAINT vid_servidor_idsegmento_fkey FOREIGN KEY (idsegmento) REFERENCES segmento(idsegmento) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2466 (class 2606 OID 44103)
-- Dependencies: 2348 190 209
-- Name: vid_servidor_idtimezone_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_servidor
    ADD CONSTRAINT vid_servidor_idtimezone_fkey FOREIGN KEY (idtimezone) REFERENCES timezone(idtimezone) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2467 (class 2606 OID 44108)
-- Dependencies: 2391 206 211
-- Name: vid_tipo_camara_idmodelo_camara_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_tipo_camara
    ADD CONSTRAINT vid_tipo_camara_idmodelo_camara_fkey FOREIGN KEY (idmodelo_camara) REFERENCES vid_modelo_camara(idmodelo_camara) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2468 (class 2606 OID 44117)
-- Dependencies: 194 2366 214
-- Name: vid_zona_camara_idcamara_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_zona_camara
    ADD CONSTRAINT vid_zona_camara_idcamara_fkey FOREIGN KEY (idcamara) REFERENCES vid_camara(idcamara) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2469 (class 2606 OID 44122)
-- Dependencies: 2410 213 214
-- Name: vid_zona_camara_idzona_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_zona_camara
    ADD CONSTRAINT vid_zona_camara_idzona_fkey FOREIGN KEY (idzona) REFERENCES vid_zona(idzona) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2470 (class 2606 OID 44129)
-- Dependencies: 215 2410 213
-- Name: vid_zona_usuario_idzona_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_zona_usuario
    ADD CONSTRAINT vid_zona_usuario_idzona_fkey FOREIGN KEY (idzona) REFERENCES vid_zona(idzona) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2471 (class 2606 OID 44134)
-- Dependencies: 191 215 2350
-- Name: vid_zona_usuario_usuario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vid_zona_usuario
    ADD CONSTRAINT vid_zona_usuario_usuario_fkey FOREIGN KEY (usuario) REFERENCES usuario(usuario) ON UPDATE RESTRICT ON DELETE CASCADE;



--
-- TOC entry 2521 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--



REVOKE ALL ON SCHEMA PUBLIC FROM PUBLIC;
REVOKE ALL ON SCHEMA PUBLIC FROM sip;
GRANT USAGE ON SCHEMA PUBLIC TO sip;
GRANT USAGE ON SCHEMA PUBLIC TO PUBLIC;

--GRANT CONNECT ON DATABASE sip TO sip;

--SELECT 'GRANT SELECT ,INSERT ,UPDATE ,DELETE ON '||tablename||' TO sip;' FROM pg_tables WHERE schemaname = current_schema();


-- Completed on 2012-07-27 14:42:14 VET

--
-- PostgreSQL database dump complete
--

