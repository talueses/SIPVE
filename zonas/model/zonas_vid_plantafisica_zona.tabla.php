<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class Vid_plantafisica_zona {
    
    private $idplantafisica = null;
    private $idzona = null;
    private $leftx = null;
    private $topy = null;
    private $width = null;
    private $height = null;
    private $bgcolor = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdplantafisica($idplantafisica){
        $this->idplantafisica = $idplantafisica;
    }
    public function getIdplantafisica(){
        return $this->idplantafisica;
    }
    public function setIdzona($idzona){
        $this->idzona = $idzona;
    }
    public function getIdzona(){
        return $this->idzona;
    }
    public function setLeftx($leftX){
        $this->leftX = $leftX;
    }
    public function getLeftx(){
        return $this->leftX;
    }
    public function setTopy($topY){
        $this->topY = $topY;
    }
    public function getTopy(){
        return $this->topY;
    }
    public function setWidth($width){
        $this->width = $width;
    }
    public function getWidth(){
        return $this->width;
    }
    public function setHeight($height){
        $this->height = $height;
    }
    public function getHeight(){
        return $this->height;
    }
    public function setBgcolor($bgcolor){
        $this->bgcolor = $bgcolor;
    }
    public function getBgcolor(){
        return $this->bgcolor;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idplantafisica !== null) && (trim($this->idplantafisica)!=='') ){
            $campos .= "idplantafisica,";
            $valores .= "'".$this->idplantafisica."',";
        }
        if(($this->idzona !== null) && (trim($this->idzona)!=='') ){
            $campos .= "idzona,";
            $valores .= "'".$this->idzona."',";
        }
        if(($this->leftX !== null) && (trim($this->leftX)!=='') ){
            $campos .= "\"leftX\",";
            $valores .= "'".$this->leftX."',";
        }
        if(($this->topY !== null) && (trim($this->topY)!=='') ){
            $campos .= "\"topY\",";
            $valores .= "'".$this->topY."',";
        }
        if(($this->width !== null) && (trim($this->width)!=='') ){
            $campos .= "width,";
            $valores .= "'".$this->width."',";
        }
        if(($this->height !== null) && (trim($this->height)!=='') ){
            $campos .= "height,";
            $valores .= "'".$this->height."',";
        }
        if(($this->bgcolor !== null) && (trim($this->bgcolor)!=='') ){
            $campos .= "bgcolor,";
            $valores .= "'".$this->bgcolor."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO vid_plantafisica_zona $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE vid_plantafisica_zona SET ";
        
        if(($this->leftX !== null) && (trim($this->leftX)!=='') ){
            $sql .= "\"leftX\" = '".$this->leftX."',";
        }
        if(($this->topY !== null) && (trim($this->topY)!=='') ){
            $sql .= "\"topY\" = '".$this->topY."',";
        }
        if(($this->width !== null) && (trim($this->width)!=='') ){
            $sql .= "width = '".$this->width."',";
        }
        if(($this->height !== null) && (trim($this->height)!=='') ){
            $sql .= "height = '".$this->height."',";
        }
        if(($this->bgcolor !== null) && (trim($this->bgcolor)!=='') ){
            $sql .= "bgcolor = '".$this->bgcolor."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idplantafisica = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM vid_plantafisica_zona  WHERE idplantafisica = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de VidPlantafisicaZonas
     * @return object Devuelve un registro como objeto
     */
    function  getVidPlantafisicaZonas(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idplantafisicaBuscador"]){
            $in = null;
            foreach ($_REQUEST["idplantafisicaBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idplantafisica in (".$in.")";
        }
        
        $sql = "select idplantafisica,idzona,\"leftX\",\"topY\",width,height,bgcolor from vid_plantafisica_zona ".$arg." order by leftX,topY";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de VidPlantafisicaZona
     * @param int $idplantafisica Codigo
     * @return object Devuelve registros como objeto
     */
    function getVidPlantafisicaZona($idplantafisica){
        $sql = "SELECT idplantafisica,idzona,\"leftX\",\"topY\",width,height,bgcolor FROM vid_plantafisica_zona WHERE idplantafisica = '".$idplantafisica."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>