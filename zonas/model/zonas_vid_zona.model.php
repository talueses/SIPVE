<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once '../model/zonas_vid_zona.tabla.php';
require_once '../model/zonas_vid_plantafisica_zona.tabla.php';
/**
 * Clase Zonas{}
 * @author David Concepcion 11-10-2010 CENIT
 */
class Zonas extends Vid_zona{
    
    /**
     * Consulta de Zonas
     * @return object Devuelve un registro como objeto
     */
    static function getZonas(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idzonaBuscador"]){
            $in = null;
            foreach ($_REQUEST["idzonaBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idzona in (".$in.")";
        }
        
        $ret = array();
        $sql = "SELECT /*start*/ * /*end*/ FROM vid_zona ".$arg." order by zona";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de una camara
     * @param int $idzona Codigo de la Zona
     * @return object Devuelve registros como objeto
     */
    static function get_Zona($idzona){
        $sql = "SELECT * FROM vid_zona WHERE idzona='".$idzona."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }

    /**
     * Consulta de Camaras por segmentos asociadas a una zona
     * Solo se listan las Camaras pertenecientes al segmento del usuario logueado
     * @param int $idzona Codigo de la Zona
     * @return object Devuelve registros como objeto
     */
    function getCamarasZona($idzona){
        $sql="select vseg.segmento,
                     vseg.descripcion,
                     vc.camara,
                     vc.numero
              from vid_zona_camara vzc,
                   vid_camara vc,
                   vid_servidor vsrv,
                   segmento vseg
              where vzc.idzona = '".$idzona."'
                and vzc.idcamara = vc.idcamara
                and vc.idservidor = vsrv.idservidor
                and vsrv.idsegmento = vseg.idsegmento
                and vseg.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."')
                    order by vseg.segmento,vc.numero, vc.camara ";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de Usuarios asociados a una zona
     * Solo se listan los usuarios pertenecientes al segmento del usuario logueado
     * @param int $idzona Codigo de la Zona
     * @return object Devuelve registros como objeto
     */
    function getUsuariosZona($idzona){
        $sql="select vzu.usuario 
              from vid_zona_usuario vzu, segmento_usuario su
              where vzu.usuario = su.usuario
                and su.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."')
                and vzu.idzona = '".$idzona."' group by vzu.usuario order by vzu.usuario";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }    
    
    /**
     * Consulta de datos dependientes de una zona
     * @param int $idzona Codigo de la zona
     * @return object Devuelve registros como objeto
     */
    function getZonaDependencies($idzona){
        $sql = "SELECT (SELECT 1 FROM vid_zona_camara WHERE idzona = '".$idzona."' group by idzona)as zonaCamara,
                       (SELECT 1 FROM vid_zona_usuario WHERE idzona = '".$idzona."' group by idzona)as zonaUsuario,
                       (SELECT 1 FROM vid_plantafisica_zona WHERE idzona = '".$idzona."' group by idzona)as zonaPlantaFisica";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }

    /**
     * Elimina los registros dependientes de la zona
     * @return boolean Devuelve falso si hay error en ejecucion SQL
     */
    function eliminarDependencies(){        
        $sql = "delete zc,zu,vpf
                FROM vid_zona vz
                left join vid_zona_camara zc on (vz.idzona = zc.idzona)
                left join vid_zona_usuario zu on (vz.idzona = zu.idzona)
                left join vid_plantafisica_zona vpf on (vz.idzona = vpf.idzona)
                WHERE vz.idzona = '".$this->getIdzona()."'";        
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return true;
    }
}

/**
 * Clase Planta Fisica de Zonas
 */
class ZonasPlantaFisica extends Vid_plantafisica_zona{

    /**
     * Consulta de una planta fisica y una zona con su asociacion no mandatoria
     * @param int $idplantafisica Codigo de planta fisica
     * @param int $idzona Codigo de zona
     * @return object Devuelve registros como objeto
     */
    static function getZonaPlantaFisica($idplantafisica,$idzona){
        $sql = "select pf.idplantafisica,
                       pf.plantafisica,
                       pf.file,
                       z.idzona,
                       z.zona,
                       vpf.idplantafisica as idplantafisicaAsoc,
                       vpf.\"leftX\",
                       vpf.\"topY\",
                       vpf.width,
                       vpf.height,
                       vpf.bgcolor
                from plantafisica pf,
                vid_zona z
                    left join vid_plantafisica_zona vpf on (vpf.idplantafisica = '".$idplantafisica."' and vpf.idzona = z.idzona)
                where pf.idplantafisica = '".$idplantafisica."'
                  and z.idzona = '".$idzona."'";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    /**
     * Consulta de plantas fisicas asociadas a una zona
     * @param int $idzona Codigo de zona
     * @return object Devuelve registros como objeto
     */
    static function getZonaPlantaFisicas($idzona){
        $sql = "select pf.idplantafisica,
                       pf.plantafisica,
                       pf.file,
                       vpf.idplantafisica as idplantafisicaAsoc,
                       vpf.\"leftX\",
                       vpf.\"topY\",
                       vpf.width,
                       vpf.height,
                       vpf.bgcolor
                from vid_plantafisica_zona vpf,
                     plantafisica pf
                where vpf.idzona = '".$idzona."'
                  and vpf.idplantafisica = pf.idplantafisica";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
}
?>