<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDZN"); //Categoria del modulo
require_once "../model/zonas_vid_zona.model.php"; // Class MODEL Zonas()
require_once "zonas.controller.php";// Class CONTROL Controller()
/**
 * Description
 * @author David Concepcion
 */
class ControlZonas extends Controller{
    /**
     * @var string mensaje de exito o error
     */
    var $mensaje = null;
    /**
     * @var string accion agregar, modificar o eliminar dato
     */
    var $accion  = null;

    /**
     * Establece la acción
     * @param string $accion Acción
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }

    /**
     * @return string Devuelve la accion establecida
     */
    public function getAccion(){
         return $this->accion;
    }

    /**
     * Consulta de Zonas
     * @return object Instanciacion de metodo getZonas() en clase Zonas{} (../model/monitorasoc.model.php)
     */
    function getZonas(){
        return Zonas::getZonas();
    }
    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }

    /**
     * Establece la Entidad
     * @param string $entidad Entidad
     */
    public function setEntidad($entidad){
         $this->entidad = $entidad;
    }

    /**
     * @return string Devuelve la Entidad establecida
     */
    public function getEntidad(){
         return $this->entidad;
    }
    /**
     * Agregar o modificar una zona
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente la zona
     */
    function setZona(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;

        $zona = new Zonas();

        //--------------------- DATOS --------------------------//
        $zona->setIdzona($_REQUEST["idzona"]);
        $zona->setZona($_REQUEST["zona"]);
        $zona->setDescripcion($_REQUEST["descripcion"]);

        if ($this->getAccion()=="agregar"){
            $exito = $zona->insertarRegistro();
        }
        if ($this->getAccion()=="modificar"){
            $exito = $zona->modificarRegistro($zona->getIdzona());
        }

        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            if ($this->getAccion()=="agregar"){
                $this->mensaje = "La Zona no pudo ser creada: ".$zona->getMensaje();
            }
            if ($this->getAccion()=="modificar"){
                $this->mensaje = "La Zona no pudo ser modificada: ".$zona->getMensaje();
            }
            return false;
        }
        // ------------------------------- MENSAJE EXITO --------------------------------//
        if ($this->getAccion()=="agregar"){
            $this->mensaje = "La Zona fu&eacute; creada exitosamente...";
        }
        if ($this->getAccion()=="modificar"){
            $this->mensaje = "La Zona fu&eacute; modificada exitosamente...";
        }

        return true;
    }    
    
    /**
     * Eliminar zona
     * @return boolean Devuelve verdadero si se elimina correctamente la zona
     */
    function eliminarZona(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        
        $zona = new Zonas();
        $zona->setIdzona($_REQUEST["idzona"]);

        // ------------------- ELIMINAR DEPENDENCIAS DE ZONA --------------------------- //
        if ($this->getAccion()=="eliminarDependencies"){
            $exito = $zona->eliminarDependencies();
            if(!$exito){
                $this->mensaje = "La Zona no pudo ser eliminada: ".$zona->getMensaje();
                return false;
            }
            
        }

        // ------------------- BUSCAR DEPENDENCIAS DE ZONA ----------------------------- //
        $row = $zona->getZonaDependencies($zona->getIdzona());
        if ($row->zonaCamara || $row->zonaUsuario || $row->zonaPlantaFisica){
            $this->mensaje = "La Zona no pudo ser eliminada";
            $this->setAccion("eliminarDependencies");
            return false;
        }       
        
        // --------------------------- ELIMINAR ZONA ----------------------------------- //        
        $exito = $zona->eliminarRegistro($zona->getIdzona());
        if(!$exito){
            $this->mensaje = "La Zona no pudo ser eliminada: ".$zona->getMensaje();
            return false;
        }
        $this->mensaje = "La Zona fu&eacute; eliminada exitosamente...";
        return true;
    }

    /**
     * Agregar o modificar planta fisica de una zona
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setZonasCoord(){
        $zona = new ZonasPlantaFisica();

        //--------------------- DATOS --------------------------//
        $zona->setIdplantafisica($_REQUEST["idplantafisica"]);
        $zona->setIdzona($_REQUEST["idzona"]);
        $zona->setLeftx($_REQUEST["leftX"]);
        $zona->setTopy($_REQUEST["topY"]);
        $zona->setWidth($_REQUEST["width"]);
        $zona->setHeight($_REQUEST["height"]);
        $zona->setBgcolor($_REQUEST["bgcolor"]);


        if ($this->getAccion()=="agregar"){
            $exito = $zona->insertarRegistro();
        }
        if ($this->getAccion()=="modificar"){
            $exito = $zona->modificarRegistro(array("idzona"=>$zona->getIdzona(),"idplantafisica"=>$zona->getIdplantafisica()));
        }

        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            if ($this->getAccion()=="agregar"){
                $this->mensaje = "La coordenada de la Zona no pudo ser creada: ".$zona->getMensaje();
            }
            if ($this->getAccion()=="modificar"){
                $this->mensaje = "La coordenada de la Zona no pudo ser modificada: ".$zona->getMensaje();
            }
            return false;
        }
        // ------------------------------- MENSAJE EXITO --------------------------------//
        if ($this->getAccion()=="agregar"){
            $this->mensaje = "La coordenada de la Zona fu&eacute; creada exitosamente...";
        }
        if ($this->getAccion()=="modificar"){
            $this->mensaje = "La coordenada de la Zona fu&eacute; modificada exitosamente...";
        }

        return true;
    }

    /**
     * Eliminar planta fisica de una zona
     * @return boolean Devuelve verdadero si se elimina correctamente
     */
    function eliminarZonaCoord(){
        $zona = new ZonasPlantaFisica();

        //--------------------- DATOS --------------------------//
        $zona->setIdplantafisica($_REQUEST["idplantafisica"]);
        $zona->setIdzona($_REQUEST["idzona"]);

        $exito = $zona->eliminarRegistro(array("idzona"=>$zona->getIdzona(),"idplantafisica"=>$zona->getIdplantafisica()));
        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            $this->mensaje = "La coordenada de la Zona no pudo ser eliminada: ".$zona->getMensaje();
            return false;
        }
        
        // ------------------------------- MENSAJE EXITO --------------------------------//
        $this->mensaje = "La coordenada de la Zona fu&eacute; eliminada exitosamente...";
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Zona");

        $this->setCampos("zona","Zona");
        $this->setCampos("descripcion","Descripci&oacute;n");

    }
    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos de la zona estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"zona","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"descripcion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

    /**
     * Establece un color RGB al azar sobre la paleta de colores estandar HTML
     * @return string Codigo Hexadecimal del Color RGB
     */
    static function random_color(){
        mt_srand((double)microtime()*1000000);
        $code = '';
        while(strlen($code)<6){
            $code .= sprintf("%02X", mt_rand(0, 255));
        }
        return $code;
    }
}
?>