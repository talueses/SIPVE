<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php 
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/zonas_vid_zona.control.php"; // Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $Zonas = new Zonas();
    $zona = $Zonas->get_Zona($_REQUEST["idzona"]);
    $camarasZona = $Zonas->getCamarasZona($zona->idzona);
    $usuariosZona = $Zonas->getUsuariosZona($zona->idzona);

    if (!$zona || !$camarasZona || !$usuariosZona){
        echo $Zonas->getMensaje();
    }
}

if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title><?php echo ucfirst($_REQUEST["accion"]);?> Camara de Video</title>        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.textarea.maxlength.js"></script>
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;                
                
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #datos{
                width: 80%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            /* Para que no se vean los Botones cuando se ejecute la accion*/
            .ui-button, #descripcion{
                position: inherit;
            }
            ul#savedItems { color: #770; font-size: .9em; list-style-position: outside; margin-left: 0; padding-left: 1.4em; }
            ul#savedItems li:hover { cursor: pointer; opacity:0.4 }
            ul#savedItems li.error { color: #ff0000; }
        </style>        
        <script type="text/javascript" language="javascript">
            $(function() {                   
                $('input:text,input:password').css({
                    background: '#fff' ,
                    border: '1px solid #d5d5d5',
                    '-moz-border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    'border-radius': '4px'
                });
                $( "input:button, input:submit" ).button();
            });
            function setImage(idplantafisica,accion){
                if (!idplantafisica){ return false;}
                             
                var link = 'mapaSetZona.php?idzona='+$('#idzona').val()+'&idplantafisica='+idplantafisica+'&accion='+accion;
                
                $("#frameMap").attr("src",link);
                $("#loadMap").fadeIn("slow");                
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="loadMap" align="center" style="position: absolute;background-color:#000000;opacity:.9;width: 100%;height: 100%;vertical-align: middle;color: #FFFFFF;font-size: 20px;display: none">
            <iframe id="frameMap" src="" ></iframe>
        </div>
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px"><?php echo ucfirst($_REQUEST["accion"] );?> Zona</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="yes"></iframe>

                <form method="POST" name="f1" action="zonasOp.php" target="ifrm1">
                    <div id="datos" >
                        <table>
                            <tr title="Zona">
                                <td align="right">Zona:</td>
                                <td><input type="text" name="zona" id="zona" value="<?php echo $zona->zona;?>" <?php echo $disabled;?> title="Zona"></td>
                            </tr>
                            <tr title="Breve descripci&oacute;n de la zona">
                                <td align="right" valign="top" >Descripci&oacute;n:</td>
                                <td>
                                    <div class="divTextarea" >
                                        <textarea name="descripcion" id="descripcion" <?php echo $disabled;?> rows="6" cols="28" maxlength="100" <?php echo $disabled;?>><?php echo $zona->descripcion;?></textarea>
                                        <div class="charLeftDiv"  align="right"><input type="text" class="charLeft" id="charLeft_descripcion" size="4" readonly > Caracteres Restantes</div>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            if ($_REQUEST["accion"]!="agregar"){
                            ?>
                            <tr title="Planta F&iacute;sica">
                                <td align="right" valign="top">Planta F&iacute;sica:</td>
                                <td class="list">
                                    <?php
                                    if ($_REQUEST["accion"]!="visualizar"){
                                        ?>
                                        <select name="idplantafisica" id="idplantafisica" onchange="setImage(this.value,'agregar')" <?php echo $disabled;?>>
                                            <option value="">Seleccione</option>
                                            <?php echo Controller::make_combo("plantafisica","where idplantafisica not in (select idplantafisica from vid_plantafisica_zona where idzona = '".$zona->idzona."')order by plantafisica", "idplantafisica", "plantafisica", "");?>
                                        </select>
                                        <?
                                    }
                                    ?>
                                    <ul id="savedItems">
                                        <?php
                                        $getZonaPlantaFisicas = ZonasPlantaFisica::getZonaPlantaFisicas($_REQUEST["idzona"]);
                                        if (count($getZonaPlantaFisicas)>0){
                                            foreach ($getZonaPlantaFisicas as $row){
                                                echo "<li onclick=\"setImage('".$row->idplantafisica."','".$_REQUEST["accion"]."')\">".$row->plantafisica."</li>";
                                            }
                                        }
                                        if (count($getZonaPlantaFisicas)<=0){
                                            echo "<li class=\"error\">No hay registros</li>";
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <?php
                            // ---------- LISTADO DE CAMARAS ASOCIADAS -------------------------//
                            if (count($camarasZona)){
                                ?>
                                <tr title="C&aacute;maras asociadas">
                                    <td align="right" valign="top">C&aacute;maras asociadas:</td>
                                    <td class="list">
                                        <pre><?php
                                            foreach ($camarasZona as $row){
                                                if ($segmento!=$row->segmento){
                                                    echo "<b>".$row->segmento." - ".$row->descripcion."</b>\n";
                                                }
                                                echo "\t--> ".$row->camara." - ".$row->numero."\n";
                                                $segmento=$row->segmento;
                                            }
                                        ?></pre>
                                    </td>
                                </tr>
                                <?php
                            }
                            // ---------- LISTADO DE USUARIOS ASOCIADOS -------------------------//
                            if (count($usuariosZona)){
                                ?>
                                <tr title="Usuarios asociados">
                                    <td align="right" valign="top">Usuarios asociados:</td>
                                    <td class="list">
                                        <pre><?php
                                            foreach ($usuariosZona as $row){
                                                echo "\t--> ".$row->usuario."\n";
                                            }
                                        ?></pre>
                                    </td>
                                </tr>
                                <?php
                            }
                            // -----------------------------------------------------------------//
                            ?>
                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idzona" id="idzona" value="<?php echo $zona->idzona ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Zona\"></input>";
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>