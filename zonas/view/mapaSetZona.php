<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Establece la zona en una planta fisica registrada
 * @author David Concepcion 06-12-2010 CENIT-DIDI
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/zonas_vid_zona.control.php"; // Class CONTROLLER

$data = ZonasPlantaFisica::getZonaPlantaFisica($_REQUEST["idplantafisica"],$_REQUEST["idzona"]);
//echo "<div align='left'><pre>".print_r($data,true)."</pre></div>";

// ------------ IMAGEN DE PLANO --------------//
$image = "../../plantafisica/images/".$data[0]->file;
$imagesize = getimagesize($image);

foreach ($data as $row){
    if ($row->idplantafisicaAsoc){ // si las coordenadas de la zona estan registradas
        $zonasCoord[] = $row;
    }
}
// Color RGB al azar sobre la paleta de colores estandar HTML
$random_color = ControlZonas::random_color();
?>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>        
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css">
            .title{
                width: <?php echo $imagesize[0]?>px;
                color: #770;
                font-style: oblique;
                background-color: #f0ebe2;                
                border: 1px solid #ccc0a9;
            }
            #close{
                font-size: 10px;
                font: bolder;
                color: #000;
                border: double #000;
                width: 8px;
                cursor: pointer;
                float: right;
                text-align: center;
                font-style: normal;                
            }
            
            #close:hover{
                opacity:0.3;
            }
            /*---------PLANO PLANTA FISICA--------*/
            #map {
                background: #fff url(<?php echo $image?>) no-repeat center;
                width: <?php echo $imagesize[0]?>px;
                height: <?php echo $imagesize[1]?>px;
                border: ridge 3px #000000;
                position: relative;
                clear: both;                
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            /*--------------ZONAS------------------*/
            #map-part {
                left: <?php echo floor(($imagesize[0]-50)/2)?>px;
                top: <?php echo floor(($imagesize[1]-50)/2)?>px;
                width: 150px;
                height: 100px;
                background-color: #<?php echo $random_color;?>;
                opacity:0.5;
                border: dashed 1px #000000;
                position: absolute;
            }
            #map-part:hover { opacity:0.8; }

            #map-part span{ display: none; }

            #map-part:hover span{
                display: block;
                vertical-align: top;
                color: #000;
                background-color: #F4F4F4;
                position: absolute;
                border: solid 1px  #BCBCBC;
                bottom: 100%;
                width: 75%;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 80%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
        </style>
        <script type="text/javascript">
            // -------------- DIMENCION DEL iFRAME frameMap------------------//
            var widthToSet = <?php echo $imagesize[0]+20?>;
            var heightToSet = <?php echo $imagesize[1]+200?>;
            $('#frameMap' ,parent.document).width(widthToSet);
            $('#frameMap' ,parent.document).height(heightToSet);
            // ------------- DIMENCION DEL CONTENEDOR loadMap DEL iFRAME frameMap----//
            if ( parseInt($('#loadMap',parent.document).width()) < parseInt(widthToSet) ){
                $('#loadMap',parent.document).width(widthToSet+50);
            }
            if ( parseInt($('#loadMap',parent.document).height()) < parseInt(heightToSet) ){
                $('#loadMap',parent.document).height(heightToSet+50);
            }

            $(function(){                   
                $('input:text,input:password').css({
                    background: '#fff' ,
                    border: '1px solid #d5d5d5',
                    '-moz-border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    'border-radius': '4px'
                });
                $( "input:button, input:submit, button" ).button();
            });
            $(function() {
                if ($('#accion').val()!="visualizar"){
                    $("#map-part").draggable({
                        start: function() {
                           $(this).css('cursor','move');
                        },
                        drag: function(event,ui) {
                           $('#leftX').val(ui.position.left);
                           $('#topY').val(ui.position.top);

                        },
                        stop: function() {
                           $(this).css('cursor','default');
                        },
                        containment:'#map'
                    });
                    $("#map-part" ).resizable({
                       resize: function(event, ui) {
                          $('#width').val(ui.size.width);
                          $('#height').val(ui.size.height);
                       },
                       maxWidth: <?php echo $imagesize[0]?>,
                       maxHeight: <?php echo $imagesize[1]?>,
                       minWidth: 50,
                       minHeight: 50,
                       containment:'#map'
                    });
                }
                <?php
                if (count($zonasCoord) > 0){
                    // ----------------- SET CSS ZONAS ------------------//
                    foreach ($zonasCoord as $row){

                        echo "  $('.zona').css({\n";
                        echo "      'left':'".$row->leftX."px',\n";
                        echo "      'top' :'".$row->topY."px',\n";
                        echo "      'width':'".$row->width."px',\n";
                        echo "      'height':'".$row->height."px',\n";
                        echo "      'background-color':'".$row->bgcolor."'\n";
                        echo "  });\n";

                        echo "  $('#leftX').val('".$row->leftX."');\n";
                        echo "  $('#topY').val('".$row->topY."');\n";
                        echo "  $('#width').val('".$row->width."');\n";
                        echo "  $('#height').val('".$row->height."');\n";
                        echo "  $('#bgcolor').val('".$row->bgcolor."');\n";
                    }
                }
                ?>
            });

            function closeDoc(){
                parent.document.location.href = 'zonasAcc.php?idzona=<?php echo $_REQUEST["idzona"]?>&accion=<?php echo ($_REQUEST["accion"]!='visualizar')?'modificar':$_REQUEST["accion"];?>';
            }

        
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <div id="divmensaje" style="width:99%;"></div>
            <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
            <center>
                <div id="titulo">
                    <?php echo $data[0]->plantafisica?>
                    <div id="close" onclick="closeDoc();" title="Cerrar" >X</div>
                </div>

            <div id="map">
                <div class="zona" id="map-part"><span><?php echo $data[0]->zona?></span></div>
            </div>

                <?php
                if ($_REQUEST["accion"]!="visualizar"){
                    echo "<button onclick=\"$('#form1').submit();\">Guardar</button>";
                }
                ?>            
                <button onclick="closeDoc();">Cancelar</button>
                <?php
                if ($_REQUEST["accion"]=="modificar"){
                    echo "<button onclick=\"$('#accion').val('eliminar');$('#form1').submit();\">Eliminar</button>";
                }
                ?>

            </center>        
            <form name="form1" id="form1" method="post" action="mapaSetZonaOp.php" target="ifrm1">
                <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                <input type="hidden" name="idplantafisica" id="idplantafisica" value="<?php echo $_REQUEST["idplantafisica"];?>">
                <input type="hidden" name="idzona" id="idzona" value="<?php echo $_REQUEST["idzona"];?>">
                <input type="hidden" name="leftX" id="leftX" value="<?php echo floor(($imagesize[0]-50)/2)?>">
                <input type="hidden" name="topY" id="topY" value="<?php echo floor(($imagesize[1]-50)/2)?>">
                <input type="hidden" name="width" id="width" value="150" >
                <input type="hidden" name="height" id="height" value="100" >
                <input type="hidden" name="bgcolor" id="bgcolor" value="#<?php echo $random_color;?>">
            </form>
        </div>
    </body>
</html>