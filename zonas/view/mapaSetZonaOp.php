<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Ejecucion de operaciones agregar, modificar y eliminar datos de coordenadas de zonas
 * @author David Concepcion 09-12-2010 CENIT-DIDI
 */

session_start(); // start up your PHP session!
//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/zonas_vid_zona.control.php"; // Class CONTROLLER
$zona = new ControlZonas();
$zona->accion = $_REQUEST["accion"];
if ($_REQUEST["accion"]=="agregar" || $_REQUEST["accion"]=="modificar"){
    $exito = $zona->setZonasCoord();
}
if ($_REQUEST["accion"]=="eliminar"){
    $exito = $zona->eliminarZonaCoord();
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" language="javascript" charset="UTF-8">
            $(function() {
                <?php            

                echo "var mensaje = \"".$zona->mensaje."\";\n";
                echo "var str;\n";
                if($exito){
                    echo "str  = '<div class=\"ui-widget\">';\n";
                    echo "str += '    <div class=\"ui-state-highlight ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';\n";
                    echo "str += '        <p>';";
                    echo "str += '            <span class=\"ui-icon ui-icon-info\" style=\"float: left; margin-right: .3em;\"></span>';\n";
                    echo "str += '                '+mensaje;\n";
                    echo "str += '        </p>';\n";
                    echo "str += '    </div>';\n";
                    echo "str += '</div>';\n";
                }
                if(!$exito){
                    echo "str  = '<div class=\"ui-widget\">';\n";
                    echo "str += '    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';\n";
                    echo "str += '        <p>';\n";
                    echo "str += '            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>';\n";
                    echo "str += '                '+mensaje;\n";
                    echo "str += '        </p>';\n";
                    echo "str += '    </div>';\n";
                    echo "str += '</div>';\n";

                }
                echo "$('#divmensaje',parent.document).html(str);\n"; 

                if ($_REQUEST["accion"]=="eliminar"){
                    ?>
                    setTimeout('parent.closeDoc()',5000);
                    <?php
                }
                ?>
            });
        </script>
    </head>
</html>