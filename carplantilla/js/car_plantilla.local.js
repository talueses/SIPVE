/**
 * 
 */
var widthV;
var heightV;
var widthH;
var heightH;
var val;
var objId     = [];
var styleObjs = [];
//------------------------------------------LOAD-------------------------------------------------------------//
$(function() {
    
    widthV  = parseInt($('#defaultWidth').val());
    heightV = parseInt($('#defaultHeight').val());
    widthH  = heightV;
    heightH = widthV;
    
    $( "input:button, input:submit,button" ).button();
    $( "#radio_tipo_carnet" ).buttonset();
    
    //-----------------------------------MAX SIZE IMG------------------------------------------//
    $('#maxWidthTemplate').val(widthV);
    $('#maxHeightTemplate').val(heightV);
    //-----------------------------------MENSAJE-----------------------------------------------//
    $('#divmensaje').live({
        click: function() {
            $(this).fadeOut(500);
        }
    });
    //-----------------------------------NOMBRES-----------------------------------------------//
    $(".nombres").live({
        keyup: function(){
            objId = $(this).attr('id').split('_');
            $('#'+objId[0]+'_view').html($(this).val().replace('\n', '<br>'));

            if ($('#'+objId[0]+'_nombre').val()!="" && !$('#'+objId[0]+'_chk').is(':checked')){
                //$('#'+objId[0]+'_chk').trigger('click');
            }
            if ($('#'+objId[0]+'_nombre').val()=="" ){
                if ($('#'+objId[0]+'_chk').is(':checked')){
                    //$('#'+objId[0]+'_chk').trigger('click');
                }
                if (objId[0]=='institucion'){
                    $('#'+objId[0]+'_view').html('Instituci&oacute;n');
                }
            }
        }
    }).each(function(){
        objId = $(this).attr('id').split('_');
        // --- Set corrdenadas Obj seleccionado por Defecto --- //
        if ($(this).val()){
            $('#'+objId[0]+'_view').html($(this).val().replace('\n', '<br>'));
        }
    });
    //-----------------------------------NOMBRES COMBOS-----------------------------------------------//
    $(".nombresCombos").live({
        change: function(){
            objId = $(this).attr('id').split('_');

            if (objId[0]!="cargo" || (objId[0]=="cargo" && $('#cargo_nombre_chk').is(':checked'))){
                $('#'+objId[0]+'_view').html($('#'+objId[0]+'_id option:selected').text());
            }

            // ---------- Set Posision Obj segun parametros de BD ---------//
            if ($('#accion').val()!="agregar"){
                $('#'+objId[0]).css({
                    top: function(){
                        val = "";
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('top');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('top');
                        }
                        $('#'+$(this).attr('id')+'_top').val(val);
                        return val;
                    },
                    left: function(){
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('left');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('left');
                        }
                        $('#'+$(this).attr('id')+'_left').val(val);
                        return val;
                    },
                    width: function(){                        
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('width');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('width');
                        }                        
                        $('#'+$(this).attr('id')+'_w').val(val);
                        return val;
                    },
                    height: function(){
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('height');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('height');
                        }
                        $('#'+$(this).attr('id')+'_h').val(val);
                        return val;
                    },
                    color: function(){
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('color');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('color');
                        }
                        $('#'+objId[0]+'_color_Control').css('backgroundColor',val);
                        $('#'+$(this).attr('id')+'_color').val(val);
                        return val;
                    },
                    'backgroundColor': function(){
                        if (objId[0] != 'cargo' ){
                            if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                                val = $('#'+objId[0]+'_id option:selected').attr('bgcolor');
                            }else{
                                val = $('#'+objId[0]+'_id option[value=""]').attr('bgcolor');
                            }
                            if ($('#'+objId[0]+'_id option:selected').attr('bgcolor_chk')=='1'){
                                $('#'+objId[0]+'_bgcolor_chk').attr('checked',true);
                                $('#'+objId[0]+'_bgcolor_Control').css('backgroundColor',val);
                                $('#'+$(this).attr('id')+'_bgcolor').val(val);
                            }else{
                                $('#'+objId[0]+'_bgcolor_chk').attr('checked',false);
                                val = '';
                            }
                        }
                        return val;
                    }                    
                });
                $('#'+objId[0]+'_view').css({
                    'font-family': function(){
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('fuenteletra');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('fuenteletra');
                        }
                        $('#'+objId[0]+'_fuenteletra option[value="'+val+'"]').attr('selected',true);
                        return val;
                    },
                    'font-size': function(){
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('fuentetamano');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('fuentetamano');
                        }
                        $('#'+objId[0]+'_fuentetamano option[value="'+val+'"]').attr('selected',true);
                        return val+'pt';
                    },
                    'text-align': function(){
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('fuentealign');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('fuentealign');
                        }
                        $('#'+objId[0]+'_fuentealign option[value="'+val+'"]').attr('selected',true);
                        return val;
                    },
                    'vertical-align': function(){
                        if ($('#'+objId[0]+'_id option:selected').attr('idplantilla')==$('#idplantilla').val()){
                            val = $('#'+objId[0]+'_id option:selected').attr('fuentevalign');
                        }else{
                            val = $('#'+objId[0]+'_id option[value=""]').attr('fuentevalign');
                        }
                        $('#'+objId[0]+'_fuentevalign option[value="'+val+'"]').attr('selected',true);                        
                        return val;
                    }
                });

                if (objId[0]=="cargo" ){ 
                    if ($('#'+objId[0]+'_id option:selected').attr('chk')=="1"){
                        //alert($('#'+objId[0]+'_id option:selected').text());
                        $('#'+objId[0]+'_view').html($('#'+objId[0]+'_id option:selected').text());
                        $('#'+objId[0]+'_nombre_chk').attr('checked',true);
                    }else{
                        $('#'+objId[0]+'_view').html('');
                        $('#'+objId[0]+'_nombre_chk').attr('checked',false);
                    }

                }
            }
            if (objId[0]=="cargo"){
                $('#'+objId[0]).css({
                    'backgroundColor': $('#'+objId[0]+'_id option:selected').attr('bgcolor')
                });
            }

            if ($('#'+objId[0]+'_id').val()!="" ){
                $('#'+objId[0]).fadeIn(500);
            }
            if ($('#'+objId[0]+'_id').val()=="" ){                
                if (objId[0]=='departamento'){
                    $('#'+objId[0]+'_view').html('Departamento');
                }
                $('#'+objId[0]).fadeOut(0);
            }
        }
    }).each(function(){
        objId = $(this).attr('id').split('_');
        $('#'+objId[0]).fadeOut(0);
        // --- Set corrdenadas Obj seleccionado por Defecto --- //
        if ($(this).val()){
            $('#'+objId[0]+'_view').html($('#'+objId[0]+'_id option:selected').text());
        }
        //--- Set coordenadas "Seleccione" a valores por defecto
        $('#'+objId[0]+'_id option[value=""]')
            .attr({
                'top': $('#'+objId[0]+'_top').val(),
                'left': $('#'+objId[0]+'_left').val(),
                'width': $('#'+objId[0]+'_w').val(),
                'height': $('#'+objId[0]+'_h').val(),
                'color': $('#'+objId[0]+'_color').val(),
                'bgcolor': $('#'+objId[0]+'_bgcolor').val(),
                'fuentetamano': $('#'+objId[0]+'_fuentetamano').val(),
                'fuenteletra': $('#'+objId[0]+'_fuenteletra').val(),
                'fuentealign': $('#'+objId[0]+'_fuentealign').val(),
                'fuentevalign': $('#'+objId[0]+'_fuentevalign').val(),
                'bgcolor_chk': function(){
                    if ($('#'+objId[0]+'_bgcolor_chk').is(':checked')){
                        return $('#'+objId[0]+'_bgcolor_chk').val();
                    }
                }
            });

    }).css('width','80%');
    //-----------------------------------COMBOS DE ESTILOS-OBJS-----------------------------------------//    
    $(".styleObjsCombos")
        .live({
            change: function(){
                setCssToObjs(this);
                
            }
        });        
    //------------------------------------COLOR------------------------------------------------//    
    $(".colorControl")
        .ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                    $(el).ColorPickerHide().css('backgroundColor', '#' + hex);

                    
                    $('#'+objId[0]+'_'+objId[1]).val(hex);

                    // desabilitar el color de fondo si el checkbox no esta marcado
                    if (objId[1]=='bgcolor' && !$('#'+objId[0]+'_'+objId[1]+'_chk').is(':checked')){
                        hex = "";
                    }
                    
                    // Reverso
                    if (objId[0]=="institucionback" && $('#tipo_carnet_back').is(':checked')){                        
                        objId[0] = objId[0].replace('back', '');
                    }

                    $('#'+objId[0]).css(objId[1]=='bgcolor'?'backgroundColor':objId[1],'#' + hex)

            },
            onBeforeShow: function () {
                    objId = $(this).attr('id').split('_');                    
                    $(this).ColorPickerSetColor($('#'+objId[0]+'_'+objId[1]).val());
                    

            }
        })
        .css({
            width:'20px',
            'border': '#000000 1px dashed'
        });
        
    //------------------------------------ORIENTACION--------------------------------------------//
    $('#orientacion').live({
        change:function(){
            if ($(this).val()=='V'){
                $('#divCarnet, #carnetImg, #carnetImgBack').width(widthV).height(heightV);
                $('#maxWidthTemplate').val(widthV);
                $('#maxHeightTemplate').val(heightV);
            }
            if ($(this).val()=='H'){
                $('#divCarnet, #carnetImg, #carnetImgBack').width(widthH).height(heightH);
                $('#maxWidthTemplate').val(widthH);
                $('#maxHeightTemplate').val(heightH);
            }
            $('div#foto, div#logo,div#institucion,div#nombreapellido,div#cedula,div#departamento,div#cargo,div#barcode').resizable({
                maxWidth:setWidth(),
                maxHeight:setHeight()
            });
        }
    });
    if ($('#orientacion').val()=='V'){
        $('#divCarnet, #carnetImg, #carnetImgBack').width(widthV).height(heightV);
    }
    if ($('#orientacion').val()=='H'){
        $('#divCarnet, #carnetImg, #carnetImgBack').width(widthH).height(heightH);
    }
    //---------------------------------------DIV CARNET------------------------------------------//
    $('#divCarnet')
        .width(setWidth())
        .height(setHeight())
        .css({
            'background':'#D3D3D3',
            'background-size':'100%'
        });
    //-----------------------------------------OBJS CARNET DRAG RESIZE---------------------------//
    $('div#foto, div#logo,div#logoback, div#institucion,div#nombreapellido,div#cedula,div#departamento,div#cargo,div#barcode,div#fecha,div#visitante,div#visitantenro')
        .css({
            top: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_top').val();
            },
            left: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_left').val();
            },
            width: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_w').val();
            },
            height: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_h').val();
            },
            'background': function(){
                var url="";
                if ($(this).attr('id')=='foto'){
                    return '#fff url(../images/User_64.png) 50% 50% no-repeat';
                }
                if ($(this).attr('id')=='logo'){
                    url = $('#archivo_logo').val() ? 'imagesPlantillas/'+$('#archivo_logo').val(): 'Network_Service-64.png';
                    return 'url(../images/'+url+') 50% 50% no-repeat';
                }
                if ($(this).attr('id')=='logoback'){
                    url = $('#archivoback_logo').val() ? 'imagesPlantillas/'+$('#archivoback_logo').val(): 'Network_Service-64.png';
                    return 'url(../images/'+url+') 50% 50% no-repeat';
                }
            },
            'background-size': function(){
                if ($(this).attr('id')=='logo' || $(this).attr('id')=='logoback'){
                    return '100%';
                }
            }
        })
        .draggable({
            containment:'#divCarnet',
            start: function() {
               $(this).css('cursor','move');
            },
            drag: function(event, ui) {
                
                $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_top').val(parseInt(ui.position.top));
                $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_left').val(parseInt(ui.position.left));
                
            },
            stop: function() {
               $(this).css('cursor','default');

            }

        })
        .resizable({
            containment:'#divCarnet',
            minWidth: 50,
            minHeight: 50,
            maxWidth:setWidth(),
            maxHeight:setHeight(),
            resize: function(event, ui) {
                $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_w').val(parseInt(ui.size.width));
                $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_h').val(parseInt(ui.size.height));
                objId = $(this).attr('id').split('_');
                
            }
        });

        $('div#institucion,div#nombreapellido,div#cedula,div#departamento,div#cargo,div#barcode,div#fecha,div#visitante').resizable({
            minWidth: 100,
            minHeight: 20
        });
        $('div#visitantenro').resizable({
            minWidth: 40,
            minHeight: 20
        });
     //-----------------------------------------OBJS CARNET DRAG RESIZE DISABLE-------------------//
     if ($('#accion').val()=='visualizar'){
         $('div#foto, div#logo,div#logoback, div#institucion,div#nombreapellido,div#cedula,div#departamento,div#cargo,div#barcode,div#fecha,div#visitante,div#visitantenro')
            .draggable({
                disabled:true
            })
            .resizable({
                disabled:true
            });
     }
     //--------------------------------------CHECK CHECKBOX --------------------------------------//
     $('input[type=checkbox]')
        .live({
            click: function(){
                setChkToObjs(this,500);                
            }
        });
    //------------------------------------COMBO TIPO CARNET------------------------------------------------//
    
    setObjTipoCarnet();
    $('.tipo_carnet').live({
        click: function(){
            setObjTipoCarnet();            
                        
        }
    });
    //------------------------------------CONTROLES ESTILOS------------------------------------------------//
    $('#styleObjs').hide();
    $('#styleObjsTitle')
        .live({
            click: function(){                
                $('#styleObjs').toggle('blind');                                
                setObjTipoCarnet();
            }
        });    
    
});
//----------------------------------------------------------------------------------------------------------------//
function previewImg(obj) {
    var rFilter = /^(image\/bmp|image\/cis-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x-cmu-raster|image\/x-cmx|image\/x-icon|image\/x-portable-anymap|image\/x-portable-bitmap|image\/x-portable-graymap|image\/x-portable-pixmap|image\/x-rgb|image\/x-xbitmap|image\/x-xpixmap|image\/x-xwindowdump)$/i;
    var str;
    $('#divmensaje').html('');
    if (!rFilter.test(obj.files[0].type)){
        str = '<div class="ui-widget">';
        str += '    <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
        str += '        <p>';
        str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';
        str += '                El archivo debe ser una imagen';
        str += '        </p>';
        str += '    </div>';
        str += '</div>';
        $('#divmensaje').html(str);
        return false;
    }

    if (obj.files && obj.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            objId = $(obj).attr('id').split('_');
            if (objId[1] == "plantilla"){
                if (objId[0]=="file"){
                    $('#carnetImg')
                        .attr('src',e.target.result)
                        .width(setWidth())
                        .height(setHeight());
                }
                if (objId[0]=="fileback"){
                    $('#carnetImgBack')
                        .attr('src',e.target.result)
                        .width(setWidth())
                        .height(setHeight());
                }
                
            }
            if (objId[1] == "logo" ){
                if (objId[0]=="file"){
                    $('div#logo')
                    .css({
                        'background':'url('+e.target.result+') 50% 50% no-repeat',
                        'background-size':'100%'
                    });                    
                }
                if (objId[0]=="fileback"){
                    $('div#logoback')
                    .css({
                        'background':'url('+e.target.result+') 50% 50% no-repeat',
                        'background-size':'100%'
                    });
                }
                
            }

        };
        reader.readAsDataURL(obj.files[0]);
    }
    return true;
}
function setWidth(){
    if ($('#orientacion').val()=="V"){
        return widthV;
    }
    if ($('#orientacion').val()=="H"){
        return widthH;
    }
    return true;
}
function setHeight(){
    if ($('#orientacion').val()=="V"){
        return heightV;
    }
    if ($('#orientacion').val()=="H"){
        return heightH;
    }
    return true;
}
function setCssToObjs(obj){
    objId = $(obj).attr('id').split('_');
                    
    if (objId[1] == 'fuentetamano'){
        styleObjs[0] = 'font-size';
        styleObjs[1] = 'pt'
    }
    if (objId[1] == 'fuenteletra'){
        styleObjs[0] = 'font-family';
        styleObjs[1] = '';
    }
    if (objId[1] == 'fuentealign'){
        styleObjs[0] = 'text-align';
        styleObjs[1] = '';
    }
    if (objId[1] == 'fuentevalign'){
        //alert($('aaaaa *** #'+objId[0]+'_'+objId[1]+' option:selected').val());
        styleObjs[0] = 'vertical-align';
        styleObjs[1] = '';
    }
    //alert('#'+objId[0]+'_view ---'+styleObjs[0]+'---'+$('#'+objId[0]+'_'+objId[1]+' option:selected').val());
    //$('#'+objId[0]+'_view').css(styleObjs[0],$('#'+objId[0]+'_'+objId[1]+' option:selected').val()+styleObjs[1]);
    $('#'+chkTipoCarnetBack(obj)+'_view').css(styleObjs[0],$('#'+objId[0]+'_'+objId[1]+' option:selected').val()+styleObjs[1]);
}

function setChkToObjs(obj,speed){
    objId = $(obj).attr('id').split('_');
                
    if (objId[0]=="institucionback" && $('#tipo_carnet_back').is(':checked')){
        objId[0]="institucion";
    }
    
    if (objId[0]=="barcode"){
        $("#barcode_back").val('');
        $("#barcode_front").val('');
        if ($(obj).is(':checked') && !$('#tipo_carnet_back').is(':checked')){
            $("#barcode_front").val('1');            
        }
        if ($(obj).is(':checked') && $('#tipo_carnet_back').is(':checked')){
            $("#barcode_back").val('1');            
        }     
    }

    // Checkbox para aparicion de objeto
    if (objId[1]=="chk"){
        if ($(obj).is(':checked')){
            $('#'+objId[0]).fadeIn(speed);
        }else{
            $('#'+objId[0]).fadeOut(speed);
        }
    }
    // Checkbox de fuente del cargo
    if (objId[0]=="cargo" && objId[1]=="nombre" && objId[2]=="chk"){
        if ($(obj).is(':checked')){
            $('#'+objId[0]+'_view').html($('#'+objId[0]+'_id option:selected').text());
        }else{
            $('#'+objId[0]+'_view').html('');
        }
    }
    // Checkbox de color de fondo
    if (objId[1]=="bgcolor" && objId[2]=="chk"){                    
        if ($(obj).is(':checked')){
            $('#'+objId[0]).css('backgroundColor','#' + $('#'+objId[0]+'_'+objId[1]).val());
        }else{
            $('#'+objId[0]).css('backgroundColor','');
        }
    }
}
function setObjTipoCarnet(){
    
    
    
    //------------------------------------COLOR------------------------------------------------//
    $(".colorControl")
        .each(function(){
            objId = $(this).attr('id').split('_');
            
            $(this).css('backgroundColor',$('#'+objId[0]+'_'+objId[1]).val());
            
            // Establecer el color de fondo a blanco si el checkbox no esta marcado 
            if (objId[1]=='bgcolor' && !$('#'+objId[0]+'_'+objId[1]+'_chk').is(':checked') ){
                $('#'+objId[0]+'_'+objId[1]).val('ffffff');
            }
            // Reverso
            objId[5] = objId[0]; // fixed
            if (objId[0]=="institucionback" && $('#tipo_carnet_back').is(':checked')){                
                objId[5] = objId[0].replace('back', '');                
            }
            
            $('#'+objId[5]).css(objId[1]=='bgcolor'?'backgroundColor':objId[1],'#' + $('#'+objId[0]+'_'+objId[1]).val())
            
        });
    //-----------------------------------COMBOS DE ESTILOS-OBJS-----------------------------------//    
    $(".styleObjsCombos").each(function(){
        setCssToObjs(this);
    });
    //--------------------------------------CHECK CHECKBOX --------------------------------------//
    $('input[type=checkbox]')
       .each(
           function(){
               objId = $(this).attr('id').split('_');
               if (objId[0]!="barcode"){
                   setChkToObjs(this,0);
               }
               
           }
       ); 
    
    
    var noSelected = [];
    var selected;

    // Verifica Modo chequeado
    $('.tipo_carnet').each(function(){                
        if($(this).is(':checked')){
            selected = $(this).val();
        }else{
            noSelected.push($(this).val());                    
        }
    });
    
    // Esconde los objetos de los modos no chequeados
    $.each(noSelected,function(index, value){
        $('.'+value).fadeOut(0);
    });
    
    // Muestra los objetos de los modos no chequeados
    $('.'+selected).fadeIn(0);
    
    
    // Departamento
    if ($('#departamento_id').val()==''){
        $('div#departamento').fadeOut(0);
    }
    // Cargo
    if ($('#cargo_id').val()==''){                
        $('div#cargo').fadeOut(0);
    }
    // fecha
    if (!$('#fecha_chk').is(':checked')){                
        $('div#fecha').fadeOut(0);
    }
    // logo
    if (!$('#logo_chk').is(':checked')){                
        $('div#logo').fadeOut(0);
    }
    // logo Reverso
    if (!$('#logoback_chk').is(':checked')){                
        $('div#logoback').fadeOut(0);
    }
    
    $('div#institucion').fadeOut(0);
    // institucion
    if ($('#institucion_chk').is(':checked') && ($('#tipo_carnet_empleado').is(':checked')||$('#tipo_carnet_visitante').is(':checked'))){   
        $('div#institucion').fadeIn(0);
        
    }
    // institucionback
    if ($('#institucionback_chk').is(':checked') && $('#tipo_carnet_back').is(':checked')){
        $('div#institucion').fadeIn(0);                
    }  
    
    
    
    //barcode    
    $("#barcode_chk").attr('checked',false);
    $("#barcode").fadeOut(0);
    if ($("#barcode_front").val()=='1' && !$('#tipo_carnet_back').is(':checked')){
        $("#barcode_chk").attr('checked',true);
        $("#barcode").fadeIn(0);
    }else if ($("#barcode_back").val()=='1' && $('#tipo_carnet_back').is(':checked')){
        $("#barcode_chk").attr('checked',true);
        $("#barcode").fadeIn(0);
    }
    //setChkToObjs($("#barcode_chk"),500);
    
    // Se re-leen los parametros de dimencion y ubicacion de la institucion
    $('div#institucion')
        .css({
            top: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_top').val();
            },
            left: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_left').val();
            },
            width: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_w').val();
            },
            height: function(){
                return $('#'+$(this).attr('id')+chkTipoCarnetBack(this)+'_h').val();
            }
        })
    
}
function chkTipoCarnetBack(obj){    
    
    // para propiedades drag y resize
    if ($('#tipo_carnet_back').is(':checked') && $(obj).attr('id')=="institucion"){        
        return 'back';
    }
    
    // Para metodos de aplicacion de estilos de etiquetas
    if ($(obj).attr('class')=="styleObjsCombos"){        
        objId = $(obj).attr('id').split('_'); //        
        if (objId[0]=="institucionback" && $('#tipo_carnet_back').is(':checked')){
            //alert(objId);
            //alert('back');
            return objId[0].replace('back', '');
        }else{
            
            //alert('NoBack');
            return objId[0];
        }
    }
    
    return '';
}
