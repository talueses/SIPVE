<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CARPLT"); //Categoria del modulo
require_once "car_plantilla.control.php";// Class CONTROL ControlCarPlantilla()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCarPlantilla extends ControlCarPlantilla{

    /**
     * @var string Ruta donde se guardan las imagenes. Valor por defecto "../images/imagesPlantillas/"
     */
    private $uploaddir = "../images/imagesPlantillas/";
    /**
     * @var int Ancho maximo de la imagen de la plantilla. Valor por defecto 638px
     */
    private $maxWidthTemplate   = null ;
    /**
     * @var int Alto maximo de la imagen de la plantilla. Valor por defecto 1004px
     */
    private $maxHeightTemplate  = null;
    /**
     * @var int Ancho maximo de la imagen del logo. Valor por defecto 640px
     */
    private $maxWidthLogo   = 640;
    /**
     * @var int Alto maximo de la imagen del logo. Valor por defecto 480px
     */
    private $maxHeightLogo  = 480;
    /**
     * @var int Peso maximo en bits. Valor por defecto 2.097.152 Bits, equivalen a 2 MB.
     */
    private $maxSize    = 2097152;
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCarPlantilla(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        $this->maxWidthTemplate  = $_REQUEST["maxWidthTemplate"] * $this->getFixSize();
        $this->maxHeightTemplate = $_REQUEST["maxHeightTemplate"] * $this->getFixSize();

        // --- Subir archivos al servidor ---//
        if(!$this->uploadFile("file_plantilla")) return false;
        if(!$this->uploadFile("fileback_plantilla")) return false;
        if(!$this->uploadFile("file_logo")) return false;
        if(!$this->uploadFile("fileback_logo")) return false;

        // --- Formatear Datos ---//
        $this->setZeroTopLeft(array("foto","logo","logoback","institucion","institucionback","nombreapellido","visitante","visitantenro","cedula", "departamento", "cargo","fecha","barcode"));
        $this->unsetCampos(array("logo","logoback","institucion","institucionback","fecha","barcode"));        
        
        //------------------ Metodo Set  -----------------//
        if(!$this->setCarPlantilla()) return false;

        if ($this->getAccion()=="agregar"){
            $_REQUEST["idplantilla"] = DB_Class::$PDO->lastInsertId();
        }

        // ---------------- Set Departamento ------------//
        $obj = new ControlOpCarDepartamento();
        $obj->setAccion("modificar");
        if (!$obj->setOpCarDepartamento()){
            $this->mensaje = $obj->mensaje;
            return false;
        }

        // ---------------- Set Cargo ------------------//
        $obj = new ControlOpCarCargo();
        $obj->setAccion("modificar");
        if (!$obj->setOpCarCargo()){
            $this->mensaje = $obj->mensaje;
            return false;
        }

        return true;
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCarPlantilla(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCarPlantilla()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Plantilla");
        
        $this->setCampos("plantilla","Nombre de la Plantilla");
        $this->setCampos("orientacion","Orientacion");
        $this->setCampos("archivo_plantilla","Archivo de imagen de la Plantilla");
        $this->setCampos("archivoback_plantilla","Archivo de imagen de reverso de la Plantilla");
        $this->setCampos("logo_chk","Logo_chk");
        $this->setCampos("archivo_logo","Archivo de imagen del logo");
        $this->setCampos("foto_top","Foto_top");
        $this->setCampos("foto_left","Foto_left");
        $this->setCampos("foto_w","Foto_w");
        $this->setCampos("foto_h","Foto_h");
        $this->setCampos("barcode_chk","Barcode_chk");
        $this->setCampos("barcode_top","Barcode_top");
        $this->setCampos("barcode_left","Barcode_left");
        $this->setCampos("barcode_w","Barcode_w");
        $this->setCampos("barcode_h","Barcode_h");
        $this->setCampos("logo_top","Logo_top");
        $this->setCampos("logo_left","Logo_left");
        $this->setCampos("logo_w","Logo_w");
        $this->setCampos("logo_h","Logo_h");
        $this->setCampos("institucion_chk","Institucion_chk");
        $this->setCampos("institucion_nombre","Institucion_nombre");
        $this->setCampos("institucion_top","Institucion_top");
        $this->setCampos("institucion_left","Institucion_left");
        $this->setCampos("institucion_w","Institucion_w");
        $this->setCampos("institucion_h","Institucion_h");
        $this->setCampos("institucion_color","Institucion_color");
        $this->setCampos("institucion_fuentetamano","Institucion_fuentetamano");
        $this->setCampos("institucion_fuenteletra","Institucion_fuenteletra");
        $this->setCampos("institucion_bgcolor_chk","Institucion_bgcolor_chk");
        $this->setCampos("institucion_bgcolor","Institucion_bgcolor");
        $this->setCampos("institucion_fuentealign","Institucion_fuentealign");
        $this->setCampos("institucion_fuentevalign","Institucion_fuentevalign");
        $this->setCampos("nombreapellido_top","Nombreapellido_top");
        $this->setCampos("nombreapellido_left","Nombreapellido_left");
        $this->setCampos("nombreapellido_w","Nombreapellido_w");
        $this->setCampos("nombreapellido_h","Nombreapellido_h");
        $this->setCampos("nombreapellido_color","Nombreapellido_color");
        $this->setCampos("nombreapellido_fuentetamano","Nombreapellido_fuentetamano");
        $this->setCampos("nombreapellido_fuenteletra","Nombreapellido_fuenteletra");
        $this->setCampos("nombreapellido_bgcolor_chk","Nombreapellido_bgcolor_chk");
        $this->setCampos("nombreapellido_bgcolor","Nombreapellido_bgcolor");
        $this->setCampos("nombreapellido_fuentealign","Nombreapellido_fuentealign");
        $this->setCampos("nombreapellido_fuentevalign","Nombreapellido_fuentevalign");
        $this->setCampos("cedula_top","Cedula_top");
        $this->setCampos("cedula_left","Cedula_left");
        $this->setCampos("cedula_w","Cedula_w");
        $this->setCampos("cedula_h","Cedula_h");
        $this->setCampos("cedula_color","Cedula_color");
        $this->setCampos("cedula_fuentetamano","Cedula_fuentetamano");
        $this->setCampos("cedula_fuenteletra","Cedula_fuenteletra");
        $this->setCampos("cedula_bgcolor_chk","Cedula_bgcolor_chk");
        $this->setCampos("cedula_bgcolor","Cedula_bgcolor");
        $this->setCampos("cedula_fuentealign","Cedula_fuentealign");
        $this->setCampos("cedula_fuentevalign","Cedula_fuentevalign");
        $this->setCampos("fecha_chk","Fecha_chk");
        $this->setCampos("fecha_top","Fecha_top");
        $this->setCampos("fecha_left","Fecha_left");
        $this->setCampos("fecha_w","Fecha_w");
        $this->setCampos("fecha_h","Fecha_h");
        $this->setCampos("fecha_color","Fecha_color");
        $this->setCampos("fecha_fuentetamano","Fecha_fuentetamano");
        $this->setCampos("fecha_fuenteletra","Fecha_fuenteletra");
        $this->setCampos("fecha_bgcolor_chk","Fecha_bgcolor_chk");
        $this->setCampos("fecha_bgcolor","Fecha_bgcolor");
        $this->setCampos("fecha_fuentealign","Fecha_fuentealign");
        $this->setCampos("fecha_fuentevalign","Fecha_fuentevalign");
        $this->setCampos("visitante_top","visitante_top");
        $this->setCampos("visitante_left","visitante_left");
        $this->setCampos("visitante_w","visitante_w");
        $this->setCampos("visitante_h","visitante_h");
        $this->setCampos("visitante_color","visitante_color");
        $this->setCampos("visitante_fuentetamano","visitante_fuentetamano");
        $this->setCampos("visitante_fuenteletra","visitante_fuenteletra");
        $this->setCampos("visitante_bgcolor_chk","visitante_bgcolor_chk");
        $this->setCampos("visitante_bgcolor","visitante_bgcolor");
        $this->setCampos("visitante_fuentealign","visitante_fuentealign");
        $this->setCampos("visitante_fuentevalign","visitante_fuentevalign");
        $this->setCampos("visitantenro_top","visitantenro_top");
        $this->setCampos("visitantenro_left","visitantenro_left");
        $this->setCampos("visitantenro_w","visitantenro_w");
        $this->setCampos("visitantenro_h","visitantenro_h");
        $this->setCampos("visitantenro_color","visitantenro_color");
        $this->setCampos("visitantenro_fuentetamano","visitantenro_fuentetamano");
        $this->setCampos("visitantenro_fuenteletra","visitantenro_fuenteletra");
        $this->setCampos("visitantenro_bgcolor_chk","visitantenro_bgcolor_chk");
        $this->setCampos("visitantenro_bgcolor","visitantenro_bgcolor");
        $this->setCampos("visitantenro_fuentealign","visitantenro_fuentealign");
        $this->setCampos("visitantenro_fuentevalign","visitantenro_fuentevalign");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"plantilla","tipoDato"=>"esAlfaNumericoSinEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"orientacion","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"archivo_plantilla","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"archivoback_plantilla","tipoDato"=>"");

        //---------------------------------------------------LOGO----------------------------------------------------------------------//
        $logoChk = false;
        if ($_REQUEST["logo_chk"]=="1"){
            $logoChk = true;
        }
        $datos[] = array("isRequired"=>$logoChk,"datoName"=>"logo_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$logoChk,"datoName"=>"archivo_logo","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$logoChk,"datoName"=>"logo_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$logoChk,"datoName"=>"logo_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$logoChk,"datoName"=>"logo_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$logoChk,"datoName"=>"logo_h","tipoDato"=>"esNumerico");

        //----------------------------------------------------FOTO-----------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"foto_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"foto_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"foto_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"foto_h","tipoDato"=>"esNumerico");

        //----------------------------------------------------BARCODE-----------------------------------------------------------------------//
        $barcodeChk = false;
        if ($_REQUEST["barcode_chk"]=="1"){
            $barcodeChk = true;
        }
        $datos[] = array("isRequired"=>$barcodeChk,"datoName"=>"barcode_chk","tipoDato"=>"esNumerico");
        //----------------------------------------------------BARCODE-----------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$barcodeChk,"datoName"=>"barcode_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$barcodeChk,"datoName"=>"barcode_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$barcodeChk,"datoName"=>"barcode_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$barcodeChk,"datoName"=>"barcode_h","tipoDato"=>"esNumerico");
        
        //-------------------------------------------------INSTITUCION-------------------------------------------------------------------//
        $institucionChk = false;
        if ($_REQUEST["institucion_chk"]=="1"){
            $institucionChk = true;
        }
        $datos[] = array("isRequired"=>$institucionChk,"datoName"=>"institucion_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$institucionChk,"datoName"=>"institucion_nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$institucionChk,"datoName"=>"institucion_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$institucionChk,"datoName"=>"institucion_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$institucionChk,"datoName"=>"institucion_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$institucionChk,"datoName"=>"institucion_h","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$institucionChk,"datoName"=>"institucion_color","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"institucion_fuentetamano","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"institucion_fuenteletra","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"institucion_bgcolor_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"institucion_bgcolor","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"institucion_fuentealign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"institucion_fuentevalign","tipoDato"=>"esAlfaNumericoConEspacios");
        
        //------------------------------------------------NOMBRE Y APELLUDO---------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_h","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_color","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_fuentetamano","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_fuenteletra","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"nombreapellido_bgcolor_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_bgcolor","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_fuentealign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombreapellido_fuentevalign","tipoDato"=>"esAlfaNumericoConEspacios");
        
        //---------------------------------------------------CEDULA----------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_h","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_color","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_fuentetamano","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_fuenteletra","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"cedula_bgcolor_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_bgcolor","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_fuentealign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula_fuentevalign","tipoDato"=>"esAlfaNumericoConEspacios");        
        //-------------------------------------------------FECHA-------------------------------------------------------------------//
        $fechaChk = false;
        if ($_REQUEST["fecha_chk"]=="1"){
            $fechaChk = true;
        }
        $datos[] = array("isRequired"=>$fechaChk,"datoName"=>"fecha_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$fechaChk,"datoName"=>"fecha_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$fechaChk,"datoName"=>"fecha_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$fechaChk,"datoName"=>"fecha_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$fechaChk,"datoName"=>"fecha_h","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>$fechaChk,"datoName"=>"fecha_color","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"fecha_fuentetamano","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"fecha_fuenteletra","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"fecha_bgcolor_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"fecha_bgcolor","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"fecha_fuentealign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"fecha_fuentevalign","tipoDato"=>"esAlfaNumericoConEspacios");

        //------------------------------------------------VISITANTE---------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_h","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_color","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_fuentetamano","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_fuenteletra","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"visitante_bgcolor_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_bgcolor","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_fuentealign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitante_fuentevalign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        //------------------------------------------------VISITANTE NUMERO--------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_top","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_left","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_w","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_h","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_color","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_fuentetamano","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_fuenteletra","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"visitantenro_bgcolor_chk","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_bgcolor","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_fuentealign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"visitantenro_fuentevalign","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

    /**
     * Carga un archivo en el servidor
     * @param file $fileObj Archivo cargado en HTML
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    private function uploadFile($fileObj){
        $exito = true;
        $aux = explode("_", $fileObj);
        $name = $aux[1];
        $archivo = "archivo";
        if ($aux[0]=="fileback"){
            $archivo .= "back";
            
        }
        $file = $_FILES[$fileObj]['name'];

        //echo "<p>".$fileObj."---".$archivo."---".$name."---".$file."<br></p>";

        if (!empty($file)){

            if ($name=="plantilla" ){
                $maxWidth = $this->maxWidthTemplate;
                $maxHeight = $this->maxHeightTemplate;
                
            }
            if ($name=="logo"){
                $maxWidth = $this->maxWidthLogo;
                $maxHeight = $this->maxHeightLogo;
                
            }
            if ($aux[0]=="fileback"){
                $name .= "Reverso";
            }
            $imageInfo = getimagesize($_FILES[$fileObj]["tmp_name"]);
            if (!$imageInfo){
                $this->mensaje = "El archivo de ".ucfirst($name)." debe ser una imagen.";
                return false;
            }else{
                if ($imageInfo[0] > $maxWidth || $imageInfo[1] > $maxHeight){
                    $this->mensaje = "La imagen de ".ucfirst($name)." es muy grande. Debe tener un m&aacute;ximo de ".$maxWidth." x ".$maxHeight." pixeles.";
                    return false;
                }
            }


            $size = $_FILES[$fileObj]['size'];
            if ($size > $this->maxSize){
                $this->mensaje = "La imagen de ".ucfirst($name)." es muy grande. Debe tener un m&aacute;ximo de ".$this->file_size($this->maxSize);
                return false;
            }

            // Eliminar archivo ya creado //
            if (!empty($_REQUEST[$archivo.'_'.$name])){
                $exito = unlink($this->uploaddir.$_REQUEST[$archivo.'_'.$name]);
            }

            if ($exito){

                $ext = substr($file,strlen($file)-4,4);
                if ($_REQUEST["plantilla"]){
                    $file = $name."_".$_REQUEST["plantilla"].$ext;
                    $url_file = $this->uploaddir . $file;
                    $exito = move_uploaded_file($_FILES[$fileObj]['tmp_name'], $url_file);
                }
            }

        }else{
            if (!empty($_REQUEST[$archivo.'_'.$name])){
                $file = $_REQUEST[$archivo.'_'.$name];
            }
        }
        //echo "<p>|-----------1</p><p>".$fileObj."---".$archivo."---".$name."---".$file."<br></p>";
        if ($aux[0]=="fileback"){
            $name = substr($name, 0,-7);
        }
        //echo "<p>|-----------2</p><p>".$fileObj."---".$archivo."---".$name."---".$file."<br></p>";
        $_REQUEST[$archivo.'_'.$name]= $file;

        if (!$exito){
            $this->mensaje = "Ocurri&oacute; un error al guardar la imagen de ".ucfirst($name).".";
            return false;
        }
        return true;
    }

    /**
     * Elimina los datos y coordenadas de un objeto del carnet
     * @param string $names Nombre del objeto
     */
    private function unsetCampos($names){        
        foreach ($names as $name) {
                        
            // Si se establecio el codigo de barras para el frente o reverso del carnet 
            if ($name=="barcode"){
                if ($_REQUEST[$name."_front"]||$_REQUEST[$name."_back"]){
                    continue;
                }
            }            
                
            if (!$_REQUEST[$name."_chk"]){

                $_REQUEST["archivo_".$name] = "";
                //$_REQUEST[$name."_nombre"]  = "";

                $_REQUEST[$name."_top"]     = "";
                $_REQUEST[$name."_left"]    = "";
                $_REQUEST[$name."_w"]       = "";
                $_REQUEST[$name."_h"]       = "";
                $_REQUEST[$name."_color"]   = "";

            }
        }
    }

    /**
     * Establece la coordenada 0,0 de un objeto del carnet
     * @param string $names Nombre del objeto
     */
    private function setZeroTopLeft($names){
        foreach ($names as $name) {
            $_REQUEST[$name."_top"]  = number_format($_REQUEST[$name."_top"] == ""?"0":$_REQUEST[$name."_top"]);
            $_REQUEST[$name."_left"] = number_format($_REQUEST[$name."_left"]== ""?"0":$_REQUEST[$name."_left"]);
        }
    }

    /**
     * Generacion Local de listas dinamicas segun datos de base de datos
     * @param string $fields campos a seleccionar en la consulta
     * @param string $tabla tabla a consultar en base de datos
     * @param string $arg argumento consulta de base de datos
     * @param string $equal valor a comparar por metodo busca_valor()
     * @return string Devuelve opciones de listas dinamicas HTML
     */
    static function comboLocal($fields,$tabla,$arg, $equal){
        $sql  = "select ".$fields." from $tabla $arg";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        $nrows = $res->rowCount();
        $fetchArray = $res->fetchAll();

        $resp = "";
        $resp .= "<option value=\"\" idplantilla=\"\" chk=\"\" top=\"\" left=\"\" width=\"\" height=\"\" color=\"\" bgcolor=\"\" fuentetamano=\"\" fuenteletra=\"\" bgcolor_chk=\"\" fuentealign=\"\" fuentevalign=\"\" >Seleccione</option>";
        if ($nrows>0){
            foreach ($fetchArray as $row){

                $resp .= "<option value=\"".trim($row["id"])."\" idplantilla=\"".trim($row["idplantilla"])."\" chk=\"".trim($row["chk"])."\" top=\"".trim($row["top"])."\" left=\"".trim($row["left_"])."\" width=\"".trim($row["width"])."\" height=\"".trim($row["height"])."\" color=\"".trim($row["color"])."\" bgcolor=\"".trim($row["bgcolor"])."\" fuentetamano=\"".trim($row["fuentetamano"])."\" fuenteletra=\"".trim($row["fuenteletra"])."\" bgcolor_chk=\"".trim($row["bgcolor_chk"])."\" fuentealign=\"".trim($row["fuentealign"])."\" fuentevalign=\"".trim($row["fuentevalign"])."\" ".self::busca_valor($equal, $row["id"])." >";

                $resp .= $row["nombre"]." &nbsp;";

                $resp .= "</option>\n";

            }
        }else{
            $resp .= "<option value=\"\">No hay registros</option>";
        }
        return $resp;
    }

    /**
     * Generacion de tabla de opciones de Estilos para el HTML
     * @param array $ops Matriz de objetos a generar
     * @param object $data Datos almacenados en la BD
     * @return string Devuelve las filas de la tabla en HTML
     */
    static function setStyleInputs($ops,$data){

        $t = Controller::getTabulation("13");

        $str = "";
        foreach ($ops as $op){

            $str .=  "<!------------------------------------------------------- ".strtoupper($op->name)." ------------------------------------------------------->\n".$t;
            $str .= "<tr class=\"".$op->tipo."\">\n".$t;

            // --------------------------------------------------------- ETIQUETA ---------------------------------------------------- //
            $str .= "    <td>\n".$t;
            $str .= "        ".$op->name."\n".$t;
            if ($op->obj=="fecha" || $op->obj=="institucion"|| $op->obj=="institucionback"){
                $obj  = $op->obj."_chk";
                $checked = $data->$obj=="1"?"checked":"";
                $str .= "        <input type=\"checkbox\" name=\"".$op->obj."_chk\" id=\"".$op->obj."_chk\" ".$checked." ".$op->disabled." value=\"1\" title=\"Establecer etiqueta en el carnet\" >\n".$t;
            }
            if ($op->obj=="cargo" ){
                $str .= "        <input type=\"checkbox\" name=\"".$op->obj."_nombre_chk\" id=\"".$op->obj."_nombre_chk\" ".$op->disabled." value=\"1\" title=\"Establecer etiqueta del nombre del cargo en el carnet\" >\n".$t;
            }
            $str .= "    </td>\n".$t;

            // --------------------------------------------------------- FUENTE ------------------------------------------------------ //
            $str .= "    <td>\n".$t;
            $str .= "        <input type=\"text\" name=\"".$op->obj."_color_Control\" id=\"".$op->obj."_color_Control\" class=\"colorControl\" readonly title=\"Color de la fuente\" ".$op->disabled."/>\n".$t;
            $obj  = $op->obj."_color";
            $colorDefault = "000000";
            $value = !isset($data->$obj)?$colorDefault:$data->$obj;
            $str .= "        <input type=\"hidden\" name=\"".$op->obj."_color\" id=\"".$op->obj."_color\" value=\"".$value."\" />\n".$t;
            $str .= "\n".$t;
            $str .= "        <select name=\"".$op->obj."_fuentetamano\" id=\"".$op->obj."_fuentetamano\" class=\"styleObjsCombos\" title=\"Tama&ntilde;o de la fuente en Puntos\" ".$op->disabled.">\n".$t;
            $obj  = $op->obj."_fuentetamano";
            for ($i=10;$i<=40;$i++){
                $selected = !isset($data->$obj)?Controller::busca_valor(($i/2),11):Controller::busca_valor(($i/2), $data->$obj);
                $str .= "            <option value=\"".($i/2)."\" ".$selected.">".$i." Pt</option>\n".$t;
            }
            $str .= "         </select>\n".$t;
            $str .= "\n".$t;
            $obj  = $op->obj."_fuenteletra";
            $str .= "         <select name=\"".$op->obj."_fuenteletra\" id=\"".$op->obj."_fuenteletra\" class=\"styleObjsCombos\" title=\"Tipo de fuente\" ".$op->disabled." >\n".$t;
            $str .= "             <option value=\"arial\" ".Controller::busca_valor("arial", $data->$obj)." style=\"font-family: arial\">Arial</option>\n".$t;
            $str .= "             <option value=\"times\" title=\"Times New Roman\"  ".Controller::busca_valor("times", $data->$obj)." style=\"font-family: timesnewroman\">Times</option>\n".$t;
            $str .= "             <option value=\"verdana\" ".Controller::busca_valor("verdana", $data->$obj)." style=\"font-family: verdana\">Verdana</option>\n".$t;
            $str .= "         </select>\n".$t;
            $str .= "    </td>\n".$t;

            // --------------------------------------------------------- FONDO ---------------------------------------------------------- //
            $str .= "    <td>\n".$t;
            if ($op->obj!="cargo"){
                $obj  = $op->obj."_bgcolor_chk";                
                $checked = $data->$obj=="1"?"checked":"";
                
                $str .= "        <input type=\"checkbox\" name=\"".$op->obj."_bgcolor_chk\" id=\"".$op->obj."_bgcolor_chk\" ".$checked." ".$op->disabled." value=\"1\" title=\"Establecer fondo\">\n".$t;
                $str .= "        <input type=\"text\" name=\"".$op->obj."_bgcolor_Control\" id=\"".$op->obj."_bgcolor_Control\" class=\"colorControl\" readonly title=\"Color del fondo\" ".$op->disabled."/>\n".$t;
                $obj  = $op->obj."_bgcolor";
                $colorDefault = "ffffff";                
                $value = !isset($data->$obj)?$colorDefault:$data->$obj;
                $str .= "        <input type=\"hidden\" name=\"".$op->obj."_bgcolor\" id=\"".$op->obj."_bgcolor\" value=\"".$value."\" />\n".$t;
            }

            $str .= "    </td>\n".$t;

            // --------------------------------------------------------- ALINEACION ------------------------------------------------------ //
            $str .= "    <td>\n".$t;
            $obj  = $op->obj."_fuentealign";
            $str .= "        H:<select name=\"".$op->obj."_fuentealign\" id=\"".$op->obj."_fuentealign\" class=\"styleObjsCombos\" title=\"Alineaci&oacute;n horizontal de la fuente\" ".$op->disabled." style=\"width: 90px\">\n".$t;
            $str .= "            <option value=\"left\" ".Controller::busca_valor("left", $data->$obj).">Izquierda</option>\n".$t;
            $str .= "            <option value=\"center\" ".Controller::busca_valor("center", $data->$obj).">Centro</option>\n".$t;
            $str .= "            <option value=\"right\" ".Controller::busca_valor("right", $data->$obj).">Derecha</option>\n".$t;
            $str .= "        </select>\n".$t;
            $str .= "        <br />\n".$t;
            $obj  = $op->obj."_fuentevalign";
            $str .= "         V:<select name=\"".$op->obj."_fuentevalign\" id=\"".$op->obj."_fuentevalign\" class=\"styleObjsCombos\" title=\"Alineaci&oacute;n vertical de la fuente\" ".$op->disabled." style=\"width: 90px\">\n".$t;
            $str .= "            <option value=\"top\" ".Controller::busca_valor("top", $data->$obj).">Tope</option>\n".$t;
            $str .= "            <option value=\"middle\" ".Controller::busca_valor("middle", $data->$obj).">Medio</option>\n".$t;
            $str .= "            <option value=\"bottom\" ".Controller::busca_valor("bottom", $data->$obj).">Fondo</option>\n".$t;
            $str .= "        </select>\n".$t;
            $str .= "    </td>\n".$t;
            $str .= "</tr>\n".$t;
        }
        return $str;
    }
}
?>