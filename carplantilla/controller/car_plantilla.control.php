<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../model/car_plantilla.model.php"; // Class MODEL CarPlantilla()
include_once "../../inicio/controller/controller.php";

/**
 * Description
 * @author David Concepcion
 */
class ControlCarPlantilla extends Controller{
    /**
     * @var string mensaje de exito o error
     */
    var $mensaje = null;
    /**
     * @var string accion agregar, modificar o eliminar dato
     */
    var $accion  = null;
    /**
     * @var array nombre de campos campos
     */
    var $campos  = null;
    /**
     * @var string nombre de la entidad 
     */
    var $entidad  = null;

    /**
     * Establece la acción
     * @param string $accion Acción
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }

    /**
     * @return string Devuelve la accion establecida
     */
    public function getAccion(){
         return $this->accion;
    }

    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }

    /**
     * Establece la Entidad
     * @param string $entidad Entidad
     */
    public function setEntidad($entidad){
         $this->entidad = $entidad;
    }

    /**
     * @return string Devuelve la Entidad establecida
     */
    public function getEntidad(){
         return $this->entidad;
    }
    
    /**
     * Agregar o modificar un CarPlantilla
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setCarPlantilla(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->sonValidosDatos()) return false;

        $obj = new CarPlantilla();

        //--------------------- DATOS --------------------------//
        $obj->setIdplantilla($_REQUEST["idplantilla"]);
        
        $obj->setPlantilla($_REQUEST["plantilla"]);
        $obj->setOrientacion($_REQUEST["orientacion"]);
        $obj->setArchivo_plantilla($_REQUEST["archivo_plantilla"]);
        $obj->setArchivoback_plantilla($_REQUEST["archivoback_plantilla"]);
        $obj->setLogo_chk($_REQUEST["logo_chk"]);
        $obj->setArchivo_logo($_REQUEST["archivo_logo"]);
        $obj->setFoto_top($_REQUEST["foto_top"]);
        $obj->setFoto_left($_REQUEST["foto_left"]);
        $obj->setFoto_w($_REQUEST["foto_w"]);
        $obj->setFoto_h($_REQUEST["foto_h"]);
        $obj->setBarcode_chk($_REQUEST["barcode_chk"]);
        $obj->setBarcode_front($_REQUEST["barcode_front"]);
        $obj->setBarcode_back($_REQUEST["barcode_back"]);
        $obj->setBarcode_top($_REQUEST["barcode_top"]);
        $obj->setBarcode_left($_REQUEST["barcode_left"]);
        $obj->setBarcode_w($_REQUEST["barcode_w"]);
        $obj->setBarcode_h($_REQUEST["barcode_h"]);
        $obj->setLogo_top($_REQUEST["logo_top"]);
        $obj->setLogo_left($_REQUEST["logo_left"]);
        $obj->setLogo_w($_REQUEST["logo_w"]);
        $obj->setLogo_h($_REQUEST["logo_h"]);
        $obj->setLogoback_chk($_REQUEST["logoback_chk"]);
        $obj->setArchivoback_logo($_REQUEST["archivoback_logo"]);
        $obj->setLogoback_top($_REQUEST["logoback_top"]);
        $obj->setLogoback_left($_REQUEST["logoback_left"]);
        $obj->setLogoback_w($_REQUEST["logoback_w"]);
        $obj->setLogoback_h($_REQUEST["logoback_h"]);
        $obj->setInstitucion_chk($_REQUEST["institucion_chk"]);
        $obj->setInstitucion_nombre($_REQUEST["institucion_nombre"]);
        $obj->setInstitucion_top($_REQUEST["institucion_top"]);
        $obj->setInstitucion_left($_REQUEST["institucion_left"]);
        $obj->setInstitucion_w($_REQUEST["institucion_w"]);
        $obj->setInstitucion_h($_REQUEST["institucion_h"]);
        $obj->setInstitucion_color($_REQUEST["institucion_color"]);
        $obj->setInstitucion_fuentetamano($_REQUEST["institucion_fuentetamano"]);
        $obj->setInstitucion_fuenteletra($_REQUEST["institucion_fuenteletra"]);
        $obj->setInstitucion_bgcolor_chk($_REQUEST["institucion_bgcolor_chk"]);
        $obj->setInstitucion_bgcolor($_REQUEST["institucion_bgcolor"]);
        $obj->setInstitucion_fuentealign($_REQUEST["institucion_fuentealign"]);
        $obj->setInstitucion_fuentevalign($_REQUEST["institucion_fuentevalign"]);
        $obj->setInstitucionback_chk($_REQUEST["institucionback_chk"]);
        $obj->setInstitucionback_top($_REQUEST["institucionback_top"]);
        $obj->setInstitucionback_left($_REQUEST["institucionback_left"]);
        $obj->setInstitucionback_w($_REQUEST["institucionback_w"]);
        $obj->setInstitucionback_h($_REQUEST["institucionback_h"]);
        $obj->setInstitucionback_color($_REQUEST["institucionback_color"]);
        $obj->setInstitucionback_fuentetamano($_REQUEST["institucionback_fuentetamano"]);
        $obj->setInstitucionback_fuenteletra($_REQUEST["institucionback_fuenteletra"]);
        $obj->setInstitucionback_bgcolor_chk($_REQUEST["institucionback_bgcolor_chk"]);
        $obj->setInstitucionback_bgcolor($_REQUEST["institucionback_bgcolor"]);
        $obj->setInstitucionback_fuentealign($_REQUEST["institucionback_fuentealign"]);
        $obj->setInstitucionback_fuentevalign($_REQUEST["institucionback_fuentevalign"]);
        $obj->setNombreapellido_top($_REQUEST["nombreapellido_top"]);
        $obj->setNombreapellido_left($_REQUEST["nombreapellido_left"]);
        $obj->setNombreapellido_w($_REQUEST["nombreapellido_w"]);
        $obj->setNombreapellido_h($_REQUEST["nombreapellido_h"]);
        $obj->setNombreapellido_color($_REQUEST["nombreapellido_color"]);
        $obj->setNombreapellido_fuentetamano($_REQUEST["nombreapellido_fuentetamano"]);
        $obj->setNombreapellido_fuenteletra($_REQUEST["nombreapellido_fuenteletra"]);
        $obj->setNombreapellido_bgcolor_chk($_REQUEST["nombreapellido_bgcolor_chk"]);
        $obj->setNombreapellido_bgcolor($_REQUEST["nombreapellido_bgcolor"]);
        $obj->setNombreapellido_fuentealign($_REQUEST["nombreapellido_fuentealign"]);
        $obj->setNombreapellido_fuentevalign($_REQUEST["nombreapellido_fuentevalign"]);
        $obj->setCedula_top($_REQUEST["cedula_top"]);
        $obj->setCedula_left($_REQUEST["cedula_left"]);
        $obj->setCedula_w($_REQUEST["cedula_w"]);
        $obj->setCedula_h($_REQUEST["cedula_h"]);
        $obj->setCedula_color($_REQUEST["cedula_color"]);
        $obj->setCedula_fuentetamano($_REQUEST["cedula_fuentetamano"]);
        $obj->setCedula_fuenteletra($_REQUEST["cedula_fuenteletra"]);
        $obj->setCedula_bgcolor_chk($_REQUEST["cedula_bgcolor_chk"]);
        $obj->setCedula_bgcolor($_REQUEST["cedula_bgcolor"]);
        $obj->setCedula_fuentealign($_REQUEST["cedula_fuentealign"]);
        $obj->setCedula_fuentevalign($_REQUEST["cedula_fuentevalign"]);
        $obj->setFecha_chk($_REQUEST["fecha_chk"]);
        $obj->setFecha_top($_REQUEST["fecha_top"]);
        $obj->setFecha_left($_REQUEST["fecha_left"]);
        $obj->setFecha_w($_REQUEST["fecha_w"]);
        $obj->setFecha_h($_REQUEST["fecha_h"]);
        $obj->setFecha_color($_REQUEST["fecha_color"]);
        $obj->setFecha_fuentetamano($_REQUEST["fecha_fuentetamano"]);
        $obj->setFecha_fuenteletra($_REQUEST["fecha_fuenteletra"]);
        $obj->setFecha_bgcolor_chk($_REQUEST["fecha_bgcolor_chk"]);
        $obj->setFecha_bgcolor($_REQUEST["fecha_bgcolor"]);
        $obj->setFecha_fuentealign($_REQUEST["fecha_fuentealign"]);
        $obj->setFecha_fuentevalign($_REQUEST["fecha_fuentevalign"]);
        $obj->setVisitante_top($_REQUEST["visitante_top"]);
        $obj->setVisitante_left($_REQUEST["visitante_left"]);
        $obj->setVisitante_w($_REQUEST["visitante_w"]);
        $obj->setVisitante_h($_REQUEST["visitante_h"]);
        $obj->setVisitante_color($_REQUEST["visitante_color"]);
        $obj->setVisitante_fuentetamano($_REQUEST["visitante_fuentetamano"]);
        $obj->setVisitante_fuenteletra($_REQUEST["visitante_fuenteletra"]);
        $obj->setVisitante_bgcolor_chk($_REQUEST["visitante_bgcolor_chk"]);
        $obj->setVisitante_bgcolor($_REQUEST["visitante_bgcolor"]);
        $obj->setVisitante_fuentealign($_REQUEST["visitante_fuentealign"]);
        $obj->setVisitante_fuentevalign($_REQUEST["visitante_fuentevalign"]);
        $obj->setVisitantenro_top($_REQUEST["visitantenro_top"]);
        $obj->setVisitantenro_left($_REQUEST["visitantenro_left"]);
        $obj->setVisitantenro_w($_REQUEST["visitantenro_w"]);
        $obj->setVisitantenro_h($_REQUEST["visitantenro_h"]);
        $obj->setVisitantenro_color($_REQUEST["visitantenro_color"]);
        $obj->setVisitantenro_fuentetamano($_REQUEST["visitantenro_fuentetamano"]);
        $obj->setVisitantenro_fuenteletra($_REQUEST["visitantenro_fuenteletra"]);
        $obj->setVisitantenro_bgcolor_chk($_REQUEST["visitantenro_bgcolor_chk"]);
        $obj->setVisitantenro_bgcolor($_REQUEST["visitantenro_bgcolor"]);
        $obj->setVisitantenro_fuentealign($_REQUEST["visitantenro_fuentealign"]);
        $obj->setVisitantenro_fuentevalign($_REQUEST["visitantenro_fuentevalign"]);
        

        if ($this->getAccion()=="agregar"){
            $exito = $obj->insertarRegistro();
        }

        if ($this->getAccion()=="modificar"){
            $exito = $obj->modificarRegistro($obj->getIdplantilla($_REQUEST["idplantilla"]));
        }

        // ------------------------------- MENSAJE ERROR --------------------------------//
        if(!$exito){
            if ($this->getAccion()=="agregar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser creado: ".$obj->getMensaje();
            }
            if ($this->getAccion()=="modificar"){
                $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser modificado: ".$obj->getMensaje();
            }
            return false;
        }
        // ------------------------------- MENSAJE EXITO --------------------------------//
        if ($this->getAccion()=="agregar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; creado exitosamente...";
        }
        if ($this->getAccion()=="modificar"){
            $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; modificado exitosamente...";
        }


        return true;
    }

    /**
     * === Eliminar CarPlantilla ===
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarCarPlantilla(){
        // -------------------- ELIMINAR LECTORA ----------------------//
        $obj = new CarPlantilla();
        //--------------------- DATOS --------------------------//
        $obj->setIdplantilla($_REQUEST["idplantilla"]);

        $exito = $obj->eliminarRegistro($obj->getIdplantilla($_REQUEST["idplantilla"]));
        if(!$exito){
            $this->mensaje = "El registro de ".$this->getEntidad()." no pudo ser eliminado: ".$obj->getMensaje();
            return false;
        }

        $this->mensaje = "El registro de ".$this->getEntidad()." fu&eacute; eliminado exitosamente...";
        return true;
    }

    
    
}
?>
