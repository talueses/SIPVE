<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/car_plantilla.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new CarPlantillas();
    $data = $obj->getCarPlantilla($_REQUEST["idplantilla"]);
    

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link type="text/css" href="../../inicio/css/colorpicker.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/colorpicker.js"></script>
        <script type="text/javascript" src="../js/car_plantilla.local.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 98%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }            
            #botones{
                margin: 10px;
            }
            #divmensaje{
                width:99%;
                position: absolute;
                top: -10px;
                opacity:0.9;
            }
            /*-------------------------------------------------------------------------------*/
            #divStyleObjs, #styleObjs{
                text-align: center;
                width:99%;
                background:#fff;
                padding: 2px 5px 2px 5px;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #styleObjsTitle{
                cursor: pointer;
                margin-bottom: 3px;
            }
            #styleObjs{
                width:100%;
            }
            #styleObjs tr {                
                    background:#fff;
            }
            #styleObjs tr th,#styleObjsTitle {

                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #styleObjs tr:hover td {
                border:  #aaaaaa dashed 1px;
                -moz-border-radius: 4px;
                -webkit-border-radius: 4px;
                border-radius: 4px;
                background:#dd0;
            }
            #styleObjs tr td {
                border:  #fff dashed 1px;
                text-align: center;
            }
            #styleObjs tr td:first-child, #styleObjs tr td:last-child{
                text-align: left;
            }
            /*-------------------------------------------------------------------------------*/
            #divCarnet{
                border:#000000 2px dashed;
                position: relative;
            }
            .carnetObjects:hover{
                opacity:0.5;
            }
            .carnetObjects{
                border:#000000 1px dashed;
                position: absolute;                
            }
            .carnetObjectsView{
                position: relative;
                width: 100%;
                height: 100%;
                display: table;
            }
            .carnetObjectsView p{
                display: table-cell;
            }
            
            .ui-resizable-se {
                right: 1px;
                bottom: 1px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Plantilla</div>
                <br/>
                <div id="divmensaje"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="car_plantilla.Op.php" target="ifrm1" enctype="multipart/form-data">
                    <div id="datos"  align="left">
                        <table border="0">
                            <tr>
                                <td rowspan="2" valign="top">
                                    <div id="divCarnet">
                                        <img src="<?php echo $data->archivo_plantilla?"../images/imagesPlantillas/".$data->archivo_plantilla:"";?>" id="carnetImg" alt="" class="empleado visitante" />
                                        <img src="<?php echo $data->archivoback_plantilla?"../images/imagesPlantillas/".$data->archivoback_plantilla:"";?>" id="carnetImgBack" alt="" class="back" />
                                        <div id="foto" class="carnetObjects empleado" title="Foto tipo carnet"></div>
                                        <div id="logo" class="carnetObjects empleado visitante" title="Logo de la instituci&oacute;n"></div>                                        
                                        <div id="logoback" class="carnetObjects back" title="Logo de la instituci&oacute;n (Reverso del Carnet)"></div>

                                        <div id="institucion" class="carnetObjects" title="Nombre de la instituci&oacute;n" >
                                            <div class="carnetObjectsView" >
                                                <p id="institucion_view">Instituci&oacute;n</p>
                                            </div>
                                        </div>
                                        <div id="nombreapellido" class="carnetObjects empleado" title="Primer Nombre y primer Apellido">
                                            <div class="carnetObjectsView">
                                                <p id="nombreapellido_view">Nombre 1 Apellido 1</p>
                                            </div>
                                        </div>
                                        <div id="cedula" class="carnetObjects empleado" title="C&eacute;dula de Indentidad">
                                            <div class="carnetObjectsView">
                                                <p id="cedula_view">C.I. XX.XXX.XXX</p>
                                            </div>                                            
                                        </div>
                                        <div id="departamento" class="carnetObjects empleado" title="Departamento">
                                            <div class="carnetObjectsView">
                                                <p id="departamento_view"></p>
                                            </div>                                            
                                        </div>
                                        <div id="cargo" class="carnetObjects empleado" title="Departamento">
                                            <div class="carnetObjectsView">
                                                <p id="cargo_view" ></p>
                                            </div>
                                        </div>
                                        <div id="barcode" class="carnetObjects" title="C&oacute;digo de Barras">
                                            <img src="../images/barcodeb39.png" width="100%" height="100%" />
                                        </div>
                                        <div id="fecha" class="carnetObjects empleado" title="Fecha de Expiraci&oacute;n">
                                            <div class="carnetObjectsView">
                                                <p id="fecha_view"><?php echo date("d-m-Y")?></p>
                                            </div>
                                        </div>
                                        <div id="visitante" class="carnetObjects visitante" title="Etiqueta de VISITANTE">
                                            <div class="carnetObjectsView">
                                                <p id="visitante_view">VISITANTE</p>
                                            </div>
                                        </div>
                                        <div id="visitantenro" class="carnetObjects visitante" title="Etiqueta de N&uacute;mero de visitante">
                                            <div class="carnetObjectsView">
                                                <p id="visitantenro_view">0001</p>
                                            </div>
                                        </div>
                                        <!--

                                        -->                                        
                                    </div>
                                </td>
                            </tr>
                            <tr >
                                <td valign="top" align="center">
                                    <table border="0" width="100%">
                                        <tr title="Tipo de Carnet">
                                            <td align="right">Modo:</td>
                                            <td>
                                                <!--select name="tipo_carnet" id="tipo_carnet" >
                                                    <option value="empleado" <?php echo Controller::busca_valor($data->tipo_carnet, "empleado");?>>Empleado</option>
                                                    <option value="visitante" <?php echo Controller::busca_valor($data->tipo_carnet, "visitante"); ?>>Visitante</option>
                                                    <option value="back" <?php echo Controller::busca_valor($data->tipo_carnet, "back"); ?>>Reverso</option>
                                                </select-->
                                                
                                                <div id="radio_tipo_carnet">
                                                    <input type="radio" id="tipo_carnet_empleado" class="tipo_carnet" name="radio" value="empleado" checked="checked" /><label for="tipo_carnet_empleado">Empleado</label>
                                                    <input type="radio" id="tipo_carnet_visitante" class="tipo_carnet" name="radio" value="visitante" /><label for="tipo_carnet_visitante">Visitante</label>
                                                    <input type="radio" id="tipo_carnet_back" class="tipo_carnet" name="radio" value="back" /><label for="tipo_carnet_back">Reverso</label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr title="Orientaci&oacute;n">
                                            <td align="right">Orientaci&oacute;n:</td>
                                            <td>
                                                <select name="orientacion" id="orientacion" <?php echo $disabled;?> >
                                                    <option value="H" <?php echo Controller::busca_valor($data->orientacion, "H");?>>Horizontal</option>
                                                    <option value="V" <?php echo Controller::busca_valor($data->orientacion, "V"); echo $data->orientacion==""?"selected":"";?>>Vertical</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr title="Nombre de la Plantilla">
                                            <td align="right" valign="top">Nombre Plantilla:</td>
                                            <td>
                                                <input type="text" name="plantilla" id="plantilla" maxlength="100" value="<?php echo $data->plantilla;?>" <?php echo $disabled; echo $_REQUEST["accion"]=="modificar"?"readonly":""?> />
                                                <small class="comment">No puede modificarse</small>
                                            </td>
                                        </tr>
                                        <tr title="Imagen de la Plantilla" class="empleado visitante">
                                            <td align="right">Imagen Plantilla:</td>
                                            <td>
                                                <input type="hidden" name="archivo_plantilla" id="archivo_plantilla" value="<?php echo $data->archivo_plantilla;?>"  />
                                                <input name="file_plantilla" type="file" id="file_plantilla" size="5" onchange="previewImg(this)" <?php echo $disabled;?>/>
                                            </td>
                                        </tr>
                                        <tr title="Imagen del Logo de la instituci&oacute;n" class="empleado visitante">
                                            <td align="right">Imagen Logo:</td>
                                            <td>                                                
                                                <input type="hidden" name="archivo_logo" id="archivo_logo" value="<?php echo $data->archivo_logo;?>"  />
                                                <input name="file_logo" type="file" id="file_logo" size="5" onchange="previewImg(this)" <?php echo $disabled;?>/>
                                                <input type="checkbox" name="logo_chk" id="logo_chk" <?php echo $data->logo_chk=="1"?"checked":""?> <?php echo $disabled;?> value="1" title="Establecer el logo en el carnet">
                                            </td>
                                        </tr>
                                        <tr title="Imagen del Reverso del Carnet" class="back">
                                            <td align="right">Imagen Reverso Plantilla:</td>
                                            <td>
                                                <input type="hidden" name="archivoback_plantilla" id="archivoback_plantilla" value="<?php echo $data->archivoback_plantilla;?>"  />
                                                <input name="fileback_plantilla" type="file" id="fileback_plantilla" size="5" onchange="previewImg(this)" <?php echo $disabled;?>/>
                                            </td>
                                        </tr>
                                        <tr title="Imagen del Logo de la instituci&oacute;n en el Reverso del Carnet" class="back">
                                            <td align="right">Imagen Logo:</td>
                                            <td>                                                
                                                <input type="hidden" name="archivoback_logo" id="archivoback_logo" value="<?php echo $data->archivoback_logo;?>"  />
                                                <input name="fileback_logo" type="file" id="fileback_logo" size="5" onchange="previewImg(this)" <?php echo $disabled;?>/>
                                                <input type="checkbox" name="logoback_chk" id="logoback_chk" <?php echo $data->logoback_chk=="1"?"checked":""?> <?php echo $disabled;?> value="1" title="Establecer el logo en el reverso carnet">
                                            </td>
                                        </tr>
                                        <tr title="Nombre de la Instituci&oacute;n" >
                                            <td align="right">Instituci&oacute;n:</td>
                                            <td valign="top">                                                
                                                <input type="text" name="institucion_nombre" id="institucion_nombre" class="nombres" maxlength="150" value="<?php echo $data->institucion_nombre;?>" <?php echo $disabled;?> />                                                
                                            </td>
                                        </tr>
                                        <tr title="C&oacute;digo de Barras">
                                            <td align="right" valign="top">C&oacute;digo de Barras:</td>
                                            <td> 
                                                <input type="checkbox" name="barcode_chk" id="barcode_chk" value="1" <?php echo $data->barcode_chk=="1"?"checked":""?> <?php echo $disabled;?> >
                                                <small class="comment" style="display: inline">Seleccione para utilizar el C&oacute;digo de Barras</small>
                                            
                                                <input type="hidden"  name="barcode_front" id="barcode_front" value="<?php echo $data->barcode_front;?>">
                                               
                                                <input type="hidden" name="barcode_back" id="barcode_back" value="<?php echo $data->barcode_back;?>"> 
                                                
                                            </td>
                                        </tr>
                                        <tr title="Departamento" class="empleado">
                                            <td align="right">Departamento:</td>
                                            <td valign="top">                                                
                                                <select name="departamento_id" id="departamento_id" <?php echo $disabled;?>  class="nombresCombos">                                                    
                                                    <?php echo ControlOpCarPlantilla::comboLocal("idplantilla, iddepartamento as id, departamento_nombre as nombre, departamento_top as top, departamento_left as left_, departamento_w as width, departamento_h as height, departamento_color as color, departamento_fuentetamano as fuentetamano, departamento_fuenteletra as fuenteletra, departamento_bgcolor_chk as bgcolor_chk, departamento_bgcolor as bgcolor, departamento_fuentealign as fuentealign, departamento_fuentevalign as fuentevalign", "car_departamento", "order by departamento_nombre", "");?>
                                                </select>              
                                            </td>
                                        </tr>
                                        <tr title="Cargo" class="empleado">
                                            <td align="right">Cargo:</td>
                                            <td valign="top">
                                                <select name="cargo_id" id="cargo_id" <?php echo $disabled;?>  class="nombresCombos">
                                                    <!--option value="" top="" left="" width="" height="" color="" bgcolor="">Seleccione</option-->
                                                    <?php echo ControlOpCarPlantilla::comboLocal("idplantilla, idcargo as id, cargo_nombre_chk as chk, cargo_nombre as nombre, cargo_top as top, cargo_left as left_, cargo_w as width, cargo_h as height, cargo_color as color, cargo_bgcolor as bgcolor, cargo_fuentetamano as fuentetamano, cargo_fuenteletra as fuenteletra, cargo_bgcolor as bgcolor, cargo_fuentealign as fuentealign, cargo_fuentevalign as fuentevalign", "car_cargo", "order by cargo_nombre", "");?>
                                                </select>
                                                
                                                <!--input type="checkbox" name="cargo_nombre_chk" id="cargo_nombre_chk" <?php echo $disabled;?> value="1" title="Establecer etiqueta del nombre"-->

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <div id="divStyleObjs">
                                                    <div id="styleObjsTitle">Estilos de las Etiquetas</div>                                                    
                                                    <table id="styleObjs">
                                                        <tr>
                                                            <th>Etiqueta</th>
                                                            <th>Fuente</th>
                                                            <th>Fondo</th>
                                                            <th>Alineaci&oacute;n</th>
                                                        </tr>
                                                        <?php
                                                        $ops[] = (object) array("name"=>"Instituci&oacute;n","obj"=>"institucion","disabled"=>$disabled,"tipo"=>"empleado visitante");
                                                        
                                                        $ops[] = (object) array("name"=>"Nombre y Apellido","obj"=>"nombreapellido","disabled"=>$disabled,"tipo"=>"empleado");
                                                        $ops[] = (object) array("name"=>"C&eacute;dula","obj"=>"cedula","disabled"=>$disabled,"tipo"=>"empleado");
                                                        $ops[] = (object) array("name"=>"Departamento","obj"=>"departamento","disabled"=>$disabled,"tipo"=>"empleado");
                                                        $ops[] = (object) array("name"=>"Cargo","obj"=>"cargo","disabled"=>$disabled,"tipo"=>"empleado");
                                                        $ops[] = (object) array("name"=>"Fecha","obj"=>"fecha","disabled"=>$disabled,"tipo"=>"empleado");
                                                        
                                                        $ops[] = (object) array("name"=>"Visitante","obj"=>"visitante","disabled"=>$disabled,"tipo"=>"visitante");
                                                        $ops[] = (object) array("name"=>"Visitante Nro.","obj"=>"visitantenro","disabled"=>$disabled,"tipo"=>"visitante");
                                                        
                                                        $ops[] = (object) array("name"=>"Instituci&oacute;n","obj"=>"institucionback","disabled"=>$disabled,"tipo"=>"back");
                                                        
                                                        echo ControlOpCarPlantilla::setStyleInputs($ops,$data);

                                                        ?>

                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="styleObjsDialog" title="">
                                        
                                    </div>
                                    <div id="botones" style="clear:left">
                                        
                                        <input type="hidden" id="defaultWidth" value="<?php echo Controller::$defaultWidth ;?>">
                                        <input type="hidden" id="defaultHeight" value="<?php echo Controller::$defaultHeight ;?>">
                                        
                                        <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                                        <input type="hidden" name="idplantilla" id="idplantilla" value="<?php echo $data->idplantilla ?>">
                                        
                                        <!--------------------------------------------------FOTO---------------------------------------------------
                                        Foto-->
                                        
                                        <input type="hidden" name="foto_top" id="foto_top" value="<?php echo !isset($data->foto_top)?"10":$data->foto_top;?>" >
                                        <input type="hidden" name="foto_left" id="foto_left" value="<?php echo !isset($data->foto_left)?"10":$data->foto_left;?>" >
                                        
                                        <input type="hidden" name="foto_w" id="foto_w" value="<?php echo !isset($data->foto_w)?"100":$data->foto_w;?>" >
                                        <input type="hidden" name="foto_h" id="foto_h" value="<?php echo !isset($data->foto_h)?"100":$data->foto_h;?>" >
                                        
                                        <!---------------------------------------------------LOGO------------------------------------------------------
                                        Logo-->
                                        
                                        <input type="hidden" name="logo_top" id="logo_top" value="<?php echo !isset($data->logo_top)?"10":$data->logo_top;?>" >
                                        <input type="hidden" name="logo_left" id="logo_left" value="<?php echo !isset($data->logo_left)?"200":$data->logo_left;?>" >
                                        
                                        <input type="hidden" name="logo_w" id="logo_w" value="<?php echo !isset($data->logo_w)?"100":$data->logo_w;?>" >
                                        <input type="hidden" name="logo_h" id="logo_h" value="<?php echo !isset($data->logo_h)?"100":$data->logo_h;?>" >
                                        
                                        <!---------------------------------------------------LOGO------------------------------------------------------
                                        Logo Reverso-->
                                        
                                        <input type="hidden" name="logoback_top" id="logoback_top" value="<?php echo !isset($data->logoback_top)?"10":$data->logoback_top;?>" >
                                        <input type="hidden" name="logoback_left" id="logoback_left" value="<?php echo !isset($data->logoback_left)?"200":$data->logoback_left;?>" >
                                        
                                        <input type="hidden" name="logoback_w" id="logoback_w" value="<?php echo !isset($data->logoback_w)?"100":$data->logoback_w;?>" >
                                        <input type="hidden" name="logoback_h" id="logoback_h" value="<?php echo !isset($data->logoback_h)?"100":$data->logoback_h;?>" >
                                        

                                        <!---------------------------------------------------INSTITUCION----------------------------------------------
                                        Institucion-->
                                        
                                        <input type="hidden" name="institucion_top" id="institucion_top" value="<?php echo !isset($data->institucion_top)?"120":$data->institucion_top;?>" >
                                        <input type="hidden" name="institucion_left" id="institucion_left" value="<?php echo !isset($data->institucion_left)?"10":$data->institucion_left;?>" >
                                        
                                        <input type="hidden" name="institucion_w" id="institucion_w" value="<?php echo !isset($data->institucion_w)?"200":$data->institucion_w;?>" >
                                        <input type="hidden" name="institucion_h" id="institucion_h" value="<?php echo !isset($data->institucion_h)?"20":$data->institucion_h;?>" >
                                        
                                        <!---------------------------------------------------INSTITUCION----------------------------------------------
                                        Institucion Reverso-->
                                        
                                        <input type="hidden" name="institucionback_top" id="institucionback_top" value="<?php echo !isset($data->institucionback_top)?"120":$data->institucionback_top;?>" >                                        
                                        <input type="hidden" name="institucionback_left" id="institucionback_left" value="<?php echo !isset($data->institucionback_left)?"10":$data->institucionback_left;?>" >
                                        
                                        <input type="hidden" name="institucionback_w" id="institucionback_w" value="<?php echo !isset($data->institucionback_w)?"200":$data->institucionback_w;?>" >
                                        <input type="hidden" name="institucionback_h" id="institucionback_h" value="<?php echo !isset($data->institucionback_h)?"20":$data->institucionback_h;?>" >
                                        
                                        <!---------------------------------------------------NOMBRE APELLIDO-------------------------------------------
                                        Nombre Apellido-->
                                        
                                        <input type="hidden" name="nombreapellido_top" id="nombreapellido_top" value="<?php echo !isset($data->nombreapellido_top)?"150":$data->nombreapellido_top;?>" >
                                        <input type="hidden" name="nombreapellido_left" id="nombreapellido_left" value="<?php echo !isset($data->nombreapellido_left)?"10":$data->nombreapellido_left;?>" >
                                        
                                        <input type="hidden" name="nombreapellido_w" id="nombreapellido_w" value="<?php echo !isset($data->nombreapellido_w)?"223":$data->nombreapellido_w;?>" >
                                        <input type="hidden" name="nombreapellido_h" id="nombreapellido_h" value="<?php echo !isset($data->nombreapellido_h)?"20":$data->nombreapellido_h;?>" >
                                        

                                        <!---------------------------------------------------CEDULA---------------------------------------------------
                                        Cedula-->
                                        
                                        <input type="hidden" name="cedula_top" id="cedula_top" value="<?php echo !isset($data->cedula_top)?"180":$data->cedula_top;?>" >
                                        <input type="hidden" name="cedula_left" id="cedula_left" value="<?php echo !isset($data->cedula_left)?"10":$data->cedula_left;?>" >
                                        
                                        <input type="hidden" name="cedula_w" id="cedula_w" value="<?php echo !isset($data->cedula_w)?"223":$data->cedula_w;?>" >
                                        <input type="hidden" name="cedula_h" id="cedula_h" value="<?php echo !isset($data->cedula_h)?"20":$data->cedula_h;?>" >
                                        

                                        <!---------------------------------------------------MAX SIZE IMG-------------------------------------------
                                        MAX SIZE IMG-->
                                        
                                        <input type="hidden" name="maxWidthTemplate" id="maxWidthTemplate" >
                                        <input type="hidden" name="maxHeightTemplate" id="maxHeightTemplate" >

                                        <!---------------------------------------------------DEPARTAMENTO-------------------------------------------
                                        Departamento-->
                                        
                                        <input type="hidden" name="departamento_top" id="departamento_top" value="210" >
                                        <input type="hidden" name="departamento_left" id="departamento_left" value="10" >
                                        
                                        <input type="hidden" name="departamento_w" id="departamento_w" value="300" >
                                        <input type="hidden" name="departamento_h" id="departamento_h" value="20" >
                                        
                                        <!---------------------------------------------------CARGO-------------------------------------------
                                        Cargo-->
                                        
                                        <input type="hidden" name="cargo_top" id="cargo_top" value="240" >
                                        <input type="hidden" name="cargo_left" id="cargo_left" value="10" >
                                        
                                        <input type="hidden" name="cargo_w" id="cargo_w" value="300" >
                                        <input type="hidden" name="cargo_h" id="cargo_h" value="20" >

                                        <!--------------------------------------------------BARCODE---------------------------------------------------
                                        BarCode-->
                                        
                                        <input type="hidden" name="barcode_top" id="barcode_top" value="<?php echo !isset($data->barcode_top)?"270":$data->barcode_top;?>" >
                                        <input type="hidden" name="barcode_left" id="barcode_left" value="<?php echo !isset($data->barcode_left)?"10":$data->barcode_left;?>" >
                                        
                                        <input type="hidden" name="barcode_w" id="barcode_w" value="<?php echo !isset($data->barcode_w)?"200":$data->barcode_w;?>" >
                                        <input type="hidden" name="barcode_h" id="barcode_h" value="<?php echo !isset($data->barcode_h)?"20":$data->barcode_h;?>" >

                                        <!--------------------------------------------------FECHA---------------------------------------------------
                                        Fecha-->
                                        
                                        <input type="hidden" name="fecha_top" id="fecha_top" value="<?php echo !isset($data->fecha_top)?"295":$data->fecha_top;?>" >
                                        <input type="hidden" name="fecha_left" id="fecha_left" value="<?php echo !isset($data->fecha_left)?"10":$data->fecha_left;?>" >
                                        
                                        <input type="hidden" name="fecha_w" id="fecha_w" value="<?php echo !isset($data->fecha_w)?"100":$data->fecha_w;?>" >
                                        <input type="hidden" name="fecha_h" id="fecha_h" value="<?php echo !isset($data->fecha_h)?"20":$data->fecha_h;?>" >

                                        <!---------------------------------------------------VISITANTE-------------------------------------------
                                        Visitante-->
                                        
                                        <input type="hidden" name="visitante_top" id="visitante_top" value="<?php echo !isset($data->visitante_top)?"150":$data->visitante_top;?>" >
                                        <input type="hidden" name="visitante_left" id="visitante_left" value="<?php echo !isset($data->visitante_left)?"10":$data->visitante_left;?>" >
                                        
                                        <input type="hidden" name="visitante_w" id="visitante_w" value="<?php echo !isset($data->visitante_w)?"223":$data->visitante_w;?>" >
                                        <input type="hidden" name="visitante_h" id="visitante_h" value="<?php echo !isset($data->visitante_h)?"40":$data->visitante_h;?>" >

                                        <!---------------------------------------------------VISITANTE NRO-------------------------------------------
                                        Visitante Nro -->

                                        <input type="hidden" name="visitantenro_top" id="visitantenro_top" value="<?php echo !isset($data->visitantenro_top)?"220":$data->visitantenro_top;?>" >
                                        <input type="hidden" name="visitantenro_left" id="visitantenro_left" value="<?php echo !isset($data->visitantenro_left)?"190":$data->visitantenro_left;?>" >

                                        <input type="hidden" name="visitantenro_w" id="visitantenro_w" value="<?php echo !isset($data->visitantenro_w)?"40":$data->visitantenro_w;?>" >
                                        <input type="hidden" name="visitantenro_h" id="visitantenro_h" value="<?php echo !isset($data->visitantenro_h)?"20":$data->visitantenro_h;?>" >
                                        
                                        <?php
                                        if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                            echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Plantilla\" id=\"botonSubmit\" />";
                                        }
                                        ?>
                                    </div>
                                </td>
                            </tr>                        
                        </table>
                    </div>
                    
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>