<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CarPlantilla {
    
    private $idplantilla = null;
    private $plantilla = null;
    private $orientacion = null;
    private $archivo_plantilla = null;
    private $archivoback_plantilla = null;
    private $logo_chk = null;
    private $archivo_logo = null;
    private $foto_top = null;
    private $foto_left = null;
    private $foto_w = null;
    private $foto_h = null;
    private $barcode_chk = null;
    private $barcode_front = null;
    private $barcode_back = null;
    private $barcode_top = null;
    private $barcode_left = null;
    private $barcode_w = null;
    private $barcode_h = null;
    private $logo_top = null;
    private $logo_left = null;
    private $logo_w = null;
    private $logo_h = null;
    private $logoback_chk = null;
    private $archivoback_logo = null;
    private $logoback_top = null;
    private $logoback_left = null;
    private $logoback_w = null;
    private $logoback_h = null;
    private $institucion_chk = null;
    private $institucion_nombre = null;
    private $institucion_top = null;
    private $institucion_left = null;
    private $institucion_w = null;
    private $institucion_h = null;
    private $institucion_color = null;
    private $institucion_fuentetamano = null;
    private $institucion_fuenteletra = null;
    private $institucion_bgcolor_chk = null;
    private $institucion_bgcolor = null;
    private $institucion_fuentealign = null;
    private $institucion_fuentevalign = null;
    private $institucionback_chk = null;
    private $institucionback_top = null;
    private $institucionback_left = null;
    private $institucionback_w = null;
    private $institucionback_h = null;
    private $institucionback_color = null;
    private $institucionback_fuentetamano = null;
    private $institucionback_fuenteletra = null;
    private $institucionback_bgcolor_chk = null;
    private $institucionback_bgcolor = null;
    private $institucionback_fuentealign = null;
    private $institucionback_fuentevalign = null;
    private $nombreapellido_top = null;
    private $nombreapellido_left = null;
    private $nombreapellido_w = null;
    private $nombreapellido_h = null;
    private $nombreapellido_color = null;
    private $nombreapellido_fuentetamano = null;
    private $nombreapellido_fuenteletra = null;
    private $nombreapellido_bgcolor_chk = null;
    private $nombreapellido_bgcolor = null;
    private $nombreapellido_fuentealign = null;
    private $nombreapellido_fuentevalign = null;
    private $cedula_top = null;
    private $cedula_left = null;
    private $cedula_w = null;
    private $cedula_h = null;
    private $cedula_color = null;
    private $cedula_fuentetamano = null;
    private $cedula_fuenteletra = null;
    private $cedula_bgcolor_chk = null;
    private $cedula_bgcolor = null;
    private $cedula_fuentealign = null;
    private $cedula_fuentevalign = null;
    private $fecha_chk = null;
    private $fecha_top = null;
    private $fecha_left = null;
    private $fecha_w = null;
    private $fecha_h = null;
    private $fecha_color = null;
    private $fecha_fuentetamano = null;
    private $fecha_fuenteletra = null;
    private $fecha_bgcolor_chk = null;
    private $fecha_bgcolor = null;
    private $fecha_fuentealign = null;
    private $fecha_fuentevalign = null;
    private $visitante_top = null;
    private $visitante_left = null;
    private $visitante_w = null;
    private $visitante_h = null;
    private $visitante_color = null;
    private $visitante_fuentetamano = null;
    private $visitante_fuenteletra = null;
    private $visitante_bgcolor_chk = null;
    private $visitante_bgcolor = null;
    private $visitante_fuentealign = null;
    private $visitante_fuentevalign = null;
    private $visitantenro_top = null;
    private $visitantenro_left = null;
    private $visitantenro_w = null;
    private $visitantenro_h = null;
    private $visitantenro_color = null;
    private $visitantenro_fuentetamano = null;
    private $visitantenro_fuenteletra = null;
    private $visitantenro_bgcolor_chk = null;
    private $visitantenro_bgcolor = null;
    private $visitantenro_fuentealign = null;
    private $visitantenro_fuentevalign = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdplantilla($idplantilla){
        $this->idplantilla = $idplantilla;
    }
    public function getIdplantilla(){
        return $this->idplantilla;
    }
    public function setPlantilla($plantilla){
        $this->plantilla = $plantilla;
    }
    public function getPlantilla(){
        return $this->plantilla;
    }
    public function setOrientacion($orientacion){
        $this->orientacion = $orientacion;
    }
    public function getOrientacion(){
        return $this->orientacion;
    }
    public function setArchivo_plantilla($archivo_plantilla){
        $this->archivo_plantilla = $archivo_plantilla;
    }
    public function getArchivo_plantilla(){
        return $this->archivo_plantilla;
    }
    public function setArchivoback_plantilla($archivoback_plantilla){
        $this->archivoback_plantilla = $archivoback_plantilla;
    }
    public function getArchivoback_plantilla(){
        return $this->archivoback_plantilla;
    }
    public function setLogo_chk($logo_chk){
        $this->logo_chk = $logo_chk;
    }
    public function getLogo_chk(){
        return $this->logo_chk;
    }
    public function setArchivo_logo($archivo_logo){
        $this->archivo_logo = $archivo_logo;
    }
    public function getArchivo_logo(){
        return $this->archivo_logo;
    }
    public function setFoto_top($foto_top){
        $this->foto_top = $foto_top;
    }
    public function getFoto_top(){
        return $this->foto_top;
    }
    public function setFoto_left($foto_left){
        $this->foto_left = $foto_left;
    }
    public function getFoto_left(){
        return $this->foto_left;
    }
    public function setFoto_w($foto_w){
        $this->foto_w = $foto_w;
    }
    public function getFoto_w(){
        return $this->foto_w;
    }
    public function setFoto_h($foto_h){
        $this->foto_h = $foto_h;
    }
    public function getFoto_h(){
        return $this->foto_h;
    }
    public function setBarcode_chk($barcode_chk){
        $this->barcode_chk = $barcode_chk;
    }
    public function getBarcode_chk(){
        return $this->barcode_chk;
    }
    public function setBarcode_front($barcode_front){
        $this->barcode_front = $barcode_front;
    }
    public function getBarcode_front(){
        return $this->barcode_front;
    }
    public function setBarcode_back($barcode_back){
        $this->barcode_back = $barcode_back;
    }
    public function getBarcode_back(){
        return $this->barcode_back;
    }
    public function setBarcode_top($barcode_top){
        $this->barcode_top = $barcode_top;
    }
    public function getBarcode_top(){
        return $this->barcode_top;
    }
    public function setBarcode_left($barcode_left){
        $this->barcode_left = $barcode_left;
    }
    public function getBarcode_left(){
        return $this->barcode_left;
    }
    public function setBarcode_w($barcode_w){
        $this->barcode_w = $barcode_w;
    }
    public function getBarcode_w(){
        return $this->barcode_w;
    }
    public function setBarcode_h($barcode_h){
        $this->barcode_h = $barcode_h;
    }
    public function getBarcode_h(){
        return $this->barcode_h;
    }
    public function setLogo_top($logo_top){
        $this->logo_top = $logo_top;
    }
    public function getLogo_top(){
        return $this->logo_top;
    }
    public function setLogo_left($logo_left){
        $this->logo_left = $logo_left;
    }
    public function getLogo_left(){
        return $this->logo_left;
    }
    public function setLogo_w($logo_w){
        $this->logo_w = $logo_w;
    }
    public function getLogo_w(){
        return $this->logo_w;
    }
    public function setLogo_h($logo_h){
        $this->logo_h = $logo_h;
    }
    public function getLogo_h(){
        return $this->logo_h;
    }
    public function setLogoback_chk($logoback_chk){
        $this->logoback_chk = $logoback_chk;
    }
    public function getLogoback_chk(){
        return $this->logoback_chk;
    }
    public function setArchivoback_logo($archivoback_logo){
        $this->archivoback_logo = $archivoback_logo;
    }
    public function getArchivoback_logo(){
        return $this->archivoback_logo;
    }
    public function setLogoback_top($logoback_top){
        $this->logoback_top = $logoback_top;
    }
    public function getLogoback_top(){
        return $this->logoback_top;
    }
    public function setLogoback_left($logoback_left){
        $this->logoback_left = $logoback_left;
    }
    public function getLogoback_left(){
        return $this->logoback_left;
    }
    public function setLogoback_w($logoback_w){
        $this->logoback_w = $logoback_w;
    }
    public function getLogoback_w(){
        return $this->logoback_w;
    }
    public function setLogoback_h($logoback_h){
        $this->logoback_h = $logoback_h;
    }
    public function getLogoback_h(){
        return $this->logoback_h;
    }
    public function setInstitucion_chk($institucion_chk){
        $this->institucion_chk = $institucion_chk;
    }
    public function getInstitucion_chk(){
        return $this->institucion_chk;
    }
    public function setInstitucion_nombre($institucion_nombre){
        $this->institucion_nombre = $institucion_nombre;
    }
    public function getInstitucion_nombre(){
        return $this->institucion_nombre;
    }
    public function setInstitucion_top($institucion_top){
        $this->institucion_top = $institucion_top;
    }
    public function getInstitucion_top(){
        return $this->institucion_top;
    }
    public function setInstitucion_left($institucion_left){
        $this->institucion_left = $institucion_left;
    }
    public function getInstitucion_left(){
        return $this->institucion_left;
    }
    public function setInstitucion_w($institucion_w){
        $this->institucion_w = $institucion_w;
    }
    public function getInstitucion_w(){
        return $this->institucion_w;
    }
    public function setInstitucion_h($institucion_h){
        $this->institucion_h = $institucion_h;
    }
    public function getInstitucion_h(){
        return $this->institucion_h;
    }
    public function setInstitucion_color($institucion_color){
        $this->institucion_color = $institucion_color;
    }
    public function getInstitucion_color(){
        return $this->institucion_color;
    }
    public function setInstitucion_fuentetamano($institucion_fuentetamano){
        $this->institucion_fuentetamano = $institucion_fuentetamano;
    }
    public function getInstitucion_fuentetamano(){
        return $this->institucion_fuentetamano;
    }
    public function setInstitucion_fuenteletra($institucion_fuenteletra){
        $this->institucion_fuenteletra = $institucion_fuenteletra;
    }
    public function getInstitucion_fuenteletra(){
        return $this->institucion_fuenteletra;
    }
    public function setInstitucion_bgcolor_chk($institucion_bgcolor_chk){
        $this->institucion_bgcolor_chk = $institucion_bgcolor_chk;
    }
    public function getInstitucion_bgcolor_chk(){
        return $this->institucion_bgcolor_chk;
    }
    public function setInstitucion_bgcolor($institucion_bgcolor){
        $this->institucion_bgcolor = $institucion_bgcolor;
    }
    public function getInstitucion_bgcolor(){
        return $this->institucion_bgcolor;
    }
    public function setInstitucion_fuentealign($institucion_fuentealign){
        $this->institucion_fuentealign = $institucion_fuentealign;
    }
    public function getInstitucion_fuentealign(){
        return $this->institucion_fuentealign;
    }
    public function setInstitucion_fuentevalign($institucion_fuentevalign){
        $this->institucion_fuentevalign = $institucion_fuentevalign;
    }
    public function getInstitucion_fuentevalign(){
        return $this->institucion_fuentevalign;
    }
    public function setInstitucionback_chk($institucionback_chk){
        $this->institucionback_chk = $institucionback_chk;
    }
    public function getInstitucionback_chk(){
        return $this->institucionback_chk;
    }
    public function setInstitucionback_top($institucionback_top){
        $this->institucionback_top = $institucionback_top;
    }
    public function getInstitucionback_top(){
        return $this->institucionback_top;
    }
    public function setInstitucionback_left($institucionback_left){
        $this->institucionback_left = $institucionback_left;
    }
    public function getInstitucionback_left(){
        return $this->institucionback_left;
    }
    public function setInstitucionback_w($institucionback_w){
        $this->institucionback_w = $institucionback_w;
    }
    public function getInstitucionback_w(){
        return $this->institucionback_w;
    }
    public function setInstitucionback_h($institucionback_h){
        $this->institucionback_h = $institucionback_h;
    }
    public function getInstitucionback_h(){
        return $this->institucionback_h;
    }
    public function setInstitucionback_color($institucionback_color){
        $this->institucionback_color = $institucionback_color;
    }
    public function getInstitucionback_color(){
        return $this->institucionback_color;
    }
    public function setInstitucionback_fuentetamano($institucionback_fuentetamano){
        $this->institucionback_fuentetamano = $institucionback_fuentetamano;
    }
    public function getInstitucionback_fuentetamano(){
        return $this->institucionback_fuentetamano;
    }
    public function setInstitucionback_fuenteletra($institucionback_fuenteletra){
        $this->institucionback_fuenteletra = $institucionback_fuenteletra;
    }
    public function getInstitucionback_fuenteletra(){
        return $this->institucionback_fuenteletra;
    }
    public function setInstitucionback_bgcolor_chk($institucionback_bgcolor_chk){
        $this->institucionback_bgcolor_chk = $institucionback_bgcolor_chk;
    }
    public function getInstitucionback_bgcolor_chk(){
        return $this->institucionback_bgcolor_chk;
    }
    public function setInstitucionback_bgcolor($institucionback_bgcolor){
        $this->institucionback_bgcolor = $institucionback_bgcolor;
    }
    public function getInstitucionback_bgcolor(){
        return $this->institucionback_bgcolor;
    }
    public function setInstitucionback_fuentealign($institucionback_fuentealign){
        $this->institucionback_fuentealign = $institucionback_fuentealign;
    }
    public function getInstitucionback_fuentealign(){
        return $this->institucionback_fuentealign;
    }
    public function setInstitucionback_fuentevalign($institucionback_fuentevalign){
        $this->institucionback_fuentevalign = $institucionback_fuentevalign;
    }
    public function getInstitucionback_fuentevalign(){
        return $this->institucionback_fuentevalign;
    }
    public function setNombreapellido_top($nombreapellido_top){
        $this->nombreapellido_top = $nombreapellido_top;
    }
    public function getNombreapellido_top(){
        return $this->nombreapellido_top;
    }
    public function setNombreapellido_left($nombreapellido_left){
        $this->nombreapellido_left = $nombreapellido_left;
    }
    public function getNombreapellido_left(){
        return $this->nombreapellido_left;
    }
    public function setNombreapellido_w($nombreapellido_w){
        $this->nombreapellido_w = $nombreapellido_w;
    }
    public function getNombreapellido_w(){
        return $this->nombreapellido_w;
    }
    public function setNombreapellido_h($nombreapellido_h){
        $this->nombreapellido_h = $nombreapellido_h;
    }
    public function getNombreapellido_h(){
        return $this->nombreapellido_h;
    }
    public function setNombreapellido_color($nombreapellido_color){
        $this->nombreapellido_color = $nombreapellido_color;
    }
    public function getNombreapellido_color(){
        return $this->nombreapellido_color;
    }
    public function setNombreapellido_fuentetamano($nombreapellido_fuentetamano){
        $this->nombreapellido_fuentetamano = $nombreapellido_fuentetamano;
    }
    public function getNombreapellido_fuentetamano(){
        return $this->nombreapellido_fuentetamano;
    }
    public function setNombreapellido_fuenteletra($nombreapellido_fuenteletra){
        $this->nombreapellido_fuenteletra = $nombreapellido_fuenteletra;
    }
    public function getNombreapellido_fuenteletra(){
        return $this->nombreapellido_fuenteletra;
    }
    public function setNombreapellido_bgcolor_chk($nombreapellido_bgcolor_chk){
        $this->nombreapellido_bgcolor_chk = $nombreapellido_bgcolor_chk;
    }
    public function getNombreapellido_bgcolor_chk(){
        return $this->nombreapellido_bgcolor_chk;
    }
    public function setNombreapellido_bgcolor($nombreapellido_bgcolor){
        $this->nombreapellido_bgcolor = $nombreapellido_bgcolor;
    }
    public function getNombreapellido_bgcolor(){
        return $this->nombreapellido_bgcolor;
    }
    public function setNombreapellido_fuentealign($nombreapellido_fuentealign){
        $this->nombreapellido_fuentealign = $nombreapellido_fuentealign;
    }
    public function getNombreapellido_fuentealign(){
        return $this->nombreapellido_fuentealign;
    }
    public function setNombreapellido_fuentevalign($nombreapellido_fuentevalign){
        $this->nombreapellido_fuentevalign = $nombreapellido_fuentevalign;
    }
    public function getNombreapellido_fuentevalign(){
        return $this->nombreapellido_fuentevalign;
    }
    public function setCedula_top($cedula_top){
        $this->cedula_top = $cedula_top;
    }
    public function getCedula_top(){
        return $this->cedula_top;
    }
    public function setCedula_left($cedula_left){
        $this->cedula_left = $cedula_left;
    }
    public function getCedula_left(){
        return $this->cedula_left;
    }
    public function setCedula_w($cedula_w){
        $this->cedula_w = $cedula_w;
    }
    public function getCedula_w(){
        return $this->cedula_w;
    }
    public function setCedula_h($cedula_h){
        $this->cedula_h = $cedula_h;
    }
    public function getCedula_h(){
        return $this->cedula_h;
    }
    public function setCedula_color($cedula_color){
        $this->cedula_color = $cedula_color;
    }
    public function getCedula_color(){
        return $this->cedula_color;
    }
    public function setCedula_fuentetamano($cedula_fuentetamano){
        $this->cedula_fuentetamano = $cedula_fuentetamano;
    }
    public function getCedula_fuentetamano(){
        return $this->cedula_fuentetamano;
    }
    public function setCedula_fuenteletra($cedula_fuenteletra){
        $this->cedula_fuenteletra = $cedula_fuenteletra;
    }
    public function getCedula_fuenteletra(){
        return $this->cedula_fuenteletra;
    }
    public function setCedula_bgcolor_chk($cedula_bgcolor_chk){
        $this->cedula_bgcolor_chk = $cedula_bgcolor_chk;
    }
    public function getCedula_bgcolor_chk(){
        return $this->cedula_bgcolor_chk;
    }
    public function setCedula_bgcolor($cedula_bgcolor){
        $this->cedula_bgcolor = $cedula_bgcolor;
    }
    public function getCedula_bgcolor(){
        return $this->cedula_bgcolor;
    }
    public function setCedula_fuentealign($cedula_fuentealign){
        $this->cedula_fuentealign = $cedula_fuentealign;
    }
    public function getCedula_fuentealign(){
        return $this->cedula_fuentealign;
    }
    public function setCedula_fuentevalign($cedula_fuentevalign){
        $this->cedula_fuentevalign = $cedula_fuentevalign;
    }
    public function getCedula_fuentevalign(){
        return $this->cedula_fuentevalign;
    }
    public function setFecha_chk($fecha_chk){
        $this->fecha_chk = $fecha_chk;
    }
    public function getFecha_chk(){
        return $this->fecha_chk;
    }
    public function setFecha_top($fecha_top){
        $this->fecha_top = $fecha_top;
    }
    public function getFecha_top(){
        return $this->fecha_top;
    }
    public function setFecha_left($fecha_left){
        $this->fecha_left = $fecha_left;
    }
    public function getFecha_left(){
        return $this->fecha_left;
    }
    public function setFecha_w($fecha_w){
        $this->fecha_w = $fecha_w;
    }
    public function getFecha_w(){
        return $this->fecha_w;
    }
    public function setFecha_h($fecha_h){
        $this->fecha_h = $fecha_h;
    }
    public function getFecha_h(){
        return $this->fecha_h;
    }
    public function setFecha_color($fecha_color){
        $this->fecha_color = $fecha_color;
    }
    public function getFecha_color(){
        return $this->fecha_color;
    }
    public function setFecha_fuentetamano($fecha_fuentetamano){
        $this->fecha_fuentetamano = $fecha_fuentetamano;
    }
    public function getFecha_fuentetamano(){
        return $this->fecha_fuentetamano;
    }
    public function setFecha_fuenteletra($fecha_fuenteletra){
        $this->fecha_fuenteletra = $fecha_fuenteletra;
    }
    public function getFecha_fuenteletra(){
        return $this->fecha_fuenteletra;
    }
    public function setFecha_bgcolor_chk($fecha_bgcolor_chk){
        $this->fecha_bgcolor_chk = $fecha_bgcolor_chk;
    }
    public function getFecha_bgcolor_chk(){
        return $this->fecha_bgcolor_chk;
    }
    public function setFecha_bgcolor($fecha_bgcolor){
        $this->fecha_bgcolor = $fecha_bgcolor;
    }
    public function getFecha_bgcolor(){
        return $this->fecha_bgcolor;
    }
    public function setFecha_fuentealign($fecha_fuentealign){
        $this->fecha_fuentealign = $fecha_fuentealign;
    }
    public function getFecha_fuentealign(){
        return $this->fecha_fuentealign;
    }
    public function setFecha_fuentevalign($fecha_fuentevalign){
        $this->fecha_fuentevalign = $fecha_fuentevalign;
    }
    public function getFecha_fuentevalign(){
        return $this->fecha_fuentevalign;
    }
    public function setVisitante_top($visitante_top){
        $this->visitante_top = $visitante_top;
    }
    public function getVisitante_top(){
        return $this->visitante_top;
    }
    public function setVisitante_left($visitante_left){
        $this->visitante_left = $visitante_left;
    }
    public function getVisitante_left(){
        return $this->visitante_left;
    }
    public function setVisitante_w($visitante_w){
        $this->visitante_w = $visitante_w;
    }
    public function getVisitante_w(){
        return $this->visitante_w;
    }
    public function setVisitante_h($visitante_h){
        $this->visitante_h = $visitante_h;
    }
    public function getVisitante_h(){
        return $this->visitante_h;
    }
    public function setVisitante_color($visitante_color){
        $this->visitante_color = $visitante_color;
    }
    public function getVisitante_color(){
        return $this->visitante_color;
    }
    public function setVisitante_fuentetamano($visitante_fuentetamano){
        $this->visitante_fuentetamano = $visitante_fuentetamano;
    }
    public function getVisitante_fuentetamano(){
        return $this->visitante_fuentetamano;
    }
    public function setVisitante_fuenteletra($visitante_fuenteletra){
        $this->visitante_fuenteletra = $visitante_fuenteletra;
    }
    public function getVisitante_fuenteletra(){
        return $this->visitante_fuenteletra;
    }
    public function setVisitante_bgcolor_chk($visitante_bgcolor_chk){
        $this->visitante_bgcolor_chk = $visitante_bgcolor_chk;
    }
    public function getVisitante_bgcolor_chk(){
        return $this->visitante_bgcolor_chk;
    }
    public function setVisitante_bgcolor($visitante_bgcolor){
        $this->visitante_bgcolor = $visitante_bgcolor;
    }
    public function getVisitante_bgcolor(){
        return $this->visitante_bgcolor;
    }
    public function setVisitante_fuentealign($visitante_fuentealign){
        $this->visitante_fuentealign = $visitante_fuentealign;
    }
    public function getVisitante_fuentealign(){
        return $this->visitante_fuentealign;
    }
    public function setVisitante_fuentevalign($visitante_fuentevalign){
        $this->visitante_fuentevalign = $visitante_fuentevalign;
    }
    public function getVisitante_fuentevalign(){
        return $this->visitante_fuentevalign;
    }
    public function setVisitantenro_top($visitantenro_top){
        $this->visitantenro_top = $visitantenro_top;
    }
    public function getVisitantenro_top(){
        return $this->visitantenro_top;
    }
    public function setVisitantenro_left($visitantenro_left){
        $this->visitantenro_left = $visitantenro_left;
    }
    public function getVisitantenro_left(){
        return $this->visitantenro_left;
    }
    public function setVisitantenro_w($visitantenro_w){
        $this->visitantenro_w = $visitantenro_w;
    }
    public function getVisitantenro_w(){
        return $this->visitantenro_w;
    }
    public function setVisitantenro_h($visitantenro_h){
        $this->visitantenro_h = $visitantenro_h;
    }
    public function getVisitantenro_h(){
        return $this->visitantenro_h;
    }
    public function setVisitantenro_color($visitantenro_color){
        $this->visitantenro_color = $visitantenro_color;
    }
    public function getVisitantenro_color(){
        return $this->visitantenro_color;
    }
    public function setVisitantenro_fuentetamano($visitantenro_fuentetamano){
        $this->visitantenro_fuentetamano = $visitantenro_fuentetamano;
    }
    public function getVisitantenro_fuentetamano(){
        return $this->visitantenro_fuentetamano;
    }
    public function setVisitantenro_fuenteletra($visitantenro_fuenteletra){
        $this->visitantenro_fuenteletra = $visitantenro_fuenteletra;
    }
    public function getVisitantenro_fuenteletra(){
        return $this->visitantenro_fuenteletra;
    }
    public function setVisitantenro_bgcolor_chk($visitantenro_bgcolor_chk){
        $this->visitantenro_bgcolor_chk = $visitantenro_bgcolor_chk;
    }
    public function getVisitantenro_bgcolor_chk(){
        return $this->visitantenro_bgcolor_chk;
    }
    public function setVisitantenro_bgcolor($visitantenro_bgcolor){
        $this->visitantenro_bgcolor = $visitantenro_bgcolor;
    }
    public function getVisitantenro_bgcolor(){
        return $this->visitantenro_bgcolor;
    }
    public function setVisitantenro_fuentealign($visitantenro_fuentealign){
        $this->visitantenro_fuentealign = $visitantenro_fuentealign;
    }
    public function getVisitantenro_fuentealign(){
        return $this->visitantenro_fuentealign;
    }
    public function setVisitantenro_fuentevalign($visitantenro_fuentevalign){
        $this->visitantenro_fuentevalign = $visitantenro_fuentevalign;
    }
    public function getVisitantenro_fuentevalign(){
        return $this->visitantenro_fuentevalign;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $campos .= "idplantilla,";
            $valores .= "'".$this->idplantilla."',";
        }
        if(($this->plantilla !== null) && (trim($this->plantilla)!=='') ){
            $campos .= "plantilla,";
            $valores .= "'".$this->plantilla."',";
        }
        if(($this->orientacion !== null) && (trim($this->orientacion)!=='') ){
            $campos .= "orientacion,";
            $valores .= "'".$this->orientacion."',";
        }
        if(($this->archivo_plantilla !== null) && (trim($this->archivo_plantilla)!=='') ){
            $campos .= "archivo_plantilla,";
            $valores .= "'".$this->archivo_plantilla."',";
        }
        if(($this->archivoback_plantilla !== null) && (trim($this->archivoback_plantilla)!=='') ){
            $campos .= "archivoback_plantilla,";
            $valores .= "'".$this->archivoback_plantilla."',";
        }
        if(($this->logo_chk !== null) && (trim($this->logo_chk)!=='') ){
            $campos .= "logo_chk,";
            $valores .= "'".$this->logo_chk."',";
        }
        if(($this->archivo_logo !== null) && (trim($this->archivo_logo)!=='') ){
            $campos .= "archivo_logo,";
            $valores .= "'".$this->archivo_logo."',";
        }
        if(($this->foto_top !== null) && (trim($this->foto_top)!=='') ){
            $campos .= "foto_top,";
            $valores .= "'".$this->foto_top."',";
        }
        if(($this->foto_left !== null) && (trim($this->foto_left)!=='') ){
            $campos .= "foto_left,";
            $valores .= "'".$this->foto_left."',";
        }
        if(($this->foto_w !== null) && (trim($this->foto_w)!=='') ){
            $campos .= "foto_w,";
            $valores .= "'".$this->foto_w."',";
        }
        if(($this->foto_h !== null) && (trim($this->foto_h)!=='') ){
            $campos .= "foto_h,";
            $valores .= "'".$this->foto_h."',";
        }
        if(($this->barcode_chk !== null) && (trim($this->barcode_chk)!=='') ){
            $campos .= "barcode_chk,";
            $valores .= "'".$this->barcode_chk."',";
        }
        if(($this->barcode_front !== null) && (trim($this->barcode_front)!=='') ){
            $campos .= "barcode_front,";
            $valores .= "'".$this->barcode_front."',";
        }
        if(($this->barcode_back !== null) && (trim($this->barcode_back)!=='') ){
            $campos .= "barcode_back,";
            $valores .= "'".$this->barcode_back."',";
        }
        if(($this->barcode_top !== null) && (trim($this->barcode_top)!=='') ){
            $campos .= "barcode_top,";
            $valores .= "'".$this->barcode_top."',";
        }
        if(($this->barcode_left !== null) && (trim($this->barcode_left)!=='') ){
            $campos .= "barcode_left,";
            $valores .= "'".$this->barcode_left."',";
        }
        if(($this->barcode_w !== null) && (trim($this->barcode_w)!=='') ){
            $campos .= "barcode_w,";
            $valores .= "'".$this->barcode_w."',";
        }
        if(($this->barcode_h !== null) && (trim($this->barcode_h)!=='') ){
            $campos .= "barcode_h,";
            $valores .= "'".$this->barcode_h."',";
        }
        if(($this->logo_top !== null) && (trim($this->logo_top)!=='') ){
            $campos .= "logo_top,";
            $valores .= "'".$this->logo_top."',";
        }
        if(($this->logo_left !== null) && (trim($this->logo_left)!=='') ){
            $campos .= "logo_left,";
            $valores .= "'".$this->logo_left."',";
        }
        if(($this->logo_w !== null) && (trim($this->logo_w)!=='') ){
            $campos .= "logo_w,";
            $valores .= "'".$this->logo_w."',";
        }
        if(($this->logo_h !== null) && (trim($this->logo_h)!=='') ){
            $campos .= "logo_h,";
            $valores .= "'".$this->logo_h."',";
        }
        if(($this->logoback_chk !== null) && (trim($this->logoback_chk)!=='') ){
            $campos .= "logoback_chk,";
            $valores .= "'".$this->logoback_chk."',";
        }
        if(($this->archivoback_logo !== null) && (trim($this->archivoback_logo)!=='') ){
            $campos .= "archivoback_logo,";
            $valores .= "'".$this->archivoback_logo."',";
        }
        if(($this->logoback_top !== null) && (trim($this->logoback_top)!=='') ){
            $campos .= "logoback_top,";
            $valores .= "'".$this->logoback_top."',";
        }
        if(($this->logoback_left !== null) && (trim($this->logoback_left)!=='') ){
            $campos .= "logoback_left,";
            $valores .= "'".$this->logoback_left."',";
        }
        if(($this->logoback_w !== null) && (trim($this->logoback_w)!=='') ){
            $campos .= "logoback_w,";
            $valores .= "'".$this->logoback_w."',";
        }
        if(($this->logoback_h !== null) && (trim($this->logoback_h)!=='') ){
            $campos .= "logoback_h,";
            $valores .= "'".$this->logoback_h."',";
        }
        if(($this->institucion_chk !== null) && (trim($this->institucion_chk)!=='') ){
            $campos .= "institucion_chk,";
            $valores .= "'".$this->institucion_chk."',";
        }
        if(($this->institucion_nombre !== null) && (trim($this->institucion_nombre)!=='') ){
            $campos .= "institucion_nombre,";
            $valores .= "'".$this->institucion_nombre."',";
        }
        if(($this->institucion_top !== null) && (trim($this->institucion_top)!=='') ){
            $campos .= "institucion_top,";
            $valores .= "'".$this->institucion_top."',";
        }
        if(($this->institucion_left !== null) && (trim($this->institucion_left)!=='') ){
            $campos .= "institucion_left,";
            $valores .= "'".$this->institucion_left."',";
        }
        if(($this->institucion_w !== null) && (trim($this->institucion_w)!=='') ){
            $campos .= "institucion_w,";
            $valores .= "'".$this->institucion_w."',";
        }
        if(($this->institucion_h !== null) && (trim($this->institucion_h)!=='') ){
            $campos .= "institucion_h,";
            $valores .= "'".$this->institucion_h."',";
        }
        if(($this->institucion_color !== null) && (trim($this->institucion_color)!=='') ){
            $campos .= "institucion_color,";
            $valores .= "'".$this->institucion_color."',";
        }
        if(($this->institucion_fuentetamano !== null) && (trim($this->institucion_fuentetamano)!=='') ){
            $campos .= "institucion_fuentetamano,";
            $valores .= "'".$this->institucion_fuentetamano."',";
        }
        if(($this->institucion_fuenteletra !== null) && (trim($this->institucion_fuenteletra)!=='') ){
            $campos .= "institucion_fuenteletra,";
            $valores .= "'".$this->institucion_fuenteletra."',";
        }
        if(($this->institucion_bgcolor_chk !== null) && (trim($this->institucion_bgcolor_chk)!=='') ){
            $campos .= "institucion_bgcolor_chk,";
            $valores .= "'".$this->institucion_bgcolor_chk."',";
        }
        if(($this->institucion_bgcolor !== null) && (trim($this->institucion_bgcolor)!=='') ){
            $campos .= "institucion_bgcolor,";
            $valores .= "'".$this->institucion_bgcolor."',";
        }
        if(($this->institucion_fuentealign !== null) && (trim($this->institucion_fuentealign)!=='') ){
            $campos .= "institucion_fuentealign,";
            $valores .= "'".$this->institucion_fuentealign."',";
        }
        if(($this->institucion_fuentevalign !== null) && (trim($this->institucion_fuentevalign)!=='') ){
            $campos .= "institucion_fuentevalign,";
            $valores .= "'".$this->institucion_fuentevalign."',";
        }
        if(($this->institucionback_chk !== null) && (trim($this->institucionback_chk)!=='') ){
            $campos .= "institucionback_chk,";
            $valores .= "'".$this->institucionback_chk."',";
        }
        if(($this->institucionback_top !== null) && (trim($this->institucionback_top)!=='') ){
            $campos .= "institucionback_top,";
            $valores .= "'".$this->institucionback_top."',";
        }
        if(($this->institucionback_left !== null) && (trim($this->institucionback_left)!=='') ){
            $campos .= "institucionback_left,";
            $valores .= "'".$this->institucionback_left."',";
        }
        if(($this->institucionback_w !== null) && (trim($this->institucionback_w)!=='') ){
            $campos .= "institucionback_w,";
            $valores .= "'".$this->institucionback_w."',";
        }
        if(($this->institucionback_h !== null) && (trim($this->institucionback_h)!=='') ){
            $campos .= "institucionback_h,";
            $valores .= "'".$this->institucionback_h."',";
        }
        if(($this->institucionback_color !== null) && (trim($this->institucionback_color)!=='') ){
            $campos .= "institucionback_color,";
            $valores .= "'".$this->institucionback_color."',";
        }
        if(($this->institucionback_fuentetamano !== null) && (trim($this->institucionback_fuentetamano)!=='') ){
            $campos .= "institucionback_fuentetamano,";
            $valores .= "'".$this->institucionback_fuentetamano."',";
        }
        if(($this->institucionback_fuenteletra !== null) && (trim($this->institucionback_fuenteletra)!=='') ){
            $campos .= "institucionback_fuenteletra,";
            $valores .= "'".$this->institucionback_fuenteletra."',";
        }
        if(($this->institucionback_bgcolor_chk !== null) && (trim($this->institucionback_bgcolor_chk)!=='') ){
            $campos .= "institucionback_bgcolor_chk,";
            $valores .= "'".$this->institucionback_bgcolor_chk."',";
        }
        if(($this->institucionback_bgcolor !== null) && (trim($this->institucionback_bgcolor)!=='') ){
            $campos .= "institucionback_bgcolor,";
            $valores .= "'".$this->institucionback_bgcolor."',";
        }
        if(($this->institucionback_fuentealign !== null) && (trim($this->institucionback_fuentealign)!=='') ){
            $campos .= "institucionback_fuentealign,";
            $valores .= "'".$this->institucionback_fuentealign."',";
        }
        if(($this->institucionback_fuentevalign !== null) && (trim($this->institucionback_fuentevalign)!=='') ){
            $campos .= "institucionback_fuentevalign,";
            $valores .= "'".$this->institucionback_fuentevalign."',";
        }
        if(($this->nombreapellido_top !== null) && (trim($this->nombreapellido_top)!=='') ){
            $campos .= "nombreapellido_top,";
            $valores .= "'".$this->nombreapellido_top."',";
        }
        if(($this->nombreapellido_left !== null) && (trim($this->nombreapellido_left)!=='') ){
            $campos .= "nombreapellido_left,";
            $valores .= "'".$this->nombreapellido_left."',";
        }
        if(($this->nombreapellido_w !== null) && (trim($this->nombreapellido_w)!=='') ){
            $campos .= "nombreapellido_w,";
            $valores .= "'".$this->nombreapellido_w."',";
        }
        if(($this->nombreapellido_h !== null) && (trim($this->nombreapellido_h)!=='') ){
            $campos .= "nombreapellido_h,";
            $valores .= "'".$this->nombreapellido_h."',";
        }
        if(($this->nombreapellido_color !== null) && (trim($this->nombreapellido_color)!=='') ){
            $campos .= "nombreapellido_color,";
            $valores .= "'".$this->nombreapellido_color."',";
        }
        if(($this->nombreapellido_fuentetamano !== null) && (trim($this->nombreapellido_fuentetamano)!=='') ){
            $campos .= "nombreapellido_fuentetamano,";
            $valores .= "'".$this->nombreapellido_fuentetamano."',";
        }
        if(($this->nombreapellido_fuenteletra !== null) && (trim($this->nombreapellido_fuenteletra)!=='') ){
            $campos .= "nombreapellido_fuenteletra,";
            $valores .= "'".$this->nombreapellido_fuenteletra."',";
        }
        if(($this->nombreapellido_bgcolor_chk !== null) && (trim($this->nombreapellido_bgcolor_chk)!=='') ){
            $campos .= "nombreapellido_bgcolor_chk,";
            $valores .= "'".$this->nombreapellido_bgcolor_chk."',";
        }
        if(($this->nombreapellido_bgcolor !== null) && (trim($this->nombreapellido_bgcolor)!=='') ){
            $campos .= "nombreapellido_bgcolor,";
            $valores .= "'".$this->nombreapellido_bgcolor."',";
        }
        if(($this->nombreapellido_fuentealign !== null) && (trim($this->nombreapellido_fuentealign)!=='') ){
            $campos .= "nombreapellido_fuentealign,";
            $valores .= "'".$this->nombreapellido_fuentealign."',";
        }
        if(($this->nombreapellido_fuentevalign !== null) && (trim($this->nombreapellido_fuentevalign)!=='') ){
            $campos .= "nombreapellido_fuentevalign,";
            $valores .= "'".$this->nombreapellido_fuentevalign."',";
        }
        if(($this->cedula_top !== null) && (trim($this->cedula_top)!=='') ){
            $campos .= "cedula_top,";
            $valores .= "'".$this->cedula_top."',";
        }
        if(($this->cedula_left !== null) && (trim($this->cedula_left)!=='') ){
            $campos .= "cedula_left,";
            $valores .= "'".$this->cedula_left."',";
        }
        if(($this->cedula_w !== null) && (trim($this->cedula_w)!=='') ){
            $campos .= "cedula_w,";
            $valores .= "'".$this->cedula_w."',";
        }
        if(($this->cedula_h !== null) && (trim($this->cedula_h)!=='') ){
            $campos .= "cedula_h,";
            $valores .= "'".$this->cedula_h."',";
        }
        if(($this->cedula_color !== null) && (trim($this->cedula_color)!=='') ){
            $campos .= "cedula_color,";
            $valores .= "'".$this->cedula_color."',";
        }
        if(($this->cedula_fuentetamano !== null) && (trim($this->cedula_fuentetamano)!=='') ){
            $campos .= "cedula_fuentetamano,";
            $valores .= "'".$this->cedula_fuentetamano."',";
        }
        if(($this->cedula_fuenteletra !== null) && (trim($this->cedula_fuenteletra)!=='') ){
            $campos .= "cedula_fuenteletra,";
            $valores .= "'".$this->cedula_fuenteletra."',";
        }
        if(($this->cedula_bgcolor_chk !== null) && (trim($this->cedula_bgcolor_chk)!=='') ){
            $campos .= "cedula_bgcolor_chk,";
            $valores .= "'".$this->cedula_bgcolor_chk."',";
        }
        if(($this->cedula_bgcolor !== null) && (trim($this->cedula_bgcolor)!=='') ){
            $campos .= "cedula_bgcolor,";
            $valores .= "'".$this->cedula_bgcolor."',";
        }
        if(($this->cedula_fuentealign !== null) && (trim($this->cedula_fuentealign)!=='') ){
            $campos .= "cedula_fuentealign,";
            $valores .= "'".$this->cedula_fuentealign."',";
        }
        if(($this->cedula_fuentevalign !== null) && (trim($this->cedula_fuentevalign)!=='') ){
            $campos .= "cedula_fuentevalign,";
            $valores .= "'".$this->cedula_fuentevalign."',";
        }
        if(($this->fecha_chk !== null) && (trim($this->fecha_chk)!=='') ){
            $campos .= "fecha_chk,";
            $valores .= "'".$this->fecha_chk."',";
        }
        if(($this->fecha_top !== null) && (trim($this->fecha_top)!=='') ){
            $campos .= "fecha_top,";
            $valores .= "'".$this->fecha_top."',";
        }
        if(($this->fecha_left !== null) && (trim($this->fecha_left)!=='') ){
            $campos .= "fecha_left,";
            $valores .= "'".$this->fecha_left."',";
        }
        if(($this->fecha_w !== null) && (trim($this->fecha_w)!=='') ){
            $campos .= "fecha_w,";
            $valores .= "'".$this->fecha_w."',";
        }
        if(($this->fecha_h !== null) && (trim($this->fecha_h)!=='') ){
            $campos .= "fecha_h,";
            $valores .= "'".$this->fecha_h."',";
        }
        if(($this->fecha_color !== null) && (trim($this->fecha_color)!=='') ){
            $campos .= "fecha_color,";
            $valores .= "'".$this->fecha_color."',";
        }
        if(($this->fecha_fuentetamano !== null) && (trim($this->fecha_fuentetamano)!=='') ){
            $campos .= "fecha_fuentetamano,";
            $valores .= "'".$this->fecha_fuentetamano."',";
        }
        if(($this->fecha_fuenteletra !== null) && (trim($this->fecha_fuenteletra)!=='') ){
            $campos .= "fecha_fuenteletra,";
            $valores .= "'".$this->fecha_fuenteletra."',";
        }
        if(($this->fecha_bgcolor_chk !== null) && (trim($this->fecha_bgcolor_chk)!=='') ){
            $campos .= "fecha_bgcolor_chk,";
            $valores .= "'".$this->fecha_bgcolor_chk."',";
        }
        if(($this->fecha_bgcolor !== null) && (trim($this->fecha_bgcolor)!=='') ){
            $campos .= "fecha_bgcolor,";
            $valores .= "'".$this->fecha_bgcolor."',";
        }
        if(($this->fecha_fuentealign !== null) && (trim($this->fecha_fuentealign)!=='') ){
            $campos .= "fecha_fuentealign,";
            $valores .= "'".$this->fecha_fuentealign."',";
        }
        if(($this->fecha_fuentevalign !== null) && (trim($this->fecha_fuentevalign)!=='') ){
            $campos .= "fecha_fuentevalign,";
            $valores .= "'".$this->fecha_fuentevalign."',";
        }
        if(($this->visitante_top !== null) && (trim($this->visitante_top)!=='') ){
            $campos .= "visitante_top,";
            $valores .= "'".$this->visitante_top."',";
        }
        if(($this->visitante_left !== null) && (trim($this->visitante_left)!=='') ){
            $campos .= "visitante_left,";
            $valores .= "'".$this->visitante_left."',";
        }
        if(($this->visitante_w !== null) && (trim($this->visitante_w)!=='') ){
            $campos .= "visitante_w,";
            $valores .= "'".$this->visitante_w."',";
        }
        if(($this->visitante_h !== null) && (trim($this->visitante_h)!=='') ){
            $campos .= "visitante_h,";
            $valores .= "'".$this->visitante_h."',";
        }
        if(($this->visitante_color !== null) && (trim($this->visitante_color)!=='') ){
            $campos .= "visitante_color,";
            $valores .= "'".$this->visitante_color."',";
        }
        if(($this->visitante_fuentetamano !== null) && (trim($this->visitante_fuentetamano)!=='') ){
            $campos .= "visitante_fuentetamano,";
            $valores .= "'".$this->visitante_fuentetamano."',";
        }
        if(($this->visitante_fuenteletra !== null) && (trim($this->visitante_fuenteletra)!=='') ){
            $campos .= "visitante_fuenteletra,";
            $valores .= "'".$this->visitante_fuenteletra."',";
        }
        if(($this->visitante_bgcolor_chk !== null) && (trim($this->visitante_bgcolor_chk)!=='') ){
            $campos .= "visitante_bgcolor_chk,";
            $valores .= "'".$this->visitante_bgcolor_chk."',";
        }
        if(($this->visitante_bgcolor !== null) && (trim($this->visitante_bgcolor)!=='') ){
            $campos .= "visitante_bgcolor,";
            $valores .= "'".$this->visitante_bgcolor."',";
        }
        if(($this->visitante_fuentealign !== null) && (trim($this->visitante_fuentealign)!=='') ){
            $campos .= "visitante_fuentealign,";
            $valores .= "'".$this->visitante_fuentealign."',";
        }
        if(($this->visitante_fuentevalign !== null) && (trim($this->visitante_fuentevalign)!=='') ){
            $campos .= "visitante_fuentevalign,";
            $valores .= "'".$this->visitante_fuentevalign."',";
        }
        if(($this->visitantenro_top !== null) && (trim($this->visitantenro_top)!=='') ){
            $campos .= "visitantenro_top,";
            $valores .= "'".$this->visitantenro_top."',";
        }
        if(($this->visitantenro_left !== null) && (trim($this->visitantenro_left)!=='') ){
            $campos .= "visitantenro_left,";
            $valores .= "'".$this->visitantenro_left."',";
        }
        if(($this->visitantenro_w !== null) && (trim($this->visitantenro_w)!=='') ){
            $campos .= "visitantenro_w,";
            $valores .= "'".$this->visitantenro_w."',";
        }
        if(($this->visitantenro_h !== null) && (trim($this->visitantenro_h)!=='') ){
            $campos .= "visitantenro_h,";
            $valores .= "'".$this->visitantenro_h."',";
        }
        if(($this->visitantenro_color !== null) && (trim($this->visitantenro_color)!=='') ){
            $campos .= "visitantenro_color,";
            $valores .= "'".$this->visitantenro_color."',";
        }
        if(($this->visitantenro_fuentetamano !== null) && (trim($this->visitantenro_fuentetamano)!=='') ){
            $campos .= "visitantenro_fuentetamano,";
            $valores .= "'".$this->visitantenro_fuentetamano."',";
        }
        if(($this->visitantenro_fuenteletra !== null) && (trim($this->visitantenro_fuenteletra)!=='') ){
            $campos .= "visitantenro_fuenteletra,";
            $valores .= "'".$this->visitantenro_fuenteletra."',";
        }
        if(($this->visitantenro_bgcolor_chk !== null) && (trim($this->visitantenro_bgcolor_chk)!=='') ){
            $campos .= "visitantenro_bgcolor_chk,";
            $valores .= "'".$this->visitantenro_bgcolor_chk."',";
        }
        if(($this->visitantenro_bgcolor !== null) && (trim($this->visitantenro_bgcolor)!=='') ){
            $campos .= "visitantenro_bgcolor,";
            $valores .= "'".$this->visitantenro_bgcolor."',";
        }
        if(($this->visitantenro_fuentealign !== null) && (trim($this->visitantenro_fuentealign)!=='') ){
            $campos .= "visitantenro_fuentealign,";
            $valores .= "'".$this->visitantenro_fuentealign."',";
        }
        if(($this->visitantenro_fuentevalign !== null) && (trim($this->visitantenro_fuentevalign)!=='') ){
            $campos .= "visitantenro_fuentevalign,";
            $valores .= "'".$this->visitantenro_fuentevalign."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO car_plantilla $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE car_plantilla SET ";
        
        if(($this->plantilla !== null) && (trim($this->plantilla)!=='') ){
            $sql .= "plantilla = '".$this->plantilla."',";
        }
        if(($this->orientacion !== null) && (trim($this->orientacion)!=='') ){
            $sql .= "orientacion = '".$this->orientacion."',";
        }
        if(($this->archivo_plantilla !== null) && (trim($this->archivo_plantilla)!=='') ){
            $sql .= "archivo_plantilla = '".$this->archivo_plantilla."',";
        }
        if(($this->archivoback_plantilla !== null) && (trim($this->archivoback_plantilla)!=='') ){
            $sql .= "archivoback_plantilla = '".$this->archivoback_plantilla."',";
        }
        if(($this->logo_chk !== null) && (trim($this->logo_chk)!=='') ){
            $sql .= "logo_chk = '".$this->logo_chk."',";
        }
        else{
            $sql .= "logo_chk = NULL,";
        }
        if(($this->archivo_logo !== null) && (trim($this->archivo_logo)!=='') ){
            $sql .= "archivo_logo = '".$this->archivo_logo."',";
        }
        else{
            $sql .= "archivo_logo = NULL,";
        }
        if(($this->foto_top !== null) && (trim($this->foto_top)!=='') ){
            $sql .= "foto_top = '".$this->foto_top."',";
        }
        if(($this->foto_left !== null) && (trim($this->foto_left)!=='') ){
            $sql .= "foto_left = '".$this->foto_left."',";
        }
        if(($this->foto_w !== null) && (trim($this->foto_w)!=='') ){
            $sql .= "foto_w = '".$this->foto_w."',";
        }
        if(($this->foto_h !== null) && (trim($this->foto_h)!=='') ){
            $sql .= "foto_h = '".$this->foto_h."',";
        }
        if(($this->barcode_chk !== null) && (trim($this->barcode_chk)!=='') ){
            $sql .= "barcode_chk = '".$this->barcode_chk."',";
        }
        else{
            $sql .= "barcode_chk = NULL,";
        }
        if(($this->barcode_front !== null) && (trim($this->barcode_front)!=='') ){
            $sql .= "barcode_front = '".$this->barcode_front."',";
        }
        else{
            $sql .= "barcode_front = NULL,";
        }
        if(($this->barcode_back !== null) && (trim($this->barcode_back)!=='') ){
            $sql .= "barcode_back = '".$this->barcode_back."',";
        }
        else{
            $sql .= "barcode_back = NULL,";
        }
        if(($this->barcode_top !== null) && (trim($this->barcode_top)!=='') ){
            $sql .= "barcode_top = '".$this->barcode_top."',";
        }
        else{
            $sql .= "barcode_top = NULL,";
        }
        if(($this->barcode_left !== null) && (trim($this->barcode_left)!=='') ){
            $sql .= "barcode_left = '".$this->barcode_left."',";
        }
        else{
            $sql .= "barcode_left = NULL,";
        }
        if(($this->barcode_w !== null) && (trim($this->barcode_w)!=='') ){
            $sql .= "barcode_w = '".$this->barcode_w."',";
        }
        else{
            $sql .= "barcode_w = NULL,";
        }
        if(($this->barcode_h !== null) && (trim($this->barcode_h)!=='') ){
            $sql .= "barcode_h = '".$this->barcode_h."',";
        }
        else{
            $sql .= "barcode_h = NULL,";
        }
        if(($this->logo_top !== null) && (trim($this->logo_top)!=='') ){
            $sql .= "logo_top = '".$this->logo_top."',";
        }
        else{
            $sql .= "logo_top = NULL,";
        }
        if(($this->logo_left !== null) && (trim($this->logo_left)!=='') ){
            $sql .= "logo_left = '".$this->logo_left."',";
        }
        else{
            $sql .= "logo_left = NULL,";
        }
        if(($this->logo_w !== null) && (trim($this->logo_w)!=='') ){
            $sql .= "logo_w = '".$this->logo_w."',";
        }
        else{
            $sql .= "logo_w = NULL,";
        }
        if(($this->logo_h !== null) && (trim($this->logo_h)!=='') ){
            $sql .= "logo_h = '".$this->logo_h."',";
        }
        else{
            $sql .= "logo_h = NULL,";
        }
        if(($this->logoback_chk !== null) && (trim($this->logoback_chk)!=='') ){
            $sql .= "logoback_chk = '".$this->logoback_chk."',";
        }
        else{
            $sql .= "logoback_chk = NULL,";
        }
        if(($this->archivoback_logo !== null) && (trim($this->archivoback_logo)!=='') ){
            $sql .= "archivoback_logo = '".$this->archivoback_logo."',";
        }
        else{
            $sql .= "archivoback_logo = NULL,";
        }
        if(($this->logoback_top !== null) && (trim($this->logoback_top)!=='') ){
            $sql .= "logoback_top = '".$this->logoback_top."',";
        }
        else{
            $sql .= "logoback_top = NULL,";
        }
        if(($this->logoback_left !== null) && (trim($this->logoback_left)!=='') ){
            $sql .= "logoback_left = '".$this->logoback_left."',";
        }
        else{
            $sql .= "logoback_left = NULL,";
        }
        if(($this->logoback_w !== null) && (trim($this->logoback_w)!=='') ){
            $sql .= "logoback_w = '".$this->logoback_w."',";
        }
        else{
            $sql .= "logoback_w = NULL,";
        }
        if(($this->logoback_h !== null) && (trim($this->logoback_h)!=='') ){
            $sql .= "logoback_h = '".$this->logoback_h."',";
        }
        else{
            $sql .= "logoback_h = NULL,";
        }
        if(($this->institucion_chk !== null) && (trim($this->institucion_chk)!=='') ){
            $sql .= "institucion_chk = '".$this->institucion_chk."',";
        }
        else{
            $sql .= "institucion_chk = NULL,";
        }
        if(($this->institucion_nombre !== null) && (trim($this->institucion_nombre)!=='') ){
            $sql .= "institucion_nombre = '".$this->institucion_nombre."',";
        }
        else{
            $sql .= "institucion_nombre = NULL,";
        }
        if(($this->institucion_top !== null) && (trim($this->institucion_top)!=='') ){
            $sql .= "institucion_top = '".$this->institucion_top."',";
        }
        else{
            $sql .= "institucion_top = NULL,";
        }
        if(($this->institucion_left !== null) && (trim($this->institucion_left)!=='') ){
            $sql .= "institucion_left = '".$this->institucion_left."',";
        }
        else{
            $sql .= "institucion_left = NULL,";
        }
        if(($this->institucion_w !== null) && (trim($this->institucion_w)!=='') ){
            $sql .= "institucion_w = '".$this->institucion_w."',";
        }
        else{
            $sql .= "institucion_w = NULL,";
        }
        if(($this->institucion_h !== null) && (trim($this->institucion_h)!=='') ){
            $sql .= "institucion_h = '".$this->institucion_h."',";
        }
        else{
            $sql .= "institucion_h = NULL,";
        }
        if(($this->institucion_color !== null) && (trim($this->institucion_color)!=='') ){
            $sql .= "institucion_color = '".$this->institucion_color."',";
        }
        else{
            $sql .= "institucion_color = NULL,";
        }
        if(($this->institucion_fuentetamano !== null) && (trim($this->institucion_fuentetamano)!=='') ){
            $sql .= "institucion_fuentetamano = '".$this->institucion_fuentetamano."',";
        }
        if(($this->institucion_fuenteletra !== null) && (trim($this->institucion_fuenteletra)!=='') ){
            $sql .= "institucion_fuenteletra = '".$this->institucion_fuenteletra."',";
        }
        if(($this->institucion_bgcolor_chk !== null) && (trim($this->institucion_bgcolor_chk)!=='') ){
            $sql .= "institucion_bgcolor_chk = '".$this->institucion_bgcolor_chk."',";
        }
        else{
            $sql .= "institucion_bgcolor_chk = NULL,";
        }
        if(($this->institucion_bgcolor !== null) && (trim($this->institucion_bgcolor)!=='') ){
            $sql .= "institucion_bgcolor = '".$this->institucion_bgcolor."',";
        }
        if(($this->institucion_fuentealign !== null) && (trim($this->institucion_fuentealign)!=='') ){
            $sql .= "institucion_fuentealign = '".$this->institucion_fuentealign."',";
        }
        if(($this->institucion_fuentevalign !== null) && (trim($this->institucion_fuentevalign)!=='') ){
            $sql .= "institucion_fuentevalign = '".$this->institucion_fuentevalign."',";
        }
        if(($this->institucionback_chk !== null) && (trim($this->institucionback_chk)!=='') ){
            $sql .= "institucionback_chk = '".$this->institucionback_chk."',";
        }
        else{
            $sql .= "institucionback_chk = NULL,";
        }
        if(($this->institucionback_top !== null) && (trim($this->institucionback_top)!=='') ){
            $sql .= "institucionback_top = '".$this->institucionback_top."',";
        }
        else{
            $sql .= "institucionback_top = NULL,";
        }
        if(($this->institucionback_left !== null) && (trim($this->institucionback_left)!=='') ){
            $sql .= "institucionback_left = '".$this->institucionback_left."',";
        }
        else{
            $sql .= "institucionback_left = NULL,";
        }
        if(($this->institucionback_w !== null) && (trim($this->institucionback_w)!=='') ){
            $sql .= "institucionback_w = '".$this->institucionback_w."',";
        }
        else{
            $sql .= "institucionback_w = NULL,";
        }
        if(($this->institucionback_h !== null) && (trim($this->institucionback_h)!=='') ){
            $sql .= "institucionback_h = '".$this->institucionback_h."',";
        }
        else{
            $sql .= "institucionback_h = NULL,";
        }
        if(($this->institucionback_color !== null) && (trim($this->institucionback_color)!=='') ){
            $sql .= "institucionback_color = '".$this->institucionback_color."',";
        }
        else{
            $sql .= "institucionback_color = NULL,";
        }
        if(($this->institucionback_fuentetamano !== null) && (trim($this->institucionback_fuentetamano)!=='') ){
            $sql .= "institucionback_fuentetamano = '".$this->institucionback_fuentetamano."',";
        }
        if(($this->institucionback_fuenteletra !== null) && (trim($this->institucionback_fuenteletra)!=='') ){
            $sql .= "institucionback_fuenteletra = '".$this->institucionback_fuenteletra."',";
        }
        if(($this->institucionback_bgcolor_chk !== null) && (trim($this->institucionback_bgcolor_chk)!=='') ){
            $sql .= "institucionback_bgcolor_chk = '".$this->institucionback_bgcolor_chk."',";
        }
        else{
            $sql .= "institucionback_bgcolor_chk = NULL,";
        }
        if(($this->institucionback_bgcolor !== null) && (trim($this->institucionback_bgcolor)!=='') ){
            $sql .= "institucionback_bgcolor = '".$this->institucionback_bgcolor."',";
        }
        if(($this->institucionback_fuentealign !== null) && (trim($this->institucionback_fuentealign)!=='') ){
            $sql .= "institucionback_fuentealign = '".$this->institucionback_fuentealign."',";
        }
        if(($this->institucionback_fuentevalign !== null) && (trim($this->institucionback_fuentevalign)!=='') ){
            $sql .= "institucionback_fuentevalign = '".$this->institucionback_fuentevalign."',";
        }
        if(($this->nombreapellido_top !== null) && (trim($this->nombreapellido_top)!=='') ){
            $sql .= "nombreapellido_top = '".$this->nombreapellido_top."',";
        }
        if(($this->nombreapellido_left !== null) && (trim($this->nombreapellido_left)!=='') ){
            $sql .= "nombreapellido_left = '".$this->nombreapellido_left."',";
        }
        if(($this->nombreapellido_w !== null) && (trim($this->nombreapellido_w)!=='') ){
            $sql .= "nombreapellido_w = '".$this->nombreapellido_w."',";
        }
        if(($this->nombreapellido_h !== null) && (trim($this->nombreapellido_h)!=='') ){
            $sql .= "nombreapellido_h = '".$this->nombreapellido_h."',";
        }
        if(($this->nombreapellido_color !== null) && (trim($this->nombreapellido_color)!=='') ){
            $sql .= "nombreapellido_color = '".$this->nombreapellido_color."',";
        }
        if(($this->nombreapellido_fuentetamano !== null) && (trim($this->nombreapellido_fuentetamano)!=='') ){
            $sql .= "nombreapellido_fuentetamano = '".$this->nombreapellido_fuentetamano."',";
        }
        if(($this->nombreapellido_fuenteletra !== null) && (trim($this->nombreapellido_fuenteletra)!=='') ){
            $sql .= "nombreapellido_fuenteletra = '".$this->nombreapellido_fuenteletra."',";
        }
        if(($this->nombreapellido_bgcolor_chk !== null) && (trim($this->nombreapellido_bgcolor_chk)!=='') ){
            $sql .= "nombreapellido_bgcolor_chk = '".$this->nombreapellido_bgcolor_chk."',";
        }
        else{
            $sql .= "nombreapellido_bgcolor_chk = NULL,";
        }
        if(($this->nombreapellido_bgcolor !== null) && (trim($this->nombreapellido_bgcolor)!=='') ){
            $sql .= "nombreapellido_bgcolor = '".$this->nombreapellido_bgcolor."',";
        }
        if(($this->nombreapellido_fuentealign !== null) && (trim($this->nombreapellido_fuentealign)!=='') ){
            $sql .= "nombreapellido_fuentealign = '".$this->nombreapellido_fuentealign."',";
        }
        if(($this->nombreapellido_fuentevalign !== null) && (trim($this->nombreapellido_fuentevalign)!=='') ){
            $sql .= "nombreapellido_fuentevalign = '".$this->nombreapellido_fuentevalign."',";
        }
        if(($this->cedula_top !== null) && (trim($this->cedula_top)!=='') ){
            $sql .= "cedula_top = '".$this->cedula_top."',";
        }
        if(($this->cedula_left !== null) && (trim($this->cedula_left)!=='') ){
            $sql .= "cedula_left = '".$this->cedula_left."',";
        }
        if(($this->cedula_w !== null) && (trim($this->cedula_w)!=='') ){
            $sql .= "cedula_w = '".$this->cedula_w."',";
        }
        if(($this->cedula_h !== null) && (trim($this->cedula_h)!=='') ){
            $sql .= "cedula_h = '".$this->cedula_h."',";
        }
        if(($this->cedula_color !== null) && (trim($this->cedula_color)!=='') ){
            $sql .= "cedula_color = '".$this->cedula_color."',";
        }
        if(($this->cedula_fuentetamano !== null) && (trim($this->cedula_fuentetamano)!=='') ){
            $sql .= "cedula_fuentetamano = '".$this->cedula_fuentetamano."',";
        }
        if(($this->cedula_fuenteletra !== null) && (trim($this->cedula_fuenteletra)!=='') ){
            $sql .= "cedula_fuenteletra = '".$this->cedula_fuenteletra."',";
        }
        if(($this->cedula_bgcolor_chk !== null) && (trim($this->cedula_bgcolor_chk)!=='') ){
            $sql .= "cedula_bgcolor_chk = '".$this->cedula_bgcolor_chk."',";
        }
        else{
            $sql .= "cedula_bgcolor_chk = NULL,";
        }
        if(($this->cedula_bgcolor !== null) && (trim($this->cedula_bgcolor)!=='') ){
            $sql .= "cedula_bgcolor = '".$this->cedula_bgcolor."',";
        }
        if(($this->cedula_fuentealign !== null) && (trim($this->cedula_fuentealign)!=='') ){
            $sql .= "cedula_fuentealign = '".$this->cedula_fuentealign."',";
        }
        if(($this->cedula_fuentevalign !== null) && (trim($this->cedula_fuentevalign)!=='') ){
            $sql .= "cedula_fuentevalign = '".$this->cedula_fuentevalign."',";
        }
        if(($this->fecha_chk !== null) && (trim($this->fecha_chk)!=='') ){
            $sql .= "fecha_chk = '".$this->fecha_chk."',";
        }
        else{
            $sql .= "fecha_chk = NULL,";
        }
        if(($this->fecha_top !== null) && (trim($this->fecha_top)!=='') ){
            $sql .= "fecha_top = '".$this->fecha_top."',";
        }
        else{
            $sql .= "fecha_top = NULL,";
        }
        if(($this->fecha_left !== null) && (trim($this->fecha_left)!=='') ){
            $sql .= "fecha_left = '".$this->fecha_left."',";
        }
        else{
            $sql .= "fecha_left = NULL,";
        }
        if(($this->fecha_w !== null) && (trim($this->fecha_w)!=='') ){
            $sql .= "fecha_w = '".$this->fecha_w."',";
        }
        else{
            $sql .= "fecha_w = NULL,";
        }
        if(($this->fecha_h !== null) && (trim($this->fecha_h)!=='') ){
            $sql .= "fecha_h = '".$this->fecha_h."',";
        }
        else{
            $sql .= "fecha_h = NULL,";
        }
        if(($this->fecha_color !== null) && (trim($this->fecha_color)!=='') ){
            $sql .= "fecha_color = '".$this->fecha_color."',";
        }
        else{
            $sql .= "fecha_color = NULL,";
        }
        if(($this->fecha_fuentetamano !== null) && (trim($this->fecha_fuentetamano)!=='') ){
            $sql .= "fecha_fuentetamano = '".$this->fecha_fuentetamano."',";
        }
        if(($this->fecha_fuenteletra !== null) && (trim($this->fecha_fuenteletra)!=='') ){
            $sql .= "fecha_fuenteletra = '".$this->fecha_fuenteletra."',";
        }
        if(($this->fecha_bgcolor_chk !== null) && (trim($this->fecha_bgcolor_chk)!=='') ){
            $sql .= "fecha_bgcolor_chk = '".$this->fecha_bgcolor_chk."',";
        }
        else{
            $sql .= "fecha_bgcolor_chk = NULL,";
        }
        if(($this->fecha_bgcolor !== null) && (trim($this->fecha_bgcolor)!=='') ){
            $sql .= "fecha_bgcolor = '".$this->fecha_bgcolor."',";
        }
        if(($this->fecha_fuentealign !== null) && (trim($this->fecha_fuentealign)!=='') ){
            $sql .= "fecha_fuentealign = '".$this->fecha_fuentealign."',";
        }
        if(($this->fecha_fuentevalign !== null) && (trim($this->fecha_fuentevalign)!=='') ){
            $sql .= "fecha_fuentevalign = '".$this->fecha_fuentevalign."',";
        }
        if(($this->visitante_top !== null) && (trim($this->visitante_top)!=='') ){
            $sql .= "visitante_top = '".$this->visitante_top."',";
        }
        else{
            $sql .= "visitante_top = NULL,";
        }
        if(($this->visitante_left !== null) && (trim($this->visitante_left)!=='') ){
            $sql .= "visitante_left = '".$this->visitante_left."',";
        }
        else{
            $sql .= "visitante_left = NULL,";
        }
        if(($this->visitante_w !== null) && (trim($this->visitante_w)!=='') ){
            $sql .= "visitante_w = '".$this->visitante_w."',";
        }
        else{
            $sql .= "visitante_w = NULL,";
        }
        if(($this->visitante_h !== null) && (trim($this->visitante_h)!=='') ){
            $sql .= "visitante_h = '".$this->visitante_h."',";
        }
        else{
            $sql .= "visitante_h = NULL,";
        }
        if(($this->visitante_color !== null) && (trim($this->visitante_color)!=='') ){
            $sql .= "visitante_color = '".$this->visitante_color."',";
        }
        else{
            $sql .= "visitante_color = NULL,";
        }
        if(($this->visitante_fuentetamano !== null) && (trim($this->visitante_fuentetamano)!=='') ){
            $sql .= "visitante_fuentetamano = '".$this->visitante_fuentetamano."',";
        }
        if(($this->visitante_fuenteletra !== null) && (trim($this->visitante_fuenteletra)!=='') ){
            $sql .= "visitante_fuenteletra = '".$this->visitante_fuenteletra."',";
        }
        if(($this->visitante_bgcolor_chk !== null) && (trim($this->visitante_bgcolor_chk)!=='') ){
            $sql .= "visitante_bgcolor_chk = '".$this->visitante_bgcolor_chk."',";
        }
        else{
            $sql .= "visitante_bgcolor_chk = NULL,";
        }
        if(($this->visitante_bgcolor !== null) && (trim($this->visitante_bgcolor)!=='') ){
            $sql .= "visitante_bgcolor = '".$this->visitante_bgcolor."',";
        }
        if(($this->visitante_fuentealign !== null) && (trim($this->visitante_fuentealign)!=='') ){
            $sql .= "visitante_fuentealign = '".$this->visitante_fuentealign."',";
        }
        if(($this->visitante_fuentevalign !== null) && (trim($this->visitante_fuentevalign)!=='') ){
            $sql .= "visitante_fuentevalign = '".$this->visitante_fuentevalign."',";
        }
        if(($this->visitantenro_top !== null) && (trim($this->visitantenro_top)!=='') ){
            $sql .= "visitantenro_top = '".$this->visitantenro_top."',";
        }
        else{
            $sql .= "visitantenro_top = NULL,";
        }
        if(($this->visitantenro_left !== null) && (trim($this->visitantenro_left)!=='') ){
            $sql .= "visitantenro_left = '".$this->visitantenro_left."',";
        }
        else{
            $sql .= "visitantenro_left = NULL,";
        }
        if(($this->visitantenro_w !== null) && (trim($this->visitantenro_w)!=='') ){
            $sql .= "visitantenro_w = '".$this->visitantenro_w."',";
        }
        else{
            $sql .= "visitantenro_w = NULL,";
        }
        if(($this->visitantenro_h !== null) && (trim($this->visitantenro_h)!=='') ){
            $sql .= "visitantenro_h = '".$this->visitantenro_h."',";
        }
        else{
            $sql .= "visitantenro_h = NULL,";
        }
        if(($this->visitantenro_color !== null) && (trim($this->visitantenro_color)!=='') ){
            $sql .= "visitantenro_color = '".$this->visitantenro_color."',";
        }
        else{
            $sql .= "visitantenro_color = NULL,";
        }
        if(($this->visitantenro_fuentetamano !== null) && (trim($this->visitantenro_fuentetamano)!=='') ){
            $sql .= "visitantenro_fuentetamano = '".$this->visitantenro_fuentetamano."',";
        }
        if(($this->visitantenro_fuenteletra !== null) && (trim($this->visitantenro_fuenteletra)!=='') ){
            $sql .= "visitantenro_fuenteletra = '".$this->visitantenro_fuenteletra."',";
        }
        if(($this->visitantenro_bgcolor_chk !== null) && (trim($this->visitantenro_bgcolor_chk)!=='') ){
            $sql .= "visitantenro_bgcolor_chk = '".$this->visitantenro_bgcolor_chk."',";
        }
        else{
            $sql .= "visitantenro_bgcolor_chk = NULL,";
        }
        if(($this->visitantenro_bgcolor !== null) && (trim($this->visitantenro_bgcolor)!=='') ){
            $sql .= "visitantenro_bgcolor = '".$this->visitantenro_bgcolor."',";
        }
        if(($this->visitantenro_fuentealign !== null) && (trim($this->visitantenro_fuentealign)!=='') ){
            $sql .= "visitantenro_fuentealign = '".$this->visitantenro_fuentealign."',";
        }
        if(($this->visitantenro_fuentevalign !== null) && (trim($this->visitantenro_fuentevalign)!=='') ){
            $sql .= "visitantenro_fuentevalign = '".$this->visitantenro_fuentevalign."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idplantilla = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM car_plantilla  WHERE idplantilla = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CarPlantillas
     * @return object Devuelve un registro como objeto
     */
    function  getCarPlantillas(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idplantillaBuscador"]){
            $in = null;
            foreach ($_REQUEST["idplantillaBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idplantilla in (".$in.")";
        }
        
        $sql = "select /*start*/ idplantilla,plantilla,orientacion,archivo_plantilla,archivoback_plantilla,logo_chk,archivo_logo,foto_top,foto_left,foto_w,foto_h,barcode_chk,barcode_front,barcode_back,barcode_top,barcode_left,barcode_w,barcode_h,logo_top,logo_left,logo_w,logo_h,logoback_chk,archivoback_logo,logoback_top,logoback_left,logoback_w,logoback_h,institucion_chk,institucion_nombre,institucion_top,institucion_left,institucion_w,institucion_h,institucion_color,institucion_fuentetamano,institucion_fuenteletra,institucion_bgcolor_chk,institucion_bgcolor,institucion_fuentealign,institucion_fuentevalign,institucionback_chk,institucionback_top,institucionback_left,institucionback_w,institucionback_h,institucionback_color,institucionback_fuentetamano,institucionback_fuenteletra,institucionback_bgcolor_chk,institucionback_bgcolor,institucionback_fuentealign,institucionback_fuentevalign,nombreapellido_top,nombreapellido_left,nombreapellido_w,nombreapellido_h,nombreapellido_color,nombreapellido_fuentetamano,nombreapellido_fuenteletra,nombreapellido_bgcolor_chk,nombreapellido_bgcolor,nombreapellido_fuentealign,nombreapellido_fuentevalign,cedula_top,cedula_left,cedula_w,cedula_h,cedula_color,cedula_fuentetamano,cedula_fuenteletra,cedula_bgcolor_chk,cedula_bgcolor,cedula_fuentealign,cedula_fuentevalign,fecha_chk,fecha_top,fecha_left,fecha_w,fecha_h,fecha_color,fecha_fuentetamano,fecha_fuenteletra,fecha_bgcolor_chk,fecha_bgcolor,fecha_fuentealign,fecha_fuentevalign,visitante_top,visitante_left,visitante_w,visitante_h,visitante_color,visitante_fuentetamano,visitante_fuenteletra,visitante_bgcolor_chk,visitante_bgcolor,visitante_fuentealign,visitante_fuentevalign,visitantenro_top,visitantenro_left,visitantenro_w,visitantenro_h,visitantenro_color,visitantenro_fuentetamano,visitantenro_fuenteletra,visitantenro_bgcolor_chk,visitantenro_bgcolor,visitantenro_fuentealign,visitantenro_fuentevalign /*end*/ from car_plantilla ".$arg." order by plantilla,orientacion";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CarPlantilla
     * @param int $idplantilla Codigo
     * @return object Devuelve registros como objeto
     */
    function getCarPlantilla($idplantilla){
        $sql = "SELECT idplantilla,plantilla,orientacion,archivo_plantilla,archivoback_plantilla,logo_chk,archivo_logo,foto_top,foto_left,foto_w,foto_h,barcode_chk,barcode_front,barcode_back,barcode_top,barcode_left,barcode_w,barcode_h,logo_top,logo_left,logo_w,logo_h,logoback_chk,archivoback_logo,logoback_top,logoback_left,logoback_w,logoback_h,institucion_chk,institucion_nombre,institucion_top,institucion_left,institucion_w,institucion_h,institucion_color,institucion_fuentetamano,institucion_fuenteletra,institucion_bgcolor_chk,institucion_bgcolor,institucion_fuentealign,institucion_fuentevalign,institucionback_chk,institucionback_top,institucionback_left,institucionback_w,institucionback_h,institucionback_color,institucionback_fuentetamano,institucionback_fuenteletra,institucionback_bgcolor_chk,institucionback_bgcolor,institucionback_fuentealign,institucionback_fuentevalign,nombreapellido_top,nombreapellido_left,nombreapellido_w,nombreapellido_h,nombreapellido_color,nombreapellido_fuentetamano,nombreapellido_fuenteletra,nombreapellido_bgcolor_chk,nombreapellido_bgcolor,nombreapellido_fuentealign,nombreapellido_fuentevalign,cedula_top,cedula_left,cedula_w,cedula_h,cedula_color,cedula_fuentetamano,cedula_fuenteletra,cedula_bgcolor_chk,cedula_bgcolor,cedula_fuentealign,cedula_fuentevalign,fecha_chk,fecha_top,fecha_left,fecha_w,fecha_h,fecha_color,fecha_fuentetamano,fecha_fuenteletra,fecha_bgcolor_chk,fecha_bgcolor,fecha_fuentealign,fecha_fuentevalign,visitante_top,visitante_left,visitante_w,visitante_h,visitante_color,visitante_fuentetamano,visitante_fuenteletra,visitante_bgcolor_chk,visitante_bgcolor,visitante_fuentealign,visitante_fuentevalign,visitantenro_top,visitantenro_left,visitantenro_w,visitantenro_h,visitantenro_color,visitantenro_fuentetamano,visitantenro_fuenteletra,visitantenro_bgcolor_chk,visitantenro_bgcolor,visitantenro_fuentealign,visitantenro_fuentevalign FROM car_plantilla WHERE idplantilla = '".$idplantilla."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>