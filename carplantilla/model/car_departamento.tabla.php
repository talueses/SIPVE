<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CarDepartamento {
    
    private $iddepartamento = null;
    private $idplantilla = null;
    private $departamento_nombre = null;
    private $departamento_top = null;
    private $departamento_left = null;
    private $departamento_w = null;
    private $departamento_h = null;
    private $departamento_color = null;
    private $departamento_fuentetamano = null;
    private $departamento_fuenteletra = null;
    private $departamento_bgcolor_chk = null;
    private $departamento_bgcolor = null;
    private $departamento_fuentealign = null;
    private $departamento_fuentevalign = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIddepartamento($iddepartamento){
        $this->iddepartamento = $iddepartamento;
    }
    public function getIddepartamento(){
        return $this->iddepartamento;
    }
    public function setIdplantilla($idplantilla){
        $this->idplantilla = $idplantilla;
    }
    public function getIdplantilla(){
        return $this->idplantilla;
    }
    public function setDepartamento_nombre($departamento_nombre){
        $this->departamento_nombre = $departamento_nombre;
    }
    public function getDepartamento_nombre(){
        return $this->departamento_nombre;
    }
    public function setDepartamento_top($departamento_top){
        $this->departamento_top = $departamento_top;
    }
    public function getDepartamento_top(){
        return $this->departamento_top;
    }
    public function setDepartamento_left($departamento_left){
        $this->departamento_left = $departamento_left;
    }
    public function getDepartamento_left(){
        return $this->departamento_left;
    }
    public function setDepartamento_w($departamento_w){
        $this->departamento_w = $departamento_w;
    }
    public function getDepartamento_w(){
        return $this->departamento_w;
    }
    public function setDepartamento_h($departamento_h){
        $this->departamento_h = $departamento_h;
    }
    public function getDepartamento_h(){
        return $this->departamento_h;
    }
    public function setDepartamento_color($departamento_color){
        $this->departamento_color = $departamento_color;
    }
    public function getDepartamento_color(){
        return $this->departamento_color;
    }
    public function setDepartamento_fuentetamano($departamento_fuentetamano){
        $this->departamento_fuentetamano = $departamento_fuentetamano;
    }
    public function getDepartamento_fuentetamano(){
        return $this->departamento_fuentetamano;
    }
    public function setDepartamento_fuenteletra($departamento_fuenteletra){
        $this->departamento_fuenteletra = $departamento_fuenteletra;
    }
    public function getDepartamento_fuenteletra(){
        return $this->departamento_fuenteletra;
    }
    public function setDepartamento_bgcolor_chk($departamento_bgcolor_chk){
        $this->departamento_bgcolor_chk = $departamento_bgcolor_chk;
    }
    public function getDepartamento_bgcolor_chk(){
        return $this->departamento_bgcolor_chk;
    }
    public function setDepartamento_bgcolor($departamento_bgcolor){
        $this->departamento_bgcolor = $departamento_bgcolor;
    }
    public function getDepartamento_bgcolor(){
        return $this->departamento_bgcolor;
    }
    public function setDepartamento_fuentealign($departamento_fuentealign){
        $this->departamento_fuentealign = $departamento_fuentealign;
    }
    public function getDepartamento_fuentealign(){
        return $this->departamento_fuentealign;
    }
    public function setDepartamento_fuentevalign($departamento_fuentevalign){
        $this->departamento_fuentevalign = $departamento_fuentevalign;
    }
    public function getDepartamento_fuentevalign(){
        return $this->departamento_fuentevalign;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->iddepartamento !== null) && (trim($this->iddepartamento)!=='') ){
            $campos .= "iddepartamento,";
            $valores .= "'".$this->iddepartamento."',";
        }
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $campos .= "idplantilla,";
            $valores .= "'".$this->idplantilla."',";
        }
        if(($this->departamento_nombre !== null) && (trim($this->departamento_nombre)!=='') ){
            $campos .= "departamento_nombre,";
            $valores .= "'".$this->departamento_nombre."',";
        }
        if(($this->departamento_top !== null) && (trim($this->departamento_top)!=='') ){
            $campos .= "departamento_top,";
            $valores .= "'".$this->departamento_top."',";
        }
        if(($this->departamento_left !== null) && (trim($this->departamento_left)!=='') ){
            $campos .= "departamento_left,";
            $valores .= "'".$this->departamento_left."',";
        }
        if(($this->departamento_w !== null) && (trim($this->departamento_w)!=='') ){
            $campos .= "departamento_w,";
            $valores .= "'".$this->departamento_w."',";
        }
        if(($this->departamento_h !== null) && (trim($this->departamento_h)!=='') ){
            $campos .= "departamento_h,";
            $valores .= "'".$this->departamento_h."',";
        }
        if(($this->departamento_color !== null) && (trim($this->departamento_color)!=='') ){
            $campos .= "departamento_color,";
            $valores .= "'".$this->departamento_color."',";
        }
        if(($this->departamento_fuentetamano !== null) && (trim($this->departamento_fuentetamano)!=='') ){
            $campos .= "departamento_fuentetamano,";
            $valores .= "'".$this->departamento_fuentetamano."',";
        }
        if(($this->departamento_fuenteletra !== null) && (trim($this->departamento_fuenteletra)!=='') ){
            $campos .= "departamento_fuenteletra,";
            $valores .= "'".$this->departamento_fuenteletra."',";
        }
        if(($this->departamento_bgcolor_chk !== null) && (trim($this->departamento_bgcolor_chk)!=='') ){
            $campos .= "departamento_bgcolor_chk,";
            $valores .= "'".$this->departamento_bgcolor_chk."',";
        }
        if(($this->departamento_bgcolor !== null) && (trim($this->departamento_bgcolor)!=='') ){
            $campos .= "departamento_bgcolor,";
            $valores .= "'".$this->departamento_bgcolor."',";
        }
        if(($this->departamento_fuentealign !== null) && (trim($this->departamento_fuentealign)!=='') ){
            $campos .= "departamento_fuentealign,";
            $valores .= "'".$this->departamento_fuentealign."',";
        }
        if(($this->departamento_fuentevalign !== null) && (trim($this->departamento_fuentevalign)!=='') ){
            $campos .= "departamento_fuentevalign,";
            $valores .= "'".$this->departamento_fuentevalign."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO car_departamento $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE car_departamento SET ";
        
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $sql .= "idplantilla = '".$this->idplantilla."',";
        }
        else{
            $sql .= "idplantilla = NULL,";
        }
        if(($this->departamento_nombre !== null) && (trim($this->departamento_nombre)!=='') ){
            $sql .= "departamento_nombre = '".$this->departamento_nombre."',";
        }
        if(($this->departamento_top !== null) && (trim($this->departamento_top)!=='') ){
            $sql .= "departamento_top = '".$this->departamento_top."',";
        }
        else{
            $sql .= "departamento_top = NULL,";
        }
        if(($this->departamento_left !== null) && (trim($this->departamento_left)!=='') ){
            $sql .= "departamento_left = '".$this->departamento_left."',";
        }
        else{
            $sql .= "departamento_left = NULL,";
        }
        if(($this->departamento_w !== null) && (trim($this->departamento_w)!=='') ){
            $sql .= "departamento_w = '".$this->departamento_w."',";
        }
        else{
            $sql .= "departamento_w = NULL,";
        }
        if(($this->departamento_h !== null) && (trim($this->departamento_h)!=='') ){
            $sql .= "departamento_h = '".$this->departamento_h."',";
        }
        else{
            $sql .= "departamento_h = NULL,";
        }
        if(($this->departamento_color !== null) && (trim($this->departamento_color)!=='') ){
            $sql .= "departamento_color = '".$this->departamento_color."',";
        }
        else{
            $sql .= "departamento_color = NULL,";
        }
        if(($this->departamento_fuentetamano !== null) && (trim($this->departamento_fuentetamano)!=='') ){
            $sql .= "departamento_fuentetamano = '".$this->departamento_fuentetamano."',";
        }
        if(($this->departamento_fuenteletra !== null) && (trim($this->departamento_fuenteletra)!=='') ){
            $sql .= "departamento_fuenteletra = '".$this->departamento_fuenteletra."',";
        }
        if(($this->departamento_bgcolor_chk !== null) && (trim($this->departamento_bgcolor_chk)!=='') ){
            $sql .= "departamento_bgcolor_chk = '".$this->departamento_bgcolor_chk."',";
        }
        else{
            $sql .= "departamento_bgcolor_chk = NULL,";
        }
        if(($this->departamento_bgcolor !== null) && (trim($this->departamento_bgcolor)!=='') ){
            $sql .= "departamento_bgcolor = '".$this->departamento_bgcolor."',";
        }
        if(($this->departamento_fuentealign !== null) && (trim($this->departamento_fuentealign)!=='') ){
            $sql .= "departamento_fuentealign = '".$this->departamento_fuentealign."',";
        }
        if(($this->departamento_fuentevalign !== null) && (trim($this->departamento_fuentevalign)!=='') ){
            $sql .= "departamento_fuentevalign = '".$this->departamento_fuentevalign."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE iddepartamento = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM car_departamento  WHERE iddepartamento = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CarDepartamentos
     * @return object Devuelve un registro como objeto
     */
    function  getCarDepartamentos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["iddepartamentoBuscador"]){
            $in = null;
            foreach ($_REQUEST["iddepartamentoBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where iddepartamento in (".$in.")";
        }
        
        $sql = "select iddepartamento,idplantilla,departamento_nombre,departamento_top,departamento_left,departamento_w,departamento_h,departamento_color,departamento_fuentetamano,departamento_fuenteletra,departamento_bgcolor_chk,departamento_bgcolor,departamento_fuentealign,departamento_fuentevalign from car_departamento ".$arg." order by departamento_nombre,departamento_top";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CarDepartamento
     * @param int $iddepartamento Codigo
     * @return object Devuelve registros como objeto
     */
    function getCarDepartamento($iddepartamento){
        $sql = "SELECT iddepartamento,idplantilla,departamento_nombre,departamento_top,departamento_left,departamento_w,departamento_h,departamento_color,departamento_fuentetamano,departamento_fuenteletra,departamento_bgcolor_chk,departamento_bgcolor,departamento_fuentealign,departamento_fuentevalign FROM car_departamento WHERE iddepartamento = '".$iddepartamento."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>