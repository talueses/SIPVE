<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CarCargo {
    
    private $idcargo = null;
    private $idplantilla = null;
    private $cargo_nombre_chk = null;
    private $cargo_nombre = null;
    private $cargo_top = null;
    private $cargo_left = null;
    private $cargo_w = null;
    private $cargo_h = null;
    private $cargo_color = null;
    private $cargo_bgcolor = null;
    private $cargo_fuentetamano = null;
    private $cargo_fuenteletra = null;
    private $cargo_fuentealign = null;
    private $cargo_fuentevalign = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcargo($idcargo){
        $this->idcargo = $idcargo;
    }
    public function getIdcargo(){
        return $this->idcargo;
    }
    public function setIdplantilla($idplantilla){
        $this->idplantilla = $idplantilla;
    }
    public function getIdplantilla(){
        return $this->idplantilla;
    }
    public function setCargo_nombre_chk($cargo_nombre_chk){
        $this->cargo_nombre_chk = $cargo_nombre_chk;
    }
    public function getCargo_nombre_chk(){
        return $this->cargo_nombre_chk;
    }
    public function setCargo_nombre($cargo_nombre){
        $this->cargo_nombre = $cargo_nombre;
    }
    public function getCargo_nombre(){
        return $this->cargo_nombre;
    }
    public function setCargo_top($cargo_top){
        $this->cargo_top = $cargo_top;
    }
    public function getCargo_top(){
        return $this->cargo_top;
    }
    public function setCargo_left($cargo_left){
        $this->cargo_left = $cargo_left;
    }
    public function getCargo_left(){
        return $this->cargo_left;
    }
    public function setCargo_w($cargo_w){
        $this->cargo_w = $cargo_w;
    }
    public function getCargo_w(){
        return $this->cargo_w;
    }
    public function setCargo_h($cargo_h){
        $this->cargo_h = $cargo_h;
    }
    public function getCargo_h(){
        return $this->cargo_h;
    }
    public function setCargo_color($cargo_color){
        $this->cargo_color = $cargo_color;
    }
    public function getCargo_color(){
        return $this->cargo_color;
    }
    public function setCargo_bgcolor($cargo_bgcolor){
        $this->cargo_bgcolor = $cargo_bgcolor;
    }
    public function getCargo_bgcolor(){
        return $this->cargo_bgcolor;
    }
    public function setCargo_fuentetamano($cargo_fuentetamano){
        $this->cargo_fuentetamano = $cargo_fuentetamano;
    }
    public function getCargo_fuentetamano(){
        return $this->cargo_fuentetamano;
    }
    public function setCargo_fuenteletra($cargo_fuenteletra){
        $this->cargo_fuenteletra = $cargo_fuenteletra;
    }
    public function getCargo_fuenteletra(){
        return $this->cargo_fuenteletra;
    }
    public function setCargo_fuentealign($cargo_fuentealign){
        $this->cargo_fuentealign = $cargo_fuentealign;
    }
    public function getCargo_fuentealign(){
        return $this->cargo_fuentealign;
    }
    public function setCargo_fuentevalign($cargo_fuentevalign){
        $this->cargo_fuentevalign = $cargo_fuentevalign;
    }
    public function getCargo_fuentevalign(){
        return $this->cargo_fuentevalign;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcargo !== null) && (trim($this->idcargo)!=='') ){
            $campos .= "idcargo,";
            $valores .= "'".$this->idcargo."',";
        }
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $campos .= "idplantilla,";
            $valores .= "'".$this->idplantilla."',";
        }
        if(($this->cargo_nombre_chk !== null) && (trim($this->cargo_nombre_chk)!=='') ){
            $campos .= "cargo_nombre_chk,";
            $valores .= "'".$this->cargo_nombre_chk."',";
        }
        if(($this->cargo_nombre !== null) && (trim($this->cargo_nombre)!=='') ){
            $campos .= "cargo_nombre,";
            $valores .= "'".$this->cargo_nombre."',";
        }
        if(($this->cargo_top !== null) && (trim($this->cargo_top)!=='') ){
            $campos .= "cargo_top,";
            $valores .= "'".$this->cargo_top."',";
        }
        if(($this->cargo_left !== null) && (trim($this->cargo_left)!=='') ){
            $campos .= "cargo_left,";
            $valores .= "'".$this->cargo_left."',";
        }
        if(($this->cargo_w !== null) && (trim($this->cargo_w)!=='') ){
            $campos .= "cargo_w,";
            $valores .= "'".$this->cargo_w."',";
        }
        if(($this->cargo_h !== null) && (trim($this->cargo_h)!=='') ){
            $campos .= "cargo_h,";
            $valores .= "'".$this->cargo_h."',";
        }
        if(($this->cargo_color !== null) && (trim($this->cargo_color)!=='') ){
            $campos .= "cargo_color,";
            $valores .= "'".$this->cargo_color."',";
        }
        if(($this->cargo_bgcolor !== null) && (trim($this->cargo_bgcolor)!=='') ){
            $campos .= "cargo_bgcolor,";
            $valores .= "'".$this->cargo_bgcolor."',";
        }
        if(($this->cargo_fuentetamano !== null) && (trim($this->cargo_fuentetamano)!=='') ){
            $campos .= "cargo_fuentetamano,";
            $valores .= "'".$this->cargo_fuentetamano."',";
        }
        if(($this->cargo_fuenteletra !== null) && (trim($this->cargo_fuenteletra)!=='') ){
            $campos .= "cargo_fuenteletra,";
            $valores .= "'".$this->cargo_fuenteletra."',";
        }
        if(($this->cargo_fuentealign !== null) && (trim($this->cargo_fuentealign)!=='') ){
            $campos .= "cargo_fuentealign,";
            $valores .= "'".$this->cargo_fuentealign."',";
        }
        if(($this->cargo_fuentevalign !== null) && (trim($this->cargo_fuentevalign)!=='') ){
            $campos .= "cargo_fuentevalign,";
            $valores .= "'".$this->cargo_fuentevalign."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO car_cargo $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE car_cargo SET ";
        
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $sql .= "idplantilla = '".$this->idplantilla."',";
        }
        else{
            $sql .= "idplantilla = NULL,";
        }
        if(($this->cargo_nombre_chk !== null) && (trim($this->cargo_nombre_chk)!=='') ){
            $sql .= "cargo_nombre_chk = '".$this->cargo_nombre_chk."',";
        }
        else{
            $sql .= "cargo_nombre_chk = NULL,";
        }
        if(($this->cargo_nombre !== null) && (trim($this->cargo_nombre)!=='') ){
            $sql .= "cargo_nombre = '".$this->cargo_nombre."',";
        }
        if(($this->cargo_top !== null) && (trim($this->cargo_top)!=='') ){
            $sql .= "cargo_top = '".$this->cargo_top."',";
        }
        else{
            $sql .= "cargo_top = NULL,";
        }
        if(($this->cargo_left !== null) && (trim($this->cargo_left)!=='') ){
            $sql .= "cargo_left = '".$this->cargo_left."',";
        }
        else{
            $sql .= "cargo_left = NULL,";
        }
        if(($this->cargo_w !== null) && (trim($this->cargo_w)!=='') ){
            $sql .= "cargo_w = '".$this->cargo_w."',";
        }
        else{
            $sql .= "cargo_w = NULL,";
        }
        if(($this->cargo_h !== null) && (trim($this->cargo_h)!=='') ){
            $sql .= "cargo_h = '".$this->cargo_h."',";
        }
        else{
            $sql .= "cargo_h = NULL,";
        }
        if(($this->cargo_color !== null) && (trim($this->cargo_color)!=='') ){
            $sql .= "cargo_color = '".$this->cargo_color."',";
        }
        else{
            $sql .= "cargo_color = NULL,";
        }
        if(($this->cargo_bgcolor !== null) && (trim($this->cargo_bgcolor)!=='') ){
            $sql .= "cargo_bgcolor = '".$this->cargo_bgcolor."',";
        }
        if(($this->cargo_fuentetamano !== null) && (trim($this->cargo_fuentetamano)!=='') ){
            $sql .= "cargo_fuentetamano = '".$this->cargo_fuentetamano."',";
        }
        if(($this->cargo_fuenteletra !== null) && (trim($this->cargo_fuenteletra)!=='') ){
            $sql .= "cargo_fuenteletra = '".$this->cargo_fuenteletra."',";
        }
        if(($this->cargo_fuentealign !== null) && (trim($this->cargo_fuentealign)!=='') ){
            $sql .= "cargo_fuentealign = '".$this->cargo_fuentealign."',";
        }
        if(($this->cargo_fuentevalign !== null) && (trim($this->cargo_fuentevalign)!=='') ){
            $sql .= "cargo_fuentevalign = '".$this->cargo_fuentevalign."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcargo = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM car_cargo  WHERE idcargo = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CarCargos
     * @return object Devuelve un registro como objeto
     */
    function  getCarCargos(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcargoBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcargoBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcargo in (".$in.")";
        }
        
        $sql = "select idcargo,idplantilla,cargo_nombre_chk,cargo_nombre,cargo_top,cargo_left,cargo_w,cargo_h,cargo_color,cargo_bgcolor,cargo_fuentetamano,cargo_fuenteletra,cargo_fuentealign,cargo_fuentevalign from car_cargo ".$arg." order by cargo_nombre_chk,cargo_nombre";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CarCargo
     * @param int $idcargo Codigo
     * @return object Devuelve registros como objeto
     */
    function getCarCargo($idcargo){
        $sql = "SELECT idcargo,idplantilla,cargo_nombre_chk,cargo_nombre,cargo_top,cargo_left,cargo_w,cargo_h,cargo_color,cargo_bgcolor,cargo_fuentetamano,cargo_fuenteletra,cargo_fuentealign,cargo_fuentevalign FROM car_cargo WHERE idcargo = '".$idcargo."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>