<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}	
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
require_once "../controller/m.vidcamaraExport.control.op.php";
Controller::chkModuleDevice(); // Verificar si el modulo Corresponde al dispositivo 

$obj = new ModelCamarasExports();
$data = $obj->getCamarasUsuario($_SESSION["usuario"]);
?>
<!DOCTYPE html> 
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1;text/html; charset=UTF-8;">         
        <title>Sistema Integral de Seguridad</title>        
        <link rel="stylesheet" href="../../inicio/css/mobile/jquery.mobile.structure.css" />           
        <link rel="stylesheet" href="../../inicio/css/mobile/jquery.mobile.theme.css" />
        <link rel="stylesheet" href="../../inicio/css/mobile/mobile.custom.css" />
        <link rel="stylesheet" href="../../inicio/css/comunes.css" />
        <script src="../../inicio/js/jquery.js"></script>        
        <script type="text/javascript" src="../../inicio/js/jquery.mobile.detectmobile.js"></script> <!-- Detect Mobile -->
        <script src="../../inicio/js/jquery.mobile.custom.js"></script>        
        <script src="../../inicio/js/jquery.mobile.min.js"></script>              
        <link rel="stylesheet" href="../../inicio/css/mobile/mobile.custom.css" /> <!-- Custom Style code -->                 
        <style type="text/css">
            #camarasList{
                font-size: 12px;
                font-weight: bold;
                font-style: oblique;
                border: 1px solid #ccc0a9;
            }
                
            #camarasList a{
                font-style: normal;
            }            
            .nav-glyphish-icons{
                text-align: center;
                margin-top: -20px;
            }
            #btnExport .ui-icon { background:  url(../images/40-inbox.png) 50% 50% no-repeat; background-size: 24px 24px; }
        </style>
    </head>
    <body>
        <div data-role="page" data-theme="c" >
            <div id="headerMobile" data-role="header" data-theme="c" data-position="fixed">                 
                <a href="../../inicio/view/m.index.php" rel="external" data-role="button" data-icon="grid" data-theme="d">Men&uacute;</a>
                <center><h4>Exportar C&aacute;maras a Mobil</h4></center>
                <?php echo Controller::getMobilUserInfo();?>
            </div>
            <div data-role="content" data-theme="d" >
                <div class="ui-body ui-body-e ui-corner-all " id="camarasList">                    
                    <div class="ui-body ui-bar-c ui-corner-all">C&aacute;maras</div>
                    <pre><?php
                        foreach ($data as $row){
                            echo "\t--> ".$row->camara."\n";                            
                        }
                        ?>
                    </pre>
                    <div class="nav-glyphish-icons">
                        <?php
                        if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"exportar")){
                            ?><a href="m.vidcamaraExport.Op.php" data-theme="d" data-role="button" data-inline="true" rel="external" data-iconpos="top" data-icon="custom" target="_blank" id="btnExport">Exportar C&aacute;maras</a><?php
                        }
                        ?>
                    </div>
                    
                </div>
            </div>
            <div data-role="footer" id="footerMobile"  data-mini="true" data-position="fixed" data-theme="c"> 
                <p><center><small><?php echo Controller::$footerMessage?></small></center></p>
            </div>
        </div>
    </body>
</html>