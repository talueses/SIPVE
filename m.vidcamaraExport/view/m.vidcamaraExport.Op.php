<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
session_start(); // start up your PHP session!
/**
 * Descargar de archivo XML de camaras para MOBIL
 * @author David Concepcion CENIT-DIDI
 */

require_once "../controller/m.vidcamaraExport.control.op.php";

header('Content-Type: text/xml');
header('Content-Disposition: attachment; filename="camaras-'.date("Y-m-d_his").'.xml"');


$obj = new ModelCamarasExports();
$data = $obj->getCamarasUsuario($_SESSION["usuario"]);
echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
echo "<cameras>\n";
foreach ($data as $row){
    $type = "IP Camera";
    switch (strtoupper($row->nombre_marca_camara)){        
        case "AXIS":
            $type = "Axis IP Camera";
            break;
        case "AVTECH":
            $type = "AVTECH AVC791 DVR";
            break;
    }
    echo "<camera name=\"".$row->camara."\" type=\"".$type."\" url=\"http://".$row->ipv4."\" camInstance=\"\" username=\"".$row->cam_usuario_video."\" password=\"".$row->cam_clave_video."\" enabled=\"true\" setNames=\"\" bitOptions=\"0\" />\n";
}
echo "</cameras>";
?>
