<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
/**
 * Clase ModelCamarasExports{}
 * @author David Concepcion CENIT-DIDI
 */
class ModelCamarasExports {
    /**
     * Consulta de camaras asociadas a un usuario
     * @param string $usuario login del usuario de sistema
     * @return object  Devuelve registros como objeto
     */
    function getCamarasUsuario($usuario){        
        $sql = "select  vc.camara,
                        vc.ipv4,
                        vc.puerto,
                        vc.cam_usuario_video,
                        vc.cam_clave_video,
                        mac.nombre_marca_camara
                                    from vid_zona_usuario vzu,
                                        vid_zona_camara vzc,
                                        vid_camara vc,
                                        vid_servidor vs,
                                        vid_tipo_camara tc,
                                        vid_modelo_camara mc,
                                        vid_marca_camara mac
                                    where vzu.usuario      = '".$usuario."'
                                    and vzu.idzona       = vzc.idzona
                                    and vzc.idcamara     = vc.idcamara
                                    and vc.idservidor    = vs.idservidor
                                    and vs.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$usuario."')
                                    and vc.idtipo_camara = tc.idtipo_camara
                                    and tc.idmodelo_camara = mc.idmodelo_camara
                                    and mc.idmarca_camara = mac.idmarca_camara
                                        group by vc.ipv4
                                        order by vc.prioridad desc, vc.idservidor,vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
}
?>
