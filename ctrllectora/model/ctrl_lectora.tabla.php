<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CtrlLectora {
    
    private $idlectora = null;
    private $idcontroladora = null;
    private $idpuerta = null;
    private $numero = null;
    private $canal = null;
    private $estado = null;
    private $nombre = null;
    private $descripcion = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdlectora($idlectora){
        $this->idlectora = $idlectora;
    }
    public function getIdlectora(){
        return $this->idlectora;
    }
    public function setIdcontroladora($idcontroladora){
        $this->idcontroladora = $idcontroladora;
    }
    public function getIdcontroladora(){
        return $this->idcontroladora;
    }
    public function setIdpuerta($idpuerta){
        $this->idpuerta = $idpuerta;
    }
    public function getIdpuerta(){
        return $this->idpuerta;
    }
    public function setNumero($numero){
        $this->numero = $numero;
    }
    public function getNumero(){
        return $this->numero;
    }
    public function setCanal($canal){
        $this->canal = $canal;
    }
    public function getCanal(){
        return $this->canal;
    }
    public function setEstado($estado){
        $this->estado = $estado;
    }
    public function getEstado(){
        return $this->estado;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idlectora !== null) && (trim($this->idlectora)!=='') ){
            $campos .= "idlectora,";
            $valores .= "'".$this->idlectora."',";
        }
        if(($this->idcontroladora !== null) && (trim($this->idcontroladora)!=='') ){
            $campos .= "idcontroladora,";
            $valores .= "'".$this->idcontroladora."',";
        }
        if(($this->idpuerta !== null) && (trim($this->idpuerta)!=='') ){
            $campos .= "idpuerta,";
            $valores .= "'".$this->idpuerta."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $campos .= "numero,";
            $valores .= "'".$this->numero."',";
        }
        if(($this->canal !== null) && (trim($this->canal)!=='') ){
            $campos .= "canal,";
            $valores .= "'".$this->canal."',";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $campos .= "estado,";
            $valores .= "'".$this->estado."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $campos .= "descripcion,";
            $valores .= "'".$this->descripcion."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO ctrl_lectora $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE ctrl_lectora SET ";
        
        if(($this->idcontroladora !== null) && (trim($this->idcontroladora)!=='') ){
            $sql .= "idcontroladora = '".$this->idcontroladora."',";
        }
        if(($this->idpuerta !== null) && (trim($this->idpuerta)!=='') ){
            $sql .= "idpuerta = '".$this->idpuerta."',";
        }
        if(($this->numero !== null) && (trim($this->numero)!=='') ){
            $sql .= "numero = '".$this->numero."',";
        }
        if(($this->canal !== null) && (trim($this->canal)!=='') ){
            $sql .= "canal = '".$this->canal."',";
        }
        if(($this->estado !== null) && (trim($this->estado)!=='') ){
            $sql .= "estado = '".$this->estado."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        if(($this->descripcion !== null) && (trim($this->descripcion)!=='') ){
            $sql .= "descripcion = '".$this->descripcion."',";
        }
        else{
            $sql .= "descripcion = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idlectora = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM ctrl_lectora  WHERE idlectora = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CtrlLectoras
     * @return object Devuelve un registro como objeto
     */
    function  getCtrlLectoras(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idlectoraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idlectoraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idlectora in (".$in.")";
        }
        
        $sql = "select /*start*/ idlectora,idcontroladora,idpuerta,numero,canal,estado,nombre,descripcion /*end*/ from ctrl_lectora ".$arg." order by numero,canal";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CtrlLectora
     * @param int $idlectora Codigo
     * @return object Devuelve registros como objeto
     */
    function getCtrlLectora($idlectora){
        $sql = "SELECT idlectora,idcontroladora,idpuerta,numero,canal,estado,nombre,descripcion FROM ctrl_lectora WHERE idlectora = '".$idlectora."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>