<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'ctrl_lectora.tabla.php';

/**
 * Clase CtrlLectoras{}
 * @author David Concepcion CENIT-DIDI
 */
class CtrlLectoras extends CtrlLectora{

    /**
     * Consulta de Lectoras
     * @return object Devuelve un registro como objeto
     */
    function getLectoras(){
        $ret = array();
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idlectoraBuscador"]){
            $in = null;
            foreach ($_REQUEST["idlectoraBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and l.idlectora in (".$in.")";
        }
        
        $sql = "SELECT  /*start*/
                        l.idlectora,
                        c.ipv4,
                        c.nodo,
                        l.nombre,
                        l.numero,
                        p.puerta,
                        l.canal,
                        l.estado
                        /*end*/
                FROM ctrl_lectora l,
                     ctrl_puerta p,
                     ctrl_controladora c
                where l.idcontroladora = c.idcontroladora
                  and c.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."')
                  and l.idpuerta = p.idpuerta
                  ".$arg."
                    order by c.ipv4,c.nodo,l.numero";
        //echo "<div align='left'><pre>".paginationSQL::setSql($sql)."</pre></div>";        
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de los numeros existentes de lectoras
     * @return object Devuelve registros como objeto
     */
    function getNumerosLectoras($idcontroladora) {
        $sql = "select numero from  ctrl_lectora where idcontroladora = '".$idcontroladora."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }        
}
?>
