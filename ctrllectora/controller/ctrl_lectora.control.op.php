<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CTRLLEC"); //Categoria del modulo
require_once "ctrl_lectora.control.php";// Class CONTROL ControlCtrlLectora()

/**
 * Description
 * @author David Concepcion
 */
class ControlOpCtrlLectora extends ControlCtrlLectora{
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCtrlLectora(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        //------------------ Metodo Set  -----------------//
        if(!$this->setCtrlLectora()) return false;
        
        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCtrlLectora(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCtrlLectora()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Lectora"); 
        
        $this->setCampos("idcontroladora","Controladora");
        $this->setCampos("idpuerta","Puerta");
        $this->setCampos("numero","N&uacute;mero");
        $this->setCampos("canal","Canal");
        $this->setCampos("estado","Estado");
        $this->setCampos("nombre","Nombre");
        $this->setCampos("descripcion","Descripci&oacute;n");
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idcontroladora","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"numero","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idpuerta","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombre","tipoDato"=>"esAlfaNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"descripcion","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"canal","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"estado","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

    /**
     * Proceso de sincronizacion para armar y desarmar lectoras de acceso
     * @return boolean devuelve verdadero si el proceso se ejecuta exitosamente
     */
    function lectoraAcc(){

        //------------------ Sincronizar -----------------//
        $sinc = new ControlOpCtrlSincronizar();
        if(!$sinc->setOpCtrlSincronizar()){
            $this->mensaje = $sinc->mensaje;
            return false;
        }

        $this->mensaje = "La petici&oacute;n de ".$this->getAccion();
        if ($_REQUEST["all"]=="true"){
            $this->mensaje .= " todas las lectoras ";
        }else{
            $this->mensaje .= " lectora ";
        }
        $this->mensaje .= "fu&eacute; enviada exitosamente...";

        return true;

    }
}
?>