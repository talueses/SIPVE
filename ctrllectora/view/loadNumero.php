<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Proceso Ajax para cargar los numeros de lectoras segun la controladora seleccionada
 * @author David Concepcion
 */

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_lectora.control.op.php";// Class CONTROLLER

//sleep(1);

$obj = new CtrlLectoras();

// ------ Carga de numeros de lectoras sin los numeros ya registrados ----------//
$numExist = 0;
if ($_REQUEST["idcontroladora"]){
    $numExist = $obj->getNumerosLectoras($_REQUEST["idcontroladora"]);    
}

$nStr  = "<select name=\"numero\" id=\"numero\" onchange=\"setCanal(this)\" >\n";
if ($_REQUEST["idcontroladora"]){
    for ($i=1;$i<=16;$i++){
        $chkNum = false;
        if (count($numExist)>0){
            foreach ($numExist as $row){
                if ($row->numero == $i){
                    $chkNum = true;
                }
            }
        }        
        if (!$chkNum || $_REQUEST["numero"] == $i){
            $nStr .= '   <option value="'.$i.'" '.ControlCtrlLectora::busca_valor($_REQUEST["numero"], $i ).'>'.$i.'</option>\n';
        }
    }
}else{
    $nStr .= "   <option value=\"\">Seleccione</option>\n";
}
$nStr .= "</select>\n";

echo json_encode(array("numero"=>$nStr));
?>