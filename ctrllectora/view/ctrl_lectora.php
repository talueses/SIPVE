<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de horarios
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/ctrl_lectora.control.op.php";// Class CONTROLLER

$obj  = new CtrlLectoras();
$data = $obj->getLectoras();

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>CtrlLectoras</title>
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <style type="text/css" >
            #tabla{
                float:left;
                width:30%;
                height:95%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
            div#buttonsOptions {
                width: 250px;
                padding: 9px;
                border:#c0c0c0 dashed 2px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
            });
            function visualizar(fila){
                $('#idlectora').val(fila.id);
                $('#accion').val('visualizar');
                $('#f1').attr('action','ctrl_lectora.Acc.php');
                $('#f1').submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    $('#'+fila.id).addClass('chequeada');
            }

            function omover(fila){                
                    if (fila.id===filaseleccionada.id ) return false;                                        
                    $('#'+fila.id).addClass('pasada');
            }
            
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;                    
                    $('#'+fila.id).removeClass('pasada');
            }
            function Accion(acc){
                $('#accion').val(acc);
                $('#f1').attr('action','ctrl_lectora.Acc.php');

                if (acc == "modificar" || acc == "eliminar" || acc == "enviarArchivo"){
                    if ($('#idlectora').val()==""){
                        alert("Debe seleccionar un registro de la lista");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("\xbf Esta seguro que desea eliminar el registro ?")){
                            return false;
                        }
                        $('#f1').attr('action','ctrl_lectora.Op.php');
                    }
                }

                $('#f1').submit();
                return true;
            }
            function lectoraAcc(op){

                if ($('#idlectora').val() == "" && !$('#all').attr('checked')){
                    alert("Debe seleccionar un registro de la lista");
                    return false;
                }

                if ($('#all').attr('checked')==true && !confirm('\xbf Esta seguro de '+op+' todas las lectoras ?')){
                    return false;
                }
                $('#accion').val(op);
                $('#f1').attr('action','ctrl_lectora.Op.php');
                $('#f1').submit();
                return true;
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="cargando" align="center" style="position: absolute;background-color:#000000;opacity:.7;width: 100%;height: 100%;vertical-align: middle;color: #FFFFFF;font-size: 20px;display: none">
            <br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<img src="../images/loading51.gif" border="0"><br>&nbsp;<b>Cargando...</b>
        </div>
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <div id="tabla" align="center">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                    echo Controller::$mensajePermisos;
                }else{
                    
                    echo Buscador::getObjBuscador("l.idlectora", "concat(c.ipv4,' - ',c.nodo) as controladora,l.nombre", "ctrl_lectora l,ctrl_controladora c", "where l.idcontroladora = c.idcontroladora order by c.ipv4,c.nodo,l.nombre");
                    ?>
                    <table id="listado">
                            <tr>
                                <td id="titulo" colspan="5" align="center"><b>Lectoras</b></td>
                            </tr>
                            <tr>
                                <th align="center" width="10%">N&deg;</th>
                                <th>Controladora</th>
                                <th>Lectora </th>
                                <th title="N&uacute;mero de Lectora">N&deg;</th>
                                <!--th title="Estado de Comunicaci&oacute;n">&nbsp;</th-->

                            </tr>
                            <?php
                            if (count($data) > 0){
                                foreach ($data as $key => $row){?>
                                    <tr id="<?php echo $row->idlectora;?>" onclick="visualizar(this);oclic(this)" onmouseover="omover(this)" onmouseout="omout(this)">
                                        <td align="center"><b><?php echo paginationSQL::$start+$key+1;?></b></td>
                                        <td><b><?php echo $row->ipv4." - ".$row->nodo;?></b></td>
                                        <td><?php echo $row->nombre;?></td>
                                        <td title="N&uacute;mero de Lectora" align="center"><?php echo $row->numero;?></td>
                                        <?php
                                        switch ($row->estado){
                                            case "0":
                                                $fileImg = "Bullet-Red-16.png";
                                                $title = "Fuera de Linea";
                                                break;
                                            case "1":
                                                $fileImg = "Bullet-Green-16.png";
                                                $title = "En Linea";
                                                break;
                                            case "2":
                                                $fileImg = "Bullet-Yellow-16.png";
                                                $title = "No se pudo establecer la conexi&oacute;n";
                                                break;
                                        }
                                        ?>
                                        <!--td align="center" title="<?php echo $title;?>">                                            
                                            <img src="../images/<?php echo $fileImg;?>" title="<?php echo $title;?>" alt="" border="0"/>
                                        </td-->
                                    </tr>
                                <?php
                                }
                            }else{
                                ?>
                                <tr>
                                    <td colspan="5">
                                        <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                    </td>
                                </tr>

                                <?php
                            }
                            ?>

                    </table>
                    <?php echo paginationSQL::links();?>
                    <div id="botones" >
                        <center>
                            <form name="f1" id="f1" target="frm1" action="ctrl_lectora.Acc.php" method="POST">
                                <input type="hidden" name="idlectora" id="idlectora" value="">
                                <input type="hidden" name="accion" id="accion" value="">
                                <?php
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"agregar")){
                                    ?><input type="submit" value="Agregar" onclick="return Accion('agregar')"  class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"modificar")){
                                    ?><input type="submit" value="Modificar" onclick="return Accion('modificar')" class="boton"><?php
                                }
                                if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"eliminar")){
                                    ?><input type="button" value="Eliminar" onclick="return Accion('eliminar')" class="boton"><?php
                                }
                                ?>
                                <input type="submit" value="Ayuda" 	onclick="this.form.action='ctrl_lectora.Ayuda.php'" class="boton">
                                <br />&nbsp;<br />
                                <div id="buttonsOptions" class="ui-corner-all" >
                                    <?php
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"armar")){
                                        ?>
                                         <button type="button" style="font-size: 10px;" onclick="lectoraAcc('armar')">
                                             <img src="../images/Comment-Check-UI-48.png" alt="" style="margin-bottom: 5px;" />
                                            <br />
                                            Armar Lectora
                                        </button>
                                        <?php
                                    }
                                    echo "&nbsp;&nbsp;";
                                    if (Controller::chkPermiso($_SESSION["usuario"],CATEGORIA, "desarmar")){
                                        ?>
                                        <button type="button" style="font-size: 10px;" onclick="lectoraAcc('desarmar')">
                                            <img src="../images/Comment-Delete-UI-48.png" alt=""  style="margin-bottom: 5px;" />
                                            <br />
                                            Desarmar Lectora
                                        </button>
                                        <?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"abrir") || Controller::chkPermiso($_SESSION["usuario"],CATEGORIA, "cerrar")){
                                        ?><br><input type="checkbox" name="all" id="all" value="true"  >Todas las Lectoras<?php
                                    }
                                    ?>
                                </div>
                                <br /> &nbsp;
                            </form>
                        </center>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                <iframe name="frm1" id="frm1" frameborder="0" scrolling="no" style="width:100%; height:650px" src="ctrl_lectora.Acc.php?accion=agregar"></iframe>
            </div>
        </div>s
    </body>
</html>