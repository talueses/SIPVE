<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_lectora.control.op.php";// Class CONTROLLER

if ($_REQUEST["accion"] == "modificar" || $_REQUEST["accion"] == "visualizar"){
    $obj = new CtrlLectoras();
    $data = $obj->getCtrlLectora($_REQUEST["idlectora"]);

    if (!$data ){
        echo $obj->getMensaje();
    }
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.textarea.maxlength.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 500px;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit" ).button();
                if ($('#accion').val()!="visualizar"){
                    loadNumero();
                }
                
            });
            function setNumero(obj){
                var from = 1;
                var to = 8;
                if (obj.value==2){
                    from +=8;
                    to +=8;
                }

                var txt='<option value="">Seleccione</option>';
                for (i=from;i<=to;i++){                        
                    txt += '<option value="'+i+'">'+i+'</option>';
                }
                $('#numero').html(txt);
            }
            function setCanal(obj){
                if (obj.value<9){
                    $('#canal').val(1);
                }
                if (obj.value>8){
                    $('#canal').val(2);
                }
            }
            function loadNumero(){
                $.ajax({
                    beforeSend: function(){
                        $('#divNumero, #divPuerta').html('<img src="../images/loading51.gif" alt="" width="16" /> Cargando...');
                    },
                    type: 'POST',
                    dataType:'json',
                    url: 'loadNumero.php',
                    data: 'idcontroladora='+$('#idcontroladora').val()+'&numero=<?php echo $data->numero?>'+'&idpuerta=<?php echo $data->idpuerta?>',
                    success: function(data){
                        $('#divNumero').html(data.numero);
                    }
                });
            }
            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Lectora</div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="ctrl_lectora.Op.php" target="ifrm1">
                    <div id="datos"  align="center">
                        <table>
                            <tr title="Controladora">
                                <td align="right">Controladora:</td>
                                <td>
                                    <select name="idcontroladora" id="idcontroladora" onchange="loadNumero()" <?php echo $disabled;?> >
                                        <option value="">Seleccione</option>
                                        <?php echo ControlCtrlLectora::make_combo("ctrl_controladora ", "where idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') order by ipv4,nodo", "idcontroladora", "ipv4,'Nodo: '||nodo as nodo", $data->idcontroladora,true);?>
                                    </select>
                                </td>
                            </tr>
                            <tr title="N&uacute;mero">
                                <td align="right">N&uacute;mero:</td>
                                <td>
                                    <div id="divNumero">
                                        <select name="numero" id="numero" onchange="setCanal(this)" <?php echo $disabled;?> >                                            
                                            <?php
                                            echo '<option value="'.$data->numero.'" selected>'.$data->numero.'</option>';
                                            ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr title="Puerta">
                                <td align="right">Puerta:</td>
                                <td>
                                    <select name="idpuerta" id="idpuerta" <?php echo $disabled;?>>
                                        <option value="">Seleccione</option>
                                        <?php echo ControlCtrlLectora::make_combo("ctrl_puerta", "where idpuerta not in (select idpuerta from ctrl_lectora where idpuerta not in ('".($data->idpuerta?$data->idpuerta:"0")."'))  order by cast(numero as :cast_int)", "idpuerta", "numero||' - '||puerta as num_puerta", $data->idpuerta,false);?>
                                    </select>
                                </td>
                            </tr>
                            <?php
                            if ($_REQUEST["accion"]!="agregar"){
                                /*?>
                                <tr title="Estado de conexi&oacute;n a la controladora">
                                    <td align="right" >Estado:</td>
                                    <td valign="top">
                                        <?php
                                        switch ($data->estado){
                                            case "0":
                                                $fileImg = "Bullet-Red-20.png";
                                                $title = "Fuera de Linea";
                                                break;
                                            case "1":
                                                $fileImg = "Bullet-Green-20.png";
                                                $title = "En Linea";
                                                break;
                                            case "2":
                                                $fileImg = "Bullet-Yellow-20.png";
                                                $title = "No se pudo establecer la conexi&oacute;n";
                                                break;
                                        }
                                        ?>
                                        <img style="float: left" src="../images/<?php echo $fileImg;?>" title="<?php echo $title;?>" alt="" border="0"/>
                                    </td>
                                </tr>
                                <?*/
                            }
                            ?>
                            <tr title="Nombre">
                                <td align="right">Nombre:</td>
                                <td>
                                    <input type="text" name="nombre" id="nombre" value="<?php echo $data->nombre;?>" <?php echo $disabled;?>>
                                </td>
                            </tr>
                            <tr title="Breve descripci&oacute;n de la lectora">
                                <td align="right" valign="top" >Descripci&oacute;n:</td>
                                <td>
                                    <div class="divTextarea" >
                                        <textarea name="descripcion" id="descripcion" <?php echo $disabled;?> rows="6" cols="28" maxlength="250" <?php echo $disabled;?>><?php echo $data->descripcion;?></textarea>
                                        <div class="charLeftDiv"  align="right"><input type="text" class="charLeft" id="charLeft_descripcion" size="4" readonly > Caracteres Restantes</div>
                                    </div>
                                </td>
                            </tr>


                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="idlectora" id="idlectora" value="<?php echo $data->idlectora ?>">
                        <input type="hidden" name="canal" id="canal" value="<?php echo !$data->canal?"1":$data->canal?>">
                        <input type="hidden" name="estado" id="estado" value="<?php echo $data->estado?$data->estado:"0" ?>">
                            <?php
                            if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                echo "<input type=\"submit\" value=\"".ucfirst($_REQUEST["accion"])." Lectora\"  />"; //onclick=\"$('#cargando',parent.document).fadeIn('slow');\"
                            }
                            ?>
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>