<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';

/**
 * Clase 
 * @author David Concepcion CENIT-DIDI
 */
class ModelMonitoracceso {
    /**
     * Consulta de Log de Acceso
     * @return object Devuelve un registro como objeto
     */
    function getLog(){
        $sql = "select lg.id as logID,
                       lg.dirnodofuente,
                       lg.nodolectora,
                       lg.numeropuerta,
                       lg.revision,
                       lg.indiceusuario,
                       lg.valortransferido,
                       lg.numerotarjeta,
                       lg.numeroaltotarjeta,
                       lg.numerobajotarjeta,
                       lg.codigofuncion,
                       lg.subcodigo,
                       lg.subfuncion,
                       lg.codigoext,
                       lg.nivelusuario,
                       lg.anio,
                       lg.dia,
                       lg.mes,
                       lg.diasemana,
                       lg.hora,
                       lg.minuto,
                       lg.segundo,
                       lg.log,
                       lg.fcreado,
                       lg.idsincronizar,
                       cast(concat(lg.anio,'-',lg.mes,'-',lg.dia,' ',lg.hora,':',lg.minuto,':',lg.segundo) as :cast_datetime)as fechaEvento,
                       cu.idusuario,
                       cu.tipo_tarjeta,                       
                       concat(cu.nombres,' ',COALESCE(cu.apellidos))as usuarioTrj,
                       cp.puerta,
                       (select 1 from car_visitante cv where cv.numero = cu.numero_visitante) as hasCarnetVisitante,
                       ce.idcarnet_empleado as hasCarnetEmpleado,
                       ce.archivo_foto
                from ctrl_logacceso lg
                        left join ctrl_usuario cu on (lg.indiceusuario = cu.numero)
                        left join car_empleado ce on (ce.cedula = cu.cedula)
                        left join ctrl_puerta cp on (lg.numeropuerta = cast(cp.numero as :cast_int))
		where lg.idsincronizar is null
                     and cast(concat(lg.anio,'-',lg.mes,'-',lg.dia,' ',lg.hora,':',lg.minuto,':',lg.segundo) as :cast_datetime) between '".date("Y-m-d 00:00:00")."' and '".date("Y-m-d 23:59:59")."'                         
                    order by cast(concat(lg.anio,'-',lg.mes,'-',lg.dia,' ',lg.hora,':',lg.minuto,':',lg.segundo) as :cast_datetime) desc
                    ";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de detalle de log
     * @param int $logID Codigo de log
     * @return object Devuelve registros como objeto
     */
    function getLogDetalle($logID){
       
        $sql = "select lg.id as logID,                       
                       cast(concat(lg.anio,'-',lg.mes,'-',lg.dia,' ',lg.hora,':',lg.minuto,':',lg.segundo) as datetime)as fechaEvento,
                       lg.codigofuncion,
                       lg.log,
                       cu.idusuario,
                       cu.tipo_tarjeta,
                       cu.cedula,
                       cu.numero_visitante,
                       cu.telefono,
                       cu.email,
                       cu.tarjetayopin,
                       cu.codigo_id,
                       cu.codigo_sitio,
                       cu.codigo_tarjeta,
                       concat(cu.nombres,' ',ifnull(cu.apellidos,''))as usuarioTrj,
                       cg.grupo,
                       cz.descripcion as horario,
                       cd.departamento_nombre,
                       cp.puerta,                       
                       (select 1 from car_visitante cv where cv.numero = cu.numero_visitante) as hasCarnetVisitante,
                       ce.idcarnet_empleado as hasCarnetEmpleado,
                       ce.archivo_foto
                from ctrl_logacceso lg
                        left join ctrl_usuario cu      on (lg.indiceusuario = cu.numero)
                        left join ctrl_zonadetiempo cz on (cu.idzonadetiempo = cz.indice)
                        left join car_departamento cd  on (cu.iddepartamento = cd.iddepartamento)
                        left join ctrl_grupo cg        on (cu.idgrupo = cg.idgrupo)
                        left join car_empleado ce      on (ce.cedula = cu.cedula)                        
                        left join ctrl_puerta cp       on (lg.numeropuerta = cp.numero)
		where lg.id ='".$logID."'
                    ";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();    
    }
}
?>
