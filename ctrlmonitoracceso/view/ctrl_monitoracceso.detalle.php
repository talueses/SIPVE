<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 07-06-2012 CENIT-DIDI
 * Detalle de log de acceso
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/ctrl_monitoracceso.control.op.php";

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

$obj = new ModelMonitoracceso();

$data = $obj->getLogDetalle($_REQUEST["logID"]);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>        
        <style type="text/css" >
            body{
                font-size: 100%;
                margin:0px;
                background-color: #fff
            }
            
            #tabla{
                width:99%;                
                margin: 6px 1px 0px 1px;
            }            
            img.foto{      
                width: 150px;
                border: #000000 1px dashed;                
            }
            table{
                font-size: 15px;
            }
            table.tdStaticWidth td:first-child{
                width: 90px;
            }
        </style>
    </head>
    <body>
        <center> 
                <table id="tabla" class="ui-corner-all ui-state-active">
                    
                    <tr>
                        <td rowspan="10" width="150" align="center" valign="top">
                            <img class="foto ui-corner-all" src="<?php echo $data->archivo_foto?"../../carempleado/images/fotos/".$data->archivo_foto:"../images/User_64.png"?>" alt="Foto" />
                        </td>                        
                        <th class="ui-state-default ui-corner-all"><b>Datos Personales</b></th>
                    </tr>
                    <tr>
                        <td align="left">
                            <?php 
                            echo $data->usuarioTrj."&nbsp;&nbsp;&nbsp;";
                            
                            if ($data->tipo_tarjeta=="empleado"){
                                ?><img src="../images/User-32.png" alt="E" width="16" title="Empleado" /> <?php
                            }
                            if ($data->tipo_tarjeta=="visitante"){
                                ?><img src="../images/User-Gray-Go-32.png" alt="V" width="16" title="Visitante" /> <?php
                            }
                            if ($data->hasCarnetEmpleado && $data->tipo_tarjeta=="empleado" || ($data->hasCarnetVisitante && $data->tipo_tarjeta=="visitante")){
                                ?><img src="../images/Carnet-32.png" alt="Carnet" title="Carnet Asignado" width="16" /><?php
                            }
                            
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">                           
                            <?php 
                            if ($data->tipo_tarjeta=="empleado"){
                                echo "C.I. ".number_format($data->cedula, 0, "", ".");
                                        
                            }
                            if ($data->tipo_tarjeta=="visitante"){
                                echo "N&deg; ".sprintf('%04d',$data->numero_visitante);
                            }                            
                            ?>                            
                        </td>
                    </tr>
                    <tr>
                        <td align="left"><?php echo $data->departamento_nombre?></td>
                    </tr>
                    <?php
                    if ($data->tipo_tarjeta=="empleado"){
                        ?>
                        <tr>
                            <td align="left">
                                <?php echo $data->telefono;?>
                            </td>
                        </tr>                    
                        <tr>
                            <td align="left">
                                <?php echo $data->email;?>
                            </td>
                        </tr>
                        <?php
                    }                    
                    ?>                    
                    <tr>
                        <th class="ui-state-default ui-corner-all"><b>Datos Tarjeta</b></th>
                    </tr>
                    <tr>
                        <td align="left">
                            <table class="tdStaticWidth">
                                <tr>
                                    <td class="ui-state-focus ui-corner-tl">Modo Acceso</td>
                                    <td><?php echo ControlOpCtrlMonitoracceso::$modoAcceso[$data->tarjetayopin]; ?></td>
                                </tr>
                                <tr>
                                    <td class="ui-state-focus">Codigos</td>
                                    <td><?php echo $data->codigo_id." - ".$data->codigo_sitio.":".$data->codigo_tarjeta; ?></td>
                                </tr>
                                <tr>
                                    <td class="ui-state-focus">Grupo Puertas</td>
                                    <td><?php echo $data->grupo; ?></td>
                                </tr>
                                <tr>
                                    <td class="ui-state-focus ui-corner-bl">Horario</td>
                                    <td><?php echo $data->horario; ?></td>
                                </tr>
                            </table>                            
                        </td>
                    </tr>
                    <tr>
                        <th class="ui-state-default ui-corner-all"><b>Datos Acceso</b></th>
                    </tr>
                    <tr>
                        <td align="left">
                            <table class="tdStaticWidth">
                                <tr>
                                    <td class="ui-state-focus ui-corner-tl">Fecha</td>
                                    <td><?php echo Controller::formatoFecha($data->fechaEvento)?></td>
                                </tr>
                                <tr>
                                    <td class="ui-state-focus">Puerta</td>
                                    <td><?php echo $data->puerta; ?></td>
                                </tr>
                                <tr>
                                    <td class="ui-state-focus ui-corner-bl">Detalle</td>
                                    <td>
                                        <span style="<?php echo in_array($data->codigofuncion, ControlOpCtrlMonitoracceso::$error_codes)?"font-weight: bold;color: #cd0a0a":"" ?>">
                                            <?php 
                                            if ($data->codigofuncion=="56"){
                                                echo "Fingerprint image failed";
                                            }else{
                                                echo $data->log;
                                            }                            
                                            ?>
                                        </span>
                                    </td>
                                </tr>
                            </table>                            
                        </td>
                    </tr>
                </table>
            
        </center>
    </body>
</html>