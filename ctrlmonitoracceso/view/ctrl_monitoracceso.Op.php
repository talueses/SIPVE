<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 *  Carga de log por Ajax 
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/ctrl_monitoracceso.control.op.php";// Class CONTROLLER

$obj = new ModelMonitoracceso();
$data = $obj->getLog();
?>
<script type="text/javascript" language="javascript">
    
    /* Cabecera flotante de la tabla */
    var tableOffset;
    var $header;
    var $fixedHeader;
    $(function() {
        $( "input:button, input:submit,button" ).button();
        tableOffset = $("#listado").offset().top;        
        $header = $("#listado > thead").clone();
        $fixedHeader = $("#header-fixed").append($header);
        fixHeader();
        $('#logContent').bind("scroll", function() {            
            fixHeader();
        });
        
        // --- Esconder boton PopUp ---//
        if (window.name=='monitorAcceso'){            
            $('#popUpImg').hide();
        }
        
    });
    function fixHeader(){
        var offset = $('#logContent').scrollTop();
        
        if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
            $fixedHeader.show();
        }
        else if (offset < tableOffset) {
            $fixedHeader.hide();
        }
    }
</script>
<style type="text/css">
    #imgStatus{
        position: absolute;        
        margin-top: 2px;
    }
</style>
<div id="tabla" align="center" >    
    
    <div id="loadinImg"></div>
    <div id="popUpImg">
        <button type="button">
            <img src="../images/Gnome-Preferences-System-Windows-32.png" alt="PopUp" title="Abrir en otra ventana" width="20"/>
        </button>
    </div>
    <table id="header-fixed" align="center" class="ui-corner-all ui-widget-content"></table>
    
    <table id="listado" align="center" class="ui-corner-all ui-widget-content">
        <thead>
        <tr>
            <td id="titulo" class="ui-widget-header ui-corner-all" colspan="6" align="center">
                
                <b>Listado de Eventos </b>
                <?php 
                $status = shell_exec("../../inicio/shell_script/ctrlAccProcess.sh status");

                if (preg_match("/Running/", $status)){
                    $src = "Bullet-Green-16.png";
                    $title = "Proceso en ejecuci&oacute;n";                        
                }else{                        
                    $src = "Bullet-Red-16.png";
                    $title = "Proceso detenido";
                }                    
                ?>
                <img src="../images/<?php echo $src;?>" alt="" title="<?php echo $title;?>" id="imgStatus">
                <small class="comment">Del d&iacute;a en curso (<?php echo date("d-m-Y")?>)</small>
            </td>
        </tr>
        <tr>
            <th class="ui-state-default ui-corner-left" width="12%">Fecha</th>
            <th class="ui-state-default" width="18%">Puerta</th>
            <th class="ui-state-default" width="14%">Nombre</th>
            <th class="ui-state-default ui-corner-right" width="46%">Detalle</th>
        </tr>
        </thead>
        <tbody>
        <?php                        
        if (count($data) > 0){
            
            foreach ($data as $key => $row){
                ?>
                <tr id="<?php echo $row->logID;?>" onmouseover="omover(this)" onmouseout="omout(this)"  idusuario="<?php echo $row->idusuario?>">
                    
                    <td align="center" class="ui-corner-left "><?php echo $row->fechaEvento==""?"&nbsp;":Controller::formatoFecha($row->fechaEvento);?></td>
                    <td><?php echo $row->puerta==""?"&nbsp;":$row->puerta;?></td>
                    <td>
                        <?php 
                        if ($row->tipo_tarjeta=="empleado"){
                            ?><img src="../images/User-32.png" alt="E" width="16" title="Empleado" /> <?php
                        }
                        if ($row->tipo_tarjeta=="visitante"){
                            ?><img src="../images/User-Gray-Go-32.png" alt="V" width="16" title="Visitante" /> <?php
                        }
                        echo $row->usuarioTrj==""?"&nbsp;":$row->usuarioTrj;
                        if ($row->hasCarnetEmpleado && $row->tipo_tarjeta=="empleado" || ($row->hasCarnetVisitante && $row->tipo_tarjeta=="visitante")){
                            ?><img src="../images/Carnet-32.png" alt="Carnet" title="Carnet Asignado" width="16" style="float: right;" /><?php
                        }
                        ?>
                    </td>
                    <td class="ui-corner-right">
                        <span style="<?php echo in_array($row->codigofuncion, ControlOpCtrlMonitoracceso::$error_codes)?"font-weight: bold;color: #cd0a0a":"" ?>">
                            <?php 
                            if ($row->codigofuncion=="56"){
                                echo "Fingerprint image failed";
                            }else{
                                echo $row->log;
                            }                            
                            ?>
                        </span>&nbsp;
                    </td>
                </tr>
                <?
            }
        }else{
            ?>
            <tr>
                <td colspan="6">
                    <div id="divmensaje" style="width:99%;border:2px solid red" class="ui-corner-all" align="center">No se encontraron registros</div>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    
</div>