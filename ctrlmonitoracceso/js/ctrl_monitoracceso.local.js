var loadingMsg = '<img src="../images/loading51.gif" alt="Cargando..." title="Cargando..." width="16"  class="ui-corner-all ui-widget-header"/>';
var width      = screen.width ;
var height     = screen.height ;
var popUpWindow;
var timerGetLog;
var openerURL;
$(function() {
    
    $( "input:button, input:submit,button" ).button();
    
    timerGetLog = startGetLog();
    
    // --- Caja de Dialogo para captura o carga de foto --- //
    $("#loadDetalle").dialog({
       autoOpen: false,
       resizable: false,
       draggable: false,
       width: 600,
       height:400,
       position:['center',70],
       modal:true,
       open: function(event, ui){
           var d = new Date();
           $("#frameDetalle").attr('src','ctrl_monitoracceso.detalle.php?logID='+$('#logID').val()+'&'+d.getMilliseconds());
       },
       close: function(event, ui) {
           $('#frameDetalle').attr('src','');
       }
    });
    
    // --- Open dialogo detalle acceso --- //
    $('table#listado tbody tr').live({
        click: function() {            
            
            $("#logID").val($(this).attr('id'));
            
            if ($(this).attr('idusuario')!=""){
                $("#loadDetalle").dialog('open');
            }
        }
    });
    // --- PopUp --- //
    $('div#popUpImg button').live({
        click: function() { 
            
            //clearTimeout(timerGetLog);
            clearInterval(timerGetLog);
            var str;            
            str  = '<center>';
            str += '    <div id="tabla" class="ui-corner-all ui-widget-content">';
            str += '        <div class="ui-widget ui-corner-all info" style="margin-top: 20px; padding: 0 .7em;">';
            str += '            <span class="icon-info " style="float: left; margin-right: .3em;"></span>';
            str += '            <p>Para cargar el listado aqu&iacute; debe cerrar la ventana emergente.</p>';            
            str += '        </div>';
            str += '    </div>';
            str += '</center>';
            
            $('#logContent').html(str);    
            
            var d = new Date();            
            popUpWindow = window.open("ctrl_monitoracceso.php?"+d.getMilliseconds() , "monitorAcceso" , "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes,width="+width+",height="+height);            
            $(popUpWindow).unload(function(){
                if(this.location == 'about:blank'){
                    $(this).unload(function(){
                        timerGetLog = startGetLog();
                    });
                }
            });            
        }
    });
    
    
    
    if (window.opener){
        openerURL = window.opener.location.href
        setInterval(function(){
            if (openerURL!=window.opener.location.href){ 
                window.close();
            }
        },1000);
    }  
});


function startGetLog(){
    return setInterval(function(){
        getLog();
    },1000);
}

function getLog(){
    
    //var d = new Date(); // Objeto fecha para evitar cache en el Ajax
    //d.getMilliseconds()
    $.ajax({
        url: 'ctrl_monitoracceso.Op.php',
        type: "POST",
        beforeSend: function(){                    
            $('#loadinImg').html(loadingMsg).show();
        },
        success: function(html){                        
            $('#loadinImg').html('').hide();
            
            if ($('#cargando')){
                
                $('#cargando').fadeOut(500);
                setTimeout(function(){
                    $('#logContent').html(html);    

                },500);
                
                return false;
            }

            $('#logContent').html(html);

        }
    });    
}
filaseleccionada = {};
function omover(fila){
    if (fila.id===filaseleccionada.id ) return false;    
    $('#'+fila.id).addClass('pasada');
    
}
function omout(fila){
    if (fila.id===filaseleccionada.id ) return false;    
    $('#'+fila.id).removeClass('pasada');
}
