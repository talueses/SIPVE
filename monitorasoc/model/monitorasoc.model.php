<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once '../model/monitorasoc_vid_zona_usuario.tabla.php';
/**
 * Clase Zonas{}
 * @author David Concepcion 11-10-2010 CENIT
 */
class   MonitorAsoc extends Vid_zona_usuario{
    /**
     * Consulta de usuarios
     * Solo se listan los usuarios pertenecientes al segmento del usuario logueado
     * @return object Devuelve un registro como objeto
     */
    static function getUsuarios(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["usuarioBuscador"]){
            $in = null;
            foreach ($_REQUEST["usuarioBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " and u.usuario in (".$in.")";
        }
        
        $ret = array();
        $sql = "SELECT /*start*/ distinct u.* /*end*/
                FROM usuario u, segmento_usuario su 
                where u.usuario = su.usuario and idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."') ".$arg." order by u.usuario, u.nombre, u.apellido";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de zonas asociadas segmentos de un usuario
     * Las zonas estan asociadas a camaras
     * Las camaras estab asociadas a servidores
     * Los segmentos estan asociados a servidores
     * Los segmentos estan asociados a usuarios
     * Solo se listan las zonas pertenecientes al segmento del usuario logueado
     * @param string $usuario Login del usuario
     * @return object Devuelve registros como objeto
     */
    static function getSegmentosZonasUsuario($usuario){
        $sql = "select seg.idsegmento,
                       seg.segmento,
                       seg.descripcion as \"segmentoDesc\",
                       vz.idzona,
                       vz.zona,
                       vz.descripcion as \"zonaDesc\"
                from segmento_usuario \"segUsr\",
                     segmento seg,
                     vid_servidor vsrv,
                     vid_camara vc,
                     vid_zona_camara vzc,
                     vid_zona vz
                where \"segUsr\".usuario = '".$usuario."'                  
                  and \"segUsr\".idsegmento = seg.idsegmento
                  and seg.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."')
                  and seg.idsegmento = vsrv.idsegmento
                  and vsrv.idservidor = vc.idservidor
                  and vc.idcamara = vzc.idcamara
                  and vzc.idzona = vz.idzona
                    group by seg.idsegmento,vz.idzona
                    order by seg.segmento, vz.zona";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Consulta de zonas asociadas a un usuario
     * @param string $usuario Login del usuario
     * @return object Devuelve registros como objeto
     */
    static function getZonasUsuario($usuario){
        $sql = "select * from vid_zona_usuario where usuario = '".$usuario."'";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    public function eliminarZonasUsuario($usuario){
        $sql = "DELETE FROM vid_zona_usuario  WHERE usuario = '".$usuario."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar
    
}

?>
