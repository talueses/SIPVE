<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDASCMNTR"); //Categoria del modulo
require_once "../model/monitorasoc.model.php"; // Class MODEL Zonas()
require_once "../controller/monitorasoc.controller.php";// Class CONTROL Controller()

/**
 * Description
 * @author David Concepcion
 */
class ControlAsoc extends Controller{
    /**
     * @var string mensaje de exito o error
     */
    var $mensaje = null;
    /**
     * @var string accion agregar, modificar o eliminar dato
     */
    var $accion  = null;

    /**
     * Establece la acción
     * @param string $accion Acción
     */
    public function setAccion($accion){
         $this->accion = $accion;
    }

    /**
     * @return string Devuelve la accion establecida
     */
    public function getAccion(){
         return $this->accion;
    }

    /**
     * Establece las zonas asociadas a un usuario
     * @return boolean Devuelve falso si el proceso de agregar o eliminar falla
     */
    function setZonaUsuario(){
        $zonaUsuario = new MonitorAsoc();

        $zonaUsuario->setUsuario($_POST["usuario"]);

        //-------------- ELIMINAR DATOS ASOCIADOS --------------//
        $exito = $zonaUsuario->eliminarZonasUsuario($zonaUsuario->getUsuario());
        if (!$exito){
            $this->mensaje = "El usuario no se pudo asociar: ".$zonaUsuario->getMensaje();
            return false;
        }

        //--------------------- DATOS --------------------------//
        foreach ($_POST["idzona"] as $row){
            
            $zonaUsuario->setIdzona($row);
            $exito = $zonaUsuario->insertarRegistro();
            if (!$exito){
                $this->mensaje = "El usuario no se pudo asociar: ".$zonaUsuario->getMensaje();
                return false;
            }
        }
        $this->mensaje = "El usuario fu&eacute; asociado exitosamente...";
        return true;
    }
}
?>
