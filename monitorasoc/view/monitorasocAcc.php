<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de accion asociar datos
 * @author David Concepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

require_once "../controller/monitorasoc.control.php";// Class CONTROLLER


$data = MonitorAsoc::getZonasUsuario($_REQUEST["usuario"]);
if (count($data) > 0){
    foreach ($data as $row){
        $equal[] = $row->idzona;
    }
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title><?php echo ucfirst($_REQUEST["accion"]);?> Camara de Video</title>
        
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" media="screen" href="../css/jquery.toChecklist.css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>        
        <script type="text/javascript" src="../js/jquery.toChecklist.js"></script>
        <style type="text/css">
            .smallButton{
                font-size: 10px;
            }
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;                
                
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #datos{
                width: 80%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
		/*$('#idzona').toChecklist({
                    showSelectedItems : true
                });*/
                $('input:text,input:password').css({
                    background: '#fff' ,
                    border: '1px solid #d5d5d5',
                    '-moz-border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    'border-radius': '4px'
                });
                $( "input:button, input:submit" ).button();                
            }); 
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="contenido" align="center">
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px">Zonas asociadas al usuario: <font style="color: #0000FF"><?php echo $_REQUEST["usuario"];?></font></div>
                <br/>
                <div id="divmensaje" style="width:99%;"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0px" height="0px"  scrolling="0"></iframe>

                <form method="POST" name="f1" action="monitorasocOp.php" target="ifrm1">
                    <div id="datos" >
                        <!--
                        <div align="center" class="smallButton">
                            Seleccionar->
                            <input type="button" class="smallButton" onclick="$('#idzona').toChecklist('checkAll');"  value="Todos">
                            <input type="button" class="smallButton" onclick="$('#idzona').toChecklist('clearAll');" value="Ninguno">
                        </div>
                        -->
                        <table>
                            <tr>
                                <td valign="top">
                                    <dl>
                                        <dt><b>Segmentos</b></dt>
                                        <dd>&nbsp;&nbsp;&nbsp;Zonas:</dd>
                                    </dl>
                                </td>
                                <td>
                                    <select name="idzona[]" id="idzona" style="width: 400px;height: 200px" multiple >
                                        <?php
                                        $data = MonitorAsoc::getSegmentosZonasUsuario($_REQUEST["usuario"]);
                                        $nrows = count($data);
                                        if ($nrows > 0){
                                            foreach ($data as $row){
                                                if ($segmento != $row->segmento){
                                                    echo "<optgroup label=\"".$row->segmento." - ".$row->segmentoDesc."\">";
                                                }
                                                echo "<option value=\"".$row->idzona."\" ".ControlAsoc::busca_valor($equal, $row->idzona)." >";
                                                echo "  ".$row->zona." - ".$row->zonaDesc;
                                                echo "</option>\n";
                                                $segmento = $row->segmento;
                                                if ($segmento != $row->segmento){
                                                    echo "</optgroup>";
                                                }
                                            }
                                        }else{
                                            echo "<option value=\"\">No hay registros</option>";
                                        }
                                        ?>
                                    </select>
                                    <br>
                                    <div id="zonasSelected">Zonas seleccionadas</div>
                                    <ul id="idzona_selectedItems"></ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="botones" style="clear:left">
                        <input type="hidden" name="accion" value="<?php echo $_REQUEST["accion"];?>">
                        <input type="hidden" name="usuario" id="usuario" value="<?php echo $_REQUEST["usuario"];?>">
                        <input type="submit" value="Asociar Usuario">
                    </div>
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>