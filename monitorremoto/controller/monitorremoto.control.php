<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "VIDMNTR"); //Categoria del modulo
require_once "../model/monitorremoto.model.php";
require_once "monitorremoto.controller.php";
require_once "../../inicio/controller/camaraCommands.control.php";
/**
 * Clase ControlMonitor{}
 *  => Herencia clase Controller{}
 *     Metodos de gestion de monitor remoto
 * @author David Concepcion 07-10-2010 CENIT
 */
class ControlMonitor extends Controller{
    
    private $data = array();
    private $camaraCommands;
    
    public function __set($name, $value)
    {
        //echo "Estableciendo '$name' igual a '$value'<br>";
        $this->data[$name]=$value;
    }
    public function __get($name)
    {
        //echo "Obteniendo '$name'-'".$this->data[$name]."'<br>";
        return $this->data[$name];
    }
    
    /**
     * Formacion de tags HTML IMG dinamicos
     * @param array $camaras Matriz de datos de camaras
     * @param int $i numero de la camara
     * @param string $size Tamaño de la camara => B:Grande, M: Mediana, S: Pequeña
     * @return string Devuelve objetos HTML
     */
    static function getImg($camaras,$i,$size){
        $id  = null;
        $scr = null;
        // ---------------------- IMAGEN GRANDE 640x480 --------------------- //
        if ($size=="B"){
            $id   =  " class=\"BIG\"";
            $properties = " width=\"640\" height=\"480\" onclick=\"$('#option').val('moveImg');\"";
            $scr = $camaras["ipv4Servidor-".$i];
        }
        // ---------------------- IMAGEN MEDIANA 320x240 -------------------- //
        if ($size=="M"){
            $id   =  " class=\"MEDIUM\"";
            $properties = " width=\"320\" height=\"240\" onclick=\"return mover(this);\" ";
            //$scr = $camaras["ipv4Servidor-".$i];
            $scr = $camaras["ipv4Camara-".$i];
        }
        // ---------------------- IMAGEN PEQUEÑA 160x120 -------------------- //
        if ($size=="S"){
            $id   =  " class=\"SMALL\"";
            $properties = " width=\"160\" height=\"120\" onclick=\"return mover(this);\" ";
            $scr = $camaras["ipv4Camara-".$i];
        }
        
        $properties .= " ipv4Camara=\"".$camaras["ipv4Camara-".$i]."\" ipv4Servidor=\"".$camaras["ipv4Servidor-".$i]."\" mainControlsMethod=\"".$camaras["mainControlsMethod-".$i]."\" extraPanelMethod=\"".$camaras["extraPanelMethod-".$i]."\" rotacion=\"".$camaras["rotacion-".$i]."\" guardRunning=\"".$camaras["guardRunning-".$i]."\" onError=\"errorImg(this);\"";// <<--- PROPIEDADES DINAMICAS
        
        //$str = "<img ".$id." alt=\"".$camaras["title-".$i]."\" title=\"".$camaras["title-".$i]."\" src=\"".$scr."\" name=\"".$camaras["camara-".$i]."\" id=\"".$camaras["camara-".$i]."\" ".$properties."> \n\t\t\t";
        
        
        $str = "<input type=\"image\" ".$id." alt=\"".$camaras["title-".$i]."\" title=\"".$camaras["title-".$i]."\" src=\"".$scr."\" name=\"".$camaras["camara-".$i]."\" id=\"".$camaras["camara-".$i]."\" ".$properties."> \n\t\t\t";
        
        
        /*$str .= "<input type=\"hidden\" id=\"ipv4Camara-".$camaras["camara-".$i]."\" value=\"".$camaras["ipv4Camara-".$i]."\" > \n\t\t\t";
        $str .= "<input type=\"hidden\" id=\"ipv4Servidor-".$camaras["camara-".$i]."\" value=\"".$camaras["ipv4Servidor-".$i]."\" > \n\t\t\t";
        $str .= "<input type=\"hidden\" id=\"nameCamara-".$camaras["camara-".$i]."\" value=\"".$camaras["camara-".$i]."\" > \n\t\t\t";
        $str .= "<input type=\"hidden\" id=\"titleCamara-".$camaras["camara-".$i]."\" value=\"".$camaras["title-".$i]."\" > \n\t\t\t";*/

        $str = "<span id=\"camara-".$camaras["camara-".$i]."\" >\n".$str."\n</span>";

        // --- Botones Panel de Control ---//
        if ($size=="B"){
            $str.= "<br />";
            $str.= "<center>";
            $str.= "<button name=\"mainControlsButton\" id=\"mainControlsButton\"  type=\"button\" onclick=\"$('#mainControls').dialog('open');\" >Panel de Control</button>";
            $str.= "<button name=\"extraPanelButton\" id=\"extraPanelButton\"  type=\"button\" onclick=\"$('#extraPanel').dialog('open');\" >Panel Extra</button>";
            $str.= "</center>";
        }
        return $str;
    }

    /**
     * instanciacion metodo getImg() con separacion de lineas (tag <br>)
     * @param int $plantilla numero de plantilla seleccionada
     * @param int $countData cantidad de camaras asociadas al usuario
     * @param array $camaras Matriz de datos de camaras
     * @param int $nPerLine numero de objetos por linea
     * @return string Devuelve objetos HTML
     */
    static function getImgExtra($plantilla,$countData,$camaras,$nPerLine){
        $str = null;
        $j=1;        
        for ($i = $plantilla;$i<=($countData-1);$i++){            
            $str.= self::getImg($camaras, $i, "S");
            if ($j==$nPerLine){
                $str.= "<br>\n\t\t\t";
                $j=0;
            }
            $j++;
        }
        return $str;
    }

    /**
     * === Actualizacion de evento ===
     * Se actualiza el estatus y la observacion
     * @return boolean Devuelve verdadero si la ejecucion es exitosa
     */
    function updateEvento(){
        //------------------ VALIDACION DE CAMPOS  -----------------//
        if(!$this->esValidoObservacion()) return false;

        $evento = new vid_evento();
        $evento->setIdevento($_POST["idevento"]);
        $evento->setStatus($_POST["status"]);
        $evento->setFile(base64_decode($_POST["file"]));
        $evento->setIdservidor($_POST["idservidor"]);
        $evento->setObservacion($_POST["observacion"]);
        
        $exito = $evento->modificarRegistro($evento->getIdevento());
        if(!$exito){
            $this->mensaje = "Evento no atendido";
        }
        
        $this->mensaje = "Evento atendido exitosamente...";
        return true;
    }
    
    /**
     * === Validacion IdServidor ===
     * @return boolean Devuelve verdadero si la ejecucion es exitosa
     */
    function esValidoObservacion(){
        $observacion = (isset($_POST['observacion']) && trim($_POST['observacion'])!=='')? trim($_POST['observacion']) : null;
        if($observacion===null){
            $this->mensaje = "Falta introducir la Observaci&oacute;n";
            return false;
        }
        return true;
    }

    /**
     * Verifica el estado de captura de eventos de una camara
     * @return boolean Devuelve verdadero si la camara esta activa y vis.
     */
    function getMotionStatus(){
        $camaras = Monitor::getCamarasServidor($_REQUEST["idservidor"]);
        $nThread = 0;
        foreach ($camaras as $row){
            $nThread++;
            if ($row->idcamara == trim($_REQUEST["idcamara"])){
                // ---------- PAUSA DETECCION DE MOVIMIENTO ------------//
                $url = "http://".$row->ipv4Serv.":18080/".$nThread."/detection/status";
                //echo "<br>".$url;
                $resp = self::httpRequest($url, $row->so_usuario, $row->so_clave);
                break;
            }
        }
        //echo $resp;
        if (preg_match("/ACTIVE/", $resp)){
            return true;
        }
        if (!preg_match("/ACTIVE/", $resp)){
            return false;
        }        
    }
    
    /**
     * Envia comandos hacia una camara por httpRequest usando PHP-CURL
     * @param int $idcamara Codigo de la camara
     * @return string Devuelve la respuesta de la ejecucion del comando remoto 
     */
    static function sendComandoCamara($idcamara){        
        $param = Monitor::getCamarasConf($idcamara);
        //echo "<div align='left'><pre>".print_r($param, true)."</pre></div>";
        $response = self::httpRequest($param->ipv4.":".$param->puerto.CamaraCommands::$command,$param->cam_usuario,$param->cam_clave);

        return $response;
    }

}
?>