<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Modificado por David Concepcion 08-09-2010 CENIT
 * Nuevos metodos autogenerados:
 *  - modificarRegistro()
 *  - eliminarRegistro()
 * Escritura de archivos PHP file_put_contents() y asignacion de permisologia chmod()
 */
require_once "dblink.php";
 
class Motionconf {
	

	function crearDataObject($tabla) {
		$sql = "SHOW COLUMNS FROM $tabla";
		$link = DbLink::getLink();
		$result = mysql_query($sql, $link);
		if (!$result) {
			$this->mensaje = mysql_error($link);
		    return false;
		}
		$nombre = ucfirst(strtolower($tabla));
                
		$str = "<?php\n";
		$str .= "require_once 'dblink.php';\n";
		$str .= "class $nombre {\n";
		$str .= "\tvar \$mensaje = null;\n";
		
		
		while ($metadata = mysql_fetch_assoc($result)) {
			$str .= "\tvar $".$metadata['Field']." = null;\n";
		}
		$str .= "\n\n\n\n//============================GETER Y SETER FUNCIONES ====================================\n\n";
		
		$str .= "\tpublic function  getMensaje(){\n";
		$str .= "\t\t return \$this->mensaje;\n";
		$str .= "\t}\n\n";
		
		
		
		mysql_data_seek($result, 0);
		while ($metadata = mysql_fetch_assoc($result)) {
			$str .= "\tpublic function set".ucfirst(strtolower($metadata['Field']))."($".$metadata['Field']."){\n";
			$str .= "\t\t\$this->".$metadata['Field']." = $".$metadata['Field'].";\n";;
			$str .= "\t}\n\n\n";

			$str .= "\tpublic function get".ucfirst(strtolower($metadata['Field']))."(){\n";
			$str .= "\t\t return \$this->".$metadata['Field'].";\n";;
			$str .= "\t}\n\n\n";
		}
		$str .= "\n\n\n\n//============================ FUNCION PARA INSERTAR ====================================\n\n";
		
		$str .= "\tpublic function insertarRegistro(){\n";
		$str .= "\t\t\$campos = \"\"; \$valores = \"\";\n";
		mysql_data_seek($result, 0);
		while ($metadata = mysql_fetch_assoc($result)) {
			$str .= "\t\tif((\$this->".$metadata['Field']." !== null) && (trim(\$this->".$metadata['Field'].")!=='') ){\n";
			$str .= "\t\t\t\$campos .= \"".$metadata['Field'].",\";\n";
			if(strpos("_".$metadata['Type'],"int(")){
				$str .= "\t\t\t\$valores .= \"\".\$this->".$metadata['Field'].".\",\";\n";
			}else{
				$str .= "\t\t\t\$valores .= \"'\".\$this->".$metadata['Field'].".\"',\";\n";
			}
			$str .= "\t\t}\n\n";
		}
		$str .= "\t\t\$campos = \"(\$campos)\";\n";
		$str .= "\t\t\$valores = \"(\$valores)\";\n";
		$str .= "\t\t\$campos = str_replace(\",)\",\")\",\$campos);\n";
		$str .= "\t\t\$valores = str_replace(\",)\",\")\",\$valores);\n";
		$str .= "\t\t\$sql = \"INSERT INTO ".$tabla." \$campos VALUES \$valores\";\n";
		$str .= "\t\t\$link = DbLink::getLink();\n";
		$str .= "\t\t\$result = mysql_query(\$sql, \$link);\n";
		$str .= "\t\tif (!\$result) {\n";
		$str .= "\t\t\t\$this->mensaje = mysql_error(\$link);\n";
		$str .= "\t\t\treturn false;\n";
		$str .= "\t\t}\n";
		$str .= "\t\treturn true;\n";
		$str .= "\t}//fin de la funcion insertar\n\n";
		

		
		
		$str .= "\n\n\n\n//============================ FUNCION PARA MODIFICAR ====================================\n\n";
		
		// Comienzo de la generacion del codigo para modificar registros
		$str .= "\tpublic function modificarRegistro(\$valor){\n";
		$str .= "\t\t\$sql = \"UPDATE ".$tabla." SET \";\n";
		mysql_data_seek($result, 0);
		while ($metadata = mysql_fetch_assoc($result)) {
			if(strtoupper($metadata['Key'])!=="PRI"){
				$str .= "\t\tif((\$this->".$metadata['Field']." !== null) && (trim(\$this->".$metadata['Field'].")!=='') ){\n";
				if(strpos("_".$metadata['Type'],"int(")){
					$str .= "\t\t\t\$sql .= \"".$metadata['Field']."=\".\$this->".$metadata['Field'].".\",\";\n";
				}else{
					$str .= "\t\t\t\$sql .= \"".$metadata['Field']."='\".\$this->".$metadata['Field'].".\"',\";\n";
				}
				$str .= "\t\t}\n";
			}
                        if(strtoupper($metadata['Key'])==="PRI"){
                            $arg = " WHERE ".$metadata['Field']." = '\".\$valor.\"'";
                        }
		}
		$str .= "\t\t\$sql .= \",\";\n";
		$str .= "\t\t\$sql  = str_replace(\",,\",\"\",\$sql);\n\n";
                $str .= "\t\t\$sql .= \"".$arg."\";\n";
		$str .= "\t\t\$link = DbLink::getLink();\n";
		$str .= "\t\t\$result = mysql_query(\$sql, \$link);\n";
		$str .= "\t\tif (!\$result) {\n";
		$str .= "\t\t\t\$this->mensaje = mysql_error(\$link);\n";
		$str .= "\t\t\treturn false;\n";
		$str .= "\t\t}\n";
		$str .= "\t\treturn true;\n";
		$str .= "\t}//fin de la funcion modificar\n\n";
				
                $str .= "\n\n\n\n//============================ FUNCION PARA ELIMINAR ====================================\n\n";
                $str .= "\tpublic function eliminarRegistro(\$valor){\n";
                while ($metadata = mysql_fetch_assoc($result)) {
                        if(strtoupper($metadata['Key'])==="PRI"){
                            $arg = " WHERE ".$metadata['Field']." = '\".\$valor.\"'";
                        }
		}
                $str .= "\t\t\$sql = \"DELETE FROM ".$tabla." ".$arg."\";\n";
                $str .= "\t\t\$link = DbLink::getLink();\n";
		$str .= "\t\t\$result = mysql_query(\$sql, \$link);\n";
		$str .= "\t\tif (!\$result) {\n";
		$str .= "\t\t\t\$this->mensaje = mysql_error(\$link);\n";
		$str .= "\t\t\treturn false;\n";
		$str .= "\t\t}\n";
		$str .= "\t\treturn true;\n";
		$str .= "\t}//fin de la funcion eliminar\n\n";
                


                
		
		
		
		
		$str .= "\n\n\n\n//============================ EJEMPLOS SETEANDO  ====================================\n\n";
		mysql_data_seek($result, 0);
		$str .= "/*\n";
		while ($metadata = mysql_fetch_assoc($result)) {
			$str .= "\t\$config->set".ucfirst(strtolower($metadata['Field']))."(trim(\$_POST['".$metadata['Field']."']));\n";
		}
		$str .= "*/\n";
		
		
		
		$str .= "}//fin de la clase\n\n\n";
		$str .= "?>";

		
		//echo $str;                
		$res = file_put_contents('../model/monitorremoto_'.$tabla.'.tabla.php', $str);
                chmod("../model/monitorremoto_".$tabla.".tabla.php", 0777);
		
		return $res;
    }	
}
?>
