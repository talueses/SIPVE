<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php'; // conexion Base de Datos
require_once "../../inicio/controller/controller.php"; 
require_once "../model/monitorremoto_vid_evento.tabla.php"; // Tabla: vid_evento
/**
 * Clase Monitor{}
 * @author David Concepcion 07-10-2010 CENIT *
 */
class Monitor {
    
    private $data = array();

    public function __set($name, $value)
    {
        //echo "Estableciendo '$name' igual a '$value'<br>";
        $this->data[$name]=$value;
    }
    public function __get($name)
    {
        //echo "Obteniendo '$name'-'".$this->data[$name]."'<br>";
        return $this->data[$name];
    }

    /**
     * Consulta de camaras asociadas por zonas a un usuario
     * @param string $usuario login del usuario de sistema
     * @return object  Devuelve registros como objeto
     */
    function getCamarasZona($usuario){
        $sql = "select vc.ipv4 as \"ipv4Camara\", 
                       vs.*,
                       vc.*,
                       vs.ipv4 as \"ipv4Servidor\",
                       tc.ptz, 
                       tc.urlvideo, 
                       concat(mac.nombre_marca_camara,mc.nombre_modelo_camara,tc.nombre_tipo_camara)as \"controlFile\",
                       (select count(idcamaraaxisguard) from vid_camara_axisguard vg where vg.idcamara = vc.idcamara and vg.running = 'yes') as \"guardRunning\"
                from vid_zona_usuario vzu,
                     vid_zona_camara vzc,
                     vid_camara vc,
		     vid_servidor vs,
                     vid_tipo_camara tc,
                     vid_modelo_camara mc,
                     vid_marca_camara mac
                where vzu.usuario      = '".$usuario."'
                  and vzu.idzona       = vzc.idzona
                  and vzc.idcamara     = vc.idcamara
 		  and vc.idservidor    = vs.idservidor
                  and vs.idsegmento in (select idsegmento from segmento_usuario where usuario = '".$_SESSION["usuario"]."')
                  and vc.idtipo_camara = tc.idtipo_camara
                  and tc.idmodelo_camara = mc.idmodelo_camara
                  and mc.idmarca_camara = mac.idmarca_camara
                    /*group by vc.ipv4*/
                    order by vc.prioridad desc, vc.idservidor,vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";        
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
        
    }
       
    /**
     * Consulta de eventos de una camara
     * @param int $idcamara Codigo de la camara
     * @param string $arg Complemnto SQL del argumento de la consulta
     * @return object Devuelve registros como objeto
     */
    function getEventos($idcamara,$arg){
        $sql = "select /*start*/ 
                       ve.*,
                       vc.*,
                       tz.location,
                       tz.standard_time,vs.ipv4 as \"ipv4Servidor\",
                       vc.ipv4 as \"ipv4Camara\",
                       (select file from vid_evento \"veAUX\" where ve.idservidor = \"veAUX\".idservidor and ve.camara = \"veAUX\".camara and ve.evento = \"veAUX\".evento and \"veAUX\".descripcion = 'picture_save' and concat(extract(year from ve.fecha),'-',extract(month from ve.fecha),'-',extract(day from ve.fecha)) = concat(extract(year from \"veAUX\".fecha),'-',extract(month from \"veAUX\".fecha),'-',extract(day from \"veAUX\".fecha)) limit 1)as file_picture_save
                       /*end*/
                from vid_camara vc, vid_servidor vs, timezone tz, vid_evento ve
                where vc.idcamara = '".$idcamara."'
                  and vc.idservidor = ve.idservidor
                  and ve.fecha between '".date("Y-m-d 00:00:00")."' and '".date("Y-m-d 23:59:59")."'
                  and vc.numero     = cast(ve.camara as :cast_int)
                  and vc.idservidor = vs.idservidor
                  and vs.idtimezone = tz.idtimezone
                  ".$arg;
        //echo "<div align=\"left\"><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Instanciacion de metodo getImg() en clase ControlMonitor{} (../controller/monitorremoto.control.php)
     * @param array $camaras Matriz de datos de camaras
     * @param int $i numero de la camara
     * @param string $size Tamaño de la camara
     * @return string Devuelve objetos HTML
     */
    function getImg($camaras,$i,$size){
        require_once "../controller/monitorremoto.control.php";
        return ControlMonitor::getImg($camaras,$i,$size);
    }

    /**
     * Instanciacion de metodo getImgExtra() en clase ControlMonitor{} (../controller/monitorremoto.control.php)
     * @param int $plantilla numero de plantilla seleccionada
     * @param int $countData cantidad de camaras asociadas al usuario
     * @param array $camaras Matriz de datos de camaras
     * @param int $nPerLine numero de objetos por linea
     * @return string Devuelve objetos HTML
     */
    function getImgExtra($plantilla,$countData,$camaras,$nPerLine){
        require_once "../controller/monitorremoto.control.php";
        return ControlMonitor::getImgExtra($plantilla,$countData,$camaras,$nPerLine);
    }

    /**
     * Instanciacion de metodo busca_valor() en clase ControlMonitor{} (../controller/monitorremoto.control.php)
     * @param string $equal valor a comparar
     * @param string $valor valor a comparar
     * @return string Devuelve SELECTED si los valores $equal y $valor son iguales
     */
    function busca_valor($equal, $valor ){
        require_once "../controller/monitorremoto.control.php";
        return ControlMonitor::busca_valor($equal, $valor );
    }

    /**
     * Instanciacion de metodo make_combo() en clase ControlMonitor{} (../controller/monitorremoto.control.php)
     * @param string $tabla tabla a consultar en base de datos
     * @param string $arg argumento consulta de base de datos
     * @param string $value campo de base de datos para valor de la opcion
     * @param string $descripcion campo de base de datos para descripcion que se muestra
     * @param string $equal valor a comparar por metodo busca_valor()
     * @param boolean $group valor de etiqueta de agrupacion <optgroup>
     * @return string Devuelve opciones de listas dinamicas HTML
     */
    function make_combo($tabla,$arg, $value, $descripcion, $equal,$group=false){
        require_once "../controller/monitorremoto.control.php";
        return ControlMonitor::make_combo($tabla,$arg, $value, $descripcion, $equal,$group);
    }

    /**
     * Consulta de camaras asociadas a un servidor ordenada por numero
     * @param string $idservidor Codigo del servidor
     * @return object Devuelve registros como objeto
     */
    static function getCamarasServidor($idservidor){
        $sql = "select vs.idservidor,
                       vc.idcamara,
                       vs.ipv4 as \"ipv4Serv\",
                       vs.so_usuario,
                       vs.so_clave,
                       vc.numero
                from vid_camara vc,
                     vid_servidor vs
                where vc.idservidor = '".$idservidor."'
                  and vc.idservidor = vs.idservidor
                  order by vc.numero";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
    /**
     * Consulta de configuracion de una camara
     * @param int $idcamara Codigo de la camara
     * @return object Devuelve un registro como objeto
     */
    static function getCamarasConf($idcamara){
        $sql = "select vc.ipv4,
                       vc.puerto,
                       vc.cam_usuario,
                       vc.cam_clave,
                       vc.rotacion,
                       tc.ptz
                from vid_camara vc, 
                     vid_tipo_camara tc
                where vc.idcamara = '".$idcamara."'
                  and vc.idtipo_camara = tc.idtipo_camara";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->mensaje = DB_Class::$dbErrorMsg;
            return false;
        }        
        return $res->fetchObject(); 
    }

}
?>