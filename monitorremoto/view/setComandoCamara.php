<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../controller/monitorremoto.control.php"; // Class CONTROLLER
$obj = new CamaraCommands();

$obj->option = $_REQUEST["option"];
$obj->value = $_REQUEST["value"];

//echo "<div align='left'><pre>".print_r($_REQUEST)."</pre></div>";

$method = false;

// --- Establecer nombre de metodo --- //
if (method_exists($obj, $_REQUEST["mainControlsMethod"]) && $_REQUEST["control"]=="main"){
    $method = $_REQUEST["mainControlsMethod"];
}
if (method_exists($obj, $_REQUEST["extraPanelMethod"]) && $_REQUEST["control"]=="extra"){
    $method = $_REQUEST["extraPanelMethod"];
}

// --- Establecer comandos de controles segun metodo ---//
if (method_exists($obj, $method)){
   
    if ($obj->option == "moveImg"){
        $obj->value  = $_REQUEST[$_REQUEST["idcamara"]."_x"].",".$_REQUEST[$_REQUEST["idcamara"]."_y"];
    }
    
    call_user_func(array($obj, $method));
    //echo "<p>Command:".CamaraCommands::$command."</p>";
}

$response = ControlMonitor::sendComandoCamara($_REQUEST["idcamara"]);

// ---------------------------- RESPUESTAS ---------------------------//
if (method_exists($obj, $method)){
    if ($response){
        //pan, tilt, zoom, focus, iris, autofocus, autoiris
        $response = str_replace("pan=", "", $response);
        $response = str_replace("tilt=", "", $response);
        $response = str_replace("zoom=", "", $response);
        $response = str_replace("focus=", "", $response);
        $response = str_replace("iris=", "", $response);
        $response = str_replace("auto", "", $response);
        $response = str_replace("\r", "", $response);
        $response = "position\n".$response;
        $response = explode("\n", $response);
        //echo "<div align='left'><pre>".print_r($response,true)."</pre></div>";
    }
}
echo json_encode($response);
?>