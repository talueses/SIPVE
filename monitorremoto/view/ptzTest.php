<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/monitorremoto.control.php"; // Class CONTROLLER

$obj = new ControlMonitor();


?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title><?php echo ucfirst($_REQUEST["accion"]);?> Camara de Video</title>

        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>        
        <script type="text/javascript" src="../js/jquery.base64.js"></script>
        <script type="text/javascript" src="../js/jquery.mousewheel.js"></script>
        <style type="text/css">
            div.savedItems { color: #770; font-size: .8em; list-style-position: outside; margin-left: 0;}
            div.savedItems .error { color: #ff0000;padding-left: 10px; }
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            
            #botones{
                margin: 10px;
            }

            /* Para que se vean los Tabs cuando se cargue el mapa*/
            .ui-tabs, .ui-tabs .ui-tabs-nav li, .ui-button{
                position: inherit;
            }
            #plantilla tr td , #plantilla th{                
                background:#ddd;                
                margin: 5px 1px 0px 1px;                
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #plantilla tr th{
                height: 20px;
            }
            
            /*--------------------------------------*/
            .listado{
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            .listado tr {
                    background:#fff;
            }
            .listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 80%; 
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }                 
                
            .imgCamaras{
                cursor: crosshair;
                background:#fff;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 16px;
                -webkit-border-radius: 16px;
                                border-radius: 16px;
            }            
            #pan, #zoom{
                width: 160px;
            }   
            #tilt{
                height: 200px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                // --------------- Estilos ------------------//                
                $('input:text,input:password').css({
                    background: '#fff' ,
                    border: '1px solid #d5d5d5',
                    '-moz-border-radius': '4px',
                    '-webkit-border-radius': '4px',
                    'border-radius': '4px'
                });
                $('input:button, input:submit').button(); 
                $('#radioKeyAccess').buttonset(); 
                
                
                // ------------ Comandos de teclado--------------- //
                $(window).keyup(function (e) {                            
                    if ($('#keyAccess').val()=='on'){                        
                        if (e.which == 37){
                            setMove('left');
                        }
                        if (e.which == 38 && !e.ctrlKey){
                            setMove('up');
                        }
                        if (e.which == 39){
                            setMove('right');
                        }
                        if (e.which == 40 && !e.ctrlKey){
                            setMove('down');
                        }
                        if (e.which == 38 && e.ctrlKey){
                            setMove('zoomIn');

                        }                    
                        if (e.which == 40 && e.ctrlKey){
                            setMove('zoomOut');
                        }                        
                    }                    
                });
                // -----------  Comandos de Zoom con Rueda del Mause ----------- // 
                $('.imgCamaras').mousewheel(function(event, delta, deltaX, deltaY) {                                        
                    if (delta > 0){
                        setMove('zoomIn');
                    }                        
                    if (delta < 0){
                        setMove('zoomOut');
                    }
                    event.preventDefault(); // Evita scrooling                    
                });


                // ------------------- Establecer Barras Slider --------------------//
                // PAM                
                $( "#pan" ).slider({
                    max:180,
                    min:-180,
                    stop: function( event, ui ) {
                        setPTZcommand($.base64.encode('/axis-cgi/com/ptz.cgi?pan='+ui.value));
                        $('#resPan').html(ui.value);
                    },
                    animate: true
                });
                // TILT
                $( "#tilt" ).slider({
                    orientation: "vertical",
                    max:90,
                    min:-30,
                    stop: function( event, ui ) {
                        setPTZcommand($.base64.encode('/axis-cgi/com/ptz.cgi?tilt='+ui.value));
                        $('#resTilt').html(ui.value);
                    },
                    animate: true
                });
                // ZOOM
                $( "#zoom" ).slider({
                    max:9999,
                    min:1,
                    stop: function( event, ui ) {
                        setPTZcommand($.base64.encode('/axis-cgi/com/ptz.cgi?zoom='+ui.value));
                        $('#resZoom').html(ui.value);
                        $('#currentZoom').val(ui.value);
                        
                    },
                    animate: true
                });
                // FOCUS
                $( "#focus" ).slider({
                    max:9999,
                    min:1,
                    stop: function( event, ui ) {
                        setPTZcommand($.base64.encode('/axis-cgi/com/ptz.cgi?focus='+ui.value));
                        $('#resFocus').html(ui.value);
                    },
                    animate: true
                });
                // IRIS
                $( "#iris" ).slider({
                    max:9999,
                    min:1,
                    stop: function( event, ui ) {
                        setPTZcommand($.base64.encode('/axis-cgi/com/ptz.cgi?iris='+ui.value));
                        $('#resIris').html(ui.value);
                    },
                    animate: true
                });
                
                // --------------- Establecer Paneles de Control -----------------------//
                $( "#mainControls, #extraPanel" ).dialog({ 
                    autoOpen: false,
                    resizable: false,
                    width: 230
                    
                });
                $( "#mainControls" ).dialog({                     
                    height: 318,
                    position:['right','top']
                });
                $( "#extraPanel" ).dialog({                                         
                    position:['right','bottom']
                });

                // --------- Posicion Actual de la Camara ----------  //
                setPTZcommand('<?php echo base64_encode("/axis-cgi/com/ptz.cgi?query=position");?>');
            });
            
            function setPTZcommand(comando){
                //alert(comando);
                $.ajax({                    
                    dataType: 'json',
                    type: "POST",
                    url: "setPTZcommand.php",
                    //data: 'idcamara='+$('#idcamara').val()+'&',
                    data: 'idcamara=1&comando='+comando,
                    success: function(data){                        
                        if (data[0]=='position'){
                            //alert(data);
                            // PAN
                            var pan = parseInt(data[1]);
                            if (pan){
                                $('#pan').slider( "option", "value", pan);
                                $('#resPan').html(pan);                            
                            }                                
                            // TILT
                            var tilt = parseInt(data[2]);
                            if (tilt){
                                $('#tilt').slider( "option", "value", tilt);                            
                                $('#resTilt').html(tilt);                           
                            }                            
                            // ZOOM
                            var zoom = parseInt(data[3]);
                            if (zoom){
                                $('#zoom').slider( "option", "value", zoom);                            
                                $('#resZoom').html(zoom);
                                $('#currentZoom').val(zoom);                                
                            }                            
                            // FOCUS
                            var focus = parseInt(data[4]);
                            if (focus){
                                $('#focus').slider( "option", "value", focus);                            
                                $('#resFocus').html(focus);
                            }                            
                            // IRIS
                            var iris = parseInt(data[5]);
                            if (iris){
                                $('#iris').slider( "option", "value", iris);                            
                                $('#resIris').html(iris);
                            }
                        }
                        
                    }
                });
            }
            function setMove(comando){
                if (comando=="home"){
                    setPTZcommand('<?php echo base64_encode("/axis-cgi/com/ptz.cgi?pan=0");?>');
                    $('#pan').slider( "option", "value", 0);
                    $('#resPan').html(0);
                    setPTZcommand('<?php echo base64_encode("/axis-cgi/com/ptz.cgi?tilt=0");?>');
                    $('#tilt').slider( "option", "value", 0);
                    $('#resTilt').html(0); 
                    setPTZcommand('<?php echo base64_encode("/axis-cgi/com/ptz.cgi?zoom=1");?>');
                    $('#zoom').slider( "option", "value", 1);
                    $('#resZoom').html(1); 
                    $('#currentZoom').val('1');
                    
                }
                if (comando!="home"){
                    switch(comando) {
                        case 'upleft':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=upleft");?>'
                            break;
                        case 'up':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=up");?>'
                            break;
                        case 'upright':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=upright");?>'
                            break;
                        case 'right':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=right");?>'
                            break;
                        case 'left':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=left");?>'
                            break;
                        case 'left':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=left");?>'
                            break;
                        case 'right':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=right");?>'
                            break;
                        case 'downleft':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=downleft");?>'
                            break;
                        case 'down':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=down");?>'
                            break;
                        case 'downright':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?move=downright");?>'
                            break;
                        case 'zoomIn':
                            var currentZoom = parseInt($('#currentZoom').val());
                            currentZoom += 1000;
                            if (currentZoom > 9999){
                                currentZoom = 9999;
                            }
                            setPTZcommand($.base64.encode('/axis-cgi/com/ptz.cgi?zoom='+currentZoom));                            
                            break;
                        case 'zoomOut':
                            var currentZoom = parseInt($('#currentZoom').val());
                            currentZoom -= 1000;
                            if (currentZoom < 1){
                                currentZoom = 1;
                            }                            
                            setPTZcommand($.base64.encode('/axis-cgi/com/ptz.cgi?zoom='+currentZoom));
                            break;
                        case 'autofocusOn':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?autofocus=on");?>'
                            break;
                        case 'autofocusOff':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?autofocus=off");?>'
                            break;
                        case 'autoirisOn':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?autoiris=on");?>'
                            break;
                        case 'autoirisOff':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?autoiris=off");?>'
                            break;
                        case 'ircutfilterOn':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?ircutfilter=on");?>'
                            break;
                        case 'ircutfilterOff':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?ircutfilter=off");?>'
                            break;
                        case 'ircutfilterAuto':
                            comando = '<?php echo base64_encode("/axis-cgi/com/ptz.cgi?ircutfilter=auto");?>'
                            break;
                        case 'keyAccessOn':
                            $('#keyAccess').val('on');
                            break;
                        case 'keyAccessOff':
                            $('#keyAccess').val('off');
                            break;
                        

                    }
                    setPTZcommand(comando);
                    setTimeout('setPTZcommand(\'<?php echo base64_encode("/axis-cgi/com/ptz.cgi?query=position");?>\');',1000);
                }
                
            }
        </script>
        </head>
        <body style="margin:0px;background:#ddd;">
            <div id="contenido" align="center">
                <div id="mainControls" title="Panel de Control">
                    <table border="0" width="100%" class="listado">
                        <tr>                            
                            <td height="100"  style="padding: 0px 0px 0px 20px;" align="center">
                                <a href="#" onclick="setMove('upleft');"><img src="../images/ptzUpLeft.gif"  /></a>
                                <a href="#" onclick="setMove('up');"><img src="../images/ptzUp.gif"  /></a>
                                <a href="#" onclick="setMove('upright');"><img src="../images/ptzUpRight.gif"  /></a>
                                <br />
                                <a href="#" onclick="setMove('left');" ><img src="../images/ptzLeft.gif"  /></a>
                                <a href="#" onclick="setMove('home');" ><img src="../images/ptzCenter.gif"  /></a>
                                <a href="#" onclick="setMove('right');" ><img src="../images/ptzRight.gif"  /></a>
                                <br />
                                <a href="#" onclick="setMove('downleft');" ><img src="../images/ptzDownLeft.gif"  /></a>
                                <a href="#" onclick="setMove('down');" ><img src="../images/ptzDown.gif"  /></a>
                                <a href="#" onclick="setMove('downright');" ><img src="../images/ptzDownRight.gif"  /></a>  
                            </td>
                            <td  rowspan="3" valign="top" align="center" >
                                 <b><label id="resTilt" >0</label>&deg;</b><div id="tilt"></div>
                            </td>
                        </tr>
                        <tr>                            
                            <td align="center" > 
                                <table class="listado"  style="padding: 0px;margin: 0px;"  >
                                    <tr>
                                        <th>Pan: <b><label id="resPan">0</label>&deg;</b></th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="pan"></div>
                                        </td>
                                    </tr>                        
                                </table>
                                <br />
                                <table class="listado"  style="padding: 0px;margin: 0px;"  >
                                    <tr>
                                        <th>Zoom: <b><label id="resZoom">1</label><input type="hidden" name="currentZoom" id="currentZoom" /></b></th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="zoom"></div>
                                        </td>
                                    </tr>                        
                                </table>
                            </td>
                        </tr>                        
                    </table>                    
                    <center><input type="button" value="Controles Extra" style="width: 100%; margin-top: 5px;" onclick="$('#extraPanel').dialog('open');" /></center>
                    <br />                    
                </div>
                <div id="extraPanel" title="Controles Extra">
                    <table border="0" width="100%" class="listado" >
                        <tr>
                            <td  align="center">
                                <table class="listado" width="100%" style="padding: 0px;margin: 0px;"  >
                                    <tr>
                                        <th>Foco: <b><label id="resFocus">1</label></b></th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="focus"></div>
                                        </td>
                                    </tr>                        
                                </table>
                                <table class="listado" width="100%" style="padding: 0px;margin: 5px 0px 0px 0px;"  >
                                    <tr>
                                        <th>Iris: <b><label id="resIris">1</label></b></th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="iris"></div>
                                        </td>
                                    </tr>                        
                                </table>
                                <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;"  >
                                    <tr>
                                        <th>Auto Foco</th>
                                    </tr>
                                    <tr>
                                        <td  align="center" >
                                            <input type="button" value="On" onclick="setMove('autofocusOn');" />
                                            <input type="button" value="Off" onclick="setMove('autofocusOff');" />
                                        </td>
                                    </tr>                        
                                </table>
                                <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                                    <tr>
                                        <th>Auto Iris</th>
                                    </tr>
                                    <tr>
                                        <td  align="center" >
                                            <input type="button" value="On" onclick="setMove('autoirisOn');" />
                                            <input type="button" value="Off" onclick="setMove('autoirisOff');" />
                                        </td>
                                    </tr>                        
                                </table>
                                <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                                    <tr>
                                        <th style="font-size: 11px;">Compensaci&oacute;n de luz de fondo</th>
                                    </tr>
                                    <tr>
                                        <td  align="center" >
                                            <input type="button" value="On" onclick="setMove('autoirisOn');" />
                                            <input type="button" value="Off" onclick="setMove('autoirisOff');" />
                                        </td>
                                    </tr>                        
                                </table>
                                <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                                    <tr>
                                        <th>Filtro IR Cut</th>
                                    </tr>
                                    <tr>
                                        <td  align="center" >
                                            <input type="button" value="On" onclick="setMove('ircutfilterOn');" style="width: 40px;"/>
                                            <input type="button" value="Off" onclick="setMove('ircutfilterOff');"  style="width: 40px;"/>
                                            <input type="button" value="Auto" onclick="setMove('ircutfilterAuto');"  style="width: 40px;"/>
                                        </td>
                                    </tr>                        
                                </table>
                                <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                                    <tr>
                                        <th >Movimiento por Teclado</th>
                                    </tr>
                                    <tr>
                                        <td  align="center" >
                                            <div id="radioKeyAccess">
                                                <input type="radio" name="radioKey" id="radioKeyAccess1" onclick="setMove('keyAccessOn');"/><label for="radioKeyAccess1">On</label>
                                                <input type="radio" name="radioKey" id="radioKeyAccess2" onclick="setMove('keyAccessOff');" checked="checked"/><label for="radioKeyAccess2">Off</label>
                                            </div>
                                            <!--<input type="button" value="On" onclick="setMove('keyAccessOn');" />
                                            <input type="button" value="Off" onclick="setMove('keyAccessOff');" />-->
                                        </td>
                                    </tr>                        
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="titulo" style="padding: 5px">PTZ Test Center</div>
                <br/>
                <div id="datos"  align="center">
                    <table id="plantilla" align="center">
                        <tr>
                            <td>
                                <!--<img class="imgCamaras" src="http://150.186.195.64:80/axis-cgi/mjpg/video.cgi?resolution=640x480&imagerotation=0" />-->
                                <form action="setPTZcommand.php" target="ptzMoveImg">
                                    <input class="imgCamaras" type="image"  src="http://150.186.195.66:80/axis-cgi/mjpg/video.cgi" />
                                    <input type="hidden" name="comando" id="comando" value="<?php echo base64_encode("/axis-cgi/com/ptz.cgi?");?>" />
                                    <input type="hidden" name="ptzAcc" id="ptzAcc" value="moveImg" />
                                    <input type="hidden" name="keyAccess" id="keyAccess" value="off" />
                                    
                                </form>
                            </td>
                        </tr>
                    </table>
                    <input type="button" value="Panel de Control" onclick="$('#mainControls').dialog('open');" />
                </div>
            </div>
            <iframe name="ptzMoveImg" id="ptzMoveImg" width="0" height="0" scrolling="no"></iframe>
        </body>
</html>
