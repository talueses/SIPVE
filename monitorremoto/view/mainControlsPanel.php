<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
session_start(); // start up your PHP session!

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/monitorremoto.control.php"; // Class CONTROLLER

$obj = new CamaraCommands();

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

if (method_exists($obj, $_REQUEST["mainControlsMethod"])){
    call_user_func(array($obj, $_REQUEST["mainControlsMethod"]));    
}

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script> 
        <style type="text/css">
            
            .listado{
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            .listado tr {
                    background:#fff;
            }
            .listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }                  
            #pan, #zoom{
                width: 160px;
            }   
            #tilt{
                height: 200px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                var maxPan   = parseInt(<?php echo $obj->maxPan;?>);
                var minPan   = parseInt(<?php echo $obj->minPan;?>);
                var maxTilt  = parseInt(<?php echo $obj->maxTilt;?>);
                var minTilt  = parseInt(<?php echo $obj->minTilt;?>);
                var maxZoom  = parseInt(<?php echo $obj->maxZoom;?>);
                var minZoom  = parseInt(<?php echo $obj->minZoom;?>);
                $('input:button, input:submit').button(); 
                
                //$('#radioKeyAccess').buttonset(); 
                

                // ------------------- Establecer Barras Slider --------------------//
                // PAM                
                $( "#pan" ).slider({
                    max:maxPan,
                    min:minPan,
                    stop: function( event, ui ) {
                        setComandoCamara('pan',ui.value);
                        $('#resPan').html(ui.value);
                    },
                    animate: true
                });
                // TILT
                if ($('.BIG', parent.document).attr('rotacion')=="180"){
                    maxTilt -= 60;
                    minTilt -= 60;
                }
                $( "#tilt" ).slider({
                    orientation: "vertical",
                    max:maxTilt,
                    min:minTilt,
                    stop: function( event, ui ) {
                        setComandoCamara('tilt',ui.value);
                        $('#resTilt').html(ui.value);
                    },
                    animate: true
                });
                // ZOOM
                $( "#zoom" ).slider({
                    max:maxZoom,
                    min:minZoom,
                    stop: function( event, ui ) {
                        setComandoCamara('zoom',ui.value);
                        $('#resZoom').html(valZoom(ui.value));
                        $('#currentZoom').val(ui.value);
                        
                    },
                    animate: true
                });

                // --------- Posicion Actual de la Camara ----------  //
                setComandoCamara('position','');
            });
            
            function setComandoCamara(option,value){
                //alert('idcamara='+$('#idcamara', parent.document).val()+'&control=main&mainControlsMethod='+$('#mainControlsMethod', parent.document).val()+'&option='+option+'&value='+value);
                $.ajax({                    
                    dataType: 'json',
                    type: "POST",
                    url: "setComandoCamara.php",
                    data: 'idcamara='+$('#idcamara', parent.document).val()+'&control=main&mainControlsMethod='+$('#mainControlsMethod', parent.document).val()+'&option='+option+'&value='+value,
                    success: function(data){
                        //alert(data);
                        if (data[0]=='position'){
                            //alert(data);
                            // PAN
                            var pan = parseInt(data[1]);
                            if (pan){
                                $('#pan').slider( "option", "value", pan);
                                $('#resPan').html(pan);                            
                            }                                
                            // TILT
                            var tilt = parseInt(data[2]);
                            if (tilt){
                                $('#tilt').slider( "option", "value", tilt);                            
                                $('#resTilt').html(tilt);                           
                            }                            
                            // ZOOM
                            var zoom = parseInt(data[3]);
                            if (zoom){
                                $('#zoom').slider( "option", "value", zoom);                            
                                $('#resZoom').html(valZoom(zoom));
                                $('#currentZoom').val(zoom);                                
                            }                            
                        }                        
                    }
                });
            }
            function setMove(option){
                var value = null;
                if (option=="home"){
                    setComandoCamara('pan','0');
                    $('#pan').slider( "option", "value", 0);
                    $('#resPan').html(0);
                    setComandoCamara('tilt','0');
                    $('#tilt').slider( "option", "value", 0);
                    $('#resTilt').html(0); 
                    setComandoCamara('zoom','1');
                    $('#zoom').slider( "option", "value", 1);
                    $('#resZoom').html(valZoom(1));
                    $('#currentZoom').val('1');
                    
                }
                if (option!="home"){
                    setComandoCamara(option,value);                    
                    setTimeout('setComandoCamara(\'position\',\'\');',1000);
                }
                
            }
            function valZoom(value){
                var maxZoom  = parseInt(<?php echo $obj->maxZoom;?>);
                return parseInt(((parseInt(value) * 100) / maxZoom))+'%';
            }
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <center>
            <table border="0" width="99%" class="listado">
                <tr>                            
                    <td height="100"  style="padding: 0px 0px 0px 20px;" align="center">
                        <a href="#" onclick="setMove('upleft');"><img src="../images/ptzUpLeft.gif"  /></a>
                        <a href="#" onclick="setMove('up');"><img src="../images/ptzUp.gif"  /></a>
                        <a href="#" onclick="setMove('upright');"><img src="../images/ptzUpRight.gif"  /></a>
                        <br />
                        <a href="#" onclick="setMove('left');" ><img src="../images/ptzLeft.gif"  /></a>
                        <a href="#" onclick="setMove('home');" ><img src="../images/ptzCenter.gif"  /></a>
                        <a href="#" onclick="setMove('right');" ><img src="../images/ptzRight.gif"  /></a>
                        <br />
                        <a href="#" onclick="setMove('downleft');" ><img src="../images/ptzDownLeft.gif"  /></a>
                        <a href="#" onclick="setMove('down');" ><img src="../images/ptzDown.gif"  /></a>
                        <a href="#" onclick="setMove('downright');" ><img src="../images/ptzDownRight.gif"  /></a>  
                    </td>
                    <td  rowspan="3" valign="top" align="center" >
                        <b><label id="resTilt" >0</label>&deg;</b><div id="tilt"></div>
                    </td>
                </tr>
                <tr>                            
                    <td align="center" > 
                        <table class="listado"  style="padding: 0px;margin: 0px;"  >
                            <tr>
                                <th>Pan: <b><label id="resPan">0</label>&deg;</b></th>
                            </tr>
                            <tr>
                                <td>
                                    <div id="pan"></div>
                                </td>
                            </tr>                        
                        </table>
                        <br />
                        <table class="listado"  style="padding: 0px;margin: 0px;"  >
                            <tr>
                                <th>Zoom: <b><label id="resZoom">0%</label><input type="hidden" name="currentZoom" id="currentZoom" /></b></th>
                            </tr>
                            <tr>
                                <td>
                                    <div id="zoom"></div>
                                </td>
                            </tr>                        
                        </table>
                    </td>
                </tr>                        
            </table>
        </center>
    </body>
</html>
