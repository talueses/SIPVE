<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion (dcf) 07-10-2010 CENIT
 * Seleccion de plantillas para mostrar camaras
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/monitorremoto.control.php";
$monitor = new Monitor();

// -------- CAMARAS ASOCIADAS A SEGMENTOS -ZONAS  ------------------------//
$data = $monitor->getCamarasZona($_SESSION['usuario']);


?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>Plantilla Monitor Remoto</title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>  
        <style type="text/css" >

            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 25px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3;
                    background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
             #plantilla tr td , #plantilla th{                
                background:#ddd;                
                margin: 5px 1px 0px 1px;                
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #plantilla tr th{
                height: 20px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function(){
                //$('#mainFrame' ,parent.document).css('height','750px');
            });
            function abrirPlantilla(plantilla){
                var usuario     = "<?php echo $_SESSION['usuario']?>";
                var width       = screen.width ;
                var height      = screen.height ;
                var camarasAsoc = "<?php echo count($data);?>";

                if (camarasAsoc == "0"){
                    alert("El usuario "+usuario+" no tiene camaras asociadas.");
                    return false;
                }
                if (parseInt(camarasAsoc) < parseInt(plantilla)){
                    alert("Las cantidad de camaras asociadas es menor a la de la plantilla seleccionada");
                    return false;
                }
                var d = new Date();
                window.open("monitorremoto.php?usuario="+usuario+"&plantilla="+plantilla+"&"+d.getMilliseconds() , "monitor" , "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes,width="+width+",height="+height);
            }            
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">            
        <center>
            <table id="listado" style="width:99%;background:#fff;">
                <?php
                if ((!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"verCamaras")) || (!Controller::$chkCookie["MonitorVid"])){
                    echo "<tr><td align=\"center\" height=\"620\" valign=\"top\">".Controller::$mensajePermisos."</td></tr>";
                }else{
                    ?>
                    <tr>
                        <td id="titulo" align="center">
                            <font color="<?php echo count($data)==0 ? "#B22222" : "#00008B"; ?>"><?php echo "Usuario  <i>".$_SESSION['usuario']."</i> tiene ".count($data)." c&aacute;mara(s) asociada(s)";?></font>
                        </td>
                    </tr>
                    <tr >
                        <td align="center" >
                            <!--<a href="ptzTest.php" target="_blank" style="float: left;" title="Prueba Funcionalidad PTZ &laquo;150.186.195.64&raquo;"><img src="../images/Cctv-Camera-32.png" /></a>-->
                            <table id="plantilla" align="center">
                                <tr>
                                    <th colspan="3" align="center">
                                        Presione la plantilla de su preferencia para ver las c&aacute;maras

                                    </th>
                                </tr>
                                <tr>
                                    <td><img src="../images/1camara.png" width="300" height="190" title="Una c&aacute;mara" alt="Una c&aacute;mara" onclick="abrirPlantilla('1')"></td>
                                    <td><img src="../images/2camaras.png" width="300" height="190" title="Dos c&aacute;maras" alt="Dos c&aacute;maras" onclick="abrirPlantilla('2')"></td>
                                    <td><img src="../images/3camaras.png" width="300" height="190" title="Tres c&aacute;maras" alt="Tres c&aacute;maras" onclick="abrirPlantilla('3')"></td>
                                </tr>
                                <tr>
                                    <td><img src="../images/4camaras.png" width="300" height="190" title="Cuatro c&aacute;maras" alt="Cuatro c&aacute;maras" onclick="abrirPlantilla('4')"></td>
                                    <td><img src="../images/5camaras.png" width="300" height="190" title="Cinco c&aacute;maras" alt="Cinco c&aacute;maras" onclick="abrirPlantilla('5')"></td>
                                    <td><img src="../images/6camaras.png" width="300" height="190" title="Seis c&aacute;maras" alt="Seis c&aacute;maras" onclick="abrirPlantilla('6')"></td>
                                </tr>
                                <tr>
                                    <td><img src="../images/7camaras.png" width="300" height="190" title="Siete c&aacute;maras" alt="Siete c&aacute;maras" onclick="abrirPlantilla('7')"></td>
                                    <td><img src="../images/9camaras.png" width="300" height="190" title="Nueve c&aacute;maras" alt="Nueve c&aacute;maras" onclick="abrirPlantilla('9')"></td>
                                    <td><img src="../images/12camaras.png" width="300" height="190" title="Doce c&aacute;maras" alt="Doce c&aacute;maras" onclick="abrirPlantilla('12')"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>           
        </center>
    </body>
</html>