<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 07-10-2010 CENIT
 * Ejecucion de operacion modificar estatus y observacion evento
 * 
 */
session_start(); // start up your PHP session!
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
//echo "<div align='left'><pre>".print_r($_POST,true)."</pre></div>";

require_once "../controller/monitorremoto.control.php";
$monitor = new ControlMonitor();
$exito = $monitor->updateEvento();
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="../css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" language="javascript">
            $(function() {
                var mensaje = '<?php echo $monitor->mensaje?>';
                var str;
                str  = '<div class="ui-widget">';
                <?php            
                if($exito){
                    ?>
                    str += '    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
                    str += '        <p>';
                    str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>';                    

                    $("#status",parent.document).attr("disabled",true);
                    $("#btnGuardar",parent.document).attr("disabled",true);
                    $("#observacion",parent.document).attr("readonly",true);
                    parent.setStatus("1");
                    
                    <?php
                }
                if(!$exito){
                    ?>
                    str += '    <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">';
                    str += '        <p>';
                    str += '            <span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>';                    
                    parent.setStatus("0");
                    <?php
                }
                ?>
                str += '                '+mensaje;
                str += '        </p>';
                str += '    </div>';
                str += '</div>';
                $('#divmensaje',parent.document).html(str).fadeIn('slow');
                
            });
        </script>
    </head>
    <body>
    </body>
</html>
