<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once "../controller/monitorremoto.control.php"; // Class CONTROLLER
$obj = new ControlMonitor();


//echo "<div align='left'><pre>".print_r($_REQUEST)."</pre></div>";

if ($_REQUEST["ptzAcc"]=="moveImg"){
    $_REQUEST["comando"] = base64_decode($_REQUEST["comando"]);
    $_REQUEST["comando"] .= "center=".$_REQUEST["x"].",".$_REQUEST["y"];
    $_REQUEST["comando"] = base64_encode($_REQUEST["comando"]);
    ?>
    <script type="text/javascript" language="javascript">
        setTimeout('window.parent.setPTZcommand(\'<?php echo base64_encode("/axis-cgi/com/ptz.cgi?query=position");?>\');',1000);        
    </script>
    <?php
}

$param["ipv4"]    = "150.186.195.66";
$param["puerto"]  = "80";
$param["usuario"] = "";
$param["clave"]   = "";
$param["comando"] = $_REQUEST["comando"];

echo $obj->setPTZcommand($param);
?>