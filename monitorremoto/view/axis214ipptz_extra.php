<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php

session_start(); // start up your PHP session!

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/monitorremoto.control.php"; // Class CONTROLLER

$obj = new CamaraCommands();

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

if (method_exists($obj, $_REQUEST["extraPanelMethod"])){
    call_user_func(array($obj, $_REQUEST["extraPanelMethod"]));    
}

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>                
        <style type="text/css">
            
            .listado{
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            .listado tr {
                    background:#fff;
            }
            .listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            } 
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                var maxFocus = parseInt(<?php echo $obj->maxFocus;?>);
                var minFocus = parseInt(<?php echo $obj->minFocus;?>);
                var maxIris  = parseInt(<?php echo $obj->maxIris;?>);
                var minIris  = parseInt(<?php echo $obj->minIris;?>);
                
                if($('#keyAccess', parent.document).val()=='on'){
                    $("#radioKeyAccess1").attr("checked", true);
                }
                
                $('input:button, input:submit').button(); 
                $('#radioKeyAccess').buttonset(); 
                
                
                
                $( "#focus" ).slider({
                    max:maxFocus,
                    min:minFocus,
                    stop: function( event, ui ) {
                        setComandoCamara('focus',ui.value);
                        $('#resFocus').html(setValPercent(ui.value,parseInt(<?php echo $obj->maxFocus;?>)));
                    },
                    animate: true
                });
                // IRIS
                $( "#iris" ).slider({
                    max:maxIris,
                    min:minIris,
                    stop: function( event, ui ) {
                        setComandoCamara('iris',ui.value);
                        $('#resIris').html(setValPercent(ui.value,parseInt(<?php echo $obj->maxIris;?>)));
                    },
                    animate: true
                });

                // --------- Posicion Actual de la Camara ----------  //
                setComandoCamara('position','');
            });
            function setComandoCamara(option,value){
                //alert('idcamara='+$('#idcamara', parent.document).val()+'&extraPanelMethod='+$('#extraPanelMethod', parent.document).val()+'&option='+option+'&value='+value);
                $.ajax({                    
                    dataType: 'json',
                    type: "POST",
                    url: "setComandoCamara.php",
                    //data: 'idcamara='+$('#idcamara').val()+'&',
                    data: 'idcamara='+$('#idcamara', parent.document).val()+'&control=extra&extraPanelMethod='+$('#extraPanelMethod', parent.document).val()+'&option='+option+'&value='+value,
                    success: function(data){
                        //alert(data);
                        if (data[0]=='position'){
                            //alert(data);                            
                            // FOCUS
                            var focus = parseInt(data[4]);
                            if (focus){
                                $('#focus').slider( "option", "value", focus);                            
                                $('#resFocus').html(setValPercent(focus,parseInt(<?php echo $obj->maxFocus;?>)));
                            }                            
                            // IRIS
                            var iris = parseInt(data[5]);
                            if (iris){
                                $('#iris').slider( "option", "value", iris);                            
                                $('#resIris').html(setValPercent(iris,parseInt(<?php echo $obj->maxIris;?>)));
                            }
                        }                        
                    }
                });
            }
            function setMove(option){
                var value = null;                
                switch(option) {
                    case 'autofocusOn':
                        option = 'autofocus';
                        value  = 'on';
                        break;
                    case 'autofocusOff':
                        option = 'autofocus';
                        value  = 'off';
                        break;
                    case 'autoirisOn':
                        option = 'autoiris';
                        value  = 'on';
                        break;
                    case 'autoirisOff':
                        option = 'autoiris';
                        value  = 'off';
                        break;
                    case 'ircutfilterOn':
                        option = 'ircutfilter';
                        value  = 'on';
                        break;
                    case 'ircutfilterOff':
                        option = 'ircutfilter';
                        value  = 'off';
                        break;
                    case 'ircutfilterAuto':
                        option = 'ircutfilter';
                        value  = 'auto';
                        break;
                    case 'keyAccessOn':
                        $('#keyAccess', parent.document).val('on');
                        return false;
                        break;
                    case 'keyAccessOff':
                        $('#keyAccess',parent.document).val('off');
                        return false;
                        break;

                }
                setComandoCamara(option,value);
                setTimeout('setComandoCamara(\'position\',\'\');',1000);                
            }
            function setValPercent(value,maxVal){
                return parseInt(((parseInt(value) * 100) / maxVal))+'%';
            }
        </script>
    </head>
    <body style="margin:0px;background:#fff;">
        <center>
            <table border="0" width="99%" class="listado" >
                <tr>
                    <td  align="center">
                        <table class="listado" width="100%" style="padding: 0px;margin: 0px;"  >
                            <tr>
                                <th>Foco: <b><label id="resFocus">0%</label></b></th>
                            </tr>
                            <tr>
                                <td>
                                    <div id="focus"></div>
                                </td>
                            </tr>                        
                        </table>
                        <table class="listado" width="100%" style="padding: 0px;margin: 5px 0px 0px 0px;"  >
                            <tr>
                                <th>Iris: <b><label id="resIris">0%</label></b></th>
                            </tr>
                            <tr>
                                <td>
                                    <div id="iris"></div>
                                </td>
                            </tr>                        
                        </table>
                        <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;"  >
                            <tr>
                                <th>Auto Foco</th>
                            </tr>
                            <tr>
                                <td  align="center" >
                                    <input type="button" value="On" onclick="setMove('autofocusOn');" />
                                    <input type="button" value="Off" onclick="setMove('autofocusOff');" />
                                </td>
                            </tr>                        
                        </table>
                        <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                            <tr>
                                <th>Auto Iris</th>
                            </tr>
                            <tr>
                                <td  align="center" >
                                    <input type="button" value="On" onclick="setMove('autoirisOn');" />
                                    <input type="button" value="Off" onclick="setMove('autoirisOff');" />
                                </td>
                            </tr>                        
                        </table>
                        <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                            <tr>
                                <th style="font-size: 11px;">Compensaci&oacute;n de luz de fondo</th>
                            </tr>
                            <tr>
                                <td  align="center" >
                                    <input type="button" value="On" onclick="setMove('autoirisOn');" />
                                    <input type="button" value="Off" onclick="setMove('autoirisOff');" />
                                </td>
                            </tr>                        
                        </table>
                        <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                            <tr>
                                <th>Filtro IR Cut</th>
                            </tr>
                            <tr>
                                <td  align="center" >
                                    <input type="button" value="On" onclick="setMove('ircutfilterOn');" style="width: 40px;"/>
                                    <input type="button" value="Off" onclick="setMove('ircutfilterOff');"  style="width: 40px;"/>
                                    <input type="button" value="Auto" onclick="setMove('ircutfilterAuto');"  style="width: 40px;"/>
                                </td>
                            </tr>                        
                        </table>
                        <table  class="listado" width="80%" style="padding: 0px;margin: 5px 0px 0px 0px;    "  >
                            <tr>
                                <th >Movimiento por Teclado</th>
                            </tr>
                            <tr>
                                <td  align="center" >
                                    <div id="radioKeyAccess">
                                        <input type="radio" name="radioKey" id="radioKeyAccess1" onclick="setMove('keyAccessOn');"/><label for="radioKeyAccess1">On</label>
                                        <input type="radio" name="radioKey" id="radioKeyAccess2" onclick="setMove('keyAccessOff');" checked="checked"/><label for="radioKeyAccess2">Off</label>
                                    </div>
                                </td>
                            </tr>                        
                        </table>
                    </td>
                </tr>
            </table>
        </center>        
    </body>
</html>