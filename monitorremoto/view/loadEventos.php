<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 07-10-2010 CENIT
 * Listado de eventos asociados a una camara
 */
session_start(); // start up your PHP session!
require_once "../controller/monitorremoto.control.php";
//echo "<div align=\"left\"><pre>".print_r($_REQUEST,true)."</pre></div>".date("h:i:s a");



$monitor = new Monitor();

if ($_REQUEST["accion"]=="ultimos10"){
    $_SESSION['perPage'] = $_SESSION['page'] = null;
    $arg = "and ve.descripcion = 'movie_start' order by idevento desc /*limit 8*/";
}
if ($_REQUEST["accion"]=="verTodos"){
    $arg = "and ve.descripcion = 'movie_start' order by idevento desc";
}
$eventos = $monitor->getEventos($_REQUEST["idcamara"],$arg);

if (count($eventos)==0){
    echo "<div align=\"center\" style=\"width:99%;border:2px solid green;\">No se encontraron eventos</div>";
}else{
    date_default_timezone_set($eventos[0]->location);
    //echo "<div align=\"left\"><pre>".print_r($eventos,true)."</pre></div>";
    $linkVerTodos = basename($_SERVER["PHP_SELF"])."?idcamara=".$_REQUEST["idcamara"]."&accion=verTodos";
    ?>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
            <title>Listado de Eventos</title>
            <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
            <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
            <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
            <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
            <style type="text/css" >
                #tabla {
                    font-size: 13px;
                    height: 99%;
                    width:99%;
                    border:  #aaaaaa solid 1px;
                    overflow-x:hidden
                    width:95%;

                    background:#fff;
                    -moz-border-radius: 6px;
                    -webkit-border-radius: 6px;
                                    border-radius: 6px;
                }
                #listado{
                    width:99%;
                    background:#fff;
                    margin: 0px 1px 0px 1px;
                    border:  #aaaaaa solid 1px;
                    -moz-border-radius: 6px;
                    -webkit-border-radius: 6px;
                                    border-radius: 6px;
                }
                #titulo{
                    height: 30px;
                    border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                    -moz-border-radius: 6px;
                    -webkit-border-radius: 6px;
                                    border-radius: 6px;
                }
                #listado tr {
                        background:#fff;
                }
                #listado tr th {
                        border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                        -moz-border-radius: 6px;
                    -webkit-border-radius: 6px;
                                    border-radius: 6px;
                }

                #listado tr.chequeada {
                        background:#dd0;
                }
                #listado tr.pasada {
                        background:#dd0;
                        cursor:pointer;
                }
                .boton{
                    font-size: 10px;
                }
                /* Para que no se vean los Botones cuando se ejecute la accion*/
                .ui-button{
                    position: inherit;
                }
            </style>
            <script type="text/javascript" language="javascript">
                $(function() {                   
                    $('input:text,input:password').css({
                        background: '#fff' ,
                        border: '1px solid #d5d5d5',
                        '-moz-border-radius': '4px',
                        '-webkit-border-radius': '4px',
                        'border-radius': '4px'
                    });
                    $( "input:button, input:submit, button" ).button();
                });
                filaseleccionada = {};
                function oclic(fila){
                    var aux = fila.id.split("|");
                    var idevento = aux[0];
                    var fecha    = aux[1];
                    var evento   = aux[2];
                    var idcamara = "<?php echo $_REQUEST["idcamara"]?>";
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    fila.className = 'chequeada';
                    
                    <?php
                    //-------------------- INVOCADA DESDE LA MISMA VENTANA ---------------------//
                    if ($_REQUEST["accion"]=="verTodos"){
                        echo "$(document).attr('location','setStatusEvento.php?idevento='+idevento+'&fecha='+fecha+'&evento='+evento+'&idcamara='+idcamara+'&accion=setStatusEvento');";
                    }
                    //-------------------- INVOCADA DESDE LA VENTANA GRANDE --------------------//
                    if ($_REQUEST["accion"]!="verTodos"){
                        //echo "loadEvento('setStatusEvento.php?idevento='+idevento+'&fecha='+fecha+'&idcamara='+idcamara+'&accion=setStatusEvento');";     
                        //echo "$(document).attr('location','setStatusEvento.php?idevento='+idevento+'&fecha='+fecha+'&evento='+evento+'&idcamara='+idcamara+'&accion=setStatusEvento');";
                        echo "top.setLoadEvento('setStatusEvento.php?idevento='+idevento+'&fecha='+fecha+'&evento='+evento+'&idcamara='+idcamara+'&accion=setStatusEvento');";
                    }
                    ?>                        
                }

                function omover(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    fila.className = 'pasada';
                }
                function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;
                    fila.className = "";
                }
                function getReloj(){                    
                    var d = new Date();
                    $("#reloj").html(d.getDate()+"-"+d.getUTCMonth()+"-"+d.getFullYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds());                    
                    setTimeout("getReloj()",1000);
                }

                $(function(){
                   /*$.ajax({
                        type: "POST",
                        url: "getCamaraStatus.php",
                        data: "idcamara=<?php echo $eventos[0]->idcamara?>&idservidor=<?php echo $eventos[0]->idservidor?>",
                        success: function(html){
                            $("#camaraStatus").html(html);
                        }
                    });                    
                    /setTimeout("ajaxLoadEventos();", 10*1000);*/
                    if (parseInt($('#guardRunning', parent.document).val()) != 0 ){
                        $('#guardRunningImg').css('display','inherit');
                    }
                    
                });
            </script>
        </head>
        <body id="body" style="margin:0px;background:#fff;">
            <center>
                <div id="tabla" align="center">
                    <table id="listado" align="center">
                        <tr>
                            <th colspan="4" style="background:#bbb;">
                                <div id="camaraStatus" align="left" style="width: 40px;float:left;" >
                                    <?php
                                    $_REQUEST["idservidor"] = $eventos[0]->idservidor;
                                    include_once "getCamaraStatus.php";
                                    ?>
                                </div>
                                <div id="guardRunningImg" align="left" style="width: 40px;float:left;display: none;" >
                                    <img src="../images/Info-red-20.png" title="Existe una guardia activa para la c&aacute;mara <?php echo $eventos[0]->camara;?>." onclick="alert('Existe una guardia activa para la c&aacute;mara <?php echo $eventos[0]->camara;?>')" style="cursor: help;"/>
                                </div>
                                
                                <div id="reloj" align="right" style="float:left;" >
                                    &nbsp;
                                    <?php 
                                    if ($_REQUEST["accion"]!="verTodos"){
                                        echo date("d-m-Y H:i:s");
                                    }?>
                                </div>
                                <div style="width: 53%;float:left;" >
                                    <?php echo trim($eventos[0]->camara ." - ".$eventos[0]->numero);?>
                                </div>
                                <div align="right" style="float:left;" >
                                    <?php
                                    if ($_REQUEST["accion"]=="verTodos"){
                                        //echo "<input class=\"boton\" type=\"button\" value=\"Cerrar\" onclick=\"$('#loadEvento',parent.document).fadeOut('slow');\">";
                                    }else{
                                        echo "<input class=\"boton\" type=\"button\" value=\"Ver Todos\" onclick=\"top.setLoadEvento('loadEventos.php?idcamara=".$_REQUEST["idcamara"]."&accion=verTodos');\">";
                                    }
                                    ?>                            
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <th align="center" width="10%">N&deg;</th>
                            <th>&nbsp;</th>
                            <th>Fecha</th>
                            <th>Estado</th>
                        </tr>
                        <?php
                        foreach ($eventos as $key => $row){

                            echo "<tr id=\"".$row->idevento."|".$row->fecha."|".$row->evento."\" class=\"fila\" onclick=\"oclic(this)\" onmouseover=\"omover(this)\" onmouseout=\"omout(this)\">\n";
                            echo "<td align=\"center\"><b>".(paginationSQL::$start+$key+1)."</b></td>";
                            echo "  <td align=\"center\" >\n";
                            if (preg_match("/timelapse/", $row->file)){
                                //echo "<img src=\"../images/Clock-black-32.png\" />";
                                echo "<img src=\"../images/Clock-32.png\" width=\"20\" title=\"Video generado por lapso de tiempo\"/>";
                            }else{
                                //echo "<img src=\"../images/user_remove-32.png\" />";
                                //echo "<img src=\"../images/Camera-Go-32.png\" />";
                                echo "<img src=\"../images/Video-security-32.png\"  width=\"20\" title=\"Video generado por detecci&oacute;n de movimiento\"/>";
                                
                            }
                            echo "  </td>\n";
                            echo "  <td align=\"center\" >".Controller::formatoFecha($row->fecha)."</td>\n";
                            if ($row->status=="1"){
                                echo "  <td align=\"center\" style=\"color:#006400;\"><b>Atendido</b></td>\n";
                            }else{
                                echo "  <td align=\"center\" style=\"color:#FF0000;\"><b>No atendido</b></td>\n";
                            }

                            echo "</tr>\n";
                        }
                        ?>                
                    </table>  
                    <?php echo paginationSQL::links();?>
                </div>          
            </center>
        </body>
    </html>
    <?php
}
?>