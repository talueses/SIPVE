<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/** * 
 * @author David Concepcion (dcf) 07-10-2010 CENIT
 * Monitoreo de camaras por plantilla previamente seleccionada
 * 
 * @todo reset filds form formComandoCamara
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	echo "<script type=\"text/javascript\" language=\"javascript\">\n";
        echo "\t alert(\"La sesion a expirado\");\n";
        echo "\t window.close();\n";
        echo "</script>";
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past


require_once "../controller/monitorremoto.control.php";
$monitor = new Monitor();


// -------- CAMARAS ASOCIADAS A SEGMENTOS -ZONAS  ------------------------//
$data = $monitor->getCamarasZona($_SESSION['usuario']);
//echo "<div align=\"left\"><pre>".print_r($data,true)."</pre></div>";
$i=0;
foreach ($data as $row){
    $numero = $row->numero;
    if ($row->numero < 10){
        $numero = "0".$row->numero;
    }
    //$camaras["ipv4Servidor-".$i] = "http://".$row->ipv4Servidor.":80".$numero."/";
    //$camaras["ipv4Camara-".$i] = "http://".$row->ipv4Camara."/IMAGE.jpg";
    $auth = null;
    if ($row->cam_usuario_video && $row->cam_clave_video){
        $auth = $row->cam_usuario_video.":".$row->cam_clave_video."@";
    }
    $camaras["ipv4Servidor-".$i] = "http://".$auth.$row->ipv4Camara.":".$row->puerto.$row->urlvideo ;
    $camaras["ipv4Camara-".$i]   = "http://".$row->ipv4Servidor.":80".$numero."/";
    $camaras["camara-".$i] = $row->idcamara; // identificador para buscar los eventos
    $camaras["title-".$i] = $row->camara." - ".$row->numero; // Titulo de la camara
    $camaras["rotacion-".$i] = $row->rotacion; // Titulo de la camara
    $camaras["guardRunning-".$i] = $row->guardRunning; // guardRunning de la camara
    
    // Si el metodo de controles pricipales existe
    $mainControlsMethod = $row->controlFile;
    //echo method_exists("ControlMonitor", strtolower($mainControlsMethod))."---".strtolower($mainControlsMethod)."<br>";
    if (method_exists("CamaraCommands", strtolower($mainControlsMethod))){
        $camaras["mainControlsMethod-".$i] = strtolower($mainControlsMethod);
    } 
    
    // Si el archivo y metodo de controles extra existen
    $extraPanelMethod = $row->controlFile."_extra";
    //echo file_exists(strtolower($extraPanelMethod).".php")." - ".strtolower($extraPanelMethod)." --- ".method_exists("ControlMonitor", strtolower($extraPanelMethod))."<br>";
    if (file_exists(strtolower($extraPanelMethod).".php") && method_exists("CamaraCommands", strtolower($extraPanelMethod))) {
        $camaras["extraPanelMethod-".$i] = strtolower($extraPanelMethod);
    } 
    $i++;
}
//echo "<div align=\"left\"><pre>".print_r($camaras,true)."</pre></div>";
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">        
        <title>Monitor Remoto</title>
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>        
        <script type="text/javascript" src="../js/jquery.mousewheel.js"></script>
        <style type="text/css">
            #plantilla tr td , #plantilla th{                
                background:#ddd;                
                margin: 5px 1px 0px 1px;                
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #plantilla tr th{
                height: 20px;
            }
            #listaEventos{
                background:#fff;
            }
            img,  input[type="image"]{
                background:#fff;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            .BIG{
                cursor: crosshair;                
                background:#fff;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }           
            
        </style>
        <script type="text/javascript" language="javascript">
            $(function(){
                // ------------ Comandos de teclado--------------- //
                $(window).keyup(function (e) {
                    if ($('#keyAccess').val()=='on'){
                        $('#option').val('move');
                        if (e.which == 37){
                            $('#value').val('left');
                        }
                        if (e.which == 38 && !e.ctrlKey){
                            $('#value').val('up');
                        }
                        if (e.which == 39){
                            $('#value').val('right');
                        }
                        if (e.which == 40 && !e.ctrlKey){
                            $('#value').val('down');
                        }
                        if (e.which == 38 && e.ctrlKey){// ZoomIn
                            $('#option').val('rzoom');
                            $('#value').val('1000');
                        }                    
                        if (e.which == 40 && e.ctrlKey){// ZoomOut
                            $('#option').val('rzoom');
                            $('#value').val('-1000'); 
                        }
                        $('#mainControls, #extraPanel').dialog('close');
                        $('#formComandoCamara').submit();
                    }                    
                });
                $( "input:button, input:submit, button" ).button();                
                
                // --- Cerrar cajas de dialogo al mover imagen con click --- //
                $('.BIG').live('click',function(){
                    $('#mainControls, #extraPanel').dialog('close');
                });
                
                // -----------  Comandos de Zoom con Rueda del Mause ----------- // 
                $('.BIG').mousewheel(function(event, delta, deltaX, deltaY) {       
                    if (delta > 0){// ZoomIn
                        $('#value').val('1000');
                    }                        
                    if (delta < 0){// ZoomOut
                        $('#value').val('-1000');                        
                    }
                    event.preventDefault(); // Evita scrooling
                    if (delta){
                        $('#option').val('rzoom');
                        $('#mainControls, #extraPanel').dialog('close');
                        $('#formComandoCamara').submit();
                    }
                });
                
                // --- Caja de Dialogo para listado y observaciones de eventos --- //
                $("#loadEvento").dialog({
                   autoOpen: false,
                   resizable: false,
                   draggable: false,
                   width: 700,
                   height:800,
                   position:'top',
                   modal:true,
                   close: function(event, ui) { 
                       $('#frameEvento').attr('src','');                       
                   }
                });
                
                // --------------- Establecer Paneles de Control -----------------------//
                $( "#mainControls, #extraPanel" ).dialog({ 
                    autoOpen: false,
                    resizable: false,
                    width: 250
                    
                });
                $( "#mainControls" ).dialog({                     
                    height: 280,
                    position:['right','top'],
                    open: function(event, ui) { 
                        $('#frameMainControls').attr('src','mainControlsPanel.php?mainControlsMethod='+$('#mainControlsMethod').val());
                    },
                    close: function(event, ui) { 
                        $('#frameMainControls').attr('src','');
                    }
                });
                $( "#extraPanel" ).dialog({
                    width: 265,
                    height: 510,
                    position:['right','bottom'],
                    open: function(event, ui) { 
                        $('#frameExtraPanel').attr('src',$('#extraPanelMethod').val()+'.php?extraPanelMethod='+$('#extraPanelMethod').val());
                    },
                    close: function(event, ui) { 
                        $('#frameExtraPanel').attr('src','');
                    }
                });
                
            });
            $('body').ready(function(){
                hideLoading();
                ajaxLoadEventos();
                setControlButtons();
            
            });
            // ---- Establecer Botones de Controles ---- //
            function setControlButtons(){
                $( "#mainControlsButton, #extraPanelButton" ).button({ disabled: true });
                //alert(parseInt($('#guardRunning').val()));
                // --- Controles Principales ---//
                if ($('#mainControlsMethod').val() && (parseInt($('#guardRunning').val()) == 0 )){
                    
                    $('#mainControlsButton').button({ disabled: false });
                }
                // --- Controles Extra ---//
                if ($('#extraPanelMethod').val() && (parseInt($('#guardRunning').val()) == 0)){ 
                    $('#extraPanelButton').button({ disabled: false });
                }               
                
            }
            
            function mover(obj){                
                
                $('#frameListaEventos').attr('src','');
                $('#option, #value').val('');
                $('#keyAccess').val('off');
                
                var big = ".BIG";
                var small = "#"+obj.id;

                $(big).fadeOut("slow");
                $(small).fadeOut("slow");
                
                var src                = $(big).attr("src");
                var name               = $(big).attr("name");
                var title              = $(big).attr("title");
                var alt                = $(big).attr("alt");
                var ipv4Servidor       = $(big).attr("ipv4Servidor");
                var ipv4Camara         = $(big).attr("ipv4Camara");
                var mainControlsMethod = $(big).attr("mainControlsMethod");
                var extraPanelMethod   = $(big).attr("extraPanelMethod");
                var rotacion           = $(big).attr("rotacion");
                var guardRunning       = $(big).attr("guardRunning");
                
                //if ($(small).attr("class") != "SMALL"){
                if ($(small).attr("class") == "BIG"){
                    
                    $(big).attr("src",'');
                    //----------------------- IMAGEN GRANDE -------------------------//                    
                    $(big).attr("src",$(small).attr("src"));

                    //----------------------- IMAGEN MEDIANA -------------------------//
                    $(small).attr("src",src);
                }
                
                //if ($(small).attr("class") == "SMALL"){
                if ($(small).attr("class") != "BIG"){

                    $(small).attr('src','');
                    //----------------------- IMAGEN GRANDE -------------------------//                                       
                    $(big).attr("src",$(small).attr("ipv4Servidor"));

                    //----------------------- IMAGEN PEQUEÑA -------------------------//                   
                    $(small).attr("src",$(big).attr("ipv4Camara"));
                }
                
                //----------------------- IMAGEN GRANDE -------------------------//
                $(big).attr({
                    name:               $(small).attr("name"),
                    title:              $(small).attr("title"),
                    alt:                $(small).attr("alt"),
                    ipv4Servidor :      $(small).attr("ipv4Servidor"),
                    ipv4Camara:         $(small).attr("ipv4Camara"),
                    mainControlsMethod: $(small).attr("mainControlsMethod"),
                    extraPanelMethod:   $(small).attr("extraPanelMethod"),
                    rotacion:           $(small).attr("rotacion"),
                    guardRunning:       $(small).attr("guardRunning")
                });
                $('#idcamara').val($(small).attr("name"));// ID de la camara que se esta cargando
                $('#mainControlsMethod').val($(small).attr("mainControlsMethod"));// Archivo de control principal de la camara que se esta cargando                
                $('#extraPanelMethod').val($(small).attr("extraPanelMethod"));// Archivo de control extra de la camara que se esta cargando                
                $('#guardRunning').val($(small).attr("guardRunning"));// Valor que identifica si la camara tiene una guardia activa
                
                
                //----------------------- IMAGEN MEDIANA O EQUEÑA ---------------//
                $(small).attr({
                    name:             name,
                    title:            title,
                    alt:              alt,
                    ipv4Servidor :    ipv4Servidor,
                    ipv4Camara:       ipv4Camara,
                    mainControlsMethod: mainControlsMethod,
                    extraPanelMethod:   extraPanelMethod,
                    rotacion:         rotacion,
                    guardRunning:     guardRunning
                });
                
                $(big).fadeIn("slow");
                $(small).fadeIn("slow");
                $('#rotacion').val($(big).attr("rotacion"));
                $('#mainControls, #extraPanel').dialog('close');
                ajaxLoadEventos();
                setControlButtons();
                                
                return true;
            }
            
            function ajaxLoadEventos(){
                
                var d = new Date();
                $('#frameListaEventos').attr('src','loadEventos.php?idcamara=' + $('.BIG').attr('name')+'&accion=ultimos10&'+d.getMilliseconds());
                setTimeout("ajaxLoadEventos();", 10*1000);
            }

            function setLoadEvento(link){
                $("#frameEvento").attr("src",'');
                var d = new Date();
                $("#frameEvento").attr("src",link+"&"+d.getMilliseconds());
                
                $('#loadEvento').dialog('open');
            }
            
            function loadImg(id){                
                var d = new Date();
                var src = $("#"+id).attr("src");
                var aux = src.split("?");
                $("#"+id).attr("src",aux[0]+"?t="+d.getTime());                
            }

            function errorImg(obj){
                
                var objId = "#"+obj.id;
                if ($(objId).attr("class")=="BIG"){
                    $(objId).attr("src","../images/Video-Camera-640x480.png");
                }
                if ($(objId).attr("class")=="MEDIUM"){
                    $(objId).attr("src","../images/Video-Camera-320x240.png");
                }
                if ($(objId).attr("class")=="SMALL"){
                    $(objId).attr("src","../images/Video-Camera-160x120.png");
                }
            }
            
            function hideLoading(){
                $("#cargando").fadeOut("slow");
            }            
        </script>
    </head>
    <body id="body" style="margin:0px;background:#fff;" onload="//hideLoading();" >
        <div id="cargando" align="center" style="position: absolute;background-color:#000000;opacity:.7;width: 100%;height: 1440px;vertical-align: middle;color: #FFFFFF;font-size: 20px;">
            <br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<img src="../images/loading51.gif" border="0"><br>&nbsp;<b>Cargando...</b>
        </div>
        <div id="loadEvento" title="Listado de Alarmas" >
            <iframe id="frameEvento" src="" width="670" height="750" frameborder="0" style="overflow-x: hidden; overflow-y: scroll "></iframe>
        </div>        
        <div id="contenido" align="center">
            <div align="right" style="float: right;top: auto;position: relative">
                <input type="button" value="Cerrar Ventana" onclick="javascript:window.close();">
            </div>
            <iframe name="commandExec" id="commandExec" width="0" height="0" scrolling="no"></iframe>
            <form action="setComandoCamara.php" name="formComandoCamara" id="formComandoCamara" target="commandExec">
                <table id="plantilla" align="center">
                    <?php
                    if ($_GET["plantilla"]==1){
                        ?>
                        <!---------------------------------------------------- 1 CAMARA-------------------------------------------------------------------------------------->

                        <tr>
                            <td valign="top">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");
                                ?>
                            </td>
                            <td  valign="top" rowspan="2">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 2);
                                ?>                                
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>                        
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==2){
                        ?>
                        <!---------------------------------------------------- 2 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td rowspan="1">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");
                                ?>
                            </td>
                            <td >
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                            <td  valign="top" colspan="2">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 2);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==3){
                        ?>
                        <!---------------------------------------------------- 3 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "2", "M");
                                ?>
                            </td>
                            <td  valign="top" rowspan="3">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 2);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==4){
                        ?>
                        <!----------------------------------------------------4 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td rowspan="1">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");
                                ?>
                            </td>
                            <td >
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "2", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "3", "M");
                                ?>
                            </td>

                        </tr>
                        <tr>
                            <td valign="top" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                            <td  valign="top" colspan="3">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 5);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==5){
                        ?>
                        <!----------------------------------------------------5 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td rowspan="2">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");                        
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "2", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "3", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "4", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                            <td  valign="top" colspan="3">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 4);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==6){
                        ?>
                        <!----------------------------------------------------6 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "2", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "3", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" colspan="2">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "4", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "5", "M");
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <td valign="top" colspan="2" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                            <td  valign="top" colspan="3">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 2);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==7){
                        ?>
                        <!----------------------------------------------------7 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td rowspan="2">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");                        
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "2", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "3", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "4", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "5", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "6", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                            <td  valign="top" colspan="3">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 5);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==9){
                        ?>
                        <!----------------------------------------------------9 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "2", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "3", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "4", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="2">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "5", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "6", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "7", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "8", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                            <td  valign="top" colspan="2">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 4);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    if ($_GET["plantilla"]==12){
                        ?>
                        <!----------------------------------------------------12 CAMARAS-------------------------------------------------------------------------------------->
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "1", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "2", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "3", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "4", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "5", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" colspan="2">
                                <?php
                                echo $monitor->getImg($camaras, "0", "B");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "6", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "7", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "8", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "9", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "10", "M");
                                ?>
                            </td>
                            <td>
                                <?php
                                echo $monitor->getImg($camaras, "11", "M");
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2" bgcolor="#FFFFFF">
                                <div id="listaEventos">
                                    <iframe id="frameListaEventos" src="" width="100%" height="250" frameborder="0" scrolling="no"></iframe>
                                </div>
                            </td>
                            <td  valign="top" colspan="3">
                                <?php
                                //--------------------------------- CAMARAS EXTRA -------------------------------//
                                echo $monitor->getImgExtra($_GET["plantilla"], count($data), $camaras, 5);
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>                                
                
                <input type="hidden" name="option" id="option" value="" />
                <input type="hidden" name="value" id="value" value="" />
                <input type="hidden" name="keyAccess" id="keyAccess" value="off" />      
                <input type="hidden" name="rotacion" id="rotacion" value="<?php echo $camaras["rotacion-0"]?>" />
                <input type="hidden" name="idcamara" id="idcamara" value="<?php echo $camaras["camara-0"]?>" />
                <input type="hidden" name="mainControlsMethod" id="mainControlsMethod" value="<?php echo $camaras["mainControlsMethod-0"]?>" />
                <input type="hidden" name="extraPanelMethod" id="extraPanelMethod" value="<?php echo $camaras["extraPanelMethod-0"]?>" />
                <input type="hidden" name="control" id="control" value="main" />
                <input type="hidden" name="guardRunning" id="guardRunning" value="<?php echo $camaras["guardRunning-0"]?>" />
                
                
            </form>
        </div>
        <div id="mainControls" title="Panel de Control">
            <iframe id="frameMainControls" src="" width="225" height="230" frameborder="0" scrolling="no"></iframe>
        </div>        
        <div id="extraPanel" title="Controles Extra">
            <iframe id="frameExtraPanel" src="" width="240" height="470" frameborder="0" scrolling="no"></iframe>
        </div>
        <script type="text/javascript" language="javascript">
            /*********CARGA DE EVENTOS DE CAMARA CARGADA POR DEF.*******/
            //ajaxLoadEventos();
            /*********CARGA DE IMAGENES PEQUENAS CADA 3 SEG.************/
            /*$(".SMALL").load(function(){
                var id = $(this).attr("id");
                setTimeout("loadImg("+id+")",3*1000);
            });*/
        </script>
    </body>
</html>