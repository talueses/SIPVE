<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Obliga a descargar un archivo
 * @author David Concepcion 20-10-2010 CENIT - DIDI
 */

$file_path = trim(base64_decode($_REQUEST['file']));

$file = @fopen ($file_path, "r");
if (!$file) {
    ?>
    <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
    <script type="text/javascript" language="javascript" charset="UTF-8">
        $(function() {
            var str;
            str  = '<div class="ui-widget">\n';
            str += '    <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">\n';
            str += '        <p>\n';
            str += '            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>\n';
            str += '                El archivo de video no est&aacute; disponible\n';
            str += '        </p>\n';
            str += '    </div>\n';
            str += '</div>\n';
            $('#divmensaje',parent.document).html(str).fadeIn(500);
        });
        //$('#divmensaje',parent.document).css('border','2px solid red');
        //$('#divmensaje',parent.document).html('El archivo de video no est&aacute; disponible');
        
    </script>
    <?php
    exit(0);
}

header('Content-Type: video/x-msvideo');
header('Content-Disposition: attachment; filename="'.basename($file_path).'"');
while (!feof ($file)) {
    echo $line = fgets ($file, 1024);
}
fclose($file);
?>
