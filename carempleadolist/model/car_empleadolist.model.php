<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php'; // conexion Base de Datos

class ModelEmpleadoList{
    
    /**
     * Consulta de los Carnets de Empleados registrados y sus impresiones
     * @param array $arg Complemnto SQL del argumento de la consulta
     * @return object Devuelve registros como objeto
     */
    public function getEmpleadosList($arg){
        $sql = "select /*start*/
                       ce.idcarnet_empleado ,
                       ce.cedula,
                       ce.nombre,
                       ce.apellido,
                       ce.archivo_foto,
                       ce.fecha_vencimiento,
                       cc.cargo_nombre,
                       cc.cargo_color,
                       cc.cargo_bgcolor,
                       cd.departamento_nombre
                       /*end*/
                from car_empleado ce,
                     car_cargo cc, 
                     car_departamento cd
                where ce.idcargo = cc.idcargo
                  and ce.iddepartamento = cd.iddepartamento
                  ".$arg["cond"]."
                  order by cd.departamento_nombre,cc.cargo_nombre,ce.cedula,ce.nombre,ce.apellido";        
        //echo "<div align=\"left\"><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
        
    }
    
    public function loadImpresion($idcarnet_empleado){
        $sql = "select /*start*/
                       cei.idcarnet_empleado_impresion,
                       cei.idcarnet_empleado,
                       cei.usuario,
                       cei.modo,
                       cei.fecha, 
                       u.nombre,
                       u.apellido
                       /*end*/
                from car_empleado_impresion cei, usuario u
                where cei.idcarnet_empleado = '".$idcarnet_empleado."'
                    and cei.usuario = u.usuario
                  order by cei.fecha desc";
        
        //echo "<div align=\"left\"><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
    
}

?>
