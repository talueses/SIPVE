<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CARLISTEMP"); //Categoria del modulo
include_once "../model/car_empleadolist.model.php";
include_once "../../inicio/controller/controller.php";

class ControlEmpleadoList extends Controller {
    /**
     * @var string mensaje de exito o error
     */
    public $mensaje = null;
    /**
     * @var string mensaje de exito o error
     */
    public $error = false;
    /**
     * @var array nombre de campos campos
     */
    var $campos  = null;
    /**
     * @var string nombre de la entidad
     */
    var $entidad  = null;
    /**
     * Establece el nombre de los campos
     * @param string $name Nombre de la posicion del vector
     * @param string $value Valor de la posicion del vector
     */
    public function setCampos($name,$value){
         $this->campos[$name] = $value;
    }

    /**
     * @return array Devuelve el nombre de los campos establecido
     */
    public function getCampos($name){
         return $this->campos[$name];
    }

    /**
     * Establece la Entidad
     * @param string $entidad Entidad
     */
    public function setEntidad($entidad){
         $this->entidad = $entidad;
    }

    /**
     * @return string Devuelve la Entidad establecida
     */
    public function getEntidad(){
         return $this->entidad;
    }
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Listado de carnets de empleados");
        //$this->setCampos("field","fieldName"); 
        $this->setCampos("fecha_desde","Fecha Desde");
        $this->setCampos("fecha_hasta","Fecha Hasta");
    }
    /**
     * Establece los criterios de busqueda del listado de carnet de empleados
     * @return object Devuelve registros como objeto
     */
    function loadData() {
        $this->setNombres();
        
        // ----- Datos Obligatorios --- //
        
        if ($_REQUEST["fecha_desde"] == "" && $_REQUEST["fecha_hasta"] != "" || $_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] == ""){
            
            $datos   = array();
            //------------------------------------------------------------------------------------------------------------//
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_desde","tipoDato"=>"");
            $datos[] = array("isRequired"=>true,"datoName"=>"fecha_hasta","tipoDato"=>"");
            if (!$this->validarDatos($datos)){
                $mensaje = $this->mensaje;
            }

            $this->mensaje  = "<div class=\"ui-widget\">\n";
            $this->mensaje .= "    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">\n";
            $this->mensaje .= "        <p>\n";
            $this->mensaje .= "            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>\n";
            $this->mensaje .= "                ".$mensaje;
            $this->mensaje .= "        </p>\n";
            $this->mensaje .= "    </div>\n";
            $this->mensaje .= "</div>\n";
            $this->error = true;
        }
        
        // ---- Argumento de la consulta segun parametros seleccionados en la vista ---- //
        /**
         * campos: campos a mostrar segun parametros de la vista
         * tablas: tablas 
         * cond:   campos a enlazar segun parametros de la vista
         * @var array 
         */
        $arg = array();
        if ($_REQUEST["cedula_emp"] != ""){
            $arg["cond"] .= " and ce.cedula = '".$_REQUEST["cedula_emp"]."'\n";
        }
        if ($_REQUEST["idcargo"] != ""){
            $arg["cond"] .= " and ce.idcargo = '".$_REQUEST["idcargo"]."'\n";
        }
        if ($_REQUEST["iddepartamento"] != ""){
            $arg["cond"] .= " and ce.iddepartamento = '".$_REQUEST["iddepartamento"]."'\n";
        }
        
        if ($_REQUEST["fecha_desde"] != "" && $_REQUEST["fecha_hasta"] != ""){
            $arg["cond"] .= " and ce.fecha_vencimiento between '".Controller::formatoFecha($_REQUEST["fecha_desde"])." 00:00:00' and '".Controller::formatoFecha($_REQUEST["fecha_hasta"])." 23:59:59'\n";
        }
        
        
        $obj = new ModelEmpleadoList();
        $data = $obj->getEmpleadosList($arg);
        
        return $data;
    }    
}

?>
