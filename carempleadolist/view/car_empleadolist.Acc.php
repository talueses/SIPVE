<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 07-10-2010 CENIT
 * Listado de Carnets registrados de empleados
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/car_empleadolist.control.op.php";
//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";

$obj = new ControlEmpleadoList();
$data = $obj->loadData();

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/jquery.qtip.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.qtip.js"></script>
        <style type="text/css" >
            #tabla {
                font-size: 13px;
                height: 99%;
                width:99%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            .boton{
                font-size: 10px;
            }
            /* Para que no se vean los Botones cuando se ejecute la accion*/
            .ui-button{
                position: inherit;
            }
            img.foto{                
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            .divCargo{
                padding: 0px 5px 0px 5px;
                border: #000000 1px dashed;
                -moz-border-radius: 8px;
                -webkit-border-radius: 8px;
                                border-radius: 8px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {                   
                $( "input:button, input:submit, button" ).button();
                
                $('.fila').qtip({
                    content: function(api) {
                        // Retrieve content from custom attribute of the $('.selector') elements.
                        return '<img class="foto" src="../../carempleado/images/fotos/'+$(this).attr('archivo_foto')+'" alt="Foto" />';
                    },
                    position: {
                        my: 'top left',
                        target: 'mouse',
                        viewport: $(window), // Keep it on-screen at all times if possible
                        adjust: {
                            x: 10,  y: 10
                        }
                    },
                    hide: {
                        fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
                    },
                    style: {
                        classes: 'ui-tooltip-youtube'
                    }
                });
                
                // --- Caja de Dialogo para listado y observaciones de eventos --- //
                $("#loadDetail").dialog({
                   autoOpen: false,
                   resizable: false,
                   draggable: false,
                   width: 500,
                   height:580,
                   position:'top',
                   modal:true,
                   close: function(event, ui) {
                       $('#frameDetail').attr('src','');
                   }
                });
            });
            function setLoadDetail(obj){
                $("#frameDetail").attr('src','');
                var d = new Date();
                $("#frameDetail").attr('src','loadImpresion.php?idcarnet_empleado='+obj.id+'&'+d.getMilliseconds());

                $('#loadDetail').dialog('open');
            }
            filaseleccionada = {};
            function omover(fila){
                if (fila.id===filaseleccionada.id ) return false;
                fila.className = 'pasada';
            }
            function omout(fila){
                if (fila.id===filaseleccionada.id ) return false;
                fila.className = "";
            }
            function oclic(fila){
                alert(fila.id)
            }
        </script>
    </head>
    <body style="margin:0px;">
        <div id="loadDetail" title="Historial de Impresi&oacute;n de carnets" >
            <iframe id="frameDetail" src="" width="480" height="530" frameborder="0" scrolling="auto" ></iframe>
        </div>
        <center>
            <div id="divmensaje" style="width:99%;"><?php echo $obj->mensaje?></div>
            <?php
            if (!$obj->error){
                ?>
                <div id="tabla" align="center">
                    <br>
                    <table id="listado" align="center">
                        <tr>
                            <td id="titulo" colspan="6" align="center"><b>Listado de Carnets registrados de empleados</b></td>
                        </tr>
                        <tr>
                            <th align="center" width="5%">N&deg;</th>
                            
                            <th >C&eacute;dula</th>
                            <th>Nombre</th>
                            <th>Cargo</th>
                            <th>Departamento</th>                            
                            
                        </tr>
                        <?php
                        if (count($data) > 0){
                            foreach ($data as $key => $row){

                                echo "<tr id=\"".$row->idcarnet_empleado."\" class=\"fila\" archivo_foto=".$row->archivo_foto." onclick=\"setLoadDetail(this)\" onmouseover=\"omover(this)\" onmouseout=\"omout(this)\">\n";
                                echo "  <td align=\"center\"><b>".(paginationSQL::$start+$key+1)."</b></td>";
                                //echo "  <td title=\"Foto Carnet\" align=\"center\" ><img src=\"../../carempleado/images/fotos/".$row->archivo_foto."\" alt=\"Foto\" /></td>\n";
                                echo "  <td align=\"center\" >".number_format($row->cedula, 0, "", ".")."</td>\n";
                                echo "  <td align=\"center\" >".$row->nombre." ".$row->apellido."</td>\n";
                                echo "  <td align=\"center\" ><span class=\"divCargo\" style=\"background: #".$row->cargo_bgcolor.";color: #".$row->cargo_color."\">".$row->cargo_nombre."</span></td>\n";
                                echo "  <td align=\"center\" >".$row->departamento_nombre."</td>\n";
                                echo "</tr>\n";
                            }
                        }else{
                            ?>
                            <tr>
                                <td colspan="6">
                                    <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                    
                                </td>
                            </tr>

                            <?php
                        }
                        ?>
                    </table>
                    <?php echo paginationSQL::links();?>
                </div>
                <?php
            }
            ?>            
        </center>
    </body>
</html>