<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/*
 * Autor David Concepcion 07-10-2010 CENIT
 * Listado de Carnets registrados de empleados
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/car_empleadolist.control.op.php";
//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
$obj = new ModelEmpleadoList();

$dataEmp = $obj->getEmpleadosList(array("cond"=>" and ce.idcarnet_empleado = '".$_REQUEST["idcarnet_empleado"]."'"));

$data = $obj->loadImpresion($_REQUEST["idcarnet_empleado"]);


?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/jquery.qtip.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.qtip.js"></script>
        <style type="text/css" >
            #tabla {
                font-size: 13px;
                height: 99%;
                width:99%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden                

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            .listado{
                width:99%;
                background:#fff;
                margin: 6px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            .listado tr {
                    background:#fff;
            }
            .listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }

            .listado tr.chequeada {
                    background:#dd0;
            }
            .listado tr.pasada {
                    background:#dd0;
                    cursor:default;
            }
            .boton{
                font-size: 10px;
            }
            /* Para que no se vean los Botones cuando se ejecute la accion*/
            .ui-button{
                position: inherit;
            }
            img.foto{         
                border: #000000 1px dashed;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
            .divCargo{
                padding: 0px 5px 0px 5px;
                border: #000000 1px dashed;
                -moz-border-radius: 8px;
                -webkit-border-radius: 8px;
                                border-radius: 8px;
            } 
            .usuarioTip{
                display: none;
            }
            .divCargo{
                padding: 0px 5px 0px 5px;
                border: #000000 1px dashed;
                -moz-border-radius: 8px;
                -webkit-border-radius: 8px;
                                border-radius: 8px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {                   
                $( "input:button, input:submit, button" ).button();    
                $('.colUsuario').qtip({
                    content: function(api) {
                        return $('#usuarioTip'+$(this).attr('id'));
                    },
                    position: {
                        my: 'top left',
                        target: 'mouse',
                        viewport: $(window), // Keep it on-screen at all times if possible
                        adjust: {
                            x: 10,  y: 10
                        }
                    },
                    hide: {
                        fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
                    },
                    style: {
                        classes: 'ui-tooltip-youtube ' // 
                    }
                });
            });
            
            filaseleccionada = {};
            function omover(fila){
                if (fila.id===filaseleccionada.id ) return false;
                fila.className = 'pasada';
            }
            function omout(fila){
                if (fila.id===filaseleccionada.id ) return false;
                fila.className = "";
            }
        </script>
    </head>
    <body style="margin:0px;background-color: #fff">
        <center>            
            <div id="tabla" align="center">
                
                <table class="listado top" align="center" border="0">
                    
                    <tr>
                        <td rowspan="5" width="28%" align="center">
                            <img class="foto" src="../../carempleado/images/fotos/<?php echo $dataEmp[0]->archivo_foto?>" alt="Foto" />
                        </td>
                        <td align="left"><?php echo $dataEmp[0]->nombre." ".$dataEmp[0]->apellido?></td>
                    </tr>
                    <tr>
                        <td align="left">C.I. <?php echo number_format($dataEmp[0]->cedula, 0, "", ".")?></td>
                    </tr>
                    <tr>
                        <td align="left"><?php echo $dataEmp[0]->departamento_nombre?></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="divCargo" style="background: <?php echo "#".$dataEmp[0]->cargo_bgcolor.";color: #".$dataEmp[0]->cargo_color.";";?>"><?php echo $dataEmp[0]->cargo_nombre?></span></td>
                    </tr>
                    <tr>
                        <td>
                            <img src="car_empleadolist.barcodeImage.php?barcode=<?php echo $dataEmp[0]->cedula?>" alt="barcode">                            
                        </td>
                    </tr>
                </table>
                
                <table class="listado" align="center">                        
                    <tr>
                        <th align="center" width="5%">N&deg;</th>                            
                        <th >Usuario</th>
                        <th>Modo</th>   
                        <th>Fecha</th>                            
                    </tr>
                    <?php
                    if (count($data) > 0){
                        foreach ($data as $key => $row){

                            echo "<tr  class=\"fila\" onmouseover=\"omover(this)\" onmouseout=\"omout(this)\">\n";
                            echo "  <td align=\"center\"><b>".(paginationSQL::$start+$key+1)."</b></td>";
                            echo "  <td align=\"center\" id=\"".$row->idcarnet_empleado_impresion."\" class=\"colUsuario\" >".$row->usuario."<div class=\"usuarioTip\" id=\"usuarioTip".$row->idcarnet_empleado_impresion."\">".$row->nombre." ".$row->apellido."</div></td>\n";
                            
                            if ($row->modo=="F"){
                                $modo = "Frente";
                            }
                            if ($row->modo=="B"){
                                $modo = "Reverso";
                            }
                            
                            echo "  <td align=\"center\" >".$modo."</td>\n";   
                            echo "  <td align=\"center\" >".Controller::formatoFecha($row->fecha)."</td>\n";                                
                            echo "</tr>\n";
                        }
                    }else{
                        ?>
                        <tr>
                            <td colspan="6">
                                <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>

                            </td>
                        </tr>

                        <?php
                    }
                    ?>
                </table>
                <?php echo paginationSQL::links();?>
            </div>        
        </center>
    </body>
</html>
