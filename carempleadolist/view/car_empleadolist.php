<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de reporte: Listado de Carnets registrados de empleados
 * @author David Cocepcion CENIT-DIDI
 */
session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/car_empleadolist.control.op.php";// Class CONTROLLER


?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>VidAccesopcs</title>
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.ui.datepicker-es.js"></script>
        <style type="text/css" >
            #tabla{
                float:left;
                width:30%;
                height:95%;
            }
            #listado{
                width:99%;
                margin: 0px 1px 0px 1px;
            }
            #titulo {
                height: 30px;                
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                text-align: right;
                
            }
            
            #botones{
                margin: 10px;
            }

            select{
                width: 100%;
            }
            select option[value=""]{
                font-weight: bold;
                color: #0000FF;
            }

        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button,input:reset" ).button();
                
                $('#listado tr th').addClass('ui-corner-left ui-state-focus');
                
                $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
                var dates = $('#fecha_desde, #fecha_hasta').datepicker({                    
                    inline: true,
                    dateFormat: 'dd-mm-yy',
                    maxDate:'d m y',
                    changeMonth: true,
                    changeYear: true,

                    // Range
                    onSelect: function( selectedDate ) {
                        var option = this.id == "fecha_desde" ? "minDate" : "maxDate",
                            instance = $( this ).data( "datepicker" ),
                            date = $.datepicker.parseDate(
                                    instance.settings.dateFormat ||
                                    $.datepicker._defaults.dateFormat,
                                    selectedDate, instance.settings );
                        dates.not( this ).datepicker( "option", option, date );
                    }
                }).attr('readonly',true);
            });
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <div id="tabla" align="center" class="ui-widget-content ui-corner-all">
                <?php
                if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                    echo Controller::$mensajePermisos;
                }else{
                    ?>
                    <form name="f1" id="f1" target="frm1" action="car_empleadolist.Acc.php" method="get">
                        <table id="listado" class="ui-widget-content ui-corner-all">
                            <tr>
                                <td id="titulo" class="ui-widget-header ui-corner-all" colspan="3" align="center"><b>Listado de Carnets registrados de empleados</b></td>
                            </tr>
                            <tr>
                                <th >C&eacute;dula</th>
                                <td>
                                    <input type="text" name="cedula_emp" id="cedula_emp" maxlength="8" >
                                </td>
                            </tr>
                            <tr>
                                <th>Cargo</th>
                                <td>
                                    <select name="idcargo" id="idcargo">
                                        <option value="">&laquo; Todos &raquo;</option>
                                        <?php echo Controller::make_combo("car_cargo", "order by cargo_nombre", "idcargo", "cargo_nombre", "");?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Departamento</th>
                                <td>
                                    <select name="iddepartamento" id="iddepartamento">
                                        <option value="">&laquo; Todos &raquo;</option>
                                        <?php echo Controller::make_combo("car_departamento", "order by departamento_nombre", "iddepartamento", "departamento_nombre", "");?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>F. Vencimiento<small class="comment">Rango de Fechas</small></th>
                                <td>
                                    <input type="text" name="fecha_desde" id="fecha_desde" size="10">:<input type="text" name="fecha_hasta" id="fecha_hasta" size="10">
                                </td>
                            </tr>
                        </table>
                        <div id="botones" >
                            <center>
                                <input type="hidden" name="accion" id="accion" value="">
                                <input type="submit" value="Aceptar" class="boton">
                                <input type="reset" value="Cancelar" class="boton">
                                <input type="submit" value="Ayuda" onclick="this.form.action=''" class="boton">
                            </center>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
            <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                <iframe name="frm1" id="frm1" frameborder="0" scrolling="no" style="width:100%; height:650px" src=""></iframe>
            </div>
        </div>
    </body>
</html>