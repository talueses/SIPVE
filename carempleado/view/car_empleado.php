<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Listado de carets de empleados
 * @author David Cocepcion CENIT-DIDI
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

require_once "../controller/car_empleado.control.op.php";// Class CONTROLLER

$obj  = new CarEmpleados();
$data = $obj->getCarnetsEmpleados();

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">
        <title>CarEmpleados</title>        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <link href="../../inicio/css/jquery.qtip.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery.qtip.js"></script>
        <style type="text/css" >
            #tabla{
                float:left;
                width:30%;
                height:95%;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado{
                width:99%;
                background:#fff;
                margin: 0px 1px 0px 1px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 30px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #listado tr {
                    background:#fff;
            }
            #listado tr th {
                    border: 1px solid #d3d3d3; background: #e6e6e6 url(../../inicio/css/images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x; font-weight: normal; color: #555555;
                    -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }

            #listado tr.chequeada {
                    background:#dd0;
            }
            #listado tr.pasada {
                    background:#dd0;
                    cursor:pointer;
            }
            #botones{
                margin: 10px;
            }
            #buttonPrint{
                width: 80px;
                padding: 5px 5px 5px 5px;
                border: #aaaaaa dashed 2px ;
                margin-top: 10px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;

            }
            img.foto{                
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
                                border-radius: 6px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            $(function() {
                $( "input:button, input:submit,button" ).button();
                
                
                $('.rows').qtip({
                    content: function(api) {
                        // Retrieve content from custom attribute of the $('.selector') elements.
                        return '<img class="foto" src="../../carempleado/images/fotos/'+$(this).attr('archivo_foto')+'" alt="Foto" />';
                    },
                    position: {
                        my: 'top left',
                        target: 'mouse',
                        viewport: $(window), // Keep it on-screen at all times if possible
                        adjust: {
                            x: 10,  y: 10
                        }
                    },
                    hide: {
                        fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
                    },
                    style: {
                        classes: 'ui-tooltip-youtube'
                    }
                });
                
            });
            function visualizar(fila){
                var aux = fila.id.split('-');
                $('#idcarnet_empleado').val(aux[0]);
                $('#idplantilla').val(aux[1]);
                $('#accion').val('visualizar');
                $('#f1').attr('action','car_empleado.Acc.php');
                $('#f1').submit();
            }

            filaseleccionada = {};
            function oclic(fila){
                    filaseleccionada.className='';
                    filaseleccionada = fila;
                    $('#'+fila.id).addClass('chequeada');
            }

            function omover(fila){                
                    if (fila.id===filaseleccionada.id ) return false;                                        
                    $('#'+fila.id).addClass('pasada');
            }
            
            function omout(fila){
                    if (fila.id===filaseleccionada.id ) return false;                    
                    $('#'+fila.id).removeClass('pasada');
            }
            function Accion(acc){
                $('#accion').val(acc);
                $('#f1').attr('action','car_empleado.Acc.php');

                if (acc == "agregar"){
                    $('#idcarnet_empleado, #idplantilla').val('');
                    $('#listado tr').each(function(){
                        $(this).attr("className",'');
                    });
                }
                
                if (acc == "modificar" || acc == "eliminar" || acc == "imprimir"){
                    if ($('#idcarnet_empleado').val()==""){
                        alert("Debe seleccionar un registro de la lista");
                        return false
                    }
                    if (acc == "eliminar"){
                        if (!confirm("\xbf Esta seguro que desea eliminar el registro ?")){
                            return false;
                        }
                        $('#f1').attr('action','car_empleado.Op.php');
                    }
                    if (acc == "imprimir"){                        
                        $('#f1').attr('action','car_empleado.prePrint.php');
                    }
                }

                $('#f1').submit();
                return true;
            }
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="principal" style="width:100%;height:650px;border:#000000 solid 1px;" >
            <?php
            Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"");
            if (!Controller::$chkCookie["CarnetsEmp"]){
                echo "<table id=\"listado\"><tr><td  align=\"center\" height=\"620\" valign=\"top\"> ".Controller::$mensajePermisos."</td></tr></table>";                
            }else{            
                ?>
                <div id="tabla" align="center">
                    <?php
                    if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"listar")){
                        echo Controller::$mensajePermisos;
                    }else{
                        echo Buscador::getObjBuscador("idcarnet_empleado", "cedula,nombre,apellido", "car_empleado", "order by cast(cedula as :cast_int),nombre,apellido");
                        ?>
                        <table id="listado">
                                <tr>
                                    <td id="titulo" colspan="4" align="center"><b>Carents de Empleados</b></td>
                                </tr>
                                <tr>
                                    <th align="center" width="10%">N&deg;</th>
                                    <th>C&eacute;dula</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>

                                </tr>
                                <?php
                                if (count($data) > 0){
                                    foreach ($data as $key => $row){?>
                                    <tr id="<?php echo $row->idcarnet_empleado."-".$row->idplantilla;?>" class="rows" archivo_foto="<?php echo $row->archivo_foto;?>" onclick="visualizar(this);oclic(this)" onmouseover="omover(this)" onmouseout="omout(this)">
                                            <td align="center"><b><?php echo paginationSQL::$start+$key+1;?></b></td>                                            
                                            <td><?php echo $row->cedula;?></td>
                                            <td><?php echo $row->nombre;?></td>
                                            <td><?php echo $row->apellido;?></td>

                                        </tr>
                                    <?php
                                    }
                                }else{
                                    ?>
                                    <tr>
                                        <td colspan="4">
                                            <div id="divmensaje" style="width:99%;border:2px solid red" align="center">No se encontraron registros</div>
                                        </td>
                                    </tr>

                                    <?php
                                }
                                ?>

                        </table>
                        <?php echo paginationSQL::links();?>
                        <div id="botones" >
                            <center>
                                <form name="f1" id="f1" target="frm1" action="car_empleado.Acc.php" method="POST">
                                    <input type="hidden" name="idcarnet_empleado" id="idcarnet_empleado" value="">
                                    <input type="hidden" name="idplantilla" id="idplantilla" value="">
                                    <input type="hidden" name="accion" id="accion" value="">
                                    <?php
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"agregar")){
                                        ?><input type="submit" value="Agregar" onclick="return Accion('agregar')"  class="boton"><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"modificar")){
                                        ?><input type="submit" value="Modificar" onclick="return Accion('modificar')" class="boton"><?php
                                    }
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"eliminar")){
                                        ?><input type="button" value="Eliminar" onclick="return Accion('eliminar')" class="boton"><?php
                                    }
                                    echo "<input type=\"submit\" value=\"Ayuda\" onclick=\"this.form.action='car_empleado.Ayuda.php'\" class=\"boton\">";
                                    if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"imprimir")){
                                        ?>
                                        <div id="buttonPrint" align="center" >
                                            <button type="button" title="Imprimir Carnet" onclick="return Accion('imprimir')">
                                                <img src="../images/Gnome-Printer-48.png" alt="Print"/>
                                            </button>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </form>
                            </center>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div id="detalle" style="float:left;width:69%;height:650px;border-left:#000000 solid 1px;">
                    <iframe name="frm1" id="frm1" frameborder="0" scrolling="no" style="width:100%; height:650px" src="car_empleado.Acc.php?accion=agregar"></iframe>
                </div>            
                <?php
            }
            ?>
        </div>
    </body>
</html>