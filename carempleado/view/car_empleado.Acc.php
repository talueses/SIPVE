<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
/**
 * Formulario de acciones agregar, modificar y visualizar datos
 * @author David Concepcion
 */

session_start(); // start up your PHP session!
if(!isset($_SESSION['usuario'])){
	header("location: login.php",true);
	return false;
}
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

//echo "<div align='left'><pre>".print_r($_REQUEST,true)."</pre></div>";
require_once "../controller/car_empleado.control.op.php";// Class CONTROLLER
$obj = new CarEmpleados();
if (!$_REQUEST["idcarnet_empleado"]){
    $_REQUEST["idcarnet_empleado"] = "0";
}
if (!$_REQUEST["idplantilla"]){
    $_REQUEST["idplantilla"] = "0";
}
$data = $obj->getCarPlantillaEmpleado(array("idplantilla"=>$_REQUEST["idplantilla"],"idcarnet_empleado"=>$_REQUEST["idcarnet_empleado"]));
//echo "<div align='left'><pre>".print_r($data,true)."</pre></div>";
if (!$data ){
    echo $obj->getMensaje();
}
$disabled = "";
if ($_REQUEST["accion"] == "visualizar"){
    $disabled = "disabled";
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8 ">        
        <link type="text/css" href="../../inicio/css/jquery-ui.css" rel="stylesheet" />
        <link href="../../inicio/css/comunes.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../inicio/js/jquery.js"></script>
        <script type="text/javascript" src="../../inicio/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/jshashtable.js"></script>
        <script type="text/javascript" src="../js/jquery.numberformatter-1.2.2.js"></script>
        <script type="text/javascript" src="../js/car_empleado.local.js"></script>
        
        <!-- IU DatePicker jQuery Calendar Plugin -->
        <script type="text/javascript" src="../../inicio/js/jquery.ui.datepicker-es.js"></script>
        
        <style type="text/css">
            .list{
                color: #000000;
                font-style: oblique;
                background-color: #f0ebe2;
                width: 300px;
                border: 1px solid #ccc0a9;
            }
            #contenido{
                float:left;
                width:99%;
                height:95%;
                padding: 2px;
                border:  #aaaaaa solid 1px;
                overflow-x:hidden
                width:95%;

                background:#fff;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #titulo{
                height: 20px;
                border: 1px solid #aaaaaa; background: #cccccc url(../../inicio/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x; color: #222222; font-weight: bold;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #datos{
                width: 98%;
                background:#fff;
                margin: 5px 1px 0px 1px;
                padding: 5px;
                border:  #aaaaaa solid 1px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
            }
            #botones{
                margin: 10px;
            }
            #divmensaje{
                width:99%;
                position: absolute;
                top: -10px;
                opacity:0.9;
            }
            /*-------------------------------------------------------------------------------*/
            #divCarnet{
                /*border:#000000 2px dashed;*/
                position: relative;
            }
            /*.carnetObjects:hover{
                opacity:0.5;
            }*/
            .carnetObjects{
                /*border:#000000 1px dashed;*/
                position: absolute;
            }
            .carnetObjectsView{
                position: relative;
                width: 100%;
                height: 100%;
                display: table;
            }
            .carnetObjectsView p{
                display: table-cell;
            }

            .ui-resizable-se {
                right: 1px;
                bottom: 1px;
            }
            #buttonFoto{
                width: 80px;
                padding: 5px 5px 5px 5px;
                border: #aaaaaa dashed 2px ;
                margin-bottom: 10px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
				border-radius: 6px;
                
            }
        </style>
        <script type="text/javascript" language="javascript">
                           
        </script>
    </head>
    <body style="margin:0px;background:#ddd;">
        <div id="loadFoto" title="Captura o Carga de Foto" >
            <iframe id="frameFoto" width="100%" height="550" frameborder="0" scrolling="auto" ></iframe>
        </div>
        <div id="contenido" align="center">            
            <?php
            if (!Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,$_REQUEST["accion"])){
                echo Controller::$mensajePermisos;
            }else{
                ?>
                <div id="titulo" style="padding: 5px" ><?php echo ucfirst($_REQUEST["accion"] );?> Carnet</div>
                <br/>
                <div id="divmensaje"></div>
                <iframe name="ifrm1" id="ifrm1" frameborder="0" width="0" height="0"  scrolling="no"></iframe>
                <form method="POST" name="f1" action="car_empleado.Op.php" target="ifrm1">
                    Plantilla 
                    <select name="idplantilla" id="idplantilla" <?php echo $disabled;?>>
                        
                        <?php
                        if ($_REQUEST["accion"]!="agregar"){
                            //$arg = " where idplantilla = '".$_REQUEST["idplantilla"]."' ";
                        }else{
                            //echo "<option value=\"\">Seleccione</option>\n";
                        }echo "<option value=\"\">Seleccione</option>\n";
                        echo ControlCarEmpleado::make_combo("car_plantilla",$arg." order by plantilla", "idplantilla", "plantilla", $data->idplantilla,false);

                        ?>
                    </select>
                    <div id="datos"  align="left">
                        <table border="0">
                            <tr>
                                <td rowspan="2" valign="top">
                                    <div id="divCarnet">
                                        <img src="../../carplantilla/images/imagesPlantillas/<?php echo $data->archivo_plantilla;?>" id="carnetImg" alt="" />    
                                        <div id="foto" class="carnetObjects empleado" title="Foto tipo carnet"></div>
                                        <div id="logo" class="carnetObjects" title="Logo de la instituci&oacute;n"></div>

                                        <div id="institucion" class="carnetObjects" title="Nombre de la instituci&oacute;n" >
                                            <div class="carnetObjectsView" >
                                                <p id="institucion_view"><?php echo $data->institucion_nombre;?></p>
                                            </div>
                                        </div>
                                        <div id="nombreapellido" class="carnetObjects empleado" title="Primer Nombre y primer Apellido">
                                            <div class="carnetObjectsView">
                                                <p id="nombreapellido_view">Nombre 1 Apellido 1</p>
                                            </div>
                                        </div>
                                        <div id="cedula" class="carnetObjects empleado" title="C&eacute;dula de Indentidad">
                                            <div class="carnetObjectsView">
                                                <p id="cedula_view">C.I. XX.XXX.XXX</p>
                                            </div>
                                        </div>
                                        <div id="departamento" class="carnetObjects empleado" title="Departamento">
                                            <div class="carnetObjectsView">
                                                <p id="departamento_view"></p>
                                            </div>
                                        </div>
                                        <div id="cargo" class="carnetObjects empleado" title="Departamento">
                                            <div class="carnetObjectsView">
                                                <p id="cargo_view" ></p>
                                            </div>
                                        </div>
                                        <div id="barcode" class="carnetObjects" title="C&oacute;digo de Barras">
                                            <img src="" width="100%" height="100%" alt="barcode" />
                                        </div>
                                        <div id="fecha" class="carnetObjects empleado" title="Fecha de Expiraci&oacute;n">
                                            <div class="carnetObjectsView">
                                                <p id="fecha_view"><?php echo $data->fecha_vencimiento ? Controller::formatoFecha($data->fecha_vencimiento) : date("d-m-Y");?></p>
                                            </div>
                                        </div>
                                        <!--

                                        -->
                                    </div>
                                </td>
                            </tr>
                            <tr >
                                <td valign="top" align="center">
                                    <table>
                                        <tr title="Departamento">
                                            <td align="right">Departamento:</td>
                                            <td>
                                                <select name="departamento_id" id="departamento_id" <?php echo $disabled;?>  class="nombresCombos">
                                                    <?php echo ControlOpCarEmpleado::comboLocal("idplantilla, iddepartamento as id, departamento_nombre as nombre, departamento_top as top, departamento_left as left_, departamento_w as width, departamento_h as height, departamento_color as color, departamento_fuentetamano as fuentetamano, departamento_fuenteletra as fuenteletra, departamento_bgcolor_chk as bgcolor_chk, departamento_bgcolor as bgcolor, departamento_fuentealign as fuentealign, departamento_fuentevalign as fuentevalign", "car_departamento", "where idplantilla = '".$_REQUEST["idplantilla"]."' order by departamento_nombre", $data->iddepartamento);?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr title="Cargo">
                                            <td align="right">Cargo:</td>
                                            <td>
                                                <select name="cargo_id" id="cargo_id" <?php echo $disabled;?>  class="nombresCombos">
                                                    <!--option value="" top="" left="" width="" height="" color="" bgcolor="">Seleccione</option-->
                                                    <?php echo ControlOpCarEmpleado::comboLocal("idplantilla, idcargo as id, cargo_nombre_chk as chk, cargo_nombre as nombre, cargo_top as top, cargo_left as left_, cargo_w as width, cargo_h as height, cargo_color as color, cargo_bgcolor as bgcolor, cargo_fuentetamano as fuentetamano, cargo_fuenteletra as fuenteletra, cargo_bgcolor as bgcolor, cargo_fuentealign as fuentealign, cargo_fuentevalign as fuentevalign", "car_cargo", "where idplantilla = '".$_REQUEST["idplantilla"]."' order by cargo_nombre", $data->idcargo);?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr title="C&eacute;dula">
                                            <td align="right">C&eacute;dula:</td>
                                            <td>
                                                <input type="text" name="cedula_emp" id="cedula_emp" class="nombres" maxlength="8" value="<?php echo $data->cedula;?>" <?php echo $disabled;?>>
                                                &nbsp;
                                                <span id="cedulaMsg">
                                                    <?php
                                                    if ($data->access!=""){
                                                        echo "<img src=\"../images/Credit-Card-UI-32.png\" alt=\"Tarjeta de Acceso Asignada\" title=\"Tarjeta de Acceso Asignada\" style=\"position: absolute;\" width=\"32\"/>";
                                                    }
                                                    ?>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr title="Nombre">
                                            <td align="right">Nombre:</td>
                                            <td>
                                                <input type="text" name="nombre" id="nombre" class="nombres" maxlength="50" value="<?php echo $data->nombre;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <tr title="Apellido">
                                            <td align="right">Apellido:</td>
                                            <td>
                                                <input type="text" name="apellido" id="apellido" class="nombres" maxlength="50" value="<?php echo $data->apellido;?>" <?php echo $disabled;?>>
                                            </td>
                                        </tr>
                                        <?php
                                        if ($data->fecha_chk=="1"){
                                            ?>
                                            <tr title="Fecha de Vencimiento">
                                                <td align="right">Fecha de Vencimiento:</td>
                                                <td>
                                                    <input type="text" name="fecha_vencimiento" id="fecha_vencimiento" maxlength="10" value="<?php echo $data->fecha_vencimiento ? Controller::formatoFecha($data->fecha_vencimiento) : date("d-m-Y");?>" <?php echo $disabled;?>>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                    <div id="botones" style="clear:left">
                                        <input type="hidden" name="archivo_foto" id="archivo_foto" value="<?php echo $data->archivo_foto;?>" >
                                        <input type="hidden" name="orientacion" id="orientacion" value="<?php echo $data->orientacion ?>">    
                                        <input type="hidden" name="archivo_logo" id="archivo_logo" value="<?php echo $data->archivo_logo ?>">
                                        
                                        <input type="hidden" id="defaultWidth" value="<?php echo Controller::$defaultWidth ;?>">
                                        <input type="hidden" id="defaultHeight" value="<?php echo Controller::$defaultHeight ;?>">

                                        <input type="hidden" name="logo_chk" id="logo_chk" class="chkObj" value="<?php echo $data->logo_chk;?>">
                                        <input type="hidden" name="institucion_chk" id="institucion_chk" class="chkObj" value="<?php echo $data->institucion_chk;?>">
                                        <input type="hidden" name="fecha_chk" id="fecha_chk" class="chkObj" value="<?php echo $data->fecha_chk ?>">
                                        <input type="hidden" name="barcode_front" id="barcode_front" class="chkObj" value="<?php echo $data->barcode_front ?>">
                                        <input type="hidden" name="barcode_back" id="barcode_back" value="<?php echo $data->barcode_back ?>">


                                        <!--------------------------------------------------FOTO----------------------------------------------------->
                                        <input type="hidden" name="foto_top" id="foto_top" value="<?php echo $data->foto_top;?>" >
                                        <input type="hidden" name="foto_left" id="foto_left" value="<?php echo $data->foto_left;?>" >
                                        <input type="hidden" name="foto_w" id="foto_w" value="<?php echo $data->foto_w;?>" >
                                        <input type="hidden" name="foto_h" id="foto_h" value="<?php echo $data->foto_h;?>" >

                                        <!---------------------------------------------------LOGO------------------------------------------------------>
                                        <input type="hidden" name="logo_top" id="logo_top" value="<?php echo $data->logo_top;?>" >
                                        <input type="hidden" name="logo_left" id="logo_left" value="<?php echo $data->logo_left;?>" >
                                        <input type="hidden" name="logo_w" id="logo_w" value="<?php echo $data->logo_w;?>" >
                                        <input type="hidden" name="logo_h" id="logo_h" value="<?php echo $data->logo_h;?>" >


                                        <!---------------------------------------------------INSTITUCION------------------------------------------------>
                                        <input type="hidden" name="institucion_top" id="institucion_top" value="<?php echo $data->institucion_top;?>" >
                                        <input type="hidden" name="institucion_left" id="institucion_left" value="<?php echo $data->institucion_left;?>" >
                                        <input type="hidden" name="institucion_w" id="institucion_w" value="<?php echo $data->institucion_w;?>" >
                                        <input type="hidden" name="institucion_h" id="institucion_h" value="<?php echo $data->institucion_h;?>" >

                                        <!---------------------------------------------------NOMBRE APELLIDO--------------------------------------------->
                                        <input type="hidden" name="nombreapellido_top" id="nombreapellido_top" value="<?php echo $data->nombreapellido_top;?>" >
                                        <input type="hidden" name="nombreapellido_left" id="nombreapellido_left" value="<?php echo !isset($data->nombreapellido_left)?"10":$data->nombreapellido_left;?>" >
                                        <input type="hidden" name="nombreapellido_w" id="nombreapellido_w" value="<?php echo $data->nombreapellido_w;?>" >
                                        <input type="hidden" name="nombreapellido_h" id="nombreapellido_h" value="<?php echo $data->nombreapellido_h;?>" >

                                        <!---------------------------------------------------CEDULA----------------------------------------------------->
                                        <input type="hidden" name="cedula_top" id="cedula_top" value="<?php echo $data->cedula_top;?>" >
                                        <input type="hidden" name="cedula_left" id="cedula_left" value="<?php echo $data->cedula_left;?>" >
                                        <input type="hidden" name="cedula_w" id="cedula_w" value="<?php echo $data->cedula_w;?>" >
                                        <input type="hidden" name="cedula_h" id="cedula_h" value="<?php echo $data->cedula_h;?>" >

                                        <!--------------------------------------------------BARCODE----------------------------------------------------->
                                        <input type="hidden" name="barcode_top" id="barcode_top" value="<?php echo $data->barcode_top;?>" >
                                        <input type="hidden" name="barcode_left" id="barcode_left" value="<?php echo $data->barcode_left;?>" >
                                        <input type="hidden" name="barcode_w" id="barcode_w" value="<?php echo $data->barcode_w;?>" >
                                        <input type="hidden" name="barcode_h" id="barcode_h" value="<?php echo $data->barcode_h;?>" >

                                        <!--------------------------------------------------FECHA------------------------------------------------------->
                                        <input type="hidden" name="fecha_top" id="fecha_top" value="<?php echo $data->fecha_top;?>" >
                                        <input type="hidden" name="fecha_left" id="fecha_left" value="<?php echo $data->fecha_left;?>" >
                                        <input type="hidden" name="fecha_w" id="fecha_w" value="<?php echo $data->fecha_w;?>" >
                                        <input type="hidden" name="fecha_h" id="fecha_h" value="<?php echo $data->fecha_h;?>" >

                                        <!--------------------------------------------------CONF ESTILOS------------------------------------------------>
                                        <?php
                                        
                                        $ops = array("institucion","nombreapellido","cedula","fecha");
                                        echo ControlOpCarEmpleado::setStyleInputs($ops,$data);
                                        ?>
                                        
                                        <input type="hidden" name="accion" id="accion" value="<?php echo $_REQUEST["accion"];?>">
                                        <input type="hidden" name="idcarnet_empleado" id="idcarnet_empleado" value="<?php echo $_REQUEST["idcarnet_empleado"];?>">
                                        
                                        <?php
                                        if ($_REQUEST["accion"] == "agregar" || $_REQUEST["accion"] == "modificar"){
                                            
                                            if (Controller::chkPermiso($_SESSION["usuario"], CATEGORIA,"cargarFoto")){
                                                ?>
                                                <div id="buttonFoto" align="center">
                                                    <button type="button" onclick="$('#loadFoto').dialog('open');" title="Captura o Carga de Foto">
                                                        <img src="../images/Camera-48.png" alt="setPhoto" />
                                                    </button>
                                                </div>
                                                <?php
                                            }
                                            ?>                                            
                                            <input type="submit" value="<?php echo ucfirst($_REQUEST["accion"])?> Carnet" />
                                            <?php                                            
                                        }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                        </table>                         
                    </div> 
                </form>
                <?php
            }
            ?>
        </div>
    </body>
</html>