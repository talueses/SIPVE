<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
define("CATEGORIA", "CAREMP"); //Categoria del modulo
require_once "car_empleado.control.php";// Class CONTROL ControlCarEmpleado()

/**
 * Description
 * @author David Concepcion
 * @name ControlOpCarEmpleado
 */
class ControlOpCarEmpleado extends ControlCarEmpleado{
    
    
    
    /**
     * Proceso de registrar o actualizar registro
     * @return boolean Devuelve verdadero si se registra o actualiza correctamente
     */
    function setOpCarEmpleado(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();        

        $_REQUEST["iddepartamento"] = $_REQUEST["departamento_id"];
        $_REQUEST["idcargo"] = $_REQUEST["cargo_id"];
        $_REQUEST["cedula"] = $_REQUEST["cedula_emp"];
        
        if ($_REQUEST["idcarnet_empleado"]=="0"){
            $_REQUEST["idcarnet_empleado"] = "";
        }        

        //------------------ Metodo Set  -----------------//
        if(!$this->setCarEmpleado()) return false;
        
        // --- Limpiar cache de fotos generado por el modulo FotoLoader --- //
        //$folder = "../images/fotos/";
        $folder = $this->getFolders("fotosCarnet");
        for ($i=1;$i<=5;$i++){
            
            $file = $folder."E".$_REQUEST["cedula"]."-crop-".$i.".png";
            // --- borrar archivo --- //
            if (($folder.$_REQUEST["archivo_foto"]) != $file){
                @unlink($file);
            }
        }

        return true;        
    }
    
    /**
     * Proceso de eliminar registro
     * @return boolean Devuelve verdadero si el proseco de eliminar se ejecuta exitosamente
     */
    function eliminarOpCarEmpleado(){
        // ---------- Set Nombres de Campos ---------------//
        $this->setNombres();
        //------------------ Metodo Set  -----------------//
        if(!$this->eliminarCarEmpleado()) return false;
        return true;
    }
    
    /**
     * Establece los nombres de la Entidad y los campos
     */
    function setNombres(){
        $this->setEntidad("Carnet de Empleado");
        
        $this->setCampos("idplantilla","Plantilla");
        $this->setCampos("iddepartamento","Departamento");
        $this->setCampos("idcargo","Cargo");
        $this->setCampos("cedula","C&eacute;dula");
        $this->setCampos("nombre","Nombre");
        $this->setCampos("apellido","Apellido");
        $this->setCampos("archivo_foto","Foto");
        $this->setCampos("fecha_vencimiento","Fecha de Vencimiento");        
        
    }

    /**
     * === Validacion Datos ===
     * @return boolean Devuelve verdadero si los datos estan correctos
     */
    function sonValidosDatos(){
        $datos   = array();
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idplantilla","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"iddepartamento","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"idcargo","tipoDato"=>"esNumerico");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"cedula","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"nombre","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"apellido","tipoDato"=>"esAlfaNumericoConEspacios");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>true,"datoName"=>"archivo_foto","tipoDato"=>"");
        //------------------------------------------------------------------------------------------------------------//
        $datos[] = array("isRequired"=>false,"datoName"=>"fecha_vencimiento","tipoDato"=>"");        
        //------------------------------------------------------------------------------------------------------------//

        if (!$this->validarDatos($datos))return false;
        
        return true;
    }

    /**
     * Generacion Local de listas dinamicas segun datos de base de datos
     * @param string $fields campos a seleccionar en la consulta
     * @param string $tabla tabla a consultar en base de datos
     * @param string $arg argumento consulta de base de datos
     * @param string $equal valor a comparar por metodo busca_valor()
     * @return string Devuelve opciones de listas dinamicas HTML
     */
    static function comboLocal($fields,$tabla,$arg, $equal){
        $sql  = "select ".$fields." from $tabla $arg";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query($sql);
        $nrows = $res->rowCount();
        $fetchArray = $res->fetchAll();

        $resp = "";
        $resp .= "<option value=\"\" idplantilla=\"\" chk=\"\" top=\"\" left=\"\" width=\"\" height=\"\" color=\"\" bgcolor=\"\" fuentetamano=\"\" fuenteletra=\"\" bgcolor_chk=\"\" fuentealign=\"\" fuentevalign=\"\" >Seleccione</option>";
        if ($nrows>0){
            foreach ($fetchArray as $row){

                $resp .= "<option value=\"".trim($row["id"])."\" idplantilla=\"".trim($row["idplantilla"])."\" chk=\"".trim($row["chk"])."\" top=\"".trim($row["top"])."\" left=\"".trim($row["left_"])."\" width=\"".trim($row["width"])."\" height=\"".trim($row["height"])."\" color=\"".trim($row["color"])."\" bgcolor=\"".trim($row["bgcolor"])."\" fuentetamano=\"".trim($row["fuentetamano"])."\" fuenteletra=\"".trim($row["fuenteletra"])."\" bgcolor_chk=\"".trim($row["bgcolor_chk"])."\" fuentealign=\"".trim($row["fuentealign"])."\" fuentevalign=\"".trim($row["fuentevalign"])."\" ".self::busca_valor($equal, $row["id"])." >";

                $resp .= $row["nombre"]." &nbsp;";

                $resp .= "</option>\n";

            }
        }else{
            $resp .= "<option value=\"\">No hay registros</option>";
        }
        return $resp;
    }
   
    /**
     * Genera imagen unica del carnet con todos los elementos configurados (FRENTE)
     * @param type $op Valor de impresion o pre-impresion (vista previa)
     */
    function setCarnetFront($op){
        
        $data = CarEmpleados::getCarPlantillaEmpleado(array("idplantilla"=>$_REQUEST["idplantilla"],"idcarnet_empleado"=>$_REQUEST["idcarnet_empleado"]));        
        $data->cargo_bgcolor_chk = "1"; // obligar el color del fondo del cargo
        $param["data"] = $data;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Imagen Plantilla --- //
        $archivo = $this->getFolders("imagesPlantilla").$data->archivo_plantilla;         
        $dst_image = imagecreatefromstring(file_get_contents($archivo));
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Redimencionar Imagen Plantilla --- //
        list($src_w,$src_h) = getimagesize($archivo);
        list($newWidth, $newHeight) = $this->getFixedFormatCarnet($data->orientacion);        
        $tempImg = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled ($tempImg, $dst_image , 0 ,0 , 0 , 0 , $newWidth , $newHeight , $src_w , $src_h);
        $dst_image  = $tempImg;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN LOGO --- //
        if ($data->logo_chk=="1"){
            $param["archivo"]   = $this->getFolders("imagesPlantilla").$data->archivo_logo;
            $param["dst_image"] = $dst_image;
            $param["obj"] = "logo";
            $dst_image = $this->setImgage($param);
        }        
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN FOTO CARNET ---//
        $param["archivo"]   = $this->getFolders("fotosCarnet").$data->archivo_foto;
        $param["dst_image"] = $dst_image;
        $param["obj"] = "foto";
        $dst_image = $this->setImgage($param);
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN CODIGO DE BARRAS --- //
        if ($data->barcode_front=="1"){            
            $_REQUEST["barcode"] = $data->cedula;
            $_REQUEST["accion"]="print";
            include_once "../view/car_empleado.barcodeImage.php";
            $param["src_image"] = $_REQUEST["barcodeImg"];
            $param["dst_image"] = $dst_image;
            $param["obj"] = "barcode";
            $dst_image = $this->setImgage($param);
        }       
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO INSTITUCION --- //
        if ($data->institucion_chk=="1"){
            $param["dst_image"] = $dst_image;
            $param["obj"] = "institucion";
            $param["text"] = $data->institucion_nombre;
            $dst_image = $this->setText($param);
        }
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO NOMBRE y APELLIDO --- //        
        $param["dst_image"] = $dst_image;
        $param["obj"] = "nombreapellido";
        $param["text"] = $data->nombre." ".$data->apellido;
        $dst_image = $this->setText($param);
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO CEDULA --- //
        $param["dst_image"] = $dst_image;
        $param["obj"] = "cedula";
        $param["text"] = "C.I. ".str_replace(",", ".", number_format($data->cedula));
        $dst_image = $this->setText($param);
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO FECHA --- //
        if ($data->fecha_chk=="1"){
            $param["dst_image"] = $dst_image;
            $param["obj"] = "fecha";
            $param["text"] = $this->formatoFecha($data->fecha_vencimiento);
            $dst_image = $this->setText($param);
        }
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO DEPARTAMENTO --- //        
        $param["dst_image"] = $dst_image;
        $param["obj"] = "departamento";
        $param["text"] = $data->departamento_nombre;
        $dst_image = $this->setText($param);
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO CARGO --- //
        $param["dst_image"] = $dst_image;
        $param["obj"] = "cargo";
        if ($data->cargo_nombre_chk=="1"){
            $param["text"] = $data->cargo_nombre;
        }else{
            $param["text"] = "";
        }
        $dst_image = $this->setText($param);
        
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMPRIMIR --- //
        if ($op=="print"){
            self::printCarnet($data,'F');
        }
        
        header('Content-Type: image/jpeg');        
        imagejpeg($dst_image,null,100);
        
    }
    
    /**
     * Genera imagen unica del carnet con todos los elementos configurados (REVERSO)
     * @param type $op Valor de impresion o pre-impresion (vista previa)
     */
    function setCarnetBack($op){
        $data = CarEmpleados::getCarPlantillaEmpleado(array("idplantilla"=>$_REQUEST["idplantilla"],"idcarnet_empleado"=>$_REQUEST["idcarnet_empleado"]));
        $data->cargo_bgcolor_chk = "1"; // obligar el color del fondo del cargo
        $param["data"] = $data;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Imagen Plantilla --- //
        $archivo = $this->getFolders("imagesPlantilla").$data->archivoback_plantilla;
        $dst_image = imagecreatefromstring(file_get_contents($archivo));
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Redimencionar Imagen Plantilla --- //
        list($src_w,$src_h) = getimagesize($archivo);
        list($newWidth, $newHeight) = $this->getFixedFormatCarnet($data->orientacion);
        $tempImg = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled ($tempImg, $dst_image , 0 ,0 , 0 , 0 , $newWidth , $newHeight , $src_w , $src_h);
        $dst_image  = $tempImg;
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN LOGO --- //
        if ($data->logoback_chk=="1"){
            $param["archivo"]   = $this->getFolders("imagesPlantilla").$data->archivoback_logo;
            $param["dst_image"] = $dst_image;
            $param["obj"] = "logoback";
            $dst_image = $this->setImgage($param);
        }     
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMAGEN CODIGO DE BARRAS --- //
        if ($data->barcode_back=="1"){
            $_REQUEST["barcode"] = $data->cedula;
            $_REQUEST["accion"]="print";
            include_once "../view/car_empleado.barcodeImage.php";
            $param["src_image"] = $_REQUEST["barcodeImg"];
            $param["dst_image"] = $dst_image;
            $param["obj"] = "barcode";
            $dst_image = $this->setImgage($param);
        }       
        //-------------------------------------------------------------------------------------------------------------------//
        // --- TEXTO INSTITUCION --- //
        if ($data->institucionback_chk=="1"){
            $param["dst_image"] = $dst_image;
            $param["obj"] = "institucionback";
            $param["text"] = $data->institucion_nombre;
            $dst_image = $this->setText($param);
        }
        //-------------------------------------------------------------------------------------------------------------------//
        // --- IMPRIMIR --- //
        if ($op=="print"){
            self::printCarnet($data,'B');
        }
        
        header('Content-Type: image/jpeg');
        imagejpeg($dst_image,null,100);
    }
    
    /**
     * Imprime (descarga) el carnet y guarda log de impresion
     * @param type $data Datos del carnet
     * @param type $modo Indica si es frente o reverso
     */
    private static function printCarnet($data,$modo){
        //-------------------------------------------------------------------------------------------------------------------//
        // --- Descarga Imegen --- //
        if ($modo=="F"){
            $file_nameModo="FRENTE";
        }
        if ($modo=="B"){
            $file_nameModo="REVERSO";
        }        
        //$file_name = "CARNET-".$file_nameModo."-".$data->cedula.".png";
        $file_name = "CARNET-".$file_nameModo."-".$data->cedula.".jpeg";
        header('Content-Disposition: attachment; filename="'.$file_name.'"'); // download file
        ////-----------------------------------------------------------------------------------------------------------------//
        // --- Log Impresion --- //
        $obj = new ControlOpCarEmpleadoImpresion();
        $_REQUEST["idcarnet_empleado"] = $data->idcarnet_empleado;
        $_REQUEST["usuario"] = $_SESSION["usuario"];
        $_REQUEST["modo"] = $modo;
        $obj->setAccion("agregar");
        $obj->setOpCarEmpleadoImpresion();
    }
    
}
?>