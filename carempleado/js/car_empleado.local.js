var loadingMsg = '<img src="../images/loading51.gif" alt="" width="16" /> Cargando...';
var widthV;
var heightV;
var widthH;
var heightH;
var val;
var objId     = [];
var styleObjs = []; // [0]: Obj to apply style, [1]: css property, [2]: css sub-property, [3]: val to apply

//------------------------------------------LOAD-------------------------------------------------------------//
$(function() {
    
    widthV  = parseInt($('#defaultWidth').val());
    heightV = parseInt($('#defaultHeight').val());
    widthH  = heightV;
    heightH = widthV;
    
    $( "input:button, input:submit,button" ).button();
    // ------------------------------ COMBO PLANTILLA ---------------------------------------- //
    $('#idplantilla')
        .live({
            change:function(){
                $('#botones').hide();
                var url ='car_empleado.Acc.php?idcarnet_empleado='+$('#idcarnet_empleado').val()+'&idplantilla='+$(this).val()+'&accion='+$('#accion').val();
                //alert(url);
                $('#datos').show().html(loadingMsg);
                document.location.href = url;                
            }
        });
    if (!$('#idplantilla').val()){
        $('#datos, #botones').hide();
    }
    
    // ------------- IU DatePicker jQuery Calendar Plugin ------------------------------------- //
    $.datepicker.setDefaults( $.datepicker.regional['es'] );
    $('#fecha_vencimiento').datepicker({
        inline: true,
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        minDate : 'd m Y',
        onSelect: function(dateText) {
            $('#fecha_view').html(dateText);
        }
    }).attr('readonly',true).css({
        background: '#fff' ,
        border: '1px solid #d5d5d5',
        '-moz-border-radius': '4px',
        '-webkit-border-radius': '4px',
        'border-radius': '4px'
    });

    //-----------------------------------MENSAJE-----------------------------------------------//
    $('#divmensaje').live({
        click: function() {
            $(this).fadeOut(500);
        }
    });
    //---------------------------------------DIV CARNET------------------------------------------//
    $('#divCarnet')
        .width(setWidth())
        .height(setHeight())
        .css({
            'background':'#D3D3D3',
            'background-size':'100%'
        });
    if ($('#orientacion').val()=='V'){
        $('#divCarnet, #carnetImg').width(widthV).height(heightV);
    }
    if ($('#orientacion').val()=='H'){
        $('#divCarnet, #carnetImg').width(widthH).height(heightH);
    }
    $('div#foto, div#logo, div#institucion,div#nombreapellido,div#cedula,div#barcode,div#fecha')
        .css({
            top: function(){
                return $('#'+$(this).attr('id')+'_top').val();
            },
            left: function(){
                return $('#'+$(this).attr('id')+'_left').val();
            },
            width: function(){
                return $('#'+$(this).attr('id')+'_w').val();
            },
            height: function(){
                return $('#'+$(this).attr('id')+'_h').val();
            },
            'background': function(){
                if ($(this).attr('id')=='foto'){
                    if ($('#archivo_foto').val()==""){
                        return '#fff url(../images/User_64.png) 50% 50% no-repeat';
                    }else{
                        return '#fff url(../images/fotos/'+$('#archivo_foto').val()+') no-repeat';
                    }
                 
                }
                if ($(this).attr('id')=='logo'){                    
                    return 'url(../../carplantilla/images/imagesPlantillas/'+$('#archivo_logo').val()+') 50% 50% no-repeat';
                }
                
            },
            'background-size': function(){
                if ($(this).attr('id')=='logo' || $(this).attr('id')=='foto'){
                    return '100%';
                }
            }
        })
    //----------------------------------------BARCODE IMG----------------------------------------------//
    $('div#barcode img').attr({
        src: setBarcodeImg()        
    });
    
    //----------------------------------COMBOS DE ESTILOS-OBJS-----------------------------------------//
    $(".styleObjs")
        .each(function(){
            objId = $(this).attr('id').split('_');
            
            styleObjs[0] = '_view';
            styleObjs[2] = '';
            styleObjs[3] = $('#'+objId[0]+'_'+objId[1]).val();

            if (objId[1] == 'fuentetamano'){                
                styleObjs[1] = 'font-size';
                styleObjs[2] = 'pt'

            }
            if (objId[1] == 'fuenteletra'){
                styleObjs[1] = 'font-family';                
            }
            if (objId[1] == 'fuentealign'){
                styleObjs[1] = 'text-align';
            }
            if (objId[1] == 'fuentevalign'){                
                styleObjs[1] = 'vertical-align';
            }
            if (objId[1] == 'color'){                
                styleObjs[1] = 'color';
                styleObjs[3] = "#"+styleObjs[3];
            }
            if (objId[1] == 'bgcolor'){                
                styleObjs[0] = '';
                styleObjs[1] = 'backgroundColor';
                styleObjs[3] = "#"+styleObjs[3];
            }
            $('#'+objId[0]+styleObjs[0]).css(styleObjs[1],styleObjs[3]+styleObjs[2]);
        });
    //-----------------------------------NOMBRES-----------------------------------------------//
    $(".nombres").live({
        keyup: function(){
            setNombres(this);
        }
    }).each(function(){        
        setNombres(this);
    });
    //-----------------------------------NOMBRES COMBOS-----------------------------------------------//
    $(".nombresCombos").live({
        change: function(){
            setNombresCombos(this);
        }
    }).each(function(){
        setNombresCombos(this);
    });
    //-----------------------------------OBJETOS CHEQUEADOS PARA VISIBILIDAD--------------------------------------//
    $(".chkObj")
        .each(function(){
            objId = $(this).attr('id').split('_');
            
            if (objId[1] !="bgcolor"){

                if ($(this).val()=="1"){
                    $('#'+objId[0]).show();
                }else{
                    $('#'+objId[0]).hide();
                }
            }else{
                if ($(this).val()==""){
                    $('#'+objId[0]).css('backgroundColor','');
                }
            }
        });

    //-------------------------------------------AJAX CEDULA------------------------------------------------------//
    cedulaChk();
    $('#cedula_emp').live({
        change:function(){
            cedulaChk();
        },
        keyup: function(){
            //----------------------------------------BARCODE IMG----------------------------------------------//
            $('div#barcode img').attr({
                src: setBarcodeImg()                
            });
        }
    });
    // --- Caja de Dialogo para captura o carga de foto --- //
    $("#loadFoto").dialog({
       autoOpen: false,
       resizable: false,
       draggable: false,
       width: '98%',
       height:600,
       position:'top',
       modal:true,
       open: function(event, ui){
           if ($('#cedula_emp').val()==""){
               $(this).dialog('close');
               alert('Debe ingresar el campo C\xe9dula');
               return false;
           }           
           var d = new Date();
            $("#frameFoto").attr('src','../../fotoloader/view/fotoloader.php?idcarnet_empleado='+$('#idcarnet_empleado').val()+'&cedula='+$('#cedula_emp').val()+'&archivo_foto='+$('#archivo_foto').val()+'&foto_w='+$('#foto_w').val()+'&foto_h='+$('#foto_h').val()+'&tipo=empleado&'+d.getMilliseconds());
       },
       close: function(event, ui) {
           $('#frameFoto').attr('src','');
       }
    });
    
});
//---------------------------------------END LOAD-----------------------------------------------------------------//
function cedulaChk(){
   $.ajax({
        beforeSend: function(){
            $('#divmensaje').html('').hide();
            $('#cedulaMsg').html(loadingMsg);
            $('#botones input:submit').hide();
        },
        type: 'POST',
        dataType:'json',
        //url: 'car_empleado.cedulaChk.php',
        url: 'car_empleado.Op.php',
        data: 'idcarnet_empleado='+$('#idcarnet_empleado').val()+'&cedula='+$('#cedula_emp').val()+'&accion=cedulaChk',
        success: function(data){                                
            $('#cedulaMsg').html('');
            if (data.chk=='1'){

                $('#botones input:submit').hide();
                str  = '<div class=\"ui-widget\">';
                str += '    <div class=\"ui-state-error ui-corner-all\" style=\"margin-top: 20px; padding: 0 .7em;\">';
                str += '        <p>';
                str += '            <span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span>';
                str += '                La c&eacute;dula ya esta registrada para otro carnet';
                str += '        </p>';
                str += '    </div>';
                str += '</div>';
                $('#divmensaje').html(str).fadeIn(500);
                return false;
            }
            $('#botones input:submit').show();            
            if (parseInt(data.access)>0){
                $('#cedulaMsg').html('<img src="../images/Credit-Card-UI-32.png" alt="Tarjeta de Acceso Asignada" title="Tarjeta de Acceso Asignada" style="position: absolute;" width="32"/>');
            }                    
            return true;
        }
    });
}
/**
 * Establece el Ancho segun la orientacion
 */
function setWidth(){
    if ($('#orientacion').val()=="V"){
        return widthV;
    }
    if ($('#orientacion').val()=="H"){
        return widthH;
    }
    return true;
}
/**
 * Establece el Alto segun la orientacion
 */
function setHeight(){
    if ($('#orientacion').val()=="V"){
        return heightV;
    }
    if ($('#orientacion').val()=="H"){
        return heightH;
    }
    return true;
}

/**
 * Establece los valores de la lista de seleccion en la vista previa del carnet
 */
function setNombresCombos(obj){
    objId = $(obj).attr('id').split('_');

    if (objId[0]!="cargo" || (objId[0]=="cargo" && $('#'+objId[0]+'_id option:selected').attr('chk')=="1" )){
        $('#'+objId[0]+'_view').html($('#'+objId[0]+'_id option:selected').text());
    }
    if (objId[0]=="cargo" && $('#'+objId[0]+'_id option:selected').attr('chk')==""){
            $('#'+objId[0]+'_view').html('');
    }

    // ---------- Set Posision Obj segun parametros de BD ---------//

    $('#'+objId[0]).css({
        top: function(){
            val = $('#'+objId[0]+'_id option:selected').attr('top');
            return val;
        },
        left: function(){
            val = $('#'+objId[0]+'_id option:selected').attr('left');
            return val;
        },
        width: function(){
            val = $('#'+objId[0]+'_id option:selected').attr('width');
            return val;
        },
        height: function(){
            val = $('#'+objId[0]+'_id option:selected').attr('height');
            return val;
        },
        color: function(){
            val = $('#'+objId[0]+'_id option:selected').attr('color');
            return val;
        },
        'backgroundColor': function(){
            val = $('#'+objId[0]+'_id option:selected').attr('bgcolor');
            if (objId[0] != 'cargo' ){
                if ($('#'+objId[0]+'_id option:selected').attr('bgcolor_chk')==''){
                    val = '';
                }
            }
            return val;
        }
    }).hide();

    $('#'+objId[0]+'_view').css({
        'font-family': function(){
            val = $('#'+objId[0]+'_id option:selected').attr('fuenteletra');
            return val;
        },
        'font-size': function(){
            val = $('#'+objId[0]+'_id option:selected').attr('fuentetamano');
            return val+'pt';
        },
        'text-align': function(){
            val = $('#'+objId[0]+'_id option:selected').attr('fuentealign');
            return val;
        },
        'vertical-align': function(){
            val = $('#'+objId[0]+'_id option:selected').attr('fuentevalign');
            return val;
        }
    });
    
    if ($('#'+objId[0]+'_id').val()!="" ){
        $('#'+objId[0]).fadeIn(500);
    }
    if ($('#'+objId[0]+'_id').val()=="" ){
        $('#'+objId[0]).hide();
    }
}

/**
 * Establece los objetos de texto en la vista previa del carnet
 */
function setNombres(obj){
    objId = $(obj).attr('id').split('_');
    val = '';

    if (objId[0]=='nombre'){
        val = $(obj).val()+' '+$('#apellido').val();
    }
    if (objId[0]=='apellido'){
        val = $('#nombre').val()+' '+$(obj).val();
    }
    if (objId[0]=='cedula'){
        val = 'C.I. '+$.formatNumber($(obj).val(), {format:"#,###",locale:"es"}) ;
    }
    if (objId[0]=='nombre' || objId[0]=='apellido'){
        objId[0] = 'nombreapellido';
    }

    $('#'+objId[0]+'_view').html(val.replace('\n', '<br>'));

    if ($('#nombre').val()=="" && $('#apellido').val()=="" && objId[0] == 'nombreapellido'){
        $('#'+objId[0]+'_view').html('Nombre 1 Apellido 1');
    }
    if ($('#'+objId[0]+'_emp').val()=="" ){
        $('#'+objId[0]+'_view').html('C.I. XX.XXX.XXX');
    }
}

/**
 * Establece la foto capturada por el modulo fotoLoader
 */
function setPhoto(){
    var d = new Date();
    $('div#foto')
        .css({
            'background': function(){                
                if ($('#archivo_foto').val()==""){
                    return '#fff url(../images/User_64.png) 50% 50% no-repeat';
                }else{
                    return '#fff url(../images/fotos/'+$('#archivo_foto').val()+'?'+d.getMilliseconds()+') no-repeat';
                }               

            },
            'background-size': '100%'
            
        });
    $('#loadFoto').dialog('close');
}

/**
 * Establece la imagen del codigo de barras
 */
function setBarcodeImg(){
    if ($('#cedula_emp').val()==""){
        return '../images/barcodeb39.png';
    }else{
        return 'car_empleado.barcodeImage.php?barcode='+$('#cedula_emp').val();
    }
}