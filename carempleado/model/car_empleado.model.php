<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'car_empleado.tabla.php';

/**
 * Clase CarEmpleados{}
 * @author David Concepcion CENIT-DIDI
 */
class CarEmpleados extends CarEmpleado{
    
    /**
     * Consulta de Plantilla para carnet de empleado
     * @param array $arg argumentos de la consulta
     * @return object Devuelve registros como objeto
     */
    public static function getCarPlantillaEmpleado($arg){
        $sql = "SELECT /***DATOS PLANTILLA***/
                       cp.idplantilla,
                       cp.plantilla,
                       cp.orientacion,
                       cp.archivo_plantilla,
                       cp.logo_chk,
                       cp.archivo_logo,
                       cp.foto_top,
                       cp.foto_left,
                       cp.foto_w,
                       cp.foto_h,
                       cp.barcode_chk,
                       cp.barcode_front,
                       cp.barcode_top,
                       cp.barcode_left,
                       cp.barcode_w,
                       cp.barcode_h,
                       cp.logo_top,
                       cp.logo_left,
                       cp.logo_w,
                       cp.logo_h,
                       cp.institucion_chk,
                       cp.institucion_nombre,
                       cp.institucion_top,
                       cp.institucion_left,
                       cp.institucion_w,
                       cp.institucion_h,
                       cp.institucion_color,
                       cp.institucion_fuentetamano,
                       cp.institucion_fuenteletra,
                       cp.institucion_bgcolor_chk,
                       cp.institucion_bgcolor,
                       cp.institucion_fuentealign,
                       cp.institucion_fuentevalign,
                       cp.nombreapellido_top,
                       cp.nombreapellido_left,
                       cp.nombreapellido_w,
                       cp.nombreapellido_h,
                       cp.nombreapellido_color,
                       cp.nombreapellido_fuentetamano,
                       cp.nombreapellido_fuenteletra,
                       cp.nombreapellido_bgcolor_chk,
                       cp.nombreapellido_bgcolor,
                       cp.nombreapellido_fuentealign,
                       cp.nombreapellido_fuentevalign,
                       cp.cedula_top,
                       cp.cedula_left,
                       cp.cedula_w,
                       cp.cedula_h,
                       cp.cedula_color,
                       cp.cedula_fuentetamano,
                       cp.cedula_fuenteletra,
                       cp.cedula_bgcolor_chk,
                       cp.cedula_bgcolor,
                       cp.cedula_fuentealign,
                       cp.cedula_fuentevalign,
                       cp.fecha_chk,
                       cp.fecha_top,
                       cp.fecha_left,
                       cp.fecha_w,
                       cp.fecha_h,
                       cp.fecha_color,
                       cp.fecha_fuentetamano,
                       cp.fecha_fuenteletra,
                       cp.fecha_bgcolor_chk,
                       cp.fecha_bgcolor,
                       cp.fecha_fuentealign,
                       cp.fecha_fuentevalign,
                       cp.visitante_top,
                       cp.visitante_left,
                       cp.visitante_w,
                       cp.visitante_h,
                       cp.visitante_color,
                       cp.visitante_fuentetamano,
                       cp.visitante_fuenteletra,
                       cp.visitante_bgcolor_chk,
                       cp.visitante_bgcolor,
                       cp.visitante_fuentealign,
                       cp.visitante_fuentevalign,
                       cp.visitantenro_top,
                       cp.visitantenro_left,
                       cp.visitantenro_w,
                       cp.visitantenro_h,
                       cp.visitantenro_color,
                       cp.visitantenro_fuentetamano,
                       cp.visitantenro_fuenteletra,
                       cp.visitantenro_bgcolor_chk,
                       cp.visitantenro_bgcolor,
                       cp.visitantenro_fuentealign,
                       cp.visitantenro_fuentevalign,
                       

                       /***DATOS REVERSO***/                       
                       cp.archivoback_plantilla,                       
                       cp.barcode_back,
                       cp.logoback_chk,
                       cp.archivoback_logo,
                       cp.logoback_top,
                       cp.logoback_left,
                       cp.logoback_w,
                       cp.logoback_h,
                       cp.institucionback_chk,
                       cp.institucionback_top,
                       cp.institucionback_left,
                       cp.institucionback_w,
                       cp.institucionback_h,
                       cp.institucionback_color,
                       cp.institucionback_fuentetamano,
                       cp.institucionback_fuenteletra,
                       cp.institucionback_bgcolor_chk,
                       cp.institucionback_bgcolor,
                       cp.institucionback_fuentealign,
                       cp.institucionback_fuentevalign,
                       
                       /***DATOS EMPLEADO***/
                       ce.idcarnet_empleado,                       
                       ce.iddepartamento,
                       ce.idcargo,
                       ce.cedula,
                       ce.nombre,
                       ce.apellido,
                       ce.archivo_foto,
                       ce.fecha_vencimiento,

                        /***DATOS DEPARTAMENTO***/
                       cd.departamento_nombre,
                       cd.departamento_top,
                       cd.departamento_left,
                       cd.departamento_w,
                       cd.departamento_h,
                       cd.departamento_color,
                       cd.departamento_fuentetamano,
                       cd.departamento_fuenteletra,
                       cd.departamento_bgcolor_chk,
                       cd.departamento_bgcolor,
                       cd.departamento_fuentealign,
                       cd.departamento_fuentevalign,

                        /***DATOS cargo***/
                       cc.cargo_nombre_chk,
                       cc.cargo_nombre,
                       cc.cargo_top,
                       cc.cargo_left,
                       cc.cargo_w,
                       cc.cargo_h,
                       cc.cargo_color,
                       cc.cargo_fuentetamano,
                       cc.cargo_fuenteletra,
                       cc.cargo_bgcolor,
                       cc.cargo_fuentealign,
                       cc.cargo_fuentevalign                        

                 FROM car_plantilla cp
                    left join car_empleado ce on (ce.idcarnet_empleado = '".$arg["idcarnet_empleado"]."')                     
                    left join car_departamento cd on (ce.iddepartamento = cd.iddepartamento)
                    left join car_cargo cc on (ce.idcargo = cc.idcargo)
                 WHERE cp.idplantilla = '".$arg["idplantilla"]."'";
        //echo "<div align='left'><pre>".$sql."</pre></div>"; 
        //cp.idplantilla = ce.idplantilla and          
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }
    /**
     * Consulta de Cedula ya existenta
     * @param array $arg argumentos de la consulta
     * @return object Devuelve registros como objeto
     */
    static function getCedulaChk($arg){
        if ($arg["idcarnet_empleado"]){
            $id = " and idcarnet_empleado <> '".$arg["idcarnet_empleado"]."'";
        }        
        $sql = "select
                    ( select count(cedula) from car_empleado where cedula = '".$arg["cedula"]."' ".$id.")as chk ,
                    ( select count(cedula) from ctrl_usuario where cedula = '".$arg["cedula"]."' and tipo_tarjeta = 'empleado')as access";
        //echo "<div align='left'><pre>".$sql."</pre></div>";        
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            return false;
        }        
        return $res->fetchObject();
    }    
    
    /**
     * Consulta de Carnets de Empleados
     * @return object Devuelve un registro como objeto
     */
    function  getCarnetsEmpleados(){
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcarnet_empleadoBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcarnet_empleadoBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcarnet_empleado in (".$in.")";
        }
                
        $sql = "select /*start*/
                       idcarnet_empleado,
                       idplantilla,
                       iddepartamento,
                       idcargo,
                       cedula,
                       nombre,
                       apellido,
                       archivo_foto,
                       fecha_vencimiento
                       /*end*/
                from car_empleado 
                ".$arg."
                    order by cast(cedula as :cast_int),nombre,apellido";
        //echo "<div align='left'><pre>".$sql."</pre></div>";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);
    }
}
?>
