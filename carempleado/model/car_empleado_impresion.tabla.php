<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CarEmpleadoImpresion {
    
    private $idcarnet_empleado_impresion = null;
    private $idcarnet_empleado = null;
    private $usuario = null;
    private $modo = null;
    private $fecha = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcarnet_empleado_impresion($idcarnet_empleado_impresion){
        $this->idcarnet_empleado_impresion = $idcarnet_empleado_impresion;
    }
    public function getIdcarnet_empleado_impresion(){
        return $this->idcarnet_empleado_impresion;
    }
    public function setIdcarnet_empleado($idcarnet_empleado){
        $this->idcarnet_empleado = $idcarnet_empleado;
    }
    public function getIdcarnet_empleado(){
        return $this->idcarnet_empleado;
    }
    public function setUsuario($usuario){
        $this->usuario = $usuario;
    }
    public function getUsuario(){
        return $this->usuario;
    }
    public function setModo($modo){
        $this->modo = $modo;
    }
    public function getModo(){
        return $this->modo;
    }
    public function setFecha($fecha){
        $this->fecha = $fecha;
    }
    public function getFecha(){
        return $this->fecha;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcarnet_empleado_impresion !== null) && (trim($this->idcarnet_empleado_impresion)!=='') ){
            $campos .= "idcarnet_empleado_impresion,";
            $valores .= "'".$this->idcarnet_empleado_impresion."',";
        }
        if(($this->idcarnet_empleado !== null) && (trim($this->idcarnet_empleado)!=='') ){
            $campos .= "idcarnet_empleado,";
            $valores .= "'".$this->idcarnet_empleado."',";
        }
        if(($this->usuario !== null) && (trim($this->usuario)!=='') ){
            $campos .= "usuario,";
            $valores .= "'".$this->usuario."',";
        }
        if(($this->modo !== null) && (trim($this->modo)!=='') ){
            $campos .= "modo,";
            $valores .= "'".$this->modo."',";
        }
        if(($this->fecha !== null) && (trim($this->fecha)!=='') ){
            $campos .= "fecha,";
            $valores .= "'".$this->fecha."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO car_empleado_impresion $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE car_empleado_impresion SET ";
        
        if(($this->idcarnet_empleado !== null) && (trim($this->idcarnet_empleado)!=='') ){
            $sql .= "idcarnet_empleado = '".$this->idcarnet_empleado."',";
        }
        if(($this->usuario !== null) && (trim($this->usuario)!=='') ){
            $sql .= "usuario = '".$this->usuario."',";
        }
        if(($this->modo !== null) && (trim($this->modo)!=='') ){
            $sql .= "modo = '".$this->modo."',";
        }
        if(($this->fecha !== null) && (trim($this->fecha)!=='') ){
            $sql .= "fecha = '".$this->fecha."',";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcarnet_empleado_impresion = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM car_empleado_impresion  WHERE idcarnet_empleado_impresion = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CarEmpleadoImpresions
     * @return object Devuelve un registro como objeto
     */
    function  getCarEmpleadoImpresions(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcarnet_empleado_impresionBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcarnet_empleado_impresionBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcarnet_empleado_impresion in (".$in.")";
        }
        
        $sql = "select idcarnet_empleado_impresion,idcarnet_empleado,usuario,modo,fecha from car_empleado_impresion ".$arg." order by usuario,modo";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CarEmpleadoImpresion
     * @param int $idcarnet_empleado_impresion Codigo
     * @return object Devuelve registros como objeto
     */
    function getCarEmpleadoImpresion($idcarnet_empleado_impresion){
        $sql = "SELECT idcarnet_empleado_impresion,idcarnet_empleado,usuario,modo,fecha FROM car_empleado_impresion WHERE idcarnet_empleado_impresion = '".$idcarnet_empleado_impresion."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>