<?php 
/*******************************************************************************\
  *     @copyright
  * 
  *                                 === SIPve ===
  *     Sistema Integrado de Protección con capacidades de Videovigilancia
  *     Control de Acceso y Carnetización para el resguardo físico de instalaciones.
  * 
  *     Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
  *                        Dirección de Investigación, Desarrollo e Innovación.
  *                        Gilda Ramos.
  *                        José Medina.
  *                        Héctor Reverón.
  *                        David Concepción.
  *                        Ronald Delgado.
  *                        Jenner Fuentes.
  * 
  *     This program is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either VERSION 3 of the License, or
  *     (at your option) any later version.
  *     
  *     This program is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  * 
  *     You should have received a copy of the GNU General Public License
  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.  
  * 
  *     Para mas información visite 
  *     @link http://repositorio.softwarelibre.gob.ve/ - RNA
  *     @link http://sourceforge.net/projects/sipve/   - SourceForge
  *     @link https://gitlab.com/talueses/SIPVE - Gitlab Repositorio.
  * 
 \*******************************************************************************/
?>
<?php
require_once 'dblink.php';
class CarEmpleado {
    
    private $idcarnet_empleado = null;
    private $idplantilla = null;
    private $iddepartamento = null;
    private $idcargo = null;
    private $cedula = null;
    private $nombre = null;
    private $apellido = null;
    private $archivo_foto = null;
    private $fecha_vencimiento = null;
    
    private $mensaje = null;

    //============================GETER Y SETER FUNCIONES ====================================//
    public function  setMensaje($mensaje){
        $this->mensaje = $mensaje;
    }
    public function  getMensaje(){
        return $this->mensaje;
    }
    public function setIdcarnet_empleado($idcarnet_empleado){
        $this->idcarnet_empleado = $idcarnet_empleado;
    }
    public function getIdcarnet_empleado(){
        return $this->idcarnet_empleado;
    }
    public function setIdplantilla($idplantilla){
        $this->idplantilla = $idplantilla;
    }
    public function getIdplantilla(){
        return $this->idplantilla;
    }
    public function setIddepartamento($iddepartamento){
        $this->iddepartamento = $iddepartamento;
    }
    public function getIddepartamento(){
        return $this->iddepartamento;
    }
    public function setIdcargo($idcargo){
        $this->idcargo = $idcargo;
    }
    public function getIdcargo(){
        return $this->idcargo;
    }
    public function setCedula($cedula){
        $this->cedula = $cedula;
    }
    public function getCedula(){
        return $this->cedula;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setApellido($apellido){
        $this->apellido = $apellido;
    }
    public function getApellido(){
        return $this->apellido;
    }
    public function setArchivo_foto($archivo_foto){
        $this->archivo_foto = $archivo_foto;
    }
    public function getArchivo_foto(){
        return $this->archivo_foto;
    }
    public function setFecha_vencimiento($fecha_vencimiento){
        $this->fecha_vencimiento = $fecha_vencimiento;
    }
    public function getFecha_vencimiento(){
        return $this->fecha_vencimiento;
    }
    

    //============================ FUNCION PARA INSERTAR =====================================//
    public function insertarRegistro(){
        
        $campos = ""; $valores = "";

        if(($this->idcarnet_empleado !== null) && (trim($this->idcarnet_empleado)!=='') ){
            $campos .= "idcarnet_empleado,";
            $valores .= "'".$this->idcarnet_empleado."',";
        }
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $campos .= "idplantilla,";
            $valores .= "'".$this->idplantilla."',";
        }
        if(($this->iddepartamento !== null) && (trim($this->iddepartamento)!=='') ){
            $campos .= "iddepartamento,";
            $valores .= "'".$this->iddepartamento."',";
        }
        if(($this->idcargo !== null) && (trim($this->idcargo)!=='') ){
            $campos .= "idcargo,";
            $valores .= "'".$this->idcargo."',";
        }
        if(($this->cedula !== null) && (trim($this->cedula)!=='') ){
            $campos .= "cedula,";
            $valores .= "'".$this->cedula."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $campos .= "nombre,";
            $valores .= "'".$this->nombre."',";
        }
        if(($this->apellido !== null) && (trim($this->apellido)!=='') ){
            $campos .= "apellido,";
            $valores .= "'".$this->apellido."',";
        }
        if(($this->archivo_foto !== null) && (trim($this->archivo_foto)!=='') ){
            $campos .= "archivo_foto,";
            $valores .= "'".$this->archivo_foto."',";
        }
        if(($this->fecha_vencimiento !== null) && (trim($this->fecha_vencimiento)!=='') ){
            $campos .= "fecha_vencimiento,";
            $valores .= "'".$this->fecha_vencimiento."',";
        }
                
        
        $campos = "($campos)";
        $valores = "($valores)";
        $campos = str_replace(",)",")",$campos);
        $valores = str_replace(",)",")",$valores);
        $sql = "INSERT INTO car_empleado $campos VALUES $valores";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return true;
    }

    //============================ FUNCION PARA MODIFICAR ====================================//
    public function modificarRegistro($valor){
        $sql = "UPDATE car_empleado SET ";
        
        if(($this->idplantilla !== null) && (trim($this->idplantilla)!=='') ){
            $sql .= "idplantilla = '".$this->idplantilla."',";
        }
        if(($this->iddepartamento !== null) && (trim($this->iddepartamento)!=='') ){
            $sql .= "iddepartamento = '".$this->iddepartamento."',";
        }
        if(($this->idcargo !== null) && (trim($this->idcargo)!=='') ){
            $sql .= "idcargo = '".$this->idcargo."',";
        }
        if(($this->cedula !== null) && (trim($this->cedula)!=='') ){
            $sql .= "cedula = '".$this->cedula."',";
        }
        if(($this->nombre !== null) && (trim($this->nombre)!=='') ){
            $sql .= "nombre = '".$this->nombre."',";
        }
        if(($this->apellido !== null) && (trim($this->apellido)!=='') ){
            $sql .= "apellido = '".$this->apellido."',";
        }
        if(($this->archivo_foto !== null) && (trim($this->archivo_foto)!=='') ){
            $sql .= "archivo_foto = '".$this->archivo_foto."',";
        }
        if(($this->fecha_vencimiento !== null) && (trim($this->fecha_vencimiento)!=='') ){
            $sql .= "fecha_vencimiento = '".$this->fecha_vencimiento."',";
        }
        else{
            $sql .= "fecha_vencimiento = NULL,";
        }
        

        $sql .= ",";
        $sql  = str_replace(",,","",$sql);

        $sql .= " WHERE idcarnet_empleado = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }

    //============================ FUNCION PARA ELIMINAR ====================================

    public function eliminarRegistro($valor){
        $sql = "DELETE FROM car_empleado  WHERE idcarnet_empleado = '".$valor."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return true;
    }//fin de la funcion eliminar

    /**
     * Consulta de CarEmpleados
     * @return object Devuelve un registro como objeto
     */
    function  getCarEmpleados(){
        
        // --- Valores del Buscador --- //
        if ($_REQUEST["idcarnet_empleadoBuscador"]){
            $in = null;
            foreach ($_REQUEST["idcarnet_empleadoBuscador"] as $row){
                $in.= "'".$row."',";
            }
            $in = substr($in, 0,(strlen($in)-1));
            $arg = " where idcarnet_empleado in (".$in.")";
        }
        
        $sql = "select /*start*/ idcarnet_empleado,idplantilla,iddepartamento,idcargo,cedula,nombre,apellido,archivo_foto,fecha_vencimiento /*end*/ from car_empleado ".$arg." order by cedula,nombre";
        $res = DB_Class::DB_Query(paginationSQL::setSql($sql));
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }
        return $res->fetchAll(PDO::FETCH_CLASS);        
    }

    /**
     * Consulta de CarEmpleado
     * @param int $idcarnet_empleado Codigo
     * @return object Devuelve registros como objeto
     */
    function getCarEmpleado($idcarnet_empleado){
        $sql = "SELECT idcarnet_empleado,idplantilla,iddepartamento,idcargo,cedula,nombre,apellido,archivo_foto,fecha_vencimiento FROM car_empleado WHERE idcarnet_empleado = '".$idcarnet_empleado."'";
        $res = DB_Class::DB_Query($sql);
        if (!$res) {
            $this->setMensaje(DB_Class::$dbErrorMsg);
            return false;
        }        
        return $res->fetchObject();        
    }

}
?>