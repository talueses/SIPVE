Sistema Integral de Protección Venezolano - SIPve v0.2.2-R19 (Beta) 2016
==========================================================================
0- Léeme o Guía de inicio rápido

Contenido:

1- Acerca de SIPve
2- Requerimientos
3- Instalación y configuración
    3.1- Instalación paquetes necesarios
    3.2- Instalación SIPve
    3.3- Configuraciones
        1- Base de Datos
        2- Conexión Base de Datos-SIPve 
        3- Reiniciar Apache
        4- Servidores de Video
        5- Script de sincronización Control Acceso
4- Ejecución
5- Licencia
6- Contactos

==========================================================================
1- Acerca de SIPve

    El Sistema Integrado de Protección Venezolano SIPve permite el 
    resguardo físico de instalaciones por medio de captura de video 
    (Videovigilancia), Control de Acceso a áreas restringidas e 
    identificación de personal por medio de Carnetización. 
    Está compuesto por cuatro módulos fundamentales que implementan  
    PHP, Python, C ANSI, HTML, JavaSript, JQuery, MySql 5x o PostgreSql 8.4 
    o superior, que conforman una solución tecnológica integrada.

	Estos cuatro módulos son:

    - Administración.
    - Videovigilancia.
    - Control de Acceso.
    - Carnetización.
    
==========================================================================
2- Requerimientos
    
    Se requieren de una serie de paquetes de software que son necesarios 
    instalar y configurar antes de proceder a la instalación del sistema.

    El sistema ha sido probado en distribuciones Linux Debian y derivados

    Puede ejecutarse en cualquier ambiente donde se puedan ejecutar los 
    paquetes que abajo se describen.

    Se requieren configurar dos tipos de servidores:

    - Servidor del Sistema. Paquetes necesarios:
        - apache2
        - php5,php5-curl,php5-gd, php5-pgsql ó php5-mysql
        - mysql-server o postgresql
        - build-essential
        - ssh
        - python-pexpect

    - Servidores de Video. Paquetes necesarios:
        - apache2
        - ssh
        - motion
        
==========================================================================
3- Instalación y configuración

    Ejecutar los comandos como root:

    3.1- Instalación paquetes necesarios

        === Servidor del Sistema ===
        
            # apt-get install apache2 php5 php5-curl php5-gd ssh python-pexpect build-essential

        Puede instalar MySql o PostgreSQL    

        Myqsl
            # apt-get install mysql-server php5-mysql

        PostgreSQL
            # apt-get install postgresql php5-pgsql

        === Servidores de Video ===

            # apt-get install apache2 ssh motion

    3.2- Instalación SIPve
    
        Descargar archivo comprimido desde: 
             https://sourceforge.net/projects/sipve/files/

        Descomprimir archivo en directorio Web del Apache
            # cd /var/www/
            # mkdir sipve
            # cd sipve
            # tar -xvzf sipve-vx.x.x.tar.gz

    3.3- Configuraciones

        1- Base de Datos

            --- MySql ---
            - Crear Base de Datos
            # mysql -u root -p
            # create database sipve;
            # exit;

            - Importar Base de Datos
            # mysql -u root -p -D sipve < sipve/inicio/model/YSQL_sip_base.sql
            # mysql -u root -p

            - Crear usuario de Conexión
            # CREATE USER sipve@localhost IDENTIFIED BY '***'; 
            # GRANT USAGE ON sipve.* TO sipve@localhost IDENTIFIED BY '***' ;
            # GRANT SELECT,INSERT,UPDATE,DELETE ON sipve.* TO sipve@localhost;
            # exit;

            --- PostgreSQL ---
            - Crear usuario de Conexión
            # createuser --no-createrole --no-createdb --no-superuser -EP sipve -U postgres -W

            - Crear Base de Datos
            # createdb sipve -O sipve -U postgres -W

            - Importar Base de Datos
            # psql -U sipve -W -d sipve < sipve/inicio/model/PSQL_sip_base.sql

        2- Conexión Base de Datos-SIPve 
            Editar el archivo sipve/inicio/model/conf.ini:
                dbtype      = mysql         ;tipo de base de datos: MySql o PostgreSQL
                dbname      = sipve         ;nombre de la base de datos
                user        = sipve         ;usuario de base de datos
                password    = ***           ;clave del usuario de base de datos
                host        = localhost     ;servidor de la base de datos
        
        3- Reiniciar Apache
            # /etc/init.d/apache2 restart
    
        4- Servidores de Video
            En los servidores de video crear enlace simbólico
            # ln -s "/home/user/motion" /var/www/

        5- Script de sincronización Control Acceso
            Compilar Script en el servidor del sistema
            # cd sipve/inicio/shell_script/
            # g++ comm_access_control.c -o access_controller_comunicator -I/usr/include/mysql -lmysqlclient
            # ./ctrlAccProcess.sh start

    Para más información y documentación adicional refiérase a documentos 
    en directorio:
        sipve/inicio/docs
==========================================================================
4- Ejecución
    
    En un navegador Web ingresar al sistema mediante la dirección:
    http://mi.dominio.com/sipve o http://mi.direccion.ip/sipve

    Usuario root del sistema:
    
    Usuario: admin
    clave: adminadmin

    El sistema a sido probado y certificado en navegadores:
        - Mozilla Firefox
        - Google Chrome

==========================================================================
5- Licencia

Copyright (C) 2012 Fundación Centro Nacional de Innovación Tecnológica, Cenit.
                   Dirección de Investigación, Desarrollo e Innovación.
                   Gilda Ramos.
                   José Medina.
                   Héctor Reverón.
                   David Concepción.
                   Ronald Delgado.
                   Jenner Fuentes.

GNU General Public License version 3 o posterior 
Ver archivo LICENSE.txt contenido en el software

==========================================================================
6- Contactos

Para mas información refiérase a:

  http://forja.softwarelibre.gob.ve/projects/sip/
  https://sourceforge.net/projects/sipve/   

  Documentación en línea (Wiki)    
     http://sourceforge.net/p/sipve/wiki/Home/

  Noticias
     http://sourceforge.net/p/sipve/news/

  Información 
     http://sourceforge.net/p/sipve/discussion/

   Administradores del proyecto:

    David Concepcion: https://sourceforge.net/users/davidconcepcion
    Gilda Ramos:      https://sourceforge.net/users/gildaramos
    Jose Medina:      https://sourceforge.net/users/medinajose
    Jenner Fuentes:   https://gitlab.com/talueses

